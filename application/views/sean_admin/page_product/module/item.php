<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
if(isset($_result01) && is_array($_result01))
{
 	foreach($_result01 as $key => $value)
  {
  	
  	$_result_item=$value;
  	
  }   
}

?>
<link href="public/metronic/global/plugins/jquery-minicolors/jquery.minicolors.css" rel="stylesheet" type="text/css"/>


<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品規格 <small>規格單位編輯</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="<?php echo $init_control;?>">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品規格
        <i class="fa fa-circle"></i>
      </li>
      <li>
         <a href="product/productModule">規格單位</a>
         <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         規格單位編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">編輯規格單位</span>
            </div>
            <div class="actions btn-set">
              <a href="<?php echo $_father_link;?>" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
          	<?php $this->load->view('admin-ijwedding/general/flash_error');?>
            <form class="form-horizontal" role="form" action="<?php echo $_action_link;?>"  method="POST">
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">名稱 <span class="require">*</span></label>
                <div class="col-md-10">
                  <input type="text" class="form-control" id="spec_option_name"  name="spec_option_name" maxlength="64" class="form-control" value="<?php if(isset($_result_item->spec_option_name)) echo $_result_item->spec_option_name;?>"  placeholder="如相簿模組、燙金等規格定義">
                </div>
              </div>
              <div class="form-group">
                <label for="note" class="col-md-2 control-label">商店 <span class="require">*</span></label>
                <div class="col-md-10">
                  <!-- 選項在 table ij_config 中的 type = shop_menu -->
                  <select id="spec_option_type" name="spec_option_type" class="form-control">
                     <!--  <option value=1 <?php if(isset($_result_item->spec_option_type) && $_result_item->spec_option_type==1) echo "selected";?>>雲端婚訊</option>
                     <option value=2 <?php if(isset($_result_item->spec_option_type) && $_result_item->spec_option_type==2) echo "selected";?>>現場報到系統</option>
                     --> <option value=3 <?php if(isset($_result_item->spec_option_type) && $_result_item->spec_option_type==3) echo "selected";?>>結婚商店</option>
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-2 control-label">分類 <span class="require">*</span></label>
                <div class="col-md-10">
                  <select id="category_spec_relation[]" name="category_spec_relation[]" multiple class="form-control">
                   	 <?php  echo $_upper_category_sn;?>
                  	</select>
                  <span class="help-block"> 按住 control 進行複選 </span>
                </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">使用限制</label>
                <div class="col-md-10">
                  <select name="option_limitation"  class="form-control">
                    <option value="">無</option>
                   <?php 
                   
                   foreach($_option_limitation as $key => $value)
                   {
                   		echo ' <option value="'.$value->name.'">'.$value->name.'</option>';	
                   }
                   ?>
                   
                  </select>
                  <!-- <span class="help-block"> 選項由 Table : ij_config 中的 product_option_limitation 拉出</span>-->
                </div>
              </div>
              <div class="form-group">
                <label for="password" class="col-md-2 control-label">備註</label>
                <div class="col-md-10">
                  <textarea class="form-control" name="description" id="description" rows="3"><?php if(isset($_result_item->description)) echo $_result_item->description;?></textarea>
                </div>
              </div>
              <div class="form-group">
                  <label for="seq" class="col-md-2 control-label">排序 <span class="require">*</span></label>
                  <div class="col-md-10">
                    <input type="text" id="sort_order" name="sort_order" class="form-control" value="<?php if(isset($_result_item->sort_order)) echo $_result_item->sort_order;?>">
                  </div>
                </div>
                
              <div class="form-group">
                <label for="email" class="col-md-2 control-label">是否啟用 <span class="require">*</span></label>
                <div class="col-md-10">
                  <div class="radio-list">
                      <label class="radio-inline">
                        <input type="radio" name="status" id="status" value="1" <?php if(isset($_result_item->status) && $_result_item->status==1) echo "checked";?>>  啟用  
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="status" id="status" value="0" <?php if(isset($_result_item->status) && $_result_item->status==0) echo "checked";?>>  不啟用 
                      </label>
                  </div>
                </div>
              </div>
              
             <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                    <a href="<?php echo $_father_link;?>"><button type="button" class="btn default">Cancel</button></a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->