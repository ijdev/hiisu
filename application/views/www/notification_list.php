<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容
*/
?>
<div class="container">

<!-- Page level plugin styles START -->
<!-- Page level plugin styles END -->
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <li class="active">全站通知</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
 <div id="shopcetlog"  class=" padding_left0 sidebar col-md-3 col-sm-4">
     <div class="sidebar-filter categories-menu">
		<h3>商品分類</h3>
		<ul class="padding_left0 categories_list">
			<li class="categories_list_sub_li" data-toggle="collapse" data-target="#side_menu1">商品分類 1
				<ul id="side_menu1" class="collapse"> 
					<li><a href="/shop/shopCatalog/41">商品分類 1.1</a></li>
					<li><a href="/shop/shopCatalog/42">商品分類 1.2</a></li>
					<li><a href="/shop/shopCatalog/43">商品分類 1.3</a></li>
					<li><a href="/shop/shopCatalog/47">商品分類 1.4</a></li>
				</ul>
			</li>
			<li class="categories_list_sub_li" data-toggle="collapse" data-target="#side_menu2">商品分類 2
				<ul id="side_menu2" class="collapse"> 
					<li><a href="/shop/shopCatalog/42">商品分類 2.1</a></li>
					<li><a href="/shop/shopCatalog/47">商品分類 2.2</a></li>
					<li><a href="/shop/shopCatalog/41">商品分類 2.3</a></li>
					<li><a href="/shop/shopCatalog/43">商品分類 2.4</a></li>
				</ul>
			</li>
			<li class="categories_list_sub_lastli"><a href="/shop/shopCatalog/42">商品分類 3</a></li>
		</ul>
	 </div>
	 
	 <div class="sidebar-filter filter2">
      		
		<h3 class="filter-txt " >商品篩選
		<i id="icon-filter" data-toggle="collapse" data-target="#side_menu3"  onclick="myFunction3()"  class="fa fa-minus-square" ></i>
		
		<i id="icon-filter3" data-toggle="collapse" data-target="#side_menu3" onclick="myFunction4()"  class="fa fa-plus-square"  ></i>
		</h3>		
				<script>
		
					function myFunction3() {
						document.getElementById("icon-filter3").style.display = "block";
						document.getElementById("icon-filter").style.display = "none";
						}
					function myFunction4() {
    
						document.getElementById("icon-filter3").style.display = "none";
						document.getElementById("icon-filter").style.display = "block";
						}
				</script>

		<div class="inner_filter2 collapse in" id="side_menu3">
		 <script>
  $(document).ready(function(){
    $('.fdcheck').on('change', function(){
      var category_list = [];
      $('#filterschk :input:checked').each(function(){
        var category = $(this).val();
        category_list.push(category);
      });

      if(category_list.length == 0)
        $('.filterpro').fadeIn();
      else {
        $('.filterpro').each(function(){
          var item = $(this).attr('data-tag');
          if(jQuery.inArray(item,category_list) > -1)
            $(this).fadeIn('slow');
          else
            $(this).hide();
        });
      }   
    });
  }); 
  </script>
			<div id="filterschk" class="menu2_chk_box">
				
				<h4>分類(預設1)</h4>
				
					<div class="padding_left0 chk_box_lbl">
						<input  type="checkbox" id="tab1_check" class="fdcheck" value="基礎保養" name="cate1">
						<label  class="sphov" for="tab1_check"><span>基礎保養</span></label>
					</div>
					<div class=" padding_left0 chk_box_lbl">
						<input type="checkbox" id="tab2_check" class="fdcheck" value="加強護理" name="cate2">
						<label   class="sphov"for="tab2_check"><span>加強護理</span></label>
					</div>
					<div class="padding_left0 chk_box_lbl">
						<input type="checkbox" id="tab3_check" class="fdcheck" value="臉部其他" name="cate3">
						<label  class="sphov" for="tab3_check"><span>臉部其他</span></label>
					</div>
			</div>
		
	
		</div>
      </div>
    </div>

  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="col-md-9 col-sm-8">
    <div class="product-page">
      <!--<h1>
        <i class="fa fa-comment-o"></i> 全站通知
      </h1>-->
      <?php $this->load->view('www/general/flash_error');?>
      <div class="row inbox">
      
          <table class="table table-striped table-advance table-hover">
            <thead>
              <tr>
				<h2>
					全站通知
				</h2>
				<!-- <th class="pagination-control" colspan="3" style="text-align:right;">
                	<?php echo $page_links;?>
                  <!--span class="pagination-info"> <?php echo @$start;?>-<?php echo @$end;?> of <?php echo @$total;?> </span>
                  <a class="btn btn-sm blue">
                    <i class="fa fa-angle-left"></i>
                  </a>
                  <a class="btn btn-sm blue">
                    <i class="fa fa-angle-right"></i>
                  </a>-->
                <!-- </th>-->
              </tr>
            </thead>
		
            <form method="post" id="form1">
            <tbody>
				<tr class="unread" data-messageid="1">    
					<td class="view-message hidden-xs">系統訊息</td>
					<td class="view-message">
                	    <a href="/home/newslist/detail/107"><font color="red" size="3px">網站啟用通知！</font></a>
	                </td>
					<td class="view-message text-right">
						2017-01-11 11:54:00
					</td>
				</tr>
				<tr class="unread" data-messageid="1">    
					<td class="view-message hidden-xs">系統訊息</td>
					<td class="view-message">
                	    <a href="/home/newslist/detail/108"><font color="red" size="3px">網站啟用通知！</font></a>
	                </td>
					<td class="view-message text-right">
						2017-01-10 10:14:12
					</td>
				</tr>
              <!--<?php if($messages){ foreach($messages as $key=>$_Item){?>
              <tr class="unread" data-messageid="<?php echo $key+1;?>">
               
                <td class="view-message hidden-xs">
                   <?php switch($_Item['message_code']){
                   		case 1:
                   			echo '客服:'.$_Item['update_member_sn'];
                   			break;
                   		default:
                   			echo '系統訊息';
                   		}?>
                </td>
                <td class="view-message" <?php echo (!$_Item['read_flag'])? 'style="font-weight: 700;font-size: 120%;"':'';?> >
                	<?php if($_Item['message_link_url']){?>
	                  
                  		<a target="_blank" href="member/notification/detail/<?php echo $_Item['member_message_center_sn'];?>">
	                    <?php echo $_Item['message_content'];?>&nbsp;
	                    <i class="fa fa-external-link"></i>
	                  </a>
	                  <?php }else{?>
                  		<a href="member/notification/detail/<?php echo $_Item['member_message_center_sn'];?>"><?php echo $_Item['message_subject'];?></a>
	                  <?php }?>
                </td>
                <td class="view-message text-right">
                   <?php echo $_Item['transmit_date'];?>
                </td>
              </tr>
              <?php }}?>-->
              <!--tr class="unread" data-messageid="2">
                <td class="inbox-small-cells">
                  <input type="checkbox" class="mail-checkbox">
                </td>
                <td class="view-message hidden-xs">
                   客服人員
                </td>
                <td class="view-message ">
                  <a href="http://dev.ijwedding.com/www/memberOrderList" target="_blank">
                    您的訂單已成立&nbsp;
                    <i class="fa fa-external-link"></i>
                  </a>
                </td>
                <td class="view-message text-right">
                   16:30 PM
                </td>
              </tr>
              <?php for($i=0;$i<18;$i++):?>
              <tr data-messageid="<?php echo $i+3;?>">
                <td class="inbox-small-cells">
                  <input type="checkbox" class="mail-checkbox">
                </td>
                <td class="view-message hidden-xs">
                  系統訊息
                </td>
                <td class="view-message">
                  <a href="www/notification/detail">您的密碼已修改</a>
                </td>
                <td class="view-message text-right">
                   March 15
                </td>
              </tr>
              <?php endfor;?>-->
            </tbody>
            <input type='hidden' name='flg' id='flg' value=''>
          </form>
          </table>
        </div>
        <!-- /.inbox-content -->
      </div>
    </div>
  </div>
  <!-- END CONTENT -->
</div>
</div>
<!-- END SIDEBAR & CONTENT -->
<style>
a.btn.btn-sm.blue.dropdown-toggle {
    background: #ab8b64;
} 



</style>