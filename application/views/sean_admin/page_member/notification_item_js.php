<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script src="public/metronic/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script src="public/metronic/admin/pages/scripts/components-pickers.js"></script>
<script>
jQuery(document).ready(function() {       
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
});
</script>

