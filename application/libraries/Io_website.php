<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Wedding website output library
 * 
 *
 */

class Io_website {
    var $CI;

    function __construct(){
        $this->CI =& get_instance();
    }
    
    /*
    * input module position, source url and thumb size
    * output thumb image url
    */
    public function get_thumb_url($posistion='gallery', $picture_save_dir='', $dir_sn=0 , $size='m')
    {
        switch($posistion)
        {
            case "album":
                if(ENVIRONMENT == 'development')
                {
                    return $this->CI->config->item('base_url').$picture_save_dir;
                }
                else
                {
                    return $this->CI->config->item('ij_frontend_cdn_path').$picture_save_dir;
                }
            break;
            default:
            case "gallery":
                if(ENVIRONMENT == 'development')
                {
                    return $this->CI->config->item('base_url').str_replace('website/'.$dir_sn.'/', 'website/'.$dir_sn.'/'.$size.'/', $picture_save_dir);
                }
                else
                {
                    return $this->CI->config->item('ij_frontend_cdn_path').str_replace('website/'.$dir_sn.'/', 'website/'.$dir_sn.'/'.$size.'/', $picture_save_dir);
                }
                
            break;
        }
    }

    public function get_cover_url($posistion='gallery', $picture_save_dir='', $dir_sn=0 , $size='m')
    {
        if(ENVIRONMENT == 'development')
        {
            return $this->CI->config->item('base_url').$picture_save_dir;
        }
        else
        {
            return $this->CI->config->item('ij_frontend_cdn_path').$picture_save_dir;
        }
    }
    
    /*
    * input module position, source url
    * output source or cdn image url
    */
    public function get_photo_square($posistion='gallery', $picture_save_dir='')
    {
        switch($posistion)
        {
            case "album":
            break;
            default:
            case "gallery":
                if(ENVIRONMENT == 'development')
                {
                    return $this->CI->config->item('base_url').$picture_save_dir;
                }
                else
                {
                    return $this->CI->config->item('ij_frontend_cdn_path').$picture_save_dir;
                }
                
            break;
        }
    }

    /*
    * get dealer name / link ..
    */
    public function gallery_dealer($material='name', $wedding_website_sn=0)
    {
        switch($material)
        {
            case "link":
                switch($wedding_website_sn)
                {
                    case 1:
                        return '<a href="http://www.cestbonweddings.com/index.html" target="_blank">photo by Cestbon Wedding</a>';
                    break;
                    case 2:
                        return '<a href="http://www.cestbonweddings.com/index.html" target="_blank">photo by Cestbon Wedding</a>';
                    break;
                    case 3:
                        return '<a href="http://www.cestbonweddings.com/index.html" target="_blank">photo by Cestbon Wedding</a>';
                    break;
                    case 4:
                        return '<a href="http://www.linliwedding.com/index" target="_blank">photo by Linli</a>';
                    break;
                    case 5:
                        return '<a href="http://www.linliwedding.com/index" target="_blank">photo by Linli</a>';
                    break;
                    default:
                        return '';
                    break;
                }
            break;
            case "name":
            default:
                switch($wedding_website_sn)
                {
                    case 1:
                        return 'Cestbon Wedding';
                    break;
                    case 2:
                        return 'Cestbon Wedding';
                    break;
                    case 3:
                        return 'Cestbon Wedding';
                    break;
                    case 4:
                        return 'Linli';
                    break;
                    case 5:
                        return 'Linli';
                    break;
                    default:
                        return '';
                    break;
                }
            break;
        }
    }

    /*
    * 
    */
    public function get_video_cover_overlay($material='album', $size = '')
    {
        switch($material)
        {
            case "album":
            default:
                if(ENVIRONMENT == 'development')
                {
                    return $this->CI->config->item('base_url').'public/img/bg_empty'.(($size)? ('_'.$size):'').'.png';
                }
                else
                {
                    return $this->CI->config->item('ij_frontend_cdn_path').'public/img/bg_empty'.(($size)? ('_'.$size):'').'.png';
                }
            break;
        }

    }
    
    
    public function ajax_feedback($result){
        header('Content-Type: text/plain; charset=utf-8');
        echo json_encode($result);
    }
    

    public function curl_post($api='', $para=array())
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$api);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($para));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);

        curl_close ($ch);

        return $server_output;
    }
    
    public function wedding_website_sn_encrypt($wedding_website_sn=0)
    {

        $iv = mcrypt_create_iv(
            mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC),
            MCRYPT_DEV_URANDOM
        );
        return base64_encode(
            $iv .
            mcrypt_encrypt(
                MCRYPT_RIJNDAEL_128,
                hash('sha256', $this->CI->config->item('encryption_key'), true),
                $wedding_website_sn,
                MCRYPT_MODE_CBC,
                $iv
            )
        );
    }
    
    public function wedding_website_sn_decrypt($encrypted='')
    {
        $data = base64_decode($encrypted);
        $iv = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));

        return rtrim(
            mcrypt_decrypt(
                MCRYPT_RIJNDAEL_128,
                hash('sha256', $this->CI->config->item('encryption_key'), true),
                substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
                MCRYPT_MODE_CBC,
                $iv
            ),
            "\0"
        );
    }
}
