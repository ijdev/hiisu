<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>聯盟會員分級 <small>分級列表</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        會員管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/member/partnerTable">聯盟會員</a>
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         聯盟會員分級管理
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->

    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">聯盟會員分級列表</span>
            </div>
            <div class="actions btn-set">
              <a href="<?php echo @$_action_link;?>" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <div class="portlet-body">
          	<?php $this->load->view('sean_admin/general/flash_error');?>
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>名稱</th>
                  <th>成交訂單分潤比例</th>
                  <th>招募新會員獎金</th>
                  <th>排序</th>
                  <th>是否啟用</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
               <?php

              	 if(is_array(@$_result01))
                	{
                 			foreach($_result01 as $key => $value)
                    		{
                    			if($value->status == 1)
                    				{
                    					$_active = '<i class="fa fa-check"></i>';
                    				}
                    			else
                    				{
                    					$_active = '<i class="fa fa-times"></i>';
                    				}

               ?>
                  <tr class="odd gradeX">
                    <td><?php echo $value->partner_level_name;?></td>
                    <td><?php echo $value->bonus;?>%</td>
                    <td><?php echo @$value->newmember;?></td>
                    <td><?php echo $value->sort_order;?></td>
                     <td class="center"><?php echo $_active;?></td>
                    <td class="center">
                      <a href="<?php echo $_edit_link.$value->$_table_key;?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
<?php if(stripos($this->ijw->check_permission(1,$this->router->fetch_class(),$this->router->fetch_method()),'del')){?>
                      <a href="<?php echo $_delete_link.$value->$_table_key;?>" onclick="return deletechecked();"><i class="fa fa-trash-o"></i>刪除</a>&nbsp;&nbsp;
<?php }?>
                    </td>
                  </tr>
                 <?php
			             }
			           }
           			?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->