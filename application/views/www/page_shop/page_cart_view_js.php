<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<script src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
<script src="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->
<script src="public/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script><!-- Quantity -->
<script src="public/metronic/global/plugins/rateit/src/jquery.rateit.js" type="text/javascript"></script>
<script src="//code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script><!-- for slider-range -->
<script src="public/metronic/frontend/layout/scripts/layout.js" type="text/javascript"></script>
<script type="text/javascript">
	var gift_cash='<?=$gift_cash;?>';
    jQuery(document).ready(function() {
        Layout.init();
        Layout.initOWL();
        Layout.initTouchspin();
        Layout.initFixHeaderWithPreHeader();
        Layout.initNavScrolling();
        //Layout.initUniform();
        Layout.initSliderRange();

	    jQuery('.various').fancybox({
			maxWidth	: 800,
			maxHeight	: 600,
			fitToView	: false,
			width		: '70%',
			autoSize	: true,
			closeClick	: false,
			openEffect	: 'none',
			closeEffect	: 'none',
	    //beforeLoad: function() {
				//console.log($(this));
				//return false;
	    //},
	    beforeClose: function() {
				$('#note_couponcode').fadeOut("slow");
				$('#couponcode').val('');
	    }
		});

    	$(".down-del-goods").click(function(){
	            var button = $(this);
	            var rowid=button.attr('rowid');
				var addon_from_rowid = button.attr('addon_from_rowid'); //主商品
				var supplier_sn=$(this).attr('supplier_sn');
	            if(!addon_from_rowid){ //如果是加購商品才抓主商品
	            	addon_from_rowid=rowid;
	            }
            	var r = confirm('是否確認刪除該購物車商品?（若有加購將一併刪除）');
                if(r)
                {
					$.post('shop/cartdel/', {dtable: '<?php echo base64_encode('cart');?>',dfield:'<?php echo base64_encode('rowid');?>',did:button.attr('rowid')}, function(data){
                    	if (!data.error){
			           		/*if(data.gift_cash){
								$('#show_gift_cash').text(data.gift_cash);
								$('#gift_cash').val(data.gift_cash);
			           		}
							checkoutupdate(false,rowid,supplier_sn);
                    		button.parents().parents().filter('tr').remove();
                    		$('.addon_from_rowid'+button.attr('rowid')).remove();*/
                    		location.reload();
                    	}
					},'json');
                }
        });
				$('.product_spec').change(function(event) {
					var qty_in_stock = parseInt($('option:selected', this).attr('qty_in_stock'));
					var mutispec_stock_sn = $(this).val();
					var rowid = $(this).attr('rowid');
				    var redsunglo = $('.tr_'+rowid).find('.red-sunglo');
					var qty = parseInt($(this).parent().parent().find('.qty').val());
					var min_order_qt = parseInt($(this).attr('min_order_qt'));
					var incremental = parseInt($(this).attr('incremental'));
					var price = parseInt($('option:selected', this).attr('price'));
					var promo_price = parseInt($('option:selected', this).attr('promo_price'));
				    var supplier_sn=$(this).attr('supplier_sn');
					if(price >0){ //改變價錢
						//if(price<=promo_price){
							$(this).parent().parent().find('.use_credit').attr('subtotal',qty*price);
							$(this).parent().parent().find('.goods-page-price').attr('price',price);
							$(this).parent().parent().find('.goods-page-price').html('<span>$</span>'+price);
							$(this).parent().parent().find('.goods-page-total').html('<span>$'+qty*price+'</span>');
						//}else{
						//	$(this).parent().parent().find('.use_credit').attr('subtotal',qty*promo_price);
						//	$(this).parent().parent().find('.goods-page-price').html('<strike><span>$</span>'+price+'</strike><br><strong><span>$</span>'+promo_price+'</strong>');
						//	$(this).parent().parent().find('.goods-page-total').html('<strong><span>$</span>'+qty*promo_price+'</strong>');
						//}
					}
					if(incremental >0){
						var html='';
	    			for (var x = min_order_qt; x <= qty_in_stock; x+=incremental) {
	    				ifselect=(x==qty)?'selected':'';
							html+='<option value="'+x+'" '+ifselect+'>'+x+'</option>';
	    			}
						$(this).parent().parent().find('.qty').find('option').remove().end().append(html);
					}else{
						//改變價錢
						//$(this).parent().parent().find('.qty').trigger("change");
				    $.post('shop/cartupdate/', {dval: price,dfield:'<?php echo base64_encode('price');?>',did:rowid}, function(data){
	          	if (!data.error){
	           		//$('.top-cart-info-count').text(data.total_items+' 件');
	           		//$('.top-cart-info-value').text('$ '+data.total);
	           		if(data.coupon_money && data.coupon_money > 0) redsunglo.text('已優惠：'+data.coupon_money);
	          	}
				    },'json');
				  }
				    $.post('shop/cartupdate/', {dval: mutispec_stock_sn,dfield:'<?php echo base64_encode('mutispec_stock_sn');?>',did:rowid}, function(data){
	          	if (!data.error){
	           		//$('.top-cart-info-count').text(data.total_items+' 件');
	           		//$('.top-cart-info-value').text('$ '+data.total);
	           		//更新購物車popup
	           		//$('.top-cart-content').load('shop/top_cart');
	           		if(data.coupon_money && data.coupon_money > 0) redsunglo.text('已優惠：'+data.coupon_money);
	          	}
				    },'json');
					if($("#rowid_"+rowid).is(":checked")){ //有勾選
						checkoutupdate(true,rowid,supplier_sn);
					}
				});
			$('.qty').change(function() {
					var rowid = $(this).attr('rowid');
					var addon_from_rowid = $(this).attr('addon_from_rowid');
				    var redsunglo = $('.tr_'+rowid).find('.red-sunglo');
					var qty = parseInt($(this).val());
					var price = $(this).parent().parent().find('.goods-page-price').attr('price');
					$(this).parent().parent().find('.use_credit').attr('subtotal',qty*price);
					var subtotal = $(this).parent().parent().find('.goods-page-total');
				    var supplier_sn=$(this).attr('supplier_sn');
					if($(this).val() > 0){
		    			$($('.addon_from_rowid'+rowid).find('.qty')).each(function(){
							//console.log(rowid);
							var addon_limitation = parseInt($(this).attr('addon_limitation'))*qty; //購買上限
							var unispec_qty_in_stock = parseInt($(this).attr('unispec_qty_in_stock')); //購買上限
							if (unispec_qty_in_stock < addon_limitation) addon_limitation=unispec_qty_in_stock;
							var addon_qty = parseInt($(this).val());
							var html='';
			    			for (var x = 0; x <= addon_limitation; x++) {
	    						ifselect=(x==addon_qty)?'selected':'';
									html+='<option value="'+x+'" '+ifselect+'>'+x+'</option>';
			    			}
							$(this)
							    .find('option')
							    .remove()
							    .end()
							    .append(html)
							;
						});
				$.post('shop/cartupdate/', {dval: qty,dfield:'<?php echo base64_encode('qty');?>',did:rowid}, function(data){
	          	if (!data.error){
	           		//$('.top-cart-info-count').text(data.total_items+' 件');
	           		//$('.top-cart-info-value').text('$ '+data.total);
	           		subtotal.html('<span>$'+data.subtotal+'</span>');
	           		$('#price_'+rowid).html('<span>$</span>'+data.price);
	           		if(data.coupon_money && data.coupon_money > 0) redsunglo.text('已優惠：'+data.coupon_money);
	          	}
			},'json');
            if(addon_from_rowid){ //抓主商品
            	rowid=addon_from_rowid;
            }
			//if($(".tr_"+rowid+' .checkboxrowid').is(":checked")){ //有勾選
			if($("#rowid_"+rowid).is(":checked")){ //有勾選
				checkoutupdate(true,rowid,supplier_sn);
				//$('.top-cart-content').load('shop/top_cart');
			}
					}
				});
				$('.addon_amount').click(function(event) {
					if($('#qty').val()==0 || !$('#qty').val()){
						alert('請先選擇主商品購買數量');
					}else{
						$('#addon'+$(this).attr('ij_addon_log_sn')).prop('checked', true);
					}
				});
        $('.use_credit').click(function(event) {
       		$(this).prop('disabled',true);
		});

        $('.use_credit').change(function(event) {
       		var use_credit= $(this);
       		//use_credit.prop('disabled',true);
			var rowid = $(this).attr('rowid');
			var addon_from_rowid = $(this).attr('addon_from_rowid');
			var subtotal = parseInt($(this).attr('subtotal'));
			//var gift_cash = parseInt($('#gift_cash').val());
			var pagetotal = $(this).parent().parent().find('.goods-page-total');
			var supplier_sn=$(this).attr('supplier_sn');
			console.log(gift_cash);

            if($(this).prop('checked') == true){
				if(gift_cash <= 0){
					$(this).prop('disabled',false);
				    //alert('您已無可用購物金或是尚未登入會員!');
				    alert('您已無可用購物金!');
					$(this).prop('checked',false);
					//$.uniform.update('.use_credit');
					return false;
				}
				$.post('shop/cartupdate/', {dval: subtotal,dfield:'<?php echo base64_encode('gift_cash');?>',did:rowid}, function(data){
		          	if (!data.error){
		           		//$('.top-cart-info-count').text(data.total_items+' 件');
		           		//$('.top-cart-info-value').text('$ '+data.total);
		           		pagetotal.html('<span>$</span>'+data.subtotal);
						use_credit.attr('subtotal',data.subtotal);
						$('#gift_cash').val(data.gift_cash);
						$('#show_gift_cash').text(data.gift_cash);
			            if(addon_from_rowid){ //抓主商品
			            	rowid=addon_from_rowid;
			            }
						if($("#rowid_"+rowid).is(":checked")){ //有勾選
							checkoutupdate(true,rowid,supplier_sn);
						}
		          	}
				},'json');
            }else{
				$.post('shop/cartupdate/', {dval: 0,dfield:'<?php echo base64_encode('gift_cash');?>',did:rowid}, function(data){
		          	if (!data.error){
		           		//$('.top-cart-info-count').text(data.total_items+' 件');
		           		//$('.top-cart-info-value').text('$ '+data.total);
		           		//subtotal_value = parseInt(data.gift_cash)+subtotal;
		           		pagetotal.html('<span>$</span>'+data.subtotal);
						$('#show_gift_cash').text(data.gift_cash);
						$('#gift_cash').val(data.gift_cash);
						use_credit.attr('subtotal',data.subtotal);
			            if(addon_from_rowid){ //抓主商品
			            	rowid=addon_from_rowid;
			            }
						if($("#rowid_"+rowid).is(":checked")){ //有勾選
							checkoutupdate(true,rowid,supplier_sn);
						}
		          	}
				},'json');
            }

       		use_credit.prop('disabled',false);
        });
		$('.addfavorite').click(function(e) {
			var product_sn=$(this).attr('product_sn');
            var button = $(this);
            var rowid=button.attr('rowid');
            //console.log(rowid);
			var addon_from_rowid = button.attr('addon_from_rowid'); //主商品
			var supplier_sn=$(this).attr('supplier_sn');
			if(product_sn){
					$.get('member/AddwishList/'+product_sn+'/1',function(data){
            			$('.heart'+product_sn).fadeIn("slow").html(data);
						setTimeout(function(){$('.heart'+product_sn).hide();$('.heart'+product_sn).find("#text").text("");}, 2000);
			            if(!addon_from_rowid){ //如果是加購商品才抓主商品
			            	addon_from_rowid=rowid;
			            }
		            	//var r = confirm('是否確認刪除該購物車商品?（若有加購將一併刪除）');
		                //if(r)
		                //{
							 $.post('shop/cartdel/', {dtable: '<?php echo base64_encode('cart');?>',dfield:'<?php echo base64_encode('rowid');?>',did:rowid}, function(data){
		                    	if (!data.error){
							           		//$('.top-cart-info-count').text(data.total_items+' 件');
							           		//$('.top-cart-info-value').text('$ '+data.total);
							           		if(data.gift_cash){
												$('#show_gift_cash').text(data.gift_cash);
												$('#gift_cash').val(data.gift_cash);
							           		}
							           		//更新購物車popup
							           		//$('.top-cart-content').load('shop/top_cart');
											if($("#rowid_"+rowid).is(":checked")){ //有勾選
												checkoutupdate(false,rowid,supplier_sn);
											}
		                    		button.parents().parents().filter('tr').remove();
		                    		$('.addon_from_rowid'+button.attr('rowid')).remove();
		                    	}
							},'json');
		                //}
					});
			}
		});
				//結帳檢查
				$('#checkout').click(function(e) {
					var tmpcheck=true;
					//規格
		    		$('.product_spec').each(function(){
						if(!$(this).val()){
							$(this).focus();
							alert('請選擇規格');
							tmpcheck=false;
							return false;
						}
					});
					//選商品
					/*var ifrowid=false;
						$("input[name='rowid[]']:checked").each( function () {
							if($(this).val()){
								ifrowid=true;
							}
						});
					if(!ifrowid){
						alert('請至少選取一樣商品!');
						tmpcheck=false;
						return false;
					}
					var method =$("input[name='method_shipping']:checked").val();
				     if( typeof(method) == "undefined"){
							alert('請選擇配送方式!');
							tmpcheck=false;
					   	return false;
					 }*/
					 if(tmpcheck){
    			  		//$('#purple_product_spec').prop('disabled',false);
						//$('#shop').submit();
						location.href='shop/shopView2';
					 }
				});
		//全選
		$('.checkall').change(function(e) {
			  var set = $(this).attr("data-set");
	          var checked = $(this).is(":checked");
	          var value = $(this).val();
	          console.log(value);
	          $(set).each(function () {
	              //$(this).attr("checked", checked);
	                if (checked) {
	                	$('#ckbCheckAll-'+value).attr("checked",true);
	                    if(!$(this).attr("checked")){
	                  		$(this).click();
	                	}
	                } else {
	                	$('#ckbCheckAll-'+value).attr("checked",false);
	                    if($(this).attr("checked")){
	                  	   $(this).click();
	                    }
	                }
	          });
	          if(checked){
		        $('#delivery_method_'+$(this).val()).prop('required',true);
	          }else{
		        $('#delivery_method_'+$(this).val()).prop('required',false);
	          }
	          //$.uniform.update();
		});
		//已全選更新全選checkbox
		var blueallcheck=true;
        $('.fdcheck').each(function () {
            if (!$(this).is(":checked")) {
				blueallcheck=false;
            }
        });
		if(blueallcheck) $('#ckbCheckAll').attr("checked", true);

		$('.sphov').click(function(e) {
			//console.log($(this).attr('for'));
			check_id=$(this).attr('for');
	          var checked = $(this).is(":checked");
			//console.log(checked);
	          $('.sphov').each(function () {
	              //$(this).attr("checked", checked);
	              if (checked) {
	                  if(!$(this).attr("checked")){
	                  	$(this).click();
	                	}
	              } else {
	                  if($(this).attr("checked")){
	                  	$(this).click();
	                  }
	              }
	          });
			//console.log(check_id);
			//$.uniform.update(check_id);
			//$('#'+check_id).click();
			//$('#'+check_id).attr('checked',true);
			//console.log($('#'+check_id).attr('checked'));
		});
		//勾選商品
		$('.fdcheck').change(function(e) {
			//console.log($(this).val());
			//console.log($(this).is(":checked"));
			if($(this).is(":checked")){
				ifcheckout=true;
				$('#delivery_method_'+$(this).attr('supplier_sn')).prop('required',true);
			}else{
				ifcheckout=false;
				var fdcheck_one=false;
		        $('.fdcheck'+$(this).attr('supplier_sn')).each(function () {
		            if ($(this).is(":checked")) {
						fdcheck_one=true;
		            }
		        });
			console.log(fdcheck_one);
		        if(!fdcheck_one){
					$('#delivery_method_'+$(this).attr('supplier_sn')).prop('required',false);
		        }
			}
			checkoutupdate(ifcheckout,$(this).val(),$(this).attr('supplier_sn'));
           	//console.log($(this).attr('supplier_sn'));
           	if(ifcheckout){
				$('#delivery_method_'+$(this).attr('supplier_sn')).trigger('change');
			}
		});
		checkoutupdate = function(ifcheckout,rowid,supplier_sn){
			$.post('shop/checkoutupdate/', {dval: ifcheckout,dfield:'<?php echo base64_encode('ifcheckout');?>',did:rowid,supplier_sn:supplier_sn}, function(data){
          	if (!data.error){
				if(data.total_items){
	          		sub_total = (data.sub_total)? data.sub_total:'0';
	           		$('#buy_amount').text(data.total_items);
	           		$('#sub_total').html('<span>$</span>'+sub_total);
	           		$('#use_gift_cash_amount').text(data.gift_cash_items);
	           		$('#use_gift_cash').html('<span>$</span>'+data.gift_cash_total);
	           		$('#use_coupon').html('<span>$</span>'+data.coupon_total);
	           		$('#shipping_charge_amount').html('<span>$</span>'+data.shipping_charge_amount);
	           		$('#total_order_amount').html(data.total_order_amount);
	           		$('#supplier_subtotal_'+supplier_sn).text('小計：'+thousandComma(data.supplier_subtotal));
	           		var supplier_noship=$('#min_amount_for_noshipping_'+supplier_sn).val()-data.supplier_subtotal;
					var supplier_noship_result=(supplier_noship<=0)?'已達免運':'還差 '+thousandComma(supplier_noship)+' 元';
	           		$('#supplier_noship_'+supplier_sn).text(supplier_noship_result);
	           		if(rowid!='method_shipping'){
	           			$('#delivery_method_'+supplier_sn).trigger('change');
	           		}
	           		//console.log(thousandComma(data.supplier_subtotal));
	           		//更新配送方式
					//update_html('.table-shipping',data.delivery_method);
				}else{
	          		sub_total = '0';
	           		$('#buy_amount').text(sub_total);
	           		$('#sub_total').html('<span>$</span>'+sub_total);
	           		$('#use_gift_cash_amount').text(sub_total);
	           		$('#use_gift_cash').html('<span>$</span>'+sub_total);
	           		$('#use_coupon').html('<span>$</span>'+sub_total);
	           		$('#shipping_charge_amount').html('<span>$</span>'+sub_total);
	           		$('#total_order_amount').html(sub_total);
				}
			}else{
				$('#rowid_'+rowid).prop('checked',false);
				$('.checkall').prop('checked',false);
				//$.uniform.update('#rowid_'+rowid);
				//$.uniform.update('.checkall');
				if(data.error=='-1'){
	                $('#note_login').fadeIn("slow").html('請先登入會員！');
	                location.reload();
	                //$('#note_login').css('display:inline-flex;');
				}else{
					alert(data.error);
				}
          	}
			    },'json');
			  }
				function update_html(obj,odjdata){
					if(odjdata){
				    var html = '';
				    for (var x = 0; x < odjdata.length; x++) {
		    				var status=(odjdata[x].status=='1' )?'checked':'';
		    				if(odjdata.length==1 && odjdata[x].delivery_method_name=='線上開通'){ //應該是虛擬商品隱藏配送地區
		    					$('.ship_area').hide()
		    				}else{
		    					$('.ship_area').show()
		    				}
				        html+='<tr>';
				        html+='<td><label><input type="radio" name="method_shipping" class="shipping" delivery_amount="'+odjdata[x].delivery_amount+'" value="'+odjdata[x].delivery_method+'" '+status+'>'+odjdata[x].delivery_method_name+'</label></td>';
				        html+='<td>運費 '+odjdata[x].delivery_amount+' 元</td>';
				        html+='<td>'+odjdata[x].description+'</td>';
				        html+='</tr>';
				    }
				    $(obj).html(html);
			      //$.uniform.update(obj);
					$('.shipping').change(function(e) {
						checkoutupdate($(this).val(),'method_shipping');
					});
			    }
			  }
   			//$('.checkboxrowid').trigger("change");

				//$('.couponcode').click(function(e) {
				$('#coupon_form').submit(function(e) {
					var pagetotal = $('.tr_'+$('#rowid').val()).find('.goods-page-total');
					//var use_credit = $('.tr_'+$('#rowid').val()).find('.use_credit');
					var redsunglo = $('.tr_'+$('#rowid').val()).find('.red-sunglo');
					var supplier_sn=$('#supplier_sn').val();
					//console.log(supplier_sn);

				    $.post('shop/cartupdate/', {dval: $('#couponcode').val(),dfield:'<?php echo base64_encode('coupon_code');?>',did:$('#rowid').val(),supplier_sn:supplier_sn}, function(data){
				          	if (!data.error){
				           		//$('.top-cart-info-count').text(data.total_items+' 件');
				           		//$('.top-cart-info-value').text('$ '+data.total);
				           		pagetotal.html('<span>$</span>'+data.subtotal+'');
								//use_credit.attr('subtotal',data.subtotal);
								if(data.coupon_money > 0){
									redsunglo.text('已優惠：'+data.coupon_money);
									if($('.down-del-coupon').attr('rowid')==undefined){
										redsunglo.parent().after('<div class="del-coupon_'+$('#rowid').val()+'"><br>刪除優惠<a class="del-goods down-del-coupon" supplier_sn="'+supplier_sn+'" rowid="'+$('#rowid').val()+'" href="javascript:void(0);">&nbsp;</a></div>');
									}
								}
								$.fancybox.close();
								checkoutupdate(false,'couponcode',supplier_sn);
								$('#rowid').val('');
								$('#supplier_sn').val('');
				          	}else{
				          		if(data.error=='confirm_change_coupon'){
				            		var r = confirm('請確認是否更新優惠代碼？(原優惠代碼的折扣金額將無法享用)');
				                if(r){
				                	$('#coupon_form').submit();
				                }else{
									$.fancybox.close();
			    				  	return false;
									//console.log('x');
				                }
				              }else if(data.error=='reload'){
				              	location.reload();
				          		}else{
				                $('#note_couponcode').fadeIn("slow").html(data.error);
				              }
				          	}
				    },'json');
					e.preventDefault();
    		});
			$('.various').click(function(e) {
				//var rowid = $(this).parent().find('.use_credit').attr('rowid');
				var rowid = $(this).attr('rowid');
				var supplier_sn = $(this).attr('supplier_sn');
				var redsunglo = $('.tr_'+rowid).find('.red-sunglo');
				$('#rowid').val(rowid);
				$('#supplier_sn').val(supplier_sn);
    		});

			$('#couponcode').click(function() {
				$('#note_couponcode').fadeOut("slow");
    		});

    		<?php if(@$upgrade_discount_amount){ ?>
    			$('#purple_product_spec').prop('disabled',true);
    		<?php }?>
				ifallrowid_checked=true;
				$("input[name='rowid[]']").each( function () {
					//console.log($(this).val());
					if(!$(this).val()){
						ifallrowid_checked=false;
					}
				});
				if(ifallrowid_checked){
					//console.log(ifallrowid_checked);
				}
    		//$(".down-del-coupon").click(function(){
        $(document).on('click', '.down-del-coupon', function() {
            var button = $(this);
            var rowid=button.attr('rowid');
			var redsunglo = $('.tr_'+rowid).find('.red-sunglo');
			var subtotal = $('.tr_'+rowid).find('.goods-page-total');
			var supplier_sn=$(this).attr('supplier_sn');
            var r = confirm('是否確認刪除該優惠?');
                if(r){
					$.post('shop/cartupdate/', {dval: 'del_coupon_code',dfield:'<?php echo base64_encode('coupon_code');?>',did:rowid}, function(data){
                    	if (!data.error){
					           		//$('.top-cart-info-count').text(data.total_items+' 件');
					           		//$('.top-cart-info-value').text('$ '+data.total);
	           						subtotal.html('<span>$</span>'+data.subtotal);
					           		if(data.gift_cash){
										$('#show_gift_cash').text(data.gift_cash);
										$('#gift_cash').val(data.gift_cash);
					           		}
					           		//更新購物車popup
					           		//$('.top-cart-content').load('shop/top_cart');
												//if($(".tr_"+addon_from_rowid+' .checkboxrowid').is(":checked")){ //if有勾選
									checkoutupdate(true,rowid,supplier_sn);
												//}
									redsunglo.text('優惠券？');

                    		//button.parents().parents().filter('tr').remove();
                    		//$('.del-coupon_'+rowid).parent().find('.red-sunglo').remove();
                    		$('.del-coupon_'+rowid).remove();
                    	}else{
                    		alert(data.error);
                    	}
					},'json');
                }
        });
		$('.delivery_method').change(function() {
			var supplier_sn = $(this).attr('supplier_sn');
			var supplier_name = $(this).attr('supplier_name');
			var delivery_amount = parseInt($('option:selected', this).attr('delivery_amount'));
			var outland_amount = parseInt($('option:selected', this).attr('outland_amount'));
			var description = $('option:selected', this).attr('description');
			var delivery_name=$(this).children(':selected').text();
			if(delivery_name !='門市親取') description='';
			//console.log($('#ckbCheckAll'+supplier_sn).attr('checked'));
	   		var anyone_checked=false;
	   		$('.fdcheck'+supplier_sn).each(function(){
	   			if($(this).attr('checked')){
	   				anyone_checked=true;
				}
	    	});
	    	if(!anyone_checked && $(this).val()){
				alert('請至少先勾選該店任一商品再選配送方式！');
				$(this).val('');
				return false;
			}
 			if($('input[name="receiver_addr_state"]:checked').val()=='外島地區') delivery_amount=outland_amount;
			//console.log($(this).val());
			if($(this).val() > 0){
				$('#supplier_ship_'+supplier_sn).remove();
				$.post('shop/delivery_update/', {dval: supplier_sn,dfield:'<?php echo base64_encode('delivery_method');?>',did:$(this).val(),receiver_addr_state:$('input[name="receiver_addr_state"]:checked').val()}, function(data){
					//console.log(supplier_sn);
		          	if (!data.error){
			//console.log($(this).children(':selected').text());
						supplier_shipment='<div id="supplier_ship_'+supplier_sn+'">店家：'+supplier_name+'　'+delivery_name+' 運費：新台幣 '+data+' 圓　'+description+'</div>';
						$('#supplier_shipment').append(supplier_shipment);
		           		checkoutupdate(false,'method_shipping',supplier_sn);
		          	}else{
		          		alert(data.error);
		          	}
				},'json');
			}
		});
   		$('.delivery_method').each(function(){
   			if($(this).val()){
			//console.log($(this).val());
			//console.log($(this));
				//$('#delivery_method_'+$(this).attr('supplier_sn')).trigger('change');
				$(this).trigger('change');
			}
    	});
		$('.receiver_addr_state').change(function() {
			//console.log($(this).val());
	   		$('.delivery_method').each(function(){
	   			if($(this).val()){
					$(this).trigger('change');
				}
	    	});
		});
		//$('.qty').trigger('change');
    });
var thousandComma = function(number)
{
 var num = number.toString();
 var pattern = /(-?\d+)(\d{3})/;
 while(pattern.test(num))
 {
  num = num.replace(pattern, "$1,$2");
 }
 return num;
}
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->