<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容的 page level js
*/
?>

<script src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
<script src="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->
<script src='public/metronic/global/plugins/zoom/jquery.zoom.min.js' type="text/javascript"></script><!-- product zoom -->
<script src="public/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script><!-- Quantity -->
<script src="public/metronic/global/plugins/rateit/src/jquery.rateit.js" type="text/javascript"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script><!-- for slider-range -->


<script src="public/metronic/frontend/layout/scripts/layout.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Layout.init();
        Layout.initOWL();
        //Layout.initImageZoom();
        Layout.initTouchspin();
        Layout.initFixHeaderWithPreHeader();
        Layout.initNavScrolling();
        //Layout.initUniform();
        Layout.initSliderRange();

	    jQuery('.various').fancybox({
			maxWidth	: 800,
			maxHeight	: 600,
			fitToView	: false,
			width		: '70%',
			autoSize	: true,
			closeClick	: false,
			openEffect	: 'none',
			closeEffect	: 'none',
	    //beforeLoad: function() {
				//console.log($(this));
				//return false;
	    //},
	    beforeClose: function() {
				$('#note_couponcode').fadeOut("slow");
				$('#couponcode').val('');
	    }
		});

		$('#receiver_addr_state').change(function(e) {
			//console.log($(this).val());
			var money_field='';
			if($(this).val()){
				if($(this).val()=='台灣本島'){
					$('.delivery_amount').show();
					$('.outland_amount').hide();
					money_field='delivery_amount';
				}else if($(this).val()=='外島地區'){
					$('.delivery_amount').hide();
					$('.outland_amount').show();
					money_field='outland_amount';
				}
			}else{
				$('.delivery_amount').hide();
				$('.outland_amount').hide();
			}
			var method_shipping=$('input[name="method_shipping"]:checked').val();
			if(method_shipping && money_field){
				var delivery_money=$('input[name="method_shipping"]:checked').attr(money_field);
				checkoutupdate(method_shipping,'method_shipping',money_field);
				$('#delivery_money').text(delivery_money);
				//console.log(delivery_money);
			}
		});

		$(document).on('change', '.method_shipping', function() {
			var method_shipping=$('input[name="method_shipping"]:checked').attr('delivery_method_name');
			//console.log(method_shipping);
			if(!$('#receiver_addr_state').val()){
				alert('請先選擇配送地區');
				$('input[name="method_shipping"]:checked').attr('checked',false);
				return false;
			}
			var receiver_addr_state=$('#receiver_addr_state').val();
			if(receiver_addr_state=='台灣本島'){
				var delivery_money=$('input[name="method_shipping"]:checked').attr('delivery_amount');
				money_field='delivery_amount';
			}else if(receiver_addr_state=='外島地區'){
				var delivery_money=$('input[name="method_shipping"]:checked').attr('outland_amount');
				money_field='outland_amount';
			}
			//console.log($(this).val());
			if(method_shipping){
				$('#delivery').show();
				$('#delivery_method').text(method_shipping);
				$('#delivery_money').text(delivery_money);
				if(receiver_addr_state=='台灣本島'){
					$('.delivery_amount').show();
					$('.outland_amount').hide();
				}else if(receiver_addr_state=='外島地區'){
					$('.delivery_amount').hide();
					$('.outland_amount').show();
				}
			}else{
				$('#delivery').hide();
			}
			checkoutupdate($(this).val(),'method_shipping',money_field);
		});
		//$('.method_shipping').trigger('change');
		checkoutupdate = function(ifcheckout,rowid,money_field){
			$.post('shop/checkoutupdate/', {dval: ifcheckout,dfield:'<?php echo base64_encode('ifcheckout');?>',did:rowid,money_field:money_field}, function(data){
          	if (!data.error){
				if(data.total_items){
	          		sub_total = (data.sub_total)? data.sub_total:'0';
	           		$('#buy_amount').text(data.total_items);
	           		$('#sub_total').html('<span>$</span>'+sub_total);
	           		$('#use_gift_cash_amount').text(data.gift_cash_items);
	           		$('#use_gift_cash').html('<span>$</span>'+data.gift_cash_total);
	           		$('#use_coupon').html('<span>$</span>'+data.coupon_total);
	           		$('#shipping_charge_amount').html('<span>$</span>'+data.shipping_charge_amount);
	           		$('#total_order_amount').html(data.total_order_amount);
	           		//console.log(data.total_order_amount);
	           		//更新配送方式
					//update_html('.table-shipping',data.delivery_method);
				}else{
	          		sub_total = '0';
	           		$('#buy_amount').text(sub_total);
	           		$('#sub_total').html('<span>$</span>'+sub_total);
	           		$('#use_gift_cash_amount').text(sub_total);
	           		$('#use_gift_cash').html('<span>$</span>'+sub_total);
	           		$('#use_coupon').html('<span>$</span>'+sub_total);
	           		$('#shipping_charge_amount').html('<span>$</span>'+sub_total);
	           		$('#total_order_amount').html(sub_total);
	           		console.log(data.sub_total);
				}
			}else{
				$('#rowid_'+rowid).prop('checked',false);
				$('.checkall').prop('checked',false);
				//$.uniform.update('#rowid_'+rowid);
				//$.uniform.update('.checkall');
				if(data.error=='-1'){
	                $('#note_login').fadeIn("slow").html('請先登入會員！');
	                location.reload();
	                //$('#note_login').css('display:inline-flex;');
				}else{
					alert(data.error);
				}
          	}
			    },'json');
			  }
				function update_html(obj,odjdata){
					if(odjdata){
				    var html = '';
				    for (var x = 0; x < odjdata.length; x++) {
		    				var status=(odjdata[x].status=='1' )?'checked':'';
		    				if(odjdata.length==1 && odjdata[x].delivery_method_name=='線上使用'){ //應該是虛擬商品隱藏配送地區
		    					$('.ship_area').hide()
		    				}else{
		    					$('.ship_area').show()
		    				}
				        html+='<tr>';
				        html+='<td><label><input type="radio" name="method_shipping" class="shipping" delivery_amount="'+odjdata[x].delivery_amount+'" value="'+odjdata[x].delivery_method+'" '+status+'>'+odjdata[x].delivery_method_name+'</label></td>';
				        html+='<td>運費 '+odjdata[x].delivery_amount+' 元</td>';
				        html+='<td>'+odjdata[x].description+'</td>';
				        html+='</tr>';
				    }
				    $(obj).html(html);
			      //$.uniform.update(obj);
					$('.method_shipping').change(function(e) {
						checkoutupdate($(this).val(),'method_shipping','');
					});
			    }
			  }
		$('#receiver_addr_state').trigger('change');
    });
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->