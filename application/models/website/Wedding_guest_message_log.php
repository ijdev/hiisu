<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wedding_guest_message_log extends CI_Model {

    public $limit = 10; // query limit setting
    public $until = 0; // last query sn

    public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
    }
    /*
    * 留言選項列表
    * 全數拉出管理
    */
    public function get_result( $wedding_website_sn = 0 )
    {
        $this->db->select('*');
        $this->db->from('ij_wedding_guest_message_log');
        $this->db->where('wedding_website_sn', $wedding_website_sn);
        $this->db->where('status', 1);
        $this->db->order_by('create_date', 'desc');
        $query = $this->db->get();
        return $result = $query->result_array();
    }
    /*
    * 留言選項列表
    * 全數拉出管理
    */
    public function add_new($data_insert = array())
    {
        return $this->db->insert('ij_wedding_guest_message_log', $data_insert);
    }
}