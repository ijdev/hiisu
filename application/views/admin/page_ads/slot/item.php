<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>廣告版位設定 <small>版位設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/marketing/adsSlot">廣告版位</a><i class="fa fa-circle"></i>
      </li>
      <li class="active">
        廣告版位設定
      </li>
    </ul>
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">廣告版位設定</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/marketing/adsSlot" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
              <div class="form-body">
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">頻道</label>
                  <div class="col-lg-8">
                    <select name="Item[published_locaiotn_type]" class="form-control">
                    <?php if($published_locaiotn_types){foreach($published_locaiotn_types as $key=>$_Item){ ?>
                      <option value="<?php echo $_Item['published_locaiotn_type'];?>" <?php echo (@$Item['published_locaiotn_type']==$_Item['published_locaiotn_type'] || (!@$Item['published_locaiotn_type'] && $_Item['default_flag']=='1'))?'selected':'';?>><?php echo $_Item['published_locaiotn_type_name'];?></option>
                    <?php }}?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">版位名稱*</label>
                  <div class="col-lg-8">
                    <input name="Item[banner_location_name]" type="text" required class="form-control" value="<?php echo @$Item['banner_location_name'];?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">寬度</label>
                  <div class="col-lg-8">
                    <input name="Item[width]" type="number" class="form-control" value="<?php echo @$Item['width'];?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">高度</label>
                  <div class="col-lg-8">
                    <input name="Item[hight]" type="number" class="form-control" value="<?php echo @$Item['hight'];?>">
                  </div>
                </div>                
                <div class="form-group">
                  <label for="note" class="col-lg-3 control-label">敘述</label>
                  <div class="col-lg-8">
                    <textarea name="Item[banner_location_description]" class="form-control" rows="3"><?php echo @$Item['banner_location_description'];?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">廣告型態</label>
                  <div class="col-lg-8">
                    <select name="Item[banner_type]" class="form-control">
                    <?php if($banner_types){foreach($banner_types as $key=>$_Item){ ?>
                      <option banner_type_eng_name="<?php echo $_Item['banner_type_eng_name'];?>" value="<?php echo $_Item['banner_type'];?>" <?php echo (@$Item['banner_type']==$_Item['banner_type'] || (!@$Item['banner_type'] && $_Item['default_flag']=='1'))?'selected':'';?>><?php echo $_Item['banner_type_name'];?></option>
                    <?php }}?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">預設內容</label>
                  <div class="col-lg-8">
                    <div class="adType imgAD">
                      <input class="form-control" name="banner" type="file" id="banner">
	                    <?php if(@$Item['default_banner_content_save_dir'] && @$Item['banner_type_eng_name']=='img'){?>
		                    <a class="fancybox" rel="banner" title="<?php echo @$Item['blog_article_titile'];?>" href="public/uploads/<?php echo $Item['default_banner_content_save_dir'];?>"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['default_banner_content_save_dir'];?>" class="feature-img" style="vertical-align: bottom;max-width:100%;"></a>
		                    <a href="javascript:void(0);" class="deleteTemplateDivider" default_banner_content_save_dir="<?php echo $Item['default_banner_content_save_dir'];?>" ItemId="<?php echo @$Item['banner_location_sn']; ?>"><i class="fa fa-trash-o"></i>刪除</a><br><br>
	                  	<?php }?>
                    </div>
                    <div class="adType textAD">
                      <input type="text" name="textAD" class="form-control" placeholder="廣告顯示文字" value='<?=(@$Item['banner_type_eng_name']=='text')? @$Item['default_banner_content_save_dir']:'';?>' />
                    </div>
                    <div class="adType htmlAD">
                      <textarea name="htmlAD" class="form-control" rows="3" placeholder="請貼入 HTML 代碼"><?php echo (@$Item['banner_type_eng_name']=='html')? @$Item['default_banner_content_save_dir']:'';?></textarea>
                    </div>
                  </div>
                </div>
                <!--div class="form-group">
                  <label for="active" class="col-lg-3 control-label">連結類型</label>
                  <div class="col-lg-8">
                    <select name="Item[banner_type]" class="form-control">
                    <?php if($link_types){foreach($link_types as $key=>$_Item){ ?>
                      <option value="<?php echo $_Item['banner_link_type'];?>" <?php echo (@$Item['banner_link_type']==$_Item['banner_link_type'] || (!@$Item['banner_link_type'] && $_Item['default_flag']=='1'))?'selected':'';?>><?php echo $_Item['banner_link_type_name'];?></option>
                    <?php }}?>
                    </select>
                  </div>
                </div-->                
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">預設連結</label>
                  <div class="col-lg-8">
                    <input type="url" name="Item[default_banner_link]" class="form-control" placeholder="必須為 http:// 或 https:// 開頭的有效連結" value="<?php echo @$Item['default_banner_link'];?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-lg-3 control-label">更新者</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['update_member_sn'])?@$Item['update_member_sn']:@$Item['create_member_sn']; ?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-3 control-label">更新時間</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['last_time_update']!='0000-00-00 00:00:00')?@$Item['last_time_update']:@$Item['create_date']; ?>" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <input type="hidden" name="banner_location_sn" value="<?php echo @$Item['banner_location_sn']; ?>">
                    <input type="hidden" name="default_banner_content_save_dir" value='<?php echo @$Item['default_banner_content_save_dir']; ?>'>
                    <input type="hidden" id="banner_type_eng_name" name="banner_type_eng_name" value='<?=@$Item['banner_type_eng_name'];?>'>
                    <button type="submit" class="btn green">儲存</button>
                    <button type="button" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->