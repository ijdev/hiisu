<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script>
jQuery(document).ready(function() {       
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	$(".select2_dealer").select2({
	    tags: ["典華1", "典華2", "典華3", "典華4", "典華5"]
	});
 $('.date-picker').datepicker({
        rtl: Metronic.isRTL(),
        format: "yyyy-mm-dd",
        orientation: "left",
        autoclose: true
    });
    
    
    
    /* Action On Select Box Change */
    $('select[id="addr_city_code"]').change(function() {
 
        var data = $(this).val(); // Get Selected Value

    $.ajax({
			  url: 'Member/get_town_zip',
        data: 'data=' + data,
        dataType: 'json',
        success: function(data) {
         		 $('#addr_town_code').empty();
						 $(data).appendTo("#addr_town_code");
            
        }
    });
      });
    
    
    
});


</script>

<script>
  
  // When the browser is ready...
  $(function() {
  				
  				jQuery.validator.addMethod("mobileTaiwan", function( value, element ) {
	var str = value;
	var result = false;
	if(str.length > 0){
		//是否只有數字;
		var patt_mobile = /^[\d]{1,}$/;
		result = patt_mobile.test(str);
 
		if(result){
			//檢查前兩個字是否為 09
			//檢查前四個字是否為 8869
			var firstTwo = str.substr(0,2);
			var firstFour = str.substr(0,4);
			var afterFour = str.substr(4,str.length-1);
			if(firstFour == '8869'){
				$(element).val('09'+afterFour);
				if(afterFour.length == 8){
					result = true;
				} else {
					result = false;
				}
			} else if(firstTwo == '09'){
				if(str.length == 10){
					result = true;
				} else {
					result = false;
				}
			} else {
				result = false;
			}
		}
	} else {
		result = true;
	}
	return result;
}, "手機號碼不符合格式，僅允許09開頭的10碼數字");

    // Setup form validation on the #register-form element
    $("#register-form").validate({
    
        // Specify the validation rules
        rules: {
            last_name: "required",
            lastname: "required",
            
            cell:{ 
						required:true, 
						mobileTaiwan:true 
						},
            user_name: {
                required: true,
                email: true
            },
             email: {
                required: true,
                email: true
            },
            password: {
                required: false,
                minlength: 6
            },
             confirm_password: {
                required: false,
                equalTo: "#password",
                minlength: 6
            },
            
            member_agree: "required"
        },
        
        // Specify the validation error messages
        messages: {
            wedding_date: "請輸入結婚日期！",
            birthday: "請輸入生日 ！",
            last_name: "請輸入姓！",
            first_name: "請輸入名！",
            cell:{ 
						required: "請輸入您的聯繫手機", 
						mobileTaiwan: "請輸入一個有效的聯繫電話" 
						},
            password: {
                required: "請輸入密碼",
                minlength: "密碼最小長度為6"
            },
            confirm_password: {
                required: "請輸入密碼",
               
                minlength: "密碼最小長度為6"
            },
            user_name: "請輸入正確的email格式！",
            email: "請輸入正確的email格式！",
            member_agree: "請確認會員條款已閱讀和打勾"
        },
         
        submitHandler: function(form) {
            form.submit();
        }
        
	
    });

  });
  
  </script>