<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品分類 <small>分類設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/admin">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品分類
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/product/productAttribute">分類屬性</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/product/productAttribute/<?php echo @$Attribute['attribute_sn']?>">分類屬性名稱 <?php echo @$Attribute['attribute_name']?></a>
        <i class="fa fa-circle"></i>
      <li class="active">
         分類屬性值設定
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">分類屬性值設定</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/product/productAttributeValue" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">分類屬性值 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input type="text" name="attribute_value[attribute_value_name]" class="form-control" value="<?php echo @$Item['attribute_value_name'];?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="seq" class="col-lg-3 control-label">排序</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" name="attribute_value[sort_order]" value="<?php echo (@$Item['sort_order']) ? @$Item['sort_order']:'9999'; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">是否啟用 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <select class="form-control" name="attribute_value[status]">
                      <option value="1" <?php if(@$Item['status'] || !@$Item['status']){ ?>selected<?php }?>>是</option>
                      <option value="0" <?php if(@$Item['status']===0){ ?>selected<?php }?>>否</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-lg-3 control-label">更新者</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="<?php echo (@$Item['update_member_sn'])?@$Item['update_member_sn']:@$Item['create_member_sn']; ?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-3 control-label">更新時間</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="<?php echo (@$Item['last_time_update']!='0000-00-00 00:00:00')?@$Item['last_time_update']:@$Item['create_date']; ?>" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <input type="hidden" name="attribute_value_sn" value="<?php echo @$Item['attribute_value_sn']; ?>">
                    <input type="hidden" name="attribute_value[attribute_sn]" value="<?php echo (@$Item['attribute_sn'])?@$Item['attribute_sn']:@$Attribute['attribute_sn']; ?>">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="reset" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->