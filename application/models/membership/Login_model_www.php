<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model_www extends CI_Model {

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('sean_models/Sean_general');
		$this->load->library('user_agent');
    }

    /**
     *
     * validate_login: check login data against database information
     *
     * @param string $username the username to be validated
     * @param string $password the password to be validated
     * @return mixed
     *
     */

    public function validate_login($username, $password) {

		//會員參數
		$_c_member_config="member_config";
		$_c_member_data="member_data";
		//會員資料庫參數 抓config/member_config.php 裡的member_data 陣列
		$member_item=$this->Sean_general->general_data($_c_member_config,$_c_member_data);
		// 指定會員資料庫參數 至session
		$this->session->set_userdata('member_item',$member_item);

		$_list_item="";
		foreach ($member_item as $k => $i)
		{
				if(strlen($_list_item) > 0 )
				$_list_item.=",";

				$_list_item.=$k;
		}

		$_list_item.=",apply_date,apply_memo,approval_status,approval_date,approval_memo ";

        $this->db->select($_list_item);
        $this->db->from('ij_member');
        //$this->db->join('ij_member_role_relation, ij_member_role_relation.member_sn = ij_member.member_sn, left');
        $this->db->where('user_name', $username);
		//$this->db->where('member_level_type', 1);  不用指定1吧？ wdj
		//$this->db->where('system_user_flag', 0); 不用指定0吧？ wdj
		//$this->db->where('member_status', 2);
        $this->db->limit(1);

        $query = $this->db->get();
		//echo $this->db->last_query();
		   //exit();
				if ( ! $query )
				{
						$error = $this->db->error(); // Has keys 'code' and 'message'
						$this->session->set_flashdata("success_message",$error);
						return false;
						exit();
				}


       if($query->num_rows() == 1){
           $row = $query->row();
           $array=array();
		  			if (md5($password) == $row->password ){
		  				if ($row->member_status==2 ){ //驗證通過
							   //抓去會員的資訊
							   foreach ($member_item as $k => $i){
									$array[$k]=$row->$k;
								}
								$array["member_sn"]=$row->member_sn;
								//$array["apply_date"]=$row->apply_date;
								//$array["apply_memo"]=$row->apply_memo;
								//$array["approval_status"]=$row->approval_status;
								//$array["approval_date"]=$row->approval_date;
								//$array["approval_memo"]=$row->approval_memo;
								//$array["wedding_date"]=$row->wedding_date;
								//	$_list_item.=",apply_date,apply_memo,approval_status,approval_date,approval_memo";

								if($row->member_level_type=='2'){ //聯盟會員
                                    $this->db->select("partner_sn,ij_partner.partner_level_sn,bonus");
    								$this->db->from('ij_partner');
                                    $this->db->join("ij_partner_level_config","ij_partner_level_config.partner_level_sn = ij_partner.partner_level_sn","left");
            				        $this->db->where('member_sn',$array["member_sn"]);
            				        $this->db->limit(1);
            		        		$query = $this->db->get();
            		        		if($query->num_rows() == 1)
            		        		{
            		        			$row = $query->row();
                                        $array["partner_sn"]=$row->partner_sn;
                                        $array["partner_level_sn"]=$row->partner_level_sn;
                                        $array["bonus"]=$row->bonus;
            		        		}
                                }else{
                                    $array["partner_sn"]=0;
                                    $array["partner_level_sn"]=0;
                                    $array["bonus"]=0;
                                }



								 //清空錯誤次數
						 		 //$this->reset_login_attempts($username);
								 //紀錄最後更新時間
								// $this->_update_last_login($username);
								 ///紀錄登入成功資料
								// $this->_member_log($array["mid"],$username,1);
								//回傳會員資訊
								return $array;
							}else{
								return $row->member_status;
							}

					}else{
						//紀錄登入失敗資料
						// $this->_member_log("0",$username,2);
						 //增加錯誤次數紀錄
					   //  $this->_increase_login_attempts($username);
						return '-2';

					}
			//紀錄登入失敗資料
			// $this->_member_log("0",$username,2);
			  //增加錯誤次數紀錄
		//	$this->_increase_login_attempts($username);
    }else{
        return '-1';
    }
  }

    /**
     *
     * _update_last_login: update the last time the member logged in
     *
     * @param string $username the username to be validated
     * @return boolean
     *
     */

    private function _update_last_login($username) {
        $this->db->set('last_login', 'NOW()', FALSE);
        $this->db->where('username', $username);
        $this->db->update('member');

        if ($this->db->affected_rows() == 1) {
            return true;
        }
        return false;
    }

    /**
     *
     * _increase_login_attempts: add +1 to login attempts for member
     *
     * @param string $username the username to be validated
     * @return boolean
     *
     */

    private function _increase_login_attempts($username) {
        $this->db->set('login_attempts', 'login_attempts + 1', FALSE);
        $this->db->where('username', $username);
        $this->db->update('member');

        if ($this->db->affected_rows() == 1) {
            return true;
        }
        return false;
    }

    /**
     *
     * reset_login_attempts: bring login attempts back to 0
     *
     * @param string $username the username to be validated
     * @return boolean
     *
     */

    public function reset_login_attempts($username) {
        $this->db->where('username', $username);
        $this->db->update('member', array('login_attempts' => 0));
    }

	public function _member_log($mid,$username,$type) {
//"type" => "type",//1 login true , 2 login false , 3 logout
        $data = array(
            'browser' => $this->agent->browser(),
            'b_version' => $this->agent->version(),
            'mobile' => $this->agent->mobile(),
            'robot' => $this->agent->robot(),
            'platform' => $this->agent->platform(),
            'referrer' => $this->agent->referrer(),
            'agent_string' => $this->agent->agent_string(),
            'accept_lang' => $this->agent->accept_lang(),
						'accept_charset' => $this->agent->accept_charset(),
						'connect_ip' => $this->input->ip_address()
        );

        $this->db->set('login_time', 'NOW()', FALSE);
				$this->db->set('mid', $mid, FALSE);
				$this->db->set('username', "'$username'", FALSE);
				$this->db->set('type', $type, FALSE);
        $this->db->insert('member_log', $data);

        if ($this->db->affected_rows() == 1) {
            return $nonce;
        }
        return false;
    }

}

/* End of file login_model.php */
/* Location: ./application/models/membership/login_model.php */