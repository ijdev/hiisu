<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_general_co extends CI_Model {

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('sean_models/Sean_db_tools');
	    $this->_key_id="";//table key value
		$this->_action="";//contro action

    }
    

	public function action_edit( $_where = "") {
		
		$_result="";
		
		$_result=$this->Sean_db_tools->db_get_max_record("*",$this->_table,$_where);
		
		return $_result;
		
	}
	
	public function action_insert() {
		
			$_result="";
			
			foreach($this->page_data["_table_field"] as $key => $value)
			{
					if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
					$_data[$key]=$this->input->get_post($key);
			}
			
			
			
		$_result=$this->Sean_db_tools->db_insert_record($_data,$this->page_data["_table_field"]);
		
		return $_result;
		
	}
	
	public function action_update($_where,$_key_id="") {
		
			$_result="";
					
			foreach($this->_table_field as $key => $value)
			{
					if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
					$_data[$key]=$this->input->get_post($key);
					
			}
			
			
			
			
		$_result=$this->Sean_db_tools->db_update_record($_data,$this->_table,$_where,$_key_id,$this->_id);
		
		return $_result;
		
	}
	
	public function action_delete( $_where) {
		
		$_result="";
		
		$_result=$this->Sean_db_tools->db_delete_record($this->_table,$_where,$_key_id,$this->_id);
		
		return $_result;
		
	}
	
	//取得新排序的值
	public function get_next_sort($_id=0 ) {
		
		$_result="0";
		
		$_where=" where ".$this->_id." > 0 order by sort_order desc ";
		$_result=$this->Sean_db_tools->db_get_one_record("sort_order",$this->_table,$_where);
		
		if(is_array($_result["sort_order"]) )
		$_result=1;
		else
		$_result=$_result["sort_order"]+1;
		
		return $_result;
		
	}
	
	//取得定義參數值
	public function get_define_value($_id) 
	{
		if(isset($this->$_id))
		return $this->$_id;
		else
		return "";
	}
	
	//取得定義參數值
	public function set_define_value($_key,$_value) 
	{
		if(strlen($_key) > 0 )
		{
			$this->$_key = $_value;
			return true;
		}else{
			return false;
		}
	}
	
	
}
