<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 會員中心 ticket 列表
* 按下狀態與回應可以看到與客服人員的通訊記錄 (modal ajax)
* 若狀態為以解決，ajax (ex.preview/callcenterMsg/done) 取回的頁面資料中不會有 modal footer (無送出表單)
*/
?>


<div class="container mob_padding">
<style>
  .highlight a.ticket_process{
  }
</style>
<!-- Page level plugin styles START -->
<!-- Page level plugin styles END -->
<div class="container mob_padding">
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="member">會員中心</a></li>
    <li class="active">客服中心</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar col-md-2 col-sm-3">
   <div class="desktop_view_sidebar">
		<ul class="list-group margin-bottom-25 sidebar-menu">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
	<div class="mobile_view_sidebar">
		<div class="side-nav-head border_title_bottom">
			<button class="fa fa-bars collapsed" data-toggle="collapse" data-target="#mob_sidebar_drop_down"></button>
			<h4 class="bfont">會員中心<!--會員中心--></h4>
		</div>
		<ul class="list-group margin-bottom-25 sidebar-menu collapse" id="mob_sidebar_drop_down">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
  </div>
  <!-- END SIDEBAR -->
  <!-- copy content -->
<div id="kefu_list_main" class="col-md-9 col-sm-9 margin-bottom-30 product_right border_left minheight">
      <?php $this->load->view('www/general/flash_error');?>
									<ul class="nav nav-tabs nav-button-tabs margin-bottom-20">
       									<li class="active top_btns"><a href="member/callcenter">會員問題</a></li>
     									<li class="top_btns"><a href="/member/kefu_list">問題紀錄</a></li>
      						</ul>
								<div>
        <form method="post" name="ticket_form" class="form-horizontal" role="form" action="member/addCallcenter">
        <input type="hidden" name="type" value="">
        <div class="margin-top-20 margin-bottom-20">
          <?/*
           <div class="form-group">
		    <label class="col-md-2 col-sm-2 control-label">訂單編號</label>
		   <div class="col-md-3 col-sm-3">
          <input type="text" name="associated_order_sn" id="associated_order_sn" class="form-control" placeholder="若您欲詢問的問題與訂單有關，請您儘可能提供">
           </div>
		   <label class="col-md-2 col-sm-2 control-label">或商品編號</label>
		   <div class="col-md-3 col-sm-3">
			<input size="27" name="product_sn" id="product_sn" class="form-control" value="" placeholder="可不填">
           </div>
         </div>*/?>

  	  <table class="table noborder">
          <tbody><tr>
            <td>分類</td>
            <td>
            <table>
              <tbody>
                  <?php foreach($ccapply as $_Item){?>
                <tr>
                  <td style="padding-right:20px;"><b><?php echo $_Item['ccapply_name']?></b></td>
                    <?php foreach($_Item['detail'] as $_Item2){?>
                    <td><input type="radio" required name="ccapply_detail_code" id="ccapply_detail_code" value="<?php echo $_Item2['ccapply_detail_code']?>"><?php echo $_Item2['ccapply_detail_code_name']?> &nbsp;&nbsp;&nbsp;</td>
                    <?php }?>
                </tr>
                  <?php }?>
            </tbody></table>
            </td>
          </tr>
          <tr>
            <td>主題
              <!--簡單標題--></td>
            <td><input size="27" required name="question_subject" class="form-control"></td>
          </tr>
          <tr>
            <td>帳號
              <!--帳號--></td>
            <td><input disabled="disabled" size="27" name="demo_username" id="demo_username" class="form-control" value="<?=$this->session->userdata['web_member_data']['user_name']?>"></td>
          </tr>
          <tr>
            <td>姓名
              <!--姓名--></td>
            <td><input size="27" class="form-control" name="first_name" id="first_name" value="<?=$this->session->userdata['web_member_data']['first_name']?>"></td>
          </tr>
          <tr>
            <td>電子信箱
              <!--電子郵件地址--></td>
            <td><input size="27" name="email" id="email" class="form-control" value="<?=$this->session->userdata['web_member_data']['email']?>"></td>
          </tr>
          <tr>
            <td>留言內容
              <!--留言內容--></td>
            <td><textarea name="question_description" id="question_description" required rows="8" cols="65" class="form-control"></textarea>
              <br>
              <input type="submit" value="確定" class="col-xs-12 col-sm-3 btn btn-black">
              <input name="Action" id="Action" type="hidden" value="Insert">
              </td>
          </tr>
          <input type="hidden" size="27" name="username" class="form-control" value="">
      </tbody></table>

<!-- /會員留言結束 -->
							</div></form>
						</div>
						<!--右邊結束-->
					</div>
<!-- copy content -->
</div>



</div> <!--container -->
</div>
<!-- END SIDEBAR & CONTENT -->
<style>
.display_none{
	display:none;
}
.nav-tabs.nav-button-tabs>li.active>a {
    color: #fff !important;
    background-color: rgba(0,0,0,0.6);
	padding: 10px 15px;
	cursor:pointer;
}
.nav-tabs.nav-button-tabs>li>a {
    color: #fff !important;
    padding: 10px 15px;
}
.nav-tabs.nav-button-tabs>li>a {
    color: #000 !important;
    border: 0 !important;
	cursor:pointer;
    /* background-color: rgba(0,0,0,0.1); */
    margin-right: 3px !important;
    -webkit-border-radius: 3px !important;
    -moz-border-radius: 3px !important;
    border-radius: 3px !important;
}
.nav-tabs {
    border: none;
}

.border_left {
    border-left: 1px solid rgba(100,100,100,0.1) !important;
}
</style>
