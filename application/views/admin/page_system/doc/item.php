<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/typeahead/typeahead.css">
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/pages/css/portfolio.css" rel="stylesheet" type="text/css"/>


<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>系統設定 <small>系統文件內容管理</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        系統設定
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/admin/doc">系統文件</a>
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         系統文件內容管理
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">文件名稱文件名稱 >> 編輯</span>
            </div>
            <div class="tools">
              <a href="admin/admin/doc"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">文件名稱</label>
                  <div class="col-lg-10">
                  	<?php if(@$Item['system_file_name']){?>
	                    <p class="form-control-static"><?php echo @$Item['system_file_name']; ?></p>
                  	<?php }else{?>
	                    <input type="text" name="system_file_name" class="form-control" placeholder="輸入後您將無法自行更改">
                  	<?php }?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-2 control-label">內容</label>
                  <div class="col-lg-10">
                    <textarea id="editor1" class="ckeditor form-control" name="system_file_ontent" rows="12">
                    	<?php echo @$Item['system_file_ontent']; ?>
  									</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-md-2 control-label">更新者</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['update_member_sn'])?@$Item['update_member_sn']:@$Item['create_member_sn']; ?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-md-2 control-label">更新時間</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['last_time_update']!='0000-00-00 00:00:00')?@$Item['last_time_update']:@$Item['create_date']; ?>" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-2 col-md-10">
                    <input type="hidden" name="system_file_sn" value="<?php echo @$Item['system_file_sn']; ?>">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="button" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->