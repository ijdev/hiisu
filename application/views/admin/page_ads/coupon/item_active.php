<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<!-- END PAGE LEVEL STYLES -->



<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>行銷管理 <small>實體折扣卷申請登錄</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="preview">首頁</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        優惠活動
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="preview/couponApply">實體折扣卷</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        登錄
      </li>
    </ul>
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN ITEM TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">實體折扣卷 > 活動名稱 > 兌換券登錄</span>
            </div>
            <div class="actions btn-set">
              <input type="text" class="pagination-panel-input form-control input-inline input-xm" size="30" style="margin: 0 5px;" placeholder="使用 barcode scanner 掃 code 登錄">
              <button class="btn btn-sm green table-group-action-submit"><i class="fa fa-check"></i> 登錄實體折扣卷</button>
              <a href="preview/certificateApply" class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-list"></i> 回上層</a>
            </div>
          </div>
          <div class="portlet-body form">
            <div class="alert alert-info">
              <strong>活動名稱</strong>: 活動名稱一<br>
              <strong>使用時間</strong>: 2015-05-01 ~ 2016-05-01<br>
              <strong>兌換商品</strong>: 商品一、商品二、商品三<br>
              <strong>折扣</strong>: 100 元<br>
            </div>
            <div class="note note-warning">
              <div class="table-actions-wrapper pull-right">
                <select class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size: 11px;">
                  <option>是否使用</option>
                  <option>已使用</option>
                  <option>未使用</option>
                </select>
                <select class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size: 11px;">
                  <option>是否失效</option>
                  <option>已失效</option>
                  <option>未失效</option>
                </select>
                <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="30" style="margin: 0 5px;" placeholder="使用 barcode scanner 掃 code 登錄">
                <button class="btn btn-sm yellow table-group-action-submit">搜尋</button>
              </div>
              <div style="clear:both;"></div>
            </div>
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>編號</th>
                  <th>兌換券狀態</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=0;$i<20;$i++):?>
                  <tr class="odd gradeX">
                    <td>
                      xewfrhowhfoih
                    </td>
                    <td class="center">
                      <span class="badge badge-warning badge-roundless"> 未審核 </span>
                    </td>
                    <td class="center">
                      <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>棄用</a>
                    </td>
                  </tr>
                <?php endfor;?>
              </tbody>
            </table>
            <div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                  <ul class="pagination pull-right" style="visibility: visible;">
                    <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                    <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li class="active"><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                    <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END SETTING TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
