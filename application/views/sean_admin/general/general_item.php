<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/

require_once "lang.php";



?>

<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <?php echo $_page_title;?>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase"><?php echo $_caption_subject;?></span>
            </div>
            <div class="actions btn-set">
              <a href="<?php echo $init_control;?><?php echo $_father_link;?>" class="btn green"><i class="fa fa-list"></i> <?php echo $this->lang->line("back_list");?></a>


            </div>
          </div>
          <div class="portlet-body form">
          	<?php if($_father_link=='Sysconfig/Mail/') { ?>
            <div class="note note-danger col-lg-10 pull-right">
              <h4 class="block">變數內容定義由工程師填寫</h4>
              <p>
                說明:在郵件模版中,設定了郵件內容變數(形如@XXXX@),在發出郵件時,系統會用實際值替換本變數,本範本的變數定義如下:  <br>
                @shopname@ HiiSU<br>
                @username@ 帳號<br>
                @truename@ 會員全名<br>
                @newpass@ 新密碼<br>
                @site_url@ 網址<br>
                @register_url@ 註冊驗證網址<br>
                @message@ 訊息內容<br>
              </p>
            </div>
          <?php } ?>
          <?php $this->load->view('sean_admin/general/flash_error');?>
                 <form <?php
                 	if(isset($_action_link))
                 	echo " action=\"".$_action_link."\" ";
                 	?> class="form-horizontal error" novalidate="">



									<?php

										if(isset($em_columns) && is_array($em_columns))
										{
										 	$_form_one= new sean_form_general();

										 	if(isset($_result01) && is_array($_result01))
										 	{
										 		$_form_one->add_item($em_columns,$_result01);

											}else{
												$_form_one->add_item($em_columns);
											}
										}

									?>


                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                          <button type="submit" class="btn green">儲存</button>
                          <a href="<?php echo $init_control;?><?php echo $_father_link;?>" class="btn green"><i class="fa fa-list"></i> <?php echo $this->lang->line("cencel_list");?></a>

                        </div>
                      </div>
                    </div>
                  </form>




            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
