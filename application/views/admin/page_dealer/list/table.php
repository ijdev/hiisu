<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>會員管理 <small>經銷會員列表</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        會員管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        經銷會員
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">經銷會員列表</span>
            </div>
            <div class="actions btn-set">
              <a href="<?php echo $_action_link;?>" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <div class="portlet-body">
          	<?php $this->load->view('sean_admin/general/flash_error');?>
            <div class="note note-warning">
              <form action >
              <div class="table-actions-wrapper pull-right">
                <span>
                </span>
                <!--input type="text" class="pagination-panel-input form-control input-inline input-sm" size="8" style="margin: 0 5px;" placeholder="關鍵字"-->
                <select class="table-group-action-input form-control input-inline" id="biz_domain" style="font-size:11px;">
                  <option value="">產業屬性</option>
                 <?php
                 foreach($_biz_domain_list as $key => $value)
                 {
                 		echo '<option value="'.$value->domain_name.'">'.$value->domain_name.'</option>';
                 }
                 ?>


                </select>
                <!--button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button-->
              </div>


              <div style="clear:both;"></div>
            </div>
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                	<th>簡稱</th>
                  <th>名稱</th>
                  <th>子域名</th>
                  <th>產業屬性</th>
                  <th>縣市</th>
                  <th>鄉鎮市區</th>
                  <th>電話</th>
                  <th>是否啟用</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
              	<?php
                $_member_status_ct=$this->config->item("member_status_ct");
								$_member_level_type_ct=$this->config->item("member_level_type_ct");
								//$_city_ct=$this->config->item("city_ct");

                if(is_array($_result01))
                	{
                 			foreach($_result01 as $key => $value)
                    		{
                    			if($value->dealer_status == 1)
                    				{
                    					$_active = '<i class="fa fa-check"></i>';
                    				}
                    			else
                    				{
                    					$_active = '<i class="fa fa-times"></i>';
                    				}
												  //$_town_ct_init="town_ct_".$value->addr_city_code;
												  //$_town_ct=$this->config->item($_town_ct_init);
          //$_where=" where associated_member_sn='".$value->dealer_sn."'  ";
					//$_member_domain_relation=$this->Sean_db_tools->db_get_max_record("ij_member.email","ij_member_domain_relation left join ij_member_domain_relation.member_sn=ij_member.member_sn",$_where);




                  ?>
                  <tr class="odd gradeX">
                  <!--<td><?php echo $value->dealer_sn;?></td>-->
                    <td><?php echo $value->dealer_short_name;?></td>
                    <td><?php echo $value->dealer_name;?></td>
                    <td>
                    	<?php if($value->customize_website_url){?>
                    		<a href="http://<?php echo $value->customize_website_url.'.qwedding.club';?>" target="_blank"><?php echo $value->customize_website_url;?>&nbsp;<i class="fa fa-external-link"></i></a>
                    	<?php }else{ echo '尚未設定';}?>
                    </td>
                    <td><?php echo $value->domaintype;?></td>
                    <td width="8%"><?php echo $value->city_name;?></td>
                    <td width="8%"><?php echo $value->town_name;?></td>
                    <td><?php echo $value->office_tel;?></td>
                    <td class="center"><?php echo $_active;?></td>
                    <td class="center">
                      <a href="<?php echo $init_control;?>Member/dealerTable/edit/<?php echo $value->dealer_sn;?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                      <a href="<?php echo $init_control;?>Member/dealerPage/<?php echo $value->dealer_sn;?>"><i class="fa fa-mortar-board"></i>專頁管理</a> &nbsp;&nbsp;
                      <a href="<?php echo $init_control;?>Member/dealerTable/delete/<?php echo $value->dealer_sn;?>"><i class="fa fa-trash-o"></i>停用</a>
                    </td>
                  </tr>
              <?php } } ?>
              </tbody>
            </table>
            <!-- page nav -->
            <?php
						//echo $_pagination;
						?>
            <!-- end of page nav -->
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->