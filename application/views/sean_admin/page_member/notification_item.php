<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="public/metronic/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/pages/css/inbox.css" rel="stylesheet" type="text/css"/>

<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>全站通知</h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="preview">總覽</a><i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="preview/notification">全站通知</a><i class="fa fa-circle"></i>
      </li>
      <li class="active">
        訊息內容
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-bell-o font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">訊息內容</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/main/notification" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body">
            <div class="inbox-content">
              <div class="inbox-header inbox-view-header">
                <h1>通知標題通知標題</h1>
              </div>
              <div class="inbox-view-info">
                <div class="row">
                  <div class="col-md-7">
                    <span class="bold"> 系統通知 </span>
                    on 08:20PM 29 JAN 2013
                  </div>
                  <div class="col-md-5 inbox-info-btn">
                    <div class="btn-group">
                      <button data-messageid="23" class="btn blue reply-btn"><i class="fa fa-trash-o"></i> Delete </button>
                    </div>
                    </div>
                  </div>
                </div>
                <div class="inbox-view">
                  <p>
                    <strong>Lorem ipsum</strong>dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                  </p>
                  <p>
                     Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et <a href="javascript:;">
                    iusto odio dignissim </a>
                    qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem.
                  </p>
                  <p>
                     Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                  </p>
                  <p>
                     Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.
                  </p>
                </div>
              </div>
            </div>
            <!-- /.inbox-content -->
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->