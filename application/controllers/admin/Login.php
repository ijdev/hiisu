<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends MY_Controller {
 	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->library('sean_lib/Sean_form_general');
		$this->load->model('general/My_general_ct');
		$this->load->config('ij_config');
		$this->load->config('ij_sys_config');
		$this->load->model('sean_models/Sean_general');
		$this->load->model('sean_models/Sean_db_tools');
		$this->load->model('libraries_model');
		$this->load->helper('cookie');
		$this->page_data['init_control']="admin/login/";
	}
	public function index(){
		$this->page_data['login_type']=get_cookie('login_type');
		$this->page_data['_captcha_img']=$this->captcha_img();
		$this->page_data['_action_link']="admin/main/managerSignAction";
		$this->load->view('admin/page_manager/sign_in', $this->page_data);
	}
}