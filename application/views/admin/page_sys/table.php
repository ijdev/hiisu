<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<link rel="stylesheet" href="public/metronic/global/plugins/jquery-treegrid/css/jquery.treegrid.css">
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>系統設定 <small>系統程式列表</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        系統設定
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a title="系統程式" href="admin/main/sysProgram">系統程式列表</a>
        <i class="fa fa-circle"></i>
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase"><?php if(@$current_category['sys_program_name']){ echo @$current_category['sys_program_name'].' - 子程式列表'; }else{ ?>主程式列表<?php }?></span>
            </div>
            <div class="actions btn-set pull-right">
              <a href="admin/main/sysProgramItem/0/<?php echo @$current_category['sys_program_config_sn'];?>" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
            
          </div>
          <div class="portlet-body">
            <table id="table_member" class="tree-category table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th>程式名稱</th>
                  <th>英文名稱</th>
                  <th>操作行為</th>
                  <th>狀態</th>
                  <th>功能</th>
                </tr>
              </thead>
<?php if(@$CategoryItems){foreach(@$CategoryItems as $CI){?>
              <tr class="treegrid-<?php echo $CI['sys_program_config_sn'];?><?php if($CI['upper_sys_program_config_sn']){?> treegrid-parent-<?php echo $CI['upper_sys_program_config_sn'];}?>">
                <td><a title="觀看下層分類" href="admin/main/sysProgram/<?php echo $CI['sys_program_config_sn'];?>"><?php echo $CI['sys_program_name'];?></a></td>
                <td><?php echo $CI['sys_program_eng_name'];?></td>
                <td><?php echo $CI['system_rule_set'];?></td>
                <td><?php if($CI['status']){?><i class="fa fa-check"></i><?php }else{ ?><i class="fa fa-times"></i><?php }?></td>
                <td>
                  <a href="admin/main/sysProgramItem/<?php echo $CI['sys_program_config_sn'];?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                  <?php if($CI['down_counter']==0){?>
                  	<a href="javascript:void(0);" class="deleteItem" ItemId="<?php echo $CI['sys_program_config_sn'];?>"><i class="fa fa-trash-o"></i>刪除</a>
									<?php }?>
                </td>
              </tr>
<?php }}?>
            </table>
            <span class="help-block"> 主程式的英文名稱為網址列的第二段，子程式為第三段（第一段均為admin）。 </span>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->