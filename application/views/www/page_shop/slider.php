<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 首頁 slider
*/
?>
<!-- BEGIN SLIDER -->
<div class="page-slider margin-bottom-35">
  <!-- LayerSlider start -->
  <div id="layerslider" style="width: 100%; height: 494px; margin: 0 auto;">
    <!-- slide one start -->
<?php if(@$sliders['banners']){ foreach($sliders['banners'] as $key=>$_Item){?>
      <a href="home/ad_link/<?php echo $_Item['banner_schedule_sn'];?>" title="<?php echo $_Item['banner_content_name'];?>" target="_blank">
    <div class="ls-slide ls-slide<?php echo $key+1?>" data-ls="offsetxin: right; slidedelay: 7000; transition2d: 110,111,112,113;">
      <img width="<?php echo $sliders['width'];?>" height="<?php echo $sliders['hight'];?>" style="width:<?php echo $sliders['width'];?>px;height:<?php echo $sliders['hight'];?>px;" src="public/uploads/<?php echo $_Item['banner_content_save_dir'];?>" class="ls-bg" alt="<?php echo $_Item['banner_content_name'];?>">
    </div></a>
<?php }}?>
  </div>
  <!-- LayerSlider end -->
</div>
<!-- END SLIDER -->