<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>系統設定 <small>系統程式編輯</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/admin">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        系統設定
        <i class="fa fa-circle"></i>
      </li>
      <li>
         <a href="admin/main/sysProgram">系統程式列表</a>
         <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         程式名稱 - <?php echo @$CategoryItem['sys_program_name']; ?> 編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">程式設定</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/main/sysProgram" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
             <?php $this->load->view('sean_admin/general/flash_error');?>
            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
<?php if(!@$CategoryItem['class1_program_flag']){ ?>
            <div class="form-group">
             <label class="col-md-3 control-label">主程式名稱 <span class="require">*</span></label>
             <div class="col-md-2">
              <?php echo $categorys;?>
             </div>
             <div class="col-md-2">
              <a data-toggle="modal" id="Addon" href="#basic" class="btn green"><i class="fa fa-plus"></i> 新增</a>
             </div>
            </div>
<?php }else{?>
<input type="hidden" name="category[upper_sys_program_config_sn]" value="0">
<?php }?>
               <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-md-3 control-label"><?php if(@$CategoryItem['class1_program_flag']){ ?>主<?php }?>程式名稱 <span class="require">*</span></label>
                  <div class="col-md-3">
                    <input type="text" name="category[sys_program_name]" class="form-control" value="<?php echo @$CategoryItem['sys_program_name']; ?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-md-3 control-label">英文名稱(網址) <span class="require">*</span></label>
                  <div class="col-md-3">
                    <input type="text" name="category[sys_program_eng_name]" class="form-control" value="<?php echo @$CategoryItem['sys_program_eng_name']; ?>" required>
                  </div>
                </div>
		           <div class="form-group">
		              <label for="name" class="col-md-3 control-label">狀態</label>
		              <div class="col-md-8">
		                <div class="radio-list">
		                    <label class="radio-inline">
		                      <input type="radio" name="category[status]" id="optionsRadios4" value="1" <?php if(@$CategoryItem['status']){ ?>checked<?php }?> required>  啟用  
		                    </label>
		                    <label class="radio-inline">
		                      <input type="radio" name="category[status]" id="optionsRadios5" value="0" <?php if(@$CategoryItem['status']==='0'){ ?>checked<?php }?> required>  停用 
		                    </label>
		                </div>
		              </div>
		            </div>
<?php //if(!@$CategoryItem['class1_program_flag']){ ?>
                <div class="form-group">
                  <label for="note" class="col-md-3 control-label">操作行為</label>
                  <div class="col-md-8">
                    <div class="checkbox-list">
                    	<?php foreach($category_attributes as $ca){ ?>
                      	<label class="checkbox-inline"><input type="checkbox" name="category_attribute[]" value="<?php echo @$ca['system_rule_type'];?>" <?php if(@$ca['status']){ echo 'checked';}?>/><?php echo @$ca['system_rule_name'];?></label>
                      <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-md-3 control-label">角色與行為</label>
                  <div class="col-md-8" >
                        <table class="table table-striped table-bordered table-hover" id="table_member">
                          <thead>
                            <tr>
				                    	<?php if($addons){?>
                              <th style="text-align:center;">角色</th>
				                      <?php }else{?>
                              <th style="text-align:center;">請先新增程式</th>
				                      <?php }?>
				                    	<?php foreach($category_attributes as $ca){ if(@$ca['status']){?>
				                      	<th style="text-align:center;"><?php echo @$ca['system_rule_name'];?></th>
				                      <?php } }?>
                            </tr>
                          </thead>
                          <tbody id="Addon_list">
                          	<?php if($addons){foreach($addons as $addon){?>
                            <tr class="odd gradeX" id="Addon_tr<?php echo $addon['role_sn'];?>">
                              <td align="center"><?php echo @$addon['role_name'];?></td>
				                    	<?php foreach($addon['system_rule'] as $sr){ ?>
				                      	<td align="center">
				                      		<label class="checkbox-inline"><input type="checkbox" name="system_rule[<?php echo $addon['role_sys_program_relation_sn'];?>][]" value="<?php echo @$sr['system_rule_type'];?>" <?php if(@$sr['status']){ echo 'checked';}?>/></label>
																</td>
				                      <?php } ?>
                            </tr>
                            <?php } }?>
                          </tbody>
                        </table>
                  </div>
                </div>
<?php //}?>
                <!--div class="form-group">
                  <label for="seq" class="col-md-3 control-label">排序 <span class="require">*</span></label>
                  <div class="col-md-8">
                    <input name="category[sort_order]" maxlength="4" type="text" class="form-control" value="<?php echo (@$CategoryItem['sort_order']) ? @$CategoryItem['sort_order']:'9999'; ?>" required>
                  </div>
                </div-->
                <div class="form-group">
                  <label for="updater" class="col-md-3 control-label">更新者</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" placeholder="<?php echo (@$CategoryItem['update_member_sn'])?@$CategoryItem['update_member_sn']:@$CategoryItem['create_member_sn']; ?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-md-3 control-label">更新時間</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" placeholder="<?php echo (@$CategoryItem['last_time_update']!='0000-00-00 00:00:00')?@$CategoryItem['last_time_update']:@$CategoryItem['create_date']; ?>" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <input type="hidden" name="sys_program_config_sn" value="<?php echo @$CategoryItem['sys_program_config_sn']; ?>">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="reset" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">新增主程式</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form">
          <div class="form-group">
            <label for="name" class="col-md-3 control-label">*主程式名稱</label>
            <div class="col-md-4">
              <input type="text" class="form-control" id="main_name" name="main_name" required>
            </div>
          </div>
          <!--div class="form-group">
            <label for="name" class="col-md-3 control-label">英文名稱</label>
            <div class="col-md-5">
              <input type="text" class="form-control" id="main_engname" name="main_engname">
            </div>
          </div-->
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
        <button type="button" id="Add_addon" class="btn blue">儲存</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->