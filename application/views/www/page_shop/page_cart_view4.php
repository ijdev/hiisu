<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 線上商店商品內頁
*/
//var_dump($order);
?>


<style>
  .pi-img-wrapper img{border:1px solid #EFEFEF;}
  .sale-product{background-color: #EFEFEF;padding: 12px;}
  .goods-page-image img{
    -webkit-box-shadow: 0 1px 8px rgba(0, 0, 0, 0.5);
    box-shadow: 0 1px 8px rgba(0, 0, 0, 0.5);
  }
  .del-goods-col{text-align: right;line-height: 2;}
  .del-goods{}
  .box_gift_cash{float: right; font-size: 18px; padding-right:24px;}
  .fa-exclamation-circle{font-size: 18px;background-color: #FFF;color: #FF0000;}
  .fa-usd-box{border-radius: 50%;border:2px solid #dfba49;display: inline-block;border-radius: 50% !important;    width: 18px;height: 18px;text-align: center; font-size: 12px;}
  .fa-usd-box .text-warning{font-size: 10px;}

  .shopping-total-price{border-top: 1px solid #cebea2 !important;font-weight: bold !important;padding-top: 24px !important;}
  .shopping-total{ width: 92%;}

  .table-shipping{
      border:0px; width:96% !important;margin:0 auto;
  }
  .table-shipping td{border:0px;padding: 6px 0px 6px 0;}

</style>
<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<link href="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="public/metronic/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css"><!-- for slider-range -->
<link href="public/metronic/global/plugins/rateit/src/rateit.css" rel="stylesheet" type="text/css">
<!-- Page level plugin styles END -->

			<ul class="breadcrumb">
				<li><a href="/">首頁</a></li>
				<li><a href="shop">線上商店</a></li>
				<li class="active">訂單完成</li>
			</ul>
	<!-- shopping steps -->
      <div class="shopping_steps" style="width:521px;">
				 <div class="step_over"><div class="step_normal_number">1</div>商品及配送</div>
				 <div class="step_arrow"><i class="icon-caret-right"></i></div>
				 <!--div class="step_normal"><div class="step_normal_number">2</div>折扣運費</div>
				 <div class="step_arrow"><i class="icon-caret-right"></i></div-->
				 <div class="step_normal"><div class="step_normal_number">2</div>填寫資料</div>
				 <div class="step_arrow"><i class="icon-caret-right"></i></div>
				 <div class="step_normal blkfont"><div class="step_number">3</div>訂單完成</div>
			</div>
			<!--<div class="clear_shopcart"><a onclick="location.href='cart.php?Action=clear';"><i class="icon-trashhs"></i>清空購物車</a></div>-->
	<!-- shopping steps -->
	<!-- BEGIN SIDEBAR & CONTENT -->

<!-- copy data -->
<div id="showshopping">
        <div class="row trans_name">
          <div class="col-md-3 col-sm-3">
            <strong>交易日期 : </strong><?php echo $order['order_create_date'];?>
          </div>
          <div class="col-md-3 col-sm-3">
           <strong>交易編號 : </strong>#<?php echo $order['order_num'];?>
          </div>
          <?/*<div class="col-md-6 col-sm-6" style="text-align:right;"> 
            <button type="button" class="print btn btn-default btn-sm"><i class="fa fa-print" aria-hidden="true"></i>  列印</button>
          </div>*/?>
        </div>
       <table class="table-hover responsive mob_tab order-table">
			<thead>
				 <tr>
          <th class="span_hidden">序號
          </th><th>圖片
          </th><th>商品名稱
          </th><th>賣家
          </th><th>規格
          </th><th>價格
          </th><th>數量
          </th><th>優惠折扣
          </th><th>小計
					</th></tr>
			</thead>
			<tbody class="mob_tbody">
								<?php $counter=1;
									$actual_sales_amount=0;$shipping_charge_amount=0;
									if($order['sub_order']){ foreach($order['sub_order'] as $key=> $Item2){
                      $shipping_charge_amount+=$Item2['shipping_charge_amount'];
									    if($Item2['order_item']){ foreach($Item2['order_item'] as $_key2=>$Item){ //var_dump($Item);?>
							<tr>
                <td data-label="序號" class="span_hidden mob_td1">
                <?php echo $counter;?>
                </td>
                <td class="mob_td2" data-label="圖片"><a href="/shop/shopItem/<?php echo $Item['product_sn'];?>">
								<?php if(!$Item['addon_flag']){?><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['image_path'];?>" alt="<?php echo $Item['image_alt'];?>" width="70" >
								<?php }else{?>
										<span>加購</span>
									<?php }?>
							  </a>
							  </td>
                <td class="mob_td3" data-label="商品名稱" class="receivePay_text"><a href="/shop/shopItem/<?php echo $Item['product_sn'];?>"><?php echo $Item['product_name'];?></a></td>
                <td class="mob_td2" data-label="賣家"><?php echo $Item2['brand'];?></td>
             <td class="mob_title2" data-title="規格"><?php echo ($Item['mutispec_color_name'])?$Item['mutispec_color_name']:'單一規格';?></td>
								<td class="mob_td4_1" data-label="價格"><span>$<?php echo number_format($Item['sales_price']);?></span></td>

								<td class="mob_td5_1" data-label="數量">                <!--{ html_options name="count" options=$Cart_item[list].goods[list1].storagelist selected=$Cart_item[list].goods[list1].count }-->
									<span>x<?php echo $Item['buy_amount'];?></span>
								</td>
                  <td class="mob_td4_1" data-label="優惠折扣"><span>$<?php echo number_format($Item['coupon_code_cdiscount_amount']);?></span></td>
									<td class="mob_td5" data-label="小計"><span>$<?php echo number_format($Item['actual_sales_amount']);?></span></td>
								</tr>
								<?php $counter++;$actual_sales_amount+=$Item['actual_sales_amount']; }}}
								}else{
								echo '<ul>查無訂購商品</ul>';}?>
          </tbody>
		</table>
	<table width="100%" class="order-table">
          <tbody>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td id="showtrans">
                    <table align="right" class="nomargin" style="font-size:16px;">
                          <tbody><tr>
                            <td align="center">&nbsp;</td>
                            <td align="left">&nbsp;</td>
                            <td colspan="2" align="right" style="padding-right:2px;">商品總金額：</td>
                            <td align="right">$<?php echo number_format($actual_sales_amount);?> 元</td>
                          </tr>
                          <!--tr>
                            <td align="center">&nbsp;</td>
                            <td align="left">&nbsp;</td>
                            <td colspan="2" align="right" style="padding-right:2px;">折扣金額：</td>
                            <td align="right">$<?php echo number_format($order['coupon_code_total_cdiscount_amount']) ?>元 </td>
                          </tr-->
                          <tr>
                            <td align="center">&nbsp;</td>
                            <td align="left">&nbsp;</td>
                            <td colspan="2" align="right" style="padding-right:2px;">配送費用：</td>
                            <td align="right">$<?php echo number_format($shipping_charge_amount);?> 元</td>
                          </tr>
                          <!--tr>
                            <td align="center">&nbsp;</td>
                            <td align="left">&nbsp;</td>
                            <td colspan="2" align="right" style="padding-right:2px;">配送方式：</td>
                            <td align="right"><?php echo @$delivery_method_name;?> </td>
                          </tr-->
                          <tr>
                            <td align="center">&nbsp;</td>
                            <td align="left">&nbsp;</td>
                            <td colspan="2" align="right" class="shop-item-price height-50">消費總金額：</td>
                            <td align="right" class="shop-item-price height-50">$
                            <?php echo number_format($order['total_order_amount']);?> 元 </td>
                          </tr>
                     </tbody></table>
                    </td>
				</tr>
				<tr><td align="center">
<!--div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-0 margin-bottom-10 margin-top-10 trans_name">
	&nbsp;折扣資訊
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table_all" style="border: 1px solid #ddd;">

<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-left table_title bold">
使用折價券
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 text-left table_bo table_white">
0元
</div>
<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-left table_bo noleft bold">
紅利折抵點數
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 text-left table_bo table_white">
0點
</div>

<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-left table_title bold nobottom">
紅利兌換點數
</div>
<div class="col-lg-10 col-md-10 col-sm-6 col-xs-6 text-left table_bo nobottom table_white">
0點
</div>
</div-->


<!--付款資訊-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-0 margin-bottom-10 margin-top-10 trans_name">
	 &nbsp;付款資訊
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table_all" style="border: 1px solid #ddd;">
<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left table_title bold">
付款方式
</div>
<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 text-left table_bo noleft table_white">
<?php echo $order['payment_method_name'];?>
</div>
<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left table_title bold nobottom bottom">
付款說明
</div>
<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 text-left table_bo noleft nobottom table_white">
<?php echo $order['payment_description'];?>
</div>
<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left table_title bold nobottom bottom">
付款狀態
</div>
<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 text-left table_bo noleft nobottom table_white">
    <?php if($order['payment_status']=='2' && $order['payment_done_date']!='0000-00-00 00:00:00'){
     echo '已付款'.' ('.$order['payment_done_date'].')';
    }else{
     echo $order['payment_status_name'];
    }?>
</div>
<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left table_title bold nobottom bottom">
    <?php if($order['payment_status']=='2' && $order['payment_done_date']!='0000-00-00 00:00:00'){
     echo '已付金額';
    }else{
     echo '應付金額';
    }?>
</div>
<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 text-left table_bo noleft nobottom table_white">
  <?php echo number_format($order['total_order_amount']);?>
</div>
</div>
<!--/付款資訊-->

<!--發票資訊-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-0 margin-bottom-10 margin-top-10 trans_name">
	&nbsp;發票資訊
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table_all" style="border: 1px solid #ddd;">

<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-left table_title bold">
發票格式
</div>
<div class="col-lg-10 col-md-10 col-sm-6 col-xs-6 text-left table_bo table_white">
<?php echo ($order['company_tax_id'] && $order['invoice_title'])?'三':'二'?>聯
</div>
<?php if($order['company_tax_id'] && $order['invoice_title']){?>
<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-left table_title bold bottom nobottom">
發票抬頭
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 text-left table_bo bottom table_white nobottom">
<?php echo $order['invoice_title']?>
</div>
<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-left table_bo noleft bold nobottom">
統一編號
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 text-left table_bo table_white nobottom">
<?php echo $order['company_tax_id']?>
</div>
<?}?>
<!--先隱藏
<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-left table_title bold nobottom bottom">
索取紙本發票
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 text-left table_bo bottom nobottom table_white">
否</div>
<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-left table_bo noleft bold nobottom">
發票捐贈
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 text-left table_bo nobottom table_white">
無</div>
先隱藏-->

</div>
<!--/發票資訊-->

<!--配送資訊-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-0 margin-bottom-10 margin-top-10 trans_name">
	&nbsp;配送資訊
</div>

<?php foreach($order['sub_order'] as $_item){?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table_all" style="border: 1px solid #ddd;margin-bottom:10px;">
  <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left table_title bold">
    賣家
  </div>
  <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 text-left table_bo noleft table_white">
    <?php echo $_item['brand'];?>
  </div>
  <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left table_title bold">
    配送方式
  </div>
  <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 text-left table_bo noleft table_white">
    <?php echo $_item['delivery_method_name'];?>
  </div>
  <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left table_title bold nobottom bottom">
    配送說明
  </div>
  <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 text-left table_bo noleft nobottom table_white">
    <?php echo $_item['delivery_description'];?>
  </div>
</div>
<?php }?>
<!--/配送資訊-->
<!--收貨人資訊-->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-0 margin-bottom-10 margin-top-10 trans_name">
	&nbsp;收貨人資訊
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-bottom-20 table_all" style="border: 1px solid #ddd;">
<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-left table_title bold">
收貨姓名
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 text-left table_bo table_white">
<?php echo $this->ijw->mask_words($order['receiver_last_name']);?>
</div>
<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-left table_bo noleft bold">
收貨地址
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 text-left table_bo table_white">
<?php echo $order['receiver_zipcode'];?> <?php echo $order['receiver_addr_city'];?><?php echo $order['receiver_addr_town'];?>
<?php echo $this->ijw->mask_words($order['receiver_addr1']);?>
</div>
<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-left table_title bold">
聯絡手機
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 text-left table_bo table_white">
<?php echo $this->ijw->mask_words($order['receiver_cell']);?>
</div>
<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-left table_bo bold noleft">
聯絡電話
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 text-left table_bo table_white">
<?php echo $this->ijw->mask_words($order['receiver_tel']);?>
</div>

<!--div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-left table_title bold">
宅配時間
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 text-left table_bo table_white">
不指定時間
</div>
<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-left table_bo bold noleft">
配送日期
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 text-left table_bo table_white">
&nbsp;
</div-->
<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left table_title bold nobottom bottom">
訂單備註
</div>
<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 text-left table_bo noleft nobottom table_white">
<?php if(@$order['sub_order'][0]['memo']){?>
<pre><?php echo @$order['sub_order'][0]['memo']?></pre>
<?}?>
</div>

</div>
    </td>
              </tr>
				<tr>
					<td height="23" class="padding-top-20" align="center">
						<input type="button" name="button3" id="button3" value="返回首頁" onclick="location.href='/';" class="btn size-15 cart_botton">
						<input type="button" name="button" id="button" value="查訂單" onclick="location.href='member/memberOrderList';" class="btn size-15 btn-primary cart_botton_r">
					</td>
				</tr>
		</tbody>
	</table>
 </div>
<!-- copy data -->





<!-- END SIDEBAR & CONTENT -->
<div id="gift_cash_exclamation" style="display:none;">
  <div class="alert alert-success">
    <ul>
      <li>每次消費可百分百(100%)，1點折抵新台幣1元，全額折抵訂購金額．</li>
      <li>系統依購物金的使用期限，自動從最接近過期的點數開始折抵．</li>
      <li>您可前往「會員中心」查看您持有的購物金餘額，使用期限與紀錄．</li>
    </ul>
  </div>
</div>
<div id="coupon_input" style="display:none;">
  <div style="width:75%">
    <div style="height:32px;"></div>
    <strong>請輸入代碼（折扣券、活動代碼皆適用）</strong>
    <form class="form-horizontal" role="form" id="coupon_form">
      <div class="form-body">
        <div class="form-group">
          <div class="col-md-12">
            <div class="input-group">
<div style="display:-webkit-box;text-align:center;">
	              <input required type="text" id="couponcode" style="width:420px;" class="form-control" placeholder="請輸入活動代碼並按確定">
              <span class="input-group-btn">
              <button class="btn blue couponcode" type="submit">確定</button>
              </span>
</div>
<div class="note note-success note-bordered" id="note_couponcode" style="text-align:center;width:auto;display:none;"></div>
            </div>
            <span class="help-block">
              請注意英文字母大小寫視為不同，單次結帳恕無法重複折扣．
            </span>
          </div>
        </div>
      </div>
	<input type="hidden" id="rowid">
    </form>
  </div>
</div>
<style>
.shopping_steps {
    width: 695px;
    text-align: center;
    font-size: 1.2em;
    color: #333;
    height: 52px;
    margin: 20px auto;
}
.shopping_steps .step_over {
    width: 150px;
    line-height: 12px;
    height: 50px;
    float: left;
    position: relative;
    font-weight: 700;
    margin-right: 5px;
    margin-left: 5px;
}
.shopping_steps .step_arrow {
    width: 15px;
    float: left;
}
.shopping_steps .step_normal {
    width: 150px;
    line-height: 12px;
    height: 50px;
    float: left;
    position: relative;

    color: #ccc;
    margin-right: 5px;
    margin-left: 5px;
}
.shopping_steps .step_number {
    background-color: #DB4A4A;
    color: #fff;
    position: absolute;
    top: 25px;
    left: 68px;
    font-size: 15px;
    line-height: 20px;
    height: 20px;
    width: 20px;
    border-radius: 10px !important;
}
.shopping_steps .step_normal_number {
    background-color: #ccc;
    color: #fff;
    position: absolute;
    top: 25px;
    left: 65px;
    font-size: 15px;
    line-height: 20px;
    height: 20px;
    width: 20px;
    border-radius: 10px !important;
}
i.icon-caret-right:before {
    content: "\f0da";
    font-family: fontAwesome;
    font-style: normal;
    font-size: 18px;
}
.margin-bottom-25.display_none {
    display: none;
}
.clear_shopcart {
    display: table;
    line-height: 25px;
    width: 100%;
    margin: auto;
    background-color: rgba(210,210,210,0.2);
    margin-bottom: 15px;
    border: 1px solid rgba(210, 210, 210, 0.2);
    text-align: right;
}
.clear_shopcart a {
    display: block;
    margin: 5px;
    float: right;
    cursor: pointer;
    color: #8F8F8F;
    line-height: 25px;
    height: 25px;
    padding: 5px;
    padding-top: 0px;
    padding-bottom: 0px;
}
.clear_shopcart i {
    margin-right: 5px;
    font-size: 14px;
    color: #333333;
}
i.icon-trashhs:before {
    content: "\f014";
    font-family: fontAwesome;
    font-style: normal;
}
.trans_name {
    width: 100%;
    margin: auto;
    text-align: left;
    font-size: 16px;
    color: #333333;
    margin-bottom: 10px;
}



.border_left {
    border-left: 1px solid rgba(100,100,100,0.1) !important;
}
table.responsive {
    width: 100%;
    margin: 0;
    padding: 0;
    border-collapse: collapse;
    border-spacing: 0;
}
table {
    background-color: transparent;
}
table.responsive tr {
    border: 1px solid #ddd;
}
table.responsive th {
    background-color: #F3F3F3;
}
table.responsive th {
    text-transform: uppercase;
    font-size: 14px;
    letter-spacing: 1px;
}
table.responsive th, table.responsive td {
    padding: 5px;
    text-align: center;
}
.shopping_present {
    padding: 10px;
    border: 1px solid rgba(100,100,100,0.1);
}
#cart_add_items .pics {
    width: 70px;
}
td.padding-top-20 {
    padding-bottom: 20px;
}
.shop-item-price.height-50 {
    color: #de4a50;
    font-size: 18px;
}
.cart_botton {
    border-radius: 3px !important;
	color:#66667c;
}
.cart_botton_r {
    border-radius: 3px !important;
	color:#fff;
}
.cart_botton:hover {
    border-radius: 3px !important;
	color:#000;
}
.cart_botton_r:hover {
	color:#fff;
}
.shopping_steps .step_over {

    color: #ccc;
}
.blkfont {
   font-weight: 700;
    color: #000!important;
}
.table_all {
    padding: 5px;
	font-size: 14px;
    line-height: 1.4;
}
</style>
<div id="css" style="display:none;">
<style type="text/css">
  .order-table{
    border: 0px;
    width: 100%;
    border-spacing: 2px;
  }
    .order-table tr{
    }
      .order-table tr th{
        border-bottom: 2px solid #EFEFEF;
        padding:9px 0 10px 0;
        font-weight: bold;
      }
      .order-table tr td{
        border-bottom: 1px solid #EFEFEF;
        padding:9px 0 10px 0;
      }
      .table_title{
        text-align:left;
        float:left;
        padding-right:20px;
      }
      .table_bo{
        text-align:left;
      }
.trans_name{
        text-align:left;
}
</style>
</div>