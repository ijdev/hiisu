<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
* for admin console template preview
*/

class Sysconfig extends MY_Controller {

	/**
	 * 總控首頁 Template
	 *
	 */

	 	public function __construct()
		{
			 parent::__construct();

				$this->load->library('session');
				$this->load->helper('form');
				$this->load->helper('url');
				$this->load->library('form_validation');
				$this->load->library('sean_lib/Sean_form_general');
				$this->load->model('general/My_general_ct');
				$this->load->config('ij_config');
				$this->load->config('ij_sys_config');
			    $this->load->model('sean_models/Sean_general');
				$this->load->model('sean_models/Sean_db_tools');
				$this->load->model('libraries_model');
				$this->page_data = array();
				$this->page_data['unreadmessage']= $this->libraries_model->get_unread_message_count();
				$this->page_data['init_control']="admin/";
				$this->init_control=$this->page_data['init_control'];
				$this->page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
				$this->page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$this->page_data['page_content']['view_path'] = 'sean_admin/page_home/page_content'; // 頁面主內容 view 位置, 必要
				$this->page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_home/page_content_js'; // 頁面主內容的 JS view 位置, 必要
				$this->lang->load('general', 'zh-TW');

				$this->LoginCheck();
		}

 /*
	*
  * 檢查是否有登入
	*/

	//$this->LoginCheck();
	public function LoginCheck()
	{

			if($this->session->userdata('member_login')== true)
			return true;
			else
			redirect(base_url("/admin/login"));
			exit();
	}


/* ------------------------------------------------------ End 總控系統管理---------------------------------------------------*/


	 public function Country_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Country_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Country_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Country_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(

		  'city_name'=>array('header'=>$_table_field["city_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'city_eng_name'=>array('header'=>$_table_field["city_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'country_tel'=>array('header'=>$_table_field["country_tel"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'10', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'eff_start_date'=>array('header'=>$_table_field["eff_start_date"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'eff_end_date'=>array('header'=>$_table_field["eff_end_date"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}


/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function State_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="State_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function State_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="State_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(

		  'country_code'=>array('header'=>$_table_field["country_code"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'state_name'=>array('header'=>$_table_field["state_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'state_eng_name'=>array('header'=>$_table_field["state_eng_name"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'eff_start_date'=>array('header'=>$_table_field["eff_start_date"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'eff_end_date'=>array('header'=>$_table_field["eff_end_date"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Currency_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Currency_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Currency_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Currency_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
			'currency_name'=>array('header'=>$_table_field["currency_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'currency_eng_name'=>array('header'=>$_table_field["currency_eng_name"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'currency_abbr'=>array('header'=>$_table_field["currency_abbr"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Event_type_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Event_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Event_type_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Event_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
			'event_type_name'=>array('header'=>$_table_field["event_type_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}


/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function City_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="City_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function City_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="City_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(

		  'associated_country_code'=>array('header'=>$_table_field["associated_country_code"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
		  'associated_state_code'=>array('header'=>$_table_field["associated_state_code"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'city_name'=>array('header'=>$_table_field["city_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'city_eng_name'=>array('header'=>$_table_field["city_eng_name"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'eff_start_date'=>array('header'=>$_table_field["eff_start_date"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'eff_end_date'=>array('header'=>$_table_field["eff_end_date"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}


/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Town_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Town_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'admin/page_sys/town';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'admin/page_sys/town_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Town_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Town_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(

		  'city_code'=>array('header'=>$_table_field["city_code"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'town_name'=>array('header'=>$_table_field["town_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'town_eng_name'=>array('header'=>$_table_field["town_eng_name"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'zipcode'=>array('header'=>$_table_field["zipcode"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			//'eff_start_date'=>array('header'=>$_table_field["eff_start_date"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			//'eff_end_date'=>array('header'=>$_table_field["eff_end_date"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'4', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'（800以上為外島）','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Cc_faq_close_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Cc_faq_close_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Cc_faq_close_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Cc_faq_close_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
			'cc_question_close_name'=>array('header'=>$_table_field["cc_question_close_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'cc_question_close_eng_name'=>array('header'=>$_table_field["cc_question_close_eng_name"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}


/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Member_level_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Member_level_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Member_level_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Member_level_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=99;
    //$_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
			'member_level'=>array('header'=>$_table_field["member_level"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'level_name'=>array('header'=>$_table_field["level_name"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'profit_sharing_rate'=>array('header'=>$_table_field["profit_sharing_rate"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}


/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Supplier_status_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Supplier_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Supplier_status_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Supplier_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
			'supplier_status_name'=>array('header'=>$_table_field["supplier_status_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'supplier_status_eng_name'=>array('header'=>$_table_field["supplier_status_eng_name"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}


/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Supplier_level_type_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Supplier_level_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Supplier_level_type_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Supplier_level_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
		($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
		(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
			'supplier_level_type_name'=>array('header'=>$_table_field["supplier_level_type_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'supplier_level_type_eng_name'=>array('header'=>$_table_field["supplier_level_type_eng_name"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}


/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function System_status_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="System_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function System_status_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="System_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
			'system_status_name'=>array('header'=>$_table_field["system_status_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'system_status_eng_name'=>array('header'=>$_table_field["system_status_eng_name"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}



/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function System_level_type_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="System_level_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function System_level_type_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="System_level_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
			'system_level_type_name'=>array('header'=>$_table_field["system_level_type_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'system_level_type_eng_name'=>array('header'=>$_table_field["system_level_type_eng_name"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}


/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Status_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Status_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'status_name'=>array('header'=>$_table_field["status_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'status_eng_name'=>array('header'=>$_table_field["status_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}


/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/



	 public function Login_status_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Login_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Login_status_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Login_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(

		  'login_status_name'=>array('header'=>$_table_field["login_status_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'login_status_eng_name'=>array('header'=>$_table_field["login_status_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}


/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Member_status_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Member_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Member_status_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Member_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'member_status_name'=>array('header'=>$_table_field["member_status_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'member_status_eng_name'=>array('header'=>$_table_field["member_status_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Serial_card_status_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Serial_card_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Serial_card_status_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Serial_card_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'serial_card_status_name'=>array('header'=>$_table_field["serial_card_status_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'serial_card_status_eng_name'=>array('header'=>$_table_field["serial_card_status_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Coupon_code_status_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Coupon_code_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Coupon_code_status_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Coupon_code_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'coupon_code_status_name'=>array('header'=>$_table_field["coupon_code_status_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'coupon_code_status_eng_name'=>array('header'=>$_table_field["coupon_code_status_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Order_cancel_reason_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Order_cancel_reason_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Order_cancel_reason_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Order_cancel_reason_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'order_cancel_reason_name'=>array('header'=>$_table_field["order_cancel_reason_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'order_cancel_reason_eng_name'=>array('header'=>$_table_field["order_cancel_reason_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Dealer_status_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Dealer_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Dealer_status_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Dealer_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'dealer_status_name'=>array('header'=>$_table_field["dealer_status_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'dealer_status_eng_name'=>array('header'=>$_table_field["dealer_status_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Member_level_type_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Member_level_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Member_level_type_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Member_level_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'member_level_type_name'=>array('header'=>$_table_field["member_level_type_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'member_level_type_eng_name'=>array('header'=>$_table_field["member_level_type_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}


/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Product_status_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Product_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Product_status_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Product_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'product_status_name'=>array('header'=>$_table_field["product_status_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'product_status_eng_name'=>array('header'=>$_table_field["product_status_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Delivery_status_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Delivery_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Delivery_status_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Delivery_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'delivery_status_name'=>array('header'=>$_table_field["delivery_status_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'delivery_status_eng_name'=>array('header'=>$_table_field["delivery_status_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}


/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Product_type_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Product_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Product_type_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Product_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'product_type_name'=>array('header'=>$_table_field["product_type_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'product_type_eng_name'=>array('header'=>$_table_field["product_type_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Discount_type_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Discount_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Discount_type_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Discount_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'discount_type_name'=>array('header'=>$_table_field["discount_type_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'discount_type_eng_name'=>array('header'=>$_table_field["discount_type_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}


/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Sub_order_status_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Sub_order_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Sub_order_status_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Sub_order_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'sub_order_status_name'=>array('header'=>$_table_field["sub_order_status_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sub_order_status_eng_name'=>array('header'=>$_table_field["sub_order_status_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Join_path_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Join_path_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Join_path_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Join_path_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'join_path_name'=>array('header'=>$_table_field["join_path_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'join_path_eng_name'=>array('header'=>$_table_field["join_path_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Approval_status_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Approval_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Approval_status_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Approval_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'approval_status_name'=>array('header'=>$_table_field["approval_status_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'approval_status_eng_name'=>array('header'=>$_table_field["approval_status_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Dealer_level_type_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Dealer_level_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Dealer_level_type_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Dealer_level_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'dealer_level_type_name'=>array('header'=>$_table_field["dealer_level_type_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'dealer_level_type_eng_name'=>array('header'=>$_table_field["dealer_level_type_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Resource_rule_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Resource_rule_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Resource_rule_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Resource_rule_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'resource_rule_name'=>array('header'=>$_table_field["resource_rule_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'resource_rule_eng_name'=>array('header'=>$_table_field["resource_rule_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Serial_card_type_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Serial_card_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Serial_card_type_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Serial_card_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'serial_card_type_name'=>array('header'=>$_table_field["serial_card_type_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'serial_card_type_eng_name'=>array('header'=>$_table_field["serial_card_type_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Cacel_order_process_status_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Cacel_order_process_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Cacel_order_process_status_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Cacel_order_process_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'cacel_order_process_name'=>array('header'=>$_table_field["cacel_order_process_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'cacel_order_process_eng_name'=>array('header'=>$_table_field["cacel_order_process_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Discount_code_type_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Discount_code_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Discount_code_type_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Discount_code_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'discount_code_type_name'=>array('header'=>$_table_field["discount_code_type_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'discount_code_type_eng_name'=>array('header'=>$_table_field["discount_code_type_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}


/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Open_invoice_type_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Open_invoice_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Open_invoice_type_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Open_invoice_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'open_invoice_type_name'=>array('header'=>$_table_field["open_invoice_type_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'open_invoice_type_eng_name'=>array('header'=>$_table_field["open_invoice_type_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Ta_deliver_time_interval_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Ta_deliver_time_interval_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Ta_deliver_time_interval_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Ta_deliver_time_interval_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'ta_deliver_time_interval_name'=>array('header'=>$_table_field["ta_deliver_time_interval_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'ta_deliver_time_interval_eng_name'=>array('header'=>$_table_field["ta_deliver_time_interval_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Invoice_donor_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Invoice_donor_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Invoice_donor_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Invoice_donor_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'invoice_donor_name'=>array('header'=>$_table_field["invoice_donor_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'invoice_donor_eng_name'=>array('header'=>$_table_field["invoice_donor_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Pricing_method_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Pricing_method_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Pricing_method_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Pricing_method_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'pricing_method_name'=>array('header'=>$_table_field["pricing_method_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'pricing_method_eng_name'=>array('header'=>$_table_field["pricing_method_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Attribute_type_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Attribute_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Attribute_type_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Attribute_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'attribute_type_name'=>array('header'=>$_table_field["attribute_type_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'attribute_type_eng_name'=>array('header'=>$_table_field["attribute_type_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Product_package_type_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Product_package_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Product_package_type_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Product_package_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'product_package_type_name'=>array('header'=>$_table_field["product_package_type_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'product_package_type_eng_name'=>array('header'=>$_table_field["product_package_type_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Spec_option_type_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Spec_option_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Spec_option_type_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Spec_option_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'spec_option_type_name'=>array('header'=>$_table_field["spec_option_type_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'spec_option_type_eng_name'=>array('header'=>$_table_field["spec_option_type_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Brand_status_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Brand_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Brand_status_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Brand_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'brand_status_name'=>array('header'=>$_table_field["brand_status_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'brand_status_eng_name'=>array('header'=>$_table_field["brand_status_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Category_status_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Category_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Category_status_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Category_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'category_status_name'=>array('header'=>$_table_field["category_status_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'category_status_eng_name'=>array('header'=>$_table_field["category_status_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Dealer_order_status_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Dealer_order_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Dealer_order_status_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Dealer_order_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'dealer_order_status_name'=>array('header'=>$_table_field["dealer_order_status_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'dealer_order_status_eng_name'=>array('header'=>$_table_field["dealer_order_status_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}


/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Banner_type_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Banner_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Banner_type_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Banner_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'banner_type_name'=>array('header'=>$_table_field["banner_type_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'banner_type_eng_name'=>array('header'=>$_table_field["banner_type_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Purchase_sales_stock_status_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Purchase_sales_stock_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Purchase_sales_stock_status_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Purchase_sales_stock_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'purchase_sales_stock_status_name'=>array('header'=>$_table_field["purchase_sales_stock_status_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'purchase_sales_stock_status_eng_name'=>array('header'=>$_table_field["purchase_sales_stock_status_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Send_member_level_type_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Send_member_level_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Send_member_level_type_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Send_member_level_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'send_member_level_type_name'=>array('header'=>$_table_field["send_member_level_type_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'send_member_level_type_eng_name'=>array('header'=>$_table_field["send_member_level_type_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Wedding_website_status_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Wedding_website_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Wedding_website_status_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Wedding_website_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'wedding_website_status_name'=>array('header'=>$_table_field["wedding_website_status_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'wedding_website_status_eng_name'=>array('header'=>$_table_field["wedding_website_status_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Question_status_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Question_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Question_status_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Question_status_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'member_question_status_name'=>array('header'=>$_table_field["member_question_status_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'member_question_status_eng_name'=>array('header'=>$_table_field["member_question_status_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Purchase_sales_stock_type_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Purchase_sales_stock_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Purchase_sales_stock_type_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Purchase_sales_stock_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'banner_type_name'=>array('header'=>$_table_field["banner_type_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'banner_type_eng_name'=>array('header'=>$_table_field["banner_type_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}


/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Invoice_carrie_type_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Invoice_carrie_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Invoice_carrie_type_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Invoice_carrie_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'invoice_carrie_type_name'=>array('header'=>$_table_field["invoice_carrie_type_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'invoice_carrie_type_eng_name'=>array('header'=>$_table_field["invoice_carrie_type_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Payment_method_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Payment_method_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Payment_method_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Payment_method_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
		($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
		(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'payment_method_name'=>array('header'=>$_table_field["payment_method_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'payment_method_eng_name'=>array('header'=>$_table_field["payment_method_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'payment_fee'=>array('header'=>$_table_field["payment_fee"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'6', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'(2.5% 請輸入0.025)','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'fee_limit'=>array('header'=>$_table_field["fee_limit"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'5', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'(0表示無上限)','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Delivery_method_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Delivery_method_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Delivery_method_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Delivery_method_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'delivery_method_name'=>array('header'=>$_table_field["delivery_method_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'delivery_method_eng_name'=>array('header'=>$_table_field["delivery_method_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}


/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Logistics_company_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Logistics_company_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Logistics_company_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Logistics_company_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'logistics_company_name'=>array('header'=>$_table_field["logistics_company_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'logistics_company_eng_name'=>array('header'=>$_table_field["logistics_company_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}


/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/

	 public function Credit_card_type_ct()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Credit_card_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);



	}

	public function Credit_card_type_ct_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Credit_card_type_ct";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
		  'credit_card_type_name'=>array('header'=>$_table_field["credit_card_type_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'credit_card_type_eng_name'=>array('header'=>$_table_field["credit_card_type_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */



		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}




			public function Rewrite_config_file()
			{
					//標題
			$this->page_data['_page_title']="<h1>代碼產生設定檔</h1>";
    	$this->page_data['_caption_subject']="代碼產生設定檔";

					$nl = "\n";

					$string_data = '<?php '.$nl.'
if (!defined(\'BASEPATH\')) exit(\'No direct script access allowed\');
'.$nl.'
$config  = array('.$nl;



					  $_config_db = array(
					  "city_ct" =>array("ddb"=>"ij_city_ct","kkey"=>"city_code","vvalue"=>"city_name"),
					  "town_ct" =>array("ddb"=>"ij_town_ct","kkey"=>"town_code","vvalue"=>"town_name"),
					  "system_status_ct" =>array("ddb"=>"ij_system_status_ct","kkey"=>"system_status","vvalue"=>"system_status_name"),
					  "system_level_type_ct" =>array("ddb"=>"system_level_type_ct","kkey"=>"system_level_type","vvalue"=>"system_level_type_name"),
					  "member_status_ct" =>array("ddb"=>"ij_member_status_ct","kkey"=>"member_status","vvalue"=>"member_status_name"),
					  "member_level_type_ct" =>array("ddb"=>"ij_member_level_type_ct","kkey"=>"member_level_type","vvalue"=>"member_level_type_name"),
					  "dealer_status_ct" =>array("ddb"=>"ij_dealer_status_ct","kkey"=>"dealer_status","vvalue"=>"dealer_status_name"),
					  //"dealer_level_type_ct" =>array("ddb"=>"ij_dealer_level_type_ct","kkey"=>"dealer_level_type","vvalue"=>"dealer_level_type_name"),
					  "domain_ct" =>array("ddb"=>"ij_biz_domain","kkey"=>"domain_sn","vvalue"=>"domain_name"),

					 );


					  foreach($_config_db as $dbkey => $dbvalue)
					  {

							  $_where=" where status=1  ";
							  $_config_array=$this->Sean_db_tools->db_get_array($dbvalue["kkey"],$dbvalue["vvalue"],$dbvalue["ddb"],$_where);


								$string_data .= '"'.$dbkey.'"=> array('.$nl;
								foreach($_config_array as $kkey => $vvalue)
								{
									$replace_from = array('"', '\\', '$');
									$replace_to   = array('&#034;', '\\\\', '&#36;');
									$key_text = str_replace($replace_from, $replace_to, $kkey); // double slashes
									$string_data .= '"'.$key_text.'" => "'.$vvalue.'",'.$nl;
								}

								$string_data .= $nl.'),'.$nl;
						}



					$string_data .= ');'.$nl;




					$this->page_data['_message']="city_ct 參數產生完成 <br> town_ct 參數產生完成";


						// Write data to the file
						$voc_file = dirname($_SERVER["SCRIPT_FILENAME"]).'/application/config/ij_config.php';


						@chmod($voc_file, 0755);
						$fh = @fopen($voc_file, 'w');
						if(!$fh){
							$this->error = 'Cannot open vocabulary file: '.$voc_file;
						}else{
							@fwrite($fh, $string_data);
							@fclose($fh);
						}
						@chmod($voc_file, 0644);
					  // 頁面主內容 view 位置, 必要
						$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_zero';
						// 頁面主內容的 JS view 位置, 必要
						$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
						$this->load->view('sean_admin/general/main', $this->page_data);


						//$_city_ct=$this->config->item("city_ct");
	 					//$this->session->set_flashdata("success_message",$_city_ct);


			}
	 public function Mail()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Mail";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where=" order by last_time_update desc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Mail_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Mail";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    //$_next_sort=1;
    //$_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */


	    $this->page_data["em_columns"] = array(
				'system_email_type'=>array('header'=>$_table_field["system_email_type"],'type'=>($_action=="edit") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
				'system_email_titile'=>array('header'=>$_table_field["system_email_titile"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
				'system_email_content'=>array('header'=>$_table_field["system_email_content"], 'type'=>($_action=="view") ? 'label':'textarea','required'=>'','required_message'=>'', 'form_control'=>'ckeditor', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'600px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
				'memo'=>array('header'=>$_table_field["memo"], 'type'=>($_action=="view") ? 'label':'textarea','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
				//'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
				'status'=>array('header'=>$_table_field["status"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'600px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
				//'create_date'=>array('header'=>$_table_field["create_date"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
				//'create_member_sn'=>array('header'=>$_table_field["create_member_sn"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
				//'update_member_sn'=>array('header'=>$_table_field["update_member_sn"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
				//'last_time_update'=>array('header'=>$_table_field["last_time_update"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
				//'last_send_time'=>array('header'=>$_table_field["last_send_time"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
		 );

   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}


/*---------------------------------------------Begin 代碼區 -------------------------------------------------------*/


	 public function Location()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Location";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Location_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Location";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
			'wedding_location_name'=>array('header'=>$_table_field["wedding_location_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'addr_country_code'=>array('header'=>$_table_field["addr_country_code"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'add_state_code'=>array('header'=>$_table_field["add_state_code"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'addr_city_code'=>array('header'=>$_table_field["addr_city_code"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'addr_town_code'=>array('header'=>$_table_field["addr_town_code"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'addr_country'=>array('header'=>$_table_field["addr_country"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'addr_state'=>array('header'=>$_table_field["addr_state"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'addr_city'=>array('header'=>$_table_field["addr_city"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'addr_town'=>array('header'=>$_table_field["addr_town"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'zipcode'=>array('header'=>$_table_field["zipcode"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'addr1'=>array('header'=>$_table_field["addr1"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'addr2'=>array('header'=>$_table_field["addr2"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'status'=>array('header'=>$_table_field["status"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'status'=>array('header'=>$_table_field["status"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'create_date'=>array('header'=>$_table_field["create_date"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'create_member_sn'=>array('header'=>$_table_field["create_member_sn"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'update_member_sn'=>array('header'=>$_table_field["update_member_sn"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'last_time_update'=>array('header'=>$_table_field["last_time_update"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

}
