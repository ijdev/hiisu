<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zh-tw">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Hiisu囍市集 | 總控登入</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<base href="<?php echo $this->config->item('base_url'); ?>" />
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="public/metronic/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="public/metronic/admin/pages/css/login2.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="public/metronic/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="public/metronic/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="public/metronic/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo">
  <!--img src="public/metronic/admin/layout3/img/logo-big-white.png" style="height: 130px;" alt=""/-->
  <img src="public/img/ij-logo.png" alt=""/>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content" style="margin-top:0px;">
  <!-- BEGIN LOGIN FORM -->
  <form class="login-form" action="<?php echo $_action_link;?>" method="post">
    <div class="alert alert-danger display-hide">
      <button class="close" data-close="alert"></button>
      <span> 所有欄位均必須輸入！</span>
    </div>
    <?php $this->load->view('sean_admin/general/flash_error');?>
     <!--div class="form-group">
      	<label class="control-label visible-ie8 visible-ie9">登入類別</label>
          <div class="radio-list">
  				  <!--<label class="radio-inline"><input type="radio" required name="login_type" <?php echo ($login_type=='9')?'checked':'';?> value="9">系統管理</label>
  				  <label class="radio-inline"><input type="radio" required name="login_type" <?php echo ($login_type=='5')?'checked':'';?> value="5">供應商</label>
  				  <label class="radio-inline"><input type="radio" required name="login_type" <?php echo ($login_type=='4')?'checked':'';?> value="4">經銷商</label>
  				  <label class="radio-inline"><input type="radio" required name="login_type" <?php echo ($login_type=='1')?'checked':'';?> value="1">總控（供應商）</label>
  				  <label class="radio-inline"><input type="radio" required name="login_type" <?php echo ($login_type=='2')?'checked':'';?> value="2">經銷商</label>
          </div>
      </div-->
    <div class="form-group">
      <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
      <label class="control-label visible-ie8 visible-ie9">帳號</label>
      <input  class="form-control form-control-solid placeholder-no-fix" type="text" required autocomplete="off"  placeholder="帳號" name="username"/>
    </div>
    <div class="form-group">
      <label class="control-label visible-ie8 visible-ie9">密碼</label>
      <input  class="form-control form-control-solid placeholder-no-fix" type="password" required autocomplete="off" placeholder="密碼" name="password"/>
    </div>
    <div class="form-group row">
        <div class="col-lg-6">
          <label class="control-label visible-ie8 visible-ie9">驗證碼</label>
          <input name="rycode" type="text" class="form-control" autocomplete="off" required id="captcha" placeholder="驗證碼" size="6"><br>
        </div>
        <div class="col-lg-6">
          <?php echo $_captcha_img;?> &nbsp;&nbsp;<?php /*<i class="fa fa-refresh"></i>*/?>
        </div>
    </div>
    <div class="form-actions">
      <button type="submit" class="btn btn-primary btn-block uppercase">登入</button>
    </div>
  </form>
  <!-- END LOGIN FORM -->
</div>
<div class="copyright hide">
   2015 © IJWedding.
</div>
<!-- END LOGIN -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="public/metronic/global/plugins/respond.min.js"></script>
<script src="public/metronic/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="public/metronic/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="public/metronic/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/login.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {     
  Metronic.init(); // init metronic core components
  Layout.init(); // init current layout
  Login.init();
  Demo.init();
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>