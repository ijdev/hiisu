<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

$config  = array(
"system_status_ct"=> array(
"1" => "未啟用",
"2" => "已啟用",
),

"member_status_ct"=> array(
"1" => "已註冊,未驗證成功",
"2" => "已驗證成功",
"3" => "列入黑名單中",
"4" => "失效",

),
"member_level_type_ct"=> array(
"1" => "一般會員",
"2" => "VIP",
"3" => "策略聯盟會員",

),
"dealer_status_ct"=> array(
"1" => "未啟用",
"2" => "已啟用",

),
"dealer_level_type_ct"=> array(
"1" => "經銷商員工",
"2" => "經銷商管理員",

),
"order_ct"=> array(
	"payment_method" => "payment_method",
	"company_tax_id" => "company_tax_id",
	"invoice_title" => "invoice_title",
	"buyer_email" => "buyer_email",
	"buyer_last_name" => "buyer_last_name",
	"buyer_addr_state" => "buyer_addr_state",
	"buyer_addr_city" => "buyer_addr_city",
	"buyer_addr_town" => "buyer_addr_town",
	"buyer_zipcode" => "buyer_zipcode",
	"buyer_addr1" => "buyer_addr1",
	"buyer_tel" => "buyer_tel",
	"buyer_cell" => "buyer_cell",
	"receiver_last_name" => "receiver_last_name",
	"receiver_tel" => "receiver_tel",
	"receiver_cell" => "receiver_cell",
	"receiver_addr_state" => "receiver_addr_state",
	"receiver_addr_city" => "receiver_addr_city",
	"receiver_addr_town" => "receiver_addr_town",
	"receiver_zipcode" => "receiver_zipcode",
	"receiver_addr1" => "receiver_addr1",
	//"delivery_method" => "delivery_method",
),
"label_status"=> array(
	"6" => "primary",
	"1" => "success",
	"2" => "warning",
	"3" => "info",
	"4" => "danger",
	"5" => "default",
),
"cash_type"=> array(
	"1" => "退款購物金",
	"2" => "分潤獎金",
	"3" => "招募獎金",
),
);
