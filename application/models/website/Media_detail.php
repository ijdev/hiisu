<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Media_detail extends CI_Model {

    /*
    */

    public $limit = 10; // query limit setting
    public $until = 0; // last query sn

    public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
    }
    /*
    * media warehouse 下的媒體列表
    * 全數拉出管理
    */
    public function get_media_warehouse_result( $member_sn = 0 )
    {
        $this->db->select('*');
        $this->db->from('ij_media_detail');
        $this->db->where('create_member_sn', $member_sn);
        $this->db->where('status', 1);
        $this->db->order_by('media_detail_sn', 'desc');
        $query = $this->db->get();
        return $result = $query->result_array();
    }
    /*
    * media warehouse 最後上傳的資料
    */
    public function get_media_warehouse_last_item( $member_sn = 0, $media_warehouse_sn = 0 )
    {
        $this->db->select('*');
        $this->db->from('ij_media_detail');
        $this->db->where('create_member_sn', $member_sn);
        $this->db->where('media_warehouse', $media_warehouse_sn);
        $this->db->where('status', 1);
        $this->db->order_by('media_detail_sn', 'desc');
        $this->db->limit('media_detail_sn', 1);
        $query = $this->db->get();
        return $row = $query->row_array();
    }
    /*
    * media warehouse 更新的資料
    */
    public function update_data( $media_detail_sn = 0, $member_sn = 0, $update_data = array() )
    {
        return $this->db->update('ij_media_detail', $update_data, 'media_detail_sn = '.$media_detail_sn.' AND create_member_sn = '.$member_sn);
    }
}