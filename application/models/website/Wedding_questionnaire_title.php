<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wedding_questionnaire_title extends CI_Model {

    public $limit = 10; // query limit setting
    public $until = 0; // last query sn

    public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
    }
    /*
    */
    public function get_row( $wedding_questionnaire_title_sn = 0 )
    {
        $this->db->select('*');
        $this->db->from('ij_wedding_questionnaire_title');
        $this->db->where('ij_wedding_questionnaire_title_sn', $wedding_questionnaire_title_sn);
        $this->db->where('status', 1);
        $query = $this->db->get();
        return $result = $query->row_array();
    }
    /*
    */
    public function get_result_by_questionnaire( $wedding_questionnaire_sn = 0 )
    {
        $this->db->select('*');
        $this->db->from('ij_wedding_questionnaire_title');
        $this->db->where('wedding_questionnaire_sn', $wedding_questionnaire_sn);
        $this->db->where('status', 1);
        $this->db->order_by('wedding_questionnaire_title_sn', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
    /*
    * Update 
    */
    public function update_row( $wedding_questionnaire_title_sn = 0, $member_sn=0, $data_update = array() )
    {
        return  $this->db->update('ij_wedding_questionnaire_title', $data_update, 'wedding_questionnaire_title_sn = '.$wedding_questionnaire_title_sn.' AND create_member_sn = '. $member_sn);
    }
    /*
    * insert 
    */
    public function insert_row( $data_insert = array())
    {
        if($this->db->insert('ij_wedding_questionnaire_title', $data_insert))
        {
            return $this->db->insert_id();
        }
        return false;
    }
}