<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<link href="public/metronic/global/plugins/carousel-owl-carousel/OwlCarousel2/assets/owl.carousel.css" rel="stylesheet">
  <div style="text-align:center;">
  	<?=$about?>
  	<button onclick="location.href='/';" style="border-radius: 6px !important;" class="btn btn-lg btn-danger"> &nbsp;回囍市集首頁&nbsp; </button>
  </div>
      <div class="top_image-box clearfix owl-probox4" style="position: relative;">
        <div>
          <div id='left_scrolls'><i class="fa fa-angle-left" aria-hidden="true"></i></div>
          <div id='right_scrolls'><i class="fa fa-angle-right" aria-hidden="true"></i></div>
        </div>
        <div class="row-s-15 owl-carousel owl-theme" id="probox4">
<?php if(@$promo_category){ foreach($promo_category as $Item){?>
          <div class="top_image-box-item col-md-3 col-sm-6 col-xs-6 margin-bottom-10" style="border:0;">
            <a class="ftr_gallery-img" href='shop/shopCatalog/<?php echo $Item['category_sn'];?>'><img class="product-main-image" onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['main_picture_save_dir'];?>" alt="<?php echo $Item['main_picture_desc'];?>"></a>
            <div class="ftr_gallery-txt-box">
              <h4><a href="shop/shopCatalog/<?php echo $Item['category_sn'];?>"><?php echo $Item['category_name'];?></a></h4>
            <p class="text-left" style="height:60px; overflow:hidden; text-align: left !important;"><?php echo $this->ijw->truncate(strip_tags($Item['category_sub_name']),60);?></p>
            </div>
          </div>
  <?php }}?>
        </div>
      </div>
