<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 官網合作夥伴
*/
?>
<div id="section-contactus" class="col-md-12" >
<div class="ab_top_head">Home>聯絡我們</div>
  <div class="container">
  <div id="contact_us_main" style="margin-top: 2%;">
	<div class="contactus_contant_gray col-md-10">
	  <h4 class="col-md-11 pull-left">聯絡我們</h4>
	  <div class="col-md-10 contact_leftpart_text">
			<p>1.為了提供您最快速的服務，請您先瀏覽相關說明。若仍然無法回答您的問題請留下您的問題，我們將竭誠為您服務。</p>
			<p>2.若您已是囍市集會員，有任何相關問題請至<a href="member/callcenter">客服中心</a>詢問。</p>
		</div>
	</div>

	<div class="contactus_contant_gray col-md-8">
      <?php $this->load->view('www/general/flash_error');?>
        <form method="post" name="ticket_form" class="form-horizontal" role="form" action="home/addCallcenter">
        <div class="margin-top-20 margin-bottom-20">
  	  <table class="table noborder">
          <tbody>
          <tr>
            <td>分類</td>
            <td>
              <select name="ccapply_detail_code" id="ccapply_detail_code" required class="form-control">
                <option value="">請選擇</option>
                <?php foreach($ccapply_detail_code as $_Item){?>
                <option value="<?php echo $_Item['ccapply_detail_code']?>"><?php echo $_Item['ccapply_detail_code_name']?></option>
                <?php }?>
              </select>
            </td>
          </tr>
          <tr>
            <td>主題</td>
            <td><input size="27" type="text" required name="question_subject" class="form-control"></td>
          </tr>
          <tr>
            <td>姓名</td>
            <td><input size="27" type="text" required class="form-control" name="first_name" id="first_name" value=""></td>
          </tr>
          <tr>
            <td>聯絡電話</td>
            <td><input size="27" type="phone" required name="phone" id="phone" class="form-control" value=""></td>
          </tr>
          <tr>
            <td>電子信箱</td>
            <td><input size="27" type="email" required name="email" id="email" class="form-control" value=""></td>
          </tr>
          <tr>
            <td>留言內容</td>
            <td><textarea name="question_description" id="question_description" required rows="8" cols="65" class="form-control"></textarea></td>
          </tr>
          <tr>
            <td>驗證碼</td>
            <td>
              <input size="27" type="text" style="width:100px;display:inline;height:40px;vertical-align: bottom;" maxlength="4" required name="rycode" id="rycode" class="form-control" value="">
              <?php echo $_captcha_img;?>
            </td>
          </tr>
          <tr><td colspan="2" align="center">
              <input type="submit" style="width:100px;" value="確定" class="btn btn-black">
              <input type="hidden" name="ccapply_code" value="5">
          </td></tr>
      </tbody></table>
	</div></form>
	</div>
	</div>


  </div>
</div>