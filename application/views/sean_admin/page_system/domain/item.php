<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>產業屬性 <small>屬性設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        系統設定
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/main/domain">產業屬性設定</a>
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         產業屬性設定
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">產業屬性設定</span>
            </div>
            <div class="tools">
              <a href="admin/main/domain"><i class="fa fa-list"></i> <?php echo $this->lang->line("back_list");?></a>
            </div>
          </div>
          <div class="portlet-body form">
           <?php $this->load->view('sean_admin/general/flash_error');?>
            <form class="form-horizontal" role="form" action="<?php echo $_action_link;?>">
              <div class="form-body">
                <?php
									$_form_one= new sean_form_general();
									$em_columns = array(
										  'domain_name'=>array('header'=>"產業屬性名稱",'type'=>'textbox', 'required'=>'required', 'required_message'=>"",'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'maxlength'=>'64', 'default'=>'', 'source'=>'', 'rows'=>'15', 'cols'=>'50'),
										 // 'sort_order'=>array('header'=>"排序",'type'=>'textbox', 'required'=>'required', 'required_message'=>"",'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'maxlength'=>'10', 'default'=>'99', 'source'=>'', 'rows'=>'15', 'cols'=>'50'),
										  'status'=>array('header'=>"是否啟用",'type'=>'status', 'required'=>'required', 'required_message'=>"",'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'maxlength'=>'', 'default'=>'', 'source'=>'', 'rows'=>'15', 'cols'=>'50'),
									);
								 		  if(isset($_result01) && is_array($_result01))
										 	{
										 		$_form_one->add_item($em_columns,$_result01);

											}else{
												$_form_one->add_item($em_columns);
											}

								?>

              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                    <a href="<?php echo $_father_link;?>"><button type="button" class="btn default">Cancel</button></a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->