<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Page 主架構
*/
 
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <?php echo $_page_title;?>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->