<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script src="public/metronic/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<!--script src="public/metronic/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script-->
<script type="text/javascript" src="public/metronic/global/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script src="public/metronic/admin/pages/scripts/components-form-tools.js"></script>
<script src="public/metronic/admin/pages/scripts/portfolio.js"></script>
<script src="public/metronic/admin/pages/scripts/components-pickers.js"></script>

<script>
jQuery(document).ready(function() {       
   	Metronic.init(); // init metronic core components
	  Layout.init(); // init current layout
    //ComponentsFormTools.init();
    $('.fancybox').fancybox();
    Portfolio.init();
    ComponentsPickers.init();
    // form init & event listener
    /*
    if(jQuery('input[name="productType"]:checked').val()=='ewedding'){
        jQuery('#tabImages').show();
    }else{
        jQuery('#tabImages').hide();
    }
    */
    /*
    if(jQuery('input[name="additional-purchase"]:checked').val()=='Y'){
        jQuery('#tabAdditionalPurchase').show();
    }else{
        jQuery('#tabAdditionalPurchase').hide();
    }
    */
	var colorEnable = jQuery('input[name="template-Color[]"]:checked');
	jQuery('select[name="imgColorSelect"]').html('');
	for(var i=0; i<colorEnable.length; i++){
    	jQuery('.trImgColor[rel="'+jQuery(colorEnable[i]).val()+'"]').show();
    	var tmp = jQuery('select[name="imgColorSelect"]').html();
    	jQuery('select[name="imgColorSelect"]').html(tmp + '<option value="'+jQuery(colorEnable[i]).val()+'">' +jQuery(colorEnable[i]).val()+ '</option>');
	}
	var packageEnable = jQuery('input[name="template-Package[]"]:checked');
	for(var i=0; i<packageEnable.length; i++){
    	jQuery('.trPackage[rel="'+jQuery(packageEnable[i]).val()+'"]').show();
	}

    jQuery('input[name="productType"]').change(function(){
        if(jQuery('input[name="productType"]:checked').val()=='ewedding'){
            jQuery('#tabImages').show();
        }else{
            jQuery('#tabImages').hide();
        }
    });
    /*
    jQuery('input[name="additional-purchase"]').change(function(){
        if(jQuery('input[name="additional-purchase"]:checked').val()=='Y'){
            jQuery('#tabAdditionalPurchase').show();
        }else{
            jQuery('#tabAdditionalPurchase').hide();
        }
    });*/
    jQuery('input[name="template-Color[]"]').change(function(){
    	jQuery('.trImgColor').hide();
    	jQuery('select[name="imgColorSelect"]').html('');
    	var colorEnable = jQuery('input[name="template-Color[]"]:checked');
    	for(var i=0; i<colorEnable.length; i++){
	    	jQuery('.trImgColor[rel="'+jQuery(colorEnable[i]).val()+'"]').show();
    		var tmp = jQuery('select[name="imgColorSelect"]').html();
    		jQuery('select[name="imgColorSelect"]').html(tmp + '<option value="'+jQuery(colorEnable[i]).val()+'">' +jQuery(colorEnable[i]).val()+ '</option>');
    	}
    });
    jQuery('input[name="template-Package[]"]').change(function(){
    	jQuery('.trPackage').hide();
        if(jQuery('input[name="template-Package[]"]:checked').length > 0){
            var packageEnable = jQuery('input[name="template-Package[]"]:checked');
            for(var i=0; i<packageEnable.length; i++){
                jQuery('.trPackage[rel="'+jQuery(packageEnable[i]).val()+'"]').show();
            }
        }else{
            jQuery('.trPackage[rel="原始"]').show();
        }
    });
    jQuery('input[name="Item[pricing_method]"]').change(function(){
        var id=jQuery('input[name="Item[pricing_method]"]:checked').val();
				setting_price(id);
    });
		function setting_price(id){
		        if(id=='2'){
			        jQuery('#setting_price_1').hide();
			        jQuery('#setting_price_2').show();
		        }else{
			        jQuery('#setting_price_2').hide();
			        jQuery('#setting_price_1').show();
		        }
		}
    var interval_value =<?php echo(@$price_tier[0]['interval_value'])?@$price_tier[0]['interval_value']:'0'?>;
    jQuery('input[name="interval_value"]').change(function(){
    	interval_value = parseInt(jQuery('input[name="interval_value"]').val()); //區間單位
    	var start_qty = parseInt(jQuery('input[name="PriceTier[0][start_qty]"]').val());
    	end_qty = start_qty+interval_value-1;
    	jQuery('input[name="PriceTier[0][end_qty]"]').val(end_qty);
		});
		var area_count=<?php echo(count($price_tier))?count($price_tier):'1'?>;
    jQuery('#btn_area_add').click(function(){
    		last_area_count = area_count-1;
    		end_qty = parseInt(jQuery('input[name="PriceTier['+last_area_count+'][end_qty]"]').val());
    		start_qty=end_qty+1;
      console.log(interval_value);
    	if(interval_value > 0){
    		end_qty=start_qty+interval_value-1;
    	}else{
    		end_qty = '';
    	}
    	var html=jQuery('#sample_price_2').html().replace(/Count/g,'PriceTier['+area_count+++']').replace(/start_value/g,start_qty).replace(/end_value/g,end_qty);
      jQuery('#setting_price_2').append(html);
    });
		setting_price(<?php echo @$Item['pricing_method'];?>); //default pricing_method
		setting_spec(<?php echo (@$Item['unispec_flag'])?'1':'0';?>); //default unispec_flag
		GetColor(<?php echo @$Item['product_template_sn'];?>);
<?php if(@$channel_name){?>
	ChgStoreName('<?php echo @$channel_name;?>','<?php echo @$Item['default_root_category_sn'];?>');
	ExtChgStoreName('<?php echo @$channel_name;?>','<?php echo @$Item['product_sn'];?>');
<?php }?>
});
	function ChgStoreName(C,D){ //變動館別、帶入值
		$('#Categorys option').remove();$('#Categorys').append('<option>Loading...</option>');
		$.get('/admin/product/ChgStoreName/'+C+'/'+D,function(data){$('#Categorys option').remove();$('#Categorys').append(data);});;
	}
	function ExtChgStoreName(C,D){ //變動館別、帶入值
		$('#Ext_Categorys option').remove();$('#Ext_Categorys').append('<option>Loading...</option>');
		$.get('/admin/product/ChgExtStoreName/'+C+'/'+D,function(data){$('#Ext_Categorys option').remove();$('#Ext_Categorys').append(data);getAttr();getPackage();   });;
	}
	function GetCategorys(){
		var C=[];
		C[0]=$('#Categorys').val();
		var j=1;
		$("#Ext_Categorys :selected").map(function(i, el) {
			if($(el).val()!=C[0]){
		    C[j]=$(el).val();
		    j++;
		  }
		});
		return C;
	}
	$('#Ext_Categorys').change(function(event) {
		var C=GetCategorys();
		//console.log(C);
		//event.preventDefault();
		var D=$('#product_sn').val();
	  var AttributeList = $('#AttributeList');
		if(C!=0 && C !='' && D!=0 && D !=''){
	  $.post('admin/product/AddAttribute', {
	          categorys: C,
	          product_sn: D
	      }, function(data) {
					showAttribute(data);
	      }, 'json');
	  $.post('admin/product/GetPackage', {
	          categorys: C,
	          product_sn: D
	      }, function(data) {
					showPackage(data);
	      }, 'json');
	  }
	});
	$('#Categorys').change(function(event) {
		var C=GetCategorys();
		//console.log(C);
		//event.preventDefault();
		var D=$('#product_sn').val();
	  var AttributeList = $('#AttributeList');
		if(C!=0 && C !='' && D!=0 && D !=''){
	  $.post('admin/product/AddAttribute', {
	          categorys: C,
	          product_sn: D
	      }, function(data) {
					showAttribute(data);
	      }, 'json');
	  $.post('admin/product/GetPackage', {
	          categorys: C,
	          product_sn: D
	      }, function(data) {
					showPackage(data);
	      }, 'json');
	  }
	});	
	function showPackage(data){
	  	var PackageList1 = $('#PackageList1');
	  	var PackageList2 = $('#PackageList2'); //單一售價
	    var html = '';
	    var html_price = '';
	    for (var x = 0; x < data.length; x++) {
	    	var product_fix_price_sn=data[x].product_fix_price_sn;
	    	var product_package_config_sn=data[x].product_package_config_sn;
	    	var product_package_name=data[x].product_package_name;
	    	var fixed_price=(data[x].fixed_price)?data[x].fixed_price:'';
	    	var price=(data[x].price)?data[x].price:'';
	    	var status=(data[x].status=='1')?'checked':'';
	        html+='<label class="checkbox-inline">';
	        	html+='<input type="checkbox" class="PackageCheck" product_package_config_sn="'+product_package_config_sn+'" name="Package['+x+'][status]" value="1" '+status+'>'+product_package_name;
	        html+='</label>';
	        if(data[x].status=='1'){
	          html_price+='<div class="form-group trPackage" rel="'+product_package_name+'">';
	            html_price+='<label class="col-md-2 control-label">'+product_package_name+'</label>';
	            html_price+='<div class="col-md-3">';
	              html_price+='<input type="number" min="0" step="1" class="form-control" name="Package['+x+'][fixed_price]" value="'+fixed_price+'">';
	            html_price+='</div>';
	            html_price+='<div class="col-md-3">';
	              html_price+='<input type="number" min="0" step="1" class="form-control" name="Package['+x+'][price]" value="'+price+'">';
				      html_price += '<input type="hidden" name="Package['+x+'][product_fix_price_sn]" value="'+product_fix_price_sn +'" >';
	            html_price+='</div>';
	          html_price+='</div>';
        	}
	    }
			//console.log(html_price);
	    PackageList1.html(html);
	    PackageList2.html(html_price);

		$('.PackageCheck').click(function(event) {
			var status=($(this).filter(':checked').val())?'1':'0';
			//有勾選款型則將庫存管理的規格設定為多重，且為虛擬商品
			if(status=='1'){
				$('#open_preorder_flag1').prop('checked',true);  //預購
				$('#product_type_2').prop('checked',true);       //虛擬
				$('#unispec_flag_0').prop('checked',true);  //多重規格
				$.uniform.update();
				//console.log($('#product_type_2').prop('checked'));
				//$('#pricing_method_2').get(0).checked = true;
				//document.getElementById('pricing_method_2').checked = true;
				//$('#pricing_method_2')[0].checked = true;
			}
		var C=GetCategorys();
  		$.post('admin/product/AddPackage', {
          product_package_config_sn: $(this).attr('product_package_config_sn'),
          categorys: C,
          status: status,
          product_sn: <?php echo @$Item['product_sn']; ?>
      }, function(data) {
      	if(data){
		//console.log(data);
					showPackage(data);
				}
      }, 'json');			
		});
	}

function getPackage(){
	var C=GetCategorys();
	//console.log(C);
  $.post('admin/product/GetPackage', {
          categorys: C,
          product_sn: <?php echo @$Item['product_sn']; ?>
      }, function(data) {
				showPackage(data);
      }, 'json');
}
function getAttr(){
	var C=GetCategorys();
	//console.log(C);
  $.post('admin/product/AddAttribute', {
          categorys: C,
          product_sn: <?php echo @$Item['product_sn']; ?>
      }, function(data) {
				showAttribute(data);
      }, 'json');
}

function showAttribute(data){
  	var AttributeList = $('#AttributeList');
    var html = '';
    var z = 0;
    for (var x = 0; x < data.length; x++) {
    	var attribute_values=data[x].attribute_values;
				html += '<tr>';
				html += '<td><label class="control-label">'+data[x].attribute_name+'</label></td>';
				html += '<td>';
				html += '<div class="checkbox-list">';
				html += '<label class="checkbox-inline" style="float:right;">';
				//html += '<a data-toggle="modal" href="#addAttrValue" attribute_sn="'+ data[x].attribute_sn +'" attribute_name="'+ data[x].attribute_name +'" class="addAttr btn green btn-xs">新增屬性值</a>';
				html += '</label>';
				    for (var y = 0; y < attribute_values.length; y++) {
				    	var ifchecked=(attribute_values[y].status=='1')?'checked':'';
							html += '<label class="checkbox-inline">';
							html += '<input class="checkbox-list" name="attribute['+z+'][status]" type="checkbox" class="form-control" value="1" '+ifchecked+' >&nbsp;'+attribute_values[y].attribute_value_name;
				      html += '<input type="hidden" name="attribute['+z+'][attribute_value_sn]" value="'+ attribute_values[y].attribute_value_sn +'" >';
				      html += '<input type="hidden" name="attribute['+z+'][attribute_sn]" value="'+ data[x].attribute_sn +'" >';
				      html += '<input type="hidden" name="attribute['+z+'][product_sn]" value="'+ data[x].product_sn +'" >';
							html += '</label>';
							z++;
						}
				html += '</div>';
				html += '</td>';
				html += '</tr>';
	//console.log(html);
    }
    if(!html){ 
    	html='<span class="help-block"> (請先點選主分類或按住Ctrl點選擴展分類) </span>';
    }else{
        html += '<span class="help-block"> (若需新增分類屬性請點選主分類或按住Ctrl點選擴展分類) </span>';
    }
    AttributeList.html(html);
		$('.addAttr').click(function(event) {
			$('#attribute_sn').val($(this).attr('attribute_sn'));
			$('#AttrName').text($(this).attr('attribute_name'));
		});    
}
		$('#AddAttributeValue').click(function(event) {
  		$.post('admin/product/AddAttributeValue', {
          attribute_value_name: $('#AttributeValue').val(),
          attribute_sn: $('#attribute_sn').val(),
          product_sn: <?php echo @$Item['product_sn']; ?>
      }, function(data) {
      	if(data){
      		$('#addAttrValue').hide();
					getAttr();
				}
      }, 'json');			
		});
    $(document).on('click', '.deleteItem', function(event) {
        var button = $(this);
        	var r = confirm('是否刪除此張圖檔?');
            if(r)
            {
						    $.post('/admin/admin/ajaxdel/', {dtable: '<?php echo base64_encode('ij_product_gallery');?>',dfield:'<?php echo base64_encode('product_gallery_sn');?>',did:button.attr('ItemId')}, function(data){
                	if (data){
                		//button.parents().filter('div#image'+button.attr('ItemId')).remove();
                		$('#image'+button.attr('ItemId')).remove();
                		$('#file'+button.attr('ItemId')).show();
                		$('#image_alt'+button.attr('ItemId')).val('');
                		$('#sort_order'+button.attr('ItemId')).val('9999');
                	}else{
                		alert('刪除失敗');
                	}
						    });
            }
    });

jQuery(document).ready(function() {
		$('#product_type_2').click(function(event) { //虛擬商品＝開放預購
			//event.preventDefault();
			$('#open_preorder_flag1').prop('checked',true);
			$("input.checkbox-list[name='deliverys[]'][value='3']").prop("checked", true);			
			$.uniform.update();
			console.log($("input.checkbox-list[name='deliverys[]'][value='3']").prop("checked"));
      //jQuery('#open_preorder_flag1').prop('checked',true).prop('disabled', true);
		});
});

	/*(function ($) {
	    var _oldShow = $.fn.show;
	
	    $.fn.show = function (speed, oldCallback) {
	        return $(this).each(function () {
	            var obj = $(this),
	                newCallback = function () {
	                    if ($.isFunction(oldCallback)) {
	                        oldCallback.apply(obj);
	                    }
	
	                    obj.trigger('afterShow');
	                };
	
	            obj.trigger('beforeShow');
	
	            _oldShow.apply(obj, [speed, newCallback]);
	        });
	    };
	})(jQuery);*/
 		$('#Addon').click(function(event) { //點新增加購
			var C=$('#product_addon_sn').val();
			if(C){
				if(C!='<?php echo $Item['product_sn']; ?>'){
					$.get('/admin/product/getProductData/'+C,function(data){
						if(!data){
							 //console.log($('#Addon').attr('href'));
							 //$('#Addon').attr('href','#');
			         alert('查無此商品編號，請重新確認！');
			         //$('#product_addon_sn').focus();
    					//$('#Addon').unbind('click');
							$('#addon_product_name').text('');
							$('#product_orginal_price').text('');
							$('#basic').modal('hide');
    				  return false;
    				  
						}else{
							//$('#basic').modal('show');
		 					$('#ifedit').val('0');
							$('#addon_product_name').text(data.product_name);
							$('#product_orginal_price').text(data.min_price);
						}
					}, 'json');
				}else{
	         alert('不能加購商品自己喔！');
	         $('#product_addon_sn').focus();
         	return false;
				}
			}else{
         alert('請先輸入商品編號');
         $('#product_addon_sn').focus();
         return false;
 				 //$('#basic').stop(true,true);
			}
    });

		$('#Add_addon').click(function(event) { //新增加購送出
			if(!$('#addon_price').val()) { alert('請輸入加購價');$('#addon_price').focus();return false;}
			if(!$('#addon_limit').val()) { alert('請輸入加購數量限制');$('#addon_limit').focus();return false;}
  		$.post('admin/product/AddAddon', {
          ifedit: $('#ifedit').val(),
          addon_price: $('#addon_price').val(),
          addon_limit: $('#addon_limit').val(),
          product_orginal_price: $('#product_orginal_price').text(),
          addon_log_sn: $('#addon_log_sn').val(),
          product_name: $('#addon_product_name').text(),
          associated_product_sn: ($('#product_addon_sn').val())?$('#product_addon_sn').val():$('#associated_product_sn').val(), //加購商品編號
          product_sn: <?php echo $Item['product_sn']; ?> //所屬商品編號
      }, function(data) {
      	if(data){
      		$('#basic').hide();
      		if(data.error){
      			alert(data.error);
      		}else{
	          var html = '';
	            html += '<tr class="odd gradeX" id="Addon_tr'+data.addon_log_sn+'">';
	              html += '<td>'+data.associated_product_sn+'</td>';
	              html += '<td>'+data.product_name+'</td>';
	              html += '<td class="center">'+data.product_orginal_price+'</td>';
	              html += '<td class="center">'+data.addon_price+'</td>';
	              html += '<td class="center">'+data.addon_limitation+'</td>';
	              html += '<td class="center">'+data.unispec_qty_in_stock+' &nbsp;<a href="admin/product/inventory/item?product_sn='+data.associated_product_sn+'" target="_blank"><i class="fa fa-edit"></i>管理</a></td>';
	              html += '<td class="center">';
	    						html += '<input type="hidden" id="addon_log_sn" name="addon_log_sn" value="">';
	                html += '<a data-toggle="modal" class="EditAddon" addon_log_sn="'+data.addon_log_sn+'" href="#"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;';
	                html += '<a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>';
	              html += '</td>';
	            html += '</tr>';
					//console.log(data.ifedit);
					//console.log(data.addon_log_sn);
	          if (data.ifedit){
	      			$('#Addon_tr'+data.addon_log_sn).remove();
	      			$('#Addon_list').append(html);
	      			$('#associated_product_sn').val('');//清空
	          }else{
	      			$('#Addon_list').append(html);
	      		}
					}
				}
      }, 'json');			
		});
		
  $(document).on('click', '.EditAddon', function() {  //Edit
		 $('#associated_product_sn').val($(this).parent().prev().prev().prev().prev().prev().prev().text());
		 $('#addon_product_name').text($(this).parent().prev().prev().prev().prev().prev().text());
		 $('#product_orginal_price').text($(this).parent().prev().prev().prev().prev().text());
		 $('#addon_price').val($(this).parent().prev().prev().prev().text());
		 $('#addon_limit').val($(this).parent().prev().prev().text());
     $('#addon_log_sn').val($(this).attr('addon_log_sn'));
		 $('#ifedit').val('1');
	   //$('#basic').show();
	});
	
    $(document).on('click', '.deleteAddon', function() {
        var button = $(this);
        	var r = confirm('是否刪除此加購?');
            if(r)
            {
						    $.post('/admin/admin/ajaxdel/', {dtable: '<?php echo base64_encode('ij_addon_log');?>',dfield:'<?php echo base64_encode('addon_log_sn');?>',did:button.attr('ItemId')}, function(data){
                	if (data){
                		button.parents().parents().filter('tr').remove();
                	}else{
                		alert('刪除失敗');
                	}
						    });
            }
    });	

	jQuery(".form_datetime").datetimepicker({
        autoclose: true,
        isRTL: Metronic.isRTL(),
        format: "yyyy-mm-dd hh:ii",
        pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left")
    });
    //單一或多重規格
    jQuery('input[name="Item[unispec_flag]"]').change(function(){
        var unispec_flag=jQuery('input[name="Item[unispec_flag]"]:checked').val();
				setting_spec(unispec_flag);
    });
		function setting_spec(unispec_flag){
        if(unispec_flag=='1'){
	        jQuery('#multispec').hide();
	        jQuery('#unispec').show();
        }else{
	        jQuery('#unispec').hide();
	        jQuery('#multispec').show();
        }
		}
		var multispec_count=<?php echo(count(@$mutispecs))?count(@$mutispecs):'1'?>;
    jQuery('#btn_spec_add').click(function(){
    	last_multispec_count = multispec_count-1;
    	qty_in_stock = parseInt(jQuery('input[name="Mutispec['+last_multispec_count+'][qty_in_stock]"]').val());
    	safe_qty_in_stock = parseInt(jQuery('input[name="Mutispec['+last_multispec_count+'][safe_qty_in_stock]"]').val());
    	var html=jQuery('#sample_spec').html().replace(/Count/g,'Mutispec['+multispec_count+++']').replace(/safeqty/g,safe_qty_in_stock).replace(/lastqty/g,qty_in_stock);
      jQuery('#multispec').append(html);
    });
	$('.template_click').click(function(event) { //點版型
		GetColor($(this).val());
	});
	function GetColor(product_template_sn){
  		$.post('admin/product/GetColor', {
          product_template_sn: product_template_sn,
          product_sn: <?php echo @$Item['product_sn']; ?>
      }, function(data) {
      	if(data){
			    //for (var x = 0; x < data.length; x++) {
						showPhoto(data);
					//}
				}
      }, 'json');
	}
	function showPhoto(data){
	  	var PhotoList = $('#PhotoList');
	  	var html='';
	  for (var z = 0; z < data.length; z++) {
	  	var ifchecked = (data[z].status)?'checked':'';
	    html += '<tr class="trImgColor" id="template_color'+data[z].associated_template_color_relation_sn+'" rel="'+data[z].color_name+'"><td><input type="hidden" name="template_colors['+z+'][sort_order]" value="'+data[z].sort_order+'"><input type="checkbox" name="template_colors['+z+'][website_color_sn]" value="'+data[z].website_color_sn+'" '+ifchecked+' />'+data[z].color_name+'</td>';
			var photos = new Array();
	    photos=data[z].photos;
			if(photos){
		    for (var x = 0; x < photos.length; x++) {
		    	//var template_color_relation_sn=data[x].template_color_relation_sn;
		    	//var website_color_sn=data[x].website_color_sn;
		    	//var ifchecked=(data[x].status=='1')?'checked':'';
		      html += '<td align="center" id="TemplateItem'+photos[x].template_picture_sn+'"><a class="fancybox" rel="photos'+data[z].associated_template_color_relation_sn+'" title="'+photos[x].image_alt+'" href="public/uploads/'+photos[x].image_path+'"><img src="public/uploads/'+photos[x].image_path+'" style="width:120px;" alt="'+photos[x].image_alt+'" title="'+photos[x].image_alt+'"></a></td>';
		    }
		  }else{
				var x = 0;
		  }
	    for (var y = x; y < 5; y++) { //補空缺
        html += '<td></td>';
	    }
	    html += '</tr>';
			//console.log(html);
		}
		if (html==''){
			PhotoList.html('該版型尚未有顏色與圖片');
		}else{
	  	PhotoList.html(html);
		}
	}
 </script>

