<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dealer_table extends CI_Model {

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('sean_models/Sean_db_tools');
		$this->load->model('sean_models/Sean_db_toolss');


		/* Begin 修改  */
		$this->_ct_name="經銷商";// 功能名稱
		$this->_url="Member/dealerTable/";//列表功能url
		$this->_url_item="Member/dealerTable_item/";//處理動作 url
		$this->_table="ij_dealer";//table name
		$this->_id="dealer_sn";//table key
		$this->_table_field=array(
					 "dealer_status" => "帳號狀態",
					 "dealer_name" => "店名",
					 "dealer_short_name" => "簡稱",

					 "dealer_price_discount" => "經銷商分級",
					 "partnership_start_date" => "有效日期",
					 "partnership_end_date" => "有效日期",

					 "website_url" => "官方網站",
					 "office_tel" => "電話",
					 "office_fax" => "傳真",
					 "addr_city_code" => "city",
					 "addr_town_code" => "town",
					 "addr1" => "住址",
					 "dealer_chairman" => "公司負責人",
					 "dealer_company_title" => "公司抬頭",
					 "dealer_tax_id" => "統一編號",
					 "associated_sales_member_sn" => "客服代表",
					 "main_domain_sn" => "主要屬性",

				);

				/*

_REQUEST["domain_sn"]

Array
(
    [0] => 3
)


_REQUEST["confirm-password"]	123456

_REQUEST["account_name"]	戶名
_REQUEST["bank_name"]	銀行
_REQUEST["branch_name"]	分行
_REQUEST["bank_account"]	帳號


_REQUEST["status_1"]	1
_REQUEST["contact_name_1"]	聯絡人姓名1
_REQUEST["office_tel_1"]	電話1
_REQUEST["office_tel_ext_1"]	分機1
_REQUEST["cell_1"]	手機1
_REQUEST["email_1"]	Email 1
_REQUEST["memo_1"]	備註1


_REQUEST["status_2"]	1
_REQUEST["contact_name_2"]	聯絡人姓名2
_REQUEST["office_tel_2"]	電話 2
_REQUEST["office_tel_ext_2"]	分機2
_REQUEST["cell_2"]	手機2
_REQUEST["email_2"]	Email2
_REQUEST["memo_2"]	備註

_REQUEST["status_3"]	1
_REQUEST["contact_name_3"]	聯絡人姓名 3
_REQUEST["office_tel_3"]	電話 3
_REQUEST["office_tel_ext_3"]	分機3
_REQUEST["cell_3"]	手機3
_REQUEST["email_3"]	Email 3
_REQUEST["memo_3"]	備註


_REQUEST[""]	2


*/

		/* End 修改  */

    }

    //取得列表內容
    public function table_value_list( $_where,$_id = 0)
	{
		$_table_key=$this->_id;
	    $page_data['_body_result']="";
		//判斷是否有帶入特殊查詢
		if(strlen($_where)==0)
		{
			$_where="  order by apply_date asc";

		}
		// 取得列表
		$_v=" ".$this->_table.".*, concat(first_name,'',last_name) as ijtmp_name ";
		$_result01=$this->Sean_db_tools->db_get_max_record($_v,$this->_table,$_where);

		/* 列表標頭名稱 */
		$page_data['_table_thead_name']=array("姓名","Email","縣市","鄉鎮市區","住址","電話","結婚日期","關聯廠商","帳號狀態","功能");
		$page_data['_table_thead_field']=array("ijtmp_name","email","addr_state","addr_city","addr1","tel","wedding_date","member_level_type","member_status");


		/* 類表內容 */
		if(is_array($_result01))
		{
                 		$_i=0;
                 		foreach($_result01 as $key => $value)
                    	{


			                    $_body_result='';
						/* Begin 修改  */
			                    $i_key=0;
								foreach(	$page_data['_table_thead_field'] as $key => $_value)
								{
									$page_data['_body_result'][$_i][$i_key]=array("_value"=>$value->$_value,"_class"=>"");
			                    	$i_key++;
								}


						/* End 修改  */
									$page_data['_body_result'][$_i][$i_key]=array("_value"=>'
			                    		 		<a href="'.$this->init_control.$this->_url_item.'edit/'.$value->$_table_key.'"><i class="fa fa-edit"></i>編輯</a>&nbsp;&nbsp;
						                      <a href="'.$this->init_control.$this->_url_item.'delete/'.$value->$_table_key.'"><i class="fa fa-circle-o-notch"></i>重發驗證信</a>&nbsp;&nbsp;
											   <a href="'.$this->init_control.$this->_url_item.'delete/'.$value->$_table_key.'"><i class="fa fa-lightbulb-o"></i>忘記密碼&nbsp;&nbsp;
						                  	',"_class"=>'class="center"');
                				$_i++;


                			}

    				}
			return $page_data;

    }

	public function action_edit( $_where = "") {

		$_result="";
		$_result=$this->Sean_db_tools->db_get_max_record("*",$this->_table,$_where);


		return $_result;

	}

	public function action_insert() {

			$_result="";

			foreach($this->_table_field as $key => $value)
			{
					if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
					$_data[$key]=$this->input->get_post($key);
			}





			//$_data["email"]=$_data["user_name"];
			//$_data["member_level_type"]=1;
			//$_data["member_level"]=1;
			//$_data["system_user_flag"]=0;

			//basic insert
			//$_data["create_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			//$_data["create_date"]=date("Y-m-d H:i:s");
			//basic update and insert
			//$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			//$_data["last_time_update"]=date("Y-m-d H:i:s");

		 $page_data["_table"]=$this->_table;

		  //$_result=$this->Sean_db_tools->db_insert_record($_data,$this->_table);

			$_result=$this->Sean_db_toolss->db_insert_record($_data,$page_data);
			$_data="";
			if($_result > 0 )
		  {
				  //init_bank_info_relation

					if(strlen($this->input->get_post("account_name")) > 0 )
					$_data["account_name"]=$this->input->get_post("account_name");

					if(strlen($this->input->get_post("bank_name")) > 0 )
					$_data["bank_name"]=$this->input->get_post("bank_name");

					if(strlen($this->input->get_post("branch_name")) > 0 )
					$_data["branch_name"]=$this->input->get_post("branch_name");

					if(strlen($this->input->get_post("bank_account")) > 0 )
					$_data["bank_account"]=$this->input->get_post("bank_account");

				  $_data["associated_dealer_sn"]=$_result;
				  $_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";

				  $_data["create_date"]=date("Y-m-d H:i:s");
				  $_data["status"]=1;

				  $this->insert_bank_info_relation($_data);


				  //init_contact_info_relation

				  for($i=1;$i<=3;$i++)
				  {

				  		$_data="";
				  		if(strlen($this->input->get_post("contact_name_".$i)) > 0 )
							$_data["contact_name"]=$this->input->get_post("contact_name_".$i);

						  if(strlen($this->input->get_post("office_tel_".$i)) > 0 )
							$_data["office_tel"]=$this->input->get_post("office_tel_".$i);

							if(strlen($this->input->get_post("office_tel_ext_".$i)) > 0 )
							$_data["office_tel_ext"]=$this->input->get_post("office_tel_ext_".$i);

							if(strlen($this->input->get_post("cell_".$i)) > 0 )
							$_data["cell"]=$this->input->get_post("cell_".$i);

							if(strlen($this->input->get_post("email_".$i)) > 0 )
							$_data["email"]=$this->input->get_post("email_".$i);

							if(strlen($this->input->get_post("memo_".$i)) > 0 )
							$_data["memo"]=$this->input->get_post("memo_".$i);

						 if(strlen($this->input->get_post("status_".$i)) > 0 )
							$_data["status"]=$this->input->get_post("status_".$i);



						  $_data["associated_dealer_sn"]=$_result;
						  $_data["create_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";

						  $_data["create_date"]=date("Y-m-d H:i:s");


						  $this->insert_contact_info_relation($_data);




				  }
				  //insert dealer member
				  	 	$_data="";
				  		if(strlen($this->input->get_post("password")) > 0 )
							$_data["password"]=md5($this->input->get_post("password"));

							if(strlen($this->input->get_post("user_name")) > 0 )
							$_data["user_name"]=$this->input->get_post("user_name");

							if(strlen($this->input->get_post("dealer_name")) > 0 )
							$_data["first_name"]=$this->input->get_post("dealer_name");


							$_data["member_level_type"]=3;
							$_data["system_user_flag"]=0;
							$_data["member_status"]=1;
							$_data["associated_dealer_sn"]=$_result;
							$_data["email"]=$_data["user_name"];
							$_data["last_name"]="";

									//basic update and insert
								$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
								$_data["last_time_update"]=date("Y-m-d H:i:s");


							$this->insert_member_relation($_data);


			}


		return $_result;

	}

	function insert_member_relation($_data)
	{
					$page_data["_table"]="ij_member";
				  $_result=$this->Sean_db_toolss->db_insert_record($_data,$page_data);
	}
	function update_member_relation($_data,$_where)
	{
					$page_data["_table"]="ij_member";
					$_result=$this->Sean_db_toolss->db_update_record($_data,$_where,$page_data);
	}

	function init_contact_info_relation($_id)
	{
		$page_data["_table"]="ij_contact_info";
		$_where=" where associated_dealer_sn='".$_id."'";
		$_data["status"]=0;
		$_data["last_time_update"]=date("Y-m-d H:i:s");
		$_result=$this->Sean_db_toolss->db_update_record($_data,$_where,$page_data);

	}
	function insert_contact_info_relation($_data)
	{

					$page_data["_table"]="ij_contact_info";
				  $_result=$this->Sean_db_toolss->db_insert_record($_data,$page_data);



	}

	function update_contact_info_relation($_data,$_where)
	{
					$page_data["_table"]="ij_contact_info";
					$_result=$this->Sean_db_toolss->db_update_record($_data,$_where,$page_data);
	}




	function init_bank_info_relation($_id)
	{
		$page_data["_table"]="ij_bank_info";
		$_where=" where associated_dealer_sn='".$_id."'";
		$_data["status"]=0;
		$_data["last_time_update"]=date("Y-m-d H:i:s");
		$_result=$this->Sean_db_toolss->db_update_record($_data,$_where,$page_data);

	}

	function insert_bank_info_relation($_data)
	{

					$page_data["_table"]="ij_bank_info";
				  $_result=$this->Sean_db_toolss->db_insert_record($_data,$page_data);



	}

	function update_bank_info_relation($_data,$_where)
	{
					$page_data["_table"]="ij_bank_info";
					$_result=$this->Sean_db_toolss->db_update_record($_data,$_where,$page_data);
	}

	public function action_update($_where,$_key_id,$_key) {

			$_result="";

			foreach($this->_table_field as $key => $value)
			{
					if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
					$_data[$key]=$this->input->get_post($key);

			}

			$_data["addr_country_code"] ="886";
			$_data["add_state_code"] ="886";
			$_data["addr_country"] ="台灣";
			$_data["addr_state"] ="台灣";
	    //$_data["zipcode"]="";
	    //$_data["addr_city"]="";
	    //$_data["addr_town"]="";

			$this->session->set_flashdata("message",$_data);
			//var_dump($_where);
			//var_dump($_key_id);
			//var_dump($_key);
		$_result=$this->Sean_db_tools->db_update_record($_data,$this->_table,$_where,$_key_id,$_key);
		//echo $this->db->last_query().'<br>';
		$_data="";
		if($_result > 0 )
		  {
				  //insert /update dealer member
				  $_data=array();
		  		if(strlen($this->input->get_post("password")) > 0 )
					$_data["password"]=md5($this->input->get_post("password"));
				if(strlen($this->input->get_post("user_name")) > 0 ){
					$_data["user_name"]=$this->input->get_post("user_name");
					$_data["email"]=$_data["user_name"];
				}
					if(strlen($this->input->get_post("dealer_name")) > 0 )
					$_data["first_name"]=$this->input->get_post("dealer_name");
					//basic update and insert
					$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
					$_data["last_time_update"]=date("Y-m-d H:i:s");

					$_where=" where associated_dealer_sn='".$_result."'";
					$_result_member=$this->Sean_db_tools->db_get_one_record("associated_dealer_sn",'ij_member',$_where);
					if($_result_member){ //判斷是否輸入過對應會員帳號資料
					  $_where=" where associated_dealer_sn='".$_key_id."'";
					//var_dump($_data);
					//exit();
				    $this->update_member_relation($_data,$_where);
				 }else{
				 	//新增
							$_data["member_level_type"]=3;
							$_data["system_user_flag"]=0;
							$_data["member_status"]=1;
							$_data["associated_dealer_sn"]=$_result;
							$_data["last_name"]="";
				  $this->insert_member_relation($_data);
				 }


				  //init_bank_info_relation
					//$this->init_bank_info_relation($_result);

				  $_data=array();
					if(strlen($this->input->get_post("account_name")) > 0 )
					$_data["account_name"]=$this->input->get_post("account_name");

					if(strlen($this->input->get_post("bank_name")) > 0 )
					$_data["bank_name"]=$this->input->get_post("bank_name");

					if(strlen($this->input->get_post("branch_name")) > 0 )
					$_data["branch_name"]=$this->input->get_post("branch_name");

					if(strlen($this->input->get_post("bank_account")) > 0 )
					$_data["bank_account"]=$this->input->get_post("bank_account");

				   $_data["last_time_update"]=date("Y-m-d H:i:s");
				  $_data["status"]=1;

					$_where=" where associated_dealer_sn='".$_key_id."'";
					$_result_bank=$this->Sean_db_tools->db_get_one_record("associated_dealer_sn",'ij_bank_info',$_where);

					if($_result_bank){ //判斷是否輸入過銀行資料
					  $_where=" where associated_dealer_sn='".$_key_id."'";
				    $this->update_bank_info_relation($_data,$_where);
				 }else{
				  $_data["associated_dealer_sn"]=$_key_id;
				  $_data["create_date"]=date("Y-m-d H:i:s");
				  $_data["status"]=1;
				  $this->insert_bank_info_relation($_data);
				 }


				    for($i=1;$i<=3;$i++)
				  {

				  		$_data=array();
				  		if(strlen($this->input->get_post("contact_name_".$i)) > 0 )
							$_data["contact_name"]=$this->input->get_post("contact_name_".$i);

						  if(strlen($this->input->get_post("office_tel_".$i)) > 0 )
							$_data["office_tel"]=$this->input->get_post("office_tel_".$i);

							if(strlen($this->input->get_post("office_tel_ext_".$i)) > 0 )
							$_data["office_tel_ext"]=$this->input->get_post("office_tel_ext_".$i);

							if(strlen($this->input->get_post("cell_".$i)) > 0 )
							$_data["cell"]=$this->input->get_post("cell_".$i);

							if(strlen($this->input->get_post("email_".$i)) > 0 )
							$_data["email"]=$this->input->get_post("email_".$i);

							if(strlen($this->input->get_post("memo_".$i)) > 0 )
							$_data["memo"]=$this->input->get_post("memo_".$i);

						 if(strlen($this->input->get_post("status_".$i)) > 0 )
							$_data["status"]=$this->input->get_post("status_".$i);

						if(strlen($this->input->get_post("contact_sn_".$i)) > 0 )
							$_data["contact_sn"]=$this->input->get_post("contact_sn_".$i);



						  $_data["associated_dealer_sn"]=$_key_id;
						  $_data["create_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";

						  $_data["create_date"]=date("Y-m-d H:i:s");

							if(isset($_data["contact_sn"])  && $_data["contact_sn"] > 0 )
							{
								$_where = " where contact_sn='".$_data["contact_sn"]."'";
								$this->update_contact_info_relation($_data,$_where);
							}else{
						  	$this->insert_contact_info_relation($_data);
							}
				  }
		$domain_sn=$this->input->get_post('domain_sn');
		//var_dump($domain_sn);
		//exit();
		if($domain_sn){
		  $_where=" where associated_dealer_sn='".$_key_id."'"; //先把原本資料庫的status改成 2
			$_data_domain['status']='2';
			$page_data["_table"]="ij_member_domain_relation";
	    $this->Sean_db_toolss->db_update_record($_data_domain,$_where,$page_data);
			foreach($domain_sn as $key=>$Item){
				$_data1['associated_dealer_sn']=$_key_id;
				//$_data1['associated_member_sn']=$Item;
				$_data1['domain_sn']=$Item;
				$_data1['status']='1';
				$_data1=$this->Sean_db_tools->CheckUpdate($_data1,0); //因為沒有create_member_sn

				$_where=" where associated_dealer_sn='".$_key_id."' and domain_sn='".$Item."'";
				$_result_domain=$this->Sean_db_tools->db_get_one_record("associated_dealer_sn",'ij_member_domain_relation',$_where);
				if($_result_domain){ //判斷是否輸入過產業屬性資料
				  $_where=" where associated_dealer_sn='".$_key_id."' and domain_sn='".$Item."'";
			    $this->Sean_db_toolss->db_update_record($_data1,$_where,$page_data);
			 }else{
			  $_result1=$this->Sean_db_toolss->db_insert_record($_data1,$page_data);
			 }
			}
		}



					$this->session->set_flashdata("message",$_data);
			}


		return $_result;

	}

	public function action_delete( $_where) {

		$_result="";
		$page_data["_table"]=$this->_table;
		$_data["dealer_status"]=0;
		$_result=$this->Sean_db_toolss->db_update_record($_data,$_where,$page_data);
		return $_result;

	}

	//取得新排序的值
	public function get_next_sort($_id=0 ) {

		$_result="0";

		$_where=" where ".$this->_id." > 0 order by sort_order desc ";
		$_result=$this->Sean_db_tools->db_get_one_record("sort_order",$this->_table,$_where);

		if(is_array($_result["sort_order"]) )
		$_result=1;
		else
		$_result=$_result["sort_order"]+1;

		return $_result;

	}

	//取得定義參數值
	public function get_define_value($_id)
	{
		if(isset($this->$_id))
		return $this->$_id;
		else
		return "";
	}

	public function get_dealer_list($_table_where){
			//$_result=$this->Sean_db_tools->db_get_max_record("ij_dealer.*,email,user_name,ij_city_ct.city_name,ij_town_ct.town_name","ij_dealer left join ij_member on ij_dealer.dealer_sn=ij_member.associated_dealer_sn left join ij_city_ct on ij_city_ct.city_code=ij_dealer.addr_city_code left join ij_town_ct on ij_town_ct.town_code=ij_dealer.addr_town_code",$_table_where);
			$_result=$this->Sean_db_tools->db_get_max_record("ij_dealer.*,ij_city_ct.city_name,ij_town_ct.town_name","ij_dealer left join ij_city_ct on ij_city_ct.city_code=ij_dealer.addr_city_code left join ij_town_ct on ij_town_ct.town_code=ij_dealer.addr_town_code",$_table_where);
			foreach($_result as $key=>$Item){
				$_where=" where associated_dealer_sn='".$Item->dealer_sn."' and ij_member_domain_relation.status =1 ";
				$domaintypes=$this->Sean_db_tools->db_get_max_record("domain_name","ij_member_domain_relation left join ij_biz_domain on ij_biz_domain.domain_sn=ij_member_domain_relation.domain_sn",$_where);
				if($domaintypes){
					$domain_name=array();
	        foreach($domaintypes as $row){
	        	$domain_name[]=$row->domain_name;
	        }
					$_result[$key]->domaintype=implode('、',$domain_name);
				}else{
					$_result[$key]->domaintype='';
				}
			}
		return $_result;
	}
	public function get_newmember($dealer_sn=0,$member_sn=0){
		$this->db->select("newmember,dealer_bonus");
		$this->db->from("ij_dealer");
		//$this->db->join("ij_member","ij_member.associated_dealer_sn = ij_dealer.dealer_sn","left");
		$this->db->join("ij_dealer_level_config","ij_dealer_level_config.dealer_level_sn = ij_dealer.dealer_price_discount","left");
		//$this->db->where('member_level_type',3);//經銷會員
		$this->db->where('dealer_status',1);//有效
		if($dealer_sn)	$this->db->where('dealer_sn',$dealer_sn);
		//if($member_sn)	$this->db->where('ij_member.member_sn',$member_sn);
		$query = $this->db->get()->row_array();
		if($query){
			return $query;
		}else{
			return false;
		}
	}

function add_img(){
		$this->sourceFolder = FCPATH."public/uploads/dealer/";
		$this->sourceUrl = "public/uploads/dealer/";
		$result = array('success' => 'N', 'msg' => '', 'img' => '');

		$files = $_FILES;
		$originalFile = empty($files['Filedata']['tmp_name'])? $files['Filedata']['name']:$files['Filedata']['tmp_name'];
		$tmp = explode(".", $files['Filedata']['name']);
		$ext = ".".$tmp[COUNT($tmp)-1];
		$thumb = uniqid();

		// Validate the file type
		$fileTypes = array('jpg','jpeg','gif','png'); // File extensions
		$fileParts = pathinfo(empty($files['Filedata']['name'])? $files['upload']['name']:$files['Filedata']['name']);
		$destFile=$this->sourceFolder.$this->dealer_sn.'/'.$thumb.$ext;
		if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
			if(move_uploaded_file($originalFile,$destFile)){

					$result['success'] = 'Y';
					$result['img'] = $this->config->item('base_url').'/'.$this->sourceUrl.'/'.$this->_id.'/'.$thumb.$ext;

			}else{
				$result['msg'] = 'move_uploaded_file faile';
			}
		} else {
			$result['msg'] = 'Invalid file type.';
		}
		return $result;
	}
}