<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- BEGIN FOOTER -->
<?php $this->load->view('admin/general/_item_header.php');?>
<!-- END FOOTER -->      	
      	
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-map-marker font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase"><?php echo $_ct_name_item;?></span>
            </div>
            <div class="tools">
              <a href="admin-ijwedding/location"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
          	
          	
          	<?php $this->load->view('sean_admin/general/flash_error');?>
            <form class="form-horizontal" role="form" action="<?php echo $_action_link;?>">
              <div class="form-body">
                
                
                <?php

									$_form_one= new sean_form_general();
									
											
									$em_columns = array(										
										  'wedding_location_name'=>array('header'=>"地點名稱",'type'=>'textbox', 'required'=>'required', 'required_message'=>"",'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'maxlength'=>'', 'default'=>'', 'source'=>'', 'rows'=>'15', 'cols'=>'50'),  	
										  'addr1'=>array('header'=>"住址",'type'=>'address', 'required'=>'required', 'required_message'=>"",'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'maxlength'=>'', 'default'=>'', 'source'=>'', 'rows'=>'15', 'cols'=>'50'),  	
										  'status'=>array('header'=>"是否啟用",'type'=>'status', 'required'=>'required', 'required_message'=>"",'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'maxlength'=>'', 'default'=>'', 'source'=>'', 'rows'=>'15', 'cols'=>'50'),  	
									);
								 		  if(isset($_result01) && is_array($_result01))
										 	{
										 		$_form_one->add_item($em_columns,$_result01);
												
											}else{
												$_form_one->add_item($em_columns);
											}
									
							?>
               
               
                
                <div class="form-group">
                  <label for="updater" class="col-lg-3 control-label">更新者</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="<?php echo $_member_data["first_name"].$_member_data["last_name"];?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-3 control-label">更新時間</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="<?php echo date("Y-m-d H:i:s");?>" Disabled>
                  </div>
                </div>
              </div>
              
              
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                     <a href="<?php echo $_father_link;?>"><button type="button" class="btn default">Cancel</button></a>
                  </div>
                </div>
              </div>
            </form>
            
            
            
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
        
<!-- BEGIN FOOTER -->
<?php $this->load->view('admin/general/_item_footer.php');?>
<!-- END FOOTER -->