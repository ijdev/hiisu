<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 雲端婚訊版型
* 基本說明，每個 channel 都會有自己的 faq
* 側邊欄位上的項目就是後台的分類
* 右側欄位的內容就是所有分類的內容拉出後整個排上去
*/
?>

<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<!-- Page level plugin styles END -->

<ul class="breadcrumb">
  <?php if($this->uri->segment(2)=='ewedding'):?>
      <li><a href="preview/ewedding">雲端婚訊</a></li>
    <?php elseif($this->uri->segment(2)=='checkIn'):?>
      <li><a href="preview/checkIn">現場報到系統</a></li>
    <?php else:?>
      <li><a href="/">首頁</a></li>
    <?php endif;?>
    <li class="active">常見問題</li>
</ul>
<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN CONTENT -->
  <div class="col-md-12 col-sm-12">

    <div class="content-page faq_cnt">
      <div class="row">
        <div class="faq_web_view">
			<div class="col-md-3 col-sm-3 faq-left-block">
			  <ul class="tabbable faq-tabbable">
<?php if($content_types){foreach($content_types as $key=>$_Item){?>
    <li <?php echo ($_Item['content_title']==$content_type || $key==0) ? 'class="active"':''; ?>><a id="<?php echo $_Item['content_title'];?>" href="#tab_<?php echo $_Item['content_type_sn'];?>" data-toggle="tab"><?php echo $_Item['content_title'];?></a></li>
<?php }}?>
			  </ul>
			</div>
			<div class="col-md-9 col-sm-9 faq-rgt-block">
			  <div class="tab-content" style="padding:0; background: #fff;">
              <?php if($content_types){foreach($content_types as $key=>$_Item){?>
                <!-- START TAB -->
                <div class="tab-pane<?php echo ($key==0)? ' active':'';?>" id="tab_<?php echo $_Item['content_type_sn'];?>">
                  <div class="panel-group" id="accordion<?php echo $_Item['content_type_sn'];?>">
										<?php if($_Item['faqs']){foreach($_Item['faqs'] as $key2=>$_Item2){?>
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="#accordion<?php echo $_Item['content_type_sn'];?>_<?php echo $_Item2['faq_sn'];?>" data-parent="#accordion<?php echo $_Item['content_type_sn'];?>" data-toggle="collapse" class="accordion-toggle">
                          <h4 class="panel-title">
                              <?php echo $_Item2['faq_titile'];?>
                          </h4>
                             </a>
                        </div>
                        <div class="panel-collapse collapse<?php echo ($faq_titile==$_Item2['faq_titile'] || ($faq_titile=='' && $key2==0))? ' in':'';?>" id="accordion<?php echo $_Item['content_type_sn'];?>_<?php echo $_Item2['faq_sn'];?>">
                          <div class="panel-body">
                              <?php echo nl2br($_Item2['faq_description']);?>
                          </div>
                        </div>
                      </div>
                    <?php }}?>
                  </div>
                  <!-- /#accordion -->
                </div>
                <!-- /TAB -->
              <?php }}?>
			  </div>
			  <!-- /.tab-content -->
			</div>
		</div><!-- /.faq_web_view -->


		<div class="faq_mob_view">
			<div class="col-md-3 col-sm-3 faq-left-block">
			  <ul class="tabbable faq-tabbable">
        <?php if($content_types){foreach($content_types as $key=>$_Item){?>
				  <li class="faq_tab"><a class="inner_tab collapsed" href="#tab_2<?php echo $_Item['content_type_sn'];?>" data-toggle="collapse"><?php echo $_Item['content_title'];?><i class="fa fa-plus" aria-hidden="true"></i></a></li>
				  <div class="collapse tab-pane<?php echo ($key==0)? ' active':'';?>" id="tab_2<?php echo $_Item['content_type_sn'];?>">
					<div class="panel-group" id="accordion<?php echo $_Item['content_type_sn'];?>">
            <?php if($_Item['faqs']){foreach($_Item['faqs'] as $key2=>$_Item2){?>
						<div class="panel panel-default faq_default">
						  <div class="panel-heading faq_head_tab">
							  <a href="#accordion2<?php echo $_Item['content_type_sn'];?>_<?php echo $_Item2['faq_sn'];?>" data-parent="#accordion2<?php echo $_Item['content_type_sn'];?>" data-toggle="collapse" class="accordion-toggle">
                    <h4 class="panel-title">
                      <?php echo $_Item2['faq_titile'];?>
                    </h4>
							   </a>
						  </div>
						  <div class="panel-collapse collapse<?php echo ($faq_titile==$_Item2['faq_titile'] || ($faq_titile=='' && $key2==0))? ' in':'';?>" id="accordion2<?php echo $_Item['content_type_sn'];?>_<?php echo $_Item2['faq_sn'];?>">
							<div class="panel-body">
                  <?php echo nl2br($_Item2['faq_description']);?>
							</div>
						  </div>
						</div>
            <?php }}?>
					</div>
				  </div>
          <?php }}?>
			  </ul>
			</div>
      </div>
    </div>
  </div>
  <!-- END CONTENT -->
</div>
<!-- END SIDEBAR & CONTENT -->