<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model {

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('sean_models/Sean_general');
		$this->load->library('user_agent');
    }

    /**
     *
     * validate_login: check login data against database information
     *
     * @param string $username the username to be validated
     * @param string $password the password to be validated
     * @return mixed
     * $member_level_type先不用
     */

    public function validate_login($username, $password,$member_level_type) {

		//會員參數
		$_c_member_config="member_config";
		$_c_member_data="member_data";
		//會員資料庫參數 抓config/member_config.php 裡的member_data 陣列
		$member_item=$this->Sean_general->general_data($_c_member_config,$_c_member_data);
		// 指定會員資料庫參數 至session
		$this->session->set_userdata('member_item',$member_item);

		$_list_item="";
		foreach ($member_item as $k => $i)
		{
				if(strlen($_list_item) > 0 )
				$_list_item.=",";

				$_list_item.=$k;
		}



        $this->db->select($_list_item);
        $this->db->from('ij_member');
       // $this->db->join('ij_member_role_relation, ij_member_role_relation.member_sn = ij_member.member_sn, left');
        $this->db->where('user_name', $username);
		//$this->db->where('member_level_type', $member_level_type);
		//if($member_level_type=='9'){
		//	$this->db->where('system_user_flag', 1);
		//}
		$this->db->where('member_status', 2);
        $this->db->limit(1);
        $query = $this->db->get();
        //var_dump($member_item);
        //var_dump($query->row());
        //exit;

				if ( ! $query )
				{
						$error = $this->db->error(); // Has keys 'code' and 'message'
						$this->session->set_flashdata("success_message",$error);
						return false;
						exit();
				}


       if($query->num_rows() == 1){
           $row = $query->row();
           $array=array();
		  			if (md5($password) == $row->password){
		  				//var_dump($row->member_level_type);
		  				//exit();
							//if($row->member_level_type==$member_level_type || ($row->member_level_type=='6' && $row->member_level_type > $member_level_type)){
							//if($row->member_level_type==$member_level_type || ($row->member_level_type=='6' && $row->member_level_type > $member_level_type)){
								//if($row->system_user_flag!='1' && $member_level_type=='9'){
								//	return '您的帳號無系統管理權限喔！';
								//}else{
							   //抓去會員的資訊
							     foreach ($member_item as $k => $i){
									$array[$k]=$row->$k;
                                    //var_dump($array[$k]);
								}
								 if(@$array['associated_supplier_sn']){
									 $_where=" where supplier_sn='".@$array['associated_supplier_sn']."'";
									 @$array['supplier_name']=$this->Sean_db_tools->db_get_one_record('supplier_name','ij_supplier',$_where)['supplier_name'];
								 }else{
                                    @$array['supplier_name']='';
                                 }
                                 if(@$array['associated_dealer_sn']){
                                     $_where=" where dealer_sn='".@$array['associated_dealer_sn']."'";
                                     $_dealer=$this->Sean_db_tools->db_get_one_record('dealer_name,page_profile_pic_save_dir','ij_dealer',$_where);
                                     @$array['dealer_name']=$_dealer['dealer_name'];
                                     //var_dump(explode(':',substr($_dealer['page_profile_pic_save_dir'], 0, -1)));
                                     //exit;
                                     @$array['dealer_logo']=($page_profile_pic_save_dir=explode(':',substr($_dealer['page_profile_pic_save_dir'], 0, -1))[0])?$page_profile_pic_save_dir:'';
                                 }else{
                                   @$array['dealer_name']='';
                                 }
								 //清空錯誤次數
						 		 //$this->reset_login_attempts($username);
								 //紀錄最後更新時間
								 //$this->_update_last_login($username);
								 ///紀錄登入成功資料
								 $this->_member_log(@$array["member_sn"],$username,1);
								//回傳會員資訊
								return $array;
							//}
						//}else{
						//	return '您的帳號無此類別權限喔！';
						//}

					}else{
						//紀錄登入失敗資料
						 $this->_member_log("0",$username,2);
						 //增加錯誤次數紀錄
					   //  $this->_increase_login_attempts($username);
						return '密碼錯誤';

					}
			//紀錄登入失敗資料
			 $this->_member_log("0",$username,2);
			  //增加錯誤次數紀錄
		//	$this->_increase_login_attempts($username);
       	 }else{

        return '查無帳號資料';
      }
    }

    /**
     *
     * _update_last_login: update the last time the member logged in
     *
     * @param string $username the username to be validated
     * @return boolean
     *
     */

    private function _update_last_login($username) {
        $this->db->set('last_login', 'NOW()', FALSE);
        $this->db->where('user_name', $username);
        $this->db->update('ij_member');

        if ($this->db->affected_rows() == 1) {
            return true;
        }
        return false;
    }

    /**
     *
     * _increase_login_attempts: add +1 to login attempts for member
     *
     * @param string $username the username to be validated
     * @return boolean
     *
     */

    private function _increase_login_attempts($username) {
        $this->db->set('login_attempts', 'login_attempts + 1', FALSE);
        $this->db->where('user_name', $username);
        $this->db->update('ij_member');

        if ($this->db->affected_rows() == 1) {
            return true;
        }
        return false;
    }

    /**
     *
     * reset_login_attempts: bring login attempts back to 0
     *
     * @param string $username the username to be validated
     * @return boolean
     *
     */

    public function reset_login_attempts($username) {
        $this->db->where('user_name', $username);
        $this->db->update('ij_member', array('login_attempts' => 0));
    }

	public function _member_log($member_sn,$username,$type) {
//"type" => "type",//1 login true , 2 login false , 3 logout
        $data = array(
            'browser' => $this->agent->browser(),
            'browser_version' => $this->agent->version(),
            'mobile' => $this->agent->mobile(),
            'robot' => $this->agent->robot(),
            'platform' => $this->agent->platform(),
            'http_referrer' => $this->agent->referrer(),
            'agent_string' => $this->agent->agent_string(),
            'accept_lang' => $this->agent->accept_lang(),
						'accept_charset' => $this->agent->accept_charset(),
						'login_ip_addr' => $this->input->ip_address()
        );

        $this->db->set('login_date', 'NOW()', FALSE);
				$this->db->set('member_sn', $member_sn, FALSE);
				$this->db->set('user_name', "'$username'", FALSE);
				$this->db->set('type', $type, FALSE);
        $this->db->insert('ij_member_login_log', $data);

        if ($this->db->affected_rows() == 1) {
            return true;
        }
        return false;
    }

}

/* End of file login_model.php */
/* Location: ./application/models/membership/login_model.php */