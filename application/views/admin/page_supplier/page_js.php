<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script>
jQuery(document).ready(function() {       
  Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	$('.fancybox').fancybox();
	var ItemId='<?php echo $Item['dealer_sn']?>';
    $("#page_form").validate({
        // Specify the validation rules
        rules: {
            'dealer[dealer_thanksgiving]': {
                required: false,
                maxlength: 140
            },
             'dealer[dealer_blessing]': {
                required: false,
                maxlength: 140
            },
           
        },
        // Specify the validation error messages
        messages: {
            'dealer[dealer_thanksgiving]': {
                required: "請輸入",
                maxlength: "訪客留言最多140中文字"
            },
            'dealer[dealer_blessing]': {
                required: "請輸入",
                maxlength: "感謝有您最多140中文字"
            },
            user_name: "請輸入正確的email格式！"
        },
         
        submitHandler: function(form) {
            form.submit();
        }
    });
    $(".image_del").click(function(){
        var button = $(this);
        	var r = confirm('是否刪除此圖檔?');
            if(r)
            {
						    $.post('/admin/admin/ajaxdel/', {dtable: '<?php echo base64_encode('ij_dealer');?>',dfield:'<?php echo base64_encode('dealer_sn');?>',did:ItemId,dkind:button.attr('kind'),dfilename:button.attr('filename')}, function(data){
                	if (data){
                		//button.parents().parents().filter('tr').remove();
                		$('#'+button.attr('Itemid')).remove();
                		location.reload();
                	}else{
                		alert('刪除失敗');
                	}
						    });
            }
    });    
		var multispec_count=<?php echo(count(@$soical_media))?count(@$soical_media):'1'?>;
    jQuery('#media_add').click(function(){
    	var html=jQuery('#sample_spec').html().replace(/Count/g,'social_media['+multispec_count+++']');
    	//console.log(html);
      jQuery('#social_media').append(html);
    });
    $("#customize_website_url").change(function(){
		    $.post('/admin/product/chk_if_exist/', {dtable: '<?php echo base64_encode('ij_dealer');?>',dfield:'<?php echo base64_encode('customize_website_url');?>',dvalue:$(this).val()}, function(data){
    	console.log(data);
        	if (data==0){
        		//button.parents().parents().filter('tr').remove();
        		$('#showresult').fadeIn("slow").text('可使用');
         		$('button[type="submit"]').attr('disabled',false);
       	}else{
        		$('#showresult').fadeIn("slow").text('已被使用');
        		$('button[type="submit"]').attr('disabled',true);
        	}
		    });
    });
});
</script>

