<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Home 主內容
*/
$_data=array('content_types'=>@$content_types,'Item'=>@$Item);
?>

<!-- Page level plugin styles START -->
<!-- Page level plugin styles END -->
<div class="container">
  <!-- BEGIN SIDEBAR & CONTENT -->
  <div class="row margin-bottom-40">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">

      <div class="content-page" style="padding: 0;">
	    <ol class="breadcrumb breadcrumb-inverse" style="margin-left: -15px">
						<li><a href="/">首頁</a></li>
            <li class=""><a href="/home/blogList">婚禮情報</a></li>
            <?php if($content_type!='all'){?>
              <li class=""><?=($content_type!='all')?$content_type:''?></li>
            <?php }?>
					</ol>
        <div class="row">
          <!-- BEGIN LEFT SIDEBAR -->
          <div class="col-md-9 col-sm-9 blog-posts" style="border-right: 1px solid #eee;">
<?php if($Items){ foreach($Items as $_Item){?>
              <!-- Blog post item -->
              <div class="row">
<div class="col-md-3 col-sm-3 blog-sidebar" style="margin-left: -16px;">
                          <?php if($_Item['blog_article_image_save_dir']):?><img src="/public/uploads/<?php echo @$_Item['blog_article_image_save_dir'];?>" ><?php else:?><img alt="" src="/public/metronic/frontend/pages/img/works/img1.jpg"><?php endif;?>
                        </div>
                <div class="col-md-8 col-sm-8" style="margin-left: 35px;">
                  <h2><a href="home/blogItem/<?php echo $_Item['blog_article_sn'];?>"><?php echo $_Item['blog_article_titile'];?></a></h2>
                  <ul class="blog-info" style="border-bottom: 1px solid #eee;">
                    <li><img src="/public/img/icon_calendar.png" class="fa-calendar"/> <?php echo $_Item['last_time_update'];?></li>
                    <?php if($_Item['keyword']){?>
                      <li><img class="fa-tags" src="/public/img/icon_tag.png"/> <?php echo $_Item['keyword'];?></li>
                    <?php }?>
                  </ul>
                  <p><?php echo $this->ijw->truncate($_Item['blog_article_content'],140);?></p>
                  <a href="home/blogItem/<?php echo $_Item['blog_article_sn'];?>" class="more">Read more <i class="icon-angle-right"></i></a>
                </div>

              </div>
              <!-- End of Blog post item -->
              <hr class="blog-post-sep">
<?php }}else{
	if($search){
    echo '您搜尋的關鍵字尚無符合的文章';
  }else{
    echo '本分類尚無文章';
  }
}?>
					<?php echo $page_links;?>
          </div>
          <!-- END LEFT SIDEBAR -->

          <!-- BEGIN RIGHT SIDEBAR -->
          <?php $this->load->view('www/page_blog/sidebar',$_data)?>
          <!-- END RIGHT SIDEBAR -->
        </div>
      </div>
    </div>
    <!-- END CONTENT -->
  </div>
  <!-- END SIDEBAR & CONTENT -->
</div>