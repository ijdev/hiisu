<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link href="public/metronic/global/plugins/jquery-minicolors/jquery.minicolors.css" rel="stylesheet" type="text/css"/>


<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品管理 <small>上下架管理</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="preview">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
         <a href="preview/inventory">上下架管理</a>
         <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         上下架管理編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">上下架管理編輯</span>
            </div>
            <div class="actions btn-set">
              <a href="preview/inventory" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form">
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">商品名稱</label>
                <div class="col-md-10">
                  <p class="form-control-static">名稱名稱名稱</p>
                </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">商品編號</label>
                <div class="col-md-10">
                  <p class="form-control-static">d1234</p>
                </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">現有庫存</label>
                <div class="col-md-10">
                  <p class="form-control-static">-3</p>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-md-2 control-label">調整類型</label>
                <div class="col-md-10">
                  <div class="radio-list">
                    <label class="radio-inline"><input type="radio" name="type" value="" checked="">入庫</label>
                    <label class="radio-inline"><input type="radio" name="type" value="">報廢</label>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">成本價</label>
                <div class="col-md-10">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-md-2 control-label">數量 <span class="require">*</span></label>
                <div class="col-md-2">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label for="password" class="col-md-2 control-label">備註</label>
                <div class="col-md-10">
                  <textarea class="form-control" rows="3"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-md-2 control-label">更新者</label>
                <div class="col-md-10">
                  <input type="text" class="form-control"  placeholder="administrator" Disabled>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-md-2 control-label">更新時間</label>
                <div class="col-md-10">
                  <input type="text" class="form-control"  placeholder="2015-05-25 10:00" Disabled>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->