<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_level_ct extends My_general_ct {

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('sean_models/Sean_db_tools');
		
		
		/* Begin 修改  */
		$this->_ct_name="使用者等級代碼檔";// 功能名稱
		$this->_url="Sysconfig/Member_level_ct/";//列表功能url
		$this->_url_item="Sysconfig/Member_level_ct_item/";//處理動作 url
		$this->_table="ij_member_level_ct";//table name
		$this->_id="member_level_code";//table key 
		$this->_table_field=array(
					 "member_level_code" => "使用者等級代號",					 					 
					 "member_level" => "使用者等級",
					 "level_name" => "使用者等級名稱",					 					 
					 "profit_sharing_rate" => "預設分潤比例",					 					 
					 "default_flag" => "是否為預設值",
					 "display_flag" => "是否顯示",
				);
				
		/* End 修改  */		

    }
    
    //取得列表內容
    public function table_value_list( $_where,$_id = 0) 
	{
		$_table_key=$this->_id;    
	    $page_data['_body_result']=array();
		//判斷是否有帶入特殊查詢
		if(strlen($_where)==0)
		{
			$_where=" ";
			 
		}
		// 取得列表 	 	 	 	 	 	
		$_result01=$this->Sean_db_tools->db_get_max_record("*",$this->_table,$_where);
		
		/* 列表標頭名稱 */
		$page_data['_table_thead_name']=$this->_table_field;
		
		
		/* 類表內容 */
		if(is_array($_result01))
		{                                  
                 		$_i=0;
                 		foreach($_result01 as $key => $value)
                    	{                    			                    			                    			
			                    		
			                    		if($value->default_flag == 1)
			                    		{
			                    					$_default_flag = '<i class="fa fa-check"></i>';
			                    		}else{
			                    					$_default_flag = '<i class="fa fa-times"></i>';
			                    		}
										if($value->display_flag == 1)
			                    		{
			                    					$_display_flag = '<i class="fa fa-check"></i>';
			                    		}else{
			                    					$_display_flag = '<i class="fa fa-times"></i>';
			                    		}
			                    				
			                    		$_body_result='';
						/* Begin 修改  */
			                    		
										//$page_data['_body_result'][$_i][0]=array("_value"=>$value->member_level_code,"_class"=>"");			              
			              $page_data['_body_result'][$_i][1]=array("_value"=>$value->member_level,"_class"=>"");
										$page_data['_body_result'][$_i][2]=array("_value"=>$value->level_name,"_class"=>"");																																					
										$page_data['_body_result'][$_i][3]=array("_value"=>$value->profit_sharing_rate,"_class"=>"");																																					
										$page_data['_body_result'][$_i][4]=array("_value"=>$_default_flag,"_class"=>"");	
									  $page_data['_body_result'][$_i][5]=array("_value"=>$_display_flag,"_class"=>"");
			                    		
						/* End 修改  */
										
										
										$page_data['_body_result'][$_i][6]=array("_value"=>'
			                    		 		<a href="'.$this->init_control.$this->_url_item.'edit/'.$value->$_table_key.'"><i class="fa fa-edit"></i>編輯</a>&nbsp;&nbsp;
						                      <a href="'.$this->init_control.$this->_url_item.'delete/'.$value->$_table_key.'" onclick="return deletechecked();"><i class="fa fa-trash-o"></i>刪除</a>&nbsp;&nbsp;
						                  	',"_class"=>'class="center"');
                				$_i++;
								
						
                			}
                	
    				}
			return $page_data;
			
    }

	
	
}
