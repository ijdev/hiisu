<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="public/metronic/global/plugins/bootstrap-selectsplitter/bootstrap-selectsplitter.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/jquery-minicolors/jquery.minicolors.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/components-form-tools2.js"></script>
<link rel="stylesheet" href="public/metronic/global/plugins/jquery-ui/jquery-ui.min.css">
<script src="public/metronic/global/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
    //ComponentsFormTools2.init();
    $.ajax({
        url: 'admin/Member/auto_uno',
        data: 'id=uno',
        dataType: 'json',
        success: function(data) {
            $("#uno").autocomplete({
                source: data,
                select: function( event, ui ) {
                    var uno = ui.item.value;
                    var area = $(this).data('area');
                    if(uno) {
                        $.post('admin/Member/get_member_name/',{
                            data: uno,
                            area: area
                        }, function(data) {
                            if(data == 0) {
                                $('#show_uno_name').fadeIn("slow").html('查無資料');
                                $('#submit_button').attr('disabled',true);
                            } else if(data == 1) {
                                $('#show_uno_name').fadeIn("slow").html('該編號無'+area+'經營權');
                                $('#submit_button').attr('disabled',true);
                            } else {
                                $('#show_uno_name').fadeIn("slow").html(data);
                                $('#submit_button').attr('disabled',false);
                            }
                        });
                    }
                }
            });
        }
    });
});
$('.get_current_amount').click(function() {
  $.post('admin/product/get_current_amount', {
          mutispec_stock_sn: $(this).val()
      }, function(data) {
				$('#current_amount').text(data);
  });
});
</script>

