<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script>
jQuery(document).ready(function() {       
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
<?php if(@$channel_name){?>
	ChgStoreName('<?php echo @$channel_name;?>','<?php echo @$upper_category_sn;?>');
<?php }?>
});
function ChgStoreName(C,D){ //變動館別、帶入值
	if(!D) D='';
	$('#Categorys option').remove();$('#Categorys').append('<option>Loading...</option>');
	$.get('/admin/product/ChgStoreName/'+C+'/'+D,function(data){$('#Categorys option').remove();$('#Categorys').append(data);});;
}
</script>

