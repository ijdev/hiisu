<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 線上商店商品內頁
*/
$_web_member_data=$this->session->userdata('web_member_data');
//var_dump($_web_member_data);
?>
<div class="container">

<style>
  .pi-img-wrapper img{border:1px solid #EFEFEF;}
  .sale-product{background-color: #EFEFEF;padding: 12px;}
  .goods-page-image img{
    -webkit-box-shadow: 0 1px 8px rgba(0, 0, 0, 0.5);
    box-shadow: 0 1px 8px rgba(0, 0, 0, 0.5);
  }

  #checkbox_data_sync{
    font-size: 12px;
    color: #666;
    display: inline-block;
    margin-left: 32px;
  }
</style>
<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<link href="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="public/metronic/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- Page level plugin styles END -->

<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="shop">結婚商店</a></li>
    <li class="active">填寫訂購資料</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN CONTENT -->
  <div class="col-md-12 col-sm-12">
    <h1>訂購資料填寫</h1>
<form id="CHECKOUT" method="POST" action="shop/shopCheckoutShipping">
    <!-- BEGIN CHECKOUT PAGE -->
    <div class="panel-group checkout-page accordion scrollable" id="checkout-page">

      <!-- BEGIN PAYMENT ADDRESS -->
      <div id="payment-address" class="panel panel-default">
        <div class="panel-heading">
          <h2 class="panel-title">
            <a data-toggle="collapse" data-parent="#checkout-page" href="#payment-address-content" class="accordion-toggle">
              付款人資料
            </a>
          </h2>
        </div>
        <div id="payment-address-content" class="panel-collapse collapse in">
          <div class="panel-body row">
            <div class="col-md-6 col-sm-6">
              <h3>付款人資料</h3>
              <div class="form-group">
                <div class="form-inline">
		              <div class="form-group">
		                <label for="lastname">姓 <span class="require">*</span></label>
		                <input type="text" required id="last_name" name="order[buyer_last_name]" class="form-control" value="<?php echo (@$order["buyer_last_name"])? $order["buyer_last_name"]:$_web_member_data["last_name"];?>" >
		              </div>
		              <div class="form-group">
		                <label for="lastname">名 <span class="require">*</span></label>
		                <input type="text" required id="first_name" name="order[buyer_first_name]" class="form-control" value="<?php echo (@$order["buyer_first_name"])? $order["buyer_first_name"]:$_web_member_data["first_name"];?>" >
		              </div>
		           </div>
		          </div>
              <div class="form-group">
                <label for="telephone">手機號碼 <span class="require">*</span></label>
                <input type="text" required id="cell" name="order[buyer_cell]" class="form-control" value="<?php echo (@$order["buyer_cell"])? $order["buyer_cell"]:$_web_member_data["cell"];?>">
              </div>
              <div class="form-group">
                <label for="telephone">Email <span class="require">*</span></label>
                <input type="text" required id="email" name="order[buyer_email]" class="form-control" value="<?php echo (@$order["buyer_email"])? $order["buyer_email"]:$_web_member_data["email"];?>">
              </div>
              <div class="form-group form-inline">
                	<input type="checkbox" class="form-control" name="update_member" value="yes">
                結帳成功後同步更新您的會員資料
              </div>
            </div>
            <div class="col-md-6 col-sm-6">
              <h3>&nbsp;</h3>
              <div class="form-group">
                <label for="telephone">市話</label>
                <div class="form-inline">
                  <div class="form-group">
                    <label class="sr-only" for="exampleInputEmail2">市話區碼</label>
                    <input type="text" id="tel_area_code" name="order[buyer_tel_area_code]" class="form-control" size="4" maxlength="3" placeholder="區碼" value="<?php echo (@$order["buyer_tel_area_code"])? $order["buyer_tel_area_code"]:$_web_member_data["tel_area_code"];?>">
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="exampleInputEmail2">市話號碼</label>
                    <input type="text" id="tel" name="order[buyer_tel]" class="form-control" size="16" maxlength="10" placeholder="請輸入市話號碼" value="<?php echo (@$order["buyer_tel"])? $order["buyer_tel"]:$_web_member_data["tel"];?>">
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="exampleInputEmail2">分機</label>
                    <input type="text" id="tel_ext" name="order[buyer_tel_ext]" class="form-control" size="4" placeholder="分機" value="<?php echo (@$order["buyer_tel_ext"])? $order["buyer_tel_ext"]:$_web_member_data["tel_ext"];?>">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="telephone">地址 <span class="require">*</span></label>
                <div class="form-inline">
                  <div class="form-group">
                    <label class="sr-only" for="buyer_zipcode">郵遞區號</label>
                    <input type="text" required id="zipcode" name="order[buyer_zipcode]" class="form-control" size="8" maxlength="5" placeholder="郵遞區號" value="<?php echo (@$order["buyer_zipcode"])? $order["buyer_zipcode"]:$_web_member_data["zipcode"];?>"> 
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="addr_city_code">縣市</label>
                    <select required class="form-control input-sm" id="addr_city_code" name="order[buyer_addr_city]">
                      <option value="" disable> 縣市 </option>
													<?php
						                 while (list($keys, $values) = each ($city_ct)) {
						    							echo  ' <option value="'.$keys.'">'.$values.'</option>';
														}?>
	                  </select>
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="addr_town_code">區域</label>
                    <select required class="form-control input-sm" id="addr_town_code" name="order[buyer_addr_town]">
                      <option value="" disable> 區域 </option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <input type="text" required id="addr" name="order[buyer_addr1]" class="form-control" placeholder="地址" value="<?php echo (@$order["buyer_addr1"])? $order["buyer_addr1"]:$_web_member_data["addr1"];?>">
              </div>
            </div>
            <hr>
          </div>
        </div>
      </div>
      <!-- END PAYMENT ADDRESS -->

      <!-- BEGIN PAYMENT ADDRESS -->
<?php if(!$all_virtual_prod) { ?>
      <div id="shipping-address" class="panel panel-default">
        <div class="panel-heading">
          <h2 class="panel-title">
            <a data-toggle="collapse" data-parent="#checkout-page" href="#payment-shipping-content" class="accordion-toggle">
              收件人資料
            </a>
          </h2>
        </div>
        <div id="payment-shipping-content" class="panel-collapse collapse in">
          <div class="panel-body row">
            <div class="col-md-6 col-sm-6">
            	<div class="form-group form-inline">
              	<h3>收件人資料<span id="checkbox_data_sync"><input id="checkbox_data_sync_1" type="checkbox" class="form-control"> 同付款人資料</span></h3>
              </div>
              <div class="form-group">
                <label for="lastname">姓名 <span class="require">*</span></label>
                <input type="text" required id="name1" name="order[receiver_last_name]" class="form-control" value="<?php echo (@$order["receiver_last_name"])? $order["receiver_last_name"]:@$receiver_data["last_name"];?>">
              </div>
              <div class="form-group">
                <label for="telephone">手機號碼 <span class="require">*</span></label>
                <input type="text" required id="cell1" name="order[receiver_cell]" class="form-control" value="<?php echo (@$order["receiver_cell"])? $order["receiver_cell"]:@$receiver_data["cell"];?>">
              </div>
              <!--div class="form-group">
                <label for="telephone">Email <span class="require">*</span></label>
                <input type="text" name="" class="form-control">
              </div-->
            </div>
            <div class="col-md-6 col-sm-6">
              <h3>&nbsp;</h3>
              <div class="form-group">
                <label for="telephone">市話</label>
                <div class="form-inline">
                  <div class="form-group">
                    <label class="sr-only" for="exampleInputEmail2">市話區碼</label>
                    <input type="text" id="tel_area_code1" class="form-control" name="order[receiver_tel_area_code]" size="4" maxlength="3" placeholder="區碼" value="<?php echo (@$order["receiver_tel_area_code"])? $order["receiver_tel_area_code"]:@$receiver_data["tel_area_code"];?>">
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="exampleInputEmail2">市話號碼</label>
                    <input type="text" id="tel1" class="form-control" name="order[receiver_tel]" size="16" maxlength="10" placeholder="請輸入市話號碼" value="<?php echo (@$order["receiver_tel"])? $order["receiver_tel"]:@$receiver_data["tel"];?>">
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="exampleInputEmail2">分機</label>
                    <input type="text" id="tel_ext1" class="form-control" name="order[receiver_tel_ext]" size="4" placeholder="分機" value="<?php echo (@$order["receiver_tel_ext"])? $order["receiver_tel_ext"]:@$receiver_data["tel_ext"];?>">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="telephone">地址 <span class="require">*</span></label>
                <div class="form-inline">
                  <div class="form-group">
                    <label class="sr-only" for="zipcode1">郵遞區號</label>
                    <input type="text" required id="zipcode1" name="order[receiver_zipcode]" class="form-control" size="8" maxlength="5" placeholder="郵遞區號" value="<?php echo (@$order["receiver_zipcode"])? $order["receiver_zipcode"]:@$receiver_data["zipcode"];?>">
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="addr_city_code1">縣市</label>
                    <select required class="form-control input-sm" id="addr_city_code1" name="order[receiver_addr_city]">
                      <option value="" disable> 縣市 </option>
													<?php //var_dump($city_ct);
						                 while (list($keys, $values) = each ($city_ct1)) {
						    							echo  ' <option value="'.$keys.'">'.$values.'</option>';
														}?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="addr_town_code1">區域</label>
                    <select required class="form-control input-sm" id="addr_town_code1" name="order[receiver_addr_town]">
                      <option value="" disable> 區域 </option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <input type="text" required id="addr1" name="order[receiver_addr1]" class="form-control" value="<?php echo (@$order["receiver_addr1"])? $order["receiver_addr1"]:@$receiver_data["addr1"];?>">
              </div>
            </div>
            <hr>
          </div>
        </div>
      </div>
      <!-- END PAYMENT ADDRESS -->
<?php }?>
      <div id="checkout" class="panel panel-default">
        <div class="panel-heading">
          <h2 class="panel-title">
            <a data-toggle="collapse" data-parent="#checkout-page" href="#checkout-content" class="accordion-toggle">
              發票資料
            </a>
          </h2>
        </div>
        <div id="checkout-content" class="panel-collapse collapse in">
          <div class="panel-body">
            <div class="tabbable tabbable-tabdrop">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab1" data-toggle="tab">紙本發票(兩聯式)</a>
                </li>
                <li>
                  <a href="#tab2" id="tab2s" data-toggle="tab">公司用(三聯式)</a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane" active id="tab1">
                  <p>
                     依統一發票使用辦法規定：個人(二聯式)發票一經開立，不得任意更改或改開公司戶(三聯式)發票。
                     <a href="https://www.einvoice.nat.gov.tw/?CSRT=6201288656407980614" target="_blank">財政部電子發票流程說明</a>
                  </p>
                </div>
                <div class="tab-pane" id="tab2">
                  <p>
                    <div class="form-horizontal row">
                      <div class="form-group col-md-6 row">
                        <label class="col-md-3 control-label" style="text-align:right;">發票抬頭 <span class="require">*</span></label>
                        <div class="col-md-6">
                          <input type="text" class="form-control" id="invoice_title" name="order[invoice_title]" value="<?php echo @$order["invoice_title"]?>" placeholder="發票抬頭">
                        </div>
                      </div>
                      <div class="form-group col-md-6 row">
                        <label class="col-md-3 control-label" style="text-align:right;">統一編號 <span class="require">*</span></label>
                        <div class="col-md-6">
                          <input type="text" class="form-control" id="company_tax_id" name="order[company_tax_id]" value="<?php echo @$order["company_tax_id"]?>" maxlength="8" placeholder="統一編號">
                        </div>
                      </div>
                    </div>
                    <div style="text-align:center;">提醒：若後續辦理退貨，須填寫銷貨折讓單並蓋公司大小章或統一發票章</div>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div id="note" class="panel panel-default">
        <div class="panel-heading">
          <h2 class="panel-title">
            <a data-toggle="collapse" data-parent="#checkout-note" href="#checkout-note" class="accordion-toggle">
              訂單備註
            </a>
          </h2>
        </div>
        <div id="checkout-note" class="panel-collapse collapse in">
          <div class="panel-body">
            <textarea class="form-control" name="sub_order[memo]" rows="5"><?php echo @$order["memo"]?></textarea>
          </div>
        </div>
      </div>

      <div class="text-right padding-top-50">
        <button class="btn btn-primary" onclick="location.href='shop/shopConfirm';" type="button">上一步</button>
        <button class="btn btn-primary" id="submit1" type="submit">下一步</button>
         <br><br>
      </div>
    </div>
    <!-- END CHECKOUT PAGE -->
<input type="hidden" id="buyer_addr_state" name="order[buyer_addr_state]" value="<?php echo $buyer_addr_state;?>">
<input type="hidden" id="receiver_addr_state" name="order[receiver_addr_state]" value="<?php echo $buyer_addr_state;?>">
<input type="hidden" id="buyer_addr_country" name="order[buyer_addr_country]" value="中華民國">
<input type="hidden" id="receiver_addr_country" name="order[receiver_addr_country]" value="中華民國">
<input type="hidden" id="method_shipping" name="order[delivery_method]" value="<?php echo $method_shipping;?>">
<input type="hidden" id="shipping_charge_amount" name="order[shipping_charge_amount]" value="<?php echo $shipping_charge_amount;?>">
</form>
  </div>
  <!-- END CONTENT -->
</div>
</div>

<!-- END SIDEBAR & CONTENT -->