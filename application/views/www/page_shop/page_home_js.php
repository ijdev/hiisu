<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<script src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
<!--<script src="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>--><!-- slider for products -->
<script src="public/metronic/global/plugins/carousel-owl-carousel/OwlCarousel2/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->
<script src='public/metronic/global/plugins/zoom/jquery.zoom.min.js' type="text/javascript"></script><!-- product zoom -->
<script src="public/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script><!-- Quantity -->


<!-- BEGIN LayerSlider -->
<script src="public/metronic/global/plugins/slider-layer-slider/js/greensock.js" type="text/javascript"></script><!-- External libraries: GreenSock -->
<script src="public/metronic/global/plugins/slider-layer-slider/js/layerslider.transitions.js" type="text/javascript"></script><!-- LayerSlider script files -->
<script src="public/metronic/global/plugins/slider-layer-slider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script><!-- LayerSlider script files -->
<script src="public/metronic/frontend/pages/scripts/layerslider-init.js" type="text/javascript"></script>
<!-- END LayerSlider -->

<script src="public/metronic/frontend/layout/scripts/layout.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Layout.init();
        Layout.initOWL();
        LayersliderInit.initLayerSlider();
        //Layout.initImageZoom();
        Layout.initTouchspin();
        Layout.initFixHeaderWithPreHeader();
        Layout.initNavScrolling();

				$('.add2cart').click(function(e) {
					var product_sn=$(this).attr('product_sn');
					if(product_sn){
					 var url = "shop/add";
					    $.ajax({
					           type: "POST",
					           url: url,
							   dataType:'json',
					           data: { product_sn:product_sn,qty:'1' },
					           success: function(data)
					           {
					           	if (data.error){
					           		alert(data.error);
					           	}else{
					           		$('.top-cart-info-count').text(data.total_items+' 件');
					           		$('.top-cart-info-value').text('$ '+data.total);
					           		//更新購物車popup
					           		$('.top-cart-content').load('shop/top_cart');
					           		//console.log(data.total);
					           	}
					           }
					         });
					  }
					    e.preventDefault();
				});
				$('.fa-heart').click(function(e) {
					var product_sn=$(this).attr('product_sn');
					if(product_sn){
 						$.get('member/AddwishList/'+product_sn+'/1',function(data){
	                	$('.heart'+product_sn).fadeIn("slow").html(data);
	                	if(data!='<a href="member/memberSignIn">請先點我登入會員</a>'){
										setTimeout(function(){$('.heart'+product_sn).hide();$('.heart'+product_sn).find("#text").text("");}, 10000);
						}
					          //console.log(data);
					          //console.log($('.heart'+product_sn));
 						});
					}
				});

    });
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->