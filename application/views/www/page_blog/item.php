<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v5.0&appId=699292470176263&autoLogAppEvents=1"></script>

<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<!-- Page level plugin styles END -->
<div class="container">
  <!-- BEGIN SIDEBAR & CONTENT -->
  <div class="row margin-bottom-40">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">

      <div class="content-page" style="padding: 0;">
	   <ol class="breadcrumb breadcrumb-inverse" style="margin-left: -15px">
						<li><a href="/">Home</a></li>
            <li><a href="/home/blogList">婚禮情報</a></li>
						<li><?php echo @$Item['blog_article_titile'];?></li>
					</ol>
					 <!--<h1>Blog Item</h1>-->
        <div class="row" style="margin-left: -30px;">
          <!-- BEGIN LEFT SIDEBAR -->
          <div class="col-md-9 col-sm-9 blog-item">
            <div class="blog-item-img">
              <!-- BEGIN CAROUSEL -->
              <div class="front-carousel">
                <div id="myCarousel" class="carousel slide">
                  <!-- Carousel items -->
                  <div class="carousel-inner">
                    <!--div class="item">
                      <img src="public/metronic/frontend/pages/img/posts/img1.jpg" alt="">
                    </div-->
                    <div class="item active">
                          <?php if($Item['blog_article_image_save_dir']):?><img src="/public/uploads/<?php echo @$Item['blog_article_image_save_dir'];?>" ><?php else:?><?php endif;?>
                    </div>
                  </div>
                  <!-- Carousel nav -->
                  <!--a class="carousel-control left" href="#myCarousel" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                  </a>
                  <a class="carousel-control right" href="#myCarousel" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                  </a-->
                </div>
              </div>
              <!-- END CAROUSEL -->
            </div>
            <h2><?php echo @$Item['blog_article_titile'];?></h2>
            <?php echo @$Item['blog_article_content'];?>
            <ul class="blog-info">
              <li><i class="fa fa-calendar"></i> <?php echo $Item['last_time_update'];?></li>
                    <?php if($Item['keyword']){?>
                      <li><i class="fa fa-tags"></i> <?php echo $Item['keyword'];?></li>
                    <?php }?>
            </ul>

            <h2>留言討論</h2>
            <div class="post-comment padding-top-10">
              <div class="fb-comments" data-href="<?php echo $this->config->item('base_url').$this->uri->uri_string();?>" data-width="100%" data-numposts="5"></div>
            </div>
            <?php /*
            <hr>
            <div class="post-comment padding-top-40">
              <div id="disqus_thread"></div>
              <script type="text/javascript">
                  var disqus_shortname = 'ijwedding';

                  (function() {
                      var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                      dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                      (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                  })();
              </script>
              <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
            </div>*/?>
          </div>
          <!-- END LEFT SIDEBAR -->

          <!-- BEGIN RIGHT SIDEBAR -->


          <!-- BEGIN RIGHT SIDEBAR -->
          <?php $this->load->view('www/page_blog/sidebar')?>
          <!-- END RIGHT SIDEBAR -->
        </div>
      </div>
    </div>
    <!-- END CONTENT -->
  </div>
  <!-- END SIDEBAR & CONTENT -->
</div>