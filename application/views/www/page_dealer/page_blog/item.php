<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Home 主內容
*/
$_data=array('content_types'=>@$content_types,'Item'=>@$Item);
//var_dump($content_types);exit;
?>
  <link href="public/metronic/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="public/metronic/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="public/metronic/frontend/layout/css/themes/red.css" rel="stylesheet" id="style-color">
  <link href="/public/metronic/frontend/layout/css/style.css" rel="stylesheet">
  <script src="public/metronic/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '699292470176263',
      xfbml      : true,
      version    : 'v2.3'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<style>
p {color:#000000 !important;}
</style>
<!-- Page level plugin styles START -->
<!-- Page level plugin styles END -->
<div class="main-container with-padding">
    <div class="wrapper">
        <div class="inner-wrapper">
      <!-- BEGIN SIDEBAR & CONTENT -->
  <div class="row">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">

            <div class="row">
                <div class="col full">
                    <h4><?php echo @$Item['blog_article_titile'];?></h4>
                </div>
            </div>
	        <!--ol class="breadcrumb breadcrumb-inverse" style="margin-left: -15px">
						<li><a href="/">Home</a></li>
                        <li class=""><a href="/home/blogList">婚禮情報</a></li>
						<li class="">部落格文章標題</li>
					</ol-->
					 <!--<h1>Blog Item</h1>-->
        <div class="row">
          <!-- BEGIN LEFT SIDEBAR -->
          <div class="col-md-9 col-sm-9 blog-item">
            <div class="blog-item-img">
              <!-- BEGIN CAROUSEL -->
              <div class="front-carousel">
                <div id="myCarousel" class="carousel slide">
                  <!-- Carousel items -->
                  <div class="carousel-inner">
                    <!--div class="item">
                      <img src="public/metronic/frontend/pages/img/posts/img1.jpg" alt="">
                    </div-->
                    <div class="item active">
                          <?php if($Item['blog_article_image_save_dir']):?><img src="/public/uploads/<?php echo @$Item['blog_article_image_save_dir'];?>" ><?php else:?><?php endif;?>
                    </div>
                  </div>
                  <!-- Carousel nav -->
                  <!--a class="carousel-control left" href="#myCarousel" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                  </a>
                  <a class="carousel-control right" href="#myCarousel" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                  </a-->
                </div>
              </div>
              <!-- END CAROUSEL -->
            </div>
            <h2><?php echo @$Item['blog_article_titile'];?></h2>
            <?php echo @$Item['blog_article_content'];?>
            <ul class="blog-info">
              <li><i class="fa fa-calendar"></i> <?php echo $Item['last_time_update'];?></li>
              <li><i class="fa fa-tags"></i> <?php echo $Item['keyword'];?></li>
            </ul>

            <h2>留言討論</h2>
            <div class="post-comment padding-top-10">
              <div class="fb-comments" data-href="<?php echo $this->config->item('base_url').$this->uri->uri_string();?>" data-numposts="5" data-colorscheme="light" data-width="100%"></div>
            </div>
            <hr>
            <div class="post-comment padding-top-40">
              <div id="disqus_thread"></div>
              <script type="text/javascript">
                  /* * * CONFIGURATION VARIABLES * * */
                  var disqus_shortname = 'ijwedding';

                  /* * * DON'T EDIT BELOW THIS LINE * * */
                  (function() {
                      var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                      dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                      (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                  })();
              </script>
              <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
            </div>
          </div>
          <!-- END LEFT SIDEBAR -->

          <!-- BEGIN RIGHT SIDEBAR -->


          <!-- BEGIN RIGHT SIDEBAR -->
          <?php $this->load->view('www/page_dealer/page_blog/sidebar',$_data)?>
          <!-- END RIGHT SIDEBAR -->
        </div>
    </div>
    <!-- END CONTENT -->
  </div>
  <!-- END SIDEBAR & CONTENT -->
</div></div></div>