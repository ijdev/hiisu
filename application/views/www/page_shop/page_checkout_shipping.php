<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 線上商店商品內頁
*/
?>
<div class="container">

<style>
  .pi-img-wrapper img{border:1px solid #EFEFEF;}
  .sale-product{background-color: #EFEFEF;padding: 12px;}
  .goods-page-image img{
    -webkit-box-shadow: 0 1px 8px rgba(0, 0, 0, 0.5);
    box-shadow: 0 1px 8px rgba(0, 0, 0, 0.5);
  }
</style>
<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<link href="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="public/metronic/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- Page level plugin styles END -->

<ul class="breadcrumb">
    <li><a href="preview">首頁</a></li>
    <li><a href="preview_jerry/shop">線上商店</a></li>
    <li class="active">結帳</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN CONTENT -->
  <div class="col-md-12 col-sm-12">
    <h1>選擇付款方式</h1>
<form id="CHECKOUT" method="POST" action="shop/gotoPay">
    <!-- BEGIN CHECKOUT PAGE -->
    <div class="panel-group checkout-page accordion scrollable" id="checkout-page">


      <!-- BEGIN CONFIRM -->
      <div id="confirm" class="panel panel-default">
        <div class="panel-heading">
          <h2 class="panel-title">
            <a data-toggle="collapse" data-parent="#checkout-page" href="#confirm-content" class="accordion-toggle">
              付款方式
            </a>
          </h2>
        </div>
        <div id="confirm-content" class="panel-collapse collapse in">
          <div class="panel-body row">
            <div class="col-md-12 clearfix">
                <?php if($payments){ foreach($payments as $payment){?>
                    <div class="col-md-12">
                      <div class="form-group col-md-3 row">
                         <input style="margin-left: -10px;" type="radio" id="payment_method_<?php echo $payment['payment_method'];?>" required class="form-control" name="order[payment_method]" value="<?php echo $payment['payment_method'];?>" <?php if(@$payment['checked']) echo 'checked';?> >&nbsp;
													<label style="cursor: pointer;" for="payment_method_<?php echo $payment['payment_method'];?>"><?php echo $payment['payment_method_name'];?></label>
                      </div>
                      <div class="form-group col-md-2 row">
													<?php echo number_format($total,0);?>
                      </div>
                      <div class="form-group col-md-7 row">
													<?php echo $payment['description'];?>
                      </div>  
                    </div>                    
                <?php }}?>
            </div>
          </div>
        </div>
        <div class="text-center" style="padding-top-50">
           <button type="submit" class="btn red-sunglo btn-lg">確定送出</button>
           <br><br>
        </div>
      </div>
    </div>
    <!-- END CHECKOUT PAGE -->
<input type="hidden" name="orderx[total_order_amount]" value="<?php echo @$total_order_amount;?>">
</form>
  </div>
  <!-- END CONTENT -->
</div>
</div>

<!-- END SIDEBAR & CONTENT -->