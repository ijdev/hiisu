<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link href="public/metronic/global/plugins/jquery-minicolors/jquery.minicolors.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>


<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>版型設定 <small>編輯版型設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/admin">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品規格
        <i class="fa fa-circle"></i>
      </li>
      <li>
         <a href="admin/product/productTemplate">版型設定</a>
         <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         版型設定編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">編輯版型設定</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/product/productTemplate" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post">
              <div class="tabbable">
                <ul class="nav nav-tabs">
                  <li class="active">
                    <a href="#tab_general" data-toggle="tab"> 一般設定 </a>
                  </li>
                  <li id="tabImages">
                    <a href="#tab_images" data-toggle="tab"> 顏色與圖片 </a>
                  </li>
                  <li id="tabImages">
                    <a href="#tab_dividers" data-toggle="tab"> 分頁圖片 </a>
                  </li>                  
                </ul>
                <div class="tab-content no-space">
                  <div class="tab-pane active" id="tab_general">
			              <div class="form-group">
			                <label for="firstname" class="col-lg-3 control-label">名稱 <span class="require">*</span></label>
			                <div class="col-lg-8">
			                  <input type="text" class="form-control" name="Item[product_template_name]" value="<?php echo @$Item['product_template_name']; ?>" required>
			                </div>
			              </div>
			              <div class="form-group">
			                <label class="col-md-3 control-label">是否為母版 <span class="require">*</span></label>
			                <div class="col-md-8">
			                  <div class="radio-list">
			                    <label class="radio-inline">
			                      <input class="radio-list" name="Item[original_template_flag]" type="radio" class="form-control" value="1" required <?php echo (@$Item['original_template_flag']=='1') ?'checked':''; ?> >&nbsp;是
			                    </label>
			                    <label class="radio-inline">
			                      <input class="radio-list" name="Item[original_template_flag]" type="radio" class="form-control" value="0" required <?php echo (@$Item['original_template_flag']=='0') ?'checked':''; ?> >&nbsp;否
			                    </label>
			                  </div>
			                </div>
			              </div>
			              <div class="form-group">
			                <label for="email" class="col-lg-3 control-label">版型 Demo 網址 </label>
			                <div class="col-lg-8">
			                  <input type="url" pattern="https?://.+" id="hue-demo" class="form-control maxlength-handler" maxlength="200" name="Item[template_demo_website]" value="<?php echo @$Item['template_demo_website']; ?>" >
			                  <span class="help-block">(請輸入完整網址包含http://)</span>
			                </div>
			              </div>
			              <div class="form-group">
			                <label for="email" class="col-lg-3 control-label">版型資料夾 </label>
			                <div class="col-lg-8">
			                  <input type="text" class="form-control maxlength-handler" maxlength="200" name="Item[template_dir]" value="<?php echo @$Item['template_dir']; ?>" >
			                </div>
			              </div>
			              <div class="form-group">
			                <label for="password" class="col-lg-3 control-label">備註</label>
			                <div class="col-lg-8">
			                  <textarea class="form-control" rows="3" name="Item[description]"><?php echo @$Item['description']; ?></textarea>
			                </div>
			              </div>
			              <div class="form-group">
			                <label for="email" class="col-lg-3 control-label">排序</label>
			                <div class="col-lg-8">
			                  <input type="text" class="form-control" name="Item[sort_order]" value="<?php echo (@$Item['sort_order']) ? @$Item['sort_order']:'9999'; ?>" required>
			                </div>
			              </div>
			              <div class="form-group">
			                <label for="email" class="col-lg-3 control-label">是否啟用</label>
			                <div class="col-lg-8">
			                  <select class="form-control" name="Item[status]">
			                      <option value="1" <?php if(@$Item['status']){ ?>selected<?php }?>>是</option>
			                      <option value="0" <?php if(@$Item['status']==='0'){ ?>selected<?php }?>>否</option>
			                  </select>
			                </div>
			              </div>
			              <div class="form-group">
			                <label for="email" class="col-lg-3 control-label">更新者</label>
			                <div class="col-lg-8">
			                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['update_member_sn'])?@$Item['update_member_sn']:@$Item['create_member_sn']; ?>" Disabled>
			                </div>
			              </div>
			              <div class="form-group">
			                <label for="email" class="col-lg-3 control-label">更新時間</label>
			                <div class="col-lg-8">
			                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['last_time_update']!='0000-00-00 00:00:00')?@$Item['last_time_update']:@$Item['create_date']; ?>" Disabled>
			                </div>
			              </div>
			        </div>
              <div class="tab-pane" id="tab_images">
	              <div class="form-group">
	                <label for="module" class="col-lg-2 control-label">顏色</label>
	                <div class="col-lg-9">
	                  <div id="ColorList" class="checkbox-list form-md-line-input">
	                  </div>
	                </div>
	              </div>
                    <hr>
                    <div>
                      <div class="pull-left">
                        <h3 class="text-warning">
                          版型輪播圖
                        </h3><span class="help-block">(第一個顏色的第一張圖片為預設主圖)</span>
                      </div>
                      <div class="table-actions-wrapper pull-right">
                        <a id="tab_images_uploader_uploadfiles" class="btn green" data-toggle="modal" data-target="#uploadModal">
                          <i class="fa fa-share"></i> 上傳版型圖片
                        </a>
                          <span class="help-block"> 上限 5 張，依照排序顯示前 5 張。 </span>
                      </div>
                      <div style="clear:both;"></div>
                      <div class="table-scrollable table-scrollable-borderless">
                        <table class="table table-hover">
                          <thead>
                            <tr>
                              <th>顏色</th>
                              <th style="text-align:center;">圖片1</th>
                              <th style="text-align:center;">圖片2</th>
                              <th style="text-align:center;">圖片3</th>
                              <th style="text-align:center;">圖片4</th>
                              <th style="text-align:center;">圖片5</th>
                            </tr>
                          </thead>
                          <tbody id="PhotoList">
                          </tbody>
                        </table>
                      </div>
                      <!-- End of img table -->
                    </div>
                 </div>
              <div class="tab-pane" id="tab_dividers">
                    <div>
                      <div class="pull-left">
                        <h3 class="text-warning">
                          分頁圖片
                        </h3>
                      </div>
                      <div class="table-actions-wrapper pull-right">
                        <a id="tab_dividers_uploader_uploadfiles" class="btn green" data-toggle="modal" data-target="#uploadModal2">
                          <i class="fa fa-share"></i> 上傳分頁圖片
                        </a>
                          <!--span class="help-block"> 上限 5 張，依照排序顯示前 5 張。 </span-->
                      </div>
                      <div style="clear:both;"></div>
                      <div class="table-scrollable table-scrollable-borderless">
                        <table class="table table-hover">
                          <thead>
                            <tr>
                              <th style="text-align:center;width:100px;">區域</th>
                              <th style="text-align:center;">文案</th>
                              <th style="text-align:center;">圖片</th>
                            </tr>
                          </thead>
                          <tbody id="DividerList">
                          </tbody>
                        </table>
                      </div>
                      <!-- End of img table -->
                    </div>
                 </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <input type="hidden" id="product_template_sn" name="product_template_sn" value="<?php echo @$Item['product_template_sn']; ?>">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="reset" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
<!-- Modal -->
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">上傳圖片</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-without-legend" role="form" method="post" id="picture_form" enctype="multipart/form-data">
          <fieldset>
            <div class="form-group">
              <label for="email" class="col-lg-4 control-label">圖片標題 <span class="require">*</span></label>
              <div class="col-md-8">
                <input type="text" class="form-control" name="picture[image_alt]" required>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-4 control-label">顏色</label>
              <div class="col-md-8">
                <select class="form-control" name="picture[associated_template_color_relation_sn]" id="photoColor">
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="email" class="col-lg-4 control-label">排序</label>
              <div class="col-lg-8">
                <input type="text" class="form-control" name="picture[sort_order]" value="<?php echo (@$Item['sort_order']) ? @$Item['sort_order']:'9999'; ?>" required>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-4 control-label">選擇檔案</label>
              <div class="col-md-8">
                <input class="form-control" name="photo" type="file" required>
              </div>
            </div>
          </fieldset>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal">Close</button>
        <button class="btn btn-primary">上傳</button>
      </div>
    </div>
				<input type="hidden" name="picture[product_template_sn]" value="<?php echo @$Item['product_template_sn']; ?>">
        </form>
  </div>
</div>

<div class="modal fade" id="uploadModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
  <div class="modal-dialog" style="width:900px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel2">上傳圖片</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-without-legend" role="form" method="post" id="divider_form" enctype="multipart/form-data">
          <fieldset>
            <div class="form-group">
              <label for="email" class="col-lg-2 control-label">區域 <span class="require">*</span></label>
              <div class="col-md-10 radio-list">
                <label class="radio-inline">
                	<input type="radio" class="radio-list" name="divider[associated_area]" value="album" required>精彩影音
                </label>
                <label class="radio-inline">
                	<input type="radio" class="radio-list" name="divider[associated_area]" value="story" required>愛的故事
                </label>
                <label class="radio-inline">
                	<input type="radio" class="radio-list" name="divider[associated_area]" value="invitation" required>婚禮訊息
                </label>
                <label class="radio-inline">
                	<input type="radio" class="radio-list" name="divider[associated_area]" value="rsvp" required>出席回覆
                </label>
                <label class="radio-inline">
                	<input type="radio" class="radio-list" name="divider[associated_area]" value="guestbook" required>留言祝福
                </label>
                <label class="radio-inline">
                	<input type="radio" class="radio-list" name="divider[associated_area]" value="thanks" required>感謝有您
                </label>
                <label class="radio-inline">
                	<input type="radio" class="radio-list" name="divider[associated_area]" value="cowndown" required>婚禮倒數
                </label>
                <label class="radio-inline">
                	<input type="radio" class="radio-list" name="divider[associated_area]" value="gift" required>線上禮金
                </label>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">文案</label>
              <div class="col-md-10">
			                  <textarea class="form-control" rows="5" name="divider[default_divider_greeting_text]"><?php echo @$Item['description']; ?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-2 control-label">選擇檔案</label>
              <div class="col-md-10">
                <input class="form-control" name="dividerphoto" type="file" required>
              </div>
            </div>
          </fieldset>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal">Close</button>
        <button class="btn btn-primary">上傳</button>
      </div>
    </div>
				<input type="hidden" name="divider[product_template_sn]" value="<?php echo @$Item['product_template_sn']; ?>">
        </form>
  </div>
</div>