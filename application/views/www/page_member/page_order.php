<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="container mob_padding">
<!-- Page level plugin styles START -->
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<style type="text/css">
  .select2_dealer{
    padding: 0px;
  }
#table_member th,td
{
    text-align:center;
    vertical-align:middle;
}
</style>
<!-- Page level plugin styles END -->
<div class="container mob_padding">
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <li class="active">訂單查詢</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar col-md-2 col-sm-3">
    <div class="desktop_view_sidebar">
		<ul class="list-group margin-bottom-25 sidebar-menu">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
	<div class="mobile_view_sidebar">
		<div class="side-nav-head border_title_bottom">
			<button class="fa fa-bars collapsed" data-toggle="collapse" data-target="#mob_sidebar_drop_down"></button>
			<h4 class="bfont">會員中心<!--會員中心--></h4>
		</div>
		<ul class="list-group margin-bottom-25 sidebar-menu collapse" id="mob_sidebar_drop_down">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="col-md-10 col-sm-10 margin-bottom-30 product_right border_left minheight">
<?php $this->load->view('www/general/flash_error');?>
<!--訂單列表開始 -->
<table class="table-hover responsive">
  <thead>
    <tr class="page_odr_list_hdr">
	    <th style="display:none;">交易序號</th>
	    <th>訂單編號</th>
	    <th style="width: 90px;">交易時間</th>
	    <th>賣家</th>
	    <th>實付總額</th>
	    <th>訂單狀態</th>
	    <th>付款方式<br>/狀態</th>
	    <th style="width: 94px;">配送方式<br>/狀態</th>
	    <th>功能</th>
    </tr>
            </thead>
            <tbody>
              <?php if($orders){ foreach($orders as $key=>$_Item){ foreach($_Item['sub_order'] as $key2=>$_Item2){?>
                <tr class="page_odr_list_td">
                	<?php if($_Item2['order_sn']!=@$last_order_sn){?>
                  <td style="display:none;" class="mob_title2" data-title="交易序號" data-label="交易序號" rowspan="<?php echo $_Item['sub_count'];?>"><?php echo $_Item['order_sn'];?></td>
                  <?php $last_order_sn=$_Item2['order_sn'];$last_payment_method_name=$_Item['payment_method_name'];}?>
                  <td class="mob_title2" data-title="訂單編號" data-label="訂單編號"><?php echo $_Item2['sub_order_num'];?></td>
                  <td class="mob_title2" data-title="交易時間" data-label="交易時間"><small><?php echo $_Item['order_create_date'];?></small></td>
                  <td class="mob_title2" data-title="賣家" data-label="賣家"><?php echo $_Item2['brand'];?><?php if($_Item2['wedding_website_sn']){?><Br>編號：<?php echo $_Item2['wedding_website_sn'];}?></td>
                  <td class="mob_title2" data-title="實付總額" data-label="實付總額"><?php echo number_format($_Item2['sub_sum']);?></td>
                  <td class="mob_title2" data-title="訂單狀態" data-label="訂單狀態"><?php echo $_Item2['sub_order_status_name'];?></td>
                  <td class="mob_title2" data-title="付款方式" data-label="付款方式"><?php echo $last_payment_method_name;?><br><?php echo $_Item2['payment_status_name'];?>
                  </td>
                  <td class="mob_title2" data-title="配送狀態" data-label="配送狀態"><?php echo $_Item2['delivery_method_name'];?><br><?php echo $_Item2['delivery_status_name'];?></td>
                  <td class="text-center mob_title2" data-title="功能" data-label="功能">
	    			<div class="odr_list_btn">
<?php if($_Item2['sub_order_status']==1 && $_Item2['delivery_status']!=2 && $this->ijw->date_diff_days($_Item2['designated_delivery_date'])<=10){?>
	                  	<a class="btn btn-sm btn-primary expand cancel_order" href="javascript:void(0);">申請退訂</a>
<?php }?>

	                    <a sub_order_num="<?php echo $_Item2['sub_order_num'];?>" supplier_sn="<?php echo $_Item2['supplier_sn'];?>" sub_order_sn="<?php echo $_Item2['sub_order_sn'];?>" class="btn btn-sm btn-primary ticket_form" data-toggle="modal" href="#ticket_form">聯絡客服</a>
	                    <a class="lightbox btn btn-sm btn-primary" href="member/memberOrderDetail/<?php echo $_Item2['order_sn'];?>/<?php echo $_Item2['sub_order_sn'];?>">交易明細</a>
                    </div>
                  </td>
                </tr>
<?php if($_Item2['sub_order_status']==1 && $_Item2['delivery_status']!=2 && $this->ijw->date_diff_days($_Item2['designated_delivery_date'])<=10){?>
                <tr class="sonorder collapse">
                	<td colspan="11">
<form id="form<?php echo $_Item2['sub_order_sn'];?>" method="POST">
	<table class="pull-left col-md-12 padding0 lastcoltab">
						            <thead>
						              <tr>
						                <th>選擇</th>
						                <th>品名</th>
						                <th>單價</th>
						                <th>數量</th>
					                    <!--th>折扣</th>
					                    <th>電子禮金</th-->
						                <th>退訂總額</th>
						                <th>*是否收到貨</th>
						                <th>*退訂原因</th>
						              </tr>
						            </thead>
		<tbody class="page_ord_tbody">
						              <?php if($_Item2['order_item']){
						               foreach($_Item2['order_item'] as $key3=>$_Item3){?>
						                <tr class="odd gradeX">
						                  <td class="mob_title2" data-title="選擇">
		<input type="checkbox" associated_order_item_sn="<?php echo $_Item3['associated_order_item_sn'];?>" actual_sales_amount="<?php echo $_Item3['actual_sales_amount'];?>" order_item_sn="<?php echo $_Item3['order_item_sn'];?>" sub_order_sn="<?php echo $_Item2['sub_order_sn'];?>" id="tab_check<?php echo $_Item3['order_item_sn'];?>" class="order_item_sn fdcheck tblastcheck" name="order_item_sn[]" value="<?php echo $_Item3['order_item_sn'];?>" <?php echo ($_Item3['order_cancel_sn'])?'checked':''?> >
		<label class="sphov" for="tab_check<?php echo $_Item3['order_item_sn'];?>"></label></td>
						                  <td class="mob_title2" data-title="品名"><?php echo $_Item3['product_name'];?></td>
						                  <td class="mob_title2" data-title="單價"><?php echo $_Item3['sales_price'];?></td>
						                  <td class="mob_title2" data-title="數量"><?php echo $_Item3['buy_amount'];?></td>
					                    <!--td class="mob_title2" data-title="折扣"><?php echo $_Item3['coupon_code_cdiscount_amount'];?></td>
					                    <td class="mob_title2" data-title="電子禮金"><?php echo $_Item3['gift_cash_discount_amount'];?></td-->
						                  <td class="mob_title2" data-title="退訂總額" id="total_<?php echo $_Item3['order_item_sn'];?>"><?php echo ($_Item3['return_amount'])? $_Item3['return_amount']:'0';?></td>
						                  <td class="mob_title2" data-title="*是否收到貨">
						                  <input class="pgord_radiobtn padding0 col-md-3 pull-left" id="pgrd<?php echo $_Item3['order_item_sn'];?>" name="ifget_<?php echo $_Item3['order_item_sn'];?>" <?php echo ($_Item3['receive_goods_flag']=='1')?'checked':''?> value="1" type="radio">
						                  <label class="pgord_label col-md-3 padding0 pull-left" for="pgrd<?php echo $_Item3['order_item_sn'];?>" >是</label>
						                  <input class="pgord_radiobtn padding0 col-md-3 pull-left" id="pgrdno<?php echo $_Item3['order_item_sn'];?>" <?php echo ($_Item3['receive_goods_flag']=='2')?'checked':''?> name="ifget_<?php echo $_Item3['order_item_sn'];?>" value="2" type="radio">
						                  <label class="pgord_label col-md-3 padding0 pull-left" for="pgrdno<?php echo $_Item3['order_item_sn'];?>" >否</label></td>
						                  <td class="mob_title2" data-title="*退訂原因">
						                      <select name="ccapply_code_<?php echo $_Item3['order_item_sn'];?>" id="ccapply_code<?php echo $_Item3['order_item_sn'];?>" class="form-control">
						                        <option value="">請選擇</option>
						                        <?php foreach($cancel_reason as $cr){
						                        	if($_Item3['order_cancel_reason_code']==$cr['order_cancel_reason_code']){
						                        		$ifselected='selected';
						                        	}else{
						                        		$ifselected='';
						                        	}
						                        	?>
						                        <option <?php echo $ifselected;?> value="<?php echo $cr['order_cancel_reason_code']?>"><?php echo $cr['order_cancel_reason_name']?></option>
						                        <?php }?>
						                      </select>
						                  </td>
						                </tr>
						            <input type="hidden" name="actual_sales_amount_<?php echo $_Item3['order_item_sn'];?>" value="<?php echo $_Item3['actual_sales_amount'];?>">
						            <input type="hidden" name="order_cancel_sn_<?php echo $_Item3['order_item_sn'];?>" value="<?php echo $_Item3['order_cancel_sn'];?>">
              						<?php }}?>
			</table>
						            <!--button onclick="confirm_cancel('form<?php echo $_Item2['sub_order_sn'];?>')" class="btn btn-default">送出</button></td-->
						            <?php if($_Item2['sub_order_status']==1){?>
						            <button style="margin-top: 5px;" sub_order_sn="<?php echo $_Item2['sub_order_sn'];?>" payment_status="<?=$_Item2['pay_status']?>" delivery_status="<?=$_Item2['delivery_status']?>" type="submit" id="submit_<?php echo $_Item2['sub_order_sn'];?>" class="submit btn btn-default">送出</button>
						        	<?php }?>
						            <input type="hidden" name="sub_order_sn" value="<?php echo $_Item2['sub_order_sn'];?>">
						            <input type="hidden" name="order_sn" value="<?php echo $_Item2['order_sn'];?>">
						            <input type="hidden" name="sub_sum" value="<?php echo $_Item2['sub_sum'];?>">
            </form>
                	</td>
                </tr>
              <?php }}}}?>
            </tbody>
          </table>
          <!-- page nav -->
            <div class="row" style="margin-top:20px;">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">筆數：<span id="wish_count"><?php echo $total;?></span> </div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
					<?php echo $page_links;?>
                </div>
              </div>
            </div>
          <!-- end of page nav -->
        </div>

  <!-- copy data -->
  <div class="col-md-10 col-sm-10 margin-bottom-30 product_right border_left minheight display_none">
                              <!--訂單列表開始 -->
<table class="table-hover responsive">
  <thead>
    <tr class="page_odr_list_hdr">
	  <th>下單日期</th>
      <th>訂單編號</th>
      <th>賣家</th>
	  <th>優惠後金額</th>
      <th>訂單狀態</th>
      <th>支付狀態</th>
      <th>配送狀態</th>
      <th width="90">狀態/操作</th>
    </tr>
  </thead>
  <tbody>
	  <tr class="page_odr_list_td">
	   <td class="mob_title2" data-title="訂單編號" data-label="訂單編號">
      <a class="lightbox" href="">2016102600036</a>
		</td>
	   <td class="mob_title2" data-title="下單日期" data-label="下單日期">
      2016-10-26  20:29</td>


     <td  class="mob_title2" data-title="賣家">new</td>
	  <td class="mob_title2" data-title="優惠後金額" data-label="優惠後金額">
      750</td>
	 <td class="mob_title2" data-title="訂單狀態" data-label="訂單狀態">
      未確認</td>

      <td class="mob_title2" data-title="支付狀態" data-label="支付狀態">
      等待付款</td>
      <td class="mob_title2" data-title="配送狀態" data-label="配送狀態">
      未發貨</td>

      <td class="mob_title2" data-title="狀態/操作" data-label="狀態/操作" class="t0">

    <div class="odr_list_btn">
		<a class="lightbox btn btn-primary" href="member/memberOrderListdetail" >訂單明細</a><br>
      <a href="/member/callcenter" class="btn btn-primary" style="margin-top:5px;">訂單詢問</a><br>


      <a class="btn btn-primary" data-toggle="collapse" data-target="#pTAbContnt" style="margin-top:5px;" id="showtb" >申請退訂</a><br>
	  </div>

                </td>
    </tr>
     </tbody>
</table>



	<div  id="pTAbContnt" class="collapse In page_orlast_box pull-left col-md-12 padding0">
	<table class="pull-left col-md-12 padding0 lastcoltab">
		<thead>
			<tr>
			<th> 選擇 </th>
			<th class="col-md-2 padding0"> 品名 </th>
			<th> 單價 </th>
			<th> 數量 </th>
			<th> 折扣 </th>
			<th> 電子禮金 </th>
			<th> 退訂總額 </th>
			<th> *是否收到貨 </th>
			<th> *退訂原因是否 </th>
			</tr>

		</thead>

		<tbody class="page_ord_tbody">
		<tr>
		<td class="mob_title2" data-title="選擇"> <input type="checkbox" id="tab3_check" class="fdcheck tblastcheck"/>
			<label class="sphov" for="tab3_check"></label>
			</td>
		<td class="col-md-2 padding0 mob_title2" data-title="品名"> 選擇 </td>
		<td class="mob_title2" data-title="單價">68 </td>
		<td class="mob_title2" data-title="數量">1 </td>
		<td class="mob_title2" data-title="折扣"> 0 </td>
		<td class="mob_title2" data-title="電子禮金"> 0 </td>
		<td class="mob_title2" data-title="退訂總額"> 0  </td>
		<td class="mob_title2" data-title="*是否收到貨">
		<input type="radio" id="pgrd1" name="pgord_radio" class="pgord_radiobtn padding0 col-md-3 pull-left">
		<label class="pgord_label col-md-3 padding0 pull-left" for="pgrd1" >是</label>

		<input type="radio" id="pgrd2" name="pgord_radio" class="pgord_radiobtn padding0 col-md-3 pull-left">
		<label for="pgrd2" class="pgord_label col-md-3 padding0 pull-left">否</label>
		</td>

		<td class="mob_title2" data-title="*退訂原因是否">
					<!--
					<select id="lasttbdrp">
						<option value="請選擇">請選擇</option>
						<option value="產品不符合預期">產品不符合預期</option>
						<option value="選錯產品">選錯產品</option>
						<option value="產品價格太貴">產品價格太貴</option>
						<option value="送達時間太久，不想等">送達時間太久，不想等</option>

					</select>-->
					<div class="tagsel_btn2" id="tagsel_btn2">
							<span class="tag_selection2">請選擇</span>
						</div>
						<div class="tagsel_drop2">
							<ul class="tag_ul2">
								<li class="main_li">
									<span class="li_item2" id="7" name="請選擇">請選擇</span>
								</li>
								<li class="main_li">
									<span class="li_item2" id="8" name="產品不符合預期">產品不符合預期</span>
								</li>
								<li class="main_li">
									<span class="li_item2" id="9" name="選錯產品">選錯產品</span>
								</li>
								<li class="main_li">
									<span class="li_item2" id="10" name="產品價格太貴">產品價格太貴</span>
								</li>
								<li class="main_li">
									<span class="li_item2" id="11" name="送達時間太久，不想等">送達時間太久，不想等</span>
								</li>

							</ul>
						</div>
		</td>
		</tr>
		</tbody>

	</table>
	 <input type="submit" value="送出" class="last_tb_button"/>
</div>

							<!-- /訂單列表結束 -->

							</div>
  <!-- copy data -->
  <!-- END CONTENT -->
</div>
<!-- END SIDEBAR & CONTENT -->
<!-- Modal for 明細 -->
<div class="modal fade" id="order_detail" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">商品明細 : 訂單編號2345682　</h4>
      </div>
      <div class="modal-body">
         <div class="form">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>商品名稱</th>
                  <th>商品pid</th>
                  <th>數量</th>
                  <th>費用</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=0;$i<8;$i++):?>
                  <tr class="odd gradeX">
                    <td>商品名稱商品名稱商品名稱商品名稱</td>
                    <td>pid</td>
                    <td>1</td>
                    <td>870</td>
                  </tr>
                <?php endfor;?>
              </tbody>
            </table>
         </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">關閉</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- Modal for 明細 -->
<!-- Modal for 表單填寫 ui更改-->
<div class="modal fade" id="apply_form" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">訂單取消申請</h4>
      </div>
      <div class="modal-body">
         <div class="form">
          <form role="form">
            <div class="form-body">
              <div class="form-group">
                <label>訂單編號 : d123456789</label>
              </div>
              <div class="form-group">
                <label>退訂原因 <span class="require">*</span></label>
                <div class="input-icon">
                  <textarea class="form-control" rows="5"></textarea>
                </div>
              </div>
            </div>
          </form>
         </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">關閉</button>
        <button type="button" class="btn blue">送出表單</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
</div> <!-- container -->
</div>
<!-- Modal for 表單填寫 -->
      <!-- Modal for 表單填寫 -->
       <form role="form" name="ticket_form" method="post" action="member/addCallcenter">
      <div class="modal fade" id="ticket_form" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">請說明您的問題</h4>
            </div>
            <div class="modal-body">
               <div class="form">

                  <div class="form-body">
                    <div class="form-group">
                      <label>問題主題<span class="require">*</span></label>
                      <div class="input-icon">
                        <i class="fa fa-question-circle"></i>
                        <input type="text" required name="question_subject" class="form-control" placeholder="請輸入您的問題主題">
                      </div>
                    </div>
                   <div class="form-group">
                      <label>訂單編號</label>
                      <div class="input-icon">
                        <i class="fa fa-file-o"></i>
                        <input type="text" name="sub_order_num" id="sub_order_num" class="form-control" placeholder="若您欲詢問的問題與訂單有關，請您儘可能提供">
                        <input type="hidden" name="associated_sub_order_sn" id="associated_sub_order_sn">
                        <input type="hidden" name="supplier_sn" id="supplier_sn">
                      </div>
                    </div>
                    <div class="form-group">
                      <label>問題分類</label>
                      <select name="ccapply_code" id="ccapply_code" required class="form-control">
                        <option value="">請選擇</option>
                        <?php foreach($ccapply as $_Item){?>
                        <option <?php echo ($_Item['ccapply_name']=='訂單問題')?'selected':'';?> value="<?php echo $_Item['ccapply_code']?>"><?php echo $_Item['ccapply_name']?></option>
                        <?php }?>
                      </select>
                    </div>
                    <div class="form-group ccapply_detail">
                      <label>問題類型</label>
                      <select name="ccapply_detail_code" id="ccapply_detail_code" required class="form-control">
                        <option value="">請先選擇大類</option>
                      </select>
                    </div>
                    <!-- <div class="form-group">
                      <label>附加檔案</label>
                      <input type="file" id="exampleInputFile1">
                      <p class="help-block">
                         ※ 檔案大小2MB為限，附件格式僅接受jpeg、png。
                      </p>
                    </div>-->
                    <div class="form-group">
                      <label>問題說明 <span class="require">*</span></label>
                      <div class="input-icon">
                        <textarea required name="question_description" class="form-control" rows="5"></textarea>
                      </div>
                    </div>
                  </div>

               </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn default" data-dismiss="modal">關閉</button>
              <button type="submit" id="ticket_submit" class="btn blue">送出表單</button>
            </div>
          </div>
          </form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- Modal for 表單填寫 -->
<style>


</style>