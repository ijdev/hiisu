<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<link rel="stylesheet" href="public/metronic/global/plugins/jquery-treegrid/css/jquery.treegrid.css">
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品分類 <small>分類列表</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/admin">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a title="商品分類" href="admin/product/productCategory">商品分類</a>
        <i class="fa fa-circle"></i>
      </li>
<?php if(@$current_category['channel_name']){?>
      <li>
         <?php echo @$current_category['channel_name'];?>
        <i class="fa fa-circle"></i>
      </li>
<?php }?>
<?php if(@$upper_current_category2['category_name']){?>
      <li>
        <a title="<?php echo @$upper_current_category2['category_name'];?>" href="admin/product/productCategory/<?php echo @$upper_current_category2['category_sn'];?>"><?php echo @$upper_current_category2['category_name'];?></a>
        <i class="fa fa-circle"></i>
      </li>
<?php }?>
<?php if(@$upper_current_category1['category_name']){?>
      <li>
        <a title="<?php echo @$upper_current_category1['category_name'];?>" href="admin/product/productCategory/<?php echo @$upper_current_category1['category_sn'];?>"><?php echo @$upper_current_category1['category_name'];?></a>
        <i class="fa fa-circle"></i>
      </li>
<?php }?>
      <li class="active">
				<?php if(@$current_category['category_name']){ echo @$current_category['category_name'].' - 子分類管理'; }else{ ?>分類管理<?php }?>
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase"><?php if(@$current_category['category_name']){ echo @$current_category['category_name'].' - 子分類列表'; }else{ ?>分類列表<?php }?></span>
            </div>
            <div class="actions btn-set pull-right">
              <a href="admin/product/productCategoryItem/0/<?php echo @$current_category['category_sn'];?>" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
            <!--div class="col-md-2 pull-right">
            	<?php echo $Store;?>
            </div-->
          </div>
          <div class="portlet-body">
            <table id="table_member" class="tree-category table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th>分類名稱</th>
                  <th>分類編號</th>
                  <th>排序</th>
                  <th>館別</th>
                  <th>發佈</th>
                  <th>功能</th>
                </tr>
              </thead>
<?php if(@$CategoryItems){foreach(@$CategoryItems as $CI){?>
              <tr class="treegrid-<?php echo $CI['category_sn'];?><?php if($CI['upper_category_sn']){?> treegrid-parent-<?php echo $CI['upper_category_sn'];}?>">
                <td><a title="觀看下層分類" href="admin/product/productCategory/<?php echo $CI['category_sn'];?>"><?php echo $CI['category_name'];?></a></td>
                <td><?php echo $CI['category_sn'];?></td>
                <td><?php echo $CI['sort_order'];?></td>
                <td><?php echo $CI['channel_name'];?></td>
                <td><?php if($CI['category_status']){?><i class="fa fa-check"></i><?php }else{ ?><i class="fa fa-times"></i><?php }?></td>
                <td>
                  <a href="admin/product/productCategoryItem/<?php echo $CI['category_sn'];?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
<?php if(stripos($this->ijw->check_permission(1,$this->router->fetch_class(),$this->router->fetch_method()),'del')){?>
                  <a href="javascript:void(0);" class="deleteItem" ItemId="<?php echo $CI['category_sn'];?>"><i class="fa fa-trash-o"></i>刪除</a>
<?php }?>
                </td>
              </tr>
<?php }}?>
            </table>
              註:若分類有關聯商品或屬性，則無法刪除。
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->