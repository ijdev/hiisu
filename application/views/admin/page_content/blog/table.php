<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
//var_dump($this->page_data['permission']);
$_associated_dealer_sn=$this->session->userdata['member_data']['associated_dealer_sn'];
$_associated_supplier_sn=$this->session->userdata['member_data']['associated_supplier_sn'];
switch($this->session->userdata('member_data')['member_level_type']){
  case '9':
    $_del_chk=false; //系統管理員以外均須檢查
  break;
  default:
    $_del_chk=true;
  break;
}
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>部落格內容 <small>文章列表</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">文章列表</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/content/contentBlogItem" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
      		<form class="form-horizontal" role="form" method="get">
          <div class="table-actions-wrapper pull-right">
            <span>
            </span>
            <!--input name="keyword" type="text" class="pagination-panel-input form-control input-inline input-sm" size="10" maxlenght="10" style="margin: 0 5px;" placeholder="關鍵字"-->
            <select name="content_type_sn" id="category" class="table-group-action-input form-control input-inline input-sm">
              <option value="">內容分類</option>
<?php if($content_types){ foreach($content_types as $_Item){
	$ifselected=($content_type_sn==$_Item['content_type_sn']) ?'selected':'';
	echo '<option '.$ifselected.' value="'.$_Item['content_type_sn'].'">'.$_Item['content_title'].'</option>';
}}?>
            </select>
            <select name="post_flag" id="post_flag" class="table-group-action-input form-control input-inline input-small input-sm">
              <option value="">是否發佈</option>
              <option value="1" <?php echo($post_flag=='1') ?'selected':'';?>>已發佈</option>
              <option value="2" <?php echo($post_flag=='2') ?'selected':'';?>>未發佈</option>
              <option value="9" <?php echo($post_flag=='9') ?'selected':'';?>>已刪除</option>
            </select>
            <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button> &nbsp; 
            <button id="reset_form" onclick="this.form.flg.value='reset';" class="btn btn-sm purple"><i class="fa"></i> 清除</button>
          </div>
          <input name="flg" type="hidden" value="">
        </form>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>標題</th>
                  <th width="200">內容分類</th>
                  <th>商品分類</th>
                  <th width="80">代表圖</th>
                  <!--th>內容</th-->
                  <th width="100">排序</th>
                  <th width="80">是否發佈</th>
                  <th width="160">功能</th>
                </tr>
              </thead>
              <tbody>
								<?php if($Items){ foreach($Items as $_Item){?>
                  <tr class="odd gradeX">
                    <td><?php echo $_Item['blog_article_titile'];?><a href="home/blogItem/<?php echo $_Item['blog_article_sn'];?>" target="_blank">&nbsp;<i class="fa fa-external-link"></i></a></td>
                    <td>
                    	<?php echo $_Item['type_names'];?>
 										<!--<?php if($content_types){foreach($content_types as $key=>$ct){ 
                      echo(strrpos($_Item['associated_content_type_sn'], $ct['content_type_sn'])!==false) ? $ct['content_title'].'&nbsp;':'';
                    }}?>-->
                    </td>
                    <td><?php echo $_Item['post_category_sn_set'];?></td>
                    <td><?php if($_Item['blog_article_image_save_dir']):?><img src="/public/uploads/<?php echo @$_Item['blog_article_image_save_dir'];?>" width="60" ><?php else:?>無<?php endif;?></td>
                    <!--td>
											<?php echo $this->ijw->truncate($_Item['blog_article_content'],30);?>
                    </td-->
                    <td class="center"><?php echo $_Item['sort_order'];?></td>
                    <td class="center">
                      <?php if($_Item['post_flag']=='1'):?>
                        <i class="fa fa-check"></i>
                      <?php else:?>
                        <i class="fa fa-times"></i>
                      <?php endif;?>
                    </td>
                    <td class="center">
                      <a href="admin/content/contentBlogItem/<?php echo $_Item['blog_article_sn'];?>"><i class="fa fa-edit"></i>編輯</a>&nbsp;&nbsp;
                      <a target="_blank" href="home/blogItemPreview/<?php echo $_Item['blog_article_sn'];?>"><i class="fa fa-eye"></i>預覽</a>&nbsp;&nbsp;
    <?if(stripos($this->page_data['permission'],'del')!==false){?>
      <?if(!$_del_chk && ($_Item['associated_dealer_sn']==$_associated_dealer_sn || $_Item['associated_supplier_sn']==$_associated_supplier_sn)){?>
                  <a href="javascript:void(0);" class="deleteItem" ItemId="<?php echo $_Item['blog_article_sn'];?>"><i class="fa fa-trash-o"></i>刪除</a>
      <?}?>
    <?}?>
                    </td>
                  </tr>
                <?php }}?>
              </tbody>
            </table>

            <!--div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                  <ul class="pagination" style="visibility: visible;">
                    <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                    <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li class="active"><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                    <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                  </ul>
                </div>
              </div>
            </div-->
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->