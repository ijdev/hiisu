<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ijw{
	private $CI;

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->lang->load('general', 'zh-TW');
	}
	public function nf_to_wf($strs, $types=0){  //全形半形轉換
	    $nft = array(
	        "(", ")", "[", "]", "{", "}", ".", ",", ";", ":",
	        "-", "?", "!", "@", "#", "$", "%", "&", "|", "\\",
	        "/", "+", "=", "*", "~", "`", "'", "\"", "<", ">",
	        "^", "_",
	        "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
	        "a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
	        "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
	        "u", "v", "w", "x", "y", "z",
	        "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
	        "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
	        "U", "V", "W", "X", "Y", "Z",
	        " "
	    );
	    $wft = array(
	        "（", "）", "〔", "〕", "｛", "｝", "﹒", "，", "；", "：",
	        "－", "？", "！", "＠", "＃", "＄", "％", "＆", "｜", "＼",
	        "／", "＋", "＝", "＊", "～", "、", "、", "”", "＜", "＞",
	        "︿", "＿",
	        "０", "１", "２", "３", "４", "５", "６", "７", "８", "９",
	        "ａ", "ｂ", "ｃ", "ｄ", "ｅ", "ｆ", "ｇ", "ｈ", "ｉ", "ｊ",
	        "ｋ", "ｌ", "ｍ", "ｎ", "ｏ", "ｐ", "ｑ", "ｒ", "ｓ", "ｔ",
	        "ｕ", "ｖ", "ｗ", "ｘ", "ｙ", "ｚ",
	        "Ａ", "Ｂ", "Ｃ", "Ｄ", "Ｅ", "Ｆ", "Ｇ", "Ｈ", "Ｉ", "Ｊ",
	        "Ｋ", "Ｌ", "Ｍ", "Ｎ", "Ｏ", "Ｐ", "Ｑ", "Ｒ", "Ｓ", "Ｔ",
	        "Ｕ", "Ｖ", "Ｗ", "Ｘ", "Ｙ", "Ｚ",
	        "　"
	    );

	    if ($types == '1'){
	        // 轉全形
	        $strtmp = str_replace($nft, $wft, $strs);
	    }else{
	        // 轉半形
	        $strtmp = str_replace($wft, $nft, $strs);
	    }
	    return $strtmp;
	}
	//截字
	public function truncate($string, $length = 30, $etc = '...',$break_words = false)
	{
	if ($length == 0)
	return '';
	$string = html_entity_decode(trim(strip_tags($string)), ENT_QUOTES, 'utf-8');
	for($i = 0, $j = 0; $i < strlen($string); $i++)
	{
	if($j >= $length)
	{
	for($x = 0, $y = 0; $x < strlen($etc); $x++)
	{
	if($number = strpos(str_pad(decbin(ord(substr($string, $i, 1))), 8, '0', STR_PAD_LEFT), '0'))
	{
	$x += $number - 1;
	$y++;
	}
	else
	{
	$y += 0.5;
	}
	}
	$length -= $y;
	break;
	}
	if($number = strpos(str_pad(decbin(ord(substr($string, $i, 1))), 8, '0', STR_PAD_LEFT), '0'))
	{
	$i += $number - 1;
	$j++;
	}
	else
	{
	$j += 0.5;
	}
	}
	for($i = 0; (($i < strlen($string)) && ($length > 0)); $i++)
	{
	if($number = strpos(str_pad(decbin(ord(substr($string, $i, 1))), 8, '0', STR_PAD_LEFT), '0'))
	{
	if($length < 1.0)
	{
	break;
	}
	@$result .= substr($string, $i, $number);
	$length -= 1.0;
	$i += $number - 1;
	}
	else
	{
	@$result .= substr($string, $i, 1);
	$length -= 0.5;
	}
	}
	@$result = htmlentities($result, ENT_QUOTES, 'utf-8');
	if($i < strlen($string))
	{
	@$result .= $etc;
	}
	return @$result;
	}
	/* 字元寬度截字
	 */
	public function truncate2($str,$num,$str2='...'){
			$start=0;
	     /****************************************************
	      * UTF-8一個中文字約兩個字元，英文是一個字元
	      ****************************************************/
	     $string = mb_strimwidth($str, $start, $num, $str2, 'UTF-8');

	     //if( mb_strwidth($str, "UTF-8") > $num ){
	    //  $string .= $str2;
	     //}

	     return $string;
	}

	public function _pid_check($id,$IsComp=0) //驗證身份證字號是否合法
    {
     $flag = false;
     $id = strtoupper($id); // 將英文字母全部轉成大寫
     $id_len = strlen($id); // 取得字元長度


     if($id_len <= 0) {
        return false;
        exit;
     }
     if ($id_len > 10) {
        return false;
        exit;
     }
     if ($id_len < 10 && $id_len > 0) {
        return false;
        exit;
     }

     //檢 查 第一個字母是否為英文字
     $id_sub1 = substr($id,0,1); // 從第一個字元開始 取得字串
     $id_sub1 = ord($id_sub1); // 回傳字串的acsii 碼
     if ($id_sub1 > 90 || $id_sub1 < 65) {
        return false;
        exit;
     }

     //檢 查 身份證字號的 第二個字元 男生或女生
     $id_sub2 = substr($id,1,1);

     if($id_sub2 !="1" && $id_sub2 != "2") {
        return false;
        exit;
     }

     for ($i=1;$i<10;$i++) {
        $id_sub3 = substr($id,$i,1);
        $id_sub3 = ord($id_sub3);
        if ($id_sub3 > 57 || $id_sub3 < 48) {
           $n=$i+1;
           return false;
           exit;
        }
     }

     $num=array("A" => "10","B" => "11","C" => "12","D" => "13","E" => "14",
     "F" => "15","G" => "16","H" => "17","J" => "18","K" => "19","L" => "20",
     "M" => "21","N" => "22","P" => "23","Q" => "24","R" => "25","S" => "26",
     "T" => "27","U" => "28","V" => "29","X" => "30","Y" => "31","W" => "32",
     "Z" => "33","I" => "34","O" => "35");

     $d1 = substr($id,0,1); // 從第一個字元開始 取得字串
     $n1=substr($num[$d1],0,1)+(substr($num[$d1],1,1)*9);
     $n2=0; //初使化
     for ($j=1;$j<9;$j++) {
        $d4=substr($id,$j,1);
        $n2=$n2+$d4*(9-$j);
     }
     $n3=$n1+$n2+substr($id,9,1);
     if(($n3 % 10)!= 0) {
        return false;
        exit;
     }
     return $this->error = true;

  }

  public function _pagination($method, $rows, $perpage = 10,$uri_segment=0,$kind=0){
      $config['base_url'] = base_url($method.'/');
      $config['total_rows'] = $rows;
      $config['per_page'] = $perpage;
		$config['page_query_string'] = TRUE;
		$config['use_page_numbers'] = TRUE;
		$config['reuse_query_string'] = TRUE;
      $config['uri_segment'] = $uri_segment;
      $config['query_string_segment'] = 'page';
      $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
      $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
      $config['next_link'] = '<i class="fa fa-angle-right"></i>';
      $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
      $config['last_tag_open'] = '<li>';
      $config['last_tag_close'] = '</li>';
      $config['first_tag_open'] = '<li>';
      $config['first_tag_close'] = '</li>';
      $config['next_tag_open'] = '<li>';
      $config['next_tag_close'] = '</li>';
      $config['prev_tag_open'] = '<li>';
      $config['prev_tag_close'] = '</li>';
      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';
      if($kind=='shopx'){
	      $config['cur_tag_open'] = '<li><span>';
	      $config['cur_tag_close'] = '</span></li>';
    	}else{
	      $config['cur_tag_open'] = '<li class="active"><a>';
	      $config['cur_tag_close'] = '</a></li>';
    	}
      $config['full_tag_open'] = '<ul class="paginationn pagination-simple pagination-sm">';
      $config['full_tag_close'] = '</ul>';
      return $config;
  }
  public function admin_pagination($method, $rows, $perpage = 10,$uri_segment=0,$kind=0){  
      $config['base_url'] = base_url($method.'/');  
      $config['total_rows'] = $rows;  
      $config['per_page'] = $perpage;
			$config['page_query_string'] = TRUE;
			$config['use_page_numbers'] = TRUE;
			$config['reuse_query_string'] = TRUE;
      $config['uri_segment'] = $uri_segment;
      $config['query_string_segment'] = 'page';
      $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
      $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
      $config['next_link'] = '<i class="fa fa-angle-right"></i>';
      $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
      $config['last_tag_open'] = '<li class="next">';
      $config['last_tag_close'] = '</li>';
      $config['first_tag_open'] = '<li class="prev">';
      $config['first_tag_close'] = '</li>';
      $config['next_tag_open'] = '<li class="next">';
      $config['next_tag_close'] = '</li>';
      $config['prev_tag_open'] = '<li class="prev">';
      $config['prev_tag_close'] = '</li>';
      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';
      if($kind=='shop'){
	      $config['cur_tag_open'] = '<li><span>';
	      $config['cur_tag_close'] = '</span></li>';
    	}else{
	      $config['cur_tag_open'] = '<li class="active"><a>';
	      $config['cur_tag_close'] = '</a></li>';
    	}
      $config['full_tag_open'] = '<ul class="pagination">';
      $config['full_tag_close'] = '</ul>';
      return $config;  
  }
  public function _write_product_history($product_sn){
  	$product_history=unserialize(get_cookie('product_history'));
  	//var_dump($product_history);
  	if($product_history){
			if(in_array($product_sn, $product_history) === false){
				//set_cookie('product_history['.count($product_history).']',$product_sn,time()+24*60*60);
				array_push($product_history,$product_sn);
				set_cookie('product_history',serialize($product_history),time()+24*60*60);
			}
		}else{
			$product_history=array($product_sn);
			set_cookie('product_history',serialize($product_history),time()+24*60*60);
		}
	}
	/*
	從臉書粉絲頁網址取得id
	*/
	public function get_facebook_page_id($facebookUrl)
	{
	    $facebookId = null;
			$options = array(
			  'http'=>array(
			    'method'=>"GET",
			    'header'=>"Accept-language: zh-Tw\r\n" .
			              "Cookie: foo=bar\r\n" .  // check function.stream-context-create on php.net
			              "User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.102011-10-16 20:23:10\r\n" // i.e. An iPad
			  )
			);

			$context = stream_context_create($options);
	    $fbResponse = @file_get_contents($facebookUrl, false, $context);
	    if($fbResponse)
	    //echo $fbResponse;
	    {
	        $matches = array();
	        if (preg_match('/"entity_id":"([0-9])+"/', $fbResponse, $matches))
	        {
	            $jsonObj = json_decode("{" . $matches[0] . "}");
	            if($jsonObj)
	            {
	                $facebookId = $jsonObj->entity_id;
	            }
	        }
	    }
	    return $facebookId;
	}
	/*
	get_access_token
	*/
	private function _get_fb_access_token() {
	 $token_url = "https://graph.facebook.com/oauth/access_token?" .
	 "client_id=" . FACEBOOK_APP_ID .
	 "&client_secret=" . FACEBOOK_APP_SECRET .
	 "&grant_type=client_credentials";
	 $access_token = json_decode(file_get_contents($token_url))->access_token;
	 return $access_token;
	}
	/*
	 "&grant_type=client_credentials";
	get_articles
	*/
	public function get_fb_fans_article($fans_id,$limit=2) {
		if($this->CI->session->userdata('access_token')){
    	//var_dump($this->CI->session->userdata('access_token'));exit;
			$access_token=$this->CI->session->userdata('access_token');
		}else{
			$access_token = $this->_get_fb_access_token();
			if($access_token){
				$this->CI->session->set_userdata('access_token',$access_token);
			}else{
				return false;
			}
		}
//var_dump($access_token);
		//$graph_url="https://graph.facebook.com/{$fans_id}/?fields=posts{comments.limit(1).summary(true),likes.limit(1).summary(true),shares,message,attachments}&limit={$limit}&{$access_token}";
		$graph_url="https://graph.facebook.com/{$fans_id}?fields=access_token";

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $graph_url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		$result = curl_exec($ch);
    	//var_dump($result);exit;

		curl_close($ch);
		return json_decode($result,true);
	}
	public function get_array_from_curl($_url,$_data_array){
			$curl = curl_init($_url);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $_data_array);

			$json_response = curl_exec($curl);

			//$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			//var_dump(curl_error($curl));
			//if ( $status != 201 ) {
			    //die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
			//}

			curl_close($curl);
			//var_dump($json_response);
			//$json_response = mb_convert_encoding($json_response,'UTF-8','UTF-8');
			//$json_response=substr($json_response, 3); //2016-5-4 新加原因不明
			//echo $json_response;
			$response = json_decode($json_response, true);
			//var_dump(json_last_error_msg());
			//var_dump($response);
			//exit();
			return $response;
	}
  /*
  送信
  */
  public function send_mail($_system_email_type="系統訊息",$_user_name="",$_member_sn="",$message="",$sub_order_num="",$supplier_name="",$question_description="")
  {
    $config['protocol'] = 'smtp';
    $config['smtp_host'] = 'ssl://smtp.mailgun.org';
    $config['smtp_user'] = 'postmaster@mail.hiisu.shop';
    $config['smtp_pass'] = 'b1b5f69fc75f979b249939fad772344f-e470a504-27ba4677';
    $config['smtp_port'] = '465';
    $config['smtp_timeout'] = '5';
    $config['mailtype'] = 'html';
    $config['newline'] = "\r\n";
    $config['crlf'] = "\r\n";
    $config['charset']   = 'utf-8';
    $sender_email=$config['smtp_user'];
    $sender_name='囍市集';

    $_system_email_type=urldecode($_system_email_type);

   					$_shopname="囍市集";
						$_username="";
						$_newpass="";
						//$_site_url="http://".$_SERVER['HTTP_HOST'];
						$_truename="";
						//$_url_temp=$_site_url."/member/register";
						$_url_temp=base_url('/member/register');

						$data_tmp=$_user_name."=ijwedding";

						$base_64 = rtrim(base64_encode($data_tmp ),'=');

						$_register_url=$_url_temp."/".$base_64;

		$this->CI->load->model('sean_models/Sean_db_tools');

 			//var_dump($_member_sn);
    if(strlen($_user_name) > 0 ){
  	 	$_where=" where user_name='".$_user_name."' ";
		$_result01=$this->CI->Sean_db_tools->db_get_max_record("user_name,last_name,first_name,email,count_forgot_password","ij_member",$_where);
		//var_dump($_result01);
	}elseif(strlen($_member_sn) > 0 ){
  	 	$_where=" where member_sn='".$_member_sn."' ";
		$_result01=$this->CI->Sean_db_tools->db_get_max_record("user_name,last_name,first_name,email,count_forgot_password","ij_member",$_where);
		//var_dump($_result01);
	}

				if(is_array($_result01))
				{
					foreach($_result01 as $key => $value)
			    {
			    	$_shopname="囍市集";
						$_username=$value->user_name;
						$_newpass=rand(111111, 999999);
						$_count_forgot_password=$value->count_forgot_password;
						$_count_forgot_password=$_count_forgot_password+1;
						$_site_url="http://".$_SERVER['HTTP_HOST'];
						$_truename=$value->last_name.$value->first_name;
						$_to_email=$value->email;
					}
				}
    if(!@$_to_email){
		 return false;
		 exit();
    }
    //passwd沒用到？
    $search  = array('@shopname@', '@username@', '@newpass@','@passwd@', '@site_url@', '@truename@','@register_ijw_url@','@message@','@orderid@','@provider_name@','@question@');

    $replace = array($_shopname, $_username, $_newpass, $_newpass, $_site_url,$_truename,$_register_url,$message,$sub_order_num,$supplier_name,$question_description);

    if(strlen($_system_email_type) > 0 )
    {
	    $_where=" where system_email_type='".$_system_email_type."' ";
			$_result01=$this->CI->Sean_db_tools->db_get_one_record("*","ij_system_email",$_where);
			if($_system_email_type=="忘記密碼")
			{
				$_where=" where user_name='".$_user_name."'";
				$_data=array();
				$_data["password"]=md5($_newpass);
				$_data["last_time_resend_password"]=date("Y-m-d H:i:s");
				$_data["count_forgot_password"]=$_count_forgot_password;
				$_result=$this->CI->Sean_db_tools->db_update_record($_data,"ij_member",$_where,'','');
			}

			if(is_array($_result01))
			{
				//更新最新寄送時間
				$_where=" where system_email_sn='".$_result01["system_email_sn"]."'";
				$_data=array();
				$_data["last_time_send"]=date("Y-m-d H:i:s");
				$_result=$this->CI->Sean_db_tools->db_update_record($_data,"ij_system_email",$_where,'','');
			    $this->CI->load->library ('email') ;
			    $this->CI->email->initialize($config);

				$__mail_template_contant=str_replace($search, $replace,$_result01["system_email_titile"]);
			    $__mail_template_notes=str_replace($search, $replace,$_result01["system_email_content"]);
			    //echo $__mail_template_notes;exit;
			    $this->CI->email->from($sender_email, $sender_name);
			    $this->CI->email->to($_to_email);
			    $this->CI->email->subject($__mail_template_contant);

			    $this->CI->email->message($__mail_template_notes);
			    $this->CI->email->send();
			    return true;
		  }else{
				 return false;
		  }
	  }else{
			return false;
	  }
  }
  /*
  寄送電子報
  */
  public function send_edm($_user_name="",$_to_email="",$subject="",$message="",$_header_array)
  {
    $config['protocol'] = 'smtp';
    $config['smtp_host'] = 'ssl://smtp.mailgun.org';
    $config['smtp_user'] = 'postmaster@ijwedding.com';
    $config['smtp_pass'] = 'b65515471851e4f2dcc75fb82d03a693';
    $config['smtp_port'] = '465';
    $config['smtp_timeout'] = '5';
    $config['mailtype'] = 'html';
    $config['newline'] = "\r\n";
    $config['crlf'] = "\r\n";
    $config['charset']   = 'utf-8';
    $sender_email='ijwedding98@gmail.com';
    $sender_name='愛結婚電子報';
    $this->CI->load->library ('email') ;
    $this->CI->email->initialize($config);

    $this->CI->email->from($sender_email, $sender_name);
    $this->CI->email->to($_to_email);
    $this->CI->email->subject($subject);
    $edm_send_list_sn_json=json_encode($_header_array);
    $this->CI->email->set_header('X-Mailgun-Variables',$edm_send_list_sn_json);
    $this->CI->email->message($message);
    $this->CI->email->send();
		return true;
  }
	public function make_links_clickable($text,$target='_blank'){
	    return preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a title="外部連結" target="$target" href="$1">$1</a>', $text);
	}
	public function date_diff_days($date1,$date2=''){
    $date1 = new DateTime($date1);
    if(!$date2){
    	$date2 = new DateTime("now");
  	}
    return $date1->diff($date2)->days;
 	}
	public function date_add_days($date,$days){
		$date = new DateTime($date);
		if($days>0){
			return $date->add(new DateInterval('P'.$days.'D'))->format('Y-m-d');
		}else{
			return $date->sub(new DateInterval('P'.abs($days).'D'))->format('Y-m-d');
		}
	}
   public function _wait($url,$second,$words = 0,$back=0){

        $data['setting'] = array(
            'url'       =>  $url,
            'second'    =>  $second,
            'words'     =>  ($words) ? $words : "動作完成!"
        );
        if ($back){
        	$words=str_replace(array("\r","\n"), '', $words);
        	//var_dump($words);
        	if($back==1){
				echo "<script type='text/javascript'>alert('".$words."');history.back(1);</script>";
			}elseif($back==2){
				echo "<script type='text/javascript'>alert('".$words."');location.href='".$url."'</script>";
			}else{
        	//var_dump($url);
				echo "<script type='text/javascript'>alert('".$words."');window.opener.location.reload();window.close();</script>";
			}
			exit();
        }else{
			$this->CI->session->set_flashdata("fail_message",$words);
			//echo "<script type='text/javascript'>history.back(1);</script>";
        	redirect($url);
        	//$this->load->view("admin/wait",$data);
        	//$view=$this->load->view("admin/wait",$data,true);
			//$this->output->set_output($view);
			//exit();
        }

    }
    public function check_permission($return_current_page_set=0,$target_method=0,$target_class=0){
		if($this->CI->session->userdata('member_role_system_rules') && $this->CI->session->userdata('member_login')== true && $this->CI->session->userdata('member_data')['member_level_type'] >= 5) {
				$default_permission_pages=array('main/managerSignOut','login','main/index','main/managerSignAction','main/role_select','main/member_roles','admin/notification','admin/get_unread_message_count');
				if(!$target_method){
					$target_method=$this->CI->router->fetch_method();
					$target_class=$this->CI->router->fetch_class();
				}
				//&& !$this->CI->input->is_ajax_request() 先拿掉測試
				if(!in_array($target_class.'/'.$target_method,$default_permission_pages)  ){
									//var_dump(in_array($target_class.'/'.$target_method,$default_permission_pages));
					//$this->LoginCheck();
				  if($this->CI->session->userdata('member_role_system_rules')[0]['role_sn']=='1'){ //超級管理員
						if($return_current_page_set){
							$permission='add、edit、update、del、delete、search、print、export';
						}else{
							$permission=true;
						}
				    }else{
						$permission=false;
			//var_dump($this->CI->session->userdata('member_role_system_rules'));
			//exit();
						foreach($this->CI->session->userdata('member_role_system_rules') as $mr){
							if($mr['sys_program_eng_name']){
								if(strpos($target_method,$mr['sys_program_eng_name'])!==false && $mr['system_rule']){
									//var_dump($target_class.'/'.$target_method);
									//exit;

									if($return_current_page_set){
										$permission=$mr['system_rule'];
									}else{
										$permission=true;
									}
								}elseif(strpos($target_class,$mr['sys_program_eng_name'])!==false && $mr['system_rule']){
									if($return_current_page_set){
										$permission=$mr['system_rule'];
									}else{
										$permission=true;
									}
								}
							}
						}
					}
				}else{
					$permission=true;
				}
		}else{
			$permission=false;
		}
		return $permission;
   }
	public function mask_words( $email, $mask_char="*", $percent=50 ) {
		//echo strpos($email, "@");
		if (strpos($email, "@") !== false) {
			list( $user, $domain ) = preg_split("/@/", $email );
			$at="@";
			//var_dump($email);
		}else{
			$user=$email;
			$domain='';
			$at='';
		}
		$len = mb_strlen( $user , 'utf-8' );
		$mask_count = floor( $len * $percent /100 );
		$offset = floor( ( $len - $mask_count ) / 2 );
		$masked = mb_substr( $user, 0, $offset )
		.str_repeat( $mask_char, $mask_count )
		.mb_substr( $user, $mask_count+$offset );
		return( $masked.$at.$domain );
	}
    public function get_member_number($member_sn) {
    	return ($member_sn)?'TA'.str_pad($member_sn,6,'0',STR_PAD_LEFT):'';
	}
    public function get_sys_program_name($sys_program_eng_name) {
		$this->CI->load->model('libraries_model');
		$_sys_program_eng_name=str_replace('Item','',$sys_program_eng_name);
		$_sys_program_eng_name=str_replace('item','',$_sys_program_eng_name);
		$sys_program_name=@$this->CI->libraries_model->_select('ij_sys_program_config',array('sys_program_eng_name'=>$_sys_program_eng_name),0,0,0,0,0,'row')['sys_program_name'];
		//echo $this->CI->db->last_query().'<br>';exit;
    	return ($sys_program_name)?$sys_program_name:$sys_program_eng_name;
	}
	//取得經銷id
    public function get_dealer_sn() {
		if(!$dealer_sn=$this->CI->session->userdata('dealer_sn')){
			$this->CI->load->helper('cookie');
			if ($dealer_sn=get_cookie(base64_encode('dealer_sn'))){
				$dealer_sn=(int)base64_decode($dealer_sn);
			$this->CI->session->set_userdata('dealer_sn',$dealer_sn);
		}else{
			return false;
			}
		}
		return $dealer_sn;
	}
	//取得聯盟id
    public function get_partner_sn(){
		$this->CI->load->helper('cookie');
		$this->CI->load->library('user_agent');
		if($partner_sn=base64_decode($this->CI->input->get('partner_sn'))){
	        $data = array(
	            'partner_sn' => $partner_sn,
	            'browser' => $this->CI->agent->browser(),
	            'browser_version' => $this->CI->agent->version(),
	            'mobile' => $this->CI->agent->mobile(),
	            'platform' => $this->CI->agent->platform(),
	            'http_referrer' => $this->CI->agent->referrer(),
	            'agent_string' => $this->CI->agent->agent_string(),
				'view_ip_addr' => $this->CI->input->ip_address()
	        );

	        $this->CI->db->insert('ij_partner_hit_log', $data);
			$this->CI->session->set_userdata('partner_sn',$partner_sn);
			set_cookie(md5('partner_sn'),base64_encode($partner_sn),time()+24*60*60);
		}elseif($partner_sn=@$this->CI->session->userdata['web_member_data']['partner_sn']){ //聯盟會員自己登入
			$this->CI->session->set_userdata('partner_sn',$partner_sn);
		}else{
			if(!$partner_sn=$this->CI->session->userdata('partner_sn')){
				if ($partner_sn=base64_decode(get_cookie(md5('partner_sn')))){
					$this->CI->session->set_userdata('partner_sn',$partner_sn);
				}else{
					return false;
				}
			}
		}
		return $partner_sn;
		/*if(!$this->session->userdata['web_member_data']['associated_partner_sn']){
			//無所屬聯盟會員寫入
	  		$this->Shop_model->_update('ij_member',array('associated_partner_sn'=>$partner_sn),'member_sn',$this->session->userdata['web_member_data']['member_sn']);
			$web_member_data=$this->session->userdata['web_member_data'];
			$web_member_data['associated_partner_sn']=$partner_sn;
			$this->session->set_userdata('web_member_data',$web_member_data);
		}*/
	}
}
