<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容的 page level js
*/
?>

<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
<script src="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>


<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/frontend/layout/scripts/layout.js" type="text/javascript"></script>
<script type="text/javascript">
var oldtext='';
jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init();
        Layout.initFixHeaderWithPreHeader();
        Layout.initNavScrolling();

		jQuery('body').on('click', '.cancel_order', function (e) {
			//console.log($(this).text());
			//if(oldtext!='取消') oldtext=$(this).text();
			var el = jQuery(this).parent().parent().parent().next();
			if (el.hasClass("collapse")) {
	            el.removeClass("collapse").addClass("expand");
	            //$(this).removeClass("btn-info").addClass("btn-warning");
	            $(this).text('取消');
	            //el.slideUp(200);
	        } else {
	            el.removeClass("expand").addClass("collapse");
	             //$(this).removeClass("btn-warning").addClass("btn-info");
	           $(this).text('申請退訂');
	            //el.slideDown(200);
	        }
		});
		jQuery('body').on('click', '.order_item_sn', function (e) {
	        var order_item_sn=$(this).attr('order_item_sn');
	        var sub_order_sn=$(this).attr('sub_order_sn');
	        var associated_order_item_sn=$(this).attr('associated_order_item_sn');
	        //console.log(associated_order_item_sn);
			if ($(this).prop('checked')) {
				//console.log($('#tab_check'+associated_order_item_sn).prop('checked'));
				$('#pgrdno'+order_item_sn).attr('required',true);
				$('#pgrd'+order_item_sn).attr('required',true);
				$('#ccapply_code'+order_item_sn).attr('required',true);
				if(associated_order_item_sn>0 && !$('#tab_check'+associated_order_item_sn).prop('checked')){
					//alert('加購商品必須跟隨主商品退貨');
					//return false;
				}else{
					$('#form'+sub_order_sn+' .order_item_sn').each(function() {
		                if ($(this).attr('associated_order_item_sn')==order_item_sn) {
		                	//$(this).prop('checked',true);
		                	$(this).click();
		                }
		            });
				}
	            var actual_sales_amount=$(this).attr('actual_sales_amount');
				$('#total_'+order_item_sn).html(actual_sales_amount);
				$('#submit_'+sub_order_sn).attr('disabled',false)
	        } else {
				$('#pgrd'+order_item_sn).attr('required',false);
				$('#pgrdno'+order_item_sn).attr('required',false);
				$('#ccapply_code'+order_item_sn).attr('required',false);
	        	if(associated_order_item_sn==0 || !associated_order_item_sn){
	        		//console.log(associated_order_item_sn);
					$('#form'+sub_order_sn+' .order_item_sn').each(function() {
		                if ($(this).attr('associated_order_item_sn')==order_item_sn) {
		                	//$(this).prop('checked',false);
		                	$(this).click();
		                }
		            });
				}
				$('#total_'+order_item_sn).html('0');
				$('#submit_'+sub_order_sn).attr('disabled',true)
	        }
		});
				/*jQuery('body').on('click', '.cancel', function (e) {
				        var el = jQuery(this).parent().parent().parent().parent().parent();
				        console.log(el.html());
					if (el.hasClass("collapse")) {
	            el.removeClass("collapse").addClass("expand");
	            //$(this).removeClass("btn-info").addClass("btn-warning");
	            //$(this).text('取消');
	            //el.slideUp(200);
	        } else {
	            el.removeClass("expand").addClass("collapse");
	             //$(this).removeClass("btn-warning").addClass("btn-info");
	           //$(this).text('退訂');
	            //el.slideDown(200);
	        }
				});*/
	  $('select[id="ccapply_code"]').change(function() {
	        var data = $(this).val(); // Get Selected Value
					get_data(data);
    });

		function get_data(data){
		    $.ajax({
		        url: 'Member/get_ccapply_detail',
		        data: 'data=' + data,
		        dataType: 'json',
		        success: function(result) {
		        	if(result){
			         	$('.ccapply_detail').show('fast');
			         	$('#ccapply_detail_code').empty();
						$('#ccapply_detail_code').attr('required',true);
						$(result).appendTo("#ccapply_detail_code");
					}else{
						$('#ccapply_detail_code').attr('required',false);
			         	$('.ccapply_detail').hide('fast');
					}
		        }
		    });
    }
	  $('select[id="ccapply_code"]').trigger('change');

	  $('.ticket_form').click(function() {
			//console.log($(this).attr('sub_order_sn'));
			$('#associated_sub_order_sn').val($(this).attr('sub_order_sn'));
			$('#supplier_sn').val($(this).attr('supplier_sn'));
			$('#sub_order_num').attr('readonly',true);
			$('#sub_order_num').val($(this).attr('sub_order_num'));
    });
	$('.submit').click(function() {
		if($(this).attr('delivery_status')=='1'){
			var if_check_all=true;
			//console.log($(this).attr('sub_order_sn'));
			$('#form'+$(this).attr('sub_order_sn')+' .order_item_sn').each(function() {
				//console.log($(this).prop('checked'));
                if (!$(this).prop('checked')) {
                	if_check_all=false;
                }
            });
			//console.log(if_check_all);
            if(!if_check_all){
            	alert('配送待確認訂單退訂必須全部退訂');
            	return false;
            }
		}
		if(!confirm('請確認是否申請退訂？（送出後將無法更改）')){
			return false;
		}
    });

});
	function confirm_cancel(form) {
		var order_item_sn = $('input[name="order_item_sn"]', form).val();
	  	return false;
	  if(confirm('請再次確認您要退訂該商品？')){
	  	//form.submit();
	  }
	}
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->