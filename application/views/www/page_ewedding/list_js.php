<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Home 主內容的 page level js
*/
?>

<script src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
<script src="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->
<script src="public/metronic/global/plugins/zoom/jquery.zoom.min.js" type="text/javascript"></script><!-- product zoom -->
<script src="public/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script><!-- Quantity -->

<!-- BEGIN LayerSlider -->
<script src="public/metronic/global/plugins/slider-layer-slider/js/greensock.js" type="text/javascript"></script><!-- External libraries: GreenSock -->
<script src="public/metronic/global/plugins/slider-layer-slider/js/layerslider.transitions.js" type="text/javascript"></script><!-- LayerSlider script files -->
<script src="public/metronic/global/plugins/slider-layer-slider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script><!-- LayerSlider script files -->
<script src="public/metronic/frontend/pages/scripts/layerslider-init.js" type="text/javascript"></script>
<!-- END LayerSlider -->

<script src="public/metronic/frontend/layout/scripts/layout.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Layout.init();    
        Layout.initOWL();
        LayersliderInit.initLayerSlider();
        Layout.initImageZoom();
        Layout.initTouchspin();
        Layout.initTwitter();
        Layout.initFixHeaderWithPreHeader();
        Layout.initNavScrolling();

        jQuery('a.themeReviewBtn').hover(function(){
            jQuery(this).addClass('review');
        }, function(){
            jQuery(this).removeClass('review');
        });
        jQuery('a.themeReviewBtn').click(function(){
            var productID = jQuery(this).attr('rel');
            jQuery.fancybox({
                content : '<iframe src="home/templatePreview/'+productID+'" width="100%" height="100%" frameborder="0" style="margin:0px auto;"></iframe>',
                minWidth: '100%',
                minHeight:'75%',
                padding:0,
                margin:0,
                autoHeight: false,
                autoWidth:false,
                width:'100%',
                height:480
            });
            return false;
        });
				$('.add2cart').click(function(e) {
					var product_sn=$(this).attr('product_sn');
					var product_package_config_sn=$(this).attr('product_package_config_sn');
					console.log(product_package_config_sn);
					if(product_sn){
					 var url = "shop/add";
					    $.ajax({
					           type: "POST",
					           url: url,
										 dataType:'json',
					           data: { product_sn:product_sn,qty:'1',product_spec:product_package_config_sn },
					           success: function(data)
					           {
					           	if (data.error){
					           		alert(data.error);
					           	}else{
					           		$('.top-cart-info-count').text(data.total_items+' 件');
					           		$('.top-cart-info-value').text('$ '+data.total);
					           		//更新購物車popup
					           		$('.top-cart-content').load('shop/top_cart');
					           		//console.log(data.total);
					           	}
					           }
					         });
					  }
					    e.preventDefault();
				});        
				$('.shopaddcart').click(function(e) {
					var product_sn=$(this).attr('product_sn');
					if(product_sn){
					 var url = "shop/add";
					    $.ajax({
					           type: "POST",
					           url: url,
										 dataType:'json',
					           data: { product_sn:product_sn,qty:'1' },
					           success: function(data)
					           {
					           	if (data.error){
					           		alert(data.error);
					           	}else{
					           		$('.top-cart-info-count').text(data.total_items+' 件');
					           		$('.top-cart-info-value').text('$ '+data.total);
					           		location.href='/shop/shopView';
					           		//console.log(data.total);
					           	}
					           }
					         });
					  }
				});
    });
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->