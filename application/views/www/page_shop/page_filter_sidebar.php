<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 線上商店分類
*/

 							 $_2_name="";
 							 $_3_name="";
 							 $_1_name="";
 							 $_2_name_id="";
 							 $_3_name_id="";
 							 $_1_name_id="";

 							if(is_array($_category_array_left))
			        {
			        	foreach($_category_array_left as $key => $value)
			        	{
			        		if($value->category_sn == $_key_id && $value->category_status==1)
			        		{
			        			if($value->upper_category_sn){
						         $_3_name_id=$value->category_sn;
			        			 $_3_name=$value->category_name;
			        			 $_3_name_parent=$value->upper_category_sn;
						        	foreach($_category_array as $key => $value)
						        	{
						        		if($value->category_sn == $_3_name_parent && $value->category_status==1)
						        		{
						        			 $_2_name=$value->category_name;
						        			 $_2_name_id=$value->category_sn;
			        			       $_2_name_parent=$value->upper_category_sn;
			        			     }
						        	}
			        			 }else{
						        			 $_2_name=$value->category_name;
						        			 $_2_name_id=$value->category_sn;
			        			}
			        		}
			        	}
			        	if(@$_2_name_parent){
					        	foreach($_category_array as $key => $value)
					        	{
					        		if($value->category_sn == $_2_name_parent && $value->category_status==1)
					        		{
					        			 $_1_name=$value->category_name;
					        			 $_1_name_id=$value->category_sn;
					        		}
					        	}
					        }
        			}
?>

<div class="container">
<style>
  /*.pi-img-wrapper img{border:1px solid #EFEFEF;}*/
  .sale-product{background-color: #EFEFEF;padding: 12px;}
</style>
<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<link href="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="public/metronic/global/plugins/slider-layer-slider/css/layerslider.css" rel="stylesheet">
<link href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css"><!-- for slider-range -->
<!-- Page level plugin styles END -->

<ul class="breadcrumb">
    <li><a href="shop">結婚商店</a></li>
    <?php if($_1_name){?><li><a href="shop/shopCatalog/<?php echo $_1_name_id;?>"><?php echo $_1_name;?></a></li><?php }?>
    <?php if($_2_name){?><li><a href="shop/shopCatalog/<?php echo $_2_name_id;?>"><?php echo $_2_name;?></a></li><?php }?>
    <?php if($_3_name){?><li class="active"><a href="shop/shopCatalog/<?php echo $_3_name_id;?>"><?php echo $_3_name;?></a></li><?php }?>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->

<form id="form1" class="form-horizontal" role="form" method="get">
<div class="row margin-bottom-40">
  <!-- BEGIN CONTENT -->
  <div class="row margin-bottom-40 ">
    <!-- BEGIN SIDEBAR -->

 <div id="shopcetlog"  class="sidebar col-md-3 col-sm-4">
     <div class="sidebar-filter categories-menu">
		<h3>商品分類</h3>
		<ul class="categories_list">
			<li class="categories_list_sub_li" data-toggle="collapse" data-target="#side_menu1">商品分類 1
				<ul id="side_menu1" class="collapse">
					<li><a href="/shop/shopCatalog/41">商品分類 1.1</a></li>
					<li><a href="/shop/shopCatalog/42">商品分類 1.2</a></li>
					<li><a href="/shop/shopCatalog/43">商品分類 1.3</a></li>
					<li><a href="/shop/shopCatalog/47">商品分類 1.4</a></li>
				</ul>
			</li>
			<li class="categories_list_sub_li" data-toggle="collapse" data-target="#side_menu2">商品分類 2
				<ul id="side_menu2" class="collapse">
					<li><a href="/shop/shopCatalog/42">商品分類 2.1</a></li>
					<li><a href="/shop/shopCatalog/47">商品分類 2.2</a></li>
					<li><a href="/shop/shopCatalog/41">商品分類 2.3</a></li>
					<li><a href="/shop/shopCatalog/43">商品分類 2.4</a></li>
				</ul>
			</li>
			<li class="categories_list_sub_lastli"><a href="/shop/shopCatalog/42">商品分類 3</a></li>
		</ul>
	 </div>

	 <div class="sidebar-filter filter2">

		<?php if($page_content['category_attributes']){
      		foreach($page_content['category_attributes'] as $ca){?>

		<h3><?php echo $ca['attribute_name'];?></h3>
        <div class="form-group">
          <div style="font-size:12px;">
      	<?php if($ca['attribute_value']){
      		foreach($ca['attribute_value'] as $av){?>
            <label><input name="attribute_value_sn[]" value="<?php echo $av['attribute_value_sn'];?>" type="checkbox" <?php echo (@$av['status'])?'checked':'';?> > <?php echo $av['attribute_value_name'];?></label>&nbsp;&nbsp;
        <?php }}?>
          </div>
        </div>
        <?php }}?>





		<h3 class="filter-txt " >商品篩選
		<i id="icon-filter" data-toggle="collapse" data-target="#side_menu3"  onclick="myFunction3()"  class="fa fa-minus-square" ></i>

		<i id="icon-filter3" data-toggle="collapse" data-target="#side_menu3" onclick="myFunction4()"  class="fa fa-plus-square"  ></i>
				<script>

					function myFunction3() {
						document.getElementById("icon-filter3").style.display = "block";
						document.getElementById("icon-filter").style.display = "none";
						}
					function myFunction4() {

						document.getElementById("icon-filter3").style.display = "none";
						document.getElementById("icon-filter").style.display = "block";
						}
				</script>

		<div class="inner_filter2 collapse in" id="side_menu3">
		 <script>
  $(document).ready(function(){
    $('.fdcheck').on('change', function(){
      var category_list = [];
      $('#filterschk :input:checked').each(function(){
        var category = $(this).val();
        category_list.push(category);
      });

      if(category_list.length == 0)
        $('.filterpro').fadeIn();
      else {
        $('.filterpro').each(function(){
          var item = $(this).attr('data-tag');
          if(jQuery.inArray(item,category_list) > -1)
            $(this).fadeIn('slow');
          else
            $(this).hide();
        });
      }
    });
  });
  </script>
			<div id="filterschk" class="menu2_chk_box">

				<h4>分類(預設1)</h4>

					<div class="chk_box_lbl">
						<input  type="checkbox" id="tab1_check" class="fdcheck" value="基礎保養" name="cate1">
						<label  class="sphov" for="tab1_check"><span>基礎保養</span></label>
					</div>
					<div class="chk_box_lbl">
						<input type="checkbox" id="tab2_check" class="fdcheck" value="加強護理" name="cate2">
						<label   class="sphov"for="tab2_check"><span>加強護理</span></label>
					</div>
					<div class="chk_box_lbl">
						<input type="checkbox" id="tab3_check" class="fdcheck" value="臉部其他" name="cate3">
						<label  class="sphov" for="tab3_check"><span>臉部其他</span></label>
					</div>
			</div>


	<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 padding0 box1_sidecat">
				<div class="margin-bottom-10 size-15 fontwt400">尺寸</div>

         	<a class="tag" name="value3" href="javascript:getProduct('3','XS');">
        	<span class="txt XS" id="value22">XS</span>
			</a>
    	    <a class="tag" name="value3" href="javascript:getProduct('3','S');">
				<span class="txt L" id="value21">S</span>
			</a>
             <a class="tag" name="value3" href="javascript:getProduct('3','M');">
				<span class="txt M" id="value5">M</span>
			 </a>
              <a class="tag" name="value3" href="javascript:getProduct('3','L');">
				<span class="txt S" id="value1">L</span>
			   </a>
				<div style="clear:both"></div>
		</div>


		<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 padding0 box2_sidecat">
			<div class="margin-bottom-10 size-15 fontwt400">顏色</div>
    	         <a name="value4" href="javascript:getProduct('4','33b2c3');">
						<div id="value19" class="value 33b2c3 circle" style="float:left;width:30px;height:30px;margin: 0 5px 5px 5px;border-radius: 100%!important;background-color:#33b2c3;text-align: center;font-size: 18px;line-height: 30px; color:#fff;">
						</div>
					</a>

                  <a name="value4" href="javascript:getProduct('4','272f54');">
                	<div id="value18" class="value 272f54 circle" style="float:left;width:30px;height:30px;margin: 0 5px 5px 5px;border-radius: 100%;background-color:#272f54;text-align: center;font-size: 18px;line-height: 30px; color:#fff;">
						</div>
				 </a>

               	<a name="value4" href="javascript:getProduct('4','e2e9c7');">
                	<div id="value17" class="value e2e9c7 circle" style="float:left;width:30px;height:30px;margin: 0 5px 5px 5px;border-radius: 100%;background-color:#e2e9c7;text-align: center;font-size: 18px;line-height: 30px; color:#fff;">
					</div>
              	</a>

                 <a name="value4" href="javascript:getProduct('4','eedfb9');">
                	<div id="value11" class="value eedfb9 circle" style="float:left;width:30px;height:30px;margin: 0 5px 5px 5px;border-radius: 100%;background-color:#eedfb9;text-align: center;font-size: 18px;line-height: 30px; color:#fff;">
					</div>
              	</a>

               	<a name="value4" href="javascript:getProduct('4','881936');">
                	<div id="value10" class="value 881936 circle" style="float:left;width:30px;height:30px;margin: 0 5px 5px 5px;border-radius: 100%;background-color:#881936;text-align: center;font-size: 18px;line-height: 30px; color:#fff;">
					</div>
              	</a>

                <a name="value4" href="javascript:getProduct('4','000000');">
                	<div id="value7" class="value 000000 circle" style="float:left;width:30px;height:30px;margin: 0 5px 5px 5px;border-radius: 100%;background-color:#000000;text-align: center;font-size: 18px;line-height: 30px; color:#fff;">
					</div>
              	</a>

               	<a name="value4" href="javascript:getProduct('4','ffffff');">
        				<div id="value6" class="value ffffff circle" style="float:left;width:30px;height:30px;margin: 0 5px 5px 5px;border-radius: 100%;background-color:#ffffff;text-align: center;font-size: 18px;line-height: 30px; color:#eeeeee;border: 2px solid #eeeeee;">
							</div>
              	</a>

					<div style="clear:both"></div>
		</div>


		<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 padding0 box3_sidecat">
			<div class="margin-bottom-10 size-15 fontwt400">容量 </div>
    	  <a class="tag" name="value5" href="javascript:getProduct('5','15ml');">
        	<span class="txt 200ml" id="value20">
					15ml
        	</span>
          </a>
          <a class="tag" name="value5" href="javascript:getProduct('5','30ml');">
        	<span class="txt 100ml" id="value16">
					30ml
        	</span>
          </a>
          <a class="tag" name="value5" href="javascript:getProduct('5','40ml');">
        	<span class="txt 30ml" id="value15">
					40ml
        	</span>
          </a>
    	   <a class="tag" name="value5" href="javascript:getProduct('5','150ml');">
        	<span class="txt 15ml" id="value14">
				150ml
        	</span>
          </a>
		<a class="tag" name="value5" href="javascript:getProduct('5','200ml');">
        	<span class="txt 250ml" id="value9">
				200ml
        	</span>
          </a>
      <a class="tag" name="value5" href="javascript:getProduct('5','250ml');">
        	<span class="txt 40ml" id="value8">
				250ml
        	</span>
          </a>
		<div style="clear:both"></div>
	</div>

			<p>
			  <label class="ranglabal" for="amount">範圍:</label><br>

			</p>
			<div id="slider-range"></div>
			<p>
			<input type="text" id="amount" name="pricerange" style="border:0;">
			</p>
			<!--<p align="center" style="padding-top: 32px;">
						<button class="btn btn-default">進階搜尋</button>
			</p>-->
		</div>
      </div>

      <!--<div class="sidebar-products margin-bottom-40 clearfix">
        <h2>優惠商品</h2>
<?php if(@$promo_product3){ foreach($promo_product3 as $Item){?>
        <div class="item">
          <a href="shop/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['image_path'];?>" alt="<?php echo $Item['image_alt'];?>"></a>
          <h3><a href="shop/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><?php echo $Item['product_name'];?></a></h3>
          <div class="price">$<?php echo $Item['price1'];?>&nbsp;<font style="font-size:12px;color:#333333;"><?php echo $Item['price2'];?></font></div>
        </div>
<?php }}?>
      </div>

      <div class="sidebar-products margin-bottom-40 clearfix">
        <h2>推薦商品</h2>
<?php if(@$promo_product2){ foreach($promo_product2 as $Item){?>
        <div class="item">
          <a href="shop/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['image_path'];?>" alt="<?php echo $Item['image_alt'];?>"></a>
          <h3><a href="shop/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><?php echo $Item['product_name'];?></a></h3>
          <div class="price">$<?php echo $Item['price1'];?>&nbsp;<font style="font-size:12px;color:#333333;"><?php echo $Item['price2'];?></font></div>
        </div>
<?php }}?>
      </div>-->

      <div class="sidebar-products clearfix">
        <h2 class="pull-left">熱賣商品</h2>
<?php if(@$promo_product5){ foreach($promo_product5 as $Item){?>
        <div class="item">
          <a class="sidepro" href="shop/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['image_path'];?>" alt="<?php echo $Item['image_alt'];?>"></a>
          <h3><a href="shop/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><?php echo $Item['product_name'];?></a></h3>
          <div class="price">$<?php echo $Item['price1'];?>&nbsp;<font style="font-size:12px;color:#333333;"><?php echo $Item['price2'];?></font></div>
        </div>
<?php }}?>
      </div>
		<!---------------------------blog content--------------------------------->
		<div class="sidebar-products clearfix">
			<h2>本館文章</h2>
			<div class="blog_box">
				<div class="blog_img">
					<img src="/public/uploads/blog/c4153a3a5af73c78a3e884be91a67c7e.jpg">
				</div>
				<div class="blog_txt">
					<div class="blog-cnt-hrd">
						<h5>Heading</h5>
					</div>
					<p>Blog Dummy Text Blog Dummy Text Blog Dummy Text Blog Dummy Text</p>
				</div>
			</div>
			<div class="blog_box">
				<div class="blog_img">
					<img src="/public/uploads/blog/c4153a3a5af73c78a3e884be91a67c7e.jpg">
				</div>
				<div class="blog_txt">
					<div class="blog-cnt-hrd">
						<h5>Heading</h5>
					</div>
					<p>Blog Dummy Text Blog Dummy Text Blog Dummy Text Blog Dummy Text</p>
				</div>
			</div>
			<div class="blog_box">
				<div class="blog_img">
					<img src="/public/uploads/blog/c4153a3a5af73c78a3e884be91a67c7e.jpg">
				</div>
				<div class="blog_txt">
					<div class="blog-cnt-hrd">
						<h5>Heading</h5>
					</div>
					<p>Blog Dummy Text Blog Dummy Text Blog Dummy Text Blog Dummy Text</p>
				</div>
			</div>
			<div class="blog_box">
				<div class="blog_img">
					<img src="/public/uploads/blog/c4153a3a5af73c78a3e884be91a67c7e.jpg">
				</div>
				<div class="blog_txt">
					<div class="blog-cnt-hrd">
						<h5>Heading</h5>
					</div>
					<p>Blog Dummy Text Blog Dummy Text Blog Dummy Text Blog Dummy Text</p>
				</div>
			</div>
			<div class="blog_box">
				<div class="blog_img">
					<img src="/public/uploads/blog/c4153a3a5af73c78a3e884be91a67c7e.jpg">
				</div>
				<div class="blog_txt">
					<div class="blog-cnt-hrd">
						<h5>Heading</h5>
					</div>
					<p>Blog Dummy Text Blog Dummy Text Blog Dummy Text Blog Dummy Text</p>
				</div>
			</div>

		</div>
		<!------------------------------blog end------------------------------------------>
    </div>
    <!-- END SIDEBAR -->

    <!-- BEGIN CONTENT -->

    <div id="shop_catepg" class="col-md-9 col-sm-7 catlog-rgt-content">
    <div class="ad_product_image">
				<img src="public/img/ad_product.jpg">
	</div>





	<!-- BEGIN PRODUCT LIST-1 -->

		<!-- PRODUCT ITEM-1 START
		  <div class="row product-list">
				<?php if(@$products){ foreach($products as $Item){?>

				<div class="shop-item_outer col-md-3 col-sm-6 col-xs-12">
						<div class="product_items" id="pitems" data-tag="基礎保養">
							<ul class=" row list-inline nomargin custow">
								<li class="col-md-12 col-sm-4 col-xs-6">
									<div class="shop-item">
										<div class="thumbnail noborder nopadding">
											<div class="thumbnail_container_fix">
												<div class="thumbnail_fix">
													<a href="https://sm5r.ddcs.com.tw/product/detail655">
														<img class="img-responsive lazy" src="https://sm5r.ddcs.com.tw/images/pic.svg" data-original="https://sm5r.ddcs.com.tw/images/pic.svg" alt="test免運" style="display: block;">
													</a>
												</div>
											</div>

											<div class="shop-option-over">
												<a class="gid655 btn btn-default add-wishlist" href="javascript:void(0);" onclick="clickHeart(655);" data-item-id="1" data-toggle="tooltip" title="" data-original-title="加入追蹤清單"><i class="fa fa-heart nopadding"></i></a>
											</div>
											<div class="shop-item-info  text-left">
												<!--<span class="label"><img src="https://sm5r.ddcs.com.tw/templates/rwd/images/olock.png" border="0" /></span>-->
								<!--			</div>
										</div>
										<div class="product-item-btn-box">
											<h2 class="owl-featured noborder margin-top-10">新品活動1<a class="product_icon" title="加入追蹤" href="javascript:void(0);"><i product_sn="<?php echo $Item['product_sn'];?>" class="fa fa-heart"></i></a></h2>
											<div class="shop-item-summary shop_itm_summ">

												<!--<div class="size-15" style="height:20px; overflow:hidden; margin-bottom:5px; text-align: left !important;"></div>-->
							<!--						<div style="height:auto; overflow:hidden; text-align: left !important;">
														<h2><a href="https://sm5r.ddcs.com.tw/product/detail655">test免運</a></h2>
													</div>
											<!-- price -->
							<!--					<div class="shop-item-price text-left shop_itm_p" style="color:#ff0000 !important">
													<span class="line-through">NT 120</span> NT 100
												</div>
											</div>
										</div>
										<p class="h_item">HOT</p>
									</div>
								</li>
							</ul>
						</div>

			  </div>
	<!-- PRODUCT ITEM-1 END
				<?php }}?>
			</div>
	<!-- END PRODUCT LIST-1 -->
	<!------------------------------------------------------------------------>






	<form id="form1" class="form-horizontal" role="form" method="get">



		<!--<div class="row product-list">
			<?php if(@$promo_product1){ foreach($promo_product1 as $Item){?>
			  <div class="col-md-4 col-sm-6 col-xs-12">
				<div class="product-item">
				  <div class="pi-img-wrapper">
					<a href="shop/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><img onerror="this.src='public/uploads/notyet.gif'" style="height:250px;width:250px;" src="public/uploads/<?php echo $Item['image_path'];?>" class="img-responsive" alt="<?php echo $Item['image_alt'];?>"></a>
				  </div>
				  <h3><a href="shop/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><?php echo $Item['product_name'];?></a><div class="note note-success note-bordered heart<?php echo $Item['product_sn'];?>" style="display:none;"></div><a class="product_icon" style="float:right;" title="加入追蹤" href="javascript:void(0);"><i product_sn="<?php echo $Item['product_sn'];?>" class="fa fa-heart"></i></a></h3>
				  <div class="pi-price">$<?php echo $Item['price1'];?>&nbsp;<font style="font-size:12px;color:#333333;"><?php echo $Item['price2'];?></font></div>
				  <?php if(($Item['unispec_flag'] && $Item['unispec_qty_in_stock'])||($Item['unispec_flag']=='0' && count($Item['mutispec'])>=1)){?>
					<a href="#" product_sn="<?php echo $Item['product_sn'];?>" class="btn btn-default add2cart">Add to cart</a>
								<?php }else{?>
					<span style="float:right">補貨中</span>
								<?php }?>
				  <?php echo ($promotion_label_eng_name=@$Item['promotion'][0]['promotion_label_eng_name'])? '<div class="sticker sticker-'.$promotion_label_eng_name.'"></div>':'';?>
				</div>
			  </div>
			<?php }}?>
		</div>-->

		<div class="row list-view-sorting clearfix">
			<div class="col-md-2 col-sm-2 list-view">
			  <a href="#"><i class="fa fa-th-large"></i></a>
			  <a href="#"><i class="fa fa-th-list"></i></a>
			</div>
			<div class="col-md-12 col-sm-12 filter-bar">
			  <!---div class="pull-right">
				<label class="control-label">顯示數量:</label>
				<select class="form-control input-sm ajax_product" name="limit">
				  <option value="#?limit=24" selected="selected">24</option>
				  <option value="#?limit=25">25</option>
				  <option value="#?limit=50">50</option>
				  <option value="#?limit=75">75</option>
				  <option value="#?limit=100">100</option>
				</select>
			  </div-->
			  <div class="pull-right">
				<label class="control-label">排序依據:</label>
				<select class="form-control input-sm" name="sort" onchange="$('#form1').submit();">
				  <option value="sort_order" <?php if($sort=='sort_order'){?>selected="selected"<?php }?>>最新商品</option>
				  <option value="name_ASC" <?php if($sort=='name_ASC'){?>selected="selected"<?php }?>>名稱 (A - Z)</option>
				  <option value="name_DESC" <?php if($sort=='name_DESC'){?>selected="selected"<?php }?>>名稱 (Z - A)</option>
				  <option value="price_ASC" <?php if($sort=='price_ASC'){?>selected="selected"<?php }?>>價格 (Low &gt; High)</option>
				  <option value="price_DESC" <?php if($sort=='price_DESC'){?>selected="selected"<?php }?>>價格 (High &gt; Low)</option>
				  <option value="rating_DESC" <?php if($sort=='rating_DESC'){?>selected="selected"<?php }?>>訂購數量 (Highest)</option>
				  <option value="rating_ASC" <?php if($sort=='rating_ASC'){?>selected="selected"<?php }?>>訂購數量 (Lowest)</option>
				</select>
			  </div>
			</div>
		</div>
	</form>
<!------------------------------------------------------------------------>



		<!-- js effect filter items START -->
<div class="row product-list">
								<!-- PRODUCT ITEM-2 START -->
				<div class="shop-item_outer col-md-3 col-sm-6 col-xs-12 filterpro "  data-tag="基礎保養">
					<div class="product-item">
						  <div class="pi-img-wrapper">
							<a href="shop/shopItem/10/42"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/product/10/d3b7137ad68e4e50e290425fcc20042c.jpg" class="img-responsive" alt="客製喜帖_1"></a>
						  </div>
						<div class="product-item-btn-box">
						  <h3 class="item-name">客製喜帖_1</h3>
						  <div class="pi-textsp"><a href="shop/shopItem/10/42">進口喜帖 - CW519WH 珍珠白蕾絲婚卡</a></div>
						  <div class="note note-success note-bordered heart10" style="display:none;"></div><a class="product_icon" title="加入追蹤" href="javascript:void(0);"><i product_sn="10" class="fa fa-heart"></i></a>

						  <div class="pi-price"><span>$</span>1&nbsp;<font style="font-size:12px;color:#b52b28;"></font></div>
						 <div class="prd_btn_box">
							<a class="btn btn-default prd_btn_lft" href="">立即結帳</a>
							<a class="btn btn-default prd_btn_rgt add2cart" href="#" product_sn="10">放入購物車</a>

						 </div>

						<!-- <div class="Quantity-box">
							 <select>
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
									<option>6</option>
									<option>7</option>
									<option>8</option>
									<option>9</option>
									<option>10</option>
							  </select>
						  </div>-->

														<!--<span class="replt-txt" style="float:right">補貨中</span>-->
																	<div class="sticker sticker-promote"></div>						</div>
						<p class="h_item2">SALE</p>
					</div>
				</div>



	<!-- PRODUCT ITEM-2 END -->




							<div class="shop-item_outer col-md-3 col-sm-6 col-xs-12 filterpro "  data-tag="加強護理">
					<div class="product-item">
						  <div class="pi-img-wrapper">
							<a href="shop/shopItem/10/42"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/product/18/4d4f58eb97d2d83ffa7999b936e3ff24.jpg" class="img-responsive" alt="客製喜帖_1"></a>
						  </div>
						<div class="product-item-btn-box">
						  <h3 class="item-name">客製喜帖_1</h3>
						  <div class="pi-textsp"><a href="shop/shopItem/10/42">進口喜帖 - CW519WH 珍珠白蕾絲婚卡</a></div>
						  <div class="note note-success note-bordered heart10" style="display:none;"></div><a class="product_icon" title="加入追蹤" href="javascript:void(0);"><i product_sn="10" class="fa fa-heart"></i></a></h3>

						  <div class="pi-price"><span>$</span>1&nbsp;<font style="font-size:12px;color:#b52b28;"></font></div>
						 <div class="prd_btn_box">
							<a class="btn btn-default prd_btn_lft" href="">立即結帳</a>
							<a class="btn btn-default prd_btn_rgt add2cart" href="#" product_sn="10">放入購物車</a>

						 </div>

						<!-- <div class="Quantity-box">
							 <select>
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
									<option>6</option>
									<option>7</option>
									<option>8</option>
									<option>9</option>
									<option>10</option>
							  </select>
						  </div>-->

														<!--<span class="replt-txt" style="float:right">補貨中</span>-->
																	<div class="sticker sticker-promote"></div>						</div>
						<p class="h_item2">SALE</p>
					</div>
				</div>



	<!-- PRODUCT ITEM-2 END -->


		<div class="shop-item_outer col-md-3 col-sm-6 col-xs-12 filterpro "  data-tag="臉部其他">
					<div class="product-item">
						  <div class="pi-img-wrapper">
							<a href="shop/shopItem/10/42"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/product/16/604ad9e1c0c1c946e3e8451d25f17173.jpg" class="img-responsive" alt="客製喜帖_1"></a>
						  </div>
						<div class="product-item-btn-box">
						  <h3 class="item-name">客製喜帖_1</h3>
						  <div class="pi-textsp"><a href="shop/shopItem/10/42">進口喜帖 - CW519WH 珍珠白蕾絲婚卡</a></div>
						  <div class="note note-success note-bordered heart10" style="display:none;"></div><a class="product_icon" title="加入追蹤" href="javascript:void(0);"><i product_sn="10" class="fa fa-heart"></i></a></h3>

						  <div class="pi-price"><span>$</span>1&nbsp;<font style="font-size:12px;color:#b52b28;"></font></div>
						 <div class="prd_btn_box">
							<a class="btn btn-default prd_btn_lft" href="">立即結帳</a>
							<a class="btn btn-default prd_btn_rgt add2cart" href="#" product_sn="10">放入購物車</a>

						 </div>

						<!-- <div class="Quantity-box">
							 <select>
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
									<option>6</option>
									<option>7</option>
									<option>8</option>
									<option>9</option>
									<option>10</option>
							  </select>
						  </div>-->

														<!--<span class="replt-txt" style="float:right">補貨中</span>-->
																	<div class="sticker sticker-promote"></div>						</div>
						<p class="h_item2">SALE</p>
					</div>
				</div>

	<!-- PRODUCT ITEM-2 END -->



		<div class="shop-item_outer col-md-3 col-sm-6 col-xs-12 filterpro "  data-tag="臉部其他">
					<div class="product-item">
						  <div class="pi-img-wrapper">
							<a href="shop/shopItem/10/42"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/product/21/909206aa90295ca3553e8b24eba70b76.jpg" class="img-responsive" alt="客製喜帖_1"></a>
						  </div>
						<div class="product-item-btn-box">
						  <h3 class="item-name">客製喜帖_1</h3>
						  <div class="pi-textsp"><a href="shop/shopItem/10/42">進口喜帖 - CW519WH 珍珠白蕾絲婚卡</a></div>
						  <div class="note note-success note-bordered heart10" style="display:none;"></div><a class="product_icon" title="加入追蹤" href="javascript:void(0);"><i product_sn="10" class="fa fa-heart"></i></a></h3>

						  <div class="pi-price"><span>$</span>1&nbsp;<font style="font-size:12px;color:#b52b28;"></font></div>
						 <div class="prd_btn_box">
							<a class="btn btn-default prd_btn_lft" href="">立即結帳</a>
							<a class="btn btn-default prd_btn_rgt add2cart" href="#" product_sn="10">放入購物車</a>

						 </div>

						<!-- <div class="Quantity-box">
							 <select>
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
									<option>6</option>
									<option>7</option>
									<option>8</option>
									<option>9</option>
									<option>10</option>
							  </select>
						  </div>-->

														<!--<span class="replt-txt" style="float:right">補貨中</span>-->
																	<div class="sticker sticker-promote"></div>						</div>
						<p class="h_item2">SALE</p>
					</div>
				</div>

	<!-- PRODUCT ITEM-2 END -->



							</div>

<!-- js effect filter items End-->




			<div class="row product-list" >
				<?php if(@$products){ foreach($products as $Item){?>
				<!-- PRODUCT ITEM-2 START -->
				<div class="shop-item_outer col-md-3 col-sm-6 col-xs-12">
					<div class="product-item">
						  <div class="pi-img-wrapper">
							<a href="shop/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['image_path'];?>" class="img-responsive" alt="<?php echo $Item['image_alt'];?>"></a>
						  </div>
						<div class="product-item-btn-box">
						  <h3 class="item-name"><?php echo $Item['product_name'];?></h3>
						  <div class="pi-textsp"><a href="shop/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>">進口喜帖 - CW519WH 珍珠白蕾絲婚卡</a></div>
						  <div class="note note-success note-bordered heart<?php echo $Item['product_sn'];?>" style="display:none;"></div><a class="product_icon" title="加入追蹤" href="javascript:void(0);"><i product_sn="<?php echo $Item['product_sn'];?>" class="fa fa-heart"></i></a></h3>

						  <div class="pi-price"><span>$</span><?php echo $Item['price1'];?>&nbsp;<font style="font-size:12px;color:#b52b28;"><?php echo $Item['price2'];?></font></div>
						 <div class="prd_btn_box">
							<a class="btn btn-default prd_btn_lft" href="">立即結帳</a>
							<a class="btn btn-default prd_btn_rgt add2cart" href="#" product_sn="<?php echo $Item['product_sn'];?>">放入購物車</a>

						 </div>

						<!-- <div class="Quantity-box">
							 <select>
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
									<option>6</option>
									<option>7</option>
									<option>8</option>
									<option>9</option>
									<option>10</option>
							  </select>
						  </div>-->

							<?php if(($Item['unispec_flag'] && $Item['unispec_qty_in_stock'])||($Item['unispec_flag']=='0' && count($Item['mutispec'])>=1)){?>
								<!--<a class="btn btn-default add2cart" href="#" product_sn="<?php echo $Item['product_sn'];?>">Add to cart</a>-->
							<?php }else{?>
							<!--<span class="replt-txt" style="float:right">補貨中</span>-->
										<?php }?>
							<?php echo ($promotion_label_eng_name=@$Item['promotion'][0]['promotion_label_eng_name'])? '<div class="sticker sticker-'.$promotion_label_eng_name.'"></div>':'';?>
						</div>
						<p class="h_item2">SALE</p>
					</div>
				</div>
	<!-- PRODUCT ITEM-2 END -->
				<?php }}?>
			</div>
	<!-- END PRODUCT LIST-2 -->





	<!-- BEGIN PAGINATOR -->
      <div class="row">
        <div class="col-md-4 col-sm-4 items-info">筆數：<?php echo $total;?></div>
        <div class="col-md-8 col-sm-8">
          <ul class="pagination pull-right">
									<?php echo $page_links;?>
          </ul>
        </div>
      </div>
      <!-- END PAGINATOR -->
    </div>
    <!-- END CONTENT -->
  </div>
  <!-- END SALE PRODUCT & NEW ARRIVALS -->
  <!-- END CONTENT -->
</div>
<!-- END SIDEBAR & CONTENT -->
