<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/typeahead/typeahead.css">
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/pages/css/portfolio.css" rel="stylesheet" type="text/css"/>



<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>婚禮網站 <small>商品編輯</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/admin">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品列表
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/product/productTable/婚禮網站">婚禮網站</a>
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
        商品編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">商品編輯</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/product/productTable/婚禮網站" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
              <div class="tabbable">
                <ul class="nav nav-tabs">
                  <li class="active">
                    <a href="#tab_general" data-toggle="tab"> 一般設定 </a>
                  </li>
                  <li>
                    <a href="#tab_price" data-toggle="tab"> 銷售設定 </a>
                  </li>
                  <li>
                    <a href="#tab_meta" data-toggle="tab"> Meta </a>
                  </li>
                  <li id="tabImages">
                    <a href="#tab_images" data-toggle="tab"> 版型與顏色 </a>
                  </li>
                  <li>
                    <a href="#tab_shipping" data-toggle="tab"> 配送設置 </a>
                  </li>
                  <li>
                    <a href="#tab_stock" data-toggle="tab"> 庫存管理 </a>
                  </li>
                  <li id="tabAdditionalPurchase">
                    <a href="#tab_additional_purchase" data-toggle="tab"> 加購商品設定 </a>
                  </li>
                  <li>
                    <a href="#tab_event" data-toggle="tab"> 促銷活動 </a>
                  </li>
                </ul>
                <div class="tab-content no-space">
                  <div class="tab-pane active" id="tab_general">
                    <div class="form-body">
                      <div class="form-group">
                        <label for="name" class="col-md-2 control-label">是否發佈</label>
                        <div class="col-md-10">
                          <div class="radio-list">
                              <label class="radio-inline">
                      					<input type="radio" name="Item[product_status]" id="optionsRadios4" value="1" <?php if(@$Item['product_status']){ ?>checked<?php }?> required>  發佈  
                              </label>
                              <label class="radio-inline">
                      					<input type="radio" name="Item[product_status]" id="optionsRadios5" value="0" <?php if(@$Item['product_status']==='0'){ ?>checked<?php }?> required>  不發佈 
                                <a href="http://dev.ijwedding.com/admin_jerry/shopItem" target="_blank" onclick="if(!confirm('前往預覽畫面')){return false;}"><i class="fa fa-eye"></i></a>
                              </label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-md-2 control-label">排序</label>
                        <div class="col-md-2">
                    				<input name="Item[sort_order]" maxlength="4" type="text" class="form-control" value="<?php echo (@$Item['sort_order']) ? @$Item['sort_order']:'9999'; ?>" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">商品類型</label>
                        <div class="col-md-10">
                          <div class="radio-list">
                            <label class="radio-inline">
                              <input class="radio-list" name="Item[product_type]" type="radio" class="form-control" value="1" <?php echo (@$Item['product_type']=='1') ?'checked':''; ?> >&nbsp;實體
                            </label>
                            <label class="radio-inline">
                              <input class="radio-list" id="product_type_2" name="Item[product_type]" type="radio" class="form-control" value="2" <?php echo (@$Item['product_type']=='2') ?'checked':''; ?> >&nbsp;虛擬
                            </label>
                            <label class="radio-inline">
                              <input class="radio-list" name="Item[product_type]" type="radio" class="form-control" value="3" <?php echo (@$Item['product_type']=='3') ?'checked':''; ?> >&nbsp;客製化商品
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-md-2 control-label">產品名稱</label>
                        <div class="col-md-10">
                          <input type="text" class="form-control" name="Item[product_name]" value="<?php echo @$Item['product_name'];?>" required>
                          <span class="help-block"> 若為加購類商品請勿變更 </span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-md-2 control-label">產品英文名稱</label>
                        <div class="col-md-10">
                          <input type="text" class="form-control" name="Item[product_eng_name]" value="<?php echo @$Item['product_eng_name'];?>">
                          <span class="help-block"> 若為加購類商品請勿變更 </span>
                        </div>
                      </div>                      
                      <div class="form-group">
                        <label for="name" class="col-md-2 control-label">銷售對象</label>
                        <div class="col-md-10">
                          <div class="checkbox-list">
                            <label class="checkbox-inline">
                              <input class="checkbox-list" type="checkbox" class="form-control" name="target_buyer[]" value="一般使用者" <?php echo (strrpos(@$Item['target_buyer'],'一般使用者')!==false || strrpos(@$Item['target_buyer'],'全部')!==false) ?'checked':''; ?> >&nbsp;一般使用者
                            </label>
                            <label class="checkbox-inline">
                              <input class="checkbox-list" type="checkbox" class="form-control" name="target_buyer[]" value="經銷商" <?php echo (strrpos(@$Item['target_buyer'],'經銷商')!==false || strrpos(@$Item['target_buyer'],'全部')!==false) ?'checked':''; ?> >&nbsp;經銷商
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">主分類</label>
                        <div class="col-md-10">
                  				<?php echo $categorys;?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">擴展分類</label>
                        <div class="col-md-10">
                  				<?php echo $ext_categorys;?>
                          <span class="help-block"> 按住 control 進行複選 </span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-md-2 control-label">商品分類屬性</label>
                        <div class="col-md-10">
                          <div class="form-control height-auto">
                            <div class="table-scrollable table-scrollable-borderless">
                              <table class="table table-hover">
                                <thead>
                                  <tr>
                                    <th width="100">分類屬性</th>
                                    <th>屬性值</th>
                                  </tr>
                                </thead>
                                <tbody id="AttributeList">
                                </tbody>
                              </table>
                            </div>
                          </div>
                          <span class="help-block">依照不同的分類顯示不同的分類屬性讓管理員選擇</span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="note" class="col-md-2 control-label">付款方式</label>
                        <div class="col-md-10">
                          <div class="checkbox-list">
                          	<?php foreach($payments as $payment){?>
                            <label class="checkbox-inline">
                              <input class="checkbox-list" type="checkbox" class="form-control" name="payment[]" value="<?php echo $payment['payment_method'];?>" <?php if($payment['checked']) echo 'checked';?> >&nbsp;<?php echo $payment['payment_method_name'];?>
                            </label>
                          <?php }?>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">滿額免運</label>
                        <div class="col-md-10">
                          <div class="radio-list">
                            <label class="radio-inline">
                              <input class="radio-list" name="Item[free_delivery]" type="radio" class="form-control" value="1" <?php echo (@$Item['free_delivery']=='1') ?'checked':''; ?> >&nbsp;是
                            </label>
                            <label class="radio-inline">
                              <input class="radio-list" name="Item[free_delivery]" type="radio" class="form-control" value="0" <?php echo (@$Item['free_delivery']=='0') ?'checked':''; ?> >&nbsp;否
                            </label>
                          </div>
                        </div>
                      </div>                      
                      <div class="form-group">
                        <label for="note" class="col-md-2 control-label">商品特點</label>
                        <div class="col-md-10">
                          <textarea class="form-control" name="Item[special_item]" rows="3"><?php echo @$Item['special_item'];?></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="note" class="col-md-2 control-label">商品資訊</label>
                        <div class="col-md-10">
                          <textarea class="ckeditor form-control" name="Item[product_info]" rows="6"><?php echo @$Item['product_info'];?></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- End of tab_general -->
                  <div class="tab-pane" id="tab_price">
                    <h4>特殊規格選擇</h4>
                    <div class="form-group">
                      <label for="name" class="col-md-2 control-label">款型</label>
                      <div class="col-md-10">
                        <div class="checkbox-list" id="PackageList1">
                          <!--label class="checkbox-inline">
                          	<input type="checkbox" name="template-Package[]"  value="基本款"> 基本款 
                          </label>
                          <label class="checkbox-inline">
                          <input type="checkbox" name="template-Package[]"  value="豪華款" checked="checked"> 豪華款 </label>
                          <label class="checkbox-inline">
                          <input type="checkbox" name="template-Package[]"  value="旗艦款"> 旗艦款 </label-->
                        </div>
                      </div>
                      <p class="form-control-static">依照一般設定中的分類設定顯示不同的規格組合選擇，設定後價格設定也會顯示相對應的價格設定項目</p>
                    </div>
                    <hr>
                    <h4>購買限制與定價方式</h4>
                    <div class="form-group">
                      <label for="name" class="col-md-2 control-label">最少採購量</label>
                      <div class="col-md-2">
                        <input type="number" min="1" class="form-control" name="Item[min_order_qt]" placeholder="" value="<?php echo (@$Item['min_order_qt'])? @$Item['min_order_qt']:'1';?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="name" class="col-md-2 control-label">遞增數量</label>
                      <div class="col-md-2">
                        <input type="number" min="0" class="form-control" name="Item[incremental]" placeholder="" value="<?php echo (@$Item['incremental']!==null)? @$Item['incremental']:'1';?>">
                      </div>
                        <span class="help-block">規範選單出現的數量選擇遞增數量，填入 10，則只會出現 110,120,130.... 以此類推；填入0則購買數量只能選1</span>
                    </div>
                    <div class="form-group">
                      <label for="name" class="col-md-2 control-label">定價方式</label>
                      <div class="col-md-10">
                        <div class="radio-list">
                          <label class="radio-inline"><input type="radio" name="Item[pricing_method]" value="1" <?php echo ($Item['pricing_method']=='1')?'checked':'';?> >單一售價</label>
                          <label class="radio-inline"><input type="radio" id="pricing_method_2" name="Item[pricing_method]" value="2" <?php echo ($Item['pricing_method']=='2')?'checked':'';?> >區間計價</label>
                        </div>
                      </div>
                    </div>
                    <!-- /購買限制 -->
                    <div id="setting_price_2" style="display:none;">
                      <hr>
                      <h4>
                        區間計價設定
                        <div style="float:right;">
                          <a href="javascript:void(0);" class="btn green btn-xs" id="btn_area_add"><i class="fa fa-plus"></i> 新增區間</a>
                        </div>
                        <div style="clear:both;"></div>
                      </h4>
                      <div class="form-group">
                        <label for="name" class="col-md-4 control-label">
                          <label for="name" class="col-md-4 control-label">區間單位</label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" placeholder="個" name="interval_value" value="<?php echo @$price_tier[0]['interval_value'];?>">
                          </div>
                        </label>
                      </div>
                      <div id="sample_price_2" style="display:none;">
                        <div class="form-group">
                          <label for="name" class="col-md-4 control-label">
                            <label for="name" class="col-md-4 control-label">起始數量</label>
                            <div class="col-md-8">
                              <input type="number" min="1" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" class="form-control" placeholder="此區間起始數量" name="Count[start_qty]" value="start_value">
                            </div>
                          </label>
                          <label for="name" class="col-md-4 control-label">
                            <label for="name" class="col-md-4 control-label">結束數量</label>
                            <div class="col-md-8">
                              <input type="number" min="2" class="form-control" placeholder="此區間結束數量" name="Count[end_qty]" value="end_value">
                            </div>
                          </label>
                          <label for="name" class="col-md-3 control-label">
                            <label for="name" class="col-md-4 control-label">單價</label>
                            <div class="col-md-8">
                              <input type="number" class="form-control" placeholder="空白＝刪除" name="Count[price]" value="">
                            </div>
                          </label>
                        </div>
                      </div>
                      <?php if($price_tier){ foreach($price_tier as $key=>$pt){?>
                      <div class="form-group">
                        <label for="name" class="col-md-4 control-label">
                          <label for="name" class="col-md-4 control-label">起始數量</label>
                          <div class="col-md-8">
                            <input type="number" class="form-control" placeholder="此區間起始數量" name="PriceTier[<?php echo $key;?>][start_qty]" value="<?php echo $pt['start_qty'];?>">
                          </div>
                        </label>
                        <label for="name" class="col-md-4 control-label">
                          <label for="name" class="col-md-4 control-label">結束數量</label>
                          <div class="col-md-8">
                            <input type="number" class="form-control" placeholder="此區間結束數量" name="PriceTier[<?php echo $key;?>][end_qty]" value="<?php echo $pt['end_qty'];?>">
                          </div>
                        </label>
                        <label for="name" class="col-md-3 control-label">
                          <label for="name" class="col-md-4 control-label">單價</label>
                          <div class="col-md-8">
                            <input type="number" class="form-control" placeholder="空白＝刪除" name="PriceTier[<?php echo $key;?>][price]" value="<?php echo $pt['price'];?>">
                          </div>
                        </label>
                      </div>
                      <?php } }else{?>
                      <div class="form-group">
                        <label for="name" class="col-md-4 control-label">
                          <label for="name" class="col-md-4 control-label">起始數量</label>
                          <div class="col-md-8">
                            <input type="number" class="form-control" placeholder="此區間起始數量" name="PriceTier[0][start_qty]" value="<?php echo (@$Item['min_order_qt'])? @$Item['min_order_qt']:'1';?>">
                          </div>
                        </label>
                        <label for="name" class="col-md-4 control-label">
                          <label for="name" class="col-md-4 control-label">結束數量</label>
                          <div class="col-md-8">
                            <input type="number" class="form-control" placeholder="此區間結束數量" name="PriceTier[0][end_qty]" value="">
                          </div>
                        </label>
                        <label for="name" class="col-md-3 control-label">
                          <label for="name" class="col-md-4 control-label">單價</label>
                          <div class="col-md-8">
                            <input type="number" class="form-control" placeholder="空白＝刪除" name="PriceTier[0][price]" value="">
                          </div>
                        </label>
                      </div>
                      <?php } ?>
                    </div>
                    <div id="setting_price_1">
                      <hr>
                      <h4>單一售價設定</h4>
                      <div class="form-group">
                        <label class="col-md-2 control-label">規格</label>
                        <label class="col-md-3 control-label">定價</label>
                        <label class="col-md-3 control-label">售價</label>
                      </div>
                      <div id="PackageList2">
                      </div>
                    </div>
                  </div>
                  <!-- End of tab_price -->
                  <div class="tab-pane" id="tab_meta">
                    <div class="form-group">
                      <label class="col-md-2 control-label">Meta Title</label>
                      <div class="col-md-10">
                        <input type="text" class="form-control maxlength-handler" name="Item[meta_title]" maxlength="100" placeholder="" value="<?php echo @$Item['meta_title'];?>" >
                        <span class="help-block">
                        max 100 chars </span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Meta Keywords</label>
                      <div class="col-md-10">
                        <textarea class="form-control maxlength-handler" rows="2" name="Item[meta_keywords]" maxlength="255"><?php echo @$Item['meta_keywords'];?></textarea>
                        <span class="help-block">
                        max 255 chars 請用 , 號分隔</span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Meta Description</label>
                      <div class="col-md-10">
                        <textarea class="form-control maxlength-handler" rows="2" name="Item[meta_content]" maxlength="255"><?php echo @$Item['meta_content'];?></textarea>
                        <span class="help-block">
                        max 255 chars </span>
                      </div>
                    </div>
                  </div>
                  <!-- End of tab_meta -->
                  <div class="tab-pane" id="tab_images">
                    <!--div class="form-group">
                      <label class="col-md-2 control-label">版型 Demo 網址</label>
                      <div class="col-md-10">
                        <input type="text" class="form-control maxlength-handler" name="template-Demo" maxlength="100" placeholder="" value="http://demo.ijwedding.com/preview/luv">
                      </div>
                    </div-->
                    <div class="form-group">
                      <label for="active" class="col-md-2 control-label">版型</label>
                      <div class="col-md-10">
                        <div class="radio-list">
		                    	<?php if(@$templates){foreach($templates as $template){?>
	                          <label class="radio-inline">
	                          <input type="radio" <?php echo (@$Item['product_template_sn'])?'disabled':'';?> class="template_click" name="Item[product_template_sn]" value="<?php echo $template['product_template_sn'];?>" <?php if($template['product_template_sn']==@$Item['product_template_sn']){?>checked="checked"<?php }?> > <?php echo $template['product_template_name'];?> </label>
		                      <?php }}?>
                        </div>
                      </div>
                      <div style="clear:both;"></div>
                      <div class="table-scrollable table-scrollable-borderless"><span class="help-block">(第一個開放顏色的第一張圖片為主圖)</span>
                      <div class="col-md-12">
                        <table class="table table-hover">
                          <thead>
                            <tr>
                              <th>顏色</th>
                              <th style="text-align:center;">圖片1</th>
                              <th style="text-align:center;">圖片2</th>
                              <th style="text-align:center;">圖片3</th>
                              <th style="text-align:center;">圖片4</th>
                              <th style="text-align:center;">圖片5</th>
                            </tr>
                          </thead>
                          <tbody id="PhotoList">
                          </tbody>
                        </table>
                      </div>
                        <!--div class="checkbox-list">
		                    	<?php if(@$template_colors){foreach($template_colors as $tc){?>
	                          <label class="checkbox-inline">
	                          <input type="checkbox" name="Item[product_template_sn]" value="<?php echo $tc['product_template_sn'];?>" <?php if($tc['product_template_sn']==$tc['product_template_sn']){?>checked="checked"<?php }?> > <?php echo $tc['product_template_name'];?> </label>
		                      <?php }}?>
                        </div-->
                      </div>                      
                    </div>

                  </div>
                  <!-- End of tab_images -->
                  <div class="tab-pane" id="tab_shipping">
	                    <div class="form-group">
		                    <div class="checkbox-list">
		                    	<?php foreach($deliverys as $delivery){?>
		                      <label class="col-md-3 control-label">
		                        <input class="checkbox-list" type="checkbox" class="form-control" name="deliverys[]" value="<?php echo $delivery['delivery_method'];?>" <?php if($delivery['checked']) echo 'checked';?> >&nbsp;<?php echo $delivery['delivery_method_name'];?>
		                      </label>
		                    <?php }?>
		                    </div>
		                  </div>
	                    <!--div class="form-group">
	                      <label class="col-md-2 control-label"><?php echo $delivery['delivery_method_name'];?></label>
	                      <div class="col-md-10">
	                        <div class="radio-list">
	                          <label class="radio-inline"><input type="radio" name="deliverys[]" value="<?php echo $delivery['delivery_method'];?>" <?php if($delivery['checked']) echo 'checked';?>>是</label>
	                          <label class="radio-inline"><input type="radio" name="deliverys[]" value="0">否</label>
	                        </div>
	                      </div>
	                    </div-->
                  </div>
                  <!-- End of tab_shipping -->
                  <div class="tab-pane" id="tab_stock">
                    <div class="form-group">
                      <label class="col-md-2 control-label">開放預購</label>
                      <div class="col-md-10">
                        <div class="radio-list">
                          <label class="radio-inline"><input type="radio" id="open_preorder_flag1" name="Item[open_preorder_flag]" value="1" <?php if(@$Item['open_preorder_flag']){ echo 'checked';}?> >是</label>
                          <label class="radio-inline"><input type="radio" name="Item[open_preorder_flag]" value="0" <?php if(@$Item['open_preorder_flag']=='3'){ echo 'checked';}?> >否</label>
                        </div>
                        <span class="help-block">開放預購，庫存不足仍可購買，所有虛擬類的商品都可直接設為開放，且無須設定庫存。</span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="name" class="col-md-2 control-label">規格</label>
                      <div class="col-md-10">
                        <div class="radio-list">
                          <label class="radio-inline"><input type="radio" name="Item[unispec_flag]" value="1" <?php echo ($Item['unispec_flag']=='1')?'checked':'';?> >單一</label>
                          <label class="radio-inline"><input type="radio" id="unispec_flag_0" name="Item[unispec_flag]" value="0" <?php echo ($Item['unispec_flag']=='0')?'checked':'';?> >多重</label>
<?php if(@$Item['product_type']!='2'){?>
	                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="admin/product/inventory/item?product_sn=<?php echo @$Item['product_sn'];?>" class="btn blue btn-md input-inline" target="_blank"><i class="fa fa-mail-forward"></i> 庫存管理</a>
<?php }?>	                          
                        </div>
                      </div>
                    </div>
                    <div id="unispec">
	                    <div class="form-group">
	                      <label for="name" class="col-md-2 control-label">庫號</label>
	                      <div class="col-md-10">
	                        <input type="text" class="form-control" name="Item[unispec_warehouse_num]" value="<?php echo @$Item['unispec_warehouse_num'];?>">
	                      </div>
	                    </div>
	                    <div class="form-group">
	                      <label class="col-md-2 control-label">庫存量</label>
	                      <div class="col-md-10">
	                        <div class="radio-list">
	                          <input type="text" class="pagination-panel-input form-control input-inline input-md" size="16" name="unispec_qty_in_stock" value="<?php echo @$Item['unispec_qty_in_stock'];?>" <?php echo (@$Item['product_type']=='2') ?'':'disabled'; ?> >
	                        </div>
	                      </div>
	                    </div>
	                    <div class="form-group">
	                      <label class="col-md-2 control-label">安全庫存量</label>
	                      <div class="col-md-10">
	                        <div class="radio-list">
	                          <input type="text" class="pagination-panel-input form-control input-inline input-md" size="16" name="Item[unispec_safe_qty_in_stock]" value="<?php echo @$Item['unispec_safe_qty_in_stock'];?>">
	                        </div>
	                      </div>
	                    </div>
	                  </div>
                    <div id="multispec">
	                    <div class="form-group">
	                      <label class="col-md-2 control-label">虛擬商品單一庫存量</label>
	                      <div class="col-md-10">
	                        <div class="radio-list">
	                          <input type="text" class="pagination-panel-input form-control input-inline input-md" size="16" name="Item[unispec_qty_in_stock]" value="<?php echo @$Item['unispec_qty_in_stock'];?>" <?php echo (@$Item['product_type']=='2') ?'':'disabled'; ?> >
	                        </div>
	                      </div>
	                    </div>
	                      <hr>
	                      <h4>
	                        多重規格庫存設定
	                        <div style="float:right;">
	                          <a href="javascript:void(0);" class="btn green btn-xs" id="btn_spec_add"><i class="fa fa-plus"></i> 新增規格庫存</a>
	                        </div>
	                        <div style="clear:both;"></div>
	                      </h4>
	                      <div id="sample_spec" style="display:none;">
	                        <div class="form-group">
	                          <label for="name" class="col-md-3 control-label">
	                            <label for="name" class="col-md-3 control-label">名稱</label>
	                            <div class="col-md-8">
	                              <input type="text" class="form-control" placeholder="空白＝刪除" name="Count[color_name]" value="">
	                            </div>
	                          </label>
	                          <label for="name" class="col-md-3 control-label">
	                            <label for="name" class="col-md-3 control-label">庫號</label>
	                            <div class="col-md-8">
	                              <input type="text" class="form-control" placeholder="" name="Count[warehouse_num]" value="">
	                            </div>
	                          </label>	                          
	                          <label for="name" class="col-md-3 control-label">
	                            <label for="name" class="col-md-4 control-label">庫存量</label>
	                            <div class="col-md-5">
	                              <input type="number" min="2" class="form-control" placeholder="" name="qty_in_stock" value="lastqty" disabled>
	                            </div>
	                          </label>
	                          <label for="name" class="col-md-3 control-label">
	                            <label for="name" class="col-md-5 control-label">安全庫存量</label>
	                            <div class="col-md-5">
	                              <input type="number" class="form-control" placeholder="" name="Count[safe_qty_in_stock]" value="safeqty">
	                            </div>
	                          </label>
	                        </div>
	                      </div>
	                      <?php if(@$mutispecs){ foreach($mutispecs as $key=>$ms){?>
	                      <input type="hidden" name="Mutispec[<?php echo $key;?>][mutispec_stock_sn]" value="<?php echo @$ms['mutispec_stock_sn']; ?>">
	                      <div class="form-group">
	                          <label for="name" class="col-md-3 control-label">
	                            <label for="name" class="col-md-3 control-label">名稱</label>
	                            <div class="col-md-8">
	                              <input type="text" class="form-control" placeholder="空白＝刪除" name="Mutispec[<?php echo $key;?>][color_name]" value="<?php echo $ms['color_name'];?>">
	                            </div>
	                          </label>
	                          <label for="name" class="col-md-3 control-label">
	                            <label for="name" class="col-md-3 control-label">庫號</label>
	                            <div class="col-md-8">
	                              <input type="text" class="form-control" placeholder="" name="Mutispec[<?php echo $key;?>][warehouse_num]" value="<?php echo $ms['warehouse_num'];?>">
	                            </div>
	                          </label>	                          
	                          <label for="name" class="col-md-3 control-label">
	                            <label for="name" class="col-md-4 control-label">庫存量</label>
	                            <div class="col-md-5">
	                              <input type="number" min="2" class="form-control" placeholder="" name="qty_in_stock" value="<?php echo $ms['qty_in_stock'];?>" disabled >
	                            </div>
	                          </label>
	                          <label for="name" class="col-md-3 control-label">
	                            <label for="name" class="col-md-5 control-label">安全庫存量</label>
	                            <div class="col-md-5">
	                              <input type="number" class="form-control" placeholder="" name="Mutispec[<?php echo $key;?>][safe_qty_in_stock]" value="<?php echo $ms['safe_qty_in_stock'];?>">
	                            </div>
	                          </label>	                      	
	                      </div>
	                      <?php } }else{?>
	                        <div class="form-group">
	                          <label for="name" class="col-md-3 control-label">
	                            <label for="name" class="col-md-3 control-label">名稱</label>
	                            <div class="col-md-8">
	                              <input type="text" class="form-control" placeholder="空白＝刪除" name="Mutispec[0][color_name]" value="">
	                            </div>
	                          </label>
	                          <label for="name" class="col-md-3 control-label">
	                            <label for="name" class="col-md-3 control-label">庫號</label>
	                            <div class="col-md-8">
	                              <input type="text" class="form-control" placeholder="" name="Mutispec[0][warehouse_num]" value="">
	                            </div>
	                          </label>	                          
	                          <label for="name" class="col-md-3 control-label">
	                            <label for="name" class="col-md-4 control-label">庫存量</label>
	                            <div class="col-md-5">
	                              <input type="number" min="2" class="form-control" placeholder="" name="qty_in_stock" value="" disabled>
	                            </div>
	                          </label>
	                          <label for="name" class="col-md-3 control-label">
	                            <label for="name" class="col-md-5 control-label">安全庫存量</label>
	                            <div class="col-md-5">
	                              <input type="number" class="form-control" placeholder="" name="Mutispec[0][safe_qty_in_stock]" value="">
	                            </div>
	                          </label>
	                        </div>
	                      <?php } ?>
													<span class="help-block">名稱空白 = 刪除該規格</span>
                    </div>
                  </div>
                  <!-- End of tab_stock -->
                  <div class="tab-pane" id="tab_additional_purchase">
                    <div class="portlet light">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs font-green-sharp"></i>
                          <span class="caption-subject font-green-sharp bold uppercase">加購列表</span>
                        </div>
                        <div class="actions btn-set">
                          <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="12" id="product_addon_sn" style="margin: 0 5px;" placeholder="請輸入商品編號">
                          <a data-toggle="modal" id="Addon" href="#basic" class="btn green"><i class="fa fa-plus"></i> 新增</a>
                        </div>
                      </div>
                      <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="table_member">
                          <thead>
                            <tr>
                              <th>商品編號</th>
                              <th>商品名稱</th>
                              <th>原價</th>
                              <th>加購價</th>
                              <th>數量限制</th>
                              <th>庫存量</th>
                              <th>功能</th>
                            </tr>
                          </thead>
                          <tbody id="Addon_list">
                          	<?php if($addons){foreach($addons as $addon){?>
                            <tr class="odd gradeX" id="Addon_tr<?php echo $addon['addon_log_sn'];?>">
                              <td><?php echo @$addon['associated_product_sn'];?></td>
                              <td><?php echo @$addon['product_name'];?></td>
                              <td class="center"><?php echo @$addon['product_orginal_price'];?></td>
                              <td class="center"><?php echo @$addon['addon_price'];?></td>
                              <td class="center"><?php echo @$addon['addon_limitation'];?></td>
                              <td class="center"><?php echo @$addon['unispec_qty_in_stock'];?> 
                              	&nbsp;<a href="admin/product/inventory/item?product_sn=<?php echo @$addon['associated_product_sn'];?>" target="_blank"><i class="fa fa-edit"></i>管理</a></td>
                              <td class="center">
                    						<input type="hidden" id="addon_log_sn" name="addon_log_sn" value="">
                                <a data-toggle="modal" class="EditAddon" addon_log_sn="<?php echo @$addon['addon_log_sn'];?>" href="#basic"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                                <a href="javascript:void(0);" class="deleteAddon" ItemId="<?php echo $addon['addon_log_sn'];?>" ><i class="fa fa-trash-o"></i>刪除</a>
                              </td>
                            </tr>
                            <?php } }?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                  </div>
                  <div class="tab-pane" id="tab_event">
                				<?php if($labels){ foreach($labels as $key=>$label){?>
	                    <div class="form-group col-md-11">
		                      <label class="col-md-2 control-label">
                    				<input type="hidden" name="labels[<?php echo $key?>][product__promotion_label_relation_sn]" value="<?php echo @$label['product__promotion_label_relation_sn']; ?>">
                    				<input type="hidden" name="labels[<?php echo $key?>][promotion_label_sn]" value="<?php echo $label['promotion_label_sn']; ?>">
		                        <input class="checkbox-list" type="checkbox" class="form-control" name="labels[<?php echo $key?>][status]" value="1" <?php if(@$label['status']) echo 'checked';?> >&nbsp;<?php echo $label['promotion_label_name']?>
		                      </label>
		                      <div class="col-md-9">
						                  <div class="col-md-5">
						                    <div class="input-group date form_datetime">
						                      <input type="text" class="form-control" name="labels[<?php echo $key?>][from]" value="<?php echo (@$label['start_date']!='0000-00-00 00:00:00')? @$label['start_date']:''; ?>">
						                      <span class="input-group-btn">
						                        <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
						                      </span>
						                    </div>
						                  </div>
						                  <div class="col-md-1">到</div>
						                  <div class="col-md-5">
						                    <div class="input-group date form_datetime">
						                      <input type="text" class="form-control" name="labels[<?php echo $key?>][to]" value="<?php echo (@$label['end_date']!='0000-00-00 00:00:00')? @$label['end_date']:''; ?>">
						                      <span class="input-group-btn">
						                        <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
						                      </span>
						                    </div>
						                  </div>
						                  <?php if($label['promotion_label_name']=='特價'){ ?>
					                        <div class="form-group col-md-14" style="margin-top: 45px;">
									                  <div class="col-md-4">
				                							<label for="seq" class="col-md-4 control-label">折數 <span class="require">*</span></label>
									                      <div class="col-lg-4">
									                        <input name="labels[<?php echo $key?>][discount_percentage]" type="number" maxlength="2" style="width:75px;" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" class="form-control form-inline" value="<?php echo (@$label['discount_percentage']) ? intval(@$label['discount_percentage']*100):''; ?>" >
									                      </div>
									                      <div class="col-lg-1"><span class="form-control-static form-inline">%</span></div>
							                      </div>
									                  <div class="col-md-2"><span class="help-block">請擇一輸入</span></div>
									                  <div class="col-md-5">
				               							  <label for="seq" class="col-md-5 control-label">折扣金額 <span class="require">*</span></label>
									                      <div class="col-md-5">
									                        <input name="labels[<?php echo $key?>][discount_amount]" type="number" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" class="form-control form-inline" value="<?php echo (@$label['discount_amount']) ? intval(@$label['discount_amount']):''; ?>" >
									                      </div>
									                  </div>
									               </div>
															<?php }?>
						                </div>
		                  </div>
		                    <?php } }?>
                  </div>
                  <!-- End of tab_event -->
                </div>
                <!-- End of tab-content -->
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <input type="hidden" id="product_sn" name="product_sn" value="<?php echo @$Item['product_sn']; ?>">
                    <input type="hidden" id="channel_name" name="Item[default_channel_name]" value="<?php echo @$channel_name; ?>">
                    <input type="hidden" id="currency_code" name="Item[currency_code]" value="<?php echo (@$Item['currency_code'])? @$Item['currency_code']:'1'; ?>">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="button" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">設定加購價</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form">
          <div class="form-group">
            <label for="name" class="col-md-3 control-label">商品名稱</label>
            <div class="col-md-9">
              <p class="form-control-static" id="addon_product_name"></p>
            </div>
          </div>
          <div class="form-group">
            <label for="name" class="col-md-3 control-label">原最低售價</label>
            <div class="col-md-9">
              <p class="form-control-static" id="product_orginal_price">0</p>
            </div>
          </div>
          <div class="form-group">
            <label for="name" class="col-md-3 control-label">加購價</label>
            <div class="col-md-5">
              <input type="text" class="form-control" id="addon_price" value="" required>
            </div>
          </div>
          <div class="form-group">
            <label for="name" class="col-md-3 control-label">加購數量限制</label>
            <div class="col-md-2">
              <input type="text" class="form-control" id="addon_limit" value="1" required>
            </div>
            <div class="col-md-7">
              <span class="help-block">數量限制隨著主商品數量倍增，所以填入 3 ，買兩個主商品就最多就能買 6 個加購商品</span>
            </div>
          </div>
          
        </form>
      </div>
      <div class="modal-footer">
        <input type="hidden" id="associated_product_sn" name="associated_product_sn" value="">
        <input type="hidden" id="ifedit" name="ifedit" value="">
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
        <button type="button" id="Add_addon" class="btn blue">儲存</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" id="addAttrValue" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">新增屬性值</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form">
          <div class="form-group">
            <label for="name" class="col-md-2 control-label">分類屬性</label>
            <div class="col-md-10">
              <p class="form-control-static" id="AttrName">分類屬性1</p>
            </div>
          </div>
          <div class="form-group">
            <label for="name" class="col-md-2 control-label">屬性值</label>
            <div class="col-md-10">
              <input type="text" class="form-control" id="AttributeValue" value="">
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <input type="hidden" id="attribute_sn" name="attribute_sn" value="">
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
        <button type="button" id="AddAttributeValue" class="btn blue">確定新增</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

