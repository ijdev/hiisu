<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<style>
@media (max-width: 768px){
.cash-table {border:none;}
.cash-table tbody{border:none !important;}
.cash-table th {display: none;}
.cash-table tr.total {display: flex;border-bottom: 2px solid #ddd;margin-top:5px;}
.cash-table tr.total td:nth-of-type(1) {width: 136px;border: none;}
.cash-table tr.total td:nth-of-type(2) {width: calc(100% - 136px);border: none;}
.cash-table tr.total td[colspan="2"] {display: none;}
.cash-table tr.gradeX {display: flex;flex-wrap: wrap; border: 1px solid #ddd; margin:10px 0;}
.cash-table tr.gradeX td {display: block;text-align: left !important;padding-left: 5em;position: relative;min-height: 34px;width: 100%;}
.cash-table tr.gradeX td:before {content: attr(data-th);padding: 0 .5em;position: absolute;left: 0;}
.cash-table tr.gradeX td[data-th="編號"] {width: 50%;order: -1;}
.cash-table tr.gradeX td[data-th="狀態"] {width: 50%;order: -1;padding-left: 3em;}
.product-page{padding: 0;margin: 0 -15px;}
}
</style>
<div class="container">
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <li class="active">囍市錢包</li>
</ul>
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar col-md-2">
    <div class="desktop_view_sidebar">
		<ul class="list-group margin-bottom-25 sidebar-menu">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
	<div class="mobile_view_sidebar">
		<div class="side-nav-head border_title_bottom">
			<button class="fa fa-bars collapsed" data-toggle="collapse" data-target="#mob_sidebar_drop_down"></button>
			<h4 class="bfont">會員中心<!--會員中心--></h4>
		</div>
		<ul class="list-group margin-bottom-25 sidebar-menu collapse" id="mob_sidebar_drop_down">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="col-md-10 col-sm-12">
    <div class="product-page">
      <h2 style="height: 50px;">
        <i class="fa fa-dollar"></i> 囍市錢包
        <div style="font-size: 18px;display: inline;float: right;margin-right:20px;">當前總餘額：<span style="color:red;font-weight: bold;"><?=number_format($cash)?></span></div>
      </h2>
      <!--div class="alert alert-warning">
         當月的數目仍為<strong>預估值</strong>，正確數量將在下個月的15日結算，匯款完成系統也會透過 Email 通知您!
      </div-->
      <form method="post">
          <div class="note note-warning">
            <div class="input-group input-large date-picker input-daterange  pull-left"  data-date-format="yyyy-mm-dd">
              <span class="input-group-addon"> 期間 </span>
              <input type="text" class="form-control" name="from" value="<?=$data_array['from']?>">
              <span class="input-group-addon"> 到 </span>
              <input type="text" class="form-control" name="to" value="<?=$data_array['to']?>">
            </div>
            <div class="table-actions-wrapper pull-right">
              <select name="cash_type" class="table-group-action-input form-control input-inline input-sm">
                <option value="">類別</option>
                 <?php foreach($cash_types as $_key=>$_value){
                      echo '<option value="'.$_key.'"';
                      if(@$cash_type==$_key)
                      echo ' selected="selected"';
                      echo '>'.$_value.'</option>';
                   }?>
              </select>
              <select name="status" class="table-group-action-input form-control input-inline input-sm" style="font-size:11px;">
                <option value="">狀態</option>
              <?php foreach($status_ct as $_Item){ ?>
                <option <?php echo (@$data_array['status']==$_Item['status'])?'selected':'';?> value="<?php echo $_Item['status']?>"><?php echo $_Item['status_name']?></option>
              <?php }?>
              </select>
              <button type="submit" class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button>
            </div>
            <div style="clear:both;"></div>
          </div>
        </form>
      <table class="table table-striped table-bordered table-hover cash-table" id="table_member">
        <thead>
          <tr>
            <th>編號</th>
            <th>分類</th>
            <th>訂單編號</th>
            <th>建立時間</th>
            <th style="text-align: right;">金額</th>
            <th>備註</th>
            <th>狀態</th>
          </tr>
        </thead>
        <tbody>
      <?php if(is_array(@$orders)){
        $total=0;
        foreach($orders as $key => $value){
          $total+=$value['cash_amount'];
        ?>
            <tr class="odd gradeX">
              <td data-th="編號"><?=$value['gift_cash_sn']?></td>
              <td data-th="分類"><?=@$cash_types[$value['cash_type']]?></td>
              <td data-th="訂單編號"><?=$value['sub_order_num']?></td>
              <td data-th="建立時間"><?=$value['create_date']?></td>
              <td data-th="金額" style="text-align: right;"><?=$value['cash_amount']?></td>
              <td data-th="備註"><?=$value['memo']?></td>
              <td data-th="狀態" class="center"><span class="label label-<?=($value['status']==1)?'success':'warning'?>"><?=$value['status_name']?></span></td>
            </tr>
<?php }}?>
        </tbody>
        <tfooter>
          <tr class="total">
            <td style="text-align: right;" colspan="4">合計（不分頁數）：</td>
            <td style="text-align: right;"><?=number_format($total)?></td>
            <td colspan="2"></td>
          </tr>
        </tfooter>
      </table>
    </div>
  </div>
  <!-- END CONTENT -->
</div>
</div>
<!-- END SIDEBAR & CONTENT -->