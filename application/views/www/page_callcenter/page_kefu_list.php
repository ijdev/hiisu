<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 會員中心 ticket 列表
* 按下狀態與回應可以看到與客服人員的通訊記錄 (modal ajax)
* 若狀態為以解決，ajax (ex.preview/callcenterMsg/done) 取回的頁面資料中不會有 modal footer (無送出表單)
*/
?>
<div class="container mob_padding">
<style>
  .highlight a.ticket_process{
  }
</style>
<!-- Page level plugin styles START -->
<!-- Page level plugin styles END -->
<div class="container mob_padding">
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="member">會員中心</a></li>
    <li class="active">客服中心</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar col-md-2 col-sm-3">
    <div class="desktop_view_sidebar">
		<ul class="list-group margin-bottom-25 sidebar-menu">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
	<div class="mobile_view_sidebar">
		<div class="side-nav-head border_title_bottom">
			<button class="fa fa-bars collapsed" data-toggle="collapse" data-target="#mob_sidebar_drop_down"></button>
			<h4 class="bfont">會員中心<!--會員中心--></h4>
		</div>
		<ul class="list-group margin-bottom-25 sidebar-menu collapse" id="mob_sidebar_drop_down">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="col-md-10 col-sm-9">

      <!-- Modal 顯示與客服的訊息列表 -->
      <div class="modal fade" id="ajax_msg_call" role="basic" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">訊息記錄</h4>
            </div>
            <div class="modal-body">
              <img src="public/metronic/global/img/loading-spinner-grey.gif" alt="" class="loading">
              <span>
              &nbsp;&nbsp;內容載入中... </span>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn blue">送出</button>
            </div>
          </div>
        </div>
      </div>
      <!-- /Modal 顯示與客服的訊息列表 -->
      <!-- Modal 顯示與客服的訊息列表 -->
      <div class="modal fade" id="ajax_msg_done" role="basic" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">訊息記錄</h4>
            </div>
            <div class="modal-body">
              <img src="public/metronic/global/img/loading-spinner-grey.gif" alt="" class="loading">
              <span>
              &nbsp;&nbsp;內容載入中... </span>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn blue">送出</button>
            </div>
          </div>
        </div>
      </div>
      <!-- /Modal 顯示與客服的訊息列表 -->
      <!-- Modal for 表單填寫 -->
       <form role="form" name="ticket_form" method="post" action="member/addCallcenter">
      <div class="modal fade" id="ticket_form" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">請說明您的問題</h4>
            </div>
            <div class="modal-body">
               <div class="form">

                  <div class="form-body">
                    <div class="form-group">
                      <label>問題主題<span class="require">*</span></label>
                      <div class="input-icon">
                        <i class="fa fa-question-circle"></i>
                        <input type="text" required name="question_subject" class="form-control" placeholder="請輸入您的問題主題">
                      </div>
                    </div>
                   <div class="form-group">
                      <label>訂單編號</label>
                      <div class="input-icon">
                        <i class="fa fa-file-o"></i>
                        <input type="text" name="associated_order_sn" id="associated_order_sn" class="form-control" placeholder="若您欲詢問的問題與訂單有關，請您儘可能提供">
                      </div>
                    </div>
                    <div class="form-group">
                      <label>問題大類</label>
                      <select name="ccapply_code" id="ccapply_code" required class="form-control">
                        <option value="">請選擇</option>
                        <?php foreach($ccapply as $_Item){?>
                        <option value="<?php echo $_Item['ccapply_code']?>"><?php echo $_Item['ccapply_name']?></option>
                        <?php }?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>問題類型</label>
                      <select name="ccapply_detail_code" id="ccapply_detail_code" required class="form-control">
                        <option value="">請先選擇大類</option>
                      </select>
                    </div>
                    <!-- <div class="form-group">
                      <label>附加檔案</label>
                      <input type="file" id="exampleInputFile1">
                      <p class="help-block">
                         ※ 檔案大小2MB為限，附件格式僅接受jpeg、png。
                      </p>
                    </div>-->
                    <div class="form-group">
                      <label>問題說明 <span class="require">*</span></label>
                      <div class="input-icon">
                        <textarea required name="question_description" class="form-control" rows="5"></textarea>
                      </div>
                    </div>
                  </div>

               </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn default" data-dismiss="modal">關閉</button>
              <button type="submit" id="ticket_submit" class="btn blue">送出表單</button>
            </div>
          </div>
          </form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- Modal for 表單填寫 -->
    <!-- /End of product-page -->
  </div>
  <!-- END CONTENT -->
  <!-- copy content -->
				<div id="kefu_list_main" class="col-md-10 col-sm-9 margin-bottom-30 product_right border_left minheight">
                              <!--會員留言開始 -->
									<ul class="nav nav-tabs nav-button-tabs margin-bottom-20">
       									<li class="top_btns"><a href="member/callcenter">會員留言</a></li>
     									<li class="active top_btns"><a href="/member/kefu_list">留言紀錄</a></li>
      								</ul>
						<div class="kefu_list_table">
							<table class="col-md-12 pull-left padding0">
								<thead>
								<tr>
								<th class="col-md-2">表單時間  </th>
								<th class="col-md-1"> 編號 </th>
								<th class="col-md-2"> 問題主題</th>
								<th class="col-md-2">問題類型  </th>
								<th class="col-md-2">狀態  </th>
								<th class="col-md-1"> 回應 </th>
								<th class="col-md-2">更新時間 筆數：  </th>
								</tr>
								</thead>
								<tbody class="kefutbody">
                   <?php foreach($_result01 as $key => $value){
                    //var_dump($value);
                    $question_status_name=(@$value->member_question_status_name)? @$value->member_question_status_name:'未處理';
                    echo '
                    <tr>
                      <td class="col-md-2 mob_title2" data-title="表單時間">'.$value->issue_date.'</td>
                      <td class="col-md-1 mob_title2" data-title="編號">'.$value->member_question_sn.'</td>
                      <td class="col-md-2 mob_title2" data-title="問題主題">'.$value->question_subject.'</td>
                      <td class="col-md-2 mob_title2" data-title="問題類型">'.$value->ccapply_detail_code_name.'</td>
                      <td class="col-md-2 mob_title2" data-title="狀態"><a href="member/callcenterMsg/'.$value->member_question_sn.'" data-remote="false" data-target="#ajax_msg_call" data-toggle="modal" class="btn_ticket_view text-'.$value->description.'">'.$question_status_name.'</a></td>
                      <td class="col-md-1 mob_title2" data-title="回應"><a href="member/callcenterMsg/'.$value->member_question_sn.'" data-remote="false" data-target="#ajax_msg_call" data-toggle="modal" class="btn_ticket_view">'.$value->reply_count.'</a></td>
                      <td class="col-md-2 mob_title2" data-title="更新時間">'.$value->last_time_update.'</td>
                    </tr>
                    ';
                   }
                    ?>
								</tbody>
							</table>
          <!-- page nav -->
            <div class="row" >
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">筆數：<span id="wish_count"><?php echo $total;?></span> </div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                  <?php echo $page_links;?>
                </div>
              </div>
            </div>
          <!-- end of page nav -->
						</div>
				</div>
						</div>
						<!--右邊結束-->
					</div>
<!-- copy content -->
</div>



</div> <!--container -->
</div>
<!-- END SIDEBAR & CONTENT -->
<style>


.display_none{
	display:none;
}
.nav-tabs.nav-button-tabs>li.active>a {
    color: #fff !important;
    background-color: rgba(0,0,0,0.6)!important;
    padding: 10px 15px;
    cursor: pointer;
}
.nav-tabs.nav-button-tabs>li>a {
    color: #fff !important;
    padding: 10px 15px;
}
.nav-tabs.nav-button-tabs>li>a {
    color: #000 !important;
    border: 0 !important;
    background-color: #edeff1;
    margin-right: 3px !important;
    -webkit-border-radius: 3px !important;
    -moz-border-radius: 3px !important;
    border-radius: 3px !important;
}
.nav-tabs.nav-button-tabs>li>a:hover {
	background-color: #edeff1;
}
.nav-tabs {
    border: none;
}
.btn-black {
    background-color: #111;
    color: #FFF !important;
}
.border_left {
    border-left: 1px solid rgba(100,100,100,0.1) !important;
}

</style>
