<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品分類 <small>上架管理</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/admin">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品分類
        <i class="fa fa-circle"></i>
      </li>
      <!--li>
         <a href="admin/product/productCategory">上架管理</a>
         <i class="fa fa-circle"></i>
      </li-->
      <li class="active">
         上架管理 - <?php echo @$CategoryItem['category_name']; ?> 編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">分類上架設定</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/product/dealerCategory" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post">
            <div class="form-group">
              <label for="name" class="col-md-3 control-label">是否上架<span class="require">*</span></label>
              <div class="col-md-8">
                <div class="radio-list">
                    <label class="radio-inline">
                      <input type="radio" name="category[relation_status]" id="optionsRadios4" value="1" <?php if(@$CategoryItem['relation_status']){ ?>checked<?php }?> required>  上架
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="category[relation_status]" id="optionsRadios5" value="0" <?php if(@$CategoryItem['relation_status']==='0'){ ?>checked<?php }?> required>  下架
                    </label>
                </div>
              </div>
            </div>
                <div class="form-group">
                  <label for="seq" class="col-md-3 control-label">排序 <span class="require">*</span></label>
                  <div class="col-md-8">
                    <input name="category[sort_order]" maxlength="4" type="text" class="form-control" value="<?php echo (@$CategoryItem['sort_order']) ? @$CategoryItem['sort_order']:'999'; ?>" required>
                  </div>
                </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <input type="hidden" name="product_relation_sn" value="<?php echo @$CategoryItem['product_relation_sn']; ?>">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="reset" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-md-3 control-label">名稱 </label>
                  <div class="col-md-8">
                    <?php echo @$CategoryItem['category_name']; ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-md-3 control-label">副標</label>
                  <div class="col-md-8">
                    <?php echo @$CategoryItem['category_sub_name']; ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="seq" class="col-md-3 control-label">分潤比例 </label>
                  <div class="col-md-8">
                    <div class="form-group">
                      <div class="col-lg-1">
                        <b><?php echo (@$CategoryItem['profit_sharing_rate']) ? intval(@$CategoryItem['profit_sharing_rate']*100):'5'; ?></b>
                        <span class="form-control-static form-inline">%</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">分類 Banner</label>
                  <div class="col-md-8">
                    <?php if(@$CategoryItem['banner_save_dir']){?><img class="img-responsive" src="<?php echo ADMINUPLOADPATH.@$CategoryItem['banner_save_dir']?>"><?php }?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">分類主圖（首頁）</label>
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-lg-6">
                    <?php if(@$CategoryItem['main_picture_save_dir']){?><img style="width:250px;height:250px;" class="img-responsive" src="<?php echo ADMINUPLOADPATH.@$CategoryItem['main_picture_save_dir']?>"><?php }?>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">分類圖（選單）</label>
                  <div class="col-md-8">
                    <span class="help-block">最佳大小 700px X 300px</span>
                    <?php if(@$CategoryItem['main_banner']){?><img class="img-responsive" src="<?php echo ADMINUPLOADPATH.@$CategoryItem['main_banner']?>"><?php }?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">注意事項</label>
                  <div class="col-md-8">
                    <?php echo @$CategoryItem['return_desc']; ?>
                  </div>
                </div>
            </form>
              </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->