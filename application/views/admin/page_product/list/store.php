<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
//var_dump($Items);
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品列表 <!--small>結婚商店</small--></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container" style="width: 100%;">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/admin">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品列表
      </li>
      <!--li class="active">
        結婚商店
      </li-->
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">商品列表</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/product/productItem" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <form method="get">
            <div class="note note-warning" id="search_filter">
              <div class="form-group">
                <div class="row">
                  <div class="col-md-8">
                    <input name="search[product_sn]" value="<?=@$search['product_sn']?>" type="text" class="pagination-panel-input form-control input-inline input-sm" size="20" placeholder="商品編號">
                    <input name="search[product_name]" value="<?=@$search['product_name']?>" type="text" class="pagination-panel-input form-control input-inline input-sm" size="20" placeholder="商品名稱">
                <select id="category" name="search[default_root_category_sn]"  class="table-group-action-input form-control input-inline  input-sm" style="font-size:11px;">
                  <option value="">請選擇主分類</option>
                  <?php
                  foreach($ij_categorys as $_item) {
                    $selected='';
                    if(@$search['default_root_category_sn']==$_item['category_sn']) $selected='selected';
                    echo  ' <option '.$selected.' value="'.$_item['category_sn'].'">'.$_item['category_name'].'</option>';
                  }?>
                </select>
                <select id="supplier" name="search[supplier_sn]" class="table-group-action-input form-control input-inline  input-sm" style="font-size:11px;">
                  <option value="">請選擇供應商</option>
                  <?php
                  foreach($suppliers as $keys=>$values) {
                    $selected='';
                    if(@$search['supplier_sn']==$keys) $selected='selected';
                    echo  ' <option '.$selected.' value="'.$keys.'">'.$values.'</option>';
                  }?>
                </select>
                <select id="product_status" name="search[product_status]" class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;">
                  <option value="">請選擇狀態</option>
                  <?php
                  foreach($product_status as $keys=>$values) {
                    $selected='';
                    if(@$search['product_status']==$keys) $selected='selected';
                    echo  ' <option '.$selected.' value="'.$keys.'">'.$values.'</option>';
                  }?>
                </select>
                <select id="promotion_label" name="search[promotion_label]" class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;">
                  <option value="">請選擇</option>
                  <?php
                  foreach($promotion_labels as $key=>$value) {
                    $selected='';
                    if(@$search['promotion_label']==$key) $selected='selected';
                    echo  ' <option '.$selected.' value="'.$key.'">'.$value.'</option>';
                  }?>
                </select>

                <button type="submit" class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button>
                <button type="button" id="reset_form" onclick="location.href='/admin/product/productTable';" class="btn btn-sm purple"><i class="fa"></i> 清除</button>
                  </div>
                </div>
              </div>
              <div style="clear:both;"></div>
            </div>
          </form>
            <?php $this->load->view('sean_admin/general/flash_error');?>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>主分類 <i class="fa fa-edit"></i>(雙擊可編輯)</th>
                  <th>編號</th>
                  <th>名稱</th>
<?=($this->session->userdata('member_role_system_rules')[0]['role_name']!='供應商管理員')?'<th>供應商 <i class="fa fa-edit"></i></th>':'';?>
                  <th>定價</th>
                  <th>售價</th>
                  <th>定價/規格</th>
                  <th width="60">代表圖</th>
                  <th>推薦</th>
                  <th>熱賣</th>
                  <th>特價</th>
                  <th>獨家</th>
                  <th>狀態 <i class="fa fa-edit"></i></th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
<?php	if(@$Items){foreach($Items as $Item){?>
                  <tr class="odd gradeX">
                    <td class="click-to-select" mode="default_root_category_sn" item_id="<?php echo @$Item['product_sn'];?>" category_id="<?php echo @$Item['default_root_category_sn'];?>"><?php echo @$Item['category_name'];?></td>
                    <td><?php echo $Item['product_sn'];?></td>
                    <td><?php echo $Item['product_name'];?><a href="/shop/shopItem/<?php echo $Item['product_sn'];?>" target="_blank">&nbsp;<i class="fa fa-external-link"></i></a></td>
<?=($this->session->userdata('member_role_system_rules')[0]['role_name']!='供應商管理員')?'<td class="click-to-select" mode="supplier_sn" item_id="'.@$Item['product_sn'].'" supplier_sn="'.@$Item['supplier_sn'].'">'.$Item['supplier_name'].'<a href="/admin/supplier/supplierTable/edit/'.$Item['supplier_sn'].'" target="_blank">&nbsp;<i class="fa fa-external-link"></i></a></td>':'';?>
                    <td><?php echo @$Item['price1'];?></td>
                    <td><?php echo @$Item['min_price'];?></td>
                    <td class="center">
                      <?php if($Item['pricing_method']=='1'):?>
                        單一
                      <?php elseif($Item['pricing_method']=='2'):?>
                        區間
                      <?php endif;?>／
                      <?php if($Item['unispec_flag']=='1'):?>
                        單一
                      <?php elseif($Item['unispec_flag']=='0'):?>
                        多重
                      <?php endif;?>
                    </td>
                    <td><?php if($Item['image_path']):?><img onerror="this.src='/public/uploads/notyet.gif';" src="/public/uploads/<?php echo @$Item['image_path'];?>" width="60" ><?php else:?>無<?php endif;?></td>
									<?php if(@$Item['promotion']){ //var_dump($Item['promotion']).'<br>';
									foreach(@$Item['promotion'] as $pro){?>
                    <td class="center">
                      <?php if(@$pro['status']=='1'):?>
                        <i class="fa fa-check"></i>有
                      <?php else:?>
                        <i class="fa fa-times"></i>
                      <?php endif;?>
                    </td>
                  <?php }}else{ ?>
                   <td class="center">
                        <i class="fa fa-times"></i>
                    </td>
                   <td class="center">
                        <i class="fa fa-times"></i>
                    </td>
                   <td class="center">
                        <i class="fa fa-times"></i>
                    </td>
                   <td class="center">
                        <i class="fa fa-times"></i>
                    </td>
                  <?php }?>
                    <td class="click-to-select" mode="product_status" item_id="<?php echo @$Item['product_sn'];?>" product_status="<?php echo @$Item['product_status'];?>" class="center">
                      <?=$all_product_status[$Item['product_status']]?>
                    </td>
                    <td class="center">
                      <a href="admin/product/productItem/<?php echo $Item['product_sn'];?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
    <?if(stripos($this->page_data['permission'],'del')!==false){?>
                      <a href="javascript:void(0);" class="deleteItem" ItemId="<?php echo $Item['product_sn'];?>"><i class="fa fa-trash-o"></i>刪除</a>
    <?}?>
                    </td>
                  </tr>
<?php }}?>
              </tbody>
            </table>

          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->