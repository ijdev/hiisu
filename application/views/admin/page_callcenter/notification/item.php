<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/typeahead/typeahead.css">
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/pages/css/portfolio.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>客服中心<small>客服通知發送管理</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="/admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/CRM/notification">客服通知發送管理</a><i class="fa fa-circle"></i>
      </li>
      <li class="active">
        發送/編輯客服通知
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">發送/編輯客服通知</span>
            </div>
            <div class="tools">
              <a href="admin/CRM/notification"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">通知對象 <span class="require">*</span></label>
		                    <div class="checkbox-list">
		                    	<?php foreach($send_obj as $_item){?>
		                      <label class="col-xs-2 control-label">
		                        <input class="checkbox-list" type="checkbox" class="form-control" name="notify_ta[]" value="<?php echo $_item['member_level_type'];?>" <?php if($_item['checked']) echo 'checked';?> >&nbsp;<?php echo $_item['member_level_type_name'];?>
		                      </label>
		                    <?php }?>
		                    </div>
                  <!--div class="col-lg-10">
                    <textarea class="form-control" name="emails" rows="3" placeholder="通知對象"></textarea>
                    <span class="help-block">請填寫 Email, 多筆請用逗號","隔開</span>
                  </div-->
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">主題 <span class="require">*</span></label>
                  <div class="col-lg-10">
                    <input type="text" name="notification[message_subject]" value="<?php echo @$Item['message_subject']; ?>" class="form-control" placeholder="系統訊息主題">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">內容</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" name="notification[message_content]" rows="6" placeholder="通知內容"><?php echo @$Item['message_content']; ?></textarea>
                    <span class="help-block">內容或連結擇一填寫，若存在連結內容將無法點入，使用者會直接進入連結網頁</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">連結</label>
                  <div class="col-lg-10">
                    <input type="url" class="form-control" name="notification[message_link_url]" value="<?php echo @$Item['message_link_url']; ?>" placeholder="通知連結">
                    <span class="help-block">內容或連結擇一填寫，若存在連結內容將無法點入，使用者會直接進入連結網頁</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">顯示時間</label>
                  <div class="col-lg-10">
                    <div class="input-group date form_datetime">
                      <input type="text" class="form-control" name="notification[default_display_time]"  value="<?php echo @$Item['default_display_time']; ?>">
                      <span class="input-group-btn">
                        <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-md-2 control-label">更新者</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['update_member_sn'])?@$Item['update_member_sn']:@$Item['create_member_sn']; ?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-md-2 control-label">更新時間</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['last_time_update']!='0000-00-00 00:00:00')?@$Item['last_time_update']:@$Item['create_date']; ?>" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-2 col-md-10">
                    <button type="submit" class="btn green">發送</button>
                    <a href='admin/CRM/notification'><button type="button" class="btn default">Cancel</button></a>
                    <input type="hidden" name="member_message_center_sn" value="<?php echo @$Item['member_message_center_sn']; ?>">
                    <input type="hidden" name="notification[main_config_flag]" value="<?php echo @$Item['main_config_flag']; ?>">
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->