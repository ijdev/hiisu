<?php defined('BASEPATH') OR exit('No direct script access allowed');
//var_dump($this->router->fetch_class());
//var_dump($this->router->fetch_method());
$general_header=$this->page_data['general_header'];
$title=isset($title)?$title:'HiiSU 囍市集 | 婚禮購物商城 | Shop for your dream wedding';
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="zh-tw" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="zh-tw" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zh-tw">
<!--<![endif]-->
<!-- Head BEGIN -->
<head>
  <meta charset="utf-8">
  <title><?php echo  @$title;?></title>
  <base href="<?php echo $this->config->item('base_url'); ?>" />
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="description" content="<?php echo @$description;?>">
  <meta property="og:site_name" content="HiiSU 囍市集 | 婚禮購物商城 | Shop for your dream wedding">
  <meta property="og:title" content="<?php echo @$title;?>">
  <meta property="og:description" content="<?php echo @$description;?>">
<?php if(@$sliders['sliders']['banners']){ foreach($sliders['sliders']['banners'] as $key=>$_Item){?>
  <meta property="og:image" content="<?php echo $this->config->item('base_url'); ?>/public/uploads/<?php echo $_Item['banner_content_save_dir'];?>">
<?php }}?>
<?php if(@$page_content['product']['images']){ foreach($page_content['product']['images'] as $key=>$_Item){?>
  <meta property="og:image" content="<?php echo $this->config->item('base_url'); ?>/public/uploads/<?php echo $_Item['image_path'];?>">
<?php }}?>
<?php if(@$main_picture_save_dir){?>
  <meta property="og:image" content="<?php echo $this->config->item('base_url'); ?>/public/uploads/<?php echo $main_picture_save_dir;?>">
<?php }?>
<?php if(@$banner_save_dirx){?>
  <meta property="og:image" content="<?php echo $this->config->item('base_url'); ?>/public/uploads/<?php echo $banner_save_dir;?>">
<?php }?>
  <meta property="og:url" content="<?php echo base_url(uri_string());?>">

  <link rel="icon" type="image/png" href="public/img/favicon.png" />

  <!-- Fonts START -->
  <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
  <!-- Fonts END -->

  <!-- Global styles START -->
  <link href="public/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
  <link href="public/metronic/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Global styles END -->

  <!-- Theme styles START -->
  <link href="public/metronic/global/css/components.css" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="public/metronic/frontend/layout/css/style.css?v=1" rel="stylesheet">
  <link href="public/metronic/frontend/pages/css/style-shop.css?v=1.4.2" rel="stylesheet" type="text/css">
  <link href="public/metronic/frontend/layout/css/style-responsive.css" rel="stylesheet">
  <link href="public/metronic/frontend/pages/css/style-layer-slider.css" rel="stylesheet">
  <link href="public/metronic/frontend/layout/css/themes/red.css" rel="stylesheet" id="style-color">
  <link href="public/metronic/frontend/layout/css/custom.css" rel="stylesheet">

	<link href="public/metronic/global/plugins/webslide/color-theme.css" rel="stylesheet">
	<link href="public/metronic/global/plugins/webslide/webslidemenu.css" rel="stylesheet">
	<link href="public/metronic/global/css/brown.css" rel="stylesheet">
	<link href="public/metronic/global/css/header.css?v=1.5.5" rel="stylesheet">  <!-- Theme styles END -->
<?php if($this->router->fetch_class()=='shop' && $this->router->fetch_method()=='index'){?>
  <meta property="og:type" content="website">
  <link rel="canonical" href="https://www.hiisu.shop/">
<?php }elseif($this->router->fetch_class()=='shop' && $this->router->fetch_method()=='shopItem'){?>
  <meta property="og:type" content="product">
  <meta property="og:availability" content="instock">
  <link rel="canonical" href="https://www.hiisu.shop/shop/shopItem/<?php echo @$page_content['product']['product_sn'];?>">
<?php }elseif($this->router->fetch_class()=='shop' && $this->router->fetch_method()=='shopCatalog'){?>
  <meta property="og:type" content="website">
  <link rel="canonical" href="https://www.hiisu.shop/shop/shopCatalog/<?php echo @$_key_id;?>">
<?php }elseif($this->router->fetch_class()=='home' && $this->router->fetch_method()=='blogList'){?>
  <meta property="og:type" content="article">
  <link rel="canonical" href="https://www.hiisu.shop/home/blogList/">
<?php }elseif($this->router->fetch_class()=='home' && $this->router->fetch_method()=='blogItem'){?>
  <meta property="og:type" content="article">
  <meta property="og:image" content="<?php echo $this->config->item('base_url'); ?>/public/uploads/<?php echo $page_content['Item']['blog_article_image_save_dir'];?>">
<?php }else{?>
  <meta property="og:type" content="website">
<?php }?>
<style>
  .container{
    max-width: 1200px;
    /*padding: 0px;*/
    width: 100%;
  }
  @media screen and (max-width: 768px){
.breadcrumb{display:none;}
/*.breadcrumb>li:nth-of-type(1),.breadcrumb>li:nth-of-type(2):before{display:none;}*/
}
</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-65986140-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-65986140-4');
</script>
</head>
<!-- Head END -->


<!-- Body BEGIN -->
<div class="ecommerce page-header-fixed">
    <!-- BEGIN HEADER -->
    <?php $this->load->view(($this->uri->segment(1)? '/':'').'www/general/header', isset($general_header)? $general_header:null);?>
    <!-- Header END -->

    <!-- BEGIN SLIDER -->
	<div class="container">
    <?php if(isset($slider)):?>
	    <?php $this->load->view($slider,$sliders);?>
    <?php endif;?>
    </div>
	<!-- END SLIDER -->

	<div class="main">
    <div class="container">
			<?php $this->load->view($page_content['view_path'], $page_content);?>
		</div>
	</div>

    <!-- BEGIN PRE-FOOTER -->
    <div class="pre-footer">
      <div class="container">
        <div class="row">
          <!-- BEGIN BOTTOM ABOUT BLOCK -->

          <!-- END BOTTOM ABOUT BLOCK -->

          <!-- BEGIN BOTTOM CONTACTS -->

          <!-- END BOTTOM CONTACTS -->

          <!-- BEGIN TWITTER BLOCK -->
          <!-- END TWITTER BLOCK -->
        </div>
      </div>
    </div>
    <!-- END PRE-FOOTER -->

    <!-- BEGIN FOOTER -->
    <div class="footer">
      <div class="container">
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class ="col-md-6 col-lg-6 ftr_left-cnt clearfix "><div class="footer-menu col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="men-us col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<li class="menu_link">
						<p><a class="fmenu3"href="/home/aboutus"> 關於我們</a></p>
						<p><a href="/home/blogList/網站最新消息">最新消息</a></p>
						<p><a href="/member/terms">會員服務條款</a></p>
						<p><a href="/member/privacy">隱私權政策聲明</a></p>
						<p><a href="/home/contactus">聯絡我們</a></p>
					</li>
					<li class="mob_menu_link">
						<button class="accordion"><a class="fmenu3"href="/home/aboutus"> 關於我們</a></button>
						<div class="panel">
							<ul>
								<li><p><a href="/home/blogList/網站最新消息">最新消息</a></p></li>
								<li><p><a href="/member/terms">會員服務條款</a></p></li>
								<li><p><a href="/member/privacy">隱私權政策聲明</a></p></li>
								<li><p><a href="/home/partners">合作提案</a></p></li>
							</ul>
						</div>
					</li>
				</div>
				<div class="men-us  col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<li class="menu_link">
						<p><span style="color:white;" class="fmenu3">客戶服務</span></p>
						<p><a href="/home/blogList">婚禮情報</a></p>
						<p><a href="/home/faq">常見問題</a></p>
						<p><a href="/member/callcenter">客服中心</a></p>
					</li>
					<li class="mob_menu_link">
						<button class="accordion"><span style="color:white;" class="fmenu3">客戶服務</span></button>
						<div class="panel">
							<ul>
								<li><p><a href="/home/blogList">婚禮情報</a></p></li>
								<li><p><a href="/home/faq">常見問題</a></p></li>
								<li><p><a href="/member/callcenter">客服中心</a></p></li>
							</ul>
						</div>
					</li>
				</div>
				<div class="men-us  col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<li class="menu_link">
						<p><span style="color:white;" class="fmenu3">我的帳戶</span></p>
						<p><a href="/member/memberOrderList">我的訂單</a></p>
						<!--<p><a href="/member/memberOrderList">取消訂單/退貨</a></p>-->
						<p><a href="/member/wishList">追蹤清單</a></p>
						<p><a href="/member/memberAccount">個人資料</a></p>
						<p><a href="/member/memberPassword">修改密碼</a></p>
					</li>
					<li class="mob_menu_link">
						<button class="accordion"><span style="color:white;" class="fmenu3">我的帳戶</span></button>
						<div class="panel">
							<ul>
								<li><p><a href="/member/memberOrderList">我的訂單</a></p></li>
								<!--<li><p><a href="/member/memberOrderList">取消訂單/退貨</a></p></li>-->
								<li><p><a href="/member/wishList">追蹤清單</a></p></li>
								<li><p><a href="/member/memberAccount">個人資料</a></p></li>
								<li><p><a href="/member/memberPassword">修改密碼</a></p></li>
							</ul>
						</div>
					</li>
				</div>

			</div>
			</div>
			<div class="rgh2 col-md-6 col-lg-6">
        <div  class="ftimg col-md-12 col-lg-12 ">
    			<div class="footnew2"></div>
    			<img class="logoimg img-responsive" src="public/img/ij-logo.png" alt="ijWedding 婚禮網站"/>
        </div>
  			<div class="new_footr col-md-12 col-lg-12">
  			 <div class="taime col-md-12 col-lg-12">服務時間：週一至週五 10:00 – 18:00</div>
  			 <?/*<div class="taime col-md-12 col-lg-12"><span>客服專線：02-2599-5865</span></div>*/?>
  			 <div class="taime col-md-12 col-lg-12">客服信箱：service@aqevent.com</div>
  			</div>
      </div>
        </div>
    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12 border-b">
      <div class="taime text-center">copyright © 2018-<?=date('Y')?> aqevent All Rights Reserved.</div>
    </div>

      </div>
    </div>
    <!-- END FOOTER -->

    <!-- Load javascripts at bottom, this will reduce page load time -->
    <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
    <!--[if lt IE 9]>
    <script src="public/metronic/global/plugins/respond.min.js"></script>
    <![endif]-->
    <script src="public/metronic/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="public/metronic/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="public/metronic/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="public/metronic/frontend/layout/scripts/back-to-top.js" type="text/javascript"></script>
    <script src="public/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->

</div>
    <?php $this->load->view($page_level_js['view_path'], $page_level_js);?><!-- Go to www.addthis.com/dashboard to customize your tools -->
<!-- END BODY -->
</html>
<script>
  $(document).ready(function() {

        //move he last list item before the first item. The purpose of this is if the user clicks to slide left he will be able to see the last item.
        $('#carousel_ul li:first').before($('#carousel_ul li:last'));

        //when user clicks the image for sliding right
        $('#right_scroll').click(function(){
            //get the width of the items ( i like making the jquery part dynamic, so if you change the width in the css you won't have o change it here too ) '
            var item_width = $('#carousel_ul li').outerWidth() + 10;

            //calculae the new left indent of the unordered list
            var left_indent = parseInt($('#carousel_ul').css('left')) - item_width;

            //make the sliding effect using jquery's anumate function '
            $('#carousel_ul:not(:animated)').animate({'left' : left_indent},500,function(){

                //get the first list item and put it after the last list item (that's how the infinite effects is made) '
                $('#carousel_ul li:last').after($('#carousel_ul li:first'));

                //and get the left indent to the default -217px
                $('#carousel_ul').css({'left' : '-217px'});
            });
        });

        //when user clicks the image for sliding left
        $('#left_scroll').click(function(){
            var item_width = $('#carousel_ul li').outerWidth() + 10;

            /* same as for sliding right except that it's current left indent + the item width (for the sliding right it's - item_width) */
            var left_indent = parseInt($('#carousel_ul').css('left')) + item_width;

            $('#carousel_ul:not(:animated)').animate({'left' : left_indent},500,function(){

            /* when sliding to left we are moving the last item before the first list item */
            $('#carousel_ul li:first').before($('#carousel_ul li:last'));

            /* and again, when we make that change we are setting the left indent of our unordered list to the default -217px */
            $('#carousel_ul').css({'left' : '-217px'});
            });


        });
		// logo_slider
		$('#carousel_ul_logo li:first').before($('#carousel_ul_logo li:last'));


        //when user clicks the image for sliding right
        $('#right_scrolls').click(function(){
			//get the width of the items ( i like making the jquery part dynamic, so if you change the width in the css you won't have o change it here too ) '
            var item_width = $('#carousel_ul_logo li').outerWidth() + 10;

            //calculae the new left indent of the unordered list
            var left_indent = parseInt($('#carousel_ul_logo').css('left')) - item_width;

            //make the sliding effect using jquery's anumate function '
            $('#carousel_ul_logo:not(:animated)').animate({'left' : left_indent},500,function(){

                //get the first list item and put it after the last list item (that's how the infinite effects is made) '
                $('#carousel_ul_logo li:last').after($('#carousel_ul_logo li:first'));

                //and get the left indent to the default -217px
                $('#carousel_ul_logo').css({'left' : '-221px'});
            });
        });

        //when user clicks the image for sliding left
        $('#left_scrolls').click(function(){
            var item_width = $('#carousel_ul_logo li').outerWidth() + 10;

            /* same as for sliding right except that it's current left indent + the item width (for the sliding right it's - item_width) */
            var left_indent = parseInt($('#carousel_ul_logo').css('left')) + item_width;

            $('#carousel_ul_logo:not(:animated)').animate({'left' : left_indent},500,function(){

            /* when sliding to left we are moving the last item before the first list item */
            $('#carousel_ul_logo li:first').before($('#carousel_ul_logo li:last'));

            /* and again, when we make that change we are setting the left indent of our unordered list to the default -217px */
            $('#carousel_ul_logo').css({'left' : '-221px'});
            });
        });

	setInterval(function(){
		var item_width = $('#carousel_ul li').outerWidth() + 10;

            //calculae the new left indent of the unordered list
            var left_indent = parseInt($('#carousel_ul').css('left')) - item_width;

            //make the sliding effect using jquery's anumate function '
            $('#carousel_ul:not(:animated)').animate({'left' : left_indent},500,function(){

                //get the first list item and put it after the last list item (that's how the infinite effects is made) '
                $('#carousel_ul li:last').after($('#carousel_ul li:first'));

                //and get the left indent to the default -217px
                $('#carousel_ul').css({'left' : '-217px'});
            });
	}, 200000);
	//logo_slider
	setInterval(function(){
		var item_width = $('#carousel_ul_logo li').outerWidth() + 10;

            //calculae the new left indent of the unordered list
            var left_indent = parseInt($('#carousel_ul_logo').css('left')) - item_width;

            //make the sliding effect using jquery's anumate function '
            $('#carousel_ul_logo:not(:animated)').animate({'left' : left_indent},500,function(){

                //get the first list item and put it after the last list item (that's how the infinite effects is made) '
                $('#carousel_ul_logo li:last').after($('#carousel_ul_logo li:first'));

                //and get the left indent to the default -217px
                $('#carousel_ul_logo').css({'left' : '-221px'});
            });
	}, 20000);
});

//from header
$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >100) {
        $(".pre-header").addClass("main-nav-scrolled");
    } else {
        $(".pre-header").removeClass("main-nav-scrolled");
    }
});
$("#slideshow > div:gt(0)").hide();

setInterval(function() {
  $('#slideshow > div:first')
    .fadeOut(1500)
    .next()
    .fadeIn(1500)
    .end()
    .appendTo('#slideshow');
},  4000);
$(document).ready(function(){
	var screen_h=$(window).height();
	$(".header_nav_wrap").css("height",screen_h);
	$(".mobi-toggler").click(function(){
		$(".header_nav_wrap").css("left","0px");
	});
	$("#navToggle").click(function(){
		$(".header_nav_wrap").css("left","-100%");
	});
	$(".mob_search").click(function(){
		//$(".search-box.over-header").toggle();
		//$(".mobi-toggler").toggle();

	});
	$("#closeSearch").click(function(){
		//$(".search-box.over-header").toggle();
		//$(".mobi-toggler").toggle();
	});

	$(".tagsel_btn").click(function(){
		var id=$(this).attr('id');
		$(".tagsel_drop .tag_ul").toggle();
	});

	$(".tagsel_btn2").click(function(){
		var id=$(this).attr('id');
		$(".tagsel_drop2 .tag_ul2").toggle();
	});

	$(".li_item").click(function(){
		var category_sn=$(this).attr('category_sn');
		var name=$(this).text();
		$("#search_kind").val(category_sn);
		$(".tag_selection").text(name);
	});
	$(".li_item2").click(function(){
		var id=$(this).attr('id');
		var name=$(this).attr('name')
		$(".tag_selection2").text(name);
	});
	$(".blog_sub_1").click(function(){
		$(".blog_sub").toggle();
	});

	$(".mob_mnu_li a").click(function(){
		var curr_class=$(this).attr('class');
		if(curr_class=="blog_sub_1 drop_down_menu"){
			$(this).addClass("collapsed");
		}else{
			$(this).removeClass("collapsed");
		}
	});
});
$(function() {
	$(document).click(function(e) {
		var target = e.target;
		if (!$(target).is('.tagsel_btn') && !$(target).parents().is('.tagsel_btn')){
			$('.tag_ul').hide();
		}
	});
	$(document).click(function(e) {
		var target = e.target;
		if (!$(target).is('.tagsel_btn2') && !$(target).parents().is('.tagsel_btn2')){
			$('.tag_ul2').hide();
		}
	});
});
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        this.nextElementSibling.classList.toggle("show");
  }
}
    jQuery(document).ready(function() {
      $(document).on('click', '.top-del-goods', function () {
            var button = $(this);
            var rowid = button.attr('rowid')
            var addon_from_rowid = button.attr('addon_from_rowid');
            if(!addon_from_rowid){ //如果是加購商品才抓主商品
              addon_from_rowid=rowid;
            }
            //console.log(rowid);
              var r = confirm('是否確認刪除該購物車商品?（若有加購將一併刪除）');
                if(r)
                {
                    //console.log(button.attr('ItemId'));
                    $.post('shop/cartdel/', {dtable: '<?php echo base64_encode('cart');?>',dfield:'<?php echo base64_encode('rowid');?>',did:button.attr('rowid')}, function(data){
                      if (!data.error){
                        //更新購物車popup
                        $('#top-cart-content').load('shop/top_cart');
                        $('.top-cart-info-count').text(data.total_items+' 件');
                        $('.top-cart-info-value').text('$ '+data.total);
                        if(data.gift_cash){
                          $('#show_gift_cash').text(data.gift_cash);
                          $('#gift_cash').val(data.gift_cash);
                        }
                        //if($(".tr_"+addon_from_rowid+' .checkboxrowid').is(":checked")){ //if有勾選
                          //console.log(rowid);
                          checkoutupdate(false,rowid);
                        //}
                        //button.parents().filter('li').remove();
                        $('.addon_from_rowid'+button.attr('rowid')).remove();
                        $(".tr_"+rowid).remove();
                      }
                    },'json');
                }
        });
  });
</script>
<script>
$(document).ready(function() {
  $(".owl-probox1 .owl-carousel").owlCarousel({
	  margin:10,
    loop:true,
    autoWidth:true,
    items:5,
	  dots:false,
	  center: true,
  });
  $(".owl-probox1 #right_scrolls").click(function(){
     $(".owl-probox1 .owl-carousel").trigger('next.owl.carousel');
  })
  $(".owl-probox1 #left_scrolls").click(function(){
     $(".owl-probox1 .owl-carousel").trigger('prev.owl.carousel');
  })

 // var owl = $("#probox2");
  $(".owl-probox2 .owl-carousel").owlCarousel({
    margin:10,
    loop:true,
    autoWidth:true,
    items:5,
	dots:false,
	center: true,

  });
  $(".owl-probox2 #right_scrolls").click(function(){
    $(".owl-probox2 .owl-carousel").trigger('next.owl.carousel');
  })
  $(".owl-probox2 #left_scrolls").click(function(){
    $(".owl-probox2 .owl-carousel").trigger('prev.owl.carousel');
  })

  // var owl = $("#probox3");
  $(".owl-probox3 .owl-carousel").owlCarousel({
	margin:10,
    loop:true,
    autoWidth:true,
    items:4,
	autoplay:true,
	autoplayTimeout:5000,
	dots:false,
	center: true,

  });
  $(".owl-probox3 #right_scrolls").click(function(){
    $(".owl-probox3 .owl-carousel").trigger('next.owl.carousel');
  })
  $(".owl-probox3 #left_scrolls").click(function(){
    $(".owl-probox3 .owl-carousel").trigger('prev.owl.carousel');
  })

    // var owl = $("#probox4");
  $(".owl-probox4 .owl-carousel").owlCarousel({
    margin:10,
    loop:true,
    autoWidth:true,
    items:4,
  	autoplay:true,
  	autoplayTimeout:5100,
  	dots:true,
  	center: true,
  });
  $(".owl-probox4 #right_scrolls").click(function(){
    $(".owl-probox4 .owl-carousel").trigger('next.owl.carousel');
  })
  $(".owl-probox4 #left_scrolls").click(function(){
    $(".owl-probox4 .owl-carousel").trigger('prev.owl.carousel');
  })

    // var owl = $("#probox5");
  $(".owl-probox5 .owl-carousel").owlCarousel({
    margin:16,
    loop:true,
    autoWidth:true,
    items:5,
	autoplay:true,
	autoplayTimeout:5200,
	dots:false,
	center: true,

  });
  $(".owl-probox5 #right_scrolls").click(function(){
    $(".owl-probox5 .owl-carousel").trigger('next.owl.carousel');
  })
  $(".owl-probox5 #left_scrolls").click(function(){
    $(".owl-probox5 .owl-carousel").trigger('prev.owl.carousel');
  })
});
</script>
