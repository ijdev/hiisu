<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- BEGIN GLOBAL MANDATORY STYLES
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
-->
<link href="public/metronic/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="public/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="public/metronic/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="public/metronic/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="public/metronic/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css">
<link href="public/metronic/global/plugins/morris/morris.css" rel="stylesheet" type="text/css">
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="public/metronic/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
<link href="public/metronic/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css">
<link href="public/metronic/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="public/metronic/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="public/metronic/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="public/metronic/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">

<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico">
<script>
	//onclick="return deletechecked();"
    function deletechecked()
    {
        if(confirm("確定要刪除資料！"))
        {
            return true;
        }
        else
        {
            return false;
        }
   }
   function sendmailchecked()
    {
        if(confirm("確定要發送郵件！"))
        {
            return true;
        }
        else
        {
            return false;
        }
   }

</script>
<style>
body{
    font-family: '微軟正黑體';
}
h1, h2, h3, h4, h5, h6{
    font-family: '微軟正黑體';
}
</style>
</head>
<!-- END HEAD -->