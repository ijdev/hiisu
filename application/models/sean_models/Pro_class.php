<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
     *
     * Pro_class_model: contains generic functions, used in several controllers
     *
     */
 
class Pro_class extends CI_Model 
{

		public function __construct() {
			parent::__construct();
		  /* load 資料庫 */
		  $this->load->database();
		  
			$_c_member_config="proclass_config";
			$_c_member_data="member_data";
			//會員資料庫參數 抓config/member_config.php 裡的member_data 陣列
			$member_item=$this->Sean_general->general_data($_c_member_config,$_c_member_data);
		
		}

    
		public function get_p_type($_type)
		{	
				
				$_one="";
				$action_one="SELECT * FROM pro_class WHERE p_type = ".$_type."  and p_active = 1  ORDER BY p_sort ASC";
				$query = $this->db->query($action_one);

				if ($query->num_fields() > 0)
				{
				   	$_one=$query->result();
				} 
				$query->free_result();
				return $_one;
		} 
		
		public function get_pid($_type)
		{	
				
				$_one="";
				$action_one="SELECT * FROM pro_class WHERE pid = '".$_type."'  and p_active = 1 ";
				$query = $this->db->query($action_one);

				if ($query->num_fields() > 0)
				{
				   	$_one=$query->result();
				} 
				$query->free_result();
				return $_one;
		} 
		
		public function get_ptype_pparent($_type,$_parent)
		{	
				
				$_one="";
				$action_one="SELECT * FROM pro_class WHERE p_type = '".$_type."'  and p_parent='".$_parent."' and p_active = 1  ORDER BY p_sort ASC";
				$query = $this->db->query($action_one);

				if ($query->num_fields() > 0)
				{
				   	$_one=$query->result();
				} 
				$query->free_result();
				return $_one;
		} 
	
}

/* End of file Pro_class_model.php */
/* Location: ./application/models/Pro_class_model.php */
