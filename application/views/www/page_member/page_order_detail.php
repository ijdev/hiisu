<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="container">

<!-- Page level plugin styles START -->
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<style id="css" type="text/css">
  .order-table{
    border: 0px;
    width: 100%;
    border-spacing: 2px;
  }
    .order-table tr{
    }
      .order-table tr th{
        border-bottom: 2px solid #EFEFEF;
        padding:9px 0 10px 0;
        font-weight: bold;
      }
      .order-table tr td{
        border-bottom: 1px solid #EFEFEF;
        padding:9px 0 10px 0;
      }
</style>
<!-- Page level plugin styles END -->
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <li><a href="/member/memberOrderList">訂單查詢</a></li>
    <li class="active">訂單明細</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar col-md-2 col-sm-3">
    <div class="desktop_view_sidebar">
		<ul class="list-group margin-bottom-25 sidebar-menu">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
	<div class="mobile_view_sidebar">
		<div class="side-nav-head border_title_bottom">
			<button class="fa fa-bars collapsed" data-toggle="collapse" data-target="#mob_sidebar_drop_down"></button>
			<h4 class="bfont">會員中心<!--會員中心--></h4>
		</div>
		<ul class="list-group margin-bottom-25 sidebar-menu collapse" id="mob_sidebar_drop_down">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="col-md-10 col-sm-9">
    <div class="product-page">
      <h1>
        <i class="fa fa-list-alt"></i> 訂單明細
        <div class="actions pull-right">
          <a href="member/memberOrderList" class="btn btn-sm">
            <i class="fa fa-list-alt"></i> 回訂單列表
          </a>
        </div>
      </h1>
      <div class="alert alert-success">
        <div class="row">
          <div class="col-md-5 col-sm-5">
            <strong>訂單編號： </strong>201601123
          </div>
          <div class="col-md-5 col-sm-5">
            <strong>交易編號 : </strong>#<?php echo $order['order_sn'];?>
          </div>
          <div class="col-md-2 col-sm-2" style="text-align:right;"> 
            <button type="button" class="print btn btn-default btn-sm">列印</button>
          </div>
        </div>
      </div>
      <div class="row" id="order-detail">
        <div class="col-md-12 col-sm-12">
          <!-- 付款狀態 -->
          <div class="portlet box blue-hoki">
            <div class="portlet-title">
              <div class="caption">
                <i class="fa fa-search"></i>付款狀態
              </div>
            </div>
            <div class="portlet-body">
              <table class="order-table">
                <tr>
                  <td class="col-md-3">
                    <strong>付款方式</strong>
                  </td>
                  <td class="col-md-9">
                    <?php echo $order['payment_method_name'];?>
                  </td>
                </tr>
                <tr>
                  <td class="col-md-6">
                    <strong>付款狀態</strong>
                  </td>
                  <td class="col-md-6">
                      <?php if($order['payment_done_date']=='2' && $order['payment_done_date']!='0000-00-00 00:00:00'){
                    	 echo '已付款'.' ('.$order['payment_done_date'].')';
                    	}else{
                    	 echo $order['payment_status_name'];
                    	}?>
                  </td>
                </tr>
                <tr>
                  <td class="col-md-3">
                    <strong>應付金額</strong>
                  </td>
                  <td class="col-md-9">
                    <?php echo $order['total_order_amount'];?>
                  </td>
                </tr>                
              </table>
            </div>
          </div>
          <!-- 付款狀態 -->
<?php if($order['sub_order']){ foreach($order['sub_order'] as $key=> $Item){ 
		if($Item['channel_name']=='婚禮網站'){?>
                <div class="row">
                  <div class="col-md-12 col-sm-12">
                    <div class="portlet red-sunglo box">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs"></i>購物明細 - <?php echo $Item['channel_name']?> &nbsp; 訂單編號： <?php echo $Item['sub_order_sn']?>
                        </div>
                        <div class="actions">
                          <!--
                          <a href="javascript:;" class="btn btn-default btn-sm" data-toggle="modal" data-target="#pproductModal">
                            <i class="fa fa-plus"></i> 新增 
                          </a>
                          -->
                        </div>
                      </div>
                      <div class="portlet-body">
                        <div class="table-responsive">
                          <table class="table table-hover table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>商品名稱</th>
                                <th>規格</th>
                                <th>單價</th>
                                <th>數量</th>
                                <th>加購</th>
                                <th>折扣</th>
																<?php echo ($Item['order_item'][0]['upgrade_discount_amount'])?'<th>升級折價</th>':'';?>
                                <th>電子禮金</th>
                                <th>小計</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php if($Item['order_item']){ foreach($Item['order_item'] as $order_item){?>
                                <tr>
							                    <td><a href="/shop/shopItem/<?php echo $order_item['product_sn'];?>" target="_blank"><?php echo $order_item['product_name'];?></a></td>
							                    <td><?php echo ($order_item['product_package_name'])? $order_item['product_package_name']:'單一規格';?><?php echo ($order_item['upgrade_discount_amount'])? '(升級)':'';?></td>
							                    <td>$<?php echo $order_item['sales_price'];?></td>
							                    <td><?php echo $order_item['buy_amount'];?></td>
							                    <td><?php echo $order_item['addon_flag']==1? '<i class="fa fa-check-square-o"></i>':'';?></td>
							                    <td><?php echo $order_item['coupon_code_cdiscount_amount'];?></td>
																<?php echo ($order_item['upgrade_discount_amount'])?'<td>'.$order_item['upgrade_discount_amount'].'</td>':'';?>
							                    <td><?php echo $order_item['gift_cash_discount_amount'];?></td>
							                    <td><?php echo $order_item['actual_sales_amount'];?></td>
                                </tr>
                              <?php }}?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <!-- End of portlet-body -->
                    </div>
                    <!-- End of portlet -->
                  </div>
                </div>
<?php }}}?>
          <!-- 購物明細 -->
<?php if($order['sub_order']){ foreach($order['sub_order'] as $key=> $Item){ 
		if($Item['channel_name']=='結婚商店'){?>
          <div class="portlet box blue-hoki">
            <div class="portlet-title">
              <div class="caption">
                <i class="fa fa-search"></i>購物明細 - <?php echo $Item['channel_name']?> &nbsp; 訂單編號： <?php echo $Item['sub_order_sn']?>
              </div>
            </div>
            <div class="portlet-body">
              <table class="order-table">
                <tr>
                  <th>商品名稱</th>
                  <th>規格</th>
                  <th>單價</th>
                  <th>數量</th>
                  <th>折扣</th>
                  <th>電子禮金</th>
                  <th>小計</th>
                </tr>
                <?php if($Item['order_item']){ 
                			foreach($Item['order_item'] as $key2=> $order_item){?>
                  <tr>
                    <td><?php echo $order_item['product_name'];?></td>
							      <td><?php echo ($order_item['product_package_name'])? $order_item['product_package_name']:'單一規格';?></td>
                    <td>$<?php echo $order_item['sales_price'];?></td>
                    <td><?php echo $order_item['buy_amount'];?></td>
                    <td><?php echo $order_item['coupon_code_cdiscount_amount'];?></td>
                    <td><?php echo $order_item['gift_cash_discount_amount'];?></td>
                    <td><?php echo $order_item['actual_sales_amount'];?></td>
                  </tr>
                <?php }}?>
              </table>
            </div>
          </div>
<?php }}}?>
          <!-- 購物明細 -->
          <!-- 配送 -->
          <div class="portlet box blue-hoki">
            <div class="portlet-title">
              <div class="caption">
                <i class="fa fa-search"></i>配送
              </div>
            </div>
            <div class="portlet-body">
              <table class="order-table">
                <tr>
                  <td>
                    <strong>配送方式</strong>
                  </td>
                  <td><?php echo $order['sub_order'][0]['delivery_method_name'];?></td>
                </tr>
                <tr>
                  <td>
                    <strong>出貨狀態</strong>
                  </td>
                  <td><?php echo $order['sub_order'][0]['delivery_status_name'];?></td>
                </tr>
              </table>
            </div>
          </div>
          <!-- 配送 -->
          <div class="portlet box blue-hoki">
            <div class="portlet-title">
              <div class="caption">
                <i class="fa fa-search"></i>收件人資料
              </div>
            </div>
            <div class="portlet-body">
              <table class="order-table">
                <tr>
                  <td  class="col-md-6"><strong>姓名</strong></td>
                  <td  class="col-md-6"><?php echo $order['receiver_last_name'];?></td>
                </tr>
                <tr>
                  <td><strong>聯絡電話</strong></td>
                  <td><?php echo $order['receiver_cell'];?> &nbsp; <?php echo $order['receiver_tel_area_code'];?>-<?php echo $order['receiver_tel'];?><?php echo ($order['receiver_tel_ext'])? '#'.$order['receiver_tel_ext']:'';?></td>
                </tr>
                <tr>
                  <td><strong>地址</strong></td>
                  <td><?php echo $order['receiver_zipcode'];?> <?php echo $order['receiver_addr_city'];?><?php echo $order['receiver_addr_town'];?><?php echo $order['receiver_addr1'];?></td>
                </tr>
              </table>
            </div>
          </div>
          <!-- 配送 -->
        </div>
      </div>
    </div>
  </div>
  <!-- END CONTENT -->
</div>
</div>
<!-- END SIDEBAR & CONTENT -->