<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Page 主架構
*/
?>
<div class="main-container with-padding">

    <div class="wrapper">

        <div class="inner-wrapper">

            <div class="contact-2">

                <div class="content-inner clearfix">

                    <div class="row">
                        <div class="col full">
                            <h4>CONTACT US</h4>
                        </div>
                    </div>

                    <div class="divider clear" style="height:10px;"></div>

                    <div class="row">

                        <div class="info-box col one-third">
                            <p class="desc">PHONE</p>
                            <p class="info"><?php echo $dealer['office_tel']?></p>
                        </div>
                        <div class="info-box col one-third">
                            <p class="desc">ADDRESS</p>
                            <p class="info"><?php echo $dealer['addr1']?></p>
                        </div>
                    </div>

                    <div class="divider clear" style="height:20px;"></div>

                    <div class="row">

                        <div class="two-third col">

                            <form id="form1" method="post">
                                <input type="text" name="contact[user_name]" placeholder="姓名">
                                <input type="email" name="contact[user_email]" placeholder="EMAIL">
                                <input type="text" name="contact[message_subject]" placeholder="主題">
                                <input type="text" name="contact[website_id]" placeholder="詢問的作品編號，例：#12, #13">
                                <textarea name="contact[message_content]" placeholder="訊息內容"></textarea>
                                <input id="submit1" type="button" name="submit" value="送出">
                                <input type="hidden" name="contact[member_sn]" value="<?php echo $member_sn;?>">
                                <p class="message-info">Message Info.</p>
                            </form>

                        </div>
                        <div class="one-third col">


                            <p><?php echo $dealer['page_contactus_content']?></p>

                            <div class="divider clear" style="height:10px;"></div>

                            <div class="info-box">
                                <p class="desc">WORKING HOURS</p>
                                <p class="info"><?php echo $dealer['dealer_opening_time']?></p>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="map" data-address="London, United Kingdom">
                    <div class="open"><p>觀看地圖</p></div>
                    <div class="map-content" id="mapView"></div>
                </div>

            </div>


        </div>

    </div>


</div>