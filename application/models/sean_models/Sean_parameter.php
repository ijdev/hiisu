<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
     *
     * 
	 // init
$foo = new Sean_parameter(array("hello" => "world"));
$foo->__set($property, $value);

// set: this calls __set()
$foo->invader = "zim";

// get: this calls __get()
$foo->invader;       
     */
 
class Sean_parameter extends CI_Model 
{

		private $_data;
		
		public function __construct(Array $properties=array())
		{
			  $this->_data = $properties;
		}
    
		// magic methods!
		public function __set($property, $value)
		{
		  return $this->_data[$property] = $value;
		}
		
		public function __get($property)
		{
		  return array_key_exists($property, $this->_data)? $this->_data[$property]: $this->$property;
		}
	
}

/* End of file Sean_parameter.php */
/* Location: ./application/models/Sean_parameter.php */
