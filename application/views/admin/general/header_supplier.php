<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- BEGIN HEADER -->
<div class="page-header">
  <!-- BEGIN HEADER TOP -->
  <div class="page-header-top">
    <div class="container">
      <!-- BEGIN LOGO -->
      <div class="page-logo">
        <a href="admin/main"><img src="public/img/ij-logo.png" alt="logo" class="logo-default"></a>
      </div>
        <div style="font-size:150%;font-weight:bold;font-family:'微軟正黑體';display: inline;top: 20px;position: relative;">供應商管理系統 <?php echo $this->session->userdata['member_data']['supplier_name'];?></div>
      <!-- END LOGO -->
      <!-- BEGIN RESPONSIVE MENU TOGGLER -->
      <a href="javascript:;" class="menu-toggler"></a>
      <!-- END RESPONSIVE MENU TOGGLER -->
      <!-- BEGIN TOP NAVIGATION MENU -->
      <div class="top-menu">
        <ul class="nav navbar-nav pull-right">
          <!-- BEGIN USER LOGIN DROPDOWN -->
          <li class="dropdown dropdown-user dropdown-dark">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <img alt="" class="img-circle" style="border-radius: unset !important;" src="/public/metronic/admin/layout3/img/avatarb.jpg">
            <span class="username username-hide-mobile"><?php echo $this->session->userdata['member_data']['last_name'];?></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-default">
              <li>
                <a href="admin/CRM/notification">
                <i class="icon-envelope-open"></i> 全站通知 <span class="badge badge-danger"> <?php echo $this->page_data['unreadmessage'];?> </span>
                </a>
              </li>
<?if($this->session->userdata['member_data']['role_count'] >1){?>
              <li>
                <a href="admin/main/role_select/">
                <i class="icon-envelope-open"></i> 權限角色 </span>
                </a>
              </li>
<?}?>
              <li class="divider"></li>
             <?php
             if($this->session->userdata('member_login')== true)
             {
             	 echo  '
             	 <li>
                <a href="admin/main/managerSignOut">
                <i class="icon-logout"></i> Log Out</a>
              </li>
             	 ';
             }else{
             		echo '
             		 <li>
                <a href="admin/main/managerSignIn">
                <i class="icon-key"></i> Log In</a>
              </li>
             		';
             }
             ?>


            </ul>
          </li>
          <!-- END USER LOGIN DROPDOWN -->
        </ul>
      </div>
      <!-- END TOP NAVIGATION MENU -->
    </div>
  </div>
  <!-- END HEADER TOP -->
  <!-- BEGIN HEADER MENU -->
  <div class="page-header-menu">
    <div class="container">
      <div class="hor-menu ">
        <ul class="nav navbar-nav">
          <li class="active">
            <a href="admin/main">控制台</a>
          </li>
           <li class="menu-dropdown classic-menu-dropdown">
            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
              商品管理 <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
              <li>
                <a href="admin/product/productTable/" class="iconify">
                <i class="icon-handbag"></i> 商品列表 </a>
              </li>
              <li>
                <a href="admin/product/inventory" class="iconify">
                <i class="fa fa-dashboard"></i> 庫存管理 </a>
              </li>
            </ul>
          </li>
          <li>
            <a href="admin/order/orderTable">
              訂單管理
            </a>
          </li>
          <!--li>
            <a href="admin/content/contentBlog">
              內容管理
            </a>
          </li-->
          <li>
            <a href="admin/CRM/ticketTable">
              客服中心
            </a>
             </li>
		          <li>
                <a href="admin/supplier/supplierAccounting" class="iconify">
		            帳務管理</a>
		          </li>
             <li class="menu-dropdown classic-menu-dropdown">
              <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
                系統設定 <i class="fa fa-angle-down"></i>
              </a>
              <ul class="dropdown-menu pull-left">
                <li>
                  <a href="/admin/supplier/supplierTable/edit/<?php echo $this->session->userdata('member_data')['associated_supplier_sn'];?>">
                  <i class="icon-handbag"></i> 帳戶設定 </a>
                </li>
                <li>
                  <a href="/admin/supplier/Shipping" class="iconify">
                  <i class="fa fa-dashboard"></i> 配送與運費設定 </a>
                </li>
              </ul>
            </li>
		          <li>
		            <a title="ＦＡＱ" href="/admin/content/contentFaq">ＦＡＱ</a>
		          </li>

        </ul>
      </div>
      <!-- END MEGA MENU -->
    </div>
  </div>
  <!-- END HEADER MENU -->
</div>
<!-- END HEADER -->