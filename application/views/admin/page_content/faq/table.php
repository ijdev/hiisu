<?php defined('BASEPATH') OR exit('No direct script access allowed');
//var_dump($content_types);?>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>FAQ內容 <small>FAQ列表</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">FAQ列表</span>
            </div>
<?php if($this->session->userdata('member_role_system_rules')[0]['role_name']!='經銷商管理員' && $this->session->userdata('member_role_system_rules')[0]['role_name']!='供應商管理員'){?>
            <div class="actions btn-set">
              <a href="admin/content/contentFaqItem" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
<?php }?>
          </div>
      		<form class="form-horizontal" role="form" method="get">
          <div class="table-actions-wrapper pull-right">
            <span>
            </span>
            <?php echo $Store;?>
            <select name="content_type_sn" id="category" class="table-group-action-input form-control input-inline input-small input-sm">
              <option value="">內容分類</option>
<?php if($content_types){ foreach($content_types as $_Item){ if($_Item['post_channel_name_set']==$post_channel_name){
	$ifselected=($content_type_sn==$_Item['content_type_sn']) ?'selected':'';
	echo '<option '.$ifselected.' value="'.$_Item['content_type_sn'].'">'.$_Item['content_title'].'</option>';
}}}?>
            </select>
<?php if($this->session->userdata('member_role_system_rules')[0]['role_name']!='經銷商管理員' && $this->session->userdata('member_role_system_rules')[0]['role_name']!='供應商管理員'){?>
            <select name="post_flag" id="post_flag" class="table-group-action-input form-control input-inline input-small input-sm">
              <option value="">是否發佈</option>
              <option value="1" <?php echo($post_flag=='1') ?'selected':'';?>>已發佈</option>
              <option value="2" <?php echo($post_flag=='2') ?'selected':'';?>>未發佈</option>
              <option value="9" <?php echo($post_flag=='9') ?'selected':'';?>>已刪除</option>
            </select>
<?php }?>
            <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button> &nbsp; 
            <button id="reset_form" onclick="this.form.flg.value='reset';" class="btn btn-sm purple"><i class="fa"></i> 清除</button>
          </div>
          <input name="flg" type="hidden" value="">
        </form>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>Question</th>
                  <th>內容分類</th>
<?php if($this->session->userdata('member_role_system_rules')[0]['role_name']=='經銷商管理員' || $this->session->userdata('member_role_system_rules')[0]['role_name']=='供應商管理員'){?>
                  <th>Answer</th>
<?php }else{?>
                  <th>頻道</th>
                  <th width="200">商品分類</th>
                  <th width="100">排序</th>
                  <th width="80">是否發佈</th>
                  <th width="120">功能</th>
<?php }?>
                </tr>
              </thead>
              <tbody>
								<?php if($Items){ foreach($Items as $_Item){?>
                  <tr class="odd gradeX">
                    <td><?php echo $_Item['faq_titile'];?></td>
                    <td>
                    	<?php echo $_Item['content_title'];?>
                    </td>
<?php if($this->session->userdata('member_role_system_rules')[0]['role_name']=='經銷商管理員' || $this->session->userdata('member_role_system_rules')[0]['role_name']=='供應商管理員'){?>
                    <td>
											<?php echo $_Item['faq_description'];?>
                    </td>
<?php }else{?>
                    <td><?php echo $_Item['post_channel_name_set'];?></td>
                    <td><?php echo $_Item['post_category_sn_set'];?></td>
                    <td class="center"><?php echo $_Item['sort_order'];?></td>
                    <td class="center">
                      <?php if($_Item['post_flag']=='1'):?>
                        <i class="fa fa-check"></i>
                      <?php else:?>
                        <i class="fa fa-times"></i>
                      <?php endif;?>
                    </td>
                    <td class="center">
                      <a href="admin/content/contentFaqItem/<?php echo $_Item['faq_sn'];?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
    <?php if(stripos($this->ijw->check_permission(1,$this->router->fetch_class(),$this->router->fetch_method()),'del')){?>
                      <a href="javascript:void(0);" class="deleteItem" ItemId="<?php echo $_Item['faq_sn'];?>"><i class="fa fa-trash-o"></i>刪除</a>
    <?php }?>
                    </td>
<?php }?>
                  </tr>
                <?php }}?>
              </tbody>
            </table>

            <!--div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                  <ul class="pagination" style="visibility: visible;">
                    <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                    <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li class="active"><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                    <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                  </ul>
                </div>
              </div>
            </div-->
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->