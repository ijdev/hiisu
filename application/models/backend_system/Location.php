<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Location extends CI_Model {

    public function __construct() {
    parent::__construct();
		$this->load->library('session');
		$this->load->model('sean_models/Sean_db_toolss');
		

    }
    
   public function action_insert($page_data) {
		
			$_result="";
			$_data="";
			
			//basic insert
			$_data["create_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			$_data["create_date"]=date("Y-m-d H:i:s");
			//basic update and insert
			$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			$_data["last_time_update"]=date("Y-m-d H:i:s");
		
		
			foreach($page_data["_table_field"] as $key => $value)
			{
					if($this->input->get_post($value) || strlen($this->input->get_post($value)) > 0)
					$_data[$value]=$this->input->get_post($value);
			}
			
			
		$_result=$this->Sean_db_toolss->db_insert_record($_data,$page_data);
		
		return $_result;
		
	}
	
		public function action_delete( $_where,$page_data) {
		
		$_result="";
		
		$_result=$this->Sean_db_toolss->db_delete_record($_where,$page_data);
		
		return $_result;
		
	}
	
	public function action_update($page_data) 
	{
		
			$_result="";
					
			foreach($page_data["_table_field"] as $key => $value)
			{
					if($this->input->get_post($value) || strlen($this->input->get_post($value)) > 0)
					$_data[$value]=$this->input->get_post($value);
					
			}
			//basic update and insert
			$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			$_data["last_time_update"]=date("Y-m-d H:i:s");
		
			$_where=" where ".$page_data["_table_key"]."='".$page_data["_id"]."' ";
		$_result=$this->Sean_db_toolss->db_update_record($_data,$_where,$page_data);
		
		return $_result;
		
	}
	
	
	
}
