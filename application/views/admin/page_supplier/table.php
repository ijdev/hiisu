<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>供應商 <small>廠商列表</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        會員管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        供應商
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">供應商列表</span>
            </div>
            <div class="actions btn-set">
              <a href="<?php echo $_action_link;?>" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <div class="portlet-body">
          	<?php $this->load->view('sean_admin/general/flash_error');?>
            <div class="note note-warning">
              <form action >
              <div class="table-actions-wrapper pull-right">
                <span>
                </span>
                <!--input type="text" class="pagination-panel-input form-control input-inline input-sm" size="8" style="margin: 0 5px;" placeholder="關鍵字"-->
                <select class="table-group-action-input form-control input-inline" id="biz_domain" style="font-size:11px;">
                  <option value="">產業屬性</option>
                 <?php
                 foreach($_biz_domain_list as $key => $value)
                 {
                 		echo '<option value="'.$value->domain_name.'">'.$value->domain_name.'</option>';
                 }
                 ?>


                </select>
                <!--button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button-->
              </div>


              <div style="clear:both;"></div>
            </div>
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>廠商名稱</th>
                  <th>品牌名稱</th>
                  <th>產業屬性</th>
                  <th>縣市</th>
                  <th>窗口</th>
                  <th>聯絡電話</th>
                  <th>電子信箱</th>
                  <th>合作模式</th>
                  <th>商品數</th>
                  <th>狀態</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
              	<?php
                $_member_status_ct=$this->config->item("member_status_ct");
								$_member_level_type_ct=$this->config->item("member_level_type_ct");
								//$_city_ct=$this->config->item("city_ct");

                if(is_array($_result01))
                	{
                 			foreach($_result01 as $key => $value)
                    		{
                    			if($value->supplier_status == 1)
                    				{
                    					$_active = '<i class="fa fa-check"></i>';
                    				}
                    			else
                    				{
                    					$_active = '<i class="fa fa-times"></i>';
                    				}
												  //$_town_ct_init="town_ct_".$value->addr_city_code;
												  //$_town_ct=$this->config->item($_town_ct_init);
          //$_where=" where associated_member_sn='".$value->supplier_sn."'  ";
					//$_member_domain_relation=$this->Sean_db_tools->db_get_max_record("ij_member.email","ij_member_domain_relation left join ij_member_domain_relation.member_sn=ij_member.member_sn",$_where);




                  ?>
                  <tr class="odd gradeX">
                  <!--<td><?php echo $value->supplier_sn;?></td>-->
                    <td><?php echo $value->supplier_name;?></td>
                    <td><?php echo $value->brand;?></td>
                    <td><?php echo $value->domaintype;?></td>
                    <td><?php echo $value->city_name;?></td>
                    <td><?php echo $value->contact_name;?></td>
                    <td><?php echo $value->cell;?></td>
                    <td width="8%"><?php echo $value->email;?></td>
                    <td width="8%"><?php echo $value->cooperate_mode_type_name;?></td>
                    <td><?php echo $value->product_count;?></td>
                    <td class="center"><?php echo $_active;?></td>
                    <td class="center">
                      <a href="<?php echo $init_control;?>supplier/supplierTable/edit/<?php echo $value->supplier_sn;?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                      <a target="_blank" href="admin/supplier/supplierAccounting?supplier_name=<?php echo $value->supplier_name;?>"><i class="fa fa-cogs"></i>帳務管理</a>&nbsp;
                      <a target="_blank" href="admin/supplier/Shipping/<?php echo $value->supplier_name;?>"><i class="fa fa-cogs"></i>運費設定</a>&nbsp;
                      <a href="<?php echo $init_control;?>supplier/supplierTable/delete/<?php echo $value->supplier_sn;?>"><i class="fa fa-trash-o"></i>停用</a>
                    </td>
                  </tr>
              <?php } } ?>
              </tbody>
            </table>
            <!-- page nav -->
            <?php
						//echo $_pagination;
						?>
            <!-- end of page nav -->
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->