<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dealerlevel extends CI_Model {
    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('sean_models/Sean_db_tools');

		/* Begin 修改  */
		$this->_ct_name="會員管理";// 功能名稱
		$this->_url="Member/dealerLevel/";//列表功能url
		$this->_url_item="Member/dealerLevel_item/";//處理動作 url
		$this->_table="ij_dealer_level_config";//table name
		$this->_id="dealer_level_sn";//table key
		$this->_table_field=array(
					 "dealer_level_name" => "名稱",
					 "dealer_page_profile_config" => "經銷商專頁功能",
					 "dealer_backend_config" => "經銷商後台功能",
					 //"sort_order" => "排序", 2016-1-20 bridina說不用排序 wdj
					 "dealer_bonus" => "分潤比例",
					 "newmember" => "新會員獎金",
					 "status" => "是否啟用",
					 "description" => "說明控人員",
				);


		/* End 修改  */

    }

    //取得列表內容
    public function table_value_list( $_where,$_id = 0)
	{
		$_table_key=$this->_id;
	    $page_data['_body_result']="";
		//判斷是否有帶入特殊查詢
		if(strlen($_where)==0)
		{
			$_where="  order by apply_date asc";

		}
		// 取得列表
		$_v=" ".$this->_table.".*, concat(first_name,'',last_name) as ijtmp_name ";
		$_result01=$this->Sean_db_tools->db_get_max_record($_v,$this->_table,$_where);

		/* 列表標頭名稱 */
		$page_data['_table_thead_name']=array("姓名","Email","縣市","鄉鎮市區","住址","電話","結婚日期","關聯廠商","帳號狀態","功能");
		$page_data['_table_thead_field']=array("ijtmp_name","email","addr_state","addr_city","addr1","tel","wedding_date","member_level_type","member_status");


		/* 類表內容 */
		if(is_array($_result01))
		{
                 		$_i=0;
                 		foreach($_result01 as $key => $value)
                    	{


			                    $_body_result='';
						/* Begin 修改  */
			                    $i_key=0;
								foreach(	$page_data['_table_thead_field'] as $key => $_value)
								{
									$page_data['_body_result'][$_i][$i_key]=array("_value"=>$value->$_value,"_class"=>"");
			                    	$i_key++;
								}


						/* End 修改  */
									$page_data['_body_result'][$_i][$i_key]=array("_value"=>'
			                    		 		<a href="'.$this->init_control.$this->_url_item.'edit/'.$value->$_table_key.'"><i class="fa fa-edit"></i>編輯</a>&nbsp;&nbsp;
						                      <a href="'.$this->init_control.$this->_url_item.'delete/'.$value->$_table_key.'"><i class="fa fa-circle-o-notch"></i>重發驗證信</a>&nbsp;&nbsp;
											   <a href="'.$this->init_control.$this->_url_item.'delete/'.$value->$_table_key.'"><i class="fa fa-lightbulb-o"></i>忘記密碼&nbsp;&nbsp;
						                  	',"_class"=>'class="center"');
                				$_i++;


                			}

    				}
			return $page_data;

    }

	public function action_edit( $_where = "") {

		$_result="";
		$_result=$this->Sean_db_tools->db_get_max_record("*",$this->_table,$_where);

		return $_result;

	}

			public function action_insert() {

			$_result="";

			foreach($this->_table_field as $key => $value)
			{
					if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
						$_value=$this->input->get_post($key);
						if(is_array($_value)) $_value=implode(":",$_value);
						//echo is_array($_value);
						$_data[$key]=$_value;
			}


			//basic insert
			$_data["create_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			$_data["create_date"]=date("Y-m-d H:i:s");
			//basic update and insert
			$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			$_data["last_time_update"]=date("Y-m-d H:i:s");



		$_result=$this->Sean_db_tools->db_insert_record($_data,$this->_table);

		return $_result;

	}

	public function action_update($_where,$_key_id,$_key) {

			$_result="";

			foreach($this->_table_field as $key => $value)
			{
					if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0){
						$_value=$this->input->get_post($key);
						if(is_array($_value)) $_value=implode(":",$_value);
						//echo is_array($_value);
						$_data[$key]=$_value;
					}

			}
			//var_dump($_data);
	  	//exit();
			$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			$_data["last_time_update"]=date("Y-m-d H:i:s");


		$_result=$this->Sean_db_tools->db_update_record($_data,$this->_table,$_where,$_key_id,$_key);

		return $_result;

	}

	public function action_delete( $_where) {

		$_result="";

		$_result=$this->Sean_db_tools->db_delete_record($this->_table,$_where);

		return $_result;

	}

	//取得新排序的值
	/*public function get_next_sort($_id=0 ) {

		$_result="0";

		$_where=" where ".$this->_id." > 0 order by last_time_update desc ";
		$_result=$this->Sean_db_tools->db_get_one_record("last_time_update",$this->_table,$_where);

		if(is_array($_result["sort_order"]) )
		$_result=1;
		else
		$_result=$_result["sort_order"]+1;

		return $_result;

	}*/

	//取得定義參數值
	public function get_define_value($_id)
	{
		if(isset($this->$_id))
		return $this->$_id;
		else
		return "";
	}

}
