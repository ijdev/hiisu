<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>


<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品管理 <small>庫存管理</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/admin">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
         <a href="admin/product/inventory">庫存管理</a>
         <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         庫存管理編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">庫存管理編輯</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/product/inventory" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post">
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">商品名稱</label>
                <div class="col-md-10">
                  <p class="form-control-static"><?php echo $Item['product_name'];?></p>
                </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">商品編號</label>
                <div class="col-md-10">
                  <p class="form-control-static"><?php echo $Item['product_sn'];?></p>
                </div>
              </div>
            <?php if(@$Item['unispec_flag']!='1'){?>
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">多重規格</label>
                <div class="col-md-10">
                  <div class="radio-list">
                  	<?php if(@$mutispec){foreach(@$mutispec as $ms){?>
                    	<label class="radio-inline"><input type="radio" class="get_current_amount" name="Item[associated_mutispec_stock_sn]" value="<?php echo $ms['mutispec_stock_sn'];?>" <?php echo (@$Item['associated_mutispec_stock_sn']==$ms['mutispec_stock_sn'])?'checked':'';?> required ><?php echo $ms['color_name'];?></label>
                    <?php }}?>
                  </div>
                </div>
              </div>              
           <?php }?>
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">現有庫存</label>
                <div class="col-md-10">
                  <p class="form-control-static" id="current_amount"><?php echo (@$Item['current_amount'])?@$Item['current_amount']:'0';?></p>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-md-2 control-label">調整類型</label>
                <div class="col-md-10">
                  <div class="radio-list">
                    <label class="radio-inline"><input type="radio" name="Item[purchase_sales_stock_type]" value="1" <?php echo (@$Item['purchase_sales_stock_type']=='1')?'checked':'';?> required>入庫</label>
                    <label class="radio-inline"><input type="radio" name="Item[purchase_sales_stock_type]" value="2" <?php echo (@$Item['purchase_sales_stock_type']=='2')?'checked':'';?> required>報廢</label>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">成本價</label>
                <div class="col-md-2">
                  <input type="number" min="1" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" class="form-control" name="Item[cost]" value="<?php echo @$Item['cost'];?>">
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-md-2 control-label">數量 <span class="require">*</span></label>
                <div class="col-md-2">
                  <input type="number" min="1" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" class="form-control" name="Item[actua_spec_option_amount]" value="<?php echo abs(@$Item['actua_spec_option_amount']);?>" required>
                </div>
              </div>
              <div class="form-group">
                <label for="password" class="col-md-2 control-label">備註</label>
                <div class="col-md-10">
                  <textarea class="form-control" rows="3" name="Item[description]"><?php echo @$Item['description'];?></textarea>
                </div>
              </div>
                <div class="form-group">
                  <label for="updater" class="col-md-2 control-label">更新者</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['update_member_sn'])?@$Item['update_member_sn']:@$Item['create_member_sn']; ?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-md-2 control-label">更新時間</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['last_time_update']!='0000-00-00 00:00:00')?@$Item['last_time_update']:@$Item['create_date']; ?>" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <input type="hidden" name="purchase_sales_stock_sn" value="<?php echo @$Item['purchase_sales_stock_sn']; ?>">
                    <input type="hidden" name="Item[purchase_sales_stock_status]" value="<?php echo (@$Item['purchase_sales_stock_status'])?@$Item['purchase_sales_stock_status']:'1'; ?>">
                    <input type="hidden" name="Item[product_sn]" value="<?php echo (@$Item['product_sn'])?@$Item['product_sn']:$product_sn; ?>">
                    <input type="hidden" name="Item[currency_code]" value="<?php echo (@$Item['currency_code'])?@$Item['currency_code']:'1'; ?>">
                    <input type="hidden" name="old_amount" value="<?php echo (@$Item['actua_spec_option_amount'])?@$Item['actua_spec_option_amount']:'0'; ?>">
                    <input type="hidden" name="unispec_flag" value="<?php echo (@$Item['unispec_flag'])?@$Item['unispec_flag']:'0'; ?>">
                    <input type="hidden" name="unispec_qty_in_stock" value="<?php echo (@$Item['unispec_qty_in_stock'])?@$Item['unispec_qty_in_stock']:''; ?>">
                    <input type="hidden" name="purchase_sales_stock_type" value="<?php echo (@$Item['purchase_sales_stock_type'])?@$Item['purchase_sales_stock_type']:'0'; ?>">
                    <input type="hidden" name="associated_mutispec_stock_sn" value="<?php echo (@$Item['associated_mutispec_stock_sn'])?@$Item['associated_mutispec_stock_sn']:'0'; ?>">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="reset" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->