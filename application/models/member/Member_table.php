<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_table extends CI_Model {

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('sean_models/Sean_db_tools');


		/* Begin 修改  */
		$this->_ct_name="會員管理";// 功能名稱
		$this->_url="Member/memberTable/";//列表功能url
		$this->_url_item="Member/memberTable_item/";//處理動作 url
		$this->_table="ij_member";//table name
		$this->_id="member_sn";//table key
		$this->_table_field=array(
					 "user_name" => "登入狀態代碼",
					 "email" => "帳號",
					 "last_name" => "姓名",
					 "gender" => "性別",
					 "tel" => "市話",
					 "cell" => "手機",
					 "wedding_date" => "結婚日期",
					 "birthday" => "生日",
					 "addr1" => "住址",
					 "memo" => "備註",
					 "addr_state" => "地區",
					 "addr_city_code" => "city",
					 "addr_town_code" => "town",
					 "zipcode" => "zipcode",
					 "password" => "密碼",
					 "member_status" => "帳號狀態",
					 "member_level_type" => "會員等級",
					 "system_user_flag" => "是否為總控人員",
				);

				/*
					 address model
					addr_country_code  addr_country
					add_state_code  addr_state
					addr_city_code  addr_city
					addr_town_code  addr_town
					zipcode  addr1 addr2
				*/

				//
				//"owner_dealer" => "關聯廠商",

		/* End 修改  */

    }

    //取得列表內容
    public function table_value_list( $_where,$_id = 0)
	{
		$_table_key=$this->_id;
	    $page_data['_body_result']="";
		//判斷是否有帶入特殊查詢
		if(strlen($_where)==0)
		{
			$_where=" left join ij_member_level_type_ct on ij_member_level_type_ct.member_level_type=ij_member.member_level_type order by apply_date desc";

		}
		// 取得列表
		$_v=" ".$this->_table.".*, concat(first_name,'',last_name) as ijtmp_name ";
		$_result01=$this->Sean_db_tools->db_get_max_record($_v,$this->_table,$_where);

		/* 列表標頭名稱 */
		$page_data['_table_thead_name']=array("姓名","Email","縣市","鄉鎮市區","住址","電話","結婚日期","關聯廠商","帳號狀態","功能");
		$page_data['_table_thead_field']=array("ijtmp_name","email","addr_state","addr_city","addr1","tel","wedding_date","member_level_type","member_status");


		/* 類表內容 */
		if(is_array($_result01))
		{
                 		$_i=0;
                 		foreach($_result01 as $key => $value)
                    	{


			                    $_body_result='';
						/* Begin 修改  */
			                    $i_key=0;
								foreach(	$page_data['_table_thead_field'] as $key => $_value)
								{
									$page_data['_body_result'][$_i][$i_key]=array("_value"=>$value->$_value,"_class"=>"");
			                    	$i_key++;
								}


						/* End 修改  */
									$page_data['_body_result'][$_i][$i_key]=array("_value"=>'
			                    		 		<a href="'.$this->init_control.$this->_url_item.'edit/'.$value->$_table_key.'"><i class="fa fa-edit"></i>編輯</a>&nbsp;&nbsp;
						                      <a href="'.$this->init_control.$this->_url_item.'delete/'.$value->$_table_key.'"><i class="fa fa-circle-o-notch"></i>重發驗證信</a>&nbsp;&nbsp;
											   <a href="'.$this->init_control.$this->_url_item.'delete/'.$value->$_table_key.'"><i class="fa fa-lightbulb-o"></i>忘記密碼&nbsp;&nbsp;
						                  	',"_class"=>'class="center"');
                				$_i++;


                			}

    				}
			return $page_data;

    }

	public function action_edit( $_where = "") {

		$_result="";
		//$_result=$this->Sean_db_tools->db_get_max_record("ij_member.*,ij_wedding_website.wedding_date","ij_member left join ij_wedding_website on ij_wedding_website.member_sn=ij_member.member_sn",$_where);
		$_result=$this->Sean_db_tools->db_get_max_record("*","ij_member",$_where);

		return $_result;

	}

	public function action_insert() {

			$_result="";

			foreach($this->_table_field as $key => $value)
			{
					if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
					$_data[$key]=$this->input->get_post($key);
			}

			if(strlen($this->input->get_post("password")) > 0 )
			$_data["password"]=$this->input->get_post("password");



			$_data["email"]=$_data["user_name"];
			$_data["member_level_type"]=1;
			//$_data["member_level"]=1;
			$_data["system_user_flag"]=0;

			//basic insert
			//$_data["create_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			//$_data["create_date"]=date("Y-m-d H:i:s");
			//basic update and insert
			$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			$_data["last_time_update"]=date("Y-m-d H:i:s");



		$_result=$this->Sean_db_tools->db_insert_record($_data,$this->_table);

		return $_result;

	}

	public function action_update($_where,$_key_id,$_key) {

			$_result="";

			foreach($this->_table_field as $key => $value)
			{
					if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
					$_data[$key]=$this->input->get_post($key);

			}
			// 關聯廠商
			if($dealer_short_name=$this->input->post('dealer_short_name')){
			  $_where2=" where member_sn='".$_key."'";   //先把原本資料的status改成 2
				$_data_dealer['status']='2';
		    $this->Sean_db_tools->db_update_record($_data_dealer,'ij_member_dealer_relation',$_where2);
				$dealer_short_name_array=explode(',',$dealer_short_name);
				$member_dealers=$this->Sean_db_tools->db_get_max_array('member_dealer_relation_sn,dealer_sn','ij_member_dealer_relation',$_where2);
				foreach($dealer_short_name_array as $key=>$Item){
					$ifinsert=false;
					$_where2=" where dealer_short_name='".$Item."'";
					$_data_dealer=$this->Sean_db_tools->db_get_one_record('dealer_sn','ij_dealer',$_where2);
					foreach($member_dealers as $key2=>$Item2){
						if($_data_dealer){
							if($_data_dealer['dealer_sn']==$Item2['dealer_sn']){
								$ifinsert=true;
								 break;
							}
						}
					}
					//var_dump($_data_dealer['dealer_sn']);
					if(!$ifinsert){ //沒有新增
						$_data_dealer['dealer_sn']=$_data_dealer['dealer_sn'];
						$_data_dealer['member_sn']=$_key;
						$_data_dealer['join_path_code']='1';
						$_data_dealer['status']='1';
						$_data_dealer=$this->Sean_db_tools->CheckUpdate($_data_dealer,1); //add
						$this->Sean_db_tools->db_insert_record($_data_dealer,'ij_member_dealer_relation');
					}else{
					//echo $Item2['dealer_sn'].'-'.$ifinsert.'<br>';
						$_data_dealer['status']='1';
						$_data_dealer=$this->Sean_db_tools->CheckUpdate($_data_dealer,0); //update
			  		$_where2=" where member_dealer_relation_sn='".$Item2['member_dealer_relation_sn']."'";
				    $this->Sean_db_tools->db_update_record($_data_dealer,'ij_member_dealer_relation',$_where2);
					}
				}
			  $_where2=" where member_sn='".$_key."' and status='2'";   //把status 2 刪除
				$this->Sean_db_tools->db_delete_record('ij_member_dealer_relation',$_where2);
			}
			//exit();
			$_data["addr_country_code"] ="886";
			$_data["add_state_code"] ="886";
			$_data["addr_country"] ="中華民國";
			//$_data["addr_state"] ="台灣";
	    //$_data["zipcode"]="";
	    $_data["addr_city"]="";
	    $_data["addr_town"]="";




			$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			$_data["last_time_update"]=date("Y-m-d H:i:s");

			if(strlen($this->input->get_post("password")) > 0 )
			//$_data["password"]=$this->input->get_post("password");
			$_data["password"]=md5($this->input->get_post("password"));


			//$this->session->set_flashdata("message",$_data);
			//var_dump($this->_table);
			//exit();
		 $_result=$this->Sean_db_tools->db_update_record($_data,$this->_table,$_where,$_key_id,$_key);
						//echo $this->db->last_query();

		 //$_data2="";
		 //$_data2["wedding_date"]="";
		 //$_data2["wedding_date"]=$this->input->get_post("wedding_date");
     //$_result=$this->Sean_db_tools->db_update_record($_data2,"ij_wedding_website",$_where,"wedding_website_sn","0");



		return $_result;

	}

	public function action_delete( $_where) {

		$_result="";

		$_result=$this->Sean_db_tools->db_delete_record($this->_table,$_where);

		return $_result;

	}

	//取得新排序的值
	public function get_next_sort($_id=0 ) {

		$_result="0";

		$_where=" where ".$this->_id." > 0 order by sort_order desc ";
		$_result=$this->Sean_db_tools->db_get_one_record("sort_order",$this->_table,$_where);

		if(is_array($_result["sort_order"]) )
		$_result=1;
		else
		$_result=$_result["sort_order"]+1;

		return $_result;

	}

	//取得定義參數值
	public function get_define_value($_id)
	{
		if(isset($this->$_id))
		return $this->$_id;
		else
		return "";
	}

}
