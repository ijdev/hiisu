<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<!-- END PAGE LEVEL STYLES -->
<style>
  .feature-img{vertical-align: bottom;}
  .adType{display: none;}
</style>


<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>廣告檔期設定</h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/marketing/adsMaterialItem/<?php echo $banner_content_sn; ?>">廣告素材</a><i class="fa fa-circle"></i>
      </li>
      <li class="active">
        廣告檔期設定
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">廣告檔期設定</span>
            </div>
            <div class="tools">
              <a href="admin/marketing/adsMaterialItem/<?php echo $banner_content_sn; ?>#tab_schedule"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
          	
			            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="admin/marketing/adsScheduleItem/<?php echo @$banner_content_sn; ?>">
			              <div class="form-body">
			                <div class="form-group">
			                  <label for="name" class="col-lg-3 control-label">廣告素材</label>
			                  <div class="col-lg-8">
			                    <?php echo $banner_content_name;?>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="name" class="col-lg-3 control-label">起始時間</label>
			                  <div class="col-lg-4">
			                    <div class="input-group date form_datetime">
			                      <input name="Schedule[banner_list_start_date]" type="text" class="form-control" value="<?php echo (@$Schedule['banner_list_start_date']!='0000-00-00 00:00:00')?@$Schedule['banner_list_start_date']:'';?>">
			                      <span class="input-group-btn">
			                        <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
			                      </span>
			                    </div>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="name" class="col-lg-3 control-label">結束時間</label>
			                  <div class="col-lg-4">
			                    <div class="input-group date form_datetime">
			                    	<input name="Schedule[banner_list_end_date]" type="text" class="form-control" value="<?php echo (@$Schedule['banner_list_end_date']!='0000-00-00 00:00:00')?@$Schedule['banner_list_end_date']:'';?>">
			                      <span class="input-group-btn">
			                        <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
			                      </span>
			                    </div>
			                  </div>
			                </div>

			                <div class="form-group">
			                  <label for="active" class="col-lg-3 control-label">連結類型</label>
			                  <div class="col-lg-8">
			                   <select name="Schedule[banner_link_type]" id="banner_link_type" class="form-control">
			                      <option value="">請選擇</option>
			                    <?php if($link_types){foreach($link_types as $key=>$_Item){ ?>
			                      <option value="<?php echo $_Item['banner_link_type'];?>" banner_link_type_eng_name="<?php echo $_Item['banner_link_type_eng_name'];?>" <?php echo (@$Schedule['banner_link_type']==$_Item['banner_link_type'])?'selected':'';?>><?php echo $_Item['banner_link_type_name'];?></option>
			                    <?php }}?>
			                    </select>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="name" class="col-lg-3 control-label">連結參數</label>
			                  <div class="col-lg-8">
			                    <div class="adType search_keyword">
			                      <input type="text" name="Schedule[search_keyword]" class="form-control" placeholder="關鍵字" value="<?php echo (@$Schedule['banner_link_type_eng_name']=='search_keyword')? @$Schedule['search_keyword']:'';?>">
			                    </div>
			                    <div class="adType link_product_sn">
			                      <input type="text" name="Schedule[link_product_sn]" class="form-control" placeholder="產品編號" value="<?php echo (@$Schedule['banner_link_type_eng_name']=='link_product_sn')? @$Schedule['link_product_sn']:'';?>">
			                    </div>
			                    <div class="adType link_category_sn">
			                      <input type="text" name="Schedule[link_category_sn]" class="form-control" placeholder="分類編號" value="<?php echo (@$Schedule['banner_link_type_eng_name']=='link_category_sn')? @$Schedule['link_category_sn']:'';?>">
			                    </div>
			                    <div class="adType banner_link">
			                      <input type="url" name="Schedule[banner_link]" class="form-control" placeholder="必須為 http:// 或 https:// 開頭的有效連結" value="<?php echo (@$Schedule['banner_link_type_eng_name']=='banner_link')? @$Schedule['banner_link']:'';?>">
			                    </div>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="name" class="col-lg-3 control-label">ga_code</label>
			                  <div class="col-lg-8">
			                    <input type="text" name="Schedule[mkt_ga_code]" class="form-control" placeholder="" value="<?php echo @$Schedule['mkt_ga_code'];?>">
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="active" class="col-lg-3 control-label">View</label>
			                  <div class="col-lg-8">
			                    <span class="form-control-static"><?php echo @$Schedule['view_amount'];?></span>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="active" class="col-lg-3 control-label">Click</label>
			                  <div class="col-lg-8">
			                    <span class="form-control-static"><?php echo @$Schedule['click_amount'];?></span>
			                  </div>
			                </div>
				              <div class="form-group">
				                <label for="email" class="col-lg-3 control-label">排序</label>
				                <div class="col-lg-8">
				                  <input type="text" class="form-control" name="Schedule[sort_order]" value="<?php echo (@$Schedule['sort_order']) ? @$Schedule['sort_order']:'9999'; ?>" required>
				                </div>
				              </div>
			                <div class="form-group">
			                  <label for="active" class="col-lg-3 control-label">是否啟用 <span class="require">*</span></label>
			                  <div class="col-lg-8">
			                    <select name="Schedule[status]" class="form-control">
			                      <option value="1" <?php if(@$Schedule['status']=='1'){ ?>selected<?php }?>>是</option>
			                      <option value="0" <?php if(@$Schedule['status']=='0'){ ?>selected<?php }?>>否</option>
			                    </select>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="updater" class="col-lg-3 control-label">更新者</label>
			                  <div class="col-lg-8">
			                    <input type="text" class="form-control" placeholder="<?php echo (@$Schedule['update_member_sn'])?@$Schedule['update_member_sn']:@$Schedule['create_member_sn']; ?>" Disabled>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="time-update" class="col-lg-3 control-label">更新時間</label>
			                  <div class="col-lg-8">
			                    <input type="text" class="form-control" placeholder="<?php echo (@$Schedule['last_time_update']!='0000-00-00 00:00:00')?@$Schedule['last_time_update']:@$Schedule['create_date']; ?>" Disabled>
			                  </div>
			                </div>
			              </div>
			              <div class="form-actions">
			                <div class="row">
			                  <div class="col-md-offset-3 col-md-9">
			                    <input type="hidden" name="banner_schedule_sn" value="<?php echo @$Schedule['banner_schedule_sn']; ?>">
			                    <input type="hidden" id="banner_type" name="banner_type" value="<?php echo @$Schedule['banner_type']; ?>">
			                    <input type="hidden" id="banner_content_sn" name="Schedule[banner_content_sn]" value="<?php echo $banner_content_sn; ?>">
			                    <input type="hidden" name="banner_link" value="<?php echo @$Schedule['banner_link']; ?>">
			                    <input type="hidden" id="banner_link_type_eng_name" name="banner_link_type_eng_name" value="<?php echo @$Schedule['banner_link_type_eng_name']; ?>">
			                    <button type="submit" class="btn green">儲存</button>
			                    <button type="button" class="btn default">Cancel</button>
			                  </div>
			                </div>
			              </div>
			            </form>

          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->