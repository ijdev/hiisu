<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Marketing extends MY_Controller {
 	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->library('sean_lib/Sean_form_general');
		$this->load->model('general/My_general_ct');
		$this->load->config('ij_config');
		$this->load->config('ij_sys_config');
		$this->load->model('sean_models/Sean_general');
		$this->load->model('sean_models/Sean_db_tools');
		$this->load->model('libraries_model');
		$this->page_data = array();
		$this->page_data['unreadmessage']= $this->libraries_model->get_unread_message_count();
		$this->load->helper('cookie');
		$this->page_data['init_control']="admin/marketing/";
	}
	/**
	 * 廣告客戶列表
	 *
	 */
	public function adsCustomer()
	{
		$flg=$this->input->get('flg',true);
		if($flg=='reset'){
			redirect(base_url('admin/marketing/adsCustomer'));
			exit();
		}else{
			$from=($this->input->get('from',true))? $this->input->get('from',true):'';
			$to=($this->input->get('to',true))? $this->input->get('to',true):'';
			//$published_locaiotn_type=$this->input->get('published_locaiotn_type',true);
			//$banner_location_sn=$this->input->get('banner_location_sn',true);
		}
		$where=array();
		if($from) $where['ij_banner_customer.partnership_start_date >=']=$from.' 00:00:00';
		if($to) $where['ij_banner_customer.partnership_end_date <=']=$to.' 23:59:59';
		//if($published_locaiotn_type) $where['ij_banner_content.published_locaiotn_type']=$published_locaiotn_type;
		//if($banner_location_sn) $where['ij_banner_content.banner_location_sn']=$banner_location_sn;

		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/customer/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_content']['Items']=$this->libraries_model->_get_adsCustomers(0,$where);
		/*$where1=array('display_flag'=>'1');
		$page_data['page_content']['published_locaiotn_types']=$this->libraries_model->_select('ij_published_locaiotn_type_ct',$where1,0,0,0,0,0,'result_array'); //頻道
		if($published_locaiotn_type){
			$where2['published_locaiotn_type']=$published_locaiotn_type;
			$page_data['page_content']['banner_locations']=$this->libraries_model->_select('ij_banner_location',$where2,0,0,0,'banner_location_name',0,'result_array'); //版位
		}else{
			$page_data['page_content']['banner_locations']=array();
		}*/
		$page_data['page_content']['from'] = $from;
		$page_data['page_content']['to'] = $to;
		//$page_data['page_content']['published_locaiotn_type'] = $published_locaiotn_type;
		//$page_data['page_content']['banner_location_sn'] = $banner_location_sn;
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/customer/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 廣告客戶編輯
	 *
	 */
	public function adsCustomerItem($banner_customer_sn=0)
	{
		//var_dump($this->input->post('Item'));
		if($data_array=$this->input->post('Item')){
			if(!$banner_customer_sn=$this->input->post('banner_customer_sn')){ //新增
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);//方便秀最後更新
				$data_array['status']='1';
				$banner_customer_sn=$this->libraries_model->_insert('ij_banner_customer',$data_array);
				$this->session->set_userdata(array('Mes'=>'新增成功!'));
			}else{
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				$this->libraries_model->_update('ij_banner_customer',$data_array,'banner_customer_sn',$banner_customer_sn);
				$this->session->set_userdata(array('Mes'=>'修改成功!'));
			}
			redirect(base_url('admin/marketing/adsCustomerItem/'.$banner_customer_sn));
		}

		$page_data = array();

		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/customer/item'; // 頁面主內容 view 位置, 必要


		$page_data['page_content']['Item']=$this->libraries_model->_get_adsCustomers($banner_customer_sn);
		//經銷商短名
		$page_data['_member_dealer']=$this->libraries_model->_select('ij_dealer','dealer_status','1',0,0,'dealer_sn',0,'result_array','dealer_sn,dealer_short_name');
		//供應商
		$page_data['suppliers']=$this->libraries_model->_select('ij_supplier','supplier_status','1',0,0,'supplier_sn',0,'result_array','supplier_sn,supplier_name');
		//銷售業務
		$page_data['page_content']['sales']=$this->libraries_model->_get_admin_users();
		//代理業務
		$page_data['page_content']['agents']=$this->libraries_model->_get_admin_users();


		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/customer/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 廣告版位列表 Template
	 *
	 */
	public function adsSlot()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['Items']=$this->libraries_model->_get_adsSlots();
		//$page_data['page_content']['banner_types']=$this->libraries_model->_select('ij_banner_type_ct',$where,0,0,0,0,0,'result_array');
		//$page_data['page_content']['link_types']=$this->libraries_model->_select('ij_banner_link_type_ct',$where,0,0,0,0,0,'result_array');
		//$page_data['page_content']['published_locaiotn_types']=$this->libraries_model->_select('ij_published_locaiotn_type_ct',$where,0,0,0,0,0,'result_array');
		$page_data['page_content']['view_path'] = 'admin/page_ads/slot/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/slot/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 廣告版位編輯 Template
	 *
	 */
	public function adsSlotItem($banner_location_sn=0)
	{
		if($data_array=$this->input->post('Item')){
			if($this->input->post('banner_type_eng_name')=='img'){
				//圖片長寬必須大於0
				if($data_array['width']<=0 || $data_array['hight']<=0){
          			$this->ijw->_wait(base_url('admin/marketing/adsSlotItem/'.$banner_location_sn),"1","廣告型態為圖片，寬度跟高度必須大於 0","1");
				}
			}
			//exit();

			if(!$banner_location_sn=$this->input->post('banner_location_sn')){ //新增
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);//方便秀最後更新
				$data_array['status']='1';
				//if($data_array['banner_type'])
				$data_array['default_banner_content_save_dir']=$this->input->post($this->input->post('banner_type_eng_name').'AD');
				$default_banner_content_save_dir=$this->input->post('default_banner_content_save_dir');

				//$data_array['associated_content_type_sn']=implode('、',$data_array['associated_content_type_sn']);
				//$data_array['post_category_sn_set']=implode('、',$data_array['post_category_sn_set']);
				$banner_location_sn=$this->libraries_model->_insert('ij_banner_location',$data_array);
				$this->session->set_userdata(array('Mes'=>'新增成功!'));
			}else{
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				if($this->input->post($this->input->post('banner_type_eng_name').'AD')){
					$data_array['default_banner_content_save_dir']=$this->input->post($this->input->post('banner_type_eng_name').'AD');
				}
				//$data_array['associated_content_type_sn']=implode('、',$data_array['associated_content_type_sn']);
				//$data_array['post_category_sn_set']=implode('、',$data_array['post_category_sn_set']);
				$this->libraries_model->_update('ij_banner_location',$data_array,'banner_location_sn',$banner_location_sn);
				$this->session->set_userdata(array('Mes'=>'修改成功!'));
				//redirect(base_url('admin/marketing/contentBlog/'));
				$default_banner_content_save_dir=$this->input->post('default_banner_content_save_dir');
			}
      if (!empty($_FILES['banner']['name'])) {
					//$this->load->library('upload');
          $config['upload_path']  = ADMINUPLOADPATH.'ad/' ;
          $config['allowed_types']= 'gif|jpg|png|jpeg';
          $config['max_size']     = '4196';
          $config['max_width']    = '1920';
          $config['max_height']   = '1920';
          $config['encrypt_name']   = true;

          $this->load->library('upload', $config);
					if(!is_dir(ADMINUPLOADPATH.'ad/')) mkdir(ADMINUPLOADPATH.'ad/',0777);

					//$this->upload->initialize($config);
          if (!$this->upload->do_upload('banner')) {
              //$error = array('error' => $this->upload->display_errors());
							$this->session->set_userdata(array('Mes'=>$this->session->userdata('Mes').' '.strip_tags($this->upload->display_errors())));
          		//var_dump($error);
					}else{
              $img	 = $this->upload->data();
							$this->libraries_model->_update('ij_banner_location',array('default_banner_content_save_dir'=>'ad/'.$img['file_name']),'banner_location_sn',$banner_location_sn);
							//var_dump($img);
              if(is_file(ADMINUPLOADPATH.$default_banner_content_save_dir)) unlink(ADMINUPLOADPATH.$default_banner_content_save_dir);
          }
          //exit();
      }
			redirect(base_url('admin/marketing/adsSlotItem/'.$banner_location_sn));
		}
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$where=array('display_flag'=>'1');
		//$page_data['page_content']['Item']=$this->libraries_model->_select('ij_banner_location','banner_location_sn',$banner_location_sn,0,0,0,0,'row');
		$page_data['page_content']['Item']=$this->libraries_model->_get_adsSlots($banner_location_sn);
		//var_dump($page_data['page_content']['Item']);
		$page_data['page_content']['banner_types']=$this->libraries_model->_select('ij_banner_type_ct',$where,0,0,0,0,0,'result_array');
		$page_data['page_content']['link_types']=$this->libraries_model->_select('ij_banner_link_type_ct',$where,0,0,0,0,0,'result_array');
		$page_data['page_content']['published_locaiotn_types']=$this->libraries_model->_select('ij_published_locaiotn_type_ct',$where,0,0,0,0,0,'result_array');

		$page_data['page_content']['view_path'] = 'admin/page_ads/slot/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/slot/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 廣告素材列表 Template
	 *
	 */
	public function adsMaterial()
	{
		$flg=$this->input->get('flg',true);
		if($flg=='reset'){
			redirect(base_url('admin/marketing/adsMaterial'));
			exit();
		}else{
			//$from=($this->input->get('from',true))? $this->input->get('from',true):date('Y-m-d', strtotime(date('Y-m-d'). ' - 14 days'));
			//$to=($this->input->get('to',true))? $this->input->get('to',true):date('Y-m-d');
			//改不預設日期區間
			$from=($this->input->get('from',true))? $this->input->get('from',true):'';
			$to=($this->input->get('to',true))? $this->input->get('to',true):'';
			$published_locaiotn_type=$this->input->get('published_locaiotn_type',true);
			$banner_location_sn=$this->input->get('banner_location_sn',true);
		}
		$where=array('ij_banner_content.banner_location_sn!='=>'11');
		if($from) $where['banner_eff_start_date >=']=$from.' 00:00:00';
		if($to) $where['banner_eff_end_date <=']=$to.' 23:59:59';
		if($published_locaiotn_type) $where['ij_banner_content.published_locaiotn_type']=$published_locaiotn_type;
		if($banner_location_sn) $where['ij_banner_content.banner_location_sn']=$banner_location_sn;

		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/material/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_content']['Items']=$this->libraries_model->_get_adsMaterials(0,$where);
		$where1=array('display_flag'=>'1');
		$page_data['page_content']['published_locaiotn_types']=$this->libraries_model->_select('ij_published_locaiotn_type_ct',$where1,0,0,0,0,0,'result_array'); //頻道
		if($published_locaiotn_type){
			$where2=array('banner_location_sn!='=>'11');
			$where2['published_locaiotn_type']=$published_locaiotn_type;
			$page_data['page_content']['banner_locations']=$this->libraries_model->_select('ij_banner_location',$where2,0,0,0,'banner_location_name',0,'result_array'); //版位
		}else{
			$page_data['page_content']['banner_locations']=array();
		}
		$page_data['page_content']['from'] = $from;
		$page_data['page_content']['to'] = $to;
		$page_data['page_content']['published_locaiotn_type'] = $published_locaiotn_type;
		$page_data['page_content']['banner_location_sn'] = $banner_location_sn;
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/material/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 廣告素材編輯 Template
	 *
	 */
	public function adsMaterialItem($banner_content_sn=0)
	{
		if($data_array=$this->input->post('Item')){
			//var_dump($this->input->post('banner_type_eng_name'));
			//exit();
			if($this->input->post('domain_sn_set[]')) $data_array['domain_sn_set']=implode("、",$this->input->post('domain_sn_set[]'));
			if(!$banner_content_sn=$this->input->post('banner_content_sn')){ //新增
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);//方便秀最後更新
				$data_array['status']='1';
				//if($data_array['banner_type'])
				$data_array['click_total_amount']='0';
				$data_array['view_total_amount']='0';
				$data_array['banner_content_save_dir']=$this->input->post($this->input->post('banner_type_eng_name').'AD');
				//var_dump($data_array);
				//exit;
				//$data_array['associated_content_type_sn']=implode('、',$data_array['associated_content_type_sn']);
				//$data_array['post_category_sn_set']=implode('、',$data_array['post_category_sn_set']);
				$banner_content_sn=$this->libraries_model->_insert('ij_banner_content',$data_array);
				$this->session->set_userdata(array('Mes'=>'新增成功!'));
			}else{
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				if($this->input->post('banner_type_eng_name')!='img'){
					$data_array['banner_content_save_dir']=$this->input->post($this->input->post('banner_type_eng_name').'AD');
				}
				//$data_array['associated_content_type_sn']=implode('、',$data_array['associated_content_type_sn']);
				//$data_array['post_category_sn_set']=implode('、',$data_array['post_category_sn_set']);
				$this->libraries_model->_update('ij_banner_content',$data_array,'banner_content_sn',$banner_content_sn);
				$this->session->set_userdata(array('Mes'=>'修改成功!'));
				//redirect(base_url('admin/marketing/contentBlog/'));
				$banner_content_save_dir=$this->input->post('banner_content_save_dir');
			}
      if (!empty($_FILES['banner']['name'])) {
					//$this->load->library('upload');
          $config['upload_path']  = ADMINUPLOADPATH.'ad/' ;
          $config['allowed_types']= 'gif|jpg|png|jpeg';
          $config['max_size']     = '4196';
          $config['max_width']    = '1920';
          $config['max_height']   = '1920';
          $config['encrypt_name']   = true;

          $this->load->library('upload', $config);
					if(!is_dir(ADMINUPLOADPATH.'ad/')) mkdir(ADMINUPLOADPATH.'ad/',0777);

					//$this->upload->initialize($config);
          if (!$this->upload->do_upload('banner')) {
              //$error = array('error' => $this->upload->display_errors());
							$this->session->set_userdata(array('Mes'=>$this->session->userdata('Mes').' '.strip_tags($this->upload->display_errors())));
					}else{
              $img	 = $this->upload->data();
							$this->libraries_model->_update('ij_banner_content',array('banner_content_save_dir'=>'ad/'.$img['file_name']),'banner_content_sn',$banner_content_sn);
							//var_dump($img);
              if(@$banner_content_save_dir && is_file(ADMINUPLOADPATH.$banner_content_save_dir)) unlink(ADMINUPLOADPATH.$banner_content_save_dir);
          }
          //exit();
      }
			redirect(base_url('admin/marketing/adsMaterialItem/'.$banner_content_sn));
		}

		$flg=$this->input->get('flg',true);
		if($flg=='reset'){
			redirect(base_url('admin/marketing/adsMaterialItem/'.$banner_content_sn.'#tab_schedule'));
			exit();
		}else{
			//$from=($this->input->get('from',true))? $this->input->get('from',true):date('Y-m-d', strtotime(date('Y-m-d'). ' - 30 days'));
			//$to=($this->input->get('to',true))? $this->input->get('to',true):date('Y-m-d');
			//改不預設日期區間
			$from=($this->input->get('from',true))? $this->input->get('from',true):'';
			$to=($this->input->get('to',true))? $this->input->get('to',true):'';
		}
		$where_schedule=array('ij_banner_schedule_log.banner_content_sn'=>$banner_content_sn);
		if($from) $where_schedule['banner_list_start_date >=']=$from.' 00:00:00';
		if($to) $where_schedule['banner_list_end_date <=']=$to.' 23:59:59';
		//if($published_locaiotn_type) $where_schedule['ij_banner_content.published_locaiotn_type']=$published_locaiotn_type;
		//if($banner_location_sn) $where_schedule['ij_banner_content.banner_location_sn']=$banner_location_sn;
     //var_dump($where_schedule);

		$page_data = array();

		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/material/item'; // 頁面主內容 view 位置, 必要

		//產業屬性
		$_where=" where status='1' ";
		$this->load->model('sean_models/Sean_db_tools');
		$_biz_domain_list=$this->Sean_db_tools->db_get_max_record("*","ij_biz_domain",$_where);
		$page_data['page_content']['_biz_domain_list']=$_biz_domain_list;

		//$page_data['page_content']['Item']=$this->libraries_model->_select('ij_banner_location','banner_location_sn',$banner_location_sn,0,0,0,0,'row');
		$page_data['page_content']['Item']=$this->libraries_model->_get_adsMaterials($banner_content_sn);
		//var_dump($page_data['page_content']['Item']);
		$where=array('display_flag'=>'1');
		$page_data['page_content']['published_locaiotn_types']=$this->libraries_model->_select('ij_published_locaiotn_type_ct',$where,0,0,0,0,0,'result_array'); //頻道
		$page_data['page_content']['banner_types']=$this->libraries_model->_select('ij_banner_type_ct',$where,0,0,0,0,0,'result_array'); //型態
		$page_data['page_content']['link_types']=$this->libraries_model->_select('ij_banner_link_type_ct',$where,0,0,0,0,0,'result_array'); //連結類型
		$page_data['page_content']['banner_customers']=$this->libraries_model->_select('ij_banner_customer',0,0,0,0,'banner_customer_name',0,'result_array'); //客戶
		if($banner_content_sn){
			$where1['published_locaiotn_type']=@$page_data['page_content']['Item']['published_locaiotn_type'];
			$page_data['page_content']['banner_locations']=$this->libraries_model->_select('ij_banner_location',$where1,0,0,0,'banner_location_name',0,'result_array'); //版位
		}else{
			$page_data['page_content']['banner_locations']=array();
		}
		//ij_banner_schedule_log
		$page_data['page_content']['Items']=$this->libraries_model->_get_adsSchedules(0,$where_schedule);

		$page_data['page_content']['from'] = $from;
		$page_data['page_content']['to'] = $to;

		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/material/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}
	/**
	 * 廣告素材列表 Template
	 *
	 */
	public function keyword()
	{
		$flg=$this->input->get('flg',true);
		if($flg=='reset'){
			redirect(base_url('admin/marketing/keyword'));
			exit();
		}else{
			//$from=($this->input->get('from',true))? $this->input->get('from',true):date('Y-m-d', strtotime(date('Y-m-d'). ' - 14 days'));
			//$to=($this->input->get('to',true))? $this->input->get('to',true):date('Y-m-d');
			//改不預設日期區間
			$from=($this->input->get('from',true))? $this->input->get('from',true):'';
			$to=($this->input->get('to',true))? $this->input->get('to',true):'';
			$published_locaiotn_type=$this->input->get('published_locaiotn_type',true);
			$banner_location_sn=$this->input->get('banner_location_sn',true);
		}
		$where=array('ij_banner_content.banner_location_sn'=>'11');
		if($from) $where['banner_eff_start_date >=']=$from.' 00:00:00';
		if($to) $where['banner_eff_end_date <=']=$to.' 23:59:59';
		if($published_locaiotn_type) $where['ij_banner_content.published_locaiotn_type']=$published_locaiotn_type;
		if($banner_location_sn) $where['ij_banner_content.banner_location_sn']=$banner_location_sn;

		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/keyword/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_content']['Items']=$this->libraries_model->_get_adsMaterials(0,$where);
		$where1=array('display_flag'=>'1');
		$page_data['page_content']['published_locaiotn_types']=$this->libraries_model->_select('ij_published_locaiotn_type_ct',$where1,0,0,0,0,0,'result_array'); //頻道
		if($published_locaiotn_type){
			$where2=array('banner_location_sn!='=>'11');
			$where2['published_locaiotn_type']=$published_locaiotn_type;
			$page_data['page_content']['banner_locations']=$this->libraries_model->_select('ij_banner_location',$where2,0,0,0,'banner_location_name',0,'result_array'); //版位
		}else{
			$page_data['page_content']['banner_locations']=array();
		}
		$page_data['page_content']['from'] = $from;
		$page_data['page_content']['to'] = $to;
		$page_data['page_content']['published_locaiotn_type'] = $published_locaiotn_type;
		$page_data['page_content']['banner_location_sn'] = $banner_location_sn;
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/keyword/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 廣告素材編輯 Template
	 *
	 */
	public function keywordItem($banner_content_sn=0)
	{
		if($data_array=$this->input->post('Item')){
			//var_dump($this->input->post('banner_type_eng_name'));
			//exit();
			if($this->input->post('domain_sn_set[]')) $data_array['domain_sn_set']=implode("、",$this->input->post('domain_sn_set[]'));
			if(!$banner_content_sn=$this->input->post('banner_content_sn')){ //新增
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);//方便秀最後更新
				$data_array['status']='1';
				//if($data_array['banner_type'])
				$data_array['click_total_amount']='0';
				$data_array['view_total_amount']='0';
				$data_array['banner_content_save_dir']=$this->input->post($this->input->post('banner_type_eng_name').'AD');
				//var_dump($data_array);
				//exit;
				//$data_array['associated_content_type_sn']=implode('、',$data_array['associated_content_type_sn']);
				//$data_array['post_category_sn_set']=implode('、',$data_array['post_category_sn_set']);
				$banner_content_sn=$this->libraries_model->_insert('ij_banner_content',$data_array);
				$this->session->set_userdata(array('Mes'=>'新增成功!'));
			}else{
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				if($this->input->post('banner_type_eng_name')!='img'){
					$data_array['banner_content_save_dir']=$this->input->post($this->input->post('banner_type_eng_name').'AD');
				}
				//$data_array['associated_content_type_sn']=implode('、',$data_array['associated_content_type_sn']);
				//$data_array['post_category_sn_set']=implode('、',$data_array['post_category_sn_set']);
				$this->libraries_model->_update('ij_banner_content',$data_array,'banner_content_sn',$banner_content_sn);
				$this->session->set_userdata(array('Mes'=>'修改成功!'));
				//redirect(base_url('admin/marketing/contentBlog/'));
				$banner_content_save_dir=$this->input->post('banner_content_save_dir');
			}
      if (!empty($_FILES['banner']['name'])) {
					//$this->load->library('upload');
          $config['upload_path']  = ADMINUPLOADPATH.'ad/' ;
          $config['allowed_types']= 'gif|jpg|png|jpeg';
          $config['max_size']     = '4196';
          $config['max_width']    = '1920';
          $config['max_height']   = '1920';
          $config['encrypt_name']   = true;

          $this->load->library('upload', $config);
					if(!is_dir(ADMINUPLOADPATH.'ad/')) mkdir(ADMINUPLOADPATH.'ad/',0777);

					//$this->upload->initialize($config);
          if (!$this->upload->do_upload('banner')) {
              //$error = array('error' => $this->upload->display_errors());
							$this->session->set_userdata(array('Mes'=>$this->session->userdata('Mes').' '.strip_tags($this->upload->display_errors())));
					}else{
              $img	 = $this->upload->data();
							$this->libraries_model->_update('ij_banner_content',array('banner_content_save_dir'=>'ad/'.$img['file_name']),'banner_content_sn',$banner_content_sn);
							//var_dump($img);
              if(is_file(ADMINUPLOADPATH.$banner_content_save_dir)) unlink(ADMINUPLOADPATH.$banner_content_save_dir);
          }
          //exit();
      }
			redirect(base_url('admin/marketing/keywordItem/'.$banner_content_sn));
		}

		$flg=$this->input->get('flg',true);
		if($flg=='reset'){
			redirect(base_url('admin/marketing/keywordItem/'.$banner_content_sn.'#tab_schedule'));
			exit();
		}else{
			//$from=($this->input->get('from',true))? $this->input->get('from',true):date('Y-m-d', strtotime(date('Y-m-d'). ' - 30 days'));
			//$to=($this->input->get('to',true))? $this->input->get('to',true):date('Y-m-d');
			//改不預設日期區間
			$from=($this->input->get('from',true))? $this->input->get('from',true):'';
			$to=($this->input->get('to',true))? $this->input->get('to',true):'';
		}
		$where_schedule=array('ij_banner_schedule_log.banner_content_sn'=>$banner_content_sn);
		if($from) $where_schedule['banner_list_start_date >=']=$from.' 00:00:00';
		if($to) $where_schedule['banner_list_end_date <=']=$to.' 23:59:59';
		//if($published_locaiotn_type) $where_schedule['ij_banner_content.published_locaiotn_type']=$published_locaiotn_type;
		//if($banner_location_sn) $where_schedule['ij_banner_content.banner_location_sn']=$banner_location_sn;
     //var_dump($where_schedule);

		$page_data = array();

		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/keyword/item'; // 頁面主內容 view 位置, 必要

		//產業屬性
		$_where=" where status='1' ";
		$this->load->model('sean_models/Sean_db_tools');
		$_biz_domain_list=$this->Sean_db_tools->db_get_max_record("*","ij_biz_domain",$_where);
		$page_data['page_content']['_biz_domain_list']=$_biz_domain_list;

		//$page_data['page_content']['Item']=$this->libraries_model->_select('ij_banner_location','banner_location_sn',$banner_location_sn,0,0,0,0,'row');
		$page_data['page_content']['Item']=$this->libraries_model->_get_adsMaterials($banner_content_sn);
		//var_dump($page_data['page_content']['Item']);
		$where=array('display_flag'=>'1');
		$page_data['page_content']['published_locaiotn_types']=$this->libraries_model->_select('ij_published_locaiotn_type_ct',$where,0,0,0,0,0,'result_array'); //頻道
		$page_data['page_content']['banner_types']=$this->libraries_model->_select('ij_banner_type_ct',$where,0,0,0,0,0,'result_array'); //型態
		$page_data['page_content']['link_types']=$this->libraries_model->_select('ij_banner_link_type_ct',$where,0,0,0,0,0,'result_array'); //連結類型
		$page_data['page_content']['banner_customers']=$this->libraries_model->_select('ij_banner_customer',0,0,0,0,'banner_customer_name',0,'result_array'); //客戶
		if($banner_content_sn){
			$where1['published_locaiotn_type']=@$page_data['page_content']['Item']['published_locaiotn_type'];
			$page_data['page_content']['banner_locations']=$this->libraries_model->_select('ij_banner_location',$where1,0,0,0,'banner_location_name',0,'result_array'); //版位
		}else{
			$page_data['page_content']['banner_locations']=array();
		}
		//ij_banner_schedule_log
		$page_data['page_content']['Items']=$this->libraries_model->_get_adsSchedules(0,$where_schedule);

		$page_data['page_content']['from'] = $from;
		$page_data['page_content']['to'] = $to;

		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/keyword/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}
	public function adsMaterialReport($banner_content_sn=0,$banner_customer_sn=0)
	{
		$flg=$this->input->get('flg',true);
		if($flg=='reset'){
			redirect(base_url('admin/marketing/adsMaterial'));
			exit();
		}else{
			//$from=($this->input->get('from',true))? $this->input->get('from',true):date('Y-m-d', strtotime(date('Y-m-d'). ' - 30 days'));
			//$to=($this->input->get('to',true))? $this->input->get('to',true):date('Y-m-d');
			$from=($this->input->get('from',true))? $this->input->get('from',true):'';
			$to=($this->input->get('to',true))? $this->input->get('to',true):'';
			$published_locaiotn_type=$this->input->get('published_locaiotn_type',true);
			$banner_location_sn=$this->input->get('banner_location_sn',true);
		}
		$where=array();
		if($banner_content_sn) $where=array('ij_banner_schedule_log.banner_content_sn'=>$banner_content_sn);
		if($banner_customer_sn) $where['ij_banner_content.banner_customer_sn']=$banner_customer_sn;
		if($from) $where['banner_list_start_date >=']=$from.' 00:00:00';
		if($to) $where['banner_list_end_date <=']=$to.' 23:59:59';
		//if($from) $where['banner_eff_start_date >=']=$from.' 00:00:00';
		//if($to) $where['banner_eff_end_date <=']=$to.' 23:59:59';
		if($published_locaiotn_type) $where['ij_banner_content.published_locaiotn_type']=$published_locaiotn_type;
		if($banner_location_sn) $where['ij_banner_content.banner_location_sn']=$banner_location_sn;

		$page_data = array();

		// 內容設定

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/material/report_table'; // 頁面主內容 view 位置, 必要
		$page_data['page_content']['Items']=$this->libraries_model->_get_ads_content_Schedules($where);
		$where1=array('display_flag'=>'1');
		$page_data['page_content']['published_locaiotn_types']=$this->libraries_model->_select('ij_published_locaiotn_type_ct',$where1,0,0,0,0,0,'result_array'); //頻道
		if($published_locaiotn_type){
			$where2['published_locaiotn_type']=$published_locaiotn_type;
			$page_data['page_content']['banner_locations']=$this->libraries_model->_select('ij_banner_location',$where2,0,0,0,'banner_location_name',0,'result_array'); //版位
		}else{
			$page_data['page_content']['banner_locations']=array();
		}
		$page_data['page_content']['from'] = $from;
		$page_data['page_content']['to'] = $to;
		$page_data['page_content']['published_locaiotn_type'] = $published_locaiotn_type;
		$page_data['page_content']['banner_location_sn'] = $banner_location_sn;
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/material/report_table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}
	/**
	 * 虛擬折扣卷活動列表 Template
	 *
	 */
	public function ecoupon()
	{
		$flg=$this->input->get('flg',true);
		$search=$this->input->get('search',true);
		//var_dump($search);
		$where=array();
		if($search){
			foreach($search as $_key=>$_Item){
				if($_Item) $where[$_key]=$_Item;
			}
		}
		if($flg=='reset'){
			redirect(base_url('admin/marketing/ecoupon'));
			exit();
		}else{
			$content_type_sn=$this->input->get('content_type_sn',true);
			$post_flag=$this->input->get('post_flag',true);
		}
		$page_data = array();
		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		//$where=array('status'=>'1','blog_faq_flag'=>'1','post_flag'=>'1'); //1:blog
		//$page_data['page_content']['content_types']=$this->libraries_model->_select('ij_content_type_config',$where,0,0,0,0,0,'result_array');

		//$where=array('blog_faq_flag'=>'1'); //1:blog

		if($content_type_sn) {
			$like_field='associated_content_type_sn';
		}else{
			$like_field='0';
		}

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$coupon_code_configs=$this->libraries_model->_select('ij_coupon_code_config',$where,0,0,0,'create_date',1,'result_array');
		foreach($coupon_code_configs as $_key=>$_Item){
				$coupon_code_configs[$_key]['ifused'] = ($this->libraries_model->_select('ij_coupon_code',array('coupon_code_status'=>'2','coupon_code_config_sn'=>$_Item['coupon_code_config_sn']),0,0,0,'coupon_code_config_sn',0,'num_rows'))? true:false;
		}
		//var_dump($coupon_code_configs);
		$page_data['page_content']['Items']=$coupon_code_configs;
		$page_data['page_content']['search']=$search;
		$page_data['page_content']['view_path'] = 'admin/page_ads/ecoupon/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/ecoupon/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 虛擬折扣卷編輯 Template
	 *
	 */
	public function ecouponItem($coupon_code_config_sn=0)
	{

		if($this->input->post('Item')){
			$data_array=$this->input->post('Item');
			$apply_TA=$this->input->post('apply_TA');
		    $send_obj = $this->libraries_model->_select('ij_member_level_type_ct',array('display_flag'=>'1','member_level_type <'=>'9'),0,0,0,'member_level_type',1,'result_array');
		    $data_array['apply_TA']='';
		    //var_dump($apply_TA);
		    if($apply_TA){
			  	foreach($send_obj as $key=>$_Item){
			  	//echo $_Item['member_level_type'];
			  		foreach($apply_TA as $key2=>$_Item2){
						if($_Item['member_level_type']==$_Item2){
							$ifchecked=true;
							break;
						}else{
							$ifchecked=false;
						}
					}
					if($ifchecked){
						$data_array['apply_TA'].='1';
					}else{
						$data_array['apply_TA'].='0';
					}
					if($key < count($send_obj)-1){
						$data_array['apply_TA'].=';';
					}
			    }
			}
		  //echo count($send_obj);
			//var_dump($data_array['apply_TA']);
			//exit();

			if(!$coupon_code_config_sn=$this->input->post('coupon_code_config_sn')){ //判斷新增或修改
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				$data_array['status']='1';
				//$data_array['apply_TA']=implode(';',$apply_TA);
				$coupon_code_config_sn=$this->libraries_model->_insert('ij_coupon_code_config',$data_array);
				$this->session->set_userdata(array('Mes'=>'新增成功!'));
			}else{
				$if_used = $this->libraries_model->_select('ij_coupon_code',array('coupon_code_status'=>'2','coupon_code_config_sn'=>$coupon_code_config_sn),0,0,0,'coupon_code_config_sn',0,'num_rows');
				//var_dump($ifchange);
				//exit();
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				if(!$if_used){ //沒有已使用才可修改
					$this->session->set_userdata(array('Mes'=>'修改成功!'));
				}else{
					//$this->session->set_userdata(array('Mes'=>'該優惠代碼已有使用記錄，部份欄位無法修改!'));
				  unset($data_array['apply_TA']);
					$this->session->set_userdata(array('Mes'=>'修改成功(部份)!'));
				}
				$this->libraries_model->_update('ij_coupon_code_config',$data_array,'coupon_code_config_sn',$coupon_code_config_sn);
			}
			redirect(base_url('admin/marketing/ecouponItem/'.$coupon_code_config_sn));
		}

		$page_data = array();

		// 內容設定

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
	  $send_obj = $this->libraries_model->_select('ij_member_level_type_ct',array('display_flag'=>'1','member_level_type <'=>'9'),0,0,0,'member_level_type',1,'result_array');
		if($coupon_code_config_sn){
			$page_data['page_content']['Item'] = $this->libraries_model->_select('ij_coupon_code_config',array('status'=>'1','coupon_code_config_sn'=>$coupon_code_config_sn),0,0,0,'coupon_code_config_sn',0,'row');
			$page_data['page_content']['Item']['ifused'] = ($this->libraries_model->_select('ij_coupon_code',array('coupon_code_status'=>'2','coupon_code_config_sn'=>$coupon_code_config_sn),0,0,0,'coupon_code_config_sn',0,'num_rows'))? true:false;
		}
		if(@$page_data['page_content']['Item']['apply_TA']){
			$apply_TA_array=explode(';',$page_data['page_content']['Item']['apply_TA']);
		}else{
			$apply_TA_array=array('0','0','0');
		}
	  foreach($apply_TA_array as $key=>$_Item){
			if($_Item=='1'){
				$send_obj[$key]['checked']='1';
			}else{
				$send_obj[$key]['checked']='0';
			}
	  }
	  //var_dump($send_obj);
		$page_data['page_content']['send_obj']=$send_obj;
		$page_data['page_content']['view_path'] = 'admin/page_ads/ecoupon/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/ecoupon/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 虛擬折扣卷統計 Template
	 *
	 */
	public function ecouponReport($coupon_code_config_sn=0)
	{
		$_where=array('coupon_code_config_sn'=>$coupon_code_config_sn);
		if($coupon_code_config_sn){
      $coupon_codes=$this->libraries_model->get_coupon_codes($_where);
			$page_data = array();

			// 內容設定 $this->libraries_model->_select('ij_coupon_code',$_where,0,0,0,'last_time_update,create_date',1,'result_array')
			$types=array('0','type1: 一個優惠代碼，可以讓N個會員使用','type2: 每個優惠代碼只能用一次，不能重覆使用','type3: 一個會員只有能有一組優惠代碼，重覆使用');
			// general view setting
			$page_data['general_header'] = array(); // header 可以透過這個參數帶入
			$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

			$page_data['page_content']['Item'] = $this->libraries_model->_select('ij_coupon_code_config',array('status'=>'1','coupon_code_config_sn'=>$coupon_code_config_sn),0,0,0,'coupon_code_config_sn',0,'row');
			$page_data['page_content']['Item']['ifused'] = ($this->libraries_model->_select('ij_coupon_code',array('coupon_code_status'=>'2','coupon_code_config_sn'=>$coupon_code_config_sn),0,0,0,'coupon_code_config_sn',0,'num_rows'))? true:false;
			$page_data['page_content']['Item']['type_name'] =$types[$page_data['page_content']['Item']['coupon_code_type']];
      //統計筆數
			$page_data['page_content']['no_use'] = $this->libraries_model->_select('ij_coupon_code',array('coupon_code_status'=>'0','coupon_code_config_sn'=>$coupon_code_config_sn),0,0,0,'coupon_code_config_sn',0,'num_rows');
			$page_data['page_content']['send_out'] = $this->libraries_model->_select('ij_coupon_code',array('coupon_code_status'=>'1','coupon_code_config_sn'=>$coupon_code_config_sn),0,0,0,'coupon_code_config_sn',0,'num_rows');
			$page_data['page_content']['used'] = $this->libraries_model->_select('ij_coupon_code',array('coupon_code_status'=>'2','coupon_code_config_sn'=>$coupon_code_config_sn),0,0,0,'coupon_code_config_sn',0,'num_rows');

			//var_dump($coupon_codes);
			$page_data['page_content']['Items']=$coupon_codes;

			$page_data['page_content']['view_path'] = 'admin/page_ads/ecoupon/report'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要

			$page_data['page_level_js']['status'] = array(
															array('label' => '未發送', 'num' => 200),
															array('label' => '已發送', 'num' => 100),
															array('label' => '已使用', 'num' => 300),
															array('label' => '失效', 'num' => 100)
															);
			$page_data['page_level_js']['enable'] = array(
															array('label' => '尚未使用', 'num' => 300),
															array('label' => '已使用', 'num' => 400)
															);
			$page_data['page_level_js']['view_path'] = 'admin/page_ads/ecoupon/report_js'; // 頁面主內容的 JS view 位置, 必要
			$this->load->view('admin/general/main', $page_data);
		}else{
			$this->session->set_userdata(array('Mes'=>'查無此優惠代碼資料!'));
			redirect(base_url('admin/marketing/ecoupon'));
			exit();
		}
	}
	/**
	 * 匯入csv檔
	 *
	 */
		public function do_uploadcsv()
		{
			$config['allowed_types'] = 'csv';
			$config['max_size']	= '4000';
      $config['overwrite'] = false;
      $config['encrypt_name'] = TRUE;
      $coupon_code_config_sn=$this->input->post('coupon_code_config_sn',TRUE);
			if(!$coupon_code_config_sn){
       	$this->session->set_userdata('Mes', '很抱歉！優惠活動代碼沒有資料');
				redirect(base_url('admin/marketing/ecoupon'));
			}
			if(!is_dir(ADMINUPLOADPATH.'csv/'.$coupon_code_config_sn.'/')) mkdir(ADMINUPLOADPATH.'csv/'.$coupon_code_config_sn,0777);
      $config['upload_path']  = ADMINUPLOADPATH.'csv/'.$coupon_code_config_sn.'/' ;

			$this->load->library('upload',$config);

			if ( ! $this->upload->do_upload('csvfile'))
			{
				//$error = array('error' => $this->upload->display_errors());

       	$this->session->set_userdata('Mes', $this->upload->display_errors());
        redirect(base_url('admin/marketing/ecouponReport/'.$coupon_code_config_sn));
			}
			else
			{

				//$this->load->library('csvimport');
				$data = $this->upload->data();
				$file = $data['file_path'].$data['file_name'];
				//$fp = fopen("$file","r");
				//$myFile = file_get_contents($file);
		    //echo $file;
	//$this->load->library('Excel');
	//$data = $this->excel->excel_to_DB($file,8);
	$this->load->library('CSVReader');
  $result = array_map('str_getcsv', file($file));
  //$result = $this->csvreader->parse_file($file);
	//print_r($this->db->list_fields('ij_coupon_code'));
	$ij_coupon_code=$this->db->list_fields('ij_coupon_code');
	unset($ij_coupon_code[0]);
	//print_r($ij_coupon_code);
	//exit();

	$err='';$x=0;$y=0;$z=0;//x 為修改件數 y 為新增件數 z 為無進入資料庫物件
	foreach($result as $KI=>$_Item){
		//echo count($_Item);
		//exit();
		$temp=array();
		//判斷欄位數給 key
		if(count($_Item) == 18){
			unset($_Item[0]); //跳過第一個欄位因為是 auto
			$_Item[1] = $coupon_code_config_sn;
		}elseif(count($_Item) == 17){
			$_Item[0] = $coupon_code_config_sn;
			$ij_coupon_code = array_values($ij_coupon_code);
		}elseif(count($_Item) == 16){
			$_Item[16]=$coupon_code_config_sn;
	    unset($ij_coupon_code[1]);
			$ij_coupon_code = array_values($ij_coupon_code);
			$ij_coupon_code[16]='coupon_code_config_sn';
		}
		foreach($_Item as $KJ=>$j){
				//var_dump ($j);
			$temp[$ij_coupon_code[$KJ]] = $j;
		}
		//unset($_Item[1]); //跳過第2個欄位因為是 coupon_code_config_sn

		//var_dump($temp);
		//var_dump($this->input->post('category'));
		//exit();
		//echo $autoid.'<br>';
		//if($autoid!='' && $IDB){ 改一律新增
		//	$this->db->where('autoid',$autoid)->update('outlet',$temp);
		//	$x++;
		//}else
		if($temp){
			//var_dump($temp);
			$this->db->insert('ij_coupon_code',$temp);
			//exit();
			$y++;
		}else{
			$z++;
		}
	}
	$Mes = "本次上傳 $y 筆 失敗 $z 筆 ".$err;
	//if(is_file($file)){ unlink($file);}//刪除舊檔案
	//exit();

	$this->session->set_userdata(array('Mes'=>$Mes));
		//echo $err;
		//exit();
		//$this->session->set_userdata(array('err'=>substr($err,0,1025)));
        redirect(base_url('admin/marketing/ecouponReport/'.$coupon_code_config_sn));

			}
		}
	/**
	 * EDM列表 Template
	 *
	 */
	public function edm()
	{
		$page_data = array();
		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/edm/table'; // 頁面主內容 view 位置, 必要

		$edms=$this->libraries_model->_select('ij_edm',array('status'=>'1'),0,0,0,'last_time_update',1,'result_array');
		foreach($edms as $_key=>$_Item){
			//var_dump($this->libraries_model->get_edm_send_list(0,$_Item['edm_id'],1));
			$edms[$_key]['edm_open_amount']=$this->libraries_model->get_edm_send_list(0,$_Item['edm_sn'],1);
		}
		$page_data['page_content']['Items']=$edms;
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/edm/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * EDM編輯 Template
	 *
	 */
	public function edmItem($edm_sn=0)
	{
		if($data_array=$this->input->post('Item')){
      $rule = 'trim|required';  //共用規則
      $this->form_validation->set_rules('Item[edm_titile]', '電子報標題', $rule.'|max_length[200]');
      $this->form_validation->set_rules('Item[edm_content]', '內容', $rule);
      $this->form_validation->set_error_delimiters('', '\n');
      if($this->form_validation->run() === false){
        $this->load->library('ijw');
        $this->ijw->_wait(base_url('admin/marketing/edmItem/'.@$edm_sn),'2',validation_errors(),1);
				//var_dump($this->form_validation->run());
				//exit();
      }
			if(!$edm_sn=$this->input->post('edm_sn')){ //新增
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);//方便秀最後更新
				$data_array['status']='1';
				$data_array['send_total_amount']='0';
				//$data_array['associated_content_type_sn']=implode('、',$data_array['associated_content_type_sn']);
				//$data_array['post_category_sn_set']=implode('、',$data_array['post_category_sn_set']);
				$edm_sn=$this->libraries_model->_insert('ij_edm',$data_array);
				$this->session->set_userdata(array('Mes'=>'新增成功!'));
			}else{
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				//$data_array['banner_content_save_dir']=$this->input->post($this->input->post('banner_type_eng_name').'AD');
				//$data_array['associated_content_type_sn']=implode('、',$data_array['associated_content_type_sn']);
				//$data_array['post_category_sn_set']=implode('、',$data_array['post_category_sn_set']);
				$this->libraries_model->_update('ij_edm',$data_array,'edm_sn',$edm_sn);
				$this->session->set_userdata(array('Mes'=>'修改成功!'));
				//redirect(base_url('admin/marketing/contentBlog/'));
				//$banner_link=$this->input->post('banner_link');
			}
			redirect(base_url('admin/marketing/edmItem/'.$edm_sn));
		}
		$page_data = array();
		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/edm/item'; // 頁面主內容 view 位置, 必要
		if(@$edm_sn){
			$_where=array('status'=>'1','edm_sn'=>$edm_sn);
			$page_data['page_content']['Item']=$this->libraries_model->_select('ij_edm',$_where,0,0,0,'edm_sn',0,'row');
		}
		//var_dump($page_data['page_content']['Item']['edm_content']);
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/edm/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * EDM發送 Template
	 *
	 */
	public function edmSend($edm_sn,$edm_send_sn=0)
	{
		if($edm_sn){
			if($sendfilter=$this->input->post('sendfilter')){
				if($sendfilter!='send_member_level_type'){
					$data_array=$this->input->post($sendfilter);
				}else{
					//按會員發送
					$data_array['send_member_level_type']='9999';
				}
				//exit();
				$data_array['edm_id']=$this->input->post('edm_id');
				$data_array['memo']=$this->input->post('memo');
				$data_array['skip_cancel_edm_flag']=$this->input->post('skip_cancel_edm_flag');
				if(!$edm_send_sn=$this->input->post('edm_send_sn')){ //新增
					$data_array = $this->libraries_model->CheckUpdate($data_array,1);
					$data_array = $this->libraries_model->CheckUpdate($data_array,0);//方便秀最後更新
					$data_array['status']='1';
					$data_array['send_amount']='0';
					//$data_array['associated_content_type_sn']=implode('、',$data_array['associated_content_type_sn']);
					//$data_array['post_category_sn_set']=implode('、',$data_array['post_category_sn_set']);
					$edm_send_sn=$this->libraries_model->_insert('ij_edm_send_config ',$data_array);
					$this->session->set_userdata(array('Mes'=>'新增成功!'));
				}else{
					if($this->input->post('send_time')=='0000-00-00 00:00:00' || $this->input->post('send_time')==''){
						$data_array = $this->libraries_model->CheckUpdate($data_array,0);
						//$data_array['associated_content_type_sn']=implode('、',$data_array['associated_content_type_sn']);
						//$data_array['post_category_sn_set']=implode('、',$data_array['post_category_sn_set']);
				        //先清空原先的資料再update選擇的條件
				        $clear_array = array(
				            'send_member_level_type'=> '0',
				            'order_start_date'=> '',
				            'order_end_date'=> '',
				            'send_product_set'=> '',
				            'send_biz_domain_set'=> '',
				            'send_dealer_set'=> '',
				            'over_order_amount'=> '',
				            'wedding_year_month'=> '',
				            'wedding_start_date'=> '',
				            'wedding_end_date'=> '',
				        );
						$this->libraries_model->_update('ij_edm_send_config ',$clear_array,'edm_send_sn',$edm_send_sn);
						$this->libraries_model->_update('ij_edm_send_config ',$data_array,'edm_send_sn',$edm_send_sn);
						$this->session->set_userdata(array('Mes'=>'修改成功!'));
					}else{
						$this->session->set_userdata(array('Mes'=>'已發送無法修改!'));
					}
				}
				if($sendfilter=='send_member_level_type'){
					//一律先刪除再新增
					//$this->libraries_model->_delete('ij_edm_send_list_log',array('edm_send_id' => $edm_send_sn,'send_time'=>'0000-00-00 00:00:00'));
					//按會員發送，寫入 ij_edm_send_list_log
					$send_list=explode(",",$this->input->post($sendfilter)[$sendfilter]);
					$i=$j=0;
					foreach($send_list as $key=>$_Item){
						if(filter_var($_Item, FILTER_VALIDATE_EMAIL)){
							$send_list_log = array();
							if($member=$this->libraries_model->get_member_by_email($_Item,'member_sn,last_name,first_name,accept_edm_flag')){
				        //會員願意接收電子報或是僅發送給有訂閱者未勾選 skip_cancel_edm_flag =1
				        if($member['accept_edm_flag'] || $this->input->post('skip_cancel_edm_flag')){
				        	$send_list_log = array(
					            'edm_send_id'=> $edm_send_sn,
					            'associated_member_sn'=> $member['member_sn'],
					            'receiver_email'=> $_Item,
					            'receiver_last_name'=> $member['last_name'],
					            'receiver_first_name'=> $member['first_name']
				        	);
				        	$i++;
				        }
				      }else{
				        $send_list_log = array(
				            'edm_send_id'=> $edm_send_sn,
				            'receiver_email'=> $_Item
				        );
				        $j++;
				      }
		        //var_dump($send_list_log);
		        //exit();
				      if($send_list_log){

				      	//if($edm_send_list_sn=$this->libraries_model->_select('ij_edm_send_list_log',array('edm_send_id'=>$edm_send_sn,'receiver_email'=>$_Item),0,0,0,'edm_send_list_sn',1,'row','edm_send_list_sn')['edm_send_list_sn']){
								//	$this->libraries_model->_update('ij_edm_send_list_log ',$send_list_log,'edm_send_list_sn',$edm_send_list_sn);
				      	//}else{
						$this->libraries_model->_insert('ij_edm_send_list_log ',$send_list_log);
								//}
				      }
				    }
					}
					$this->session->set_userdata(array('Mes'=>$this->session->userdata('Mes').'符合會員Email筆數：'.$i.' 不符筆數：'.$j));
				}
				redirect(base_url('admin/marketing/edmSend/'.$edm_sn.'/'.$edm_send_sn));
			}

			$page_data = array();
			// 內容設定
			// general view setting
			$page_data['general_header'] = array(); // header 可以透過這個參數帶入
			$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
			$page_data['page_content']['view_path'] = 'admin/page_ads/edm/send'; // 頁面主內容 view 位置, 必要
			$page_data['page_content']['edm_sn'] = $edm_sn;
			$page_data['page_content']['edm_title'] = $this->libraries_model->_select('ij_edm',array('edm_sn'=>$edm_sn),0,0,0,'edm_sn',0,'row','edm_titile')['edm_titile'];
			$page_data['page_content']['edm_send_sn'] = $edm_send_sn;

			//產業屬性
			$_where=" where status='1' ";
			$this->load->model('sean_models/Sean_db_tools');
			$biz_domains=$this->Sean_db_tools->db_get_max_record("*","ij_biz_domain",$_where);
			$_biz_domain_list='';
			foreach($biz_domains as $_Item){
				$_biz_domain_list.='"'.$_Item->domain_name.'",';
			}
			$page_data['page_content']['_biz_domain_list']=$_biz_domain_list;
			if(@$edm_send_sn){
				$_where=array('status'=>'1','edm_send_sn'=>$edm_send_sn);
				$edm_send_config=$this->libraries_model->_select('ij_edm_send_config',$_where,0,0,0,'edm_send_sn',0,'row');
				if(!$edm_send_config){
					$this->session->set_userdata(array('Mes'=>'查無此發送條件!'));
					redirect(base_url('admin/marketing/edmSend/'.$edm_sn));
				}
				//按會員發送
				if($edm_send_config['send_member_level_type']=='9999'){
					$edm_send_config['edm_send_lists']=$this->libraries_model->get_edm_send_list($edm_send_sn,$edm_sn);
				}
				$page_data['page_content']['Item']=$edm_send_config;
			}
			//發送條件列表
			$page_data['page_content']['Items']=$this->libraries_model->get_edm_send_config($edm_sn,0);
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'admin/page_ads/edm/send_js'; // 頁面主內容的 JS view 位置, 必要
			$this->load->view('admin/general/main', $page_data);
		}else{
			$this->session->set_userdata(array('Mes'=>'查無此電子報!'));
			redirect(base_url('admin/marketing/edm'));
		}
	}

	/**
	 * EDM發送名單 Template
	 *
	 */
	public function edmSendList($edm_sn=0,$edm_send_sn=0)
	{
		if($edm_sn && $edm_send_sn){
			$page_data = array();
			// 內容設定
			// general view setting
			$page_data['general_header'] = array(); // header 可以透過這個參數帶入
			$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
			$page_data['page_content']['view_path'] = 'admin/page_ads/edm/send_list'; // 頁面主內容 view 位置, 必要
			//寄送條件
			$page_data['page_content']['Item']=$this->libraries_model->get_edm_send_config($edm_sn,$edm_send_sn)[0];
			$this->libraries_model->get_edm_send_config_list_send($page_data['page_content']['Item']);
			//寄送名單
			$page_data['page_content']['Items']=$this->libraries_model->get_edm_send_list($edm_send_sn,$edm_sn);
			$page_data['page_content']['edm_sn']=$edm_sn;
			$page_data['page_content']['edm_send_sn']=$edm_send_sn;
			//var_dump($page_data['page_content']['Items']);
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'admin/page_ads/edm/send_list_js'; // 頁面主內容的 JS view 位置, 必要
			$this->load->view('admin/general/main', $page_data);
		}else{
			$this->session->set_userdata(array('Mes'=>'查無此EDM寄送名單!'));
			redirect(base_url('admin/marketing/edm'));
		}
	}
	public function get_edm_send_list(){
		if ($this->input->is_ajax_request()) {
			$table=base64_decode($this->input->post('dtable',true));
			$field=base64_decode($this->input->post('dfield',true));
			$id=$this->input->post('did',true);
			if($table='ij_edm_send_config' && $field='edm_send_sn' && $id){
				if($array=$this->libraries_model->get_edm_send_config(0,$id)){
					if($result=$this->libraries_model->get_edm_send_config_list_send($array[0],1)){
						//echo '已成功取得寄送名單（'.$result.'），排程發送中';
						echo '已成功寄送（'.$result.'）';
					}else{
						echo '很抱歉！無法取得寄送名單，請確認條件是否正確（同 EDM + 信箱會被過濾）！';
					}
				}else{
					echo '很抱歉！查無該寄送條件！';
				}
			}else{
				echo '很抱歉！寄送資料不足！';
			}
		}else{
			echo false;
		}
	}
	public function adsScheduleItem($banner_content_sn,$banner_schedule_sn=0)
	{
		if($data_array=$this->input->post('Schedule')){
			//var_dump($this->input->post('banner_type_eng_name'));
			//exit();
			if(!$banner_schedule_sn=$this->input->post('banner_schedule_sn')){ //新增
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);//方便秀最後更新
				$data_array['status']='1';
				//if($data_array['banner_type'])
				$data_array['click_amount']='0';
				$data_array['view_amount']='0';

				//$data_array['associated_content_type_sn']=implode('、',$data_array['associated_content_type_sn']);
				//$data_array['post_category_sn_set']=implode('、',$data_array['post_category_sn_set']);
				$banner_schedule_sn=$this->libraries_model->_insert('ij_banner_schedule_log',$data_array);
				$this->session->set_userdata(array('Mes'=>'新增成功!'));
			}else{
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				//$data_array['banner_content_save_dir']=$this->input->post($this->input->post('banner_type_eng_name').'AD');
				//$data_array['associated_content_type_sn']=implode('、',$data_array['associated_content_type_sn']);
				//$data_array['post_category_sn_set']=implode('、',$data_array['post_category_sn_set']);
				$this->libraries_model->_update('ij_banner_schedule_log',$data_array,'banner_schedule_sn',$banner_schedule_sn);
				$this->session->set_userdata(array('Mes'=>'修改成功!'));
			}
      /*if (!empty($_FILES['banner']['name'])) {
					//$this->load->library('upload');
          $config['upload_path']  = ADMINUPLOADPATH.'ad/' ;
          $config['allowed_types']= 'gif|jpg|png|jpeg';
          $config['max_size']     = '4196';
          $config['max_width']    = '1920';
          $config['max_height']   = '1920';
          $config['encrypt_name']   = true;

          $this->load->library('upload', $config);
					if(!is_dir(ADMINUPLOADPATH.'ad/')) mkdir(ADMINUPLOADPATH.'ad/',0777);

					//$this->upload->initialize($config);
          if (!$this->upload->do_upload('banner')) {
              //$error = array('error' => $this->upload->display_errors());
							$this->session->set_userdata(array('Mes'=>$this->session->userdata('Mes').' '.strip_tags($this->upload->display_errors())));
          		//var_dump($error);
					}else{
              $img	 = $this->upload->data();
							$this->libraries_model->_update('ij_banner_content',array('banner_content_save_dir'=>'ad/'.$img['file_name']),'banner_schedule_sn',$banner_schedule_sn);
							//var_dump($img);
              if(is_file(ADMINUPLOADPATH.$banner_content_save_dir)) unlink(ADMINUPLOADPATH.$banner_content_save_dir);
          }
          //exit();
      }*/
			redirect(base_url('admin/marketing/adsScheduleItem/'.$banner_content_sn.'/'.$banner_schedule_sn));
		}




		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/schedule/item'; // 頁面主內容 view 位置, 必要
		//$page_data['page_content']['Item']=$this->libraries_model->_select('ij_banner_location','banner_location_sn',$banner_location_sn,0,0,0,0,'row');
		$page_data['page_content']['Schedule']=$this->libraries_model->_get_adsSchedules($banner_schedule_sn);
		$page_data['page_content']['banner_content_name']=$this->libraries_model->_get_adsMaterials($banner_content_sn)['banner_content_name'];
		$page_data['page_content']['banner_content_sn']=$banner_content_sn;
		//var_dump($banner_schedule_sn);
		$where=array('display_flag'=>'1');
		$page_data['page_content']['published_locaiotn_types']=$this->libraries_model->_select('ij_published_locaiotn_type_ct',$where,0,0,0,0,0,'result_array'); //頻道
		$page_data['page_content']['banner_types']=$this->libraries_model->_select('ij_banner_type_ct',$where,0,0,0,0,0,'result_array'); //型態
		$page_data['page_content']['link_types']=$this->libraries_model->_select('ij_banner_link_type_ct',$where,0,0,0,0,0,'result_array'); //連結類型
		$page_data['page_content']['banner_contents']=$this->libraries_model->_select('ij_banner_content',0,0,0,0,'sort_order',0,'result_array'); //素材

		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/schedule/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}
}