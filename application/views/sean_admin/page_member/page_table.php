<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<style>
  .select2_dealer,.select3_dealer{height:28px;}
</style>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>會員管理 <small>列表與搜尋</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">一般會員列表</span>
            </div>
            <div class="actions btn-set">
              <a href="<?php echo $_action_link;?>" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <form method="post" name="member_table" action="admin/Member/memberTable/search">
          <div class="portlet-body">
            <div class="note note-warning" id="search_filter">
              <div class="form-group">
                <div class="row">
                  <div class="col-md-4">
                    <div class="input-group input-large date-picker input-daterange" data-date="2015-05-07" data-date-format="yyyy-mm-dd">
                      <span class="input-group-addon" onclick="alert('Start Search');"> 結婚日期 </span>
                      <input type="text" class="form-control input-sm date-picker" name="wedding_date_from" value="">
                      <span class="input-group-addon"> 到 </span>
                      <input type="text" class="form-control input-sm date-picker" name="wedding_date_to" value="">
                    </div>
                  </div>
                  <div class="col-md-8">
                  	<input id="user_name" name="user_name" type="text" class="pagination-panel-input form-control input-inline input-sm" size="10" placeholder="會員帳號">
                <input id="last_name" name="last_name" type="text" class="pagination-panel-input form-control input-inline input-sm" size="8" style="margin: 0 5px;" placeholder="姓名">
                <!--input id="first_name" name="first_name" type="text" class="pagination-panel-input form-control input-inline input-sm" size="8" style="margin: 0 5px;" placeholder="名"-->
                <select id="addr_city_code" name="addr_city_code"  class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;">
                  <option value="">縣市</option>
                  <?php
       					    //$_city_list=$this->config->item("city_ct");
                 while (list($keys, $values) = each ($city_ct)) {
    							echo  ' <option value="'.$keys.'">'.$values.'</option>';

								}

                ?>
                </select>
                <select  id="addr_town_code" name="addr_town_code" class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;">
                  <option value="">鄉鎮市區</option>
                </select>
                <!-- <input type="text" id="addr1" name="addr1"  class="pagination-panel-input form-control input-inline input-sm" size="12" style="margin: 0 5px;" placeholder="住址"> -->

                <select id="member_status" name="member_status" class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;">
                  <option value="">帳號狀態</option>
                  <option value="1">已註冊,未驗證成功</option>
                  <option value="2">已驗證成功</option>
                  <option value="3">列入黑名單中</option>
                  <option value="4">失效</option>
                </select>
                <button type="submit" class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button>
                <button id="reset_form" onclick="this.form.flg.value='reset';" class="btn btn-sm purple"><i class="fa"></i> 清除</button>

                  </div>
                 <!--
                  <div class="col-md-3">
                    <input type="hidden" class="form-control input-sm select3_dealer" value="" placeholder="關聯廠商">
                  </div>
                  <div class="col-md-3">
                    <input type="hidden" class="form-control input-sm select2_dealer" value="婚紗1" placeholder="請選擇產業屬性">
                  </div>
                  -->
                </div>
              </div>
              <div style="clear:both;"></div>
            </div>
          </form>
            <!-- End of search_filter -->
            <?php $this->load->view('sean_admin/general/flash_error');?>
            <table class="table table-striped table-bordered table-hover" name="table_member" id="table_member">
              <thead>
                <tr>
                  <th>姓名</th>
                  <th>Email</th>
                  <th>縣市</th>
                  <th>鄉鎮市區</th>
                  <!-- <th>住址</th-->
                  <th>手機</th>
                  <th>結婚日期</th>
                  <th>會員身分</th>
                  <th>關聯廠商</th>
                  <th>帳號狀態</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
               <?php
                 $_member_status_ct=$this->config->item("member_status_ct");
								 //$_city_ct=$this->config->item("city_ct");
                if(is_array($_result01))
                	{
                 			foreach($_result01 as $key => $value)
                    		{

												  //$_town_ct_init="town_ct_".$value->addr_city_code;
												  //$_town_ct=$this->config->item($_town_ct_init);
                  ?>


                  <tr class="odd gradeX">
                    <td width="10%"><?php echo $value->last_name;?></td>
                    <!--td width="15%"><?php echo $value->openid_name;?></td-->
                    <td width="15%"><?php echo $value->email;?></td>
                    <td width="8%"><?php echo $value->city_name;?></td>
                    <td width="8%"><?php echo $value->town_name;?></td>
                   <!--   <td><?php echo $value->addr1;?></td>-->
                    <td width="10%"><?php echo $value->cell;?></td>
                    <td width="8%"><?php echo $value->wedding_date;?></td>
                    <td width="8%"><?php echo $value->member_level_type_name;?></td>
                    <td width="8%"><?php echo $value->member_dealer;?></td>
                    <td width="12%"><?php echo $_member_status_ct[$value->member_status];?> </td>
                    <td width="25%">
                      <a href="<?php echo $init_control;?>Member/memberTable_item/edit/<?php echo $value->member_sn;?>"><i class="fa fa-edit"></i>編輯</a>&nbsp;&nbsp;
                    <?php if($value->member_status<2){ ?>
                      <a href="<?php echo $init_control;?>Member/send_mail/<?php echo urlencode("會員驗證信");?>/<?php echo $value->member_sn;?>/<?php echo urlencode($value->user_name);?>" onclick="return sendmailchecked();"><i class="fa fa-circle-o-notch"></i>重發驗證信</a>&nbsp;&nbsp;
                     <?php } ?>
                      <a href="<?php echo $init_control;?>Member/send_mail/<?php echo urlencode("忘記密碼");?>/<?php echo $value->member_sn;?>" onclick="return sendmailchecked();"><i class="fa fa-lightbulb-o"></i>忘記密碼</a>
                    </td>
                  </tr>
                <?php } } ?>

              </tbody>
            </table>

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->