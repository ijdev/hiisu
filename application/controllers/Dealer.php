<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dealer extends MY_Controller {
	 public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('form_validation');
	    $this->load->model('Shop_model');
		$this->load->library("ijw");
	    $this->page_data=array();
		$this->page_data['cart']=$this->Shop_model->Get_Cart(); //購物車
		//$_category_array=$this->Shop_model->dealer_categoryParentChildTree('結婚商店',0,'','',''); //分類
		//$this->page_data['_category_array']=$this->Shop_model->get_banner($_category_array); //分類抓圖
		$data['Mes'] = $this->session->userdata('Mes');
		if($data['Mes']){
			$this->load->view('Message',$data);
			$this->session->set_userdata(array('Mes'=>''));
			$this->session->unset_userdata('Mes');
		}
		if($_SERVER['REQUEST_URI']!='/dealer/memberSignIn') $this->session->set_flashdata("HTTP_REFERER",base_url($_SERVER['REQUEST_URI']));
	}
	function _remap($CusU){
		$myhost = $_SERVER['HTTP_HOST'];$exc='N';

		$NURL = strtolower($myhost);
		if(ENVIRONMENT=='development'){
			$domain_retain='systemnet.tw';
		}else{
			//$domain_retain='jweb.tw';
			$domain_retain='qwedding.club';
		}
		//var_dump($NURL);
		//var_dump($domain_retain);
		//var_dump(strpos($NURL,$domain_retain));
		//exit;
		if(strpos($NURL,$domain_retain) !== false){
			$customize_website_url=explode('.',$NURL)[0];
			//var_dump($customize_website_url);exit;
			if($customize_website_url!='www' && $customize_website_url!='qwedding'){
				//echo $customize_website_url;
				$where['customize_website_url'] = $customize_website_url;
				//$where['dealer_status'] = '1';
				$exc = 'Y';
				$row = $this->Shop_model->_select('ij_dealer',$where,0,0,0,'dealer_sn',0,'row');
				//echo $this->db->last_query();
				//exit();
			}else{
				redirect('http://hiisu.shop');
			}
		}else{
			$wa['customize_website_url'] = $NURL;
			//$wa['dealer_status'] = '1';
			$row = $this->Shop_model->_select('ij_dealer',$wa,0,0,0,'dealer_sn',0,'row');
		}
		//var_dump($row);
		$exc=='Y'?$fun=strtolower($this->uri->segment(2)):$fun=strtolower($this->uri->segment(3));
		$exc=='Y'?$var=$this->uri->segment(3):$fun=$this->uri->segment(4);
		//var_dump($this->uri->segment(3));
		if($row){
			$dealer_sn = $row['dealer_sn'];
			$this->page_data['dealer_sn']=$dealer_sn;
			$this->page_data['_category_array']=$this->Shop_model->dealer_categoryParentChildTree($dealer_sn,0,'','','');
		    $this->load->model('libraries_model');
    		$this->page_data['content_types']=$this->libraries_model->_get_blog_faq_categorys(1,'首頁',1);
    		//紀錄
 			$this->session->set_userdata('dealer_sn', $dealer_sn);
			$this->load->helper('cookie');
			setcookie(base64_encode('dealer_sn'),base64_encode($dealer_sn),time()+30*60*60,'/');

			$this->page_data['dealer']=$row;
			$dealer_status = $row['dealer_status'];
			//var_dump($dealer_status);
			//exit();

			if($dealer_status=='1'){
				//$this->load->helper('cookie');
				if($fun==''){
					$this->index();
				}else{
					$NPage = array('profile','about','contactus','album','bloglist','blogitem','blogkeyword','shopcatalog','shopitem','membersignin','membersignup','memberforgotpassword');
					if(!in_array(strtolower($fun),$NPage))show_404();
					$this->$fun($var);
				}
			}else{
				$this->session->set_userdata(array('Mes'=>'很抱歉！該經銷商檔案關閉中!'));
				redirect('http://hiisu.shop');
				exit();
			}
		}else{
			//var_dump($row);
			//exit();

			$this->session->set_userdata(array('Mes'=>'很抱歉！查無此經銷商資料1!'));
			//redirect(base_url('/'));
			redirect('http://hiisu.shop');
			exit();
		}
		//echo $this->uri->segment(2).'<br>';
		//echo $this->uri->segment(3).'<br>';
		//if(!$page=$this->uri->segment(2)) $page='home';
		//$this->index($page,$this->uri->segment(3));
	}

	/**
	 * 經銷商頁面 Template
	 *
	 */
	public function index()
	{
		//echo $_SERVER['REQUEST_URI'];
		$page_data = array();
		$page_data['soical_media'] = $this->Shop_model->_select('ij_dealer_soical_media_info','dealer_sn',$this->page_data['dealer_sn'],0,0,'soical_media_name',0,'result_array');
		$page_data['slider'] = explode(':',substr($this->page_data['dealer']['page_home_slider_save_dir'], 0, -1));
		$page_data['profile'] = explode(':',substr($this->page_data['dealer']['page_profile_pic_save_dir'], 0, -1));

		$page_data['mainContain'] = $this->load->view('www/page_dealer/home', $page_data, true);
		$this->load->view('www/page_dealer/template', $page_data);
	}
	public function profile()
	{
		$page_data = array();
		$page_data['soical_media'] = $this->Shop_model->_select('ij_dealer_soical_media_info','dealer_sn',$this->page_data['dealer_sn'],0,0,'soical_media_name',0,'result_array');
		$page_data['wedding_websites'] = $this->Shop_model->_get_wedding_websites_by_dealer_sn($this->page_data['dealer_sn']);
		//var_dump($page_data['wedding_websites']);
		$page_data['mainContain'] = $this->load->view('www/page_dealer/profile', $page_data, true);
		$page_data['profile'] = explode(':',substr($this->page_data['dealer']['page_profile_pic_save_dir'], 0, -1));
		$this->load->view('www/page_dealer/template', $page_data);
	}
	public function about()
	{
		$page_data = array();
		$page_data['dealer']=$this->page_data['dealer'];
		$page_data['profile'] = explode(':',substr($this->page_data['dealer']['page_profile_pic_save_dir'], 0, -1));
		$page_data['banner'] = explode(':',substr($this->page_data['dealer']['page_cover_photo_save_dir'], 0, -1));
   		if($this->page_data['dealer']['page_facebook_id']){
			//echo $this->page_data['dealer']['page_facebook_id'];
			$articles=$this->ijw->get_fb_fans_article($this->page_data['dealer']['page_facebook_id']);
			if(@$articles['posts']['data']){
				$page_data['articles']=$articles['posts']['data'];
			}else{
				$page_data['articles']=array();
			}
    	//var_dump($articles);
		}else{
		}
		/*$fql = 'SELECT post_id, actor_id, target_id, message, type FROM stream WHERE source_id = '.$page_id.' LIMIT 50';
    $ret = $this->api_facebook( array(
						'method' => 'fql.query',
						'query' => $fql
					));
		exit();*/

		$page_data['aboutus'] = explode(':',substr($this->page_data['dealer']['page_aboutus_pic_save_dir'], 0, -1));
		$page_data['aboutus_description'] = explode(':::',$this->page_data['dealer']['page_aboutus_description']);
		$page_data['soical_media'] = $this->Shop_model->_select('ij_dealer_soical_media_info','dealer_sn',$this->page_data['dealer_sn'],0,0,'soical_media_name',0,'result_array');

		$page_data['mainContain'] = $this->load->view('www/page_dealer/about', $page_data, true);
		$this->load->view('www/page_dealer/template', $page_data);
	}
	public function contactus()
	{
		if($contact = $this->input->post('contact')){
			//var_dump($this->input->post('contact'));
			//exit();
			$data_array['message_content']='專頁聯絡我們填寫者資料：<br>姓名：'.$contact['user_name'].'<br>Email：'.$contact['user_email'].'<br>詢問的作品編號：'.$contact['website_id'].'<br>訊息內容：'.$contact['message_content'];
			$data_array['message_subject']='專頁聯絡我們主題：'.$contact['message_subject'];
			$data_array['transmit_date']=date("Y-m-d H:i:s",time());
			$data_array['default_display_time']=date("Y-m-d H:i:s",time());
			$data_array['send_count']=1;
			$data_array['message_status']='1';
			$data_array['main_config_flag']='1'; //新增主訊息為1
			$data_array['member_sn']=$contact['member_sn'];
			$this->load->library("ijw");
			if($this->ijw->send_mail('系統訊息',"",$contact['member_sn'],$data_array['message_content'])){
				$data_array['send_email_flag']='1';
			}
			//$data_array['notify_ta']=implode(':',$notify_ta);
			$data_array = $this->Shop_model->CheckUpdate($data_array,1);
			$member_message_center_sn=$this->Shop_model->_insert('ij_member_message_center_log',$data_array);
			if($member_message_center_sn){
				echo 'success';
				exit();
			}else{
				echo 'fail';
				exit();
			}
		}
		$page_data = array();
		$page_data['dealer']=$this->page_data['dealer'];
		$page_data['member_sn'] = $this->Shop_model->_select('ij_member','associated_dealer_sn',$this->page_data['dealer_sn'],0,0,'member_sn',0,'row','member_sn',0,0,0)['member_sn'];
		$page_data['mainContain'] = $this->load->view('www/page_dealer/contactus', $page_data, true);
		$page_data['soical_media'] = $this->Shop_model->_select('ij_dealer_soical_media_info','dealer_sn',$this->page_data['dealer_sn'],0,0,'soical_media_name',0,'result_array');
		$page_data['profile'] = explode(':',substr($this->page_data['dealer']['page_profile_pic_save_dir'], 0, -1));
		$this->load->view('www/page_dealer/template', $page_data);
	}
	public function album()
	{
		$wedding_website_sn= intval($this->uri->segment(3));

		$page_data = array();
		$page_data['profile'] = explode(':',substr($this->page_data['dealer']['page_profile_pic_save_dir'], 0, -1));
	    $this->load->model('wedding_album');
        $page_data['album_photos'] = $this->wedding_album->get_result((int)$wedding_website_sn);
	    //var_dump($wedding_website_sn);
	    //var_dump($page_data['album_photos']);
	    /*if($this->page_data['dealer']['page_facebook_id']){
			$this->load->library("ijw");
			//echo $this->page_data['dealer']['page_facebook_url_set'];
			$articles=$this->ijw->get_fb_fans_article($this->page_data['dealer']['page_facebook_id']);
			if($articles['posts']['data']){
				$page_data['articles']=$articles['posts']['data'];
			}else{
				$page_data['articles']=array();
			}
    	print_r($articles['posts']['data']);
		}else{
		}	*/
		$page_data['mainContain'] = $this->load->view('www/page_dealer/album2', $page_data, true);
		$this->load->view('www/page_dealer/template', $page_data);
	}
	/**
	 * 婚禮情報文章列表 by分類
	 *
	 */
	public function blogList($content_type='all')
	{
		$page_data = array();
		$page_data['profile'] = explode(':',substr($this->page_data['dealer']['page_profile_pic_save_dir'], 0, -1));
		$page_data['soical_media'] = $this->Shop_model->_select('ij_dealer_soical_media_info','dealer_sn',$this->page_data['dealer_sn'],0,0,'soical_media_name',0,'result_array');

		$content_type=urldecode($content_type);
		//echo $content_type;
		$search=$this->input->get('search',true);
		// 內容設定
		if(!$search){
			$search_array='0';
		}else{
			$search_array=array('blog_article_titile'=>$search,'keyword'=>$search,'blog_article_content'=>$search);
		}
		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		$page=($this->input->get('page',true))? $this->input->get('page',true):'1';
		$this->load->library('pagination');
		$this->load->library('ijw');
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		if($content_type!='all'){
			$like_field='associated_content_type_sn';
			$content_type_sn=$content_type;
		}else{
			$like_field='';
			$content_type_sn='';
		}
		//var_dump($search_array);
		$this->load->model('libraries_model');
		$page_data['page_content']['total'] = count($this->libraries_model->_get_blog_faq('ij_blog_article',array('status'=>'1','post_flag'=>'1','relation_status'=>'1'),$like_field,$content_type_sn,$search_array));
		$page_data['page_content']['perpage'] = 5;
		if($page_data['page_content']['total']==0) $page=0;
		$page_data['page_content']['start'] = ($page > 1)? ($page-1)*$page_data['page_content']['perpage']:'0';
		$page_data['page_content']['end'] = ($page)? $page*$page_data['page_content']['perpage']:'0';
		$config = $this->ijw->_pagination('dealer/blogList/'.$content_type.'/', $page_data['page_content']['total'],$page_data['page_content']['perpage']);
		$this->pagination->initialize($config);

		$page_data['page_content']['Items']=$this->libraries_model->_get_blog_faq('ij_blog_article',array('status'=>'1','post_flag'=>'1','relation_status'=>'1'),$like_field,$content_type_sn,$search_array,$page_data['page_content']['start'],$page_data['page_content']['perpage']);
    	$page_data['page_content']['page_links'] = $this->pagination->create_links();
		$page_data['page_content']['content_types']=$this->libraries_model->_get_blog_faq_categorys(1,'首頁',1);
		//var_dump($page_data['page_content']['content_types']);
		$page_data['page_content']['content_type']=urldecode($content_type);
		$page_data['title']=$search.'-'.$content_type_sn;
		$page_data['page_content']['search']=$search;
		$page_data['page_content']['keyword_tag']='';
		//$page_data['page_content']['view_path'] = 'www/page_blog/list'; // 頁面主內容 view 位置, 必要
		//$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		//$page_data['page_level_js']['view_path'] = 'www/page_blog/list_js'; // 頁面主內容的 JS view 位置, 必要
		$page_data['mainContain'] = $this->load->view('www/page_dealer/page_blog/list', $page_data['page_content'], true);
		//echo $page_data['mainContain'];exit;
		$this->load->view('www/page_dealer/template', $page_data);
	}

	/**
	 * 婚禮情報文章列表 by keyword
	 *
	 */
	public function blogKeyword($keyword_tag)
	{
		if(!$keyword_tag) redirect(base_url('dealer/blogList'));
		$keyword_tag=urldecode($keyword_tag);
		$page_data = array();
		$search=$this->input->get('search',true);
		if(!$search){
			$search_array='0';
		}else{
			$search_array=array('blog_article_titile'=>$search,'keyword'=>$search,'blog_article_content'=>$search);
		}
		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['profile'] = explode(':',substr($this->page_data['dealer']['page_profile_pic_save_dir'], 0, -1));
		$page_data['soical_media'] = $this->Shop_model->_select('ij_dealer_soical_media_info','dealer_sn',$this->page_data['dealer_sn'],0,0,'soical_media_name',0,'result_array');

		$page=($this->input->get('page',true))? $this->input->get('page',true):'1';
		$this->load->library('pagination');
		$this->load->library('ijw');
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		if($keyword_tag!='all'){
			$like_field='keyword';
			$content_type_sn=$keyword_tag;
		}else{
			$like_field='';
			$content_type_sn='';
		}
		$this->load->model('libraries_model');
		$page_data['page_content']['total'] = $this->libraries_model->_get_blog_faq('ij_blog_article',array('status'=>'1','post_flag'=>'1','relation_status'=>'1'),$like_field,$content_type_sn,$search_array,0,0,1);
		$page_data['page_content']['perpage'] = 5;
		if($page_data['page_content']['total']==0) $page=0;
		$page_data['page_content']['start'] = ($page > 1)? ($page-1)*$page_data['page_content']['perpage']:'0';
		$page_data['page_content']['end'] = ($page)? $page*$page_data['page_content']['perpage']:'0';
		$config = $this->ijw->_pagination('dealer/blogKeyword/'.$keyword_tag.'/', $page_data['page_content']['total'],$page_data['page_content']['perpage']);
		$this->pagination->initialize($config);

		$page_data['page_content']['Items']=$this->libraries_model->_get_blog_faq('ij_blog_article',array('status'=>'1','post_flag'=>'1','relation_status'=>'1'),$like_field,$content_type_sn,$search_array,$page_data['page_content']['start'],$page_data['page_content']['perpage']);
        $page_data['page_content']['page_links'] = $this->pagination->create_links();
		$page_data['page_content']['content_types']=$this->libraries_model->_get_blog_faq_categorys(1,'首頁',1);
		$page_data['page_content']['content_type']='';
		$page_data['page_content']['keyword_tag']=$keyword_tag;
		$page_data['title']=$search.'-'.$keyword_tag;
		$page_data['page_content']['search']=$search;

		//$page_data['page_content']['view_path'] = 'www/page_blog/list'; // 頁面主內容 view 位置, 必要
		//$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		//$page_data['page_level_js']['view_path'] = 'www/page_blog/list_js'; // 頁面主內容的 JS view 位置, 必要
		$page_data['mainContain'] = $this->load->view('www/page_dealer/page_blog/list', $page_data['page_content'], true);
		$this->load->view('www/page_dealer/template', $page_data);
	}
	/**
	 * 婚禮情報文章頁面
	 *
	 */
	public function blogItem($blog_article_sn=0)
	{
		if(!$blog_article_sn) redirect(base_url('dealer/blogList'));

		$page_data = array();
		$search=$this->input->get('search',true);
		if(!$search){
			$search_array='0';
		}else{
			$search_array=array('blog_article_titile'=>$search,'keyword'=>$search,'blog_article_content'=>$search);
		}
		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['profile'] = explode(':',substr($this->page_data['dealer']['page_profile_pic_save_dir'], 0, -1));
		$page_data['soical_media'] = $this->Shop_model->_select('ij_dealer_soical_media_info','dealer_sn',$this->page_data['dealer_sn'],0,0,'soical_media_name',0,'result_array');

		$this->load->model('libraries_model');
		$_where=array('status'=>'1','post_flag'=>'1','blog_article_sn'=>$blog_article_sn);
		$page_data['page_content']['Item']=$this->libraries_model->_select('ij_blog_article',$_where,0,0,0,0,0,'row');
		if(!$page_data['page_content']['Item']){
			$this->session->set_userdata(array('Mes'=>'很抱歉!查無該文章或該文章並未開放!'));
		    redirect(base_url('dealer/blogList'));
		}else{
			$_where=array('relation_status'=>'1','blog_article_sn'=>$blog_article_sn);
			if(!$this->libraries_model->_select('ij_blog_relation',$_where,0,0,0,'blog_article_sn',0,'row')){
				$this->session->set_userdata(array('Mes'=>'很抱歉!查無該文章或該文章並未開放!'));
			    redirect(base_url('dealer/blogList'));
			}
		}
		//for sidebar
		$page_data['page_content']['content_types']=$this->libraries_model->_get_blog_faq_categorys(1,'首頁',1);
		//$like_field='associated_content_type_sn';
		//$content_type_sn=$page_data['page_content']['Item']['associated_content_type_sn'];
		//$page_data['page_content']['Items']=$this->libraries_model->_get_blog_faq('ij_blog_article',array('status'=>'1','post_flag'=>'1'));
		$page_data['page_content']['Items'][0]['keywords']=explode(chr(13).chr(10),trim($page_data['page_content']['Item']['keyword']));
		$page_data['page_content']['content_type']=$page_data['page_content']['Item']['associated_content_type_sn'];
		$page_data['page_content']['keyword_tag']='';
		$page_data['page_content']['search']=$search;

		//$page_data['page_content']['view_path'] = 'www/page_blog/item'; // 頁面主內容 view 位置, 必要
		//$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		//$page_data['page_level_js']['view_path'] = 'www/page_blog/item_js'; // 頁面主內容的 JS view 位置, 必要
		$page_data['mainContain'] = $this->load->view('www/page_dealer/page_blog/item', $page_data['page_content'], true);
		$this->load->view('www/page_dealer/template', $page_data);
	}
	/**
	 * 線上商店分類頁 Template
	 *
	 */
	public function shopCatalog($_id="")
	{
		$this->load->model('sean_models/Sean_db_tools');
		if($_id){
			$sort=$this->input->get('sort',true);
			//$limit=$this->input->get('limit',true);explode
			$attribute_values=$this->input->get('attribute_value_sn[]',true);
			$pricerange=$this->input->get('pricerange',true);
			$pricerange_array=explode('-',str_replace('$','',$pricerange));
			if (count($pricerange_array)==2){
				$min_price=$pricerange_array[0];
				$max_price=$pricerange_array[1];
			}else{
				$products=$this->Shop_model->_get_products('結婚商店',0,$_id,0,0,'price_ASC');
				if($products){
					$min_price=($products[0]['min_price'])? $products[0]['min_price']:'0';
					$max_price=($products[count($products)-1]['min_price'])? $products[count($products)-1]['min_price']:'0';
				}else{
					$this->session->set_userdata(array('Mes'=>'很抱歉!該分類尚無開放商品!'));
				    redirect(base_url('dealer'));
				}
			}
			//echo $min_price;
			//echo $max_price;
			$page=$this->input->get('page',true);
			$page_data = array();
			// 內容設定
			$page_data['_key_id']=$_id;
			// page level setting
			// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設
			$page_data['title_wrapper'] = 'www/page_shop/title_wrapper'; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，非必要
			$dealer_sn=$this->page_data['dealer_sn'];
			$_where=" left join ij_category_relation on ij_category_relation.category_sn=ij_category.category_sn where dealer_sn=$dealer_sn and relation_status='1'";
			$_category_array=$this->Sean_db_tools->db_get_max_record("*","ij_category",$_where);
		    $page_data['_category_array']=$_category_array;

			$_where=" where upper_category_sn='".$_id."' or category_sn='".$_id."' ";
			$_category_array=$this->Sean_db_tools->db_get_max_record("*","ij_category",$_where);
			$page_data['_category_array_left']=$_category_array;

		  //找出館別分類編號
			/*if($page_data['_category_array_left'][0]->upper_category_sn=='0'){
				$top_cate_id=$page_data['_category_array_left'][0]->category_sn;
			}else{
				$top_cate_id=$page_data['_category_array_left'][0]->upper_category_sn;
				foreach($page_data['_category_array'] as $key => $value){
	        		if($value->category_sn == $top_cate_id && $value->category_status==1 && $value->upper_category_sn){
			       		$top_cate_id=$value->upper_category_sn;
			     	}
	        	}
			}
			//var_dump($_2_name_parent);
		  $page_data['top_cate_id']=$top_cate_id;*/
			//var_dump($top_cate_id);
		  //當前分類
		  $_where=" where category_sn='".$_id."' ";
		  $_category_array=$this->Sean_db_tools->db_get_max_record("*","ij_category",$_where);
		  $page_data['banner_desc']=$_category_array[0]->banner_desc;
		  $page_data['banner_save_dir']=$_category_array[0]->banner_save_dir;
		  $page_data['meta_title']=$_category_array[0]->meta_title;
		  $page_data['meta_keywords']=$_category_array[0]->meta_keywords;
		  $page_data['meta_content']=$_category_array[0]->meta_content;
		  //當前館別的下層分類
			$_down_category_array=$this->Shop_model->dealer_categoryParentChildTree($this->page_data['dealer_sn'],$_id,'','',''); //分類
		  	$page_data['down_category_array']=$_down_category_array;

			// general view setting
			$page_data['general_header'] = array(); // header 可以透過這個參數帶入
			$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
			$page_data['page_content']['category_attributes'] = $this->Shop_model->category_attributes($_id,$attribute_values);
			//推薦商品
			$page_data['page_content']['promo_product1']=$this->Shop_model->_get_products('結婚商店','1',$_id,3);
			//echo count($page_data['page_content']['promo_product1']);
			//熱賣商品
			$page_data['page_content']['promo_product2']=$this->Shop_model->_get_products('結婚商店','2',$_id,5);
			//特價商品
			$page_data['page_content']['promo_product3']=$this->Shop_model->_get_products('結婚商店','3',$_id,5);
			//熱門商品
			$page_data['page_content']['promo_product5']=$this->Shop_model->_get_products('結婚商店','5',$_id,5);

			//該分類商品
	    	$this->load->library('pagination');
			$page_data['page_content']['perpage'] = 15;
			$page_data['page_content']['total'] = count($this->Shop_model->_get_products('結婚商店',0,$_id,0,0,0,$attribute_values,@$min_price,@$max_price));
		    $config = $this->ijw->_pagination('shop/shopCatalog/'.$_id, $page_data['page_content']['total'],$page_data['page_content']['perpage'],'shop');
		    $this->pagination->initialize($config);

	    	$page_data['page_content']['page_links'] = $this->pagination->create_links();

			$page_data['page_content']['products']=$this->Shop_model->_get_products('結婚商店',0,$_id,$page_data['page_content']['perpage'],$page,$sort,$attribute_values,@$min_price,@$max_price);
			$page_data['page_content']['sort']=$sort;
			$page_data['page_content']['min_price']=@$min_price;
			$page_data['page_content']['max_price']=@$max_price;
			//分類文章
			$page_data['page_content']['blogs']=$this->libraries_model->_get_blog_faq('ij_blog_article',array('status'=>'1','post_flag'=>'1'),'post_category_sn_set',$_category_array[0]->category_name,0,5);
			//echo $this->db->last_query();

		    $page_data['profile'] = explode(':',substr($this->page_data['dealer']['page_profile_pic_save_dir'], 0, -1));
		    $page_data['soical_media'] = $this->Shop_model->_select('ij_dealer_soical_media_info','dealer_sn',$this->page_data['dealer_sn'],0,0,'soical_media_name',0,'result_array');
			$page_data['page_content']['view_path'] = 'www/page_shop/page_catalog'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'www/page_shop/page_catalog_js'; // 頁面主內容的 JS view 位置, 必要
		    $this->load->view('www/page_dealer/shop_template', $page_data);
		}else{
				$this->session->set_userdata(array('Mes'=>'請選擇分類!'));
			  redirect(base_url('shop'));
		}
	}

	/**
	 * 線上商店商品頁 Template
	 *
	 */
	public function shopItem($product_sn=0,$category_sn=0)
	{
			//var_dump($this->Shop_model->_get_product($product_sn));
			//exit();
		$this->load->model('sean_models/Sean_db_tools');

		if($product_sn && $product=$this->Shop_model->_get_product($product_sn)){
		    $this->load->helper('cookie');
			$this->ijw->_write_product_history($product_sn);
			$this->Shop_model->_update_field_amount('ij_product','product_sn',$product_sn,'click_number','+1');
			if($product['default_channel_name']=='婚禮網站'){
				redirect(base_url('home/eweddingItem/'.$product_sn));
			}
			//echo count(get_cookie('product_history[]'));
			$page_data = array();

			// 內容設定

			// page level setting
			// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設

			$page_data['_key_id']=$product['default_root_category_sn']; //商品主分類

			$_where=" where channel_name='".$product['default_channel_name']."' ";
			$_category_array=$this->Sean_db_tools->db_get_max_record("*","ij_category",$_where);
		  $page_data['_category_array']=$_category_array;

		  $_where=" where upper_category_sn='".$page_data['_key_id']."' or category_sn='".$page_data['_key_id']."' ";
		  $_category_array=$this->Sean_db_tools->db_get_max_record("*","ij_category",$_where);
		  $page_data['_category_array_left']=$_category_array;
		  $page_data['meta_title']=$product['meta_title'];
		  $page_data['meta_keywords']=$product['meta_keywords'];
		  $page_data['meta_content']=$product['meta_content'];

			// general view setting
			$page_data['general_header'] = array(); // header 可以透過這個參數帶入
			$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
			$page_data['page_content']['view_path'] = 'www/page_shop/page_item'; // 頁面主內容 view 位置, 必要
		    $page_data['page_content']['top_ad']=$this->Shop_model->get_adsMaterials('商品頁_上方腰帶廣告_中間全版');

			//熱賣商品
			$page_data['page_content']['promo_product2']=$this->Shop_model->_get_products($product['default_channel_name'],'2',$page_data['_key_id'],5);
			//特價商品
			$page_data['page_content']['promo_product3']=$this->Shop_model->_get_products($product['default_channel_name'],'3',$page_data['_key_id'],5);
			//熱門商品
			$page_data['page_content']['promo_product5']=$this->Shop_model->_get_products($product['default_channel_name'],'5',$page_data['_key_id'],5);
			//相關商品(同分類隨機)
			$page_data['page_content']['promo_product6']=$this->Shop_model->_get_products($product['default_channel_name'],'6',$page_data['_key_id'],0);
			//商品資料
			$page_data['page_content']['product']=$product;
			$page_data['page_content']['category_sn']=$category_sn;

		    $page_data['profile'] = explode(':',substr($this->page_data['dealer']['page_profile_pic_save_dir'], 0, -1));
		    $page_data['soical_media'] = $this->Shop_model->_select('ij_dealer_soical_media_info','dealer_sn',$this->page_data['dealer_sn'],0,0,'soical_media_name',0,'result_array');
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'www/page_shop/page_item_js'; // 頁面主內容的 JS view 位置, 必要
		    $this->load->view('www/page_dealer/shop_template', $page_data);
		}else{
				$this->session->set_userdata(array('Mes'=>'很抱歉!查無此商品，可能下架中，或尚未開賣!'));
			  redirect(base_url('shop'));
		}
	}
	/**
	 * 登入頁面 Template
	 *
	 */
	public function memberSignIn()
	{
		$page_data = array();
		//echo $this->session->userdata('HTTP_REFERER');
		 $page_data['login_url']="";
		// 內容設定
		 //echo $_SERVER['REQUEST_URI'];exit;
		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設
		$page_data['_captcha_img']=$this->captcha_img();

		if($this->session->flashdata("HTTP_REFERER")){
			$this->session->keep_flashdata("HTTP_REFERER"); //紀錄返回
			//}else{
		//	if(isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'],'memberSignIn')===false) $this->session->set_flashdata("HTTP_REFERER", $_SERVER['HTTP_REFERER']); //紀錄返回
		}
		//if($this->session->userdata('web_login')== true){
		//	redirect($this->session->flashdata("HTTP_REFERER"));
		//}
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['banner'] = explode(':',substr($this->page_data['dealer']['page_login_wallpaper_save_dir'], 0, -1));
		$page_data['profile'] = explode(':',substr($this->page_data['dealer']['page_profile_pic_save_dir'], 0, -1));
		$page_data['soical_media'] = $this->Shop_model->_select('ij_dealer_soical_media_info','dealer_sn',$this->page_data['dealer_sn'],0,0,'soical_media_name',0,'result_array');

		$page_data['mainContain'] = $this->load->view('www/page_dealer/sign_in', $page_data, true);
		$page_data['page_content']['view_path'] = 'www/page_dealer/sign_in'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_dealer/sign_in_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/page_dealer/template', $page_data);
	}
	/**
	 * 註冊頁面 Template
	 *
	 */
	public function memberSignUp()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_sign_up'; // 頁面主內容 view 位置, 必要
    	$page_data['_captcha_img']=$this->captcha_img();
		$page_data['banner'] = explode(':',substr($this->page_data['dealer']['page_login_wallpaper_save_dir'], 0, -1));
		$page_data['profile'] = explode(':',substr($this->page_data['dealer']['page_profile_pic_save_dir'], 0, -1));
		$page_data['soical_media'] = $this->Shop_model->_select('ij_dealer_soical_media_info','dealer_sn',$this->page_data['dealer_sn'],0,0,'soical_media_name',0,'result_array');

		$page_data['mainContain'] = $this->load->view('www/page_dealer/sign_up', $page_data, true);
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_dealer/sign_up_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/page_dealer/template', $page_data);
	}
	public function memberForgotPassword()
	{

        $_message='message';

        $_message_cap="";


        if ($this->input->post()){
        if ($this->input->post("rycode")!= $this->session->userdata('captcha'))
			{

				if ($this->input->get_post("rycode") != $this->session->userdata('captcha') && strlen($this->input->get_post("rycode")) > 0) {
                $_message_cap.="<font color=red size=\"3px\">驗證碼不符</font>";
            }
					 $this->session->set_flashdata($_message,$_message_cap);
            $data['post'] = $_POST;
           // $this->session->set_flashdata($data['post']);

            redirect(base_url('dealer/memberForgotPassword'));
            exit();
		}else{

			//

		  //$this->input->get_post("email")
		  	$_where=" where user_name='".$this->input->get_post("email")."' ";
			$this->load->model('sean_models/Sean_db_tools');

		 	$_result01=$this->Sean_db_tools->db_get_max_record("*","ij_member",$_where);
				$_login_array=false;
				if(is_array($_result01))
				{
					foreach($_result01 as $key => $value)
			    {
			    		$_shopname=$this->page_data['dealer']['dealer_name'];
						$_username=$value->user_name;
						$_newpass=rand(111111, 999999);

						$_site_url="http://".$_SERVER['HTTP_HOST'];
						$_truename=$value->last_name.$value->first_name;
						$_to_email=$value->email;
						$_login_array=true;
					}
				}
				//var_dump($_result01[0]->member_sn);
				//exit();
		 	if($_login_array==false)
			{

				$this->session->set_flashdata("fail_message",  "<strong>驗證失敗!</strong> 系統找不到您的帳號或Email，請重新輸入或聯絡管理員．");

            	redirect(base_url('dealer/memberForgotPassword'));
           		exit();

			}else{
				//$this->session->set_userdata('web_login',true);
				//$this->session->set_userdata('customer_login',true);
				//$this->session->set_userdata('web_member_data',$_login_array);
				$this->session->set_flashdata("message","<strong>申請完成!</strong> 系統已經寄發一份密碼更改確認信件到您使用來註冊的信箱中，請前往收信並依照信件中指示完成密碼修改，$_shopname感謝您的支持．");
				//$this->send_mail("忘記密碼",$this->input->get_post("email"));
			  $this->load->library("ijw");
			  $this->ijw->send_mail('忘記密碼',$this->input->get_post("email"),'','');
			  $this->Shop_model->add_member_message($_result01[0]->member_sn,"忘記密碼-<strong>申請完成!</strong>","系統已經寄發一份密碼更改確認信件到您使用來註冊的信箱中，請前往收信並依照信件中指示完成密碼修改，$_shopname感謝您的支持．");

				redirect(base_url('dealer/memberForgotPassword'));
				exit();


			}
		}
		}
		$page_data = array();
		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設

	    $page_data['_captcha_img']=$this->captcha_img();

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['banner'] = explode(':',substr($this->page_data['dealer']['page_login_wallpaper_save_dir'], 0, -1));
		$page_data['profile'] = explode(':',substr($this->page_data['dealer']['page_profile_pic_save_dir'], 0, -1));
		$page_data['soical_media'] = $this->Shop_model->_select('ij_dealer_soical_media_info','dealer_sn',$this->page_data['dealer_sn'],0,0,'soical_media_name',0,'result_array');

		$page_data['page_content']['view_path'] = 'www/page_member/page_forgot_password'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_forgot_password_js'; // 頁面主內容的 JS view 位置, 必要
		$page_data['mainContain'] = $this->load->view('www/page_dealer/forgot_password', $page_data, true);
		$this->load->view('www/page_dealer/template', $page_data);
	}
}
