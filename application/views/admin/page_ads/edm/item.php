<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/typeahead/typeahead.css">
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/pages/css/portfolio.css" rel="stylesheet" type="text/css"/>


<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>行銷管理 <small>電子報管理</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        系統設定
        <i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/marketing/edm">電子報管理</a>
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         電子報編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">電子報編輯</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/marketing/edm" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <?php $this->load->view('admin/general/flash_error');?>
            <form class="form-horizontal" role="form" method="post">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">電子報標題 <span class="require">*</span></label>
                  <div class="col-lg-10">
                    <input type="text" name="Item[edm_titile]" required value="<?php echo @$Item['edm_titile'];?>" class="form-control" placeholder="電子報標題">
                  </div>
                </div>
                <!--div class="form-group">
                  <label for="name" class="col-lg-2 control-label">最後修改日期 <span class="require">*</span></label>
                  <div class="col-lg-10">
                    <p class="form-control-static">2015-06-10 12:00</p>
                  </div>
                </div-->
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">最後發送日期</label>
                  <div class="col-lg-10">
                    <p class="form-control-static"><?php echo (@$Item['last_time_send']!='0000-00-00 00:00:00')?@$Item['last_time_send']:''; ?></p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-2 control-label">內容</label>
                  <div class="col-lg-10">
                    <textarea id="editor1" required class="ckeditor form-control" name="Item[edm_content]" rows="12"><?php echo @$Item['edm_content'];?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-lg-2 control-label">更新者</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['update_member_sn'])?@$Item['update_member_sn']:@$Item['create_member_sn']; ?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-2 control-label">更新時間</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['last_time_update']!='0000-00-00 00:00:00')?@$Item['last_time_update']:@$Item['create_date']; ?>" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-2 col-md-10">
			              <input type="hidden" name="edm_sn" value="<?php echo @$Item['edm_sn']; ?>">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="button" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->