<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Town_ct extends My_general_ct {

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('sean_models/Sean_db_tools');


		/* Begin 修改  */
		$this->_ct_name="城市代碼表";// 功能名稱
		$this->_url="Sysconfig/Town_ct/";//列表功能url
		$this->_url_item="Sysconfig/Town_ct_item/";//處理動作 url
		$this->_table="ij_town_ct";//table name
		$this->_id="town_code";//table key
		$this->_table_field=array(
					 "town_code" => "行政區代號",
					 "city_code" => "城市代號",
					 "town_name" => "行政區中文名稱",
					 "town_eng_name" => "行政區英文名稱",
					 "zipcode" => "郵遞區號",
					 "sort_order" => "排序順次",
					 "default_flag" => "是否為預設值",
					 "display_flag" => "是否顯示",
				);

		/* End 修改  */

    }

    //取得列表內容
    public function table_value_list( $_where,$_id = 0)
	{
		$_table_key=$this->_id;
	    $page_data['_body_result']=array();
		//判斷是否有帶入特殊查詢
		if(strlen($_where)==0)
		{
			$_where="  order by city_code,zipcode asc";

		}
		// 取得列表
		$_result01=$this->Sean_db_tools->db_get_max_record("*",$this->_table,$_where);

		/* 列表標頭名稱 */
		$page_data['_table_thead_name']=$this->_table_field;

		$_where=" where associated_country_code=1 and associated_state_code=0 ";
        $_city_array=$this->Sean_db_tools->db_get_array("city_code","city_name","ij_city_ct",$_where);
		//$_city_array = $this->Shop_model->_select('ij_city_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','city_code,city_name'); // city
        //var_dump($_city_array);
		//$_city_array=$this->config->item("city_ct");

		/* 類表內容 */
		if(is_array($_result01))
		{
                 		$_i=0;
                 		foreach($_result01 as $key => $value)
                    	{

			                    		if($value->default_flag == 1)
			                    		{
			                    					$_default_flag = '<i class="fa fa-check"></i>';
			                    		}else{
			                    					$_default_flag = '<i class="fa fa-times"></i>';
			                    		}
										if($value->display_flag == 1)
			                    		{
			                    					$_display_flag = '<i class="fa fa-check"></i>';
			                    		}else{
			                    					$_display_flag = '<i class="fa fa-times"></i>';
			                    		}

			                    		$_body_result='';
						/* Begin 修改  */

										$page_data['_body_result'][$_i][0]=array("_value"=>$value->town_code,"_class"=>"");
			              $page_data['_body_result'][$_i][1]=array("_value"=>$_city_array[$value->city_code],"_class"=>"");
			              $page_data['_body_result'][$_i][2]=array("_value"=>$value->town_name,"_class"=>"");
										$page_data['_body_result'][$_i][3]=array("_value"=>$value->town_eng_name,"_class"=>"");
										$page_data['_body_result'][$_i][4]=array("_value"=>$value->zipcode,"_class"=>"");
										$page_data['_body_result'][$_i][7]=array("_value"=>$value->sort_order,"_class"=>"");
										$page_data['_body_result'][$_i][8]=array("_value"=>$_default_flag,"_class"=>"");
									  $page_data['_body_result'][$_i][9]=array("_value"=>$_display_flag,"_class"=>"");

						/* End 修改  */


										$page_data['_body_result'][$_i][10]=array("_value"=>'
			                    		 		<a href="'.$this->init_control.$this->_url_item.'edit/'.$value->$_table_key.'"><i class="fa fa-edit"></i>編輯</a>&nbsp;&nbsp;
						                      <a href="'.$this->init_control.$this->_url_item.'delete/'.$value->$_table_key.'" onclick="return deletechecked();"><i class="fa fa-trash-o"></i>刪除</a>&nbsp;&nbsp;
						                  	',"_class"=>'class="center"');
                				$_i++;


                			}

    				}
			return $page_data;

    }


}
