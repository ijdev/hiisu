<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script src="public/metronic/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<script type="text/javascript" src="public/metronic/global/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js"></script>

<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script src="public/metronic/admin/pages/scripts/components-form-tools.js"></script>
<script src="public/metronic/admin/pages/scripts/portfolio.js"></script>

<script>
jQuery(document).ready(function() {
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
    //ComponentsFormTools.init();
    Portfolio.init();
	var addr_city_code='<?php echo @$receiver_addr_city_code;?>';
	var addr_town_code='<?php echo @$receiver_addr_town_code;?>';
	if(addr_city_code){
		get_town(addr_city_code);
		$('select[id="addr_city_code"]').val(addr_city_code);
	}
    $(".various").fancybox({
        'titlePosition'     : 'inside',
        'transitionIn'      : 'none',
        'transitionOut'     : 'none'
    });
    /* Action On Select Box Change */
	$('select[id="addr_city_code"]').change(function() {
		var data = $(this).val(); // Get Selected Value
		get_town(data);

    });

		function get_town(data){
		    $.ajax({
		        url: 'Member/get_town_zip',
		        data: 'data=' + data,
		        dataType: 'json',
		        success: function(data) {
		         		 $('#addr_town_code').empty();
								 $(data).appendTo("#addr_town_code");
							 //$.uniform.update('#addr_town_code');
									if(addr_town_code){
										$('select[id="addr_town_code"]').val(addr_town_code);
							 		//$.uniform.update('#addr_town_code');
							 	}else{
							 		//$.uniform.update('#addr_town_code');
									}
		        }
		    });
	    }
		$('select[id="addr_town_code"]').change(function() {
					var zipcode = $('option:selected', this).attr('zipcode');
					$('#zipcode').val(zipcode);
        });
	  $('select[id="ccapply_code"]').change(function() {
	        var data = $(this).val(); // Get Selected Value
					get_data(data);

    });

		function get_data(data){
		    $.ajax({
		        url: 'Member/get_ccapply_detail',
		        data: 'data=' + data,
		        dataType: 'json',
		        success: function(result) {
		         		 $('#ccapply_detail_code').empty();
								 $(result).appendTo("#ccapply_detail_code");
  							 //$.uniform.update('#addr_town_code');
		        }
		    });
    }
	$('select[id="ccapply_code"]').trigger('change');
	$("#set_invoice").click(function(){
		if(confirm('確定開立發票？')){
		    $.ajax({
		        url: '/admin/order/orderItem/<?php echo $order[0]['sub_order_sn']?>',
		        data: 'set_invoice=yes',
		        dataType: 'json',
		        success: function(result) {
		        	if(result=='ok'){
		         		$('#invoice_area').load('/admin/order/orderItem/<?php echo $order[0]['sub_order_sn']?> #invoice_area');
		         		$('#invoice_edit').hide('slow');
		         	}else{
		         		//$('#invoice_area').html(result);
		         		alert('很抱歉，開立發票失敗：'+result);
		         	}
		        }
		    });
		}
	});

	$(".print").click(function(){
		/*Popup($.get("admin/admin/orderItem/<?php echo $order[0]['sub_order_sn']?>/print"));
		console.log('zz');*/
		Popup();
	});
	$("#ajax_msg").on("show.bs.modal", function(e) {
			//console.log(e.relatedTarget);
	    var link = $(e.relatedTarget);
	    $(this).find(".modal-content").load(link.attr("href"));
	});
	var invoice_addr_city_code='<?php echo @$invoice_receiver_addr_city_code;?>';
	var invoice_addr_town_code='<?php echo @$invoice_receiver_addr_town_code;?>';

	if(invoice_addr_city_code){
		$('select[id="invoice_addr_city_code"]').val(invoice_addr_city_code);
		//$('select[id="invoice_addr_town_code"]').val(invoice_addr_town_code);
		//invoice_get_town(invoice_addr_city_code);
	}

	$('select[id="invoice_addr_city_code"]').change(function() {
		var data = $(this).val(); // Get Selected Value
		invoice_get_town(data);
	});

	function invoice_get_town(data){
		$.ajax({
			url: 'Member/get_town_zip',
			data: 'data=' + data,
			dataType: 'json',
			success: function(data) {
				$('#invoice_addr_town_code').empty();
					$(data).appendTo("#invoice_addr_town_code");
				if(invoice_addr_town_code){
					$('select[id="invoice_addr_town_code"]').val(invoice_addr_town_code);
				}
			}
		});
	}

	$('select[id="invoice_addr_town_code"]').change(function() {
		var zipcode = $('option:selected', this).attr('zipcode');
		$('#invoice_zipcode').val(zipcode);
	});
});
    function Popup()
    {
        var mywindow = window.open('<?php echo base_url('admin/order/orderItem/'.$order[0]['sub_order_sn'].'/print')?>', '_blank', 'height=800,width=1200');
        //mywindow.document.write('<html><head><title>my div</title><style>');
        //mywindow.document.write(css);
        //mywindow.document.write('</style></head><body >');
        //mywindow.document.write(data);
        //mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
		setTimeout(function () { mywindow.close(); },5000);
        return true;
    }
</script>

