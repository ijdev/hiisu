<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 首頁 slider
*/
?>
<!-- BEGIN SLIDER -->
<div class="page-slider">
  <!-- LayerSlider start -->
  <div id="layerslider" style="width: 100%; height: 494px; margin: 0 auto;">

    <!-- slide one start -->
    <div class="ls-slide ls-slide1" data-ls="offsetxin: right; slidedelay: 5000; transition2d: 110,111,112,113;">
      <img src="public/metronic/frontend/pages/img/layerslider/slide1/bg.jpg" class="ls-bg" alt="Slide background">
    </div>
    <!-- slide one end -->

    <!-- slide two start -->
    <div class="ls-slide ls-slide2" data-ls="offsetxin: right; slidedelay: 5000; transition2d: 110,111,112,113;">
      <img src="public/metronic/frontend/pages/img/layerslider/slide2/bg.jpg" class="ls-bg" alt="Slide background">
    </div>
    <!-- slide two end -->

    <!-- slide three start -->
    <div class="ls-slide ls-slide3" data-ls="offsetxin: right; slidedelay: 5000; transition2d: 110,111,112,113;">
      <img src="public/metronic/frontend/pages/img/layerslider/slide3/bg.jpg" class="ls-bg" alt="Slide background">
    </div>
    <!-- slide three end -->

  </div>
  <!-- LayerSlider end -->
</div>
<!-- END SLIDER -->