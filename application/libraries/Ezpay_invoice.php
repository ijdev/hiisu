<?php
set_include_path(APPPATH . 'third_party/' . PATH_SEPARATOR .     get_include_path());
require APPPATH .'third_party/ezpay_invoice.php';
class Ezpay_invoice {
	protected $CI;
	var $url ='';
	var $search_url ='';
	var $key ='';
	var $iv ='';
	var $MerchantID ='';
	public function __construct(){
		if(ENVIRONMENT=='development'){//測試環境URL
			$this->url = 'https://cinv.ezpay.com.tw/Api/invoice_issue'; // 測試串接網址
			$this->search_url='https://cinv.ezpay.com.tw/Api/invoice_search';
			$this->key = 'ywhiJEcvXP5O0GLcIt2z9MNB5C71WVTN';    // 商店專屬串接金鑰HashKey 值
			$this->iv = 'CqvAocQn5bXArtmP'; // 商店專屬串接金鑰 HashIV 值
			$this->MerchantID = '31851295'; // 商店代號
		}else{
			$this->url = 'https://inv.ezpay.com.tw/Api/invoice_issue'; // 正式串接網址
			$this->search_url='https://inv.ezpay.com.tw/Api/invoice_search';
			$this->key = 'PGyk57QOXdOBHXowNiGYT32ib6OGBN7V';    // 商店專屬串接金鑰HashKey 值
			$this->iv = 'PUVeTA6rQFxdiJJC'; // 商店專屬串接金鑰 HashIV 值
			$this->MerchantID = '312214742'; // 商店代號
		}
		$this->CI =& get_instance();
		$this->CI->load->library('ijw');
		$this->CI->load->model('libraries_model');
	}
	public function set_invoice($order=0,$ext_days=10){
		if(!$order['invoice_num'] && $order['payment_status']=='2'){
			if(!$order['invoice_receiver_middle_name']){
				//var_dump($order);exit;
				if($ext_days>0){
					$CreateStatusTime=$this->CI->ijw->date_add_days(date('Y-m-d'),$ext_days);
					$Status=3;
				}else{
					$CreateStatusTime='';
					$Status=1;
				}
				//echo '<pre>' . var_export($order, true) . '</pre>';exit;
				if($order['company_tax_id'] && $order['invoice_title']){
					$Category='B2B';
					$PrintFlag='Y';
					$CarrierType='';
					$CarrierNum='';
				}else{
					$Category='B2C';
					$PrintFlag='N';
					$CarrierType='2';
					$CarrierNum=rawurlencode($order['member_sn']);
				}
				$Amt=round($order['total_order_amount']/1.05);
				$TaxAmt=$order['total_order_amount']-$Amt;
				$post_data_array = array(
				        //post_data 欄位資料
				        'RespondType' => 'JSON',
				        'Version' => '1.4',
				        'TimeStamp' => time(), // 請以   time()  格式
				        'TransNum' => $order['credit_card_trans_id'], //? not sure
				        'MerchantOrderNo' => $order['order_num'],
				        'BuyerName' => ' '.$order['buyer_last_name'].' ',
				        'BuyerUBN' => $order['company_tax_id'],
				        'BuyerAddress' => ' '.$order['buyer_zipcode'].$order['buyer_addr_city'].$order['buyer_addr_town'].$order['buyer_addr1'].' ',
				        'BuyerEmail' => $order['buyer_email'],
				        'Category' => $Category,//B2C=買受人為個人。
				        'TaxType' => '1',
				        'TaxRate' => '5',
				        'Amt' => $Amt,
				        'TaxAmt' => $TaxAmt,
				        'TotalAmt' => $order['total_order_amount'],
				        'CarrierType' => $CarrierType,
				        'CarrierNum' => $CarrierNum,
				        'LoveCode' => '',
				        'PrintFlag' => $PrintFlag,
				        'Comment' => '',
				        'CreateStatusTime' => $CreateStatusTime,
				        'Status' => $Status //1= 立即開立， 0= 待開立， 3= 延遲開立
				);
				// 商品資訊
				$ItemName='';
		 		$ItemCount='';
		 		$ItemUnit='';
		 		$ItemPrice='';
		 		$ItemAmt='';
				if($order['order_item']){ foreach($order['order_item'] as $_key=>$_Item){
				 	$ItemName.=$_Item['product_name'];
				 	$ItemCount.=$_Item['buy_amount'];
				 	$ItemUnit.='個';
				 	$ItemPrice.=(int)$_Item['sales_price'];
				 	$ItemAmt.=(int)$_Item['sales_price']*$_Item['buy_amount'];
				 	if($_key<count($order['order_item'])-1){
						$ItemName.='|';
				 		$ItemCount.='|';
				 		$ItemUnit.='|';
				 		$ItemPrice.='|';
				 		$ItemAmt.='|';
				 	}
				}}
				$post_data_array['ItemName']=$ItemName;
				$post_data_array['ItemCount']=$ItemCount;
				$post_data_array['ItemUnit']=$ItemUnit;
				$post_data_array['ItemPrice']=$ItemPrice;
				$post_data_array['ItemAmt']=$ItemAmt;
				//echo '<pre>' . var_export($post_data_array, true) . '</pre>';exit;
				$post_data_str      = http_build_query($post_data_array); // 轉成字串排列
				if (phpversion() > 7) {
				    $post_data = trim(bin2hex(openssl_encrypt(ezpay_addpadding($post_data_str),
					'AES-256-CBC', $this->key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $this->iv)));
				} else {
				    $post_data = trim(bin2hex(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->key,
					ezpay_addpadding($post_data_str), MCRYPT_MODE_CBC, $this->iv))); //php 7 之前版本加密
				}
				$data=array('MerchantID_'=>$this->MerchantID,'PostData_'=>$post_data);
				//exit;
				$curl_result=curl_work($this->url,$data);
				$web_info=json_decode($curl_result['web_info'],true);
				//echo '<pre>' . var_export($web_info, true) . '</pre>';exit;
				if($web_info['Status']=='LIB10003' && !$order['invoice_receiver_middle_name']){
					return $this->search_invoice($order);
				}
				if($web_info['Result']){
					$result=json_decode($web_info['Result'],true);
				}else{
					$result=array();
				}
				$check_code_arr = array(
				        'MerchantID' => $this->MerchantID, // 商店代號
				        'MerchantOrderNo' => $post_data_array['MerchantOrderNo'], // 商店自訂單號 ( 訂單編號 )
				        'InvoiceTransNo' => @$result['InvoiceTransNo'], //ezPay 電子發票開立序號
				        'TotalAmt' => @$result['TotalAmt'], // 發票金額
				        'RandomNum' => @$result['RandomNum'] // 發票防偽隨機碼
				);
				ksort($check_code_arr);
				$check_str    = http_build_query($check_code_arr);
				$check_code    = strtoupper(hash('sha256', "HashIV=$this->iv&".$check_str."&HashKey=$this->key"));
				//print_r($check_code);
				//var_dump($result);
				//var_dump($web_info);
				if($check_code===@$result['CheckCode']){
					//更新訂單
	    			$invoice_data=$result;
	    			//var_dump($invoice_data);
	                $_order=array();
	                if($invoice_data['InvoiceNumber']){
	                	$_order['invoice_num'] = $invoice_data['InvoiceNumber'];
	                }
	                if($invoice_data['RandomNum']){
	                	$_order['invoice_receiver_first_name'] = $invoice_data['RandomNum'];
	                }
	                if($invoice_data['CreateTime']){
	                	$_order['invoice_time'] = $invoice_data['CreateTime'];
	                }
	                if($invoice_data['InvoiceTransNo']) $_order['invoice_receiver_middle_name'] = $invoice_data['InvoiceTransNo'];
	                $_order['invoice_aReturn_Info'] = json_encode($result);
	                $this->CI->libraries_model->_update('ij_order',$_order,'order_sn',$order['order_sn']);
	                //var_dump($_order);exit;
		        	return $web_info;
		        }else{
		        	return $web_info;
		        }
			}else{
				$this->search_invoice($order);
			}
	    }else{
	    	$result['Status']='error';
	  		if($order['invoice_num']){
		    	$result['Message']='發票已開立';
	  		}elseif($order['payment_status']!='2'){
	    		$result['Message']='付款狀態不是已付款';
	  		}
	        return $result;
	    }
	}
	public function search_invoice($order=0,$SearchType='1',$DisplayFlag=0){
		//echo '<pre>' . var_export($order, true) . '</pre>';exit;
		$post_data_array = array(
		        'RespondType' => 'JSON',
		        'Version' => '1.1',
		        'TimeStamp' => time(), // 請以   time()  格式
		        'SearchType' => $SearchType, //0=使用發票號碼及隨機碼查詢。 1=使用訂單編號及發票金額查詢。 
		        'MerchantOrderNo' => $order['order_num'],
		        'TotalAmt' => $order['total_order_amount'],
		        'InvoiceNumber' => $order['invoice_num'],
		        'RandomNum' => $order['invoice_receiver_first_name'],
		        'DisplayFlag' => $DisplayFlag
		);

		//echo '<pre>' . var_export($post_data_array, true) . '</pre>';exit;
		$post_data_str      = http_build_query($post_data_array); // 轉成字串排列
		if (phpversion() > 7) {
		    $post_data = trim(bin2hex(openssl_encrypt(ezpay_addpadding($post_data_str),
			'AES-256-CBC', $this->key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $this->iv)));
		} else {
		    $post_data = trim(bin2hex(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->key,
			ezpay_addpadding($post_data_str), MCRYPT_MODE_CBC, $this->iv))); //php 7 之前版本加密
		}
		$data=array('MerchantID_'=>$this->MerchantID,'PostData_'=>$post_data);
		//exit;
		$curl_result=curl_work($this->search_url,$data);
		$web_info=json_decode($curl_result['web_info'],true);
		//echo '<pre>' . var_export($web_info, true) . '</pre>';exit;
		if($web_info['Result']){
			$result=json_decode($web_info['Result'],true);
		}else{
			$result=array();
		}
		$check_code_arr = array(
		        'MerchantID' => $this->MerchantID, // 商店代號
		        'MerchantOrderNo' => $post_data_array['MerchantOrderNo'], // 商店自訂單號 ( 訂單編號 )
		        'InvoiceTransNo' => @$result['InvoiceTransNo'], //ezPay 電子發票開立序號
		        'TotalAmt' => @$result['TotalAmt'], // 發票金額
		        'RandomNum' => @$result['RandomNum'] // 發票防偽隨機碼
		);
		ksort($check_code_arr);
		$check_str    = http_build_query($check_code_arr);
		$check_code    = strtoupper(hash('sha256', "HashIV=$this->iv&".$check_str."&HashKey=$this->key"));
		//print_r($check_code);
		//var_dump($check_code_arr);
		if($check_code===@$result['CheckCode']){
			$_order=array();
            if($result['InvoiceNumber']){
            	$_order['invoice_num'] = $result['InvoiceNumber'];
            }
            if($result['RandomNum']){
            	$_order['invoice_receiver_first_name'] = $result['RandomNum'];
            }
            if($result['CreateTime']!=0){
            	$_order['invoice_time'] = $result['CreateTime'];
            }
            if($result['InvoiceTransNo']) $_order['invoice_receiver_middle_name'] = $result['InvoiceTransNo'];
            $_order['invoice_aReturn_Info'] = json_encode($result);
            //var_dump($result['CreateStatusTime']);
			//echo '<pre>' . var_export($result, true) . '</pre>';
            $this->CI->libraries_model->_update('ij_order',$_order,'order_sn',$order['order_sn']);
        	//echo $this->CI->db->last_query();exit;
        	return $web_info;
        }else{
        	return $web_info;
        }
	}
}