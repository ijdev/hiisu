<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/

if(isset($_result01) && is_array($_result01))
{
 	foreach($_result01 as $key => $value)
  {

  	$_result_item=$value;

  }
}
if(isset($_bank_info) && is_array($_bank_info))
{
 	foreach($_bank_info as $key => $value)
  {

  	$_bank_info_item=$value;

  }
}
if(isset($_contact_info) && is_array($_contact_info))
{
	$_ik=1;
 	foreach($_contact_info as $key => $value)
  {

  	$_contact_info_item[$_ik]=$value;
  	$_ik++;

  }
}

if(isset($_content_info) && is_array($_content_info))
{
	$_ik=1;
 	foreach($_content_info as $key => $value)
  {

  	$_content_info_item[$_ik]=$value;
  	$_ik++;

  }
}

?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>會員管理 <small>經銷會員設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        會員管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="<?php echo $_father_link;?>">經銷會員</a>
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         經銷會員設定
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">經銷會員設定</span>
            </div>
            <div class="tools">
              <?php if(@$_result_item->dealer_sn){?><a href="admin/Member/dealerPage/<?php echo @$_result_item->dealer_sn;?>">會員專頁</span></a> &nbsp; <?php }?>
              <a href="<?php echo $_father_link;?>"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
            <div class="tabbable">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_general" data-toggle="tab"> 一般設定 </a>
                </li>
                <?php
                if(isset($_result_item->dealer_sn) && $_result_item->dealer_sn > 0 )
                {
                	$_dealer_sn_now=$_result_item->dealer_sn;
                	?>


                <!--li>
                  <a href="#tab_page" data-toggle="tab"> 專頁設定 </a>
                </li-->
                <li>
                  <a href="#tab_manager" data-toggle="tab"> 管理人員 </a>
                </li>
                <li>
                  <a href="#tab_member" data-toggle="tab"> 關聯會員 </a>
                </li>
                <!--li>
                  <a href="#tab_certificate" data-toggle="tab"> 兌換券狀態 </a>
                </li>
                <li>
                  <a href="#tab_order" data-toggle="tab"> 採購紀錄 </a>
                </li-->
                <?php
              		}
              	?>
              </ul>
              <div class="tab-content no-space">
                <!-- Tab 資料設定 -->
                <div class="tab-pane active" id="tab_general">
                  <form name="register-form" id="register-form" <?php if(isset($_action_link))echo " action=\"".$_action_link."\" ";?> class="form-horizontal error" novalidate="">
                    <div class="form-body">
                      <h4>帳號設定</h4>
                      <div class="form-group">
                        <label for="active" class="col-lg-3 control-label">身份驗證 <span class="require">*</span></label>
                        <div class="col-lg-8">
                          <select name="dealer_status" id="dealer_status" class="form-control">
                            <option value=1 <?php if(isset($_result_item->dealer_status) && $_result_item->dealer_status=='1') echo ' selected="selected"';?>>通過</option>
                            <option value=2 <?php if(isset($_result_item->dealer_status) && $_result_item->dealer_status=='2') echo ' selected="selected"';?>>未通過</option>
                            <option value=2 <?php if(isset($_result_item->dealer_status) && $_result_item->dealer_status=='0') echo ' selected="selected"';?>>停用</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="firstname" class="col-lg-3 control-label">店名 <span class="require">*</span></label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="dealer_name" id="dealer_name" value="<?php if(isset($_result_item->dealer_name)) echo $_result_item->dealer_name;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="lastname" class="col-lg-3 control-label">簡稱 <span class="require">*</span></label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="dealer_short_name" id="dealer_short_name" value="<?php if(isset($_result_item->dealer_short_name)) echo $_result_item->dealer_short_name;?>">
                        </div>
                      </div>
                      <!--div class="form-group">
                        <label for="email" class="col-lg-3 control-label">Email <span class="require">*</span></label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" id="user_name" name="user_name" placeholder="partner_xxxxx@jweds.tw" value="<?php if(isset($_result_item->user_name)) { echo $_result_item->user_name; }else{ echo 'partner_@jweds.tw';}?>">
                        </div>
                      </div-->
                     <div class="form-group">
                        <label class="col-lg-3 control-label">產業屬性 </label>
                        <div class="col-lg-8">
                          <div class="checkbox-list">
                             <?php
							       					//var_dump($_member_domain_relation);
							                foreach($_biz_domain_list as $keys=>$values)
							                {
							    							$_checked="";
							    							if(isset($_member_domain_relation))
							    							{
								    							foreach($_member_domain_relation as $ckey => $cvalue)
								    							{
								    									if($cvalue->domain_sn==$values->domain_sn)
								    									$_checked=" checked";
								    							}
								    						}
							    							echo  '  <label class="checkbox-inline"><input type="checkbox" name="domain_sn[]" value="'.$values->domain_sn.'" '.$_checked.'>'.$values->domain_name.'</label>';
															}
                						?>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">會員分級</label>
                        <div class="col-lg-8">
                          <select name="dealer_price_discount" id="dealer_price_discount" class="form-control">
                            <option value="">請選擇</option>
                             <?php
							                 while (list($keys, $values) = each ($_dealer_level_list))
							                 {
							    								echo  '<option value="'.$values->dealer_level_sn.'"';
							    								if(isset($_result_item->dealer_price_discount) && $_result_item->dealer_price_discount==$values->dealer_level_sn)
							    								echo ' selected="selected"';
							    								echo '>'.$values->dealer_level_name.'</option>';
															 }
                						?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">有效日期</label>
                        <div class="col-lg-9">
                          <div class="input-group input-large date-picker input-daterange  pull-left" data-date="" data-date-format="yyyy-mm-dd">
                            <input type="text" class="form-control input-sm" name="partnership_start_date" value="<?php if(isset($_result_item->partnership_start_date)) echo $_result_item->partnership_start_date;?>">
                            <span class="input-group-addon"> 到 </span>
                            <input type="text" class="form-control input-sm" name="partnership_end_date" value="<?php if(isset($_result_item->partnership_end_date)) echo $_result_item->partnership_end_date;?>">
                          </div>
                        </div>
                      </div>
                      <!--hr>
                      <h4>密碼設定</h4> bridina 說不需要
                      <div class="form-group">
                        <label for="passwords" class="col-lg-3 control-label">密碼 <span class="require">*</span></label>
                        <div class="col-lg-8">
                          <input type="password" class="form-control" name="password" id="password" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="confirmpassword" class="col-lg-3 control-label">重複輸入密碼 <span class="require">*</span></label>
                        <div class="col-lg-8">
                          <input type="password" class="form-control" name="confirm_password" id="confirm_password" value="">
                        </div>
                      </div-->
                      <hr>
                      <h4>基本資料</h4>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">官方網站</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name='website_url' id='website_url'  value="<?php if(isset($_result_item->website_url)) echo $_result_item->website_url;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">電話</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="office_tel" id="office_tel" value="<?php if(isset($_result_item->office_tel)) echo $_result_item->office_tel;?>" >
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">傳真</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="office_fax" id="office_fax" value="<?php if(isset($_result_item->office_fax)) echo $_result_item->office_fax;?>" >
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">住址</label>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <div class="col-md-4">
						              <select id="addr_city_code" name="addr_city_code"  class="table-group-action-input form-control " >
						                  <option value="">縣市</option>
						                  <?php
						       					//$_city_list=$this->config->item("city_ct");
						                 while (list($keys, $values) = each ($_city_ct)) {
						    							echo  ' <option value="'.$keys.'">'.$values.'</option>';

														}

						                ?>
						                </select>


						                            </div>
						                            <div class="col-md-4">
						                  <select  id="addr_town_code" name="addr_town_code" class="table-group-action-input form-control  " >
						                  <option value="">鄉鎮市區</option>
						                </select>
                            </div>
                          </div>
                         <input type="text" id="addr1" name="addr1"  class="pagination-panel-input form-control " size="12" style="margin: 0 5px;" placeholder="住址" value="<?php if(isset($_result_item->addr1)) echo $_result_item->addr1;?>">

                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">公司負責人</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="dealer_chairman" id="dealer_chairman" value="<?php if(isset($_result_item->dealer_chairman)) echo $_result_item->dealer_chairman;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">公司抬頭</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="dealer_company_title" id="dealer_company_title" value="<?php if(isset($_result_item->dealer_company_title)) echo $_result_item->dealer_company_title;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">統一編號</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="dealer_tax_id" id="dealer_tax_id" value="<?php if(isset($_result_item->dealer_tax_id)) echo $_result_item->dealer_tax_id;?>">
                        </div>
                      </div>
                      <hr>
                      <h4>帳務資訊</h4>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">戶名</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="account_name" id="account_name" value="<?php if(isset($_bank_info_item->account_name)) echo $_bank_info_item->account_name;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">銀行</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="bank_name" id="bank_name" value="<?php if(isset($_bank_info_item->bank_name)) echo $_bank_info_item->bank_name;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">分行</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="branch_name" id="branch_name" value="<?php if(isset($_bank_info_item->branch_name)) echo $_bank_info_item->branch_name;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">帳號</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="bank_account" id="bank_account" value="<?php if(isset($_bank_info_item->bank_account)) echo $_bank_info_item->bank_account;?>">
                        </div>
                      </div>
                      <hr>
                      <h4>聯絡人一</h4>
                      <div class="form-group">
                        <label for="active" class="col-lg-3 control-label">是否有效 </label>
                        <div class="col-lg-8">
                          <select name="status_1" class="form-control">
                            <option value=1 <?php if(isset($_contact_info_item[1]->status) && $_contact_info_item[1]->status==1) echo ' selected="selected"';?>>是</option>
                            <option value=0 <?php if(isset($_contact_info_item[1]->status) && $_contact_info_item[1]->status==0) echo ' selected="selected"';?>>否</option>
                          </select>
                        </div>
                      </div>


                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">聯絡人姓名 </label>
                        <div class="col-lg-8">
                          <input type="text" name="contact_name_1" class="form-control"  value="<?php if(isset($_contact_info_item[1]->contact_name)) echo $_contact_info_item[1]->contact_name;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">電話 </label>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <div class="col-lg-3">
                              <input type="text" class="form-control" name="office_tel_1" value="<?php if(isset($_contact_info_item[1]->office_tel)) echo $_contact_info_item[1]->office_tel;?>">
                            </div>
                            <div class="col-lg-4">
                              <label for="name" class="col-lg-6 control-label">分機</label>
                              <div class="col-lg-6">
                                <input type="text" class="form-control" name="office_tel_ext_1"value="<?php if(isset($_contact_info_item[1]->office_tel_ext)) echo $_contact_info_item[1]->office_tel_ext;?>">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">手機 </label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="cell_1" value="<?php if(isset($_contact_info_item[1]->cell)) echo $_contact_info_item[1]->cell;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">Email </label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="email_1" value="<?php if(isset($_contact_info_item[1]->email)) echo $_contact_info_item[1]->email;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">備註</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="memo_1" value="<?php if(isset($_contact_info_item[1]->memo)) echo $_contact_info_item[1]->memo;?>">
                        </div>
                      </div>


                      <hr>
                      <h4>聯絡人二</h4>
                      <div class="form-group">
                        <label for="active" class="col-lg-3 control-label">是否有效 </label>
                        <div class="col-lg-8">
                          <select name="status_2" class="form-control">
                            <option value=1 <?php if(isset($_contact_info_item[2]->status) && $_contact_info_item[1]->status==1) echo ' selected="selected"';?>>是</option>
                            <option value=0 <?php if(isset($_contact_info_item[2]->status) && $_contact_info_item[2]->status==0) echo ' selected="selected"';?>>否</option>
                          </select>
                        </div>
                      </div>


                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">聯絡人姓名 </label>
                        <div class="col-lg-8">
                          <input type="text" name="contact_name_2" class="form-control"  value="<?php if(isset($_contact_info_item[2]->contact_name)) echo $_contact_info_item[2]->contact_name;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">電話 </label>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <div class="col-lg-3">
                              <input type="text" class="form-control" name="office_tel_2" value="<?php if(isset($_contact_info_item[2]->office_tel)) echo $_contact_info_item[2]->office_tel;?>">
                            </div>
                            <div class="col-lg-4">
                              <label for="name" class="col-lg-6 control-label">分機</label>
                              <div class="col-lg-6">
                                <input type="text" class="form-control" name="office_tel_ext_2"value="<?php if(isset($_contact_info_item[2]->office_tel_ext)) echo $_contact_info_item[2]->office_tel_ext;?>">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">手機 </label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="cell_2" value="<?php if(isset($_contact_info_item[2]->cell)) echo $_contact_info_item[2]->cell;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">Email </label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="email_2" value="<?php if(isset($_contact_info_item[2]->email)) echo $_contact_info_item[2]->email;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">備註</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="memo_2" value="<?php if(isset($_contact_info_item[2]->memo)) echo $_contact_info_item[2]->memo;?>">
                        </div>
                      </div>
                      <hr>
                      <h4>聯絡人三</h4>
                     <div class="form-group">
                        <label for="active" class="col-lg-3 control-label">是否有效 </label>
                        <div class="col-lg-8">
                          <select name="status_3" class="form-control">
                            <option value=1 <?php if(isset($_contact_info_item[3]->status) && $_contact_info_item[3]->status==1) echo ' selected="selected"';?>>是</option>
                            <option value=0 <?php if(isset($_contact_info_item[3]->status) && $_contact_info_item[3]->status==0) echo ' selected="selected"';?>>否</option>
                          </select>
                        </div>
                      </div>


                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">聯絡人姓名 </label>
                        <div class="col-lg-8">
                          <input type="text" name="contact_name_3" class="form-control"  value="<?php if(isset($_contact_info_item[3]->contact_name)) echo $_contact_info_item[3]->contact_name;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">電話 </label>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <div class="col-lg-3">
                              <input type="text" class="form-control" name="office_tel_3" value="<?php if(isset($_contact_info_item[3]->office_tel)) echo $_contact_info_item[3]->office_tel;?>">
                            </div>
                            <div class="col-lg-4">
                              <label for="name" class="col-lg-6 control-label">分機</label>
                              <div class="col-lg-6">
                                <input type="text" class="form-control" name="office_tel_ext_3"value="<?php if(isset($_contact_info_item[3]->office_tel_ext)) echo $_contact_info_item[3]->office_tel_ext;?>">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">手機 </label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="cell_3" value="<?php if(isset($_contact_info_item[3]->cell)) echo $_contact_info_item[3]->cell;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">Email </label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="email_3" value="<?php if(isset($_contact_info_item[3]->email)) echo $_contact_info_item[3]->email;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">備註</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="memo_3" value="<?php if(isset($_contact_info_item[3]->memo)) echo $_contact_info_item[3]->memo;?>">
                        </div>
                      </div>
                      <hr>


                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">客服代表</label>
                        <div class="col-lg-8">
                          <select name="associated_sales_member_sn"  id="associated_sales_member_sn" class="form-control">
                            <?php
						                 while (list($keys, $values) = each ($_associated_sales_membe_list)) {
						    							echo  ' <option value="'.$values->member_sn.'">'.$values->last_name.$values->first_name.'</option>';
														}
						                ?>
                          </select>
                        </div>
                      </div>

                     <!--
                      <div class="form-group">
                        <label for="updater" class="col-lg-3 control-label">更新者</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control"  placeholder="administrator" Disabled>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="time-update" class="col-lg-3 control-label">更新時間</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control"  placeholder="2015-05-25 10:00" Disabled>
                        </div>
                      </div>
                      -->
                    </div>


                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                        	<?php
                        	if(isset($_contact_info_item[1]->contact_sn))
                        	{
                        	 echo '<input type=hidden name="contact_sn_1" id="contact_sn_1" value="'.$_contact_info_item[1]->contact_sn.'">';

                        	}
                        	if(isset($_contact_info_item[2]->contact_sn))
                        	{
                        	  echo '<input type=hidden name="contact_sn_2" id="contact_sn_2" value="'.$_contact_info_item[2]->contact_sn.'">';

                        	}
                        	if(isset($_contact_info_item[3]->contact_sn))
                        	{
                        	 echo '<input type=hidden name="contact_sn_3" id="contact_sn_3" value="'.$_contact_info_item[3]->contact_sn.'">';

                        	}
                        	 ?>
                          <button type="submit" class="btn green">儲存</button>
                          <a href="<?php echo $_father_link;?>"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- End of Tab 資料設定 -->
                <!-- Tab  -->
                <!--div class="tab-pane" id="tab_page">
                  <form  action="admin/Member/dealerTable/contentupdate/"  class="form-horizontal error" novalidate="">

                    <div class="form-body">
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">專頁名稱</label>
                        <div class="col-lg-8">
                          <input type="text" name="dealer_name" id="dealer_name" class="form-control" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">服務描述</label>
                        <div class="col-lg-8">
                          <textarea class="ckeditor form-control" name="dealer_description" id="dealer_description" rows="3"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">感謝有您預設內容</label>
                        <div class="col-lg-8">
                          <textarea class="ckeditor form-control" name="dealer_blessing"  id="dealer_blessing" rows="3"></textarea>
                          <span class="help-block">描述將會顯示在新人結婚網站『感謝有您』功能中，最多 140 個中文字</span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">訪客留言預設內容</label>
                        <div class="col-lg-8">
                          <textarea class="ckeditor form-control" name="dealer_thanksgiving" id"dealer_thanksgiving" rows="3"></textarea>
                          <span class="help-block">將會顯示在新人結婚網站『訪客留言』功能中，最多 140 個中文字</span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">客製化子域名</label>
                        <div class="col-lg-8">
                          <p class="form-control-static">xxxxx.jweds.tw</p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">紛絲專頁</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" id="	page_facebook_url_set" name="	page_facebook_url_set" placeholder="https://www.facebook.com/kutewedding">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">其他社群媒體</label>
                        <div class="col-lg-8">
                            <div class="form-group">
                              <label for="name" class="col-lg-2 control-label">名稱</label>
                              <div class="col-lg-2">
                                <input type="text" class="form-control" name="page_social_media_url_title_1" id="page_social_media_url_title_1" placeholder="">
                              </div>
                              <label for="name" class="col-lg-2 control-label">連結</label>
                              <div class="col-lg-6">
                                <input type="text" class="form-control" name="page_social_media_url_value_1" id="page_social_media_url_value_1" placeholder="">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="name" class="col-lg-2 control-label">名稱</label>
                              <div class="col-lg-2">
                                <input type="text" class="form-control" name="page_social_media_url_title_2" id="page_social_media_url_title_2" placeholder="">
                              </div>
                              <label for="name" class="col-lg-2 control-label">連結</label>
                              <div class="col-lg-6">
                                <input type="text" class="form-control" name="page_social_media_url_value_2" id="page_social_media_url_value_2" placeholder="">
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-lg-12">
                                <button type="button" class="btn btn-sm pull-right" value="">新增媒體</button>
                              </div>
                            </div>
                        </div>
                      </div>
                      <hr>
                      <h4>首頁與登入頁設定</h4>
                      <div class="form-group">
                        <label for="name" class="col-md-3 control-label">首頁 Slides</label>
                        <div class="col-md-8">

                          <!-- <img src="http://dev.ijwedding.com/public/uno/images/home/001.jpg" width="120" style="vertical-align: bottom; margin-right:12px; padding:2px; border:1px solid #EFEFEF;" >
                             <i class="fa fa-trash-o"></i>
                          <input class="form-control" name="page_home_slider_save_dir" id="page_home_slider_save_dir" onchange="" type="file">
                     		  <span class="help-block">最適寬高 1800 x 845 </span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-md-3 control-label">關於我們介紹圖</label>
                        <div class="col-md-8">
                          <!-- <img src="http://dev.ijwedding.com/public/uno/images/pages/p2.jpg" width="120" style="vertical-align: bottom; margin-right:12px; padding:2px; border:1px solid #EFEFEF;" >
                             <i class="fa fa-trash-o"></i>
                           <input class="form-control" name="page_aboutus_pic_save_dir" id="page_aboutus_pic_save_dir" onchange="" type="file">

                          <span class="help-block">最適寬高 350 x 350 </span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-md-3 control-label">專頁大 Banner</label>
                        <div class="col-md-8">
                          <img src="public/img/page_banner.jpg" width="120" style="vertical-align: bottom;" >
                          <i class="fa fa-trash-o"></i>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-md-3 control-label">專頁頭像</label>
                        <div class="col-md-8">
                          <img src="public/img/avatar.jpg" width="120" style="vertical-align: bottom;" ><i class="fa fa-trash-o"></i>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-md-3 control-label">登入頁背景圖</label>
                        <div class="col-md-8">
                          <img src="public/img/wallpaper_sample.jpg" width="120" style="vertical-align: bottom;" ><i class="fa fa-trash-o"></i>
                        </div>
                      </div>
                      <hr>
                      <h4>聯絡我們設定</h4>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">說明內容</label>
                        <div class="col-lg-8">
                          <input class="form-control"  name="page_contactus_content" id="page_contactus_content" >
                          <span class="help-block">聯絡我們頁面中提醒客戶的文字資訊 </span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">聯絡電話</label>
                        <div class="col-lg-8">
                         <input class="form-control" name="office_tel" id="office_tel" >
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">聯繫地址</label>
                        <div class="col-lg-8">
                           <input class="form-control" name="addr1" id="addr1">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">營業時間</label>
                        <div class="col-lg-8">
                          <textarea class="ckeditor form-control" id="dealer_opening_time"  name="dealer_opening_time" rows="7">
                          	</textarea>
                        </div>
                      </div>
                    </div>
                  </form>
                </div-->
                <!-- End of Tab 管理人員 -->
                <!-- Tab 管理人員 -->
                <div class="tab-pane" id="tab_manager">

                  <div class="note note-warning" id="search_filter">
        <div class="col-md-3 col-sm-3">
        	<?php $this->load->view('www/general/flash_error');?>
        </div>
                    <div class="table-actions-wrapper pull-right">
                      <!--span>新增管理員</span>
                      <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="36" placeholder="請輸入 Email"-->
                      <button id="Add_addon" class="btn btn-sm yellow table-group-action-submit" data-toggle="modal" href="#invite_form"><i class="fa fa-check"></i> 新增管理員</button>
                    </div>
                    <div style="clear:both;"></div>
                  </div>
                  <!-- End of search_filter -->
                  <table class="table table-striped table-bordered table-hover" id="table_member_admin">
                    <thead>
                      <tr>
                        <th>姓名</th>
                        <th>Email</th>
                        <th>狀態</th>
                        <th>功能</th>
                      </tr>
                    </thead>
                    <tbody>
                <?php if(is_array($_result03))
                	{
                 			foreach($_result03 as $key => $value)
                    		{
                  ?>
                        <tr class="odd gradeX">
                          <td><a target="_blank" href="/admin/Member/memberTable_item/edit/<?php echo $value->member_sn;?>"><?php echo $value->last_name.$value->first_name;?><i class="fa fa-external-link"></i></a></td>
                          <td>
                            <?php echo $value->email;?>
                          </td>
                          <td>
                            <?php if($key%2==0):?>
                              <span class="label label-sm label-success"> 已確認 </span>
                            <?php else:?>
                              <span class="label label-sm label-warning"> 邀請中 </span>
                            <?php endif;?>
                          </td>
                          <td class="center">
                            <a class="deleteAddon" ItemId="<?php echo $value->member_sn;?>" dealer_sn="<?php echo $_dealer_sn_now;?>" href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                          </td>
                        </tr>
                      <?php }}?>
                    </tbody>
                  </table>
                </div>
                <!-- End of Tab 管理人員 -->
                <!-- Tab 所屬會員 -->
                <div class="tab-pane" id="tab_member">
                  <table class="table table-striped table-bordered table-hover" id="table_member">
                    <thead>
                      <tr>
                        <th>姓名</th>
                        <th>Email</th>
                        <th>縣市</th>
                        <th>鄉鎮市區</th>
                        <th>住址</th>
                        <th>電話</th>
                        <th>加入時間</th>
                        <th>帳號狀態</th>
                        <th>編輯</th>
                      </tr>
                    </thead>
                    <tbody>
               <?php
                 $_member_status_ct=$this->config->item("member_status_ct");
								 $_member_level_type_ct=$this->config->item("member_level_type_ct");
								 //$_city_ct=$this->config->item("city_ct");


                if(is_array($_result02))
                	{
                 			foreach($_result02 as $key => $value)
                    		{
												  $_town_ct_init="town_ct_".$value->addr_city_code;
												  $_town_ct=$this->config->item($_town_ct_init);
                  ?>


                  <tr class="odd gradeX">
                    <td><?php echo $value->last_name.$value->first_name;?></td>
                    <td><?php echo $value->email;?></td>
                    <td><?php echo $value->city_name;?></td>
                    <td><?php echo $value->town_name;?></td>
                    <td><?php echo $value->addr1;?></td>
                    <td><?php echo $value->cell;?></td>
                    <td><?php echo $value->registration_date;?></td>
                    <td><?php echo $_member_status_ct[$value->member_status];?> </td>
                    <td>
                      <a target="_blank" href="<?php echo $init_control;?>Member/memberTable_item/edit/<?php echo $value->member_sn;?>"><i class="fa fa-edit"></i></a>&nbsp;
                    </td>
                  </tr>
                <?php } } ?>
                    </tbody>
                  </table>
                </div>
                <!-- End of Tab 所屬會員 -->
                <!-- Tab 兌換券 -->
                <div class="tab-pane" id="tab_certificate">
                  <div class="row">
                    <div class="col-md-6">
                      <h4>狀態統計</h4>
                      <div id="chart_status" class="chart" style="height: 500px;"></div>
                    </div>
                    <div class="col-md-6">
                      <h4>啟用比例</h4>
                      <div id="chart_enable" class="chart" style="height: 500px;"></div>
                    </div>
                  </div>
                  <table class="table table-striped table-bordered table-hover" id="table_coupon">
                    <thead>
                      <tr>
                        <th>編號</th>
                        <th>兌換券狀態</th>
                        <th>使用會員</th>
                        <th>功能</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php for($i=0;$i<20;$i++):?>
                        <tr class="odd gradeX">
                          <td>
                            xewfrhowhfoih
                          </td>
                          <td class="center">
                            <span class="badge badge-primary badge-roundless"> 已使用 </span>
                          </td>
                          <td class="center">
                            Dsxc
                          </td>
                          <td class="center">
                            <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>棄用</a>
                          </td>
                        </tr>
                      <?php endfor;?>
                    </tbody>
                  </table>
                  <!-- page nav -->
                  <div class="row">
                    <div class="col-md-5 col-sm-12">
                      <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
                    </div>
                    <div class="col-md-7 col-sm-12">
                      <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                        <ul class="pagination" style="visibility: visible;">
                          <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                          <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                          <li><a href="#">3</a></li>
                          <li><a href="#">4</a></li>
                          <li class="active"><a href="#">5</a></li>
                          <li><a href="#">6</a></li>
                          <li><a href="#">7</a></li>
                          <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                          <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!-- end of page nav -->
                </div>
                <!-- End of Tab 兌換券 -->
                <!-- Tab 採購紀錄 -->
                <div class="tab-pane" id="tab_order">
                  <table class="table table-striped table-bordered table-hover" id="table_member_order">
                    <thead>
                      <tr>
                        <th>訂單編號</th>
                        <th>訂單價格</th>
                        <th>採購數量</th>
                        <th>登錄數量</th>
                        <th>審核通過</th>
                        <th>是否付款</th>
                        <th>功能</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php for($i=1;$i<=10;$i++):?>
                        <tr class="odd gradeX">
                          <td>d123456</td>
                          <td>10,000</td>
                          <td class="center">100</td>
                          <td class="center">100</td>
                          <td class="center">
                            <?php if($i%2==0):?>
                              <i class="fa fa-check"></i>
                            <?php else:?>
                              <i class="fa fa-times"></i>
                            <?php endif;?>
                          </td>
                          <td class="center">
                            <?php if($i%2==0):?>
                              <i class="fa fa-check"></i>
                            <?php else:?>
                              <i class="fa fa-times"></i>
                            <?php endif;?>
                          </td>
                          <td class="center">
                            <a href="preview/certificateApplyItem"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                            <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                          </td>
                        </tr>
                      <?php endfor;?>
                    </tbody>
                  </table>
                  <!-- page nav -->
                  <div class="row">
                    <div class="col-md-5 col-sm-12">
                      <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
                    </div>
                    <div class="col-md-7 col-sm-12">
                      <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                        <ul class="pagination" style="visibility: visible;">
                          <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                          <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                          <li><a href="#">3</a></li>
                          <li><a href="#">4</a></li>
                          <li class="active"><a href="#">5</a></li>
                          <li><a href="#">6</a></li>
                          <li><a href="#">7</a></li>
                          <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                          <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!-- end of page nav -->
                </div>
                <!-- End of Tab 採購紀錄 -->
              </div>
            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
<!-- Modal for 設定管理員 -->
<div class="modal fade" id="invite_form" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">新增管理員</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form" method="post">
          <div class="form-body">
            <div class="form-group">
              <label for="name" class="col-lg-3 control-label">經銷會員</label>
              <div class="col-lg-8">
                <select name="associated_member_sn" required id="associated_member_sn" class="form-control">
                	<option value="">請選擇</option>
                  <?php
										//var_dump($_result04);
	                 while (list($keys, $values) = each ($_result04)) {
	    							echo  ' <option value="'.$values->member_sn.'">'.$values->last_name.$values->first_name.' &nbsp; '.$values->email.'</option>';
									}
	                ?>
                </select>
              </div>
            </div>
            <!--div class="form-group">
              <label for="lastname" class="col-lg-3 control-label">Email</label>
              <div class="col-lg-8">
                  <input type="text" class="form-control" value="cbhuewf@gmail.com" disabled="">
              </div>
            </div>
            <div class="form-group">
              <label for="lastname" class="col-lg-3 control-label">姓</label>
              <div class="col-lg-8">
                  <input type="text" class="form-control" value="吳" disabled="">
              </div>
            </div>
            <div class="form-group">
              <label for="lastname" class="col-lg-3 control-label">名</label>
              <div class="col-lg-8">
                  <input type="text" class="form-control" value="小偉" disabled="">
              </div>
            </div-->
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn blue">確認加入管理員</button>
      </div>
        </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- Modal for 設定管理員 -->