<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        //$this->modules = array('promo', 'photo','customer','lottery','vote');
    }
    public function _select($table,$where = 0,$where_val = 0,$offset = 0,$per_page = 0,$order = 0,$desc = 0,$result = 'result',$select = 0,$like = 0,$distinct=0,$status=0, $group = 0){
        if (!$select) $select = $table.'.*';

        $this->db->from($table);
        if($this->db->field_exists('update_member_sn', $table)){ //有更新者欄位抓其user_name代替update_member_sn
            $this->db->select($select.',aaa.manager_name as update_member_sn');
            $this->db->join('ij_wedding_manager as aaa','aaa.manager_sn='.$table.'.update_member_sn','left');
        }else{
            $this->db->select($select);
        }

        if($where && $where_val && !$like){
            $this->db->where($where,$where_val);
        }elseif($where && !$where_val && !$like){
            $this->db->where($where);
        }elseif($where && $like && !$where_val){
            $this->db->like($where, $like);
        }else{
            if($this->db->field_exists('status', $table)){
            $this->db->where('status','1');
          }
        }
        if($status){
            if($this->db->field_exists('status', $table)){  //test除了指定以外都只抓1的
                $this->db->where('status','1');
            }
        }
        if($order && $desc){
            $this->db->order_by($order,'desc');
        }elseif($order && !$desc){
            $this->db->order_by($order,'asc');
        }

        if($group){
             $this->db->group_by($group);
        }

        if($offset && $per_page){
            $this->db->limit($per_page,$offset);
        }elseif(!$offset && $per_page){
            $this->db->limit($per_page);
        }
        if($distinct){
            $this->db->distinct($distinct);
        }

        $query = $this->db->get();
        //echo $this->db->last_query();

        if($result == 'row'){
            return $query->row_array();
        }elseif($result == 'rows'){
            foreach($query->result_array() as $row){
                $rows[]=$row[$select];
            }
          return @$rows;
        }elseif($result == 'result'){
            $return_array=$query->result();
            $query->free_result();
          return $return_array;
        }elseif($result == 'result_array'){
            $return_array=$query->result_array();
            $query->free_result();
          return $return_array;
        }elseif($result == 'list'){
            $_fields=explode(',',$select);
            if(strpos($_fields[0],'.')){
                $_fields[0]=explode('.',$_fields[0])[1];
            }
            if(strpos($_fields[1],'.')){
                $_fields[1]=explode('.',$_fields[1])[1];
            }
            foreach ($query->result() as $row)
            {
               $_one1= $row->{$_fields[0]};
               $_one2= $row->{$_fields[1]};
               $_one[$_one1]=$_one2;
            }
          return @$_one;
        }elseif($result == 'num_rows'){
            return $query->num_rows();
        }elseif($result == 'strings'){
            if ($select){
                return implode('、',$query->row_array());
            }
        }
    }
    public function _update($table,$data,$where = 0,$where_val = 0,$_field=0,$_field_add=0,$no_log=0){
        if($where && $where_val){
            $this->db->where($where,$where_val);
        }elseif($where && !$where_val){
            $this->db->where($where);
        }
        if($_field && $_field_add){
            $this->db->set($_field,$_field_add,FALSE);
        }
        $this->db->update($table,$data);
        //echo $this->db->last_query();
        //var_dump(strpos($table,'order'));exit;
        //if(strpos($table,'order')!==false){
            $_trans_log["_table"]=$table;
            if($where && $where_val){
                $old_data=$this->_select($table,$where,$where_val,0,0,0,0,'row');
                $_key_id=array($where=>$where_val);
                $_key_id=json_encode($_key_id,JSON_PRETTY_PRINT);
            }elseif($where && !$where_val){
                $this->db->where($where);
                $old_data=$this->_select($table,$where,0,0,0,0,0,'row');
                $_key_id=json_encode($where,JSON_PRETTY_PRINT);
            }
            $_trans_log["_before"]=json_encode($old_data,JSON_PRETTY_PRINT);
            $_trans_log["_after"]=json_encode($data,JSON_PRETTY_PRINT);
            $_trans_log["_type"]="0";
            $_trans_log["_key_id"]=$_key_id;
            $this->trans_log($_trans_log);
        //}
        return ($this->db->affected_rows() > 0);
    }
    public function trans_log(array $_trans_log)
    {
            $_table_log="ij_trans_log";

            $_data["trans_date"]=date("Y-m-d H:i:s");
            $_data["trans_table"]=$_trans_log["_table"];
            $_data["trans_type"]=$_trans_log["_type"];
            $_data["trans_column_set"]=$_trans_log["_key_id"];
            $_data["beforetrans_column_set"]=$_trans_log["_before"];
            $_data["after_trans_column_set"]=$_trans_log["_after"];
            $_data["trans_member_sn"]=( isset($this->session->userdata['member_data']['member_sn'] ) ) ? $this->session->userdata['member_data']['member_sn'] : @$this->session->userdata['web_member_data']['member_sn'];
            $_data["trans_member_name"]=( isset($this->session->userdata['member_data']['last_name'] ) ) ? $this->session->userdata['member_data']['last_name'] : @$this->session->userdata['web_member_data']['last_name'];
            $_data["trans_program_sn"]=$_SERVER['REQUEST_URI'];

            $this->_insert($_table_log,$_data);
    }
    public function _insert($table,$data){
        $this->db->insert($table,$data);
        return $this->db->insert_id();
    }
    public function _update_field_amount($table,$where,$where_val,$field,$value){
	    $sql="update $table set $field=$field $value where $where='$where_val'";
	    return $this->db->query($sql);
	}
    public function _delete($table,$where,$where_val = 0){
        if($where && $where_val){
            $this->db->where($where,$where_val);
        }elseif($where && !$where_val){
            $this->db->where($where);
        }
        $this->db->delete($table);
        return ($this->db->affected_rows() > 0);
    }
    public function group_static($_table,$_field) {
        $this->db->select("$_field,count($_field) as rs_count");
        $this->db->from($_table);
        $this->db->where("$_field is not null");
        $this->db->group_by($_field);
        $this->db->order_by($_field);
        $_query = $this->db->get();
        return $_query->result_array();
    }
    public function group_static2($_table,$_field) {
        $this->db->select("$_field,count($_field) as rs_count");
        $this->db->from($_table);
        $this->db->where("$_field is not null");
        $this->db->group_by($_field);
        $this->db->order_by($_field);
        $_query = $this->db->get();
        foreach($_query->result_array() as $row){
            $rows[$row[$_field]]=$row['rs_count'];
        }
        $_query->free_result();
        return @$rows;
    }
    public function get_distinct_data($_table,$field,$_sn_array,$_sn_field='product_sn') {
        $this->db->select('*');
        $this->db->from($_table);
        $this->db->group_by($field);
        $this->db->where("$field !=",'');
        $this->db->where_in($_sn_field,$_sn_array);
        if($this->db->field_exists('sort_order', $_table)){
            $this->db->order_by('sort_order');
        }else{
            $this->db->order_by($field);
        }
        $_query = $this->db->get();
        //echo $this->db->last_query();
        return $_query->result_array();
    }
    public function get_channels($more=0){
        if($more){
                //return array(''=>'請選擇',$more=>$more,'婚禮網站'=>'婚禮網站','婚宴管理'=>'婚宴管理','結婚商店'=>'結婚商店');
                return array(''=>'請選擇',$more=>$more,'囍市集'=>'囍市集');
        }else{
                //return array(''=>'請選擇','婚禮網站'=>'婚禮網站','婚宴管理'=>'婚宴管理','結婚商店'=>'結婚商店');
                return array(''=>'請選擇','囍市集'=>'囍市集');
        }
    }
}
?>