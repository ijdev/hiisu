<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sys_program extends My_general_ct {

   public $parrent_new_array;

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('sean_models/Sean_db_tools');
		$this->_table="ij_sys_program_config";
		$this->_id="sys_program_config_sn";

    }


    public function table_value_list( $_where,$_id = 0) {
        $page_data['_body_result']=array();
		if(strlen($_where)==0)
		{
			$_where="  order by sort_order asc";
			// $_where=" where class1_program_flag=1  order by sort_order asc";
			if($_id >  0  )
			$_where=" where class1_program_flag=2 and upper_sys_program_config_sn='$_id'  order by sort_order asc";

		}

		$_result01=$this->Sean_db_tools->db_get_max_record("*",$this->_table,$_where);

		//$_result02=$this->_parent_sort($_result01,"upper_sys_program_config_sn");


		/* 列表標頭名稱 */
		$page_data['_table_thead_name']=array("＋","功能中文名稱","功能英文名稱","檔案名稱","排序順次","使用狀態","功能");
		/* 類表內容 */
		if(is_array($_result01))
		{
                 		$_i=0;
                 			foreach($_result01 as $key => $value)
                    	{

			                    		if($value->status == 1)
			                    		{
			                    					$_active = '<i class="fa fa-check"></i>';
			                    		}else{
			                    					$_active = '<i class="fa fa-times"></i>';
			                    		}

			                    		$_body_result='';


			                    		$page_data['_body_result'][$_i][0]=array("_value"=>'
                                        <a href="'.$this->init_control.'sys_program_item/add/'.$value->sys_program_config_sn.'"><i class="fa fa-edit"></i>新增子類</a>&nbsp;&nbsp;
                    	<a href="'.$this->init_control.'Sys_program/'.$value->sys_program_config_sn.'/'.$value->sys_program_name.'"><i class="fa fa-edit"></i>預覽子類</a>
                                        ',"_class"=>"");
			                    		$page_data['_body_result'][$_i][1]=array("_value"=>$value->sys_program_name,"_class"=>"");
			                    		$page_data['_body_result'][$_i][2]=array("_value"=>$value->sys_program_eng_name,"_class"=>"");
			                    		$page_data['_body_result'][$_i][3]=array("_value"=>$value->sys_program_url,"_class"=>"");
			                    		$page_data['_body_result'][$_i][4]=array("_value"=>$value->sort_order,"_class"=>"");
									    $page_data['_body_result'][$_i][5]=array("_value"=>$_active,"_class"=>"");
			                    		$page_data['_body_result'][$_i][6]=array("_value"=>'
			                    		 		<a href="'.$this->init_control.'sys_program_item/edit/'.$value->sys_program_config_sn.'"><i class="fa fa-edit"></i>編輯</a>&nbsp;&nbsp;
						                      <a href="'.$this->init_control.'sys_program_item/delete/'.$value->sys_program_config_sn.'" onclick="return deletechecked();"><i class="fa fa-trash-o"></i>刪除</a>&nbsp;&nbsp;
						                  	',"_class"=>'class="center"');
                				$_i++;
                			}

    				}
			return $page_data;

    }


	    function _parent_sort($items, $parent_key,$parent = null)
		 {

			  $index = $parent == null ? '0' : $parent;

			  if (isset($items[$index]))
			  {

					   //$this->_result_menu .= '<ul';

					   //$this->_result_menu .= $parent == null ? ' category_sn="category" ' : '';

					   //$this->_result_menu .= '>';

					   foreach ($items[$index] as $child)
					   {

					    	if(strlen($items[$index]) > 0 )
					    	array_push( $this->parrent_new_array,$items[$index]);

					    	$this->_parent_sort($items, $parent_key, $child[$parent_key]);



					   }

			   		return $this->parrent_new_array;
			  }
		 }


}

/* End of file Sys_role.php */
/* Location: ./application/models/backend_system/Sys_role.php */