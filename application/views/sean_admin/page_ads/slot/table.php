<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>廣告版位設定 <small>版位列表</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin-ijwedding/Ads">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
        廣告版位列表
      </li>
    </ul>
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">廣告版位列表</span>
            </div>
            <div class="actions btn-set">
              <a href="admin-ijwedding/Ads/adsSlotItem/add/0" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>廣告版位名稱</th>
                  <th>頻道</th>
                  <th>類別</th>
                  <th>寬高</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
              	<?php
              	
              		$_adsSlot_type =  $this->config->item("adsSlot_type");
              		$_banner_type =  $this->config->item("banner_type");
              	
              		if(is_array($_result01))
                		{ 
                			$i=1;                                 
                 			foreach($_result01 as $key => $value)
                    		{                                      			                    			  			                    			                    			                 			                    		                    			
                    		  print '
																	<tr class="odd gradeX">                    		  
																		<td>'.$value->banner_location_name.'</td>
	                    							<td>'.$_adsSlot_type[$value->associated_category_sn].'</td>
	                    							<td>'.$_banner_type[$value->banner_tyype].'</td>                                                                                        	                    							
	                    							<td class="center">'.$value->width.'X'.$value->hight.'</td>      
								                    <td class="center">
								                      <a href="admin-ijwedding/Ads/adsSlotItem/edit/'.$value->banner_location_sn.'"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
								                      <a href="admin-ijwedding/Ads/adsSlotItem/delete/'.$value->banner_location_sn.'"><i class="fa fa-trash-o"></i>刪除</a>
								                    </td>
								                  </tr>  	                    							                                        						                        						                                          							                    		  				
                    		  			';
                    		  			
                    		  $i++;
                    		}
                  	}
              	?>                	               
              </tbody>
            </table>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->