<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>FAQ內容 <small>文章列表</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">文章列表</span>
            </div>
            <div class="actions btn-set">
              <a href="admin-ijwedding/Content/contentFaqItem/add/0" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <div class="table-actions-wrapper pull-right">
            <span>
            </span>
            <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="10" maxlenght="10" style="margin: 0 5px;" placeholder="關鍵字">
            <select class="table-group-action-input form-control input-inline input-small input-sm">
              <option value="">請選擇頻道</option>
              <option value="">雲端婚訊</option>
              <option value="">現場報到系統</option>
              <option value="">結婚商店</option>
            </select>
            <select class="table-group-action-input form-control input-inline input-small input-sm">
              <option value="">請選擇分類</option>
              <option value="">分類一</option>
              <option value="">分類二</option>
            </select>
            <select class="table-group-action-input form-control input-inline input-small input-sm">
              <option value="">是否發佈</option>
              <option value="publish">發佈</option>
              <option value="unpublished">未發佈</option>
              <option value="delete">已刪除</option>
            </select>
            <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>分類</th>
                  <th>Question</th>
                  <th>Answer</th>
                  <th>排序</th>
                  <th>是否發佈</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
              	<?php
              		if(is_array($_result01))
                		{ 
                			$i=1;                                 
                 			foreach($_result01 as $key => $value)
                    		{                    			                    			                    			
                    			if($value->status == 1)
                    				{
                    					$_active = '<i class="fa fa-check"></i>';
                    				}
                    			else
                    				{
                    					$_active = '<i class="fa fa-times"></i>';
                    				}                    			                    			
                    			
                    		  print '
																	<tr class="odd gradeX">                    		  
																		<td>'.$_result02[$value->associated_content_typel_sn_set].'</td>
	                    							<td>'.$value->faq_titile.'</td>
	                    							<td>'.$value->faq_description.'</td>                                                                                        
	                    							<td>'.$value->sort_order.'</td>
	                    							<td class="center">'.$_active.'</td>      
								                    <td class="center">
								                      <a href="admin-ijwedding/Content/contentFaqItem/edit/'.$value->faq_sn.'"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
								                      <a href="admin-ijwedding/Content/contentFaqItem/delete/'.$value->faq_sn.'"><i class="fa fa-trash-o"></i>刪除</a>
								                    </td>
								                  </tr>  	                    							                                        						                        						                                          							                    		  				
                    		  			';
                    		  			
                    		  $i++;
                    		}
                  	}
              	?>               	               
              </tbody>
            </table>

            <div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                  <ul class="pagination" style="visibility: visible;">
                    <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                    <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li class="active"><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                    <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->