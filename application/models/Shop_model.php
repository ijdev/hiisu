<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shop_model extends MY_Model{
    public function _select($table,$where = 0,$where_val = 0,$offset = 0,$per_page = 0,$order = 0,$desc = 0,$result = 'result',$select = 0,$like = 0,$distinct=0,$update_member=1,$status=1){
        if (!$select) $select = $table.'.*';

        $this->db->from($table);

         if($update_member && $this->db->field_exists('update_member_sn', $table)){ //有更新者欄位抓其user_name代替update_member_sn
	        $this->db->select($select.',aaa.first_name as update_member_sn');
	        $this->db->join('ij_member as aaa','aaa.member_sn='.$table.'.update_member_sn','left');
	      }else{
        	$this->db->select($select);
      	}

        if($where && $where_val && !$like){
            $this->db->where($where,$where_val);
        }elseif($where && !$where_val && !$like){
            $this->db->where($where);
        }elseif($where && $like && !$where_val){
            $this->db->like($where, $like);
        }else{
        	if($this->db->field_exists('status', $table)){
            $this->db->where('status','1');
          }
        }
      	/*if($status){
        	if($this->db->field_exists('status', $table)){  //test除了指定以外都只抓1的
            $this->db->where('status','1');
          }
		    }*/
        if($order && $desc){
            $this->db->order_by($order,'desc');
        }elseif($order && !$desc){
            $this->db->order_by($order,'asc');
        //}else{
            //$this->db->order_by('sort_order','asc');
        }

        if($offset && $per_page){
            $this->db->limit($per_page,$offset);
        }elseif(!$offset && $per_page){
            $this->db->limit($per_page);
        }
        if($distinct){
            $this->db->distinct($distinct);
        }

        $query = $this->db->get();
		//echo $this->db->last_query();

        if($result == 'row'){
            return $query->row_array();
        }elseif($result == 'rows'){
        	foreach($query->result_array() as $row){
        		$rows[]=$row[$select];
        	}
          return @$rows;
        }elseif($result == 'list'){
        	$_one=array();
        	$_fields=explode(',',$select);
				   foreach ($query->result() as $row)
				   {
					   $_one1= $row->{$_fields[0]};
					   $_one2= $row->{$_fields[1]};
					   $_one[$_one1]=$_one2;
				   }
          return $_one;
        }elseif($result == 'result'){
            return $query->result();
        }elseif($result == 'result_array'){
            return $query->result_array();
        }elseif($result == 'num_rows'){
            return $query->num_rows();
        }elseif($result == 'strings'){
					if ($select){
            return implode('、',$query->row_array());
          }
        }

    }

    public function _insert($table,$data){

        $this->db->insert($table,$data);
        return $this->db->insert_id();

    }

    public function _delete($table,$where,$where_val = 0){

        if($where && $where_val){
            $this->db->where($where,$where_val);
        }elseif($where && !$where_val){
            $this->db->where($where);
        }

        $this->db->delete($table);

        return ($this->db->affected_rows() > 0);

    }

    public function _count_all($table){
        return $this->db->count_all($table);
    }
    public function _selectifsame($table,$where = 0,$where_val = 0,$wherenot = 0,$wherenot_val = 0){

        $this->db->select($where);
        $this->db->from($table);

        if($where && $where_val){
            $this->db->where($where,$where_val);
				}

        if($wherenot && $wherenot_val){
            $this->db->where($wherenot.' !=',$wherenot_val);
				}

        $query = $this->db->get();
        if ($query->num_rows()>0){
        	return true;
        }else{
        	return false;
        }

    }

    public function CheckUpdate($array,$ifnew=0)
    {
    	if(@$this->session->userdata['web_member_data']['member_sn']){
    	 	$member_sn=$this->session->userdata['web_member_data']['member_sn'];
    	}else{
    	 	$member_sn='0';
    	}
    	if(!$ifnew){
        $array['last_time_update']=date("Y-m-d H:i:s",time());
        $array['update_member_sn']=$member_sn;
      }else{
        $array['create_date']=date("Y-m-d H:i:s",time());
        $array['last_time_update']=date("Y-m-d H:i:s",time());
        $array['create_member_sn']=$member_sn;
      }
        return $array;
    }

    public function categoryParentChildTree($channel_name=0,$category_sn=0,$spacing = '├─', $category_tree_array = '',$spacing2='│&nbsp;')
    {
		//NodeId為Parent的點
		    if (!is_array($category_tree_array))
		        $category_tree_array = array();

	    //取得該點的下一層
        $this->db->select('category_sn,category_name,upper_category_sn,sort_order,channel_name,category_status');
        $this->db->from("ij_category");
        $this->db->where('upper_category_sn',$category_sn);
        $this->db->where('category_status','1');
        $this->db->order_by('sort_order','asc');
        if($channel_name){
        	$this->db->where('channel_name',$channel_name);
      	}
				$query =$this->db->get()->result_array();
		//echo $this->db->last_query().'<br>';
	    if($query) { //最多10代
			//如果有下一層資料，逐一取出
        foreach($query as $key => $intro){
        	//echo $spacing . $intro['category_name'].'<br>';
          $category_tree_array[] = array("category_sn" => $intro['category_sn'], "category_name" => $spacing . $intro['category_name'], "upper_category_sn" => $intro['upper_category_sn'], "sort_order" => $intro['sort_order'], "channel_name" => $intro['channel_name'], "category_status" => $intro['category_status']);
          $category_tree_array = $this->categoryParentChildTree($channel_name,$intro['category_sn'], $spacing2.$spacing , $category_tree_array,$spacing2);
        	//var_dump($category_tree_array);
				}
	    }
		    return $category_tree_array;
    }
    public function get_banner($_array){
        foreach ($_array as $_key => $_value) {
            if($_value['upper_category_sn']=='0'){
                $_array[$_key]['main_banner']=$this->_select('ij_category','category_sn',$_value['category_sn'],0,0,'main_banner',0,'row','main_banner')['main_banner'];
                //var_dump($_array[$_key]['main_banner']);
            }
            $_array[$_key]['down_nums']=$this->_select('ij_category',array('upper_category_sn'=>$_value['category_sn'],'category_status'=>'1'),0,0,0,'category_sn',0,'num_rows');
        }
        return $_array;
    }

    public function _get_product($product_sn=0,$select=0,$result=0,$ifaddon=0)
    {
    	if($product_sn){
    		if(strpos($product_sn,'_')!==false){ // 過濾_
    			$product_sn=substr($product_sn,0,strpos($product_sn,'_'));
    		}
    	    $this->db->select('product_sn,ij_product.supplier_sn,ij_product.dealer_sn,product_name,product_eng_name,product_type,associated_brand_code,default_root_category_sn,default_channel_name,unispec_flag,pricing_method,min_price,ij_product.associated_payment_method,associated_delivery_method,ij_product.meta_title,ij_product.meta_keywords,ij_product.meta_content,product_info,open_preorder_flag,min_order_qt,unispec_safe_qty_in_stock,unispec_qty_in_stock,special_item,incremental,category_name,supplier_name,min_amount_for_noshipping,share_percent,brand,precautions,unit_name,max_order_qt,free_delivery,brand,weight,unispec_warehouse_num,product_template_sn,size,spec');
            $this->db->from("ij_product");
            $this->db->join("ij_category","ij_category.category_sn=ij_product.default_root_category_sn","left");
            $this->db->join("ij_supplier","ij_supplier.supplier_sn=ij_product.supplier_sn","left");
            //$this->db->join("ij_product_gallery","ij_product_gallery.product_sn=ij_product.product_sn","left");
    		if(is_array($product_sn)){
                $this->db->where_in('ij_product.product_sn',$product_sn);
    		}else{
            	$this->db->where('ij_product.product_sn',$product_sn);
          	}
            //if(!$ifaddon && $default_channel_name!='婚禮網站'){ //因為婚禮網站商品沒有 ij_product_gallery
            //	$this->db->where('ij_product_gallery.status','1'); //新加2016-1-7 wdj
            //}
            $this->db->where('ij_product.product_status','1'); //新加2016-1-7 wdj
            $this->db->group_by('ij_product.product_sn');
            //$this->db->order_by('ij_product_gallery.sort_order','asc'); //新加2016-1-7 wdj
        	if($result){
    			$query =$this->db->get()->result_array();
    		}else{
    			$query =$this->db->get()->row_array();
            }
    //echo $this->db->last_query();
            //return $query;
	//exit();
			if($query){
				//抓產品圖檔
				if($query['default_channel_name']=='囍市集'){
        		    $query['images'] = $this->_select('ij_product_gallery',array('product_sn' => $query['product_sn'],'status'=>'1'),0,0,0,0,0,'result_array','image_path,image_alt');
					if($query['images']){
						$query['image_path'] =$query['images'][0]['image_path'];
						$query['image_alt'] = $query['images'][0]['image_alt'];
					}else{
						$query['image_path'] ='notyet.gif';
						$query['image_alt'] = '無商品圖或商品圖尚未建檔';
					}
                    if($query['unispec_flag']=='0'){ //多重規格
                        if($query['open_preorder_flag']){ //開放預購不抓庫存
                            $where = array('product_sn' => $query['product_sn']);
                        }else{
                            $where = array('product_sn' => $query['product_sn'],'qty_in_stock >' => '0');
                        }
                        $query['mutispec']=$this->_select('ij_mutispec_stock_log',$where,0,0,0,0,0,'result_array','mutispec_stock_sn,color_name,qty_in_stock,warehouse_num,safe_qty_in_stock');
                    }
				}else{
					$query['image_path'] =$this->_Get_default_Product_associated_template_image($query['product_sn'],$query['product_template_sn']);
					$query['image_alt'] = $query['product_name'];
                    $query['Packages']=$this->_get_package_price_by_psn($query['product_sn'],$query['incremental']);
				}

				$this->load->model('libraries_model');
	        	$query['addons'] = $this->libraries_model->_get_addon_list($query['product_sn']);
	        	$query['attributes'] = $this->libraries_model->_get_product_attribute($query['product_sn'],0,0,1);

	            if($query['pricing_method']=='1'){  //單一
					$ifprices=$this->_select('ij_product_fix_price_log','product_sn',$query['product_sn'],0,0,'price',0,'num_rows');
					$price_tier=$this->_select('ij_product_fix_price_log','product_sn',$query['product_sn'],1,0,'price',0,'row');
					if($query['min_price']){
						$query['price1']=$query['min_price'];
						$query['price2']=($ifprices > 1)? '起':''; //多規格-價格
						$query['fixed_price']=($price_tier['fixed_price'])? $price_tier['fixed_price']:'';
					}else{
						$query['price1']='尚未';
						$query['price2']='';
						$query['fixed_price']='';
					}
	            }else{
	  				$query['price1'] = $query['min_price'];
					$ifprices=$this->_select('ij_product_price_tier_log','product_sn',$query['product_sn'],0,0,'price',0,'num_rows');
	  				$query['price2'] = ($ifprices > 1)? '起':''; //多區間
	  			    $query['price_tier'] = $this->_select('ij_product_price_tier_log','product_sn',$query['product_sn'],0,0,0,0,'result_array');
                    $query['fixed_price']=($query['price_tier'][0]['flexible_interval_flag'])? $query['price_tier'][0]['flexible_interval_flag']:'';
	            }
	          if($query['price1']=='尚未'){ //無定價不顯示
		    			return false;
	          }
	          //抓付款方式
	          if($query['associated_payment_method']){
	          	$query['payment_method']=$this->_get_row_list('ij_payment_method_ct','payment_method',$query['associated_payment_method'],'payment_method_name');
	          }else{
	          	//無資料抓全館開放
	          	$query['payment_method']=$this->_get_row_list('ij_payment_method_ct','payment_method',0,'payment_method_name');
	          }
	          //抓配送方式
	          if($query['associated_delivery_method']){
	          	$query['delivery_method']=$this->_get_row_list('ij_delivery_method_ct','delivery_method',$query['associated_delivery_method'],'delivery_method_name');
	          }else{
	          	//無資料抓全館開放
	          	$query['delivery_method']=$this->_get_row_list('ij_delivery_method_ct','delivery_method',0,'delivery_method_name');
	          }
	      	}
		    return $query;
    	}
  	}
	  public function _get_row_list($table,$fieldid,$data,$fieldname){
	  	if($data){
		  	$row_array=explode(':',$data);
				if(is_array($row_array)){
	          $this->db->where_in($fieldid,$row_array);
				}else{
	      	$this->db->where($fieldid,$data);
	    	}
	    }
	    $this->db->from($table);
        $this->db->where('display_flag','1');
	    $this->db->order_by('sort_order');
		$query =$this->db->get()->result_array();
		$row_list='';
	  	foreach($query as $row){
			$rows[]=$row[$fieldname];
		}
			$query['row_list']=implode('、',$rows);
			return $query;
		}


	  //欄位加減
	  public function _update_field_amount($table,$where,$where_val,$field,$value){
	    //$sql="update FROM $table set $field=$field $value where $field='".$user['id']."' and status=107 ";
	    $sql="update $table set $field=$field $value where $where='$where_val'";
	    return $this->db->query($sql);
		}
    public function _get_products($type,$promotion_label_sn=0,$category_sn=0,$limit=0,$page=0,$sort=0,$attribute_values=0,$min=0,$max=0,$search=0,$ifnew=0,$_where_in=0)
    {
    	if($type){
        $this->db->select('ij_product.product_sn,product_name,pricing_method,category_name,default_channel_name,unispec_flag,unispec_qty_in_stock,promotion_label_content_save_dir,min_price,DATEDIFF(end_date,now()) as end_date,sum(click_number+click_start_number) as click_sum,special_item,default_root_category_sn,open_preorder_flag,promotion_label_name,share_percent');
        $this->db->from("ij_product");
        $this->db->join("ij_category","ij_category.category_sn=ij_product.default_root_category_sn","left");
        $this->db->join("ij_supplier","ij_supplier.supplier_sn=ij_product.supplier_sn","left");
	      $this->db->join("ij_product_promotion_label_relation","ij_product_promotion_label_relation.product_sn=ij_product.product_sn","left");
	      $this->db->join("ij_promotion_label_config","ij_product_promotion_label_relation.promotion_label_sn=ij_promotion_label_config.promotion_label_sn","left");
        if($promotion_label_sn && $promotion_label_sn < 5){
	        $this->db->where('start_date <=',date("Y-m-d H:i:s",time()));
	        $this->db->where('end_date >=',date("Y-m-d H:i:s",time()));
	        $this->db->where('ij_product_promotion_label_relation.promotion_label_sn',$promotion_label_sn);
      	}
        if(!$_where_in && $category_sn){
	        $this->db->group_start()->or_where('ij_product.default_root_category_sn',$category_sn);
	        $this->db->join("ij_product_category_relation","ij_product_category_relation.product_sn=ij_product.product_sn","left");
	        $this->db->or_where('ij_product_category_relation.category_sn',$category_sn)->group_end();
        }elseif($_where_in){
            if($category_sn) array_push($_where_in,$category_sn);
            $this->db->group_start()->or_where_in('ij_product.default_root_category_sn',$_where_in);
            $this->db->join("ij_product_category_relation","ij_product_category_relation.product_sn=ij_product.product_sn","left");
            $this->db->or_where_in('ij_product_category_relation.category_sn',$_where_in)->group_end();
      	}
        if($search){
	        $this->db->group_start()->or_like('ij_product.product_name',$search,'both');
	        $this->db->or_like('ij_product.special_item',$search,'both');
	        $this->db->or_like('ij_product.meta_keywords',$search,'both');
	        $this->db->or_like('ij_product.meta_content',$search,'both')->group_end();
      	}
        if($attribute_values){
	        $this->db->join("ij_product_attribute_log","ij_product_attribute_log.product_sn=ij_product.product_sn","inner");
        	$this->db->where('ij_product_attribute_log.status','1');
	        $this->db->where_in('ij_product_attribute_log.attribute_value_sn',$attribute_values);
        	$this->db->having('count(DISTINCT ij_product_attribute_log.attribute_value_sn)=',count($attribute_values));
      	}
        if($min && $max){
			$this->db->where("min_price BETWEEN $min AND $max");
        }
        if($ifnew){
            $ifnew_date=$this->ijw->date_add_days(date("Y-m-d",time()),-$ifnew);
            $this->db->where("ij_product.last_time_update >=",$ifnew_date);
        }
        $this->db->where('ij_product.default_channel_name',$type);
        $this->db->where('ij_product.product_name <>','');
        $this->db->where('ij_product.product_status','1');
        $this->db->where('ij_product.product_type!=','4');//製作服務費（限加購）
        $this->db->group_by('product_sn');
        if($promotion_label_sn=='6'){ //相關商品隨機排序
	        $this->db->order_by('ij_product.sort_order','RANDOM');
	      }elseif($promotion_label_sn=='2'){   //優惠用到期日
	        	$this->db->order_by('DATEDIFF(end_date,now())');
	      }elseif($promotion_label_sn=='3'){   //推薦用推薦排序順序
	        	$this->db->order_by('recommend_sort_order');
	      }elseif($promotion_label_sn=='5'){   //熱門用點擊次數+點擊起始值
	        	$this->db->order_by('click_sum','desc');
      	  }else{
			switch ($sort) {
				case 'name_ASC':
    		$this->db->order_by('ij_product.product_name','asc');
					break;
				case 'name_DESC':
    		$this->db->order_by('ij_product.product_name','desc');
					break;
				case 'price_ASC':
    		$this->db->order_by('ij_product.min_price','asc');
					break;
				case 'price_DESC':
    		$this->db->order_by('ij_product.min_price','desc');
					break;
				default:
            $this->db->order_by('ij_product_promotion_label_relation.status','desc');
            $this->db->order_by('ij_product_promotion_label_relation.promotion_label_sn','asc');
            $this->db->order_by('ij_product.last_time_update','desc');
        	}
      	}
        if($page >= 1)$page=$page-1;
        if($page && $limit){
            $this->db->limit($limit,$page*$limit);
        }elseif(!$page && $limit){
            $this->db->limit($limit);
        }
        //var_dump($sort);
		$query =$this->db->get()->result_array();
        //echo $this->db->last_query().'<br>';
		if($max){
		}
		    if($query) {
    	        foreach($query as $key => $p){
    	        	$image_path=$this->_select('ij_product_gallery',array("product_sn" => $p['product_sn'],'status'=>'1'),0,0,1,0,0,'row','image_path,image_alt');
    	          $query[$key]['image_path'] = ($image_path['image_path'])? $image_path['image_path']:'product/cover_sample_01.jpg';
    	          $query[$key]['image_alt'] = ($image_path['image_alt'])? $image_path['image_alt']:'目前尚無圖檔';
     	          $query[$key]['promotion'] = $this->_get_promotion_label_by_psn($p['product_sn']); //抓促銷活動
    	        	if($p['default_channel_name']=='囍市集'){
    							if($p['unispec_flag']=='0'){ //多重規格
    								$where = array('product_sn' => $p['product_sn'],'qty_in_stock >' => '0');
    								$query[$key]['mutispec']=$this->_select('ij_mutispec_stock_log',$where,0,0,0,0,0,'result_array','mutispec_stock_sn,color_name,qty_in_stock');
    							}
    			    	}
    	          if($p['pricing_method']=='1'){  //單一
    							$ifprices=$this->_select('ij_product_fix_price_log','product_sn',$p['product_sn'],0,0,'price',0,'num_rows');
    							//$price_tier=$this->_select('ij_product_fix_price_log','product_sn',$p['product_sn'],1,0,'price',0,'row');
    							if($p['min_price']){
    								$query[$key]['price1']=$p['min_price'];
    								$query[$key]['price2']=($ifprices > 1)? '起':''; //多規格-價格
    							}else{
    								$query[$key]['price1']='尚未';
    								$query[$key]['price2']='';
    							}
    	          }else{
    		  				$query[$key]['price1'] = $p['min_price'];
    							$ifprices=$this->_select('ij_product_price_tier_log','product_sn',$p['product_sn'],0,0,'price',0,'num_rows');
    		  				$query[$key]['price2'] = ($ifprices > 1)? '起':''; //多區間
    	          }
                        $this->load->model('libraries_model');
                $query[$key]['attribute_values'] = @$this->libraries_model->_get_product_attribute($p['product_sn'],0,1)["attribute_values"];
    			}
		    }
			/*if($promotion_label_sn=='0' && $sort){
					switch ($sort) {
						case 'price_ASC':
							usort($query, array($this,'sort_by_price_asc'));
							break;
						case 'price_DESC':
							usort($query, array($this,'sort_by_price_desc'));
							break;
						default:
	 						break;
	     		}
	    	}*/
			    return $query;
	    }
  	}
		private function sort_by_price_asc($a, $b)
		{
		    if($a['price1'] == $b['price1']) return 0;
		    return ($a['price1'] > $b['price1']) ? 1 : -1;
		}
		private function sort_by_price_desc($a, $b)
		{
		    if($a['price1'] == $b['price1']) return 0;
		    return ($a['price1'] < $b['price1']) ? 1 : -1;
		}
		private function sort_by_supplier_name($a, $b)
		{
		    if(@$a['supplier_name'] == @$b['supplier_name']) return 0;
		    return (@$a['supplier_name'] < @$b['supplier_name']) ? 1 : -1;
		}
    public function category_attributes($category_sn=0,$attribute_values=0,$if_return_attribute_value=0){
        $this->db->select('ij_category_attribute_log.attribute_sn,attribute_name');
        $this->db->from('ij_category_attribute_log');
        $this->db->order_by('sort_order');
        $this->db->join("ij_attribute_config","ij_attribute_config.attribute_sn=ij_category_attribute_log.attribute_sn","left");
        $this->db->where('ij_category_attribute_log.status',1);
        $this->db->where('ij_attribute_config.status',1);
        $this->db->where('category_sn',$category_sn);
        $array2 = $this->db->get()->result_array();
		//echo $this->db->last_query().'<br>';
        $_array3=array();
			  foreach($array2 as $key2=>$value2) {
			  	 $attributes=$this->_select('ij_attribute_value_config',array("attribute_sn" => $value2['attribute_sn'],'status'=>'1'),0,0,0,'sort_order',0,'result_array','attribute_value_sn,attribute_value_name');
					if($attribute_values){
    			  		foreach($attributes as $key3=>$value3) {
    			  			foreach($attribute_values as $value4) {
    			  				if($value3['attribute_value_name']==$value4){  //找出有選取的屬性值
    			  					$attributes[$key3]['status']='1';
    			  				}
    			  			}
    			  		}
					}else{
                        foreach($attributes as $key3=>$value3) {
                            $_array3[]=$value3;
                        }
                    }
			  	$array2[$key2]['attribute_value']=$attributes;
			  }
              if($if_return_attribute_value){
                return $_array3;
              }else{
                return $array2;
              }
    }
    public function _get_promotion_label_by_psn($product_sn=0){ //產品跟促銷標籤
    	if($product_sn){
	        $this->db->select('ij_product_promotion_label_relation.promotion_label_sn,ij_product_promotion_label_relation.status,start_date,end_date,promotion_label_name,promotion_label_eng_name');
	        $this->db->from('ij_product_promotion_label_relation');
          $this->db->join('ij_promotion_label_config','ij_promotion_label_config.promotion_label_sn=ij_product_promotion_label_relation.promotion_label_sn','left');
					$where = array('product_sn' => $product_sn,'start_date <=' => date("Y-m-d H:i:s",time()),'end_date >=' => date("Y-m-d H:i:s",time()));
	        $this->db->where($where);
	        $this->db->order_by('promotion_label_sn');
	        $array2=$this->db->get()->result_array();
	    	return $array2;
	    }else{
	    	return array();
	    }
  	}
    public function _get_package_price_by_psn($product_sn=0,$incremental=1,$chktime=0,$product_package_config_sn=0,$ifprice=0){ //產品跟特殊規格
    	if($product_sn){
	        $this->db->select('ij_product_fix_price_log.product_package_config_sn,fixed_price,price,product_package_name');
	        $this->db->from('ij_product_fix_price_log');
	        $this->db->where('ij_product_fix_price_log.product_sn',$product_sn);
	        if($product_package_config_sn){
	        	$this->db->where('ij_product_fix_price_log.product_package_config_sn',$product_package_config_sn);
	        }else{
	        	$this->db->where('ij_product_fix_price_log.product_package_config_sn !=','');
	        }
	        if($ifprice){
	        	$this->db->where('price >','0');
	        }
	        $this->db->where('ij_product_fix_price_log.status','1');
            $this->db->join('ij_product_package_config','ij_product_package_config.product_package_config_sn=ij_product_fix_price_log.product_package_config_sn','left');
	        $this->db->order_by('ij_product_package_config.sort_order');
	        $array2=$this->db->get()->result_array();
			//echo $this->db->last_query().'<br>';
	        if(!$chktime){
	        	$chktime=date("Y-m-d H:i:s",time());
	        }
					$where = array('product_sn' => $product_sn,'start_date <=' => $chktime,'end_date >=' => $chktime,'promotion_label_sn'=>'3','status'=>'1'); //3:特價
					$promo_sale=$this->_select('ij_product_promotion_label_relation',$where,0,1,0,'discount_percentage',0,'row','discount_percentage,discount_amount');
					//var_dump($array2);
					foreach($array2 as $key=>$Item){
							if($incremental=='0'){
			      			$array2[$key]['qty_in_stock']=1;  //特定虛擬商品可購買數量為1
							}else{
			      			$array2[$key]['qty_in_stock']=20; //虛擬商品預設購買數量
							}
							$price=$Item['price'];
							if($promo_sale){ //有特價
								if($promo_sale['discount_percentage'] > 0 && $promo_sale['discount_percentage'] < 1){ //有折扣
									$price = round($price - ($price * $promo_sale['discount_percentage']));
								}elseif($promo_sale['discount_amount'] > 0){
									$price = ($price=$price - intval($promo_sale['discount_amount']) > 0)? $price:'1';
								}
			      		$array2[$key]['promo_price']=$price; //增加促銷價
			      	}else{
			      		$array2[$key]['promo_price']=$price;
							}
			      	$array2[$key]['specs']=$this->_get_package_spec($Item['product_package_config_sn']);
					}
	    	return $array2;
	    }else{
	    	return array();
	    }
  	}
  	private function _get_package_spec($product_package_config_sn=0){
  		if($product_package_config_sn){
        $this->db->select('product_package_spec_option_relation_sn,ij_product_package_spec_option_relation.spec_option_config_sn,spec_option_name,usage_limitation,description,spec_option_limitation,ij_product_package_spec_option_relation.status');
        $this->db->from('ij_product_package_spec_option_relation');
        $this->db->join('ij_spec_option_config','ij_spec_option_config.spec_option_config_sn=ij_product_package_spec_option_relation.spec_option_config_sn','left');
        $this->db->where('product_package_config_sn',$product_package_config_sn);
        $this->db->where('ij_spec_option_config.status',1);
        $this->db->order_by('sort_order');
        return $this->db->get()->result_array();
      }else{
      	return array();
      }
  	}
    private function _get_max_tier_price($qty,$product_sn)
    {
				$this->db->select("price");
        $this->db->from("ij_product_price_tier_log");
				$this->db->where('product_sn',$product_sn);
				//$this->db->where('end_qty <',$qty);
				$this->db->limit('1');
				$this->db->order_by('end_qty','desc');
	      return $this->db->get()->row_array()['price'];
  	}
    public function _get_price($qty=0,$product_sn=0,$package=0,$chktime=0){ //產品編號、數量、特殊規格、時間，找售價
       if($qty && $pricing_method = $this->_select('ij_product','product_sn',$product_sn,1,0,0,0,'row','pricing_method')['pricing_method']){
									//var_dump($package).'<br>';
            if($pricing_method=='2'){ //區間定價找出符合區間
							$where = array('start_qty <=' => $qty,'end_qty >=' => $qty,'product_sn' => $product_sn);
							if(!$price=$this->_select('ij_product_price_tier_log',$where,0,1,0,0,0,'row','price')['price']){
								if(!$price=$this->_get_max_tier_price($qty,$product_sn)){ //超出區間
		    					return false;
		    				}
           		//echo $this->db->last_query();
         				//var_dump($price);
							}
          	}elseif($pricing_method=='1'){ //單一定價
          		//var_dump($this->_get_package_price_by_psn($product_sn,1,0,$package)).'<br>';
       			$unispec_flag = $this->_select('ij_product','product_sn',$product_sn,1,0,0,0,'row','unispec_flag')['unispec_flag']; //1:單一規格
          		//var_dump($unispec_flag);
          		//if($unispec_flag && $this->_get_package_price_by_psn($product_sn,1,0,$package)){  // 是否有特殊規格
          		//if($unispec_flag){  // 是否單一規格
					$_where = array('status' => '1','product_sn' => $product_sn);
					if(!$price=$this->_select('ij_product_fix_price_log',$_where,0,1,0,'price',0,'row','price')['price']){
		                //var_dump($this->db->last_query());
					return false;
					}
				/*}else{ //特殊規格價錢不同應該只有婚禮網站商品才有所以先註解掉
					//exit();
					$package_prices=$this->_get_package_price_by_psn($product_sn,0);
                    //var_dump($package).'<br>';
                    //var_dump($package_prices).'<br>';
					if($package){
						foreach($package_prices as $pp){
							if($pp['product_package_config_sn']==$package){
								$price=$pp['price'];
								break;
							}
						}
					}else{
					return false;
					}
				}*/
            }

	        if(!$chktime){
	        	$chktime=date("Y-m-d H:i:s",time());
	        }
					$where = array('product_sn' => $product_sn,'start_date <=' => $chktime,'end_date >=' => $chktime,'promotion_label_sn'=>'3','status'=>'1'); //3:特價
					$promo_sale=$this->_select('ij_product_promotion_label_relation',$where,0,1,0,'discount_percentage',0,'row','discount_percentage,discount_amount');
							//echo $this->db->last_query().'<br>';
					if($promo_sale){
						if($promo_sale['discount_percentage'] > 0 && $promo_sale['discount_percentage'] < 1){ //有折扣
							$price = round($price - ($price * $promo_sale['discount_percentage']));
						}elseif($promo_sale['discount_amount'] > 0){
							$price = ($price=$price - intval($promo_sale['discount_amount']) > 0)? $price:'1';
						}
					}
	    	return $price;
	    }else{
	    	return false;
	    }
  	}
    public function _get_profit_sharing_rate($product_sn) //取消
    {
        $this->db->select('profit_sharing_rate');
        $this->db->from("ij_product");
        $this->db->join("ij_category","ij_product.default_root_category_sn=ij_category.category_sn","left");
        $this->db->where('product_sn',$product_sn);
        return $this->db->get()->row_array()['profit_sharing_rate'];
				//echo $this->db->last_query();
  	}
  	//版型列表
		public function Get_Template(){
			$where = array('status' => '1');
			$product_templates=$this->_select('ij_product_template_config',$where,'',0,0,'',0,'result_array');
			//echo $this->db->last_query();
			$picture=array();
			foreach($product_templates as $key=>$pt){
					$pictures=$this->_Get_distinct_associated_template_color_relation_sn($pt['product_template_sn']);
			//echo $this->db->last_query();
					$picture[$key]['product_template_sn']=$pt['product_template_sn'];
					$picture[$key]['product_template_name']=$pt['product_template_name'];
					$picture[$key]['product_template_eng_name']=$pt['product_template_eng_name'];
					$picture[$key]['template_demo_website']=$pt['template_demo_website'];
					$picture[$key]['template_dir']=$pt['template_dir'];
					$picture[$key]['description']=$pt['description'];
				  foreach($pictures as $key1=>$Item){
						$where = array('product_template_sn' => $pt['product_template_sn'],'associated_template_color_relation_sn'=>$Item['associated_template_color_relation_sn'],'status'=>'1');
				  	$picture[$key]['template_picture_set']=$this->_select('ij_template_picture',$where,'',0,5,0,0,'result_array','image_path');
						//$where = array('product_template_sn' => $pt['product_template_sn'],'status'=>'1');
					  //$website_color_sn=$this->_select('ij_template_color_relation',$where,0,0,0,0,0,'result_array','website_color_sn');
						//echo count($website_color_sn);
					  $picture[$key]['website_color_set']=$this->_Get_template_website_color($pt['product_template_sn']);
						$where = array('product_template_sn' => $pt['product_template_sn'],'status'=>'1');
					  $picture[$key]['default_divider_set']=$this->_select('ij_template_divider_config',$where,'',0,0,'associated_area',0,'result_array','default_divider_save_dir,default_divider_greeting_text,associated_area');
					}


				//echo $this->db->last_query();
			}
			//var_dump($picture);
				//exit();
				return $picture;
		}

 	//商品-版型列表
		public function Get_Product_Template($page=0,$limit=0,$not_product_sn=0){
			//$where = array('status' => '1');
			$product_templates=$this->_List_Product_Templates($page,$limit);
			//echo $this->db->last_query();
			$picture=array();
			foreach($product_templates as $key=>$pt){
				if($pt['product_sn']!=$not_product_sn){
					//$pictures=$this->_Get_distinct_associated_template_color_relation_sn($pt['product_template_sn']);
					$picture[$key]['product_sn']=$pt['product_sn'];
					$picture[$key]['product_name']=$pt['product_name'];
					$picture[$key]['product_template_sn']=$pt['product_template_sn'];
					$picture[$key]['product_template_name']=$pt['product_template_name'];
					$picture[$key]['product_template_eng_name']=$pt['product_template_eng_name'];
					$picture[$key]['template_demo_website']=$pt['template_demo_website'];
					$picture[$key]['template_dir']=$pt['template_dir'];
					$picture[$key]['description']=$pt['description'];
					$picture[$key]['image_path']=$this->_Get_default_Product_associated_template_image($pt['product_sn'],$pt['product_template_sn']);
					if($package=$this->_get_package_price_by_psn($pt['product_sn'],1,0,0,1)){  // 是否有特殊規格
						//echo count($package);
						if(count($package)>1){
							$picture[$key]['price2']='起';
						}else{
							$picture[$key]['price2']='';
						}
						$picture[$key]['product_package_config_sn']=$package[0]['product_package_config_sn']; //給預設特殊規格編號
						$picture[$key]['price1']=$package[0]['price']; //給預設規格價錢
					}else{
						$picture[$key]['product_package_config_sn']='';
					}
				}
			}
			//var_dump($picture);
				//exit();
				return $picture;
		}

		private function _Get_distinct_associated_template_color_relation_sn($product_template_sn){
	    $this->db->select('associated_template_color_relation_sn');
	    $this->db->distinct();
	    $this->db->from('ij_template_picture');
	    $this->db->join('ij_template_color_relation','ij_template_color_relation.template_color_relation_sn=ij_template_picture.associated_template_color_relation_sn','left');
	    $this->db->join('ij_website_color','ij_website_color.website_color_sn=ij_template_color_relation.website_color_sn','left');
	    $this->db->where('ij_template_picture.product_template_sn',$product_template_sn);
	    $this->db->order_by('ij_website_color.sort_order','asc');
	    return $this->db->get()->result_array();
		}
		//取得商品-版型預設圖檔
		private function _Get_default_Product_associated_template_image($product_sn,$product_template_sn){
	    $this->db->select('image_path');
	    $this->db->from('ij_template_picture');
	    $this->db->join('ij_template_color_relation','ij_template_color_relation.template_color_relation_sn=ij_template_picture.associated_template_color_relation_sn','left');
	    $this->db->join('ij_website_color','ij_website_color.website_color_sn=ij_template_color_relation.website_color_sn','left');
	    $this->db->join('ij_product_color_relation','ij_website_color.website_color_sn=ij_product_color_relation.website_color_sn','left');
	    $this->db->where('ij_template_picture.product_template_sn',$product_template_sn);
	    $this->db->where('ij_product_color_relation.product_sn',$product_sn);
	    $this->db->where('ij_template_picture.status','1'); //2016-4-19 增加
	    $this->db->where('ij_product_color_relation.status','1');
	    $this->db->order_by('ij_product_color_relation.sort_order','asc');
	    $this->db->order_by('ij_template_picture.sort_order','asc');
      $this->db->limit(1);
	    return $this->db->get()->row_array()['image_path'];
		}

		private function _Get_template_website_color($product_template_sn){
	    $this->db->select('color_name,color_eng_name,color_code,description');
	    $this->db->from('ij_website_color');
	    $this->db->join('ij_template_color_relation','ij_template_color_relation.website_color_sn=ij_website_color.website_color_sn','left');
	    $this->db->where('ij_template_color_relation.product_template_sn',$product_template_sn);
	    $this->db->where('ij_template_color_relation.status','1');
	    $this->db->order_by('ij_website_color.sort_order','asc');
	    return $this->db->get()->result_array();
		}
		//列出開放商品-版型
		public function _List_Product_Templates($page=0,$limit=0,$product_sn=0){
	    $this->db->select('product_sn,product_name,ij_product_template_config.*');
	    $this->db->from('ij_product');
	    $this->db->join('ij_product_template_config','ij_product_template_config.product_template_sn=ij_product.product_template_sn','left');
	    $this->db->where('product_status','1');
	    //特定商品編號
	    if($product_sn){
	    	$this->db->where('ij_product.product_sn',$product_sn);
	    }
	    $this->db->where('ij_product.product_template_sn !=','null');
	    $this->db->order_by('ij_product.sort_order','asc');
      if($page >= 1)$page=$page-1;
      if($page && $limit){
          $this->db->limit($limit,$page*$limit);
      }elseif(!$page && $limit){
          $this->db->limit($limit);
      }
	    return $this->db->get()->result_array();
		}
 	//商品-版型抓顏色及輪播圖檔
		public function Get_Template_images($product_sn,$website_color_sn=0){
			if($product_sn){
			//$where = array('status' => '1');
			$pt=$this->_List_Product_Templates(0,1,$product_sn);
			//echo $this->db->last_query();
			$picture['product_sn']=$product_sn;
			$picture['product_name']=$pt[0]['product_name'];
			$picture['product_template_sn']=$pt[0]['product_template_sn'];
			//$picture['product_template_name']=$pt[0]['product_template_name'];
			//$picture['product_template_eng_name']=$pt[0]['product_template_eng_name'];
			$picture['template_demo_website']=$pt[0]['template_demo_website'];
			//$picture['template_dir']=$pt[0]['template_dir'];
			//$picture['description']=$pt[0]['description'];

			if($website_color_sn==0){ //找出第一個商品-版型-顏色
				$where = array('product_sn' => $product_sn,'status' =>'1','website_color_sn >'=>'0');
				$website_color_sn=$this->_select('ij_product_color_relation',$where,'',0,1,0,0,'row','website_color_sn')['website_color_sn'];
			}
			$picture['website_color_sn']=$website_color_sn;
			$picture['images']=$this->_Get_all_Product_associated_template_image($product_sn,$pt[0]['product_template_sn'],$website_color_sn);
			$picture['colors']=$this->_Get_all_Product_associated_template_colors($product_sn,$pt[0]['product_template_sn']);
			foreach($picture['colors'] as $color){
				if($color['website_color_sn']==$website_color_sn) $picture['color_name']=$color['color_name'];
			}
			//
			//var_dump($picture['colors']);
			//exit();
			return $picture;
		}else{
			return array();
		}
	}
  //取得商品-版型-顏色所有圖檔
	private function _Get_all_Product_associated_template_image($product_sn,$product_template_sn,$website_color_sn){
		if($product_sn && $product_template_sn && $website_color_sn){
    $this->db->select('image_path,image_alt,color_name,color_eng_name,color_code');
    $this->db->from('ij_template_picture');
    $this->db->join('ij_template_color_relation','ij_template_color_relation.template_color_relation_sn=ij_template_picture.associated_template_color_relation_sn','left');
    $this->db->join('ij_website_color','ij_website_color.website_color_sn=ij_template_color_relation.website_color_sn','left');
    $this->db->join('ij_product_color_relation','ij_website_color.website_color_sn=ij_product_color_relation.website_color_sn','left');
    $this->db->where('ij_template_picture.product_template_sn',$product_template_sn);
    $this->db->where('ij_product_color_relation.product_sn',$product_sn);
    $this->db->where('ij_template_color_relation.website_color_sn',$website_color_sn);
	  $this->db->where('ij_template_picture.status','1'); //2016-4-19 增加
    $this->db->where('image_path <>',null);
    $this->db->order_by('ij_template_picture.sort_order','asc');
    $this->db->limit(5);
    return $this->db->get()->result_array();
  	}
	}
  //取得商品-所有顏色
	private function _Get_all_Product_associated_template_colors($product_sn){
		if($product_sn){
    $this->db->select('ij_product_color_relation.website_color_sn,color_name,color_eng_name,color_code');
    $this->db->from('ij_website_color');
    $this->db->join('ij_product_color_relation','ij_website_color.website_color_sn=ij_product_color_relation.website_color_sn','left');
    $this->db->where('ij_product_color_relation.product_sn',$product_sn);
    $this->db->where('ij_product_color_relation.website_color_sn <>',null);
    $this->db->order_by('ij_website_color.sort_order','asc');
    return $this->db->get()->result_array();
  	}
	}
		//上方購物車
		public function Get_Cart(){
		if($cart=$this->cart->contents()){
			$used_gift_cash=0;
			foreach($cart as $key=>$Item){
					$product=$this->_get_product($Item['id']);
					//$_cart[$key]['contents'] = $Item;
					$cart[$key]['product_sn'] = $Item['id'];
                    $cart[$key]['mutispec_color_name']='單一規格';//預設
                    if(@$product['mutispec']){ foreach($product['mutispec'] as $mu){
                        if($mu['mutispec_stock_sn']==$Item['mutispec_stock_sn']){
                            $cart[$key]['mutispec_color_name']=$mu['color_name'];
                        }
                    }}
					//$cart[$key]['channel_name'] = $product['default_channel_name'];
					$cart[$key]['image_path'] = $product['image_path'];
					$cart[$key]['image_alt'] = $product['image_alt'];
					$used_gift_cash = $used_gift_cash+$Item['gift_cash'];
			}
			usort($cart, array($this,'sort_by_supplier_name'));
			//$cart=array_reverse($cart);
			$cart[0]['used_gift_cash']=$used_gift_cash;
			return $cart;
		}else{
			return false;
		}
	}
	//結帳取得購物車-要結帳的商品
		public function Get_Cart_Checkout(){
		if($cart=$this->cart->contents()){
			$used_gift_cash=0;
			$used_coupon=0;
			$cart1['original_order_sn'] = 0;//預設
			foreach($cart as $key=>$Item){
				if($Item['status']=='3'){
					$product=$this->_get_product($Item['id']);
					//$_cart[$key]['contents'] = $Item;
		 			if(strpos($Item['id'],'_')!==false){ // 過濾_
		  			$Item['id']=substr($Item['id'],0,strpos($Item['id'],'_'));
		  		}
		 			if(@$Item['original_order_sn']){ //有母訂單值異動
						$cart1['original_order_sn'] = $Item['original_order_sn'];
		  		}
				$cart[$key]['product_sn'] = $Item['id'];
					//$cart[$key]['channel_name'] = $product['default_channel_name'];
				//var_dump($cart[$key]['channel_name']);
					$cart[$key]['image_path'] = $product['image_path'];
					$cart[$key]['image_alt'] = $product['image_alt'];
					$used_gift_cash = $used_gift_cash+$Item['gift_cash'];
					if(@$Item['coupon_money'] > 0 && @$Item['coupon_money'] < 1){ //優惠是折扣的話
						$Item['coupon_money']=round(($Item['qty']*$Item['price']) *(1-$Item['coupon_money']));
					}
					$used_coupon = $used_coupon+@$Item['coupon_money'];
			if(@$product['mutispec']){
			 	foreach($product['mutispec'] as $mu){
                  	if($mu['mutispec_stock_sn']==$Item['mutispec_stock_sn']){
                       $cart[$key]['product_package_name']= $mu['color_name'];
                       $cart[$key]['mutispec_color_name']= $mu['color_name'];
                       $cart[$key]['mutispec_warehouse_num']= @$mu['warehouse_num'];
                        //var_dump($mu);exit;
              		}
                }
           }elseif(@$product['Packages']){
           	 foreach(@$product['Packages'] as $pa){
		                  	if($pa['product_package_config_sn']==$Item['mutispec_stock_sn']){
		                       $cart[$key]['product_package_config_sn']= $pa['product_package_config_sn'];
		                       $cart[$key]['product_package_name']= $pa['product_package_name'];
	                  		}
	           }
	         }else{
		         		$cart[$key]['product_package_name']= '單一規格';
           }
 				}else{
					 $this->cart->remove($key); //刪除未選結帳商品
				}
			}
			//$cart=$this->cart->contents();//更新?why need 2017-5-21
			//var_dump($cart);
			usort($cart, array($this,'sort_by_supplier_name'));
			$cart1['cart']=$cart;
			$cart1['gift_cash_total_discount_amount']=$used_gift_cash;
			$cart1['coupon_code_total_cdiscount_amount']=$used_coupon;
			$cart1['total_order_amount']=$this->cart->total();
			$cart1['orginal_total_order_amount']=$used_gift_cash+$this->cart->total()+$used_coupon;
			return $cart1;
		}else{
			return false;
		}
	}

    //購物金餘額2
    public function get_gift_cash($member_sn=0,$yearmonth=0,$cash_type=0){
        $this->db->select('sum(cash_amount) as gift_cash');
        $this->db->from('ij_gift_cash');
        $this->db->where('status','1');
        /*if($_onlyplus){
            $this->db->where('cash_amount>',0);
        }*/
        if($member_sn){
            $this->db->where('associated_member_sn',$member_sn);
        }
        if($cash_type){
            $this->db->where('cash_type',$cash_type);
        }
        if($yearmonth){
            $this->db->like('create_date',$yearmonth);
        }
        $query=$this->db->get()->row_array();
        return ($query)?$query['gift_cash']:'0';
    }

	//購物金餘額
	public function _get_gift_cash($member_sn=0){
		if($member_sn){
            //將過期禮金標為0失效
			/*$data = array('update_member_sn' => '1','last_time_update' => date("Y-m-d H:i:s",time()),'status' => '0');
			$where = array('associated_member_sn' => $member_sn,'status' => '1','eff_end_date <=' => date("Y-m-d H:i:s",time()));//1有效 0失效 2刪除
	        $this->_update('ij_gift_cash',$data,$where);*/
			$where = array('associated_member_sn' => $member_sn,'status' => '1');//1有效 0失效 2刪除
			if($gift_cash=$this->_select('ij_gift_cash',$where,'',0,0,'used_amount',0,'row','sum(cash_amount) as gift_cash')['gift_cash']){ //有效禮金金額
				return $gift_cash;
			}else{
				return 0;
			}
		}else{
			return -2;
		}
	}
	//購物金使用寫入舊版
	public function _sav_gift_cash($member_sn=0,$gift_cash_amount=0){
		if($member_sn && $gift_cash_amount){
            //先將過期禮金標為0失效
			//$data = array('update_member_sn' => '1','last_time_update' => date("Y-m-d H:i:s",time()),'status' => '0');
			//$where = array('associated_member_sn' => $member_sn,'status' => '1','eff_end_date <=' => date("Y-m-d H:i:s",time()));//1有效 0失效 2刪除
	        //$this->_update('ij_gift_cash',$data,$where);

			$where = array('associated_member_sn' => $member_sn,'status' => '1','(cash_amount-used_amount) >' => '0');//1有效 0失效 2刪除
			$gift_cash_array=$this->_select('ij_gift_cash',$where,'',0,0,'eff_end_date','desc','result_array','sum(cash_amount-used_amount) as gift_cash_can_use,gift_cash_sn');  //有效禮金列表到期日優先
			//var_dump($gift_cash_array);
					$this->db->trans_begin();
					foreach($gift_cash_array as $key=>$Item){
						if($Item['gift_cash_sn']){
							if(intval($Item['gift_cash_can_use']) >= intval($gift_cash_amount)){ //足夠
			    			//$this->_update('ij_gift_cash',array('used_amount'),'temp_order_item_sn',$temp_order_array['temp_order_item_sn']);
								$this->Shop_model->_update_field_amount('ij_gift_cash','gift_cash_sn',$Item['gift_cash_sn'],'used_amount','+'.$gift_cash_amount);
								$gift_cash_amount=0;
								break; //已寫入已使用金額跳出
							}else{
								$used_amount = $Item['gift_cash_can_use']; //該筆餘額全部使用
								$this->Shop_model->_update_field_amount('ij_gift_cash','gift_cash_sn',$Item['gift_cash_sn'],'used_amount','+'.$used_amount);
								$gift_cash_amount=$gift_cash_amount-$Item['gift_cash_can_use']; //尚未寫入金額
							}
						}
				}
				//$this->db->trans_complete();
				//$this->db->trans_off();
				if($gift_cash_amount > 0 ){
					$this->db->trans_rollback();   //餘額不足roll back
					$this->Clean_Cart_gift_cash(); //購物金不足清空已勾選折抵的購物車清單
				}else{
					$this->db->trans_commit();
					$this->db->trans_off();
				}
				return $gift_cash_amount;
		}else{
			return 0; //無異動
		}
	}

	//購物金不足清空已勾選折抵的購物車清單
		public function Clean_Cart_gift_cash(){
		if($cart=$this->cart->contents()){
			$used_gift_cash=0;
	    $gift_cash=intval($this->session->userdata('gift_cash'));
			foreach($cart as $key=>$Item){
					if($Item['gift_cash'] > 0){
						$used_gift_cash = $used_gift_cash+$Item['gift_cash'];
						$data = array(
						        'rowid' => $key,
						        'gift_cash'  => 0
						);
						$this->cart->update($data);
    			}
			}

		  $this->session->set_userdata('gift_cash',$this->_get_gift_cash($this->session->userdata['web_member_data']['member_sn'])); //重抓
			return true;
		}else{
			return false;
		}
	}

	// 有效取得暫存訂單編號，無效新增
	public function _get_temp_shopping_cart_order_sn($member_sn,$ifaddnew=1){
		$where = array('associated_member_sn' => $member_sn,'temp_shopping_cart_status' => '1');//1有效 0失效 2刪除 3 已購
		if(!$temp_shopping_cart_order_sn=$this->_select('ij_temp_shopping_cart',$where,'',0,0,'temp_shopping_cart_order_sn',0,'row','temp_shopping_cart_order_sn')['temp_shopping_cart_order_sn']){
			if($ifaddnew){
		    $temp_shopping_cart = array(
		        'associated_member_sn'      => $member_sn,
		        'temp_shopping_cart_status'  => '1',
		        'create_date'  => date("Y-m-d H:i:s",time())
		    );
		    $temp_shopping_cart_order_sn=$this->_insert('ij_temp_shopping_cart',$temp_shopping_cart);
		  }else{
	  		$temp_shopping_cart_order_sn=false;
		  }
  	}
		return $temp_shopping_cart_order_sn;
	}
	//暫存訂單細項-新增、修改
	public function save_temp_order($member_sn,$cart_array,$status=3){
		if($member_sn && $cart_array){
			$temp_order = array();
		  if(strpos($cart_array['id'],'_')!==false){ // 過濾_
  			$cart_array['id']=substr($cart_array['id'],0,strpos($cart_array['id'],'_'));
  		}
			$product=$this->_select('ij_product','product_sn',$cart_array['id'],0,0,'',0,'row','pricing_method,currency_code');
			$temp_shopping_cart_order_sn=$this->_get_temp_shopping_cart_order_sn($member_sn);
			if($cart_array['coupon_money'] > 0 && $cart_array['coupon_money'] < 1){ //優惠是折扣的話
				$cart_array['coupon_money']=round(($cart_array['qty']*$cart_array['price']) *(1-$cart_array['coupon_money']));
			}
	    $temp_order = array(
	        'temp_shopping_cart_order_sn'      => $temp_shopping_cart_order_sn,
	        'product_sn'   => $cart_array['id'],
	        'product_name' => $cart_array['name'],
	        //'product_package_config_sn'   => $cart_array['id'],
	        //'product_package_name'   => $cart_array['id'],
	        'mutispec_stock_sn'   => $cart_array['mutispec_stock_sn'],
	        //'category_spec_option_relation_sn'   => $cart_array['id'],
	        //'product_package_spec_option_relation_sn'   => $cart_array['id'],
	        'buy_amount'   => $cart_array['qty'],
	        'pricing_method'   => $product['pricing_method'],
	        'sales_price'   => $cart_array['price'],
	        'currency_code'   => $product['currency_code'],
	        'addon_log_sn'   => $cart_array['addon_log_sn'],
	        'gift_cash_discount_amount'   => $cart_array['gift_cash'],
	        'coupon_discount_amount'   => $cart_array['coupon_money'],
	        'status'   => $status,
	        'last_time_update'  => date("Y-m-d H:i:s",time())
	    );

			$where = array('temp_shopping_cart_order_sn' => $temp_shopping_cart_order_sn,'product_sn' => $cart_array['id']);//是否已有資料
			if($cart_array['mutispec_stock_sn']) $where['mutispec_stock_sn']=$cart_array['mutispec_stock_sn'];
			if($cart_array['addon_log_sn']) $where['addon_log_sn']=$cart_array['addon_log_sn'];
			$temp_order_array=$this->_select('ij_temp_order_item',$where,0,0,0,'temp_order_item_sn',0,'row','temp_order_item_sn,mutispec_stock_sn',0,0,0,0);
            //var_dump($status);exit;
			if($temp_order_array){
                if($status==2){ //刪除
                    $this->_delete('ij_temp_order_item',$where);
                }else{
    				//if($temp_order_array['mutispec_stock_sn'] && $temp_order_array['mutispec_stock_sn']!=$cart_array['mutispec_stock_sn']){ //同商品已有規格且規格不同的話，新增
    	    	//	$this->_insert('ij_temp_order_item',$temp_order);
    				//}else{
    	    		$this->_update('ij_temp_order_item',$temp_order,'temp_order_item_sn',$temp_order_array['temp_order_item_sn']);
    				//}
                }
			}else{
	    	    $this->_insert('ij_temp_order_item',$temp_order);
			}
		}
	}
	//暫存訂單修改
	public function update_temp_order($product_sn,$cart_array,$member_sn,$status=3,$rowid=0,$ifadd=0,$supplier_sn_1=0){ // status 1有效 2刪除 3已購
		if($product_sn && $cart_array && $member_sn){
		    if(strpos($product_sn,'_')!==false){ // 過濾_
  			   $product_sn=substr($product_sn,0,strpos($product_sn,'_'));
  		    }
			$temp_order = array();
			$product=$this->_select('ij_product','product_sn',$product_sn,0,0,'',0,'row','pricing_method,currency_code');
			$temp_shopping_cart_order_sn=$this->_get_temp_shopping_cart_order_sn($member_sn);
			if($status=='2'){
				$cart_array['gift_cash']=0; //清除購物金
			}
			if($cart_array['coupon_money'] > 0 && $cart_array['coupon_money'] < 1){ //優惠是折扣的話
				$cart_array['coupon_money']=round(($cart_array['qty']*$cart_array['price']) *(1-$cart_array['coupon_money']));
			}
	    $temp_order = array(
	        'temp_shopping_cart_order_sn'      => $temp_shopping_cart_order_sn,
	        'product_sn'   => $cart_array['id'],
	        'product_name' => $cart_array['name'],
	        //'product_package_config_sn'   => $cart_array['id'],
	        //'product_package_name'   => $cart_array['id'],
	        'mutispec_stock_sn'   => $cart_array['mutispec_stock_sn'],
	        //'category_spec_option_relation_sn'   => $cart_array['id'],
	        //'product_package_spec_option_relation_sn'   => $cart_array['id'],
	        'buy_amount'   => $cart_array['qty'],
	        'pricing_method'   => $product['pricing_method'],
	        'sales_price'   => $cart_array['price'],
	        'currency_code'   => $product['currency_code'],
	        'addon_log_sn'   => $cart_array['addon_log_sn'],
	        'gift_cash_discount_amount'   => $cart_array['gift_cash'],
	        'coupon_discount_amount'   => $cart_array['coupon_money'],
	        'status'   => $status,
	        'last_time_update'  => date("Y-m-d H:i:s",time())
	    );
	    //var_dump($temp_order);
			//$where = array('temp_shopping_cart_order_sn' => $temp_shopping_cart_order_sn,'product_sn' => $product_sn,'mutispec_stock_sn' => $cart_array['mutispec_stock_sn']);//是否已有同商品資料
            //$where = array('temp_shopping_cart_order_sn' => $temp_shopping_cart_order_sn,'product_sn' => $product_sn,'status'=>'3');//是否已有同狀態的同商品資料
            $where = array('temp_shopping_cart_order_sn' => $temp_shopping_cart_order_sn,'product_sn' => $product_sn);
			//if($cart_array['mutispec_stock_sn']) $where['mutispec_stock_sn']=$cart_array['mutispec_stock_sn']; 因為可能後來才補選規格
			if($cart_array['addon_log_sn']) $where['addon_log_sn']=$cart_array['addon_log_sn'];
			$temp_order_array=$this->_select('ij_temp_order_item',$where,0,0,0,'temp_order_item_sn',0,'row','temp_order_item_sn,mutispec_stock_sn',0,0,0,0);
			if($temp_order_array){
				//var_dump($temp_order_array);
				//var_dump($cart_array);
				if($temp_order_array['mutispec_stock_sn'] && $temp_order_array['mutispec_stock_sn']!=$cart_array['mutispec_stock_sn']){ //同商品已有規格且規格不同的話，新增
    	    		if($ifadd){
    	    			$this->_insert('ij_temp_order_item',$temp_order);
    	    		}else{
    	    			$this->_update('ij_temp_order_item',$temp_order,'temp_order_item_sn',$temp_order_array['temp_order_item_sn']);
    	    		}
				}else{
                    if($status==2){ //刪除
                        $this->_delete('ij_temp_order_item',$where);
                    }else{
	    		        $this->_update('ij_temp_order_item',$temp_order,'temp_order_item_sn',$temp_order_array['temp_order_item_sn']);
                    }
				}
		//echo $this->db->last_query();

			}else{
	    	    $this->_insert('ij_temp_order_item',$temp_order);
			}
			if($rowid){ //有加購
					if($cart=$this->cart->contents()){
						foreach($cart as $key=>$Item){
							if($Item['addon_from_rowid']==$rowid){ //找出加購rowid
								$data = array(
						        'rowid' => $key,
						        'status'=>$status
								);
								$this->cart->update($data); //更新加購商品狀態
								$this->save_temp_order($member_sn,$Item,$status);
							}
						}
					}
			}
			//exit();
	    return $this->get_temp_order($member_sn,$temp_shopping_cart_order_sn,0,0,'',$supplier_sn_1);
		}
	}

	//統計並計算是否需要運費（暫存訂單）
	public function get_temp_order($member_sn,$temp_shopping_cart_order_sn=0,$method_shipping=0,$chk_order_coupon=0,$money_field='delivery_amount',$supplier_sn_1=0){
        //var_dump($supplier_sn_1);

		if(!$temp_shopping_cart_order_sn){
			$temp_shopping_cart_order_sn=$this->_get_temp_shopping_cart_order_sn($member_sn);
		}
		if(!$method_shipping && @$this->session->userdata('method_shipping')){
			$method_shipping=$this->session->userdata('method_shipping');
		}
        $delivery_method_array=$this->session->userdata('method_shipping');
        if($delivery_method_array[0]=='台灣本島'){
            $money_field='delivery_amount';
        }else if($delivery_method_array[0]=='外島地區'){
            $money_field='outland_amount';
        }
		//echo $temp_shopping_cart_order_sn;
		if($member_sn && $temp_shopping_cart_order_sn){
				$where = array('temp_shopping_cart_order_sn' => $temp_shopping_cart_order_sn,'status' => '3');//1有效 2刪除 3已購
				$temp_order_array=$this->_select('ij_temp_order_item',$where,0,0,0,'addon_log_sn',0,'row','count(*) as total_items,sum(buy_amount*sales_price) as sub_total,sum(gift_cash_discount_amount) as gift_cash_total,sum(coupon_discount_amount) as coupon_total',0,0,0,0);
        //echo $this->db->last_query();
				$sub_total=(!is_null($temp_order_array['sub_total']))? $temp_order_array['sub_total']:'0';
				$gift_cash_total=(!is_null($temp_order_array['gift_cash_total']))? $temp_order_array['gift_cash_total']:'0';
				$coupon_total=(!is_null($temp_order_array['coupon_total']))? $temp_order_array['coupon_total']:'0'; //單一商品累加
				if(!@$this->session->userdata('coupon_data')['rowid'] && $coupon_data=$this->session->userdata('coupon_data')){ //有整筆優惠判斷是否適用
					$coupon_ok=true; //預設符合
					if($coupon_data['apply_dealer']){ //判斷經銷商
						$_dealer_array=explode(';',$coupon_data['apply_dealer']);
						$_where=array('ij_member_dealer_relation.member_sn'=>$member_sn);
						if(!$this->libraries_model->get_dealer_members($_dealer_array,'ij_member_dealer_relation.member_sn','dealer_sn',0,$_where)){
							$coupon_ok=false; //不屬於該經銷商s
							$_message='很抱歉!您輸入的優惠代碼僅限特定經銷商所屬會員使用!';
						}
					}
					switch($coupon_data['apply_TA']){ //判斷適用對象
						case "1;1;0":
							$member_level_types=array('2','3','4');
							$_message='很抱歉!您輸入的優惠代碼僅限經銷、聯盟會員使用!';
						break;
						case "1;0;0":
							$member_level_types=array('3','4');
							$_message='很抱歉!您輸入的優惠代碼僅限經銷會員使用!';
						break;
						case "0;1;0":
							$member_level_types=array('2');
							$_message='很抱歉!您輸入的優惠代碼僅限聯盟會員使用!';
						break;
						case "0;0;1":
							$member_level_types=array('1','9');
							$_message='很抱歉!您輸入的優惠代碼僅限一般會員使用!';
						break;
						case "1;0;1":
							$member_level_types=array('1','3','4','9');
							$_message='很抱歉!您輸入的優惠代碼僅限一般、經銷會員使用!';
						break;
						case "0;1;1":
							$member_level_types=array('1','2','9');
							$_message='很抱歉!您輸入的優惠代碼僅限一般、聯盟會員使用!';
						break;
						default: //1;1;1
							$member_level_types=array('1','2','3','4','9');
							$_message='很抱歉!您輸入的優惠代碼僅限一般、經銷、聯盟會員使用!';
						break;
					}
					$_where=array('member_sn'=>$member_sn);
					if(!$this->libraries_model->get_member_by_where_in('member_level_type',$member_level_types,'member_sn',$_where)){
						$coupon_ok=false; //不屬於該適用對象
					}

					if($coupon_ok){ //適用該優惠
						if($coupon_data['discount_type']=='1'){ //金額 計算
							$coupon_money= (int)$coupon_data['discount_amount'];
						}else{
							if($coupon_data['discount_percent'] < 1) {
								$coupon_money=round((1-$coupon_data['discount_percent'])*$sub_total);
							}else{
								$coupon_money=0;
							}
						}
						if(!$coupon_data['min_amount_for_discount'] || $sub_total > $coupon_data['min_amount_for_discount']){ //符合滿額優惠
							if($sub_total-$gift_cash_total > $coupon_money){
								//$coupon_total=$coupon_money;
							}else{ //結帳金額低於優惠金額，優惠金額改為結帳金額
								$coupon_money=$sub_total-$gift_cash_total;
							}
							//var_dump($coupon_total);
							//var_dump($coupon_money);
							if($coupon_total < $coupon_money){
								$coupon_total=$coupon_money;
							}
						}else{
							if($chk_order_coupon){
								return '您輸入的優惠代碼需滿 '.number_format($coupon_data['min_amount_for_discount']).' 元才有優惠喔';
							}
						  $coupon_ok=false; //不適用
							//$this->session->set_flashdata('message','您輸入的優惠代碼需滿 '.$coupon_data['min_amount_for_discount'].' 元才有優惠喔');
						}
					}else{
							if($chk_order_coupon){
								return $_message;
								exit();
							}
					}
				}
				$where = array('temp_shopping_cart_order_sn' => $temp_shopping_cart_order_sn,'status' => '3','gift_cash_discount_amount >'=>'0');
				$gift_cash_items=$this->_select('ij_temp_order_item',$where,0,0,0,'addon_log_sn',0,'num_rows',0,0,0,0,0);
				//找出配送方式與計算運費是否免運
		    //$this->db->select('associated_payment_method,free_delivery,associated_delivery_method,product_type,buy_amount,sales_price,gift_cash_discount_amount,coupon_discount_amount,supplier_sn');
            $this->db->select('sum(buy_amount*sales_price-gift_cash_discount_amount-coupon_discount_amount) as supplier_total,supplier_sn');
		    $this->db->from('ij_temp_order_item');
		    $this->db->join('ij_product','ij_product.product_sn=ij_temp_order_item.product_sn','left');
		    $this->db->where('temp_shopping_cart_order_sn',$temp_shopping_cart_order_sn);
            $this->db->where('ij_temp_order_item.status','3');
            $this->db->where('ij_product.free_delivery','1');//add
            $this->db->where('ij_product.product_type !=','2');//add
            $this->db->group_by('supplier_sn');//add
		    $temp_order_items=$this->db->get()->result_array();
			//echo $this->db->last_query();
            //$free_shop_money=$this->Shop_model->_select('ij_system_param_config','system_param_name','滿額免運門檻',0,0,0,0,'row',0,0,0,0)['system_param_content'];
            //var_dump($temp_order_items);exit;
            $ship=0;
            $free_shop_money=0;
		    if($temp_order_items){
                $real_product_type=true;
			    $free_delivery_total=0;
				$free_delivery=0;
					foreach($temp_order_items as $_key=>$_Item){
                        $free_shop_money=$this->Shop_model->_select('ij_supplier','supplier_sn',$_Item['supplier_sn'],0,0,'min_amount_for_noshipping',0,'row',0,0,0,0)['min_amount_for_noshipping'];
                    //var_dump($free_shop_money);
                    //var_dump($Item['supplier_total']);
                        if($_Item['supplier_total'] >= $free_shop_money){
                            $free_delivery=1;//免運
                        }else{
                    //var_dump($delivery_method_array);
                            if(isset($delivery_method_array[$_Item['supplier_sn']])){ //有選配送方式才加總運費
                                $delivery_method=$this->Shop_model->_select('ij_delivery_method_ct','delivery_method',$delivery_method_array[$_Item['supplier_sn']],0,0,0,0,'row','delivery_amount,delivery_method_name,outland_amount');
                                $ship+=$delivery_method[$money_field];
                            }
                        }
					}
                    //var_dump($ship);

					/*if(@$coupon_data && @$coupon_ok){ //有優惠且適用優惠：判斷是否免運
						if($coupon_data['discount_type']=='1'){ //金額 計算
							$coupon_money= (int)$coupon_data['discount_amount'];
						}else{
							if($coupon_data['discount_percent'] < 1) {
								$coupon_money=round((1-$coupon_data['discount_percent'])*$free_delivery_total);
							}else{
								$coupon_money=0;
							}
						}
						$free_delivery_total=$free_delivery_total-$coupon_money;
					}*/
					//echo $coupon_money;
					//echo $free_delivery_total;

					//echo $free_delivery;

				//var_dump($this->Shop_model->_select('ij_system_param_config','system_param_name','滿額免運門檻',0,0,0,0,'row',0,0,0,0)['system_param_content']);
                }else{
                    $real_product_type=false;//全部都是虛擬
                }
					//配送方式
					if($temp_order_array['total_items']>0){ //有勾選商品
						if($real_product_type){ //非虛擬
							 //$temp_order_array['delivery_method']=$this->Shop_model->_select('ij_delivery_method_ct','display_flag','1',0,0,0,0,'result_array');
							 $temp_order_array['delivery_method']=$this->Shop_model->_get_delivery($temp_shopping_cart_order_sn);
                             //echo $temp_shopping_cart_order_sn;
							//var_dump($method_shipping);
                            if($temp_order_array['delivery_method']){
    							foreach($temp_order_array['delivery_method'] as $key=>$Item){
                                    if($Item['delivery_method']==$method_shipping){ //選中方式
                                        $temp_order_array['delivery_method'][$key]['status']='1';
                                    }
    								if($Item['delivery_amount']>0 && $free_delivery){
    									 $temp_order_array['delivery_method'][$key]['delivery_amount']=0; //滿額免運
    								}

    								$temp_order_array['delivery_method'][$key]['description']=str_replace('[free_shop_money]',$free_shop_money,$Item['description']);
    							}
                            }
						}else{ //虛擬商品線上開通
							$temp_order_array['delivery_method']=$this->Shop_model->_select('ij_delivery_method_ct','delivery_method_name','線上開通',0,0,0,0,'result_array');
						}
						//if(!$free_delivery && count($temp_order_array['delivery_method'])==1){ 先拿掉測試
						if(count($temp_order_array['delivery_method'])==1){
							//$ship=$temp_order_array['delivery_method'][0][$money_field]; //唯一配送方式的話給運費值 增加動態欄位(本島、外島)
							$temp_order_array['delivery_method'][0]['status']='1'; //唯一選擇
						}
					}else{
						$temp_order_array['delivery_method']=$this->Shop_model->_select('ij_delivery_method_ct','display_flag','1',0,0,0,0,'result_array');
						foreach($temp_order_array['delivery_method'] as $key=>$Item){
							$temp_order_array['delivery_method'][$key]['description']=str_replace('[free_shop_money]',$free_shop_money,$Item['description']);
						}
					}
					/*if($method_shipping){ //依據選擇配送方式決定運費
                        //var_dump($delivery_method_array[$Item['supplier_sn']]);
						$delivery_method=$this->Shop_model->_select('ij_delivery_method_ct','delivery_method',$delivery_method_array[$Item['supplier_sn']],0,0,0,0,'row','delivery_amount,delivery_method_name,outland_amount');
						if($free_delivery && $delivery_method[$money_field]>0){ //滿額免運
							$ship=0;
						}else{
							$ship=$delivery_method[$money_field];
						}
					}*/
				//exit();
            if($temp_order_array){
                //var_dump($temp_order_array['sub_total']);
					$temp_order_array['gift_cash_total']=$gift_cash_total; //購物金折抵
					$temp_order_array['gift_cash_items']=$gift_cash_items; //購物金折抵商品數
					$temp_order_array['coupon_total']=$coupon_total; //優惠折抵
                    $temp_order_array['shipping_charge_amount']=$ship; //運費
                    if($supplier_sn_1){
                        $temp_order_array['supplier_subtotal']=$this->Shop_model->get_supplier_ship($supplier_sn_1); //單一店家小計
                    }else{
                        $temp_order_array['supplier_subtotal']='0';
                    }
					$temp_order_array['total_order_amount']=$sub_total-$gift_cash_total-$coupon_total+$ship; //總total
				return $temp_order_array;
			}else{
				$temp_order_array['sub_total']='0'; //總計
				$temp_order_array['gift_cash_total']='0'; //購物金折抵
				$temp_order_array['gift_cash_items']='0'; //購物金折抵商品數
				$temp_order_array['coupon_total']='0'; //優惠折抵
				$temp_order_array['shipping_charge_amount']='0'; //運費
				$temp_order_array['total_order_amount']='0'; //總total
                $temp_order_array['supplier_subtotal']='0';
				$temp_order_array['delivery_method']=$this->Shop_model->_select('ij_delivery_method_ct','display_flag','1',0,0,0,0,'result_array');
				//foreach($temp_order_array['delivery_method'] as $key=>$Item){
				//	$temp_order_array['delivery_method'][$key]['description']=str_replace('[free_shop_money]',$free_shop_money,$Item['description']);
				//}

				return $temp_order_array;
				//return array();
			}
		}
		return array();
	}

	//購物送出後更新暫存購物清單
	public function update_new_temp_order($member_sn=0,$temp_shopping_cart_order_sn=0){
		if(!$member_sn){
			$member_sn=$this->session->userdata['web_member_data']['member_sn'];
		}
		if(!$temp_shopping_cart_order_sn){
			$temp_shopping_cart_order_sn=$this->_get_temp_shopping_cart_order_sn($member_sn);
		}
		if($member_sn && $temp_shopping_cart_order_sn){
			$dara = array('last_time_update' => date("Y-m-d H:i:s",time()),'temp_shopping_cart_status' => '3');//1有效 0失效 2刪除 3 已購
    	    $this->_update('ij_temp_shopping_cart',$dara,'temp_shopping_cart_order_sn',$temp_shopping_cart_order_sn); //異動暫存編號狀態3
			$new_temp_shopping_cart_order_sn=$this->_get_temp_shopping_cart_order_sn($member_sn); //要新編號
			$dara2 = array('last_time_update' => date("Y-m-d H:i:s",time()),'temp_shopping_cart_order_sn' => $new_temp_shopping_cart_order_sn);
			$where = array('temp_shopping_cart_order_sn' => $temp_shopping_cart_order_sn,'status' => '1');//1有效 2刪除 3已購
	    	$this->_update('ij_temp_order_item',$dara2,$where); //異動狀態1的商品到新編號下
	    	return $new_temp_shopping_cart_order_sn;
		}
	}
	//暫存訂單寫回購物車（會員登入時或購物完成之後）
	public function get_temp_order_to_cart($member_sn){
		if($member_sn){
			$temp_shopping_cart_order_sn=$this->_get_temp_shopping_cart_order_sn($member_sn,0);
			if($temp_shopping_cart_order_sn){
				$where = array('temp_shopping_cart_order_sn' => $temp_shopping_cart_order_sn,'status !=' => '2');//1有效 2刪除 3已放入結帳清單但可能沒有完成購物所以改抓不是2的
				$temp_order_array=$this->_select('ij_temp_order_item',$where,0,0,0,'temp_order_item_sn',0,'result_array');

				//$temp_shopping_cart_order_sn=$this->update_new_temp_order($member_sn,$temp_shopping_cart_order_sn); //更新暫存購物清單並取得新編號
			//echo $this->db->last_query();
			//var_dump($temp_order_array);
			//exit();
				$addon_count=0; //主商品-加購筆數
				$rowid=0;
				$qty=0;
				foreach($temp_order_array as $key=>$temp_order_item){
					//$this->Shop_model->_get_price($qty,$temp_order_item['product_sn']);
                    $product = $this->Shop_model->_get_product($temp_order_item['product_sn'],'supplier_name');
					if(!$temp_order_item['addon_log_sn']){ //加價後面處理
        		        $data = array(
        		            'id'      => $temp_order_item['product_sn'].'_'.$temp_order_item['mutispec_stock_sn'],
        		            'qty'     => $temp_order_item['buy_amount'],
        		            'price'   => $this->Shop_model->_get_price($temp_order_item['buy_amount'],$temp_order_item['product_sn'],$temp_order_item['mutispec_stock_sn']),
        		            'name'    => $temp_order_item['product_name'],
        		            'gift_cash'    => 0,
        		            'addon_log_sn'  => $temp_order_item['addon_log_sn'],  //加購用
        			          'addon_from_rowid'  => '', //加購用
        		            'addon_count'  => 0, //主商品-加購筆數 ，加購商品：主商品購買數
        		            'status'  => $temp_order_item['status'],
        	                'coupon_code_id'  => '',  // 優惠id
        	                'coupon_money'  => 0,  // 優惠金額
                            'original_order_sn'  => '',
        		            'mutispec_stock_sn'  => $temp_order_item['mutispec_stock_sn'],
                            'supplier_name'  => $product['supplier_name']
        		        );
                    //var_dump($data);
        		        $rowid=$this->cart->insert($data);
        		        $qty=$temp_order_item['buy_amount'];
        		        $addon_count=0; // 主商品歸0
					}else{
						$addon_count++; //加購商品數+1
						$addon_row=$this->_get_addon_row($temp_order_item['addon_log_sn']); //加價購資料
                    //var_dump($addon_row).'<p>&nbsp;</p>';
						if(($temp_order_item['buy_amount'] > $addon_row['unispec_qty_in_stock']) || $addon_row['open_preorder_flag']=='1' || $addon_row['product_type']>'1'){ //加購數量超過庫存量
							$temp_order_item['buy_amount']=$addon_row['unispec_qty_in_stock'];
						}

    			        $data = array(
    			            'id'      => $temp_order_item['product_sn'].'_'.$rowid,
    			            'qty'     => $temp_order_item['buy_amount'],
    			            'price'   => $addon_row['addon_price'],
    			            'name'    => $temp_order_item['product_name'],
    			            'gift_cash'    => 0,
    			            'addon_log_sn'  => $temp_order_item['addon_log_sn'],  //加購用
    				        'addon_from_rowid'  => $rowid, //加購用
    			            'addon_count'  => $qty, //主商品-加購筆數 ，加購商品：主商品購買數
    			            'addon_limitation'  => $addon_row['addon_limitation'],
    			            'status'  => $temp_order_item['status'],
    	                    'coupon_money'  => 0,  // 優惠金額
    			            'mutispec_stock_sn'  => $temp_order_item['mutispec_stock_sn'], //主商品規格
                            'supplier_name'  => @$product['supplier_name'] //跟隨主商品館別
    			        );
						//var_dump($product).'<br>';exit;
			            $this->cart->insert($data);
                        //var_dump($data).'<br>';
						//更新主商品資料
						$data = array(
						        'rowid' => $rowid,
						        'addon_count'  => $addon_count
						);
						$this->cart->update($data);
                        //var_dump($this->cart->contents());
					}
				}
			}
            //var_dump($this->cart->contents());
            //exit();
		}
	}
	//抓加價購資料
  public function _get_addon_row($addon_log_sn=0)
  {
  	if($addon_log_sn){
      $this->db->select('addon_log_sn,addon_price,product_orginal_price,product_name,addon_limitation,associated_product_sn,unispec_flag,unispec_qty_in_stock,apply_product_sn,open_preorder_flag,product_type');
          $this->db->from("ij_addon_log");
          $this->db->join("ij_addon_config","ij_addon_config.addon_config_sn=ij_addon_log.addon_config_sn","left");
          $this->db->join("ij_product","ij_product.product_sn=ij_addon_log.associated_product_sn","left");
          $this->db->where('addon_log_sn',$addon_log_sn);
          $this->db->where('ij_addon_log.status','1');
          $this->db->where('product_status','1'); //add
          //$this->db->where('unispec_qty_in_stock >','0');
			$query =$this->db->get()->row_array();
	    return $query;
  	}
	}
	//抓取訂單付款方式交集
  public function _get_payments(){
		$member_sn=$this->session->userdata['web_member_data']['member_sn'];
		$temp_shopping_cart_order_sn=$this->_get_temp_shopping_cart_order_sn($member_sn);
  	if($temp_shopping_cart_order_sn){
      $this->db->select('ij_product.product_sn,associated_payment_method');
      $this->db->from("ij_product");
      $this->db->join("ij_temp_order_item","ij_temp_order_item.product_sn=ij_product.product_sn","left");
      $this->db->where('temp_shopping_cart_order_sn',$temp_shopping_cart_order_sn);
      $this->db->where('status','3'); //確認結帳
			$query =$this->db->get()->result_array();
			//echo $this->db->last_query();
				//var_dump($order_sn);
				//var_dump($query).'<br>';
			foreach($query as $key=>$Item){
				if($Item['associated_payment_method']){
					//echo $Item['associated_payment_method'].':'.$Item['product_sn'].'<br>';
					$payment_array=explode(':',$Item['associated_payment_method']);
					foreach($payment_array as $key2=>$Item2){
						//$payments[$key][$Item2]=$this->Shop_model->_select('ij_payment_method_ct','payment_method',$Item2,0,0,0,0,'row','payment_method_name')['payment_method_name'];
						$payments[$key][$key2]=$Item2;
					}
					if(@$payment){
						//var_dump($payment).'<br>';
						//var_dump($payments[$key]).'<br>';
						$payment=array_intersect($payment,$payments[$key]);
						//var_dump($payment).'<br>';
					}else{
						$payment=$payments[$key];
					}
				}else{
					//預設
					$allpayment=$this->_select('ij_payment_method_ct','display_flag','1',0,0,0,0,'result_array');
				}
			}
			if(@$payment){
				foreach($payment as $key=>$Item){
					//echo $Item.'<br>';
					$samepayment[$key]=$this->_select('ij_payment_method_ct','payment_method',$Item,0,0,0,0,'row');
				}
			//var_dump($samepayment);
			//exit();

				return $samepayment;
			}else{
				//$allpayment=$this->_select('ij_payment_method_ct','display_flag','1',0,0,0,0,'result_array');
				return @$allpayment;
			}
		}else{
			return array();
		}
  }
	//抓取勾選商品配送方式交集
  public function _get_delivery($temp_shopping_cart_order_sn=0){
  	if($temp_shopping_cart_order_sn){
      $this->db->select('ij_product.product_sn,associated_delivery_method');
      $this->db->from("ij_product");
      $this->db->join("ij_temp_order_item","ij_temp_order_item.product_sn=ij_product.product_sn","left");
      $this->db->where('temp_shopping_cart_order_sn',$temp_shopping_cart_order_sn);
      $this->db->where('ij_temp_order_item.status','3');
      $this->db->where('product_type !=','2');
			$query =$this->db->get()->result_array();
			//echo $this->db->last_query();
				//var_dump($order_sn);
				//var_dump($query).'<br>';
            $alldelivery=array();
			foreach($query as $key=>$Item){
				if($Item['associated_delivery_method']){
					//echo $Item['associated_delivery_method'].':'.$Item['product_sn'].'<br>';
					$delivery_array=explode(':',$Item['associated_delivery_method']);
					foreach($delivery_array as $key2=>$Item2){
						//$deliverys[$key][$Item2]=$this->Shop_model->_select('ij_delivery_method_ct','delivery_method',$Item2,0,0,0,0,'row','delivery_method_name')['delivery_method_name'];
						$deliverys[$key][$key2]=$Item2;
					}
					if(@$delivery){
						//var_dump($delivery).'<br>';
						//var_dump($deliverys[$key]).'<br>';
						$delivery=array_intersect($delivery,$deliverys[$key]);
						//var_dump($delivery).'<br>';
					}else{
						$delivery=$deliverys[$key];
					}
				}else{
					//預設
					$alldelivery=$this->_select('ij_delivery_method_ct','display_flag','1',0,0,0,0,'result_array');
				}
			}
			if(@$delivery=array_values($delivery)){
				foreach($delivery as $key=>$Item){
					//echo $Item.'<br>';
					$samedelivery[$key]=$this->_select('ij_delivery_method_ct','delivery_method',$Item,0,0,0,0,'row');
				}

				return $samedelivery;
			}else{
				//$alldelivery=$this->_select('ij_delivery_method_ct','display_flag','1',0,0,0,0,'result_array');
            //var_dump($alldelivery);
            //exit();
				return @$alldelivery;
			}
		}else{
			return array();
		}
  }
    //抓取購物車商品配送方式交集
  public function get_supplier_delivery($cart=0,$min_amount_for_noshipping=0){
    if($cart){
        //var_dump($min_amount_for_noshipping);exit;
            $alldelivery=array();
            foreach($cart as $key=>$Item){
                if($Item['product']['associated_delivery_method']){
                    //echo $Item['associated_delivery_method'].':'.$Item['product_sn'].'<br>';
                    $delivery_array=explode(':',$Item['product']['associated_delivery_method']);
                    foreach($delivery_array as $key2=>$Item2){
                        //$deliverys[$key][$Item2]=$this->Shop_model->_select('ij_delivery_method_ct','delivery_method',$Item2,0,0,0,0,'row','delivery_method_name')['delivery_method_name'];
                        $deliverys[$key][$key2]=$Item2;
                    }
                    if(@$delivery){
                        //var_dump($delivery).'<br>';
                        //var_dump($deliverys[$key]).'<br>';
                        $delivery=array_intersect($delivery,$deliverys[$key]);
                        //var_dump($delivery).'<br>';
                    }else{
                        $delivery=$deliverys[$key];
                    }
                }else{
                    //預設
                    $alldelivery=$this->_select('ij_delivery_method_ct','display_flag','1',0,0,0,0,'result_array');
                    foreach($alldelivery as $_key=>$_Item){
                        $alldelivery[$_key]['description']=str_replace('[free_shop_money]',number_format($min_amount_for_noshipping),$_Item['description']);
                    }
                }
            }
            if(@$delivery=array_values($delivery)){
                foreach($delivery as $key=>$Item){
                    //echo $Item.'<br>';
                    $samedelivery[$key]=$this->_select('ij_delivery_method_ct','delivery_method',$Item,0,0,0,0,'row');
                    $samedelivery[$key]['description']=str_replace('[free_shop_money]',number_format($min_amount_for_noshipping),$samedelivery[$key]['description']);
                    //var_dump($samedelivery[$key]['description']);
                }
                return $samedelivery;
            }else{
                //$alldelivery=$this->_select('ij_delivery_method_ct','display_flag','1',0,0,0,0,'result_array');
            //var_dump($alldelivery);
            //exit();
                return @$alldelivery;
            }
        }else{
            return array();
        }
  }

	//抓取訂單
  public function _get_order($order_sn,$select=0,$_if_order_num=0,$_sub_order_sn=0){
  		if($select){
      	$this->db->select($select);
  		}else{
      	$this->db->select('*,ij_payment_method_ct.description as payment_description');
      }
      $this->db->from("ij_order");
      $this->db->join("ij_payment_method_ct","ij_payment_method_ct.payment_method=ij_order.payment_method","left");
      $this->db->join("ij_payment_status_ct","ij_payment_status_ct.payment_status=ij_order.payment_status","left");
      if($_if_order_num){
        $this->db->where('order_num',$order_sn);
      }else{
        $this->db->where('order_sn',$order_sn);
      }
			$query =$this->db->get()->row_array();
			if($query){
				//$query['sub_order']=$this->_select('ij_sub_order','order_sn',$order_sn,0,0,'sub_order_sn',0,'result_array');
	      $this->db->select('*,ij_delivery_method_ct.description as delivery_description');
	      $this->db->from("ij_sub_order");
	      $this->db->join("ij_delivery_method_ct","ij_delivery_method_ct.delivery_method=ij_sub_order.delivery_method","left");
	      $this->db->join("ij_delivery_status_ct","ij_delivery_status_ct.delivery_status=ij_sub_order.delivery_status","left");
	      $this->db->join("ij_sub_order_status_ct","ij_sub_order_status_ct.sub_order_status=ij_sub_order.sub_order_status","left");
          $this->db->join("ij_supplier","ij_supplier.supplier_sn=ij_sub_order.associated_supplier_sn","left");
          $this->db->join("ij_payment_status_ct","ij_payment_status_ct.payment_status=ij_sub_order.pay_status","left");
          $this->db->where('order_sn',$query['order_sn']);
          if($_sub_order_sn){
            $this->db->where('sub_order_sn',$_sub_order_sn);
          }
          $this->db->order_by('ij_sub_order.associated_supplier_sn');
	      $query['sub_order']=$this->db->get()->result_array();
          $free_shop_money=$this->Shop_model->_select('ij_system_param_config','system_param_name','滿額免運門檻',0,0,0,0,'row',0,0,0,0)['system_param_content'];
				foreach($query['sub_order'] as $key=>$Item){
                    if(!$query['sub_order'][$key]['payment_status']){
                        $query['sub_order'][$key]['payment_status']=$query['payment_status'];
                        $query['sub_order'][$key]['payment_status_name']=@$query['payment_status_name'];
                    }
                  $query['sub_order'][$key]['delivery_description']=str_replace('[free_shop_money]',$free_shop_money,$Item['delivery_description']);
						//$query['sub_order'][$key]['order_item']=$this->_select('ij_order_item','sub_order_sn',$Item['sub_order_sn'],0,0,'order_item_sn',0,'result_array');
			      $this->db->select('*,ij_order_item.order_item_sn as order_item_sn');
			      $this->db->from("ij_order_item");
			      $this->db->join("ij_order_cancel_log","ij_order_cancel_log.order_item_sn=ij_order_item.order_item_sn","left");
			      $this->db->where('sub_order_sn',$Item['sub_order_sn']);
			      $query['sub_order'][$key]['order_item']=$this->db->get()->result_array();
                    foreach($query['sub_order'][$key]['order_item'] as $key2=>$Item2){
                        $product=$this->_get_product($Item2['product_sn'],'image_path,image_alt');
                        $query['sub_order'][$key]['order_item'][$key2]['image_path']=$product['image_path'];
                        $query['sub_order'][$key]['order_item'][$key2]['image_alt']=$product['image_alt'];
                    }

				}
				return $query;
			}else{
				return array();
			}
  }
	//抓coupon資料
  public function _get_coupon($coupon_code=0,$coupon_code_id=0,$if_return_amount=0,$product_sn=0)
  {
  	if($coupon_code || $coupon_code_id){
  		if(strpos($product_sn,'_')!==false){ // 過濾_
  			$product_sn=substr($product_sn,0,strpos($product_sn,'_'));
  		}
      $this->db->from("ij_coupon_code");
      $this->db->join("ij_coupon_code_config","ij_coupon_code_config.coupon_code_config_sn=ij_coupon_code.coupon_code_config_sn","left");
      if($coupon_code){
      	$this->db->where('coupon_code like binary',$coupon_code);
      }
      if($coupon_code_id){ //直接回傳跳過 coupon_code_status 跟日期
      	$this->db->where('coupon_code_id',$coupon_code_id);
				if($if_return_amount){
					$_coupon=$this->db->get()->row_array();
					if($_coupon['lumsum_discount_flag']=='1'){ //整筆訂單折扣
						return '0';
					}else{
						if($_coupon['discount_type']=='1'){ //金額
							return (int)$_coupon['discount_amount'];
						}else{
							return (float)$_coupon['discount_percent'];
						}
					}
				}else{
      		return $this->db->get()->row_array();
				}
      }
   		$this->db->where('event_start_date <=',date("Y-m-d H:i:s",time()));
			$this->db->where('event_end_date >=',date("Y-m-d H:i:s",time()));
      $this->db->where('coupon_code_status','0'); //未使用
      //$this->db->where('used_date !=','0000-00-00 00:00:00');
			$_coupon =$this->db->get()->row_array();
			//echo $this->db->last_query();
			if($_coupon){
				if($member_sn=@$this->session->userdata['web_member_data']['member_sn']){

					$coupon_ok=true; //預設符合
					if($_coupon['apply_dealer']){ //判斷經銷商
						$_dealer_array=explode(';',$_coupon['apply_dealer']);
						$_where=array('ij_member_dealer_relation.member_sn'=>$member_sn);
						if(!$this->libraries_model->get_dealer_members($_dealer_array,'ij_member_dealer_relation.member_sn','dealer_sn',0,$_where)){
							$coupon_ok=false; //不屬於該經銷商s
							$_message='很抱歉!您輸入的優惠代碼僅限特定經銷商所屬會員使用!';
						}
					}
					switch($_coupon['apply_TA']){ //判斷適用對象
						case "1;1;0":
							$member_level_types=array('2','3','4');
							$_message='很抱歉!您輸入的優惠代碼僅限經銷、聯盟會員使用!';
						break;
						case "1;0;0":
							$member_level_types=array('3','4');
							$_message='很抱歉!您輸入的優惠代碼僅限經銷會員使用!';
						break;
						case "0;1;0":
							$member_level_types=array('2');
							$_message='很抱歉!您輸入的優惠代碼僅限聯盟會員使用!';
						break;
						case "0;0;1":
							$member_level_types=array('1','9');
							$_message='很抱歉!您輸入的優惠代碼僅限一般會員使用!';
						break;
						case "1;0;1":
							$member_level_types=array('1','3','4','9');
							$_message='很抱歉!您輸入的優惠代碼僅限一般、經銷會員使用!';
						break;
						case "0;1;1":
							$member_level_types=array('1','2','9');
							$_message='很抱歉!您輸入的優惠代碼僅限一般、聯盟會員使用!';
						break;
						default: //1;1;1
							$member_level_types=array('1','2','3','4','9');
							$_message='很抱歉!您輸入的優惠代碼僅限一般、經銷、聯盟會員使用!';
						break;
					}
					//適用對象
					$_where=array('member_sn'=>$member_sn);
					if(!$this->libraries_model->get_member_by_where_in('member_level_type',$member_level_types,'member_sn',$_where)){
						$coupon_ok=false; //不屬於該適用對象
					}
					//適用分類
					if($_coupon['apply_category']){
						$coupon_categorys=explode(';',$_coupon['apply_category']);
						if(!$this->_chk_category_product($product_sn,$coupon_categorys)){ //判斷是否符合設定優惠分類
							$coupon_ok=false; //不屬於該適用分類
							$_message='很抱歉!您輸入的優惠代碼不適用該商品!';
						}
					}
					//適用產品
					if($_coupon['apply_product']){
						$coupon_products=explode(';',$_coupon['apply_product']);
						if(!in_array($product_sn,$coupon_products)){ //判斷是否符合設定優惠產品
							$coupon_ok=false; //不屬於該適用產品
							$_message='很抱歉!您輸入的優惠代碼不適用該商品!';
						}
					}
					if($coupon_ok){ //適用該優惠
						/*if($_coupon['discount_type']=='1'){ //金額 計算
							$coupon_money= (int)$_coupon['discount_amount'];
						}else{
							if($_coupon['discount_percent'] < 1) {
								$coupon_money=round((1-$_coupon['discount_percent'])*$sub_total);
							}else{
								$coupon_money=0;
							}
						}
						if(!$_coupon['min_amount_for_discount'] || $sub_total > $_coupon['min_amount_for_discount']){ //符合滿額優惠
							if($sub_total-$gift_cash_total > $coupon_money){
								//$coupon_total=$coupon_money;
							}else{ //結帳金額低於優惠金額，優惠金額改為結帳金額
								$coupon_money=$sub_total-$gift_cash_total;
							}
							//var_dump($coupon_total);
							//var_dump($coupon_money);
							if($coupon_total < $coupon_money){
								$coupon_total=$coupon_money;
							}
						}else{
							return '您輸入的優惠代碼需滿 '.number_format($_coupon['min_amount_for_discount']).' 元才有優惠喔';
							exit();
							//$this->session->set_flashdata('message','您輸入的優惠代碼需滿 '.$_coupon['min_amount_for_discount'].' 元才有優惠喔');
						}*/
					}else{
							return $_message;
							exit();
					}

					switch ($_coupon['coupon_code_type']){
						case '1': // 一個優惠代碼，可以讓 N 個會員使用
				        //新增一組未使用且$insert_data['check_code']+1
								$insert_data=$this->_select('ij_coupon_code','coupon_code_id',$_coupon['coupon_code_id'],0,0,'coupon_code_id',0,'row');
								unset($insert_data['coupon_code_id']);
								$insert_data = $this->CheckUpdate($insert_data,1);
								$insert_data = $this->CheckUpdate($insert_data,0);
								$insert_data['check_code']=(int)$insert_data['check_code']+1;
						    $this->_insert('ij_coupon_code',$insert_data);
								//將該筆活動優惠改為已使用
								$upt_data = array('used_member_sn' => $member_sn,'coupon_code_status' => '2','used_date' => date("Y-m-d H:i:s",time()));
								$upt_data = $this->CheckUpdate($upt_data,0);
						    $this->_update('ij_coupon_code',$upt_data,array('coupon_code_id'=>$_coupon['coupon_code_id']));
							break;
						case '2': //每個優惠代碼只能用一次，不能重覆使用。
								//將該筆活動優惠改為已使用
								$upt_data = array('used_member_sn' => $member_sn,'coupon_code_status' => '2','used_date' => date("Y-m-d H:i:s",time()));
								$upt_data = $this->CheckUpdate($upt_data,0);
						    $this->_update('ij_coupon_code',$upt_data,array('coupon_code_id'=>$_coupon['coupon_code_id']));
							break;
						case '3': // 一個會員只有能有一組優惠代碼，重覆使用。
				        //新增一組未使用且$insert_data['check_code']+1
								$insert_data=$this->_select('ij_coupon_code','coupon_code_id',$_coupon['coupon_code_id'],0,0,'coupon_code_id',0,'row');
								unset($insert_data['coupon_code_id']);
								$insert_data = $this->CheckUpdate($insert_data,1);
								$insert_data = $this->CheckUpdate($insert_data,0);
								$insert_data['check_code']=(int)$insert_data['check_code']+1;
						    $this->_insert('ij_coupon_code',$insert_data);
								//將該筆活動優惠改為已使用
								$upt_data = array('used_member_sn' => $member_sn,'coupon_code_status' => '2','used_date' => date("Y-m-d H:i:s",time()));
								$upt_data = $this->CheckUpdate($upt_data,0);
						    $this->_update('ij_coupon_code',$upt_data,array('coupon_code_id'=>$_coupon['coupon_code_id']));
							break;
					}
					return $_coupon['coupon_code_id'];
				}else{
	    		return '請先登入會員!';
				}
			}else{
				//分析原因
				$_coupon_code_id=$this->_select('ij_coupon_code','coupon_code',$coupon_code,0,0,'coupon_code_id',0,'row')['coupon_code_id'];
				$_coupon_array=$this->_get_coupon(0,$_coupon_code_id);
				if($_coupon_code_id && $_coupon_array){
					if($_coupon_array['coupon_code_status']=='2'){
	    			return '很抱歉!該優惠代碼已被使用!';
					}elseif($_coupon_array['event_start_date'] > date("Y-m-d H:i:s",time())){
	    			return '很抱歉!該優惠代碼生效日期為：'.$_coupon_array['event_start_date'];
					}elseif($_coupon_array['event_end_date'] < date("Y-m-d H:i:s",time())){
	    			return '很抱歉!該優惠代碼有效日期至：'.$_coupon_array['event_start_date'];
					}else{
	    			return '很抱歉!查無此優惠代碼資料，請確認大小寫是否輸入正確';
					}
				}else{
	    		return '很抱歉!查無此優惠代碼資料，請確認輸入是否正確';
				}
			}
  	}
	}
  //private function _get_coupon($coupon_code)

  public function _save_order($order=0,$money_field=''){
	    if($order){
			$cart=$this->Get_Cart_Checkout();
	    	//主檔
	    	    $order['member_sn']=$this->session->userdata['web_member_data']['member_sn'];
				$order['orginal_total_order_amount']=intval($cart['total_order_amount'])+@$order['total_shipping_charge_amount'];
				//升級或另外加購會有母訂單代號
				$order['original_order_sn']=intval($cart['original_order_sn']);

				//$cart['orginal_total_order_amount'];
				$order['total_order_amount']=intval($cart['total_order_amount'])+@$order['total_shipping_charge_amount'];
				$order['gift_cash_total_discount_amount']=$cart['gift_cash_total_discount_amount'];

				//$_sav_gift_cash=$this->_sav_gift_cash($order['member_sn'],$order['gift_cash_total_discount_amount']); //紅利購物金使用寫入db

				$order['coupon_code_total_cdiscount_amount']=$cart['coupon_code_total_cdiscount_amount'];
				//縣市鄉鎮區改直接抓值寫入訂單
				$order['buyer_addr_city']=$this->_select('ij_city_ct','city_code',$order['buyer_addr_city'],0,0,0,0,'row','city_name')['city_name'];
				$order['buyer_addr_town']=$this->_select('ij_town_ct','town_code',$order['buyer_addr_town'],0,0,0,0,'row','town_name')['town_name'];
                $order['invoice_receiver_addr_city']=$this->_select('ij_city_ct','city_code',$order['buyer_addr_city'],0,0,0,0,'row','city_name')['city_name'];
                $order['invoice_receiver_addr_town']=$this->_select('ij_town_ct','town_code',$order['buyer_addr_town'],0,0,0,0,'row','town_name')['town_name'];
                $order['invoice_receiver_zipcode']=$order['buyer_zipcode'];
                $order['invoice_receiver_addr1']=$order['buyer_addr1'];

				@$order['receiver_addr_city']=$this->_select('ij_city_ct','city_code',$order['receiver_addr_city'],0,0,0,0,'row','city_name')['city_name'];
				@$order['receiver_addr_town']=$this->_select('ij_town_ct','town_code',$order['receiver_addr_town'],0,0,0,0,'row','town_name')['town_name'];
				$order['payment_status']='1';//未付款
				//$order_delivery_method=@$order['delivery_method'];
				unset($order['delivery_method']); //因為改在sub_order
				 //子檔共通
				$sub_order=$this->input->post('sub_order');
				$order = $this->libraries_model->CheckUpdate($order);
				$order['order_create_date']=date("Y-m-d H:i:s",time());
                $order['order_num']= $this->get_order_num($order['order_create_date']);
                //聯盟會員分潤依據
                if($partner_sn=$this->ijw->get_partner_sn()){
                    $this->load->model('member/partner_table');
                    if($partner=$this->partner_table->get_newmember($partner_sn)){
                        $order['associated_member_sn']=$partner['member_sn'];
                    }
                }
                //var_dump($order);
                //exit();
			$order_sn=$this->_insert('ij_order',$order);
                //紅利購物金
                if($this->_get_gift_cash($this->session->userdata['web_member_data']['member_sn'])>=$order['gift_cash_total_discount_amount']){
                    $this->libraries_model->sav_gift_cash($this->session->userdata['web_member_data']['member_sn'],-$order['gift_cash_total_discount_amount'],0,@$order_sn,'購物使用');
                }else{
                    $this->session->set_userdata(array('Mes'=>'很抱歉!紅利購物金不夠扣抵'));
                    redirect(base_url("shop/shopView"));
                }
				$sub_order['order_sn']=@$order_sn;
				$sub_order['sub_order_status']='1';
				$sub_order['delivery_status']='1';//改1
                $sub_order['pay_status']='1';//未付款
				//$sub_order_weight['total_weight']=0;
				$sub_order['associated_member_sn']=$this->session->userdata['web_member_data']['member_sn'];

                $method_shipping=$this->session->userdata('method_shipping');
                //var_dump($method_shipping);
                //exit;
					foreach($cart['cart'] as $key=>$_Item){
						//$_product=$this->_select('ij_product','product_sn',$_Item['id'],0,0,0,0,'row','product_type,default_channel_name,unispec_flag,product_eng_name');
						$_product=$this->_get_product($_Item['id']);
			//echo $this->db->last_query();
						if($_product['product_type']=='2') { //虛擬商品配送方式為線上開通
							$delivery_method=$this->_select('ij_delivery_method_ct','delivery_method_name','線上開通',0,0,0,0,'row','delivery_method')['delivery_method'];
						}else{
							$delivery_method=$method_shipping[$_product['supplier_sn']];
						}
						//區分子訂單
						$where = array('channel_name' => $_product['default_channel_name'],'order_sn' => $order_sn,'ij_sub_order.associated_supplier_sn' => $_product['supplier_sn']);
				//var_dump($_product['default_channel_name']);
						if(!$sub_order_sn=$this->_select('ij_sub_order',$where,0,1,0,'sub_order_sn',0,'row','sub_order_sn')['sub_order_sn']){
							$sub_order['channel_name']=$_product['default_channel_name'];
							$sub_order['product_type']=$_product['product_type'];
							$sub_order['delivery_method']=$delivery_method;
							$sub_order['associated_supplier_sn']=$_product['supplier_sn'];
                            $sub_order['shipping_charge_amount']=$this->get_supplier_ship($_product['supplier_sn'],0,$delivery_method,$money_field);
                            //$this->_select('ij_delivery_method_ct','delivery_method',$delivery_method,0,0,0,0,'row',$money_field)[$money_field];

                            if($_dealer_sn=$this->ijw->get_dealer_sn()){
                                $sub_order['associated_dealer_sn']=$_dealer_sn;
                            }
                            $sub_order['sub_order_num']=$order['order_num'].str_pad( $_product['supplier_sn'] , 4 , '0' , STR_PAD_LEFT );
							$sub_order_sn=$this->_insert('ij_sub_order',$sub_order);
						}
						//var_dump($sub_order_sn);
                        //exit;
						if($_product['product_type']=='2') { //虛擬商品配送狀態預設1
							$order_item['virtual_prod_addon_update_status']='1';
						}
						$order_item['sub_order_sn']=$sub_order_sn;
						//$cart[$key]['product'] = $this->_get_product($_Item['id']);
                        if(strpos($_Item['id'],'_')!==false){ // 過濾_
                            $_Item['id']=substr($_Item['id'],0,strpos($_Item['id'],'_'));
                        }
						$order_item['product_sn']=$_Item['id'];
						$order_item['product_name']=$_Item['name'];
						$order_item['product_eng_name']=$_product['product_eng_name'];

						//new add
						$order_item['weight']=$_product['weight'];
						//$sub_order['total_weight']+=$_product['weight'];
						$order_item['unispec_flag']=$_product['unispec_flag'];
						$order_item['unispec_warehouse_num']=$_product['unispec_warehouse_num'];
                        //var_dump($_Item);exit;
						if(@$_Item['mutispec_color_name']){
							$order_item['mutispec_stock_sn']=$_Item['mutispec_stock_sn'];
							$order_item['mutispec_color_name']=$_Item['mutispec_color_name'];
						    $order_item['mutispec_warehouse_num']=$_Item['mutispec_warehouse_num'];
                        }else{
                            $order_item['mutispec_stock_sn']='';
                            $order_item['mutispec_color_name']='單一規格';
                            $order_item['mutispec_warehouse_num']='';
 						}
						if($_product['pricing_method']=='2'){
							//$_where_price_tier = array('start_qty <=' => $_Item['qty'],'end_qty >=' => $_Item['qty'],'product_sn' => $_Item['id']); 改用價格找區間編號
							$_where_price_tier = array('price' => $_Item['price'],'product_sn' => $_Item['id']);
							$order_item['product_price_tier_sn']=$this->_select('ij_product_price_tier_log',$_where_price_tier,0,1,0,0,0,'row','product_price_tier_sn')['product_price_tier_sn'];
                            //var_dump($_where_price_tier);
                            if(!$order_item['product_price_tier_sn'])$order_item['product_price_tier_sn']='';
						}
						$order_item['product_template_sn']=($_product['product_template_sn'])?$_product['product_template_sn']:'0';
						if(@$_Item['product_package_config_sn']){
							$order_item['product_package_config_sn']=$_Item['product_package_config_sn'];
							$order_item['product_package_name']=$_Item['product_package_name'];
						}else{
                            $order_item['product_package_config_sn']='';
                            $order_item['product_package_name']='單一規格';
                        }
						$order_item['buy_amount']=$_Item['qty'];
						$order_item['sales_price']=$_Item['price'];
						if(@$_Item['coupon_money'] > 0 && @$_Item['coupon_money'] < 1){ //優惠是折扣的話 改計算金額寫入
							$_Item['coupon_money']=round(($_Item['qty']*$_Item['price']) *(1-$_Item['coupon_money']));
						}

						$order_item['coupon_code_cdiscount_amount']=$_Item['coupon_money'];
						$order_item['gift_cash_discount_amount']=$_Item['gift_cash'];
						$order_item['actual_sales_amount']=$_Item['subtotal'];//$_Item['subtotal']已經扣掉 coupon_money 、 gift_cash
						//var_dump($order_item);
						//var_dump($_Item);
						//exit();
						if($order['original_order_sn']){ //升級價差金額
							$order_item['upgrade_discount_amount']=@$_Item['upgrade_discount_amount'];
						  $order_item['sales_price']=$order_item['sales_price']+$_Item['upgrade_discount_amount']; //單價補回
						}
						$order_item['addon_flag']=(@$_Item['addon_log_sn'])?'1':'0';
                        //$order_item['associated_order_item_sn']=@$_Item['addon_log_sn']; //先放這個
                        $order_item['associated_order_item_sn']=@$parent[$_Item['addon_from_rowid']];
						$order_item['currency_code']='1';
                        if(!@$_Item['category_sn']) $_Item['category_sn']=($_product['default_root_category_sn'])?$_product['default_root_category_sn']:'0';
                        //new add 2016-8-18
						$order_item['category_sn']=@$_Item['category_sn'];
					    $profit_sharing_rate = $this->libraries_model->_select('ij_category','category_sn',@$_Item['category_sn'],0,0,'profit_sharing_rate',0,'row','profit_sharing_rate')['profit_sharing_rate'];
						if($profit_sharing_rate){
							$order_item['sharing_percentage']=$profit_sharing_rate;
							$order_item['sharing_amount']=round($profit_sharing_rate*$order_item['actual_sales_amount']);
						}
						//$order_item['cost']=$_product['cost'];
						//$order_item['gross_profit']=$_product['gross_profit'];
						//$order_item['weight']=$_product['weight'];
						$order_item_sn=$this->_insert('ij_order_item',$order_item);
                        if($_Item['addon_log_sn']==0){
                            $parent[$_Item['rowid']]=$order_item_sn;
                        }
						if($_product['product_type']!='2') { //虛擬商品以外異動庫存
							if($_product['unispec_flag']=='1') { //單一規格異動庫存
									$this->_update_field_amount('ij_product','product_sn',$_Item['id'],'unispec_qty_in_stock','-'.$_Item['qty']);
							}else{
								if(@$_Item['mutispec_stock_sn']){ //多重規格異動庫存
									//$where = array('product_sn' => $_Item['id'],'mutispec_stock_sn' => $_Item['mutispec_stock_sn']);
									$this->_update_field_amount('ij_mutispec_stock_log','mutispec_stock_sn',$_Item['mutispec_stock_sn'],'qty_in_stock','-'.$_Item['qty']);
								}
							}
						}
						//異動優惠代碼使用資料 used_dealer_sn?
						if($_Item['coupon_code_id']){
							$coupon_code_data = array('used_order_sn' => $order_sn,
																				'used_catergory_sn' => $_product['default_root_category_sn'], //先抓主分類
																				'used_product_sn' => $_Item['id'],
																				'used_member_sn' => $this->session->userdata['web_member_data']['member_sn'],
																				'used_date' => date("Y-m-d H:i:s",time()),
																				'actual_amount' => $_Item['subtotal'],
																				'discount_amount' => $_Item['coupon_money'],
																				);
						  //$coupon_code_data['used_dealer_sn'] = $Item2['mutispec_stock_sn'];
							$coupon_code_data = $this->CheckUpdate($coupon_code_data,0);
							$_where2 = array('coupon_code_id' => $_Item['coupon_code_id']);
							$this->_update('ij_coupon_code',$coupon_code_data,$_where2);
						}
					}
					if($this->session->userdata('coupon_data') && $this->session->userdata('coupon_data')['lumsum_discount_flag']=='1'){ //整筆訂單優惠代碼
							$coupon_code_data = array('used_order_sn' => $order_sn,
												//'used_catergory_sn' => $_product['default_root_category_sn'], //先抓主分類
												//'used_product_sn' => $_Item['id'],
												'used_member_sn' => $this->session->userdata['web_member_data']['member_sn'],
												'used_date' => date("Y-m-d H:i:s",time()),
												'actual_amount' => $order['total_order_amount'],
												'discount_amount' => $order['coupon_code_total_cdiscount_amount'],
												);
						  //$coupon_code_data['used_dealer_sn'] = $Item2['mutispec_stock_sn'];
							$coupon_code_data = $this->CheckUpdate($coupon_code_data,0);
							$_where2 = array('coupon_code_id' => $this->session->userdata('coupon_data')['coupon_code_id']);
							$this->_update('ij_coupon_code',$coupon_code_data,$_where2);
					}
				return $order_sn;
	    }else{
				return false;
	    }
  }
  public function get_unread_message_count(){
  	if(@$this->session->userdata['web_member_data']['member_sn']){
			return $this->_select('ij_member_message_center_log',array('default_display_time <='=>date('Y-m-d H:i:s'),'message_status'=>'1','ij_member_message_center_log.member_sn'=>@$this->session->userdata['web_member_data']['member_sn'],'read_flag'=>null),0,0,0,'read_flag',0,'num_rows');
  	}else{
  		return '0';
  	}
  }
  //發送通知
  public function add_member_message($member_sn,$message_subject,$message_content,$message_link_url=0,$send_email_flag=0,$default_display_time=0){
  	if($member_sn && $message_subject && $message_content){
			 $data_array = array(
            'member_sn'      => $member_sn,
            'message_subject'=> $message_subject,
            'message_content'=> $message_content,
            'send_email_flag' => $send_email_flag,
            'main_config_flag' => '0',
            'message_status' => '1',
            'send_count'     => '1'
        );
				$data_array = $this->CheckUpdate($data_array,1);
				if($default_display_time){
					$data_array['default_display_time']=$default_display_time;
					$data_array['transmit_date']=$default_display_time;
				}else{
					$data_array['transmit_date']=$data_array['last_time_update'];
				}
				if($message_link_url){
					$data_array['message_link_url']=$data_array['message_link_url'];
				}
				return $this->_insert('ij_member_message_center_log',$data_array);
			}else{
				return false;
			}
  }
    public function _get_callcenter_list($member_sn=0,$page=0,$perpage=0)
    {
        $this->db->select('*');
        $this->db->from("ij_member_question");
        $this->db->join("ij_ccapply_detail_code_ct","ij_member_question.ccapply_detail_code=ij_ccapply_detail_code_ct.ccapply_detail_code","left");
        //$this->db->join("ij_cc_question_close_ct","ij_member_question.cc_faq_close_code=ij_cc_question_close_ct.cc_question_close_code","left");
        $this->db->join("ij_member_question_status_ct","ij_member_question.question_status=ij_member_question_status_ct.member_question_status","left");
    		if($member_sn){
        	$this->db->where('member_sn',$member_sn);
        }
        //$this->db->where('question_status','1');
        //$this->db->order_by('ij_member_question.question_status','asc');
        $this->db->order_by('ij_member_question.last_time_update','desc');
        if($page >= 1)$page=$page-1;
        if($page && $perpage){
            $this->db->limit($perpage,$page*$perpage);
        }elseif(!$page && $perpage){
            $this->db->limit($perpage);
        }
        $array = $this->db->get()->result();
        foreach($array as $key => $p){
        	//var_dump($p);
          $array[$key]->reply_count = $this->_get_question_qna_log($p->member_question_sn,$member_sn,1);
        }
				return $array;
  	}
    public function _get_question_qna_log($member_question_sn=0,$member_sn=0,$count=0)
    {
    	if($member_question_sn && $member_sn){
        $this->db->select('*');
        $this->db->from("ij_member_question_qna_log");
        $this->db->join("ij_member_question","ij_member_question.member_question_sn=ij_member_question_qna_log.member_question_sn","left");
	      //$this->db->join('ij_member','ij_member.member_sn=ij_member_question_qna_log.update_member_sn','left');
        $this->db->where('ij_member_question_qna_log.member_question_sn',$member_question_sn);
        $this->db->where('member_sn',$member_sn);
        //$this->db->where('question_status','1');
        $this->db->order_by('ij_member_question_qna_log.qna_date','asc');
        if($count){
        	$array = $this->db->get()->num_rows();
        }else{
        	$array['qna_log'] = $this->db->get()->result_array();
       	  $where['member_question_sn']=$member_question_sn;
					$this->load->model('libraries_model');
        	$array['member_question']=$this->libraries_model->_get_callcenter_list($where);
        }

				return $array;
			}
  	}
  	//取得可升級商品
    public function get_upg_products($order_sn,$product_sn,$if_product_package_config_sn=0){
    	$order_data=$this->_get_order($order_sn);
							//echo $order_sn.'<br>';
    	$product_package_config_sn='';
    	if($order_data){
				foreach($order_data['sub_order'] as $key=>$Item){
					foreach($Item['order_item'] as $key2=>$Item2){
						if($Item2['product_sn']==$product_sn){
							$product_package_config_sn=$Item2['mutispec_stock_sn'];
							$actual_sales_amount=$Item2['actual_sales_amount'];
							$sales_price=$Item2['sales_price'];
							break;
						}
					}
				}
				//回傳該筆訂單該商品的實際售價
				if($if_product_package_config_sn){
					return $actual_sales_amount;
					exit();
				}

				if($product_package_config_sn){
    			$product=$this->_get_product($product_sn);
					if($product['Packages']){
						$array=array();
						$i=0;
						foreach($product['Packages'] as $key=>$Item){
							if ($sales_price < $Item['price']) {
							//echo $sales_price.'<br>';
							//echo $Item['price'].'<br>';
							 	$array[$i]['order_sn']=$order_sn;
							 	$array[$i]['product_sn']=$product['product_sn'];
							 	$array[$i]['product_name']=$product['product_name'];
							 	$array[$i]['product_package_name']=$Item['product_package_name'];
							 	$array[$i]['image_path']=$product['image_path'];
							 	$array[$i]['product_package_config_sn']=$Item['product_package_config_sn'];
							 	$array[$i]['pay_upg_price']=$Item['price']-$sales_price;
							 	$query='?product_sn='.$product_sn.'&product_spec='.$Item['product_package_config_sn'].'&order_sn='.$order_sn;
							 	$array[$i]['add_cart_url']=urlencode(base_url('api/shop_api/add_cart').$query);
							 	$i++;
							}
						}
						if(!$array){
							$feedback['response_code'] = 'FAIL:無可升級規格';
							$feedback['upg_products'] = array();
						}else{
							$feedback['response_code'] = 'OK';
							$feedback['upg_products'] = $array;
						}
					}else{
						$feedback['response_code'] = 'FAIL:單一規格';
					}
					//var_dump($product['Packages']);
				}else{
					$feedback['response_code'] = 'FAIL:查無對應規格';
				}
    	}else{
				$feedback['response_code'] = 'FAIL:查無訂單資料';
    	}
			return $feedback;
    }
    //取得加購商品列表
    public function get_addon_products($order_sn,$product_sn){
				  $this->load->model('libraries_model');
    			$addon_products=$this->libraries_model->_get_addon_list($product_sn);
				  //$addons2=$this->Shop_model->_get_products($query[0]['default_channel_name'],0,105); //加購分類id:105
					if($addon_products){
						foreach($addon_products as $key=>$Item){
							 	$query='?product_sn='.$Item['associated_product_sn'];
							 	$addon_products[$key]['add_cart_url']=urlencode(base_url('api/shop_api/add_cart').$query);
						}
						$feedback['response_code'] = 'OK';
						$feedback['addon_products'] = $addon_products;
					}else{
						$feedback['response_code'] = 'FAIL:查無可加購商品';
					}
			return $feedback;
    }
		//確認訂單商品有無線上開通並立即設定生效
    public function chk_order_online($order_sn,$iftest=0){
    	$order_data=$this->_get_order($order_sn);
    	if($original_order_data=$this->_get_order($order_data['original_order_sn'])){
    		if($original_order_data['total_order_amount']==0){
    			$iftest=1; //如果母訂單是0元訂單則是試用版
    		}
    	}
    	//var_dump($order_data['original_order_sn']);
    	//var_dump($original_order_data['total_order_amount']);
    	//exit();

    	$delivery_status=false; //改預設否 2016-6-7
    	$returnString='';
			$delivery_method=$this->_select('ij_delivery_method_ct','delivery_method_name','線上開通',0,0,0,0,'row','delivery_method')['delivery_method'];
    	if($order_data['payment_status']=='2'){ //已付款
				foreach($order_data['sub_order'] as $key=>$Item){
					if($Item['delivery_status']==1 && $Item['product_type']==2 && $Item['delivery_method']==$delivery_method){ //找出有線上開通的虛擬商品
						foreach($Item['order_item'] as $key2=>$Item2){
							if($Item2['virtual_prod_addon_update_status']!='3'){ //尚未更新
								if(strrpos($Item2['product_package_name'], "版")!==false){ //版型商品線上啟用
									if($Item2['upgrade_discount_amount'] || ($order_data['payment_method']=='10' && $order_data['original_order_sn']) || $iftest=='1'){ //升級網站才有金額 或是序號券折抵 或是試用版升級
										$upg_specs=$this->get_site_parameters($Item2['product_sn'],$Item2['mutispec_stock_sn'],$order_data['original_order_sn'],$order_data['order_sn']);
										$data_array['associated_order_sn']=$order_data['original_order_sn'];
										foreach($upg_specs as $key3=>$Item3){
											//echo $key3;
											if($key3=='upload_pic_total_amount' && $Item3 >0){
												$data_array['capability']='upload_pic';
												$data_array['increase']=$Item3;
												$p_name='婚紗照片';
 												$returnString.=$p_name.'-'.$Item3.' 張<br>';
											}elseif($key3=='album_pic_total_amount' && $Item3 >0){
												$data_array['capability']='album_pic';
												$data_array['increase']=$Item3;
												$p_name='精彩照片';
 												$returnString.=$p_name.'-'.$Item3.' 張<br>';
											}elseif($key3=='album_video_total_amount' && $Item3 >0){
												$data_array['capability']='album_video';
												$data_array['increase']=$Item3;
												$p_name='精彩影片';
 												//$returnString.=$p_name.'-'.$Item3.' 張<br>';
											}elseif($key3=='website_usable_duration' && $Item3 >0){
												$data_array['capability']='date_usable';
												$data_array['increase']=$Item3;
												$p_name='使用期限';
 												$returnString.=$p_name.'-'.$Item3.' 天<br>';
											}elseif($key3=='password_flag' && $Item3 >0){
												$data_array['capability']='password';
												$data_array['enable']='1';
												$p_name='密碼保護';
 												//$returnString.=$p_name.'-'.$Item3.' 張<br>';
											}elseif($key3=='sms_total_amount' && $Item3 >0){
												$data_array['capability']='sms';
												$data_array['increase']=$Item3;
												$p_name='喜宴提醒';
 												$returnString.=$p_name.'-'.$Item3.' 則<br>';
											}
											//var_dump($upg_specs);
											if(@$data_array['capability'] && @$data_array['increase']){
												$url=API_INVSITE.'api/upgrade_wedding_website';
												$this->load->library("ijw");
												//exit();
												$response=$this->ijw->get_array_from_curl($url,$data_array);
												if($response['success']){
												    //發送通知
														if(!$this->_select('ij_serial_card','associated_order_sn',$order_sn,0,0,'associated_order_sn',0,'num_rows')){ //非使用序號卡
									     			  $_message_cap="<font color=red size=\"3px\">網站升級『".$p_name."』啟用通知！</font>";
									     			  $_message='您婚禮網站升級『'.$p_name.'』已經設定完成，感謝您的支持～如有網站相關問題歡迎聯繫客服哦。';
									     			}else{
									     			  $_message_cap="<font color=red size=\"3px\">網站加規『".$p_name."』啟用通知！</font>";
									     			  $_message='您婚禮網站使用序號券加規『'.$p_name.'』已經設定完成，感謝您的支持～如有網站相關問題歡迎聯繫客服哦。';
									     			}
							 							$this->add_member_message($Item['associated_member_sn'],$_message_cap,$_message);
							 					}else{
												    $delivery_status=false;
												    @$error.=$response['error'].'<br>';
												}
											}else{
												    $delivery_status=false;
												    @$error.='查無可升級規格<br>';
											}
										}
										//var_dump($upg_specs);
										//var_dump($returnString);
										//exit();
										if($delivery_status){
											//更新運送狀態
											//$order_data['sub_order'][$key]['delivery_status']=8;
											$update_data = array('virtual_prod_addon_update_status' => '3');
											$update_data['memo'] = 'Success:'.$response['success'].'<br>';
											//更新網站規格-訂單編號? TEST
											if(!$this->_select('ij_serial_card','associated_order_sn',$order_sn,0,0,'associated_order_sn',0,'num_rows')){ //非使用序號卡
												$update_data2 = array('associated_order_sn' => $order_sn); //升級異動為新訂單編號（因為跟是否可再升級有關
											  //$update_data2['product_package_config_sn'] = $Item2['mutispec_stock_sn'];
												$update_data2 = $this->CheckUpdate($update_data2,0);
												$where2 = array('associated_order_sn' => $order_data['original_order_sn']);
												$this->_update('ij_wedding_website',$update_data2,$where2);
											}
											//echo $this->db->last_query();
											//exit();
										}else{
											$update_data = array('virtual_prod_addon_update_status' => '2');
											$update_data['memo'] = 'Error:'.$error.'<br>';
										}
										$update_data['product_package_name'] = $Item2['product_package_name'];
										$update_data = $this->CheckUpdate($update_data,0);
										$where = array('order_item_sn' => $Item2['order_item_sn']);
										$this->_update('ij_order_item',$update_data,$where);
									}else{
										//新增網站
										$data_array=$this->get_site_parameters($Item2['product_sn'],$Item2['mutispec_stock_sn']);
										$data_array['associated_order_sn']=$order_data['order_sn'];
										$data_array['associated_product_sn']=$Item2['product_sn'];
										$data_array['associated_product_template_sn']=$this->Shop_model->_select('ij_product','product_sn',$Item2['product_sn'],0,0,'product_template_sn',0,'row','product_template_sn',0,0,0)['product_template_sn']; //版型編號
										$data_array['member_sn']=$Item['associated_member_sn'];
										$url=API_INVSITE.'api/create_wedding_website';
										$this->load->library("ijw");
										$response=$this->ijw->get_array_from_curl($url,$data_array);
										if($response['success']){
												//更新運送狀態
												//$order_data['sub_order'][$key]['delivery_status']=8;
												$update_data = array('virtual_prod_addon_update_status' => '3');
											  $update_data['memo'] = 'Success:'.$response['success'].'<br>';
										    //發送通知
										    if($delivery_status!=false){
										    	$delivery_status=true;
										    }
						     			  $_message_cap="<font color=red size=\"3px\">網站啟用通知！</font>";
						     			  $_message='您的網站已經設定完成，感謝您的支持～如有網站相關問題歡迎聯繫客服哦。';

					 							$this->add_member_message($Item['associated_member_sn'],$_message_cap,$_message);
										}else{
											$delivery_status=false;
											$update_data = array('virtual_prod_addon_update_status' => '2');
											$update_data['memo'] = 'Error:'.$response['error'].'<br>';
										}
										$update_data = $this->CheckUpdate($update_data,0);
										$where = array('order_item_sn' => $Item2['order_item_sn']);
										$this->_update('ij_order_item',$update_data,$where);
									}
								}else{
									//$data_array2['associated_order_sn']=$order_data['original_order_sn'];
									$data_array['associated_order_sn']=$order_data['original_order_sn'];
									$data_array['capability']=$Item2['product_eng_name'];
				  				$this->load->model('libraries_model');
									$attribute_value_name=$this->libraries_model->_get_product_attribute($Item2['product_sn'],1); //get status=1
									if(strrpos($Item2['product_name'], "獨立網址")!==false)
									{
										$_days=$this->libraries_model->_get_wedding_websites(0,$order_data['original_order_sn'],1); //用母主訂單編號找出到期日-有效天數
										if($_days > 0){
											//var_dump($_days).'<br>';
											$data_array['increase']=$_days;
										}else{
											//錯誤：天數<=0
										}
									}elseif(strrpos($Item2['product_name'], "喜宴提醒")!==false){
										if($attribute_value_name){
											$data_array['increase']=$attribute_value_name*$Item2['buy_amount'];
										}
									}elseif(strrpos($Item2['product_name'], "精彩照片")!==false){
										if($attribute_value_name){
											$data_array['increase']=$attribute_value_name*$Item2['buy_amount'];
										}
									}elseif(strrpos($Item2['product_name'], "婚紗照片")!==false){
										if($attribute_value_name){
											$data_array['increase']=$attribute_value_name*$Item2['buy_amount'];
										}
									}elseif(strrpos($Item2['product_name'], "使用期限")!==false){
										if($attribute_value_name){
											$data_array['increase']=$attribute_value_name*$Item2['buy_amount'];
										}
									}
									$url=API_INVSITE.'api/upgrade_wedding_website';
									$this->load->library("ijw");
									$response=$this->ijw->get_array_from_curl($url,$data_array);
									if($response['success']){
											//更新運送狀態
											//$order_data['sub_order'][$key]['delivery_status']=8;
											$update_data = array('virtual_prod_addon_update_status' => '3');
											$update_data['memo'] = 'Success:'.$response['success'].'<br>';
									    if($delivery_status!=false){
									    	$delivery_status=true;
									    }
									    //發送通知
					     			  $_message_cap="<font color=red size=\"3px\">網站加購『".$Item2['product_name']."』啟用通知！</font>";
					     			  $_message='您加購的『'.$Item2['product_name'].'』已經設定完成，感謝您的支持～如有網站相關問題歡迎聯繫客服哦。';
				 							$this->add_member_message($Item['associated_member_sn'],$_message_cap,$_message);
				 					}else{
											//var_dump($data_array).'<br>';
											//var_dump($response['error']).'<br>';
											$update_data = array('virtual_prod_addon_update_status' => '2');
											$update_data['memo'] = 'Error:'.$response['Error'].'<br>';
									    $delivery_status=false;
									}
									$update_data = $this->CheckUpdate($update_data,0);
									$where = array('order_item_sn' => $Item2['order_item_sn']);
									$this->_update('ij_order_item',$update_data,$where);
								}
							}
						}
					}
					//更新子訂單運送狀態
					//$order_data['sub_order'][$key]['delivery_status']=8;
					if($delivery_status){
						$update_data = array('delivery_status' => '8');
						$update_data = $this->CheckUpdate($update_data,0);
						$where = array('sub_order_sn' => $Item['sub_order_sn']);
				    $this->_update('ij_sub_order',$update_data,$where);
				  }
				}
 				//return $order_data;
 				if($delivery_status){
	 				if($returnString){
	 					return $returnString;
	 				}else{
	 					return true;
	 				}
	 			}else{
 					return false;
	 			}
    	}else{
 				return false;
    	}
    }

    //取得婚禮網站版型參數
    public function get_site_parameters($product_sn,$product_package_config_sn,$original_order_sn=0,$order_sn=0){
          $specs=$this->Shop_model->_get_package_price_by_psn($product_sn,1,0,$product_package_config_sn)[0]['specs'];
					if($specs){
						foreach($specs as $Item2){
								if($Item2['spec_option_name']=='婚紗照片'){
									$data_array['upload_pic_total_amount']=$Item2['spec_option_limitation'];
								}elseif($Item2['spec_option_name']=='精彩影音-相片分享'){
									$data_array['album_pic_total_amount']=$Item2['spec_option_limitation'];
								}elseif($Item2['spec_option_name']=='精彩影音-影片分享'){
									$data_array['album_video_total_amount']=$Item2['spec_option_limitation'];
								}elseif($Item2['spec_option_name']=='密碼保護'){
									$data_array['password_flag']=$Item2['spec_option_limitation'];
								}elseif($Item2['spec_option_name']=='簡訊提醒'){
									$data_array['sms_total_amount']=$Item2['spec_option_limitation'];
								}elseif($Item2['spec_option_name']=='使用期限'){
									$data_array['website_usable_duration']=$Item2['spec_option_limitation'];
								}
						}
						if($original_order_sn){
							if($this->_select('ij_serial_card','associated_order_sn',$order_sn,0,0,'associated_order_sn',0,'num_rows')==0){ //非使用序號卡
		//echo $this->db->last_query();
								//待修正
								//$product_package_config_sn=$this->_select('ij_wedding_website','associated_order_sn',$original_order_sn,0,0,'product_package_config_sn',0,'row','product_package_config_sn')['product_package_config_sn'];
								$product_package_config_sn=$this->_get_order_product_package_config_sn($original_order_sn,$product_sn);
	              $old_specs=$this->get_site_parameters($product_sn,$product_package_config_sn); //原本版型規格
	              //exit();
								if($old_specs){
									foreach($data_array as $key=>$Item3){
	              		//var_dump($upg_specs[$key]);
										$data_array[$key]=intval(@$data_array[$key]-@$old_specs[$key]);
									}
								}
								return $data_array;
							}else{
								return $data_array;
							}
						}else{
							return $data_array;
						}
					}else{
						return array();
					}
    }
   public function _get_wedding_serials($serial_card_security_code)
    {
	    if($serial_card_security_code)
	    {
        $this->db->select('ij_serial_card.*,ij_dealer.dealer_short_name,ij_member.last_name,ij_member.first_name,serial_card_status_name,product_name,product_package_name,color_name');
        $this->db->from("ij_serial_card");
        $this->db->join('ij_member','ij_member.member_sn=ij_serial_card.used_member_sn','left');
        $this->db->join('ij_dealer','ij_dealer.dealer_sn=ij_serial_card.stock_out_dealer_sn','left');
        $this->db->join('ij_serial_card_status_ct','ij_serial_card_status_ct.serial_card_status=ij_serial_card.serial_card_status','left');
        $this->db->join('ij_product','ij_product.product_sn=ij_serial_card.associated_product_sn','left');
        $this->db->join('ij_product_package_config','ij_product_package_config.product_package_config_sn=ij_serial_card.associated_product_package_config_sn','left');
        $this->db->join('ij_mutispec_stock_log','ij_mutispec_stock_log.mutispec_stock_sn=ij_serial_card.associated_product_package_config_sn','left');
	      $this->db->where('serial_card_security_code like binary',$serial_card_security_code);
        $array = $this->db->get()->row_array();
        if(!$array){
        	return '很抱歉！查無該序號，請確認是否輸入正確（大小寫必須區分喔）！';
        }elseif($array['used_member_sn']){
        	return '很抱歉！該序號券已經於 '.$array['used_date'].' 被使用！';
        }elseif($array['serial_card_status']!='2'){
        	return '很抱歉！該序號券尚未設定完成，敬請回報客服調整後再做使用！';
        }else{
          $array['price']=$this->Shop_model->_get_package_price_by_psn($array['associated_product_sn'],1,0,$array['associated_product_package_config_sn'])[0]['promo_price'];
					return $array;
				}
			}else{
        	return '請輸入序號！';
			}
  	}
    public function _get_wedding_websites_by_dealer_sn($dealer_sn=0)
    {
    	if($dealer_sn){
        $this->db->where('stock_out_dealer_sn',$dealer_sn);
    	}
      $this->db->select('associated_wedding_website_sn,wedding_website_title,wedding_website_subtitle,wedding_website_content,main_picture_save_dir');
      $this->db->from("ij_wedding_website");
      $this->db->join('ij_member','ij_member.member_sn=ij_wedding_website.member_sn');
      $this->db->join('ij_serial_card','ij_serial_card.associated_wedding_website_sn=ij_wedding_website.wedding_website_sn','left');
      $this->db->where('wedding_website_status','1');
      $this->db->order_by('ij_wedding_website.last_time_update','desc');
      $array = $this->db->get()->result_array();
			return $array;
  	}
    public function _get_order_product_package_config_sn($original_order_sn,$product_sn){
    	if($sub_order=@$this->_get_order($original_order_sn,'order_sn')['sub_order']){
	      foreach($sub_order as $key1 => $_Item1){
		      foreach($_Item1['order_item'] as $key2 => $_Item2){
		      	if($_Item2['product_sn']==$product_sn){
		      		return $_Item2['mutispec_stock_sn'];
		      		break;
		      	}
		      }
	      }
		    return $sub_order[0]['order_item'][0]['mutispec_stock_sn']; //若無產品對應則回傳第一個 mutispec_stock_sn=product_package_config_sn
		  }else{
		  	return '0';
    	}
    }
    public function get_adsSlot($banner_location_name)
    {
	    	if($banner_location_name){
	        $this->db->select('ij_banner_location.*,published_locaiotn_type_name,banner_type_name,banner_type_eng_name');
	        $this->db->from("ij_banner_location");
	        $this->db->join('ij_banner_type_ct','ij_banner_location.banner_type=ij_banner_type_ct.banner_type','left');
	        //$this->db->join('ij_banner_link_type_ct','ij_banner_link_type_ct.banner_link_type=ij_banner_location.banner_link_type','left');
	        $this->db->join('ij_published_locaiotn_type_ct','ij_published_locaiotn_type_ct.published_locaiotn_type=ij_banner_location.published_locaiotn_type','left');
		      $this->db->where('status','1');
		      $this->db->where('banner_location_name',$banner_location_name);
					return $this->db->get()->row_array();
				}
  	}
    public function get_adsMaterials($banner_location_name,$limit=5)
    {
        $array = $this->get_adsSlot($banner_location_name);
        if($array){
	        $this->db->from("ij_banner_content");
	        $this->db->join('ij_banner_schedule_log','ij_banner_schedule_log.banner_content_sn=ij_banner_content.banner_content_sn','left');
            $this->db->join('ij_banner_link_type_ct','ij_banner_schedule_log.banner_link_type=ij_banner_link_type_ct.banner_link_type','left');
			$this->db->join('ij_banner_location','ij_banner_location.banner_location_sn=ij_banner_content.banner_location_sn','left');
			//$this->db->join('ij_banner_type_ct','ij_banner_content.banner_type=ij_banner_type_ct.banner_type','left');
			//$this->db->join('ij_published_locaiotn_type_ct','ij_published_locaiotn_type_ct.published_locaiotn_type=ij_banner_location.published_locaiotn_type','left');
	        //$this->db->join('ij_banner_customer','ij_banner_customer.banner_customer_sn=ij_banner_content.banner_customer_sn','left');
			$this->db->where('ij_banner_content.status','1');
			$this->db->where('ij_banner_content.banner_location_sn',$array['banner_location_sn']);
            if($banner_location_name!='首頁-熱門關鍵字'){
                $this->db->select('ij_banner_schedule_log.*,banner_content_name,banner_content_save_dir,banner_link_type_eng_name,width,hight');
                $_where_string="(banner_eff_start_date <='".date("Y-m-d H:i:s",time())."' or banner_eff_end_date >='".date("Y-m-d H:i:s",time())."')";
                $_where_string.=" and (banner_list_start_date <='".date("Y-m-d H:i:s",time())."' or banner_list_end_date >='".date("Y-m-d H:i:s",time())."')";
                $this->db->where($_where_string);
                //$this->db->where('banner_list_start_date <=',date("Y-m-d H:i:s",time()));
    			//$this->db->where('banner_list_end_date >=',date("Y-m-d H:i:s",time()));
    			$this->db->where('ij_banner_schedule_log.status','1');
                $this->db->order_by('sort_order','asc');
                $this->db->order_by('last_time_update','desc');
            }else{
                //var_dump($array);
                $this->db->select('ij_banner_content.banner_content_sn,banner_content_name,banner_content_save_dir');
                $this->db->order_by('ij_banner_content.sort_order','asc');
                $this->db->order_by('view_total_amount','desc');
                $this->db->limit($limit);
            }
	        $array['banners'] = $this->db->get()->result_array();
            //if($banner_location_name=='首頁_熱門關鍵字_H1_中間'){
                //echo $this->db->last_query().'<br>';
            //}
					//view+1
					foreach($array['banners'] as $_key=>$_Item){
						$this->_update_field_amount('ij_banner_content','banner_content_sn',$_Item['banner_content_sn'],'view_total_amount','+1');
                        if(@$_Item['banner_schedule_sn'])
						$this->_update_field_amount('ij_banner_schedule_log','banner_schedule_sn',@$_Item['banner_schedule_sn'],'view_amount','+1');
						/*switch ($_Item['banner_link_type_eng_name']){
						case 'link_product_sn':
	        		$array['banners'][$_key]['banner_link']='shop/shopItem/'.$_Item[$_Item['banner_link_type_eng_name']];
							break;
						case 'link_category_sn':
	        		$array['banners'][$_key]['banner_link']='shop/shopCatalog/'.$_Item[$_Item['banner_link_type_eng_name']];
							break;
						case 'banner_link':
	        		$array['banners'][$_key]['banner_link']=$_Item[$_Item['banner_link_type_eng_name']];
							break;
						case 'search_keyword':
	        		$array['banners'][$_key]['banner_link']='shop/shopSearch?Search='.$_Item[$_Item['banner_link_type_eng_name']];
							break;
						default:
						}*/
					}
	      }else{
	        $array['banners'] = array();
	      }
		return $array;
  	}
    public function get_adsBanner($_banner_schedule_sn,$_status=1){
        $this->db->from("ij_banner_content");
        $this->db->join('ij_banner_schedule_log','ij_banner_schedule_log.banner_content_sn=ij_banner_content.banner_content_sn','left');
        //$this->db->join('ij_banner_link_type_ct','ij_banner_schedule_log.banner_link_type=ij_banner_link_type_ct.banner_link_type','left');
        //$this->db->join('ij_banner_location','ij_banner_location.banner_location_sn=ij_banner_content.banner_location_sn','left');
        $this->db->where('ij_banner_content.status','1');
        $this->db->where('ij_banner_schedule_log.banner_schedule_sn',$_banner_schedule_sn);
        $this->db->select('ij_banner_schedule_log.*,banner_content_name,banner_content_save_dir');
        $_where_string="(banner_eff_start_date <='".date("Y-m-d H:i:s",time())."' or banner_eff_end_date >='".date("Y-m-d H:i:s",time())."')";
        $_where_string.=" and (banner_list_start_date <='".date("Y-m-d H:i:s",time())."' or banner_list_end_date >='".date("Y-m-d H:i:s",time())."')";
        $this->db->where($_where_string);
        if($_status) $this->db->where('ij_banner_schedule_log.status',$_status);
        return $this->db->get()->row_array();
    }

    public function _chk_category_product($product_sn=0,$category_array=0)
    {
    	if($product_sn && $category_array){
    		if(strpos($product_sn,'_')!==false){ // 過濾_
    			$product_sn=substr($product_sn,0,strpos($product_sn,'_'));
    		}
        $this->db->select('ij_product.product_sn');
        $this->db->from("ij_product");
        $this->db->join("ij_category","ij_category.category_sn=ij_product.default_root_category_sn","left");
        $this->db->where('ij_product.product_sn',$product_sn);

        $this->db->group_start()->or_where_in('ij_product.default_root_category_sn',$category_array);
        $this->db->join("ij_product_category_relation","ij_product_category_relation.product_sn=ij_product.product_sn","left");
        $this->db->or_where_in('ij_product_category_relation.category_sn',$category_array)->group_end();
        $this->db->group_by('ij_product.product_sn');
				$result=$this->db->get()->num_rows();
		//echo $this->db->last_query().'<br>';

				return ($result) ? true:false;
			}else{
				return false;
			}
	}
    public function dealer_categoryParentChildTree($dealer_sn,$category_sn=0,$spacing = '├─', $category_tree_array = '',$spacing2='│&nbsp;')
    {
        //NodeId為Parent的點
            if (!is_array($category_tree_array))
                $category_tree_array = array();

        //取得該點的下一層
        $this->db->select('ij_category.category_sn,category_name,upper_category_sn,ij_category_relation.sort_order,channel_name,category_status');
        $this->db->from("ij_category");
        $this->db->join('ij_category_relation','ij_category.category_sn=ij_category_relation.category_sn','left');
        $this->db->where('upper_category_sn',$category_sn);
        $this->db->where('relation_status','1');
        $this->db->where('category_status','1');
        $this->db->order_by('ij_category_relation.sort_order','asc');
        if($dealer_sn){
            $this->db->where('dealer_sn',$dealer_sn);
        }
        $query =$this->db->get()->result_array();
        if($query) { //最多10代
            //如果有下一層資料，逐一取出
        foreach($query as $key => $intro){
            //echo $spacing . $intro['category_name'].'<br>';
          $category_tree_array[] = array("category_sn" => $intro['category_sn'], "category_name" => $spacing . $intro['category_name'], "upper_category_sn" => $intro['upper_category_sn'], "sort_order" => $intro['sort_order'], "channel_name" => $intro['channel_name'], "category_status" => $intro['category_status']);
          $category_tree_array = $this->dealer_categoryParentChildTree($dealer_sn,$intro['category_sn'], $spacing2.$spacing , $category_tree_array,$spacing2);
            //var_dump($category_tree_array);
                }
        }
            return $category_tree_array;
    }
    public function get_supplier_ship($_supplier_sn,$temp_shopping_cart_order_sn=0,$_method_shipping=0,$money_field=0){
        if(!$temp_shopping_cart_order_sn) $temp_shopping_cart_order_sn=$this->_get_temp_shopping_cart_order_sn($this->session->userdata['web_member_data']['member_sn']);
        if($_supplier_sn && $temp_shopping_cart_order_sn){
            $this->db->select('sum(buy_amount*sales_price-gift_cash_discount_amount-coupon_discount_amount) as supplier_total');
            $this->db->from('ij_temp_order_item');
            $this->db->join('ij_product','ij_product.product_sn=ij_temp_order_item.product_sn','left');
            $this->db->where('temp_shopping_cart_order_sn',$temp_shopping_cart_order_sn);
            $this->db->where('ij_temp_order_item.status','3');
            $this->db->where('ij_product.free_delivery','1');
            $this->db->where('ij_product.product_type !=','2');
            $this->db->where('ij_product.supplier_sn',$_supplier_sn);
            $supplier_total=($supplier_total=$this->db->get()->row_array()['supplier_total'])?$supplier_total:0;
        //echo $this->db->last_query().'<br>';
            if(!$_method_shipping && !$money_field) return $supplier_total;
            $free_shop_money=$this->Shop_model->_select('ij_supplier','supplier_sn',$_supplier_sn,0,0,'min_amount_for_noshipping',0,'row',0,0,0,0)['min_amount_for_noshipping'];
            //var_dump($supplier_total);
            //var_dump($free_shop_money);
            if($supplier_total >=$free_shop_money){ //滿額免運
                return '0';
            }else{
                $delivery_method=$this->Shop_model->_select('ij_delivery_method_ct','delivery_method',$_method_shipping,0,0,0,0,'row','delivery_amount,delivery_method_name,outland_amount');
                return $delivery_method[$money_field];
            }
        }else{
            return array('error'=>'查無運費');
        }

    }
    // 取得單號
    public function get_order_num($_create_date=0) {
        if(!$_create_date){
            $_create_date=date('ymd');
        }else{
             $_create_date=date('ymd',strtotime($_create_date));
        }
        //var_dump($_create_date);exit;
        $sql = 'SELECT order_num FROM `ij_order` WHERE order_num like "'.$_create_date.'%" ORDER BY order_num DESC LIMIT 1';
        $query = $this->db->query($sql);
        $rnd_no=str_pad(rand(0, 99),2,'0', STR_PAD_LEFT);
        if ($query->num_rows() == 0) {
            return $_create_date.'0001'.$rnd_no;
        }
        else {
            $data = $query->row_array();
            $num = ((int)substr($data['order_num'],6,4)) + 1;
            //echo $num;exit;
            $value = str_pad( $num , 4 , '0' , STR_PAD_LEFT );
            return $_create_date.$value.$rnd_no;
        }
    }
    public function get_citys($_if_out=0){
        $this->db->select('*');
        $this->db->from('ij_town_ct');
        $this->db->join('ij_city_ct','ij_city_ct.city_code=ij_town_ct.city_code','left');
        if($_if_out){
            $this->db->where('ij_town_ct.sort_order>=',800);
        }else{
            $this->db->where('ij_town_ct.sort_order<',800);
        }
        $this->db->order_by('ij_city_ct.sort_order','asc');
        $query=$this->db->get();
        $_one=array();
        if($result=$query->result()){
           foreach ($result as $row)
           {
                //var_dump($_fields[0]);
               $_one1= $row->city_code;
               $_one2= $row->city_name;
               $_one[$_one1]=$_one2;
           }
            return $_one;
        }else{
            return array();
        }
        //echo $this->db->last_query().'<br>';

    }
}
?>