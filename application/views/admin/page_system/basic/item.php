<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>系統設定 <small>一般設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="preview">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        系統設定
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         一般設定
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">一般參數設定</span>
            </div>
            <div class="tools">
              
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-md-2 control-label">免運門檻 <span class="require">*</span></label>
                  <div class="col-md-2">
                    <input type="number" min="1" class="form-control" name="basic[system_param_content]" value="<?php echo $Item['system_param_content'];?>">
                  </div>
                  <div class="col-md-">
                    <p class="form-control-static">元</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-md-2 control-label">備註</label>
                  <div class="col-md-10">
                    <textarea class="form-control" name="basic[memo]" rows="3"><?php echo @$Item['memo'];?></textarea>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-2 col-md-10">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="button" class="btn default">Cancel</button>
										<input type="hidden" name="system_param_config_sn" value="<?php echo @$Item['system_param_config_sn'];?>">
										<input type="hidden" name="basic[system_param_name]" value="滿額免運門檻">
										<input type="hidden" name="basic[system_param_type]" value="一般設定">
										<input type="hidden" name="basic[status]" value="1">
										<input type="hidden" name="basic[sort_order]" value="1">
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->