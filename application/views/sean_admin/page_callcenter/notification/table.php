<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>全站通知</h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="preview">Home</a><i class="fa fa-circle"></i>
      </li>
      <li class="active">
        系統訊息
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">系統訊息列表</span>
            </div>
            <div class="actions btn-set">
              <a href="preview/notification/form" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <div class="note note-warning">
            <div class="input-group input-large date-picker input-daterange  pull-left" data-date="2015-05" data-date-format="yyyy-mm">
              <span class="input-group-addon" onclick="alert('Start Search');"> 期間 </span>
              <input type="text" class="form-control input-sm" name="from" value="2015-05">
              <span class="input-group-addon"> 到 </span>
              <input type="text" class="form-control input-sm" name="to" value="2015-06">
            </div>
            <div class="table-actions-wrapper pull-right">
              <span>
              </span>
              <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="12" style="margin: 0 5px; font-size:10px;" placeholder="主題">
              <select class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;">
                <option value="">是否顯示</option>
                <option value="">已顯示</option>
                <option value="">未顯示</option>
              </select>
              <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button>
            </div>
            <div style="clear:both;"></div>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>系統訊息主題</th>
                  <th>系統訊息內容</th>
                  <th>編輯日期</th>
                  <th>顯示時間</th>
                  <th>發送人數</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=0;$i<5;$i++):?>
                  <tr class="odd gradeX">
                    <td>系統訊息主題<?php echo $i;?></td>
                    <td>系統訊息內容或外連連結</td>
                    <td class="center">
                      <small>2015-06-10 12:00</small>
                    </td>
                    <td class="center">
                      <small>2015-06-11 12:00</small>
                    </td>
                    <td class="center">1</td>
                    <td class="center">
                      <a href="preview/notification/form"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                      <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                    </td>
                  </tr>
                <?php endfor;?>
              </tbody>
            </table>

            <div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                  <ul class="pagination" style="visibility: visible;">
                    <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                    <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li class="active"><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                    <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>

            
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->