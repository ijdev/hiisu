<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<style>
  .date-picker{
    height: 30px;
    vertical-align: super;
    font-size: 16px;
    width: 88px;
    padding: 10px;
  }
</style>
<div class="container">
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <li class="active">績效總覽</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar col-md-2 col-sm-3">
    <div class="desktop_view_sidebar">
		<ul class="list-group margin-bottom-25 sidebar-menu">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
	<div class="mobile_view_sidebar">
		<div class="side-nav-head border_title_bottom">
			<button class="fa fa-bars collapsed" data-toggle="collapse" data-target="#mob_sidebar_drop_down"></button>
			<h4 class="bfont">會員中心<!--會員中心--></h4>
		</div>
		<ul class="list-group margin-bottom-25 sidebar-menu collapse" id="mob_sidebar_drop_down">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="col-md-10 col-sm-9">
    <div class="product-page">
        <form method="post" id="form1">
      <h2 style="display: inline-block;">
        <i class="icon-trophy"></i> 績效總覽 &nbsp;
      </h2>
          <input type="text" name="yearmonth" id="yearmonth" value="<?=$yearmonth?>" class="date-picker">
        </form>
          <a href="member/benefit">
          <div class="col-md-6">
            <div class="alert alert-info text-center">
              <h3>分潤獎金：<?=number_format($cash)?></h3>
            </div>
          </div>
          </a>
          <a href="member/cash/3">
          <div class="col-md-6">
            <div class="alert alert-info text-center">
              <h3>招募獎金：<?=number_format($cash2)?></h3>
            </div>
          </div>
          </a>
          <div class="col-md-3">
            <div class="alert alert-warning text-center">
              <h3>分潤訂單：<?=$total_ok_orders?></h3>
            </div>
          </div>
          <div class="col-md-3">
            <div class="alert alert-warning text-center">
              <h3>相關訂單：<?=$total_no_ok_orders?></h3>
            </div>
          </div>
          <div class="col-md-3">
            <div class="alert alert-warning text-center">
              <h3>招募會員：<?=$total_members?></h3>
            </div>
          </div>
          <div class="col-md-3">
            <div class="alert alert-warning text-center">
              <h3>點擊數：<?=@$total_hits?></h3>
            </div>
          </div>
    </div>
  </div>
  <!-- END CONTENT -->
</div>
</div>
<!-- END SIDEBAR & CONTENT -->