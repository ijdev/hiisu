<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<link rel="stylesheet" href="public/metronic/global/plugins/jquery-treegrid/css/jquery.treegrid.css">
<div class="container">
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <li class="active">推廣連結</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar col-md-2 col-sm-3">
    <div class="desktop_view_sidebar">
		<ul class="list-group margin-bottom-25 sidebar-menu">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
	<div class="mobile_view_sidebar">
		<div class="side-nav-head border_title_bottom">
			<button class="fa fa-bars collapsed" data-toggle="collapse" data-target="#mob_sidebar_drop_down"></button>
			<h4 class="bfont">會員中心<!--會員中心--></h4>
		</div>
		<ul class="list-group margin-bottom-25 sidebar-menu collapse" id="mob_sidebar_drop_down">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="col-md-10 col-sm-9">
    <div class="product-page">
      <h2 style="display: inline-block;">
        <i class="icon-trophy"></i> 推廣連結
      </h2>
      <div class="form-group">
        <h4>
        加入會員：<a title="加入會員" href="<?php echo base_url('member/memberSignUp');?>?partner_sn=<?=base64_encode($this->session->userdata['web_member_data']['partner_sn'])?>"><?php echo base_url('member/memberSignUp');?>?partner_sn=<?=base64_encode($this->session->userdata['web_member_data']['partner_sn'])?></a>
          <span style="float:right;"><button class="btn btn-sm btn-info" onclick="copyStringToClipboard('<?php echo base_url('member/memberSignUp');?>?partner_sn=<?=base64_encode($this->session->userdata['web_member_data']['partner_sn'])?>')">COPY</button>&nbsp;&nbsp;&nbsp;</span>
        </h4>
      </div>
      <table class="tree-category table table-striped table-bordered table-hover" id="table_member">
        <thead>
          <tr>
            <th>商品分類</th>
            <th>推廣連結</th>
          </tr>
        </thead>
        <tbody>
<?php if(@$CategoryItems){foreach(@$CategoryItems as $CI){?>
              <tr class="treegrid-<?php echo $CI['category_sn'];?><?php if($CI['upper_category_sn']){?> treegrid-parent-<?php echo $CI['upper_category_sn'];}?>">
                <td><?php echo $CI['category_name'];?></td>
                <td>
                  <a target="_blank" id="promo_url<?=$CI['category_sn']?>" href="<?php echo base_url('shop/shopCatalog/').$CI['category_sn'];?>?partner_sn=<?=base64_encode($this->session->userdata['web_member_data']['partner_sn'])?>"> <?php echo base_url('shop/shopCatalog/').$CI['category_sn'];?>?partner_sn=<?=base64_encode($this->session->userdata['web_member_data']['partner_sn'])?></a>
                  <span style="float:right;"><button class="btn btn-sm btn-info" onclick="copyStringToClipboard('<?php echo base_url('shop/shopCatalog/').$CI['category_sn'];?>?partner_sn=<?=base64_encode($this->session->userdata['web_member_data']['partner_sn'])?>')">COPY</button></span>
                </td>
              </tr>
<?php }}?>
        </tbody>
      </table>

    </div>
  </div>
  <!-- END CONTENT -->
</div>
</div>
<!-- END SIDEBAR & CONTENT -->