<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Img_manage extends CI_Model {
	
	var $default_wid = 200;
	var	$default_hei = 200;
	var $libraryFolder;
	var $sourceFolder;
	var $sourceUrl;
	var $wedding_website_sn;
	
	function __construct(){
		parent::__construct();
		$this->libraryFolder = FCPATH."public/uploads/library/tmp/";
		$this->sourceFolder = FCPATH."public/uploads/website/";
		$this->sourceUrl = "public/uploads/website/";
		$this->wedding_website_sn = 0;
	}

	/*
	* 儲存圖片原始檔到 libraryFolder tmp file folder
	* return 暫存檔
	 */
	function add_img(){
		$result = array('success' => 'N', 'msg' => '', 'img' => '');

		$files = $_FILES;
		$originalFile = empty($files['Filedata']['tmp_name'])? $files['Filedata']['name']:$files['Filedata']['tmp_name'];
		$tmp = explode(".", $files['Filedata']['name']);
		$ext = ".".$tmp[COUNT($tmp)-1];
		$thumb = uniqid();
		$libraryFile = rtrim($this->libraryFolder,'/') . '/' .$thumb.$ext;
		
		// Validate the file type
		$fileTypes = array('jpg','jpeg','gif','png'); // File extensions
		$fileParts = pathinfo(empty($files['Filedata']['name'])? $files['upload']['name']:$files['Filedata']['name']);
		
		if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
			if(move_uploaded_file($originalFile,$libraryFile)){
				if($this->img_thumb_with_cut($thumb.$ext, $this->libraryFolder, $this->sourceFolder.'/'.$this->wedding_website_sn, $this->default_wid, $this->default_hei))
				{
					$result['success'] = 'Y';
					$result['img'] = $this->config->item('base_url').'/'.$this->sourceUrl.'/'.$this->wedding_website_sn.'/'.$thumb.$ext;
				}
				else
				{
					$result['msg'] = 'Fail to make thumb.';
				}
			}else{
				$result['msg'] = 'move_uploaded_file faile';
			}
		} else {
			$result['msg'] = 'Invalid file type.';
		}
		return $result;
	}
	/*
	* 裁切圖片
	*/
	function img_thumb_with_cut($img, $source_folder, $target_folder, $width, $height){
            $nimage = @imagecreatetruecolor($width, $height);
            
            $sourceFile = $source_folder.'/'.$img;
            $targetFile = $target_folder.'/'.$img;
			if (!is_dir($target_folder)) {
			    mkdir($target_folder);
			    chmod($target_folder, 0777);  
			}

            $mimeType = image_type_to_mime_type(exif_imagetype($sourceFile));
            switch($mimeType){
                case 'image/gif':
                    $simage = imagecreatefromgif($sourceFile);
                break;
                case 'image/jpeg':
                    $simage = imagecreatefromjpeg($sourceFile);
                break;
                case 'image/png':
                    $simage = imagecreatefrompng($sourceFile);
                break;
                default:
                    $simage = imagecreatefromjpeg($sourceFile);
                break;
            }
            $simagex = imagesx($simage);
            $simagey = imagesy($simage);
            if( ($simagex/$width) > ($simagey/$height) ){ // 以高為縮放比例基準
                    $tHight = $simagey;
                    $tWidth = floor($width*($simagey/$height));
                    $offsetX = floor(($simagex-$tWidth)/2);
                    $offsetY = 0;
            }else{
                    $tWidth = $simagex;
                    $tHight = floor($height*($simagex/$width));
                    $offsetX = 0;
                    $offsetY = floor(($simagey-$tHight)/2);
            }
            imagecopyresampled($nimage, $simage, 0, 0, $offsetX, $offsetY, $width, $height, $tWidth, $tHight);
            imagedestroy($simage);

            if($mimeType=='image/gif'){
                    imagegif($nimage, $targetFile);
            }else if($mimeType=='image/jpeg'){
                    imagejpeg($nimage, $targetFile, 100);
            }else if($mimeType=='image/png'){
                    imagepng($nimage, $targetFile, 9);
            }else{
                    imagejpeg($nimage, $targetFile, 100);
            }
            imagedestroy($nimage);
            return true;
        }
        
}