<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Member extends CI_Controller {
	/*
	* 	userLogin : input username & password to check login infor, and return token & user token to client for advance using
	*	member_data	: input toekn to get user basic infomation
	*/
	   
    public function __construct(){
        parent::__construct();
        $this->CI =& get_instance();
        $this->load->database();
        
       
    }

	public function index()
	{
		$this->load->view('welcome_message');
	}

	

	public function userLogin($type='general')

	{
				$data_log = array();
        $data_log['ip'] = $this->input->ip_address();
        $data_log['time_used'] = date('Y-m-d H:i:s');
				$feedback = "";
				$apps_check_val=0;$apps_member_val=0;$apps_member_val=0;
				
				if(!$username = $this->input->post('username'))
				{
					$username = $this->input->get('username');
				}
				if(!$pass = $this->input->post('pass'))
				{
					$pass = md5($this->input->get('pass'));
				}
				if(!$secret = $this->input->post('secret'))
				{
					$secret = $this->input->get('secret');
				}
				if(!$keyapi = $this->input->post('key'))
				{
					$keyapi = $this->input->get('key');
				}
				
				
				
				
				
				if(!$username || !$pass || !$secret || !$keyapi)
				{
					$feedback['response_code'] = 'FAIL:101';
				}else{
											
					$query = $this->db->select('*')
						->from('ij_api_apps')
						->where('api_sn', 1)
						->where('api_apps_key ', $keyapi)
						->where('api_apps_secret', $secret)
						//->where('api_apps_client_ip',$data_log['ip'] )
						->where('api_apps_status', 1)
						->get();
			        if($query->num_rows() > 0)
			        {
						$row = $query->row_array();
						foreach($row as $key => $value)
						{
								if($key=="api_apps_sn")
								$apps_check_val=$value;
						}


						if($apps_check_val  > 0 )
						{
				    		$query = $this->db->select('*')
								->from('ij_member')
								->where('user_name', $username)
								//->where('member_level_type', 1)
								//->where('system_user_flag', 0)
						      	->get();
					        if($query->num_rows() > 0)
							{
					        	$apps_member = $query->row_array();
				        		foreach($apps_member as $key => $value)
					        	{
				        			if($key=="member_sn")
				        				$apps_member_val=$value;
				        			
				        			if($key=="member_status")
				        				$_member_status=$value;
				        			
				        			if($key=="password")
				        				$_password=$value;
					        	}


								if($apps_member_val > 0 )
								{
									$_login_val=1;
									
									if($_member_status!=2)
									{
										$feedback['response_code'] = 'ACCOUNT_CLOSE';
										$_login_val=0;
								  	}
								  	if($_password != $pass )
									{
										$feedback['response_code'] = 'PASS_WRONG';
										$_login_val=0;
								  	}
								  	if($_login_val==1)
								  	{
									  	$feedback['response_code'] = 'OK';
									  	$feedback['member_sn'] = $apps_member_val;
								  	}
									
								}
								else
								{
									$feedback['response_code'] = 'FAIL:NO_USER data found';
								}	
					        }
					        else
					        {
								$feedback['response_code'] = 'FAIL:104';
					        }	
					    }
					    else
					    {
					     	$feedback['response_code'] = 'FAIL:103';
						}
			        }else{
								$feedback['response_code'] = 'FAIL:查無可用的 api_app 資料' ;
			        }	
		
				}

		 if($feedback['response_code']=="OK")
		 {
		 	$data_log="";
		 	$feedback['login_token']=$this->getToken($username);
		 	$feedback['expire']=time()+3600;//expire  1
		 	
		 	$data_log["api_sn"]=1;
		 	$data_log["api_apps_sn"]=$apps_check_val;
		 	$data_log["api_apps_key"]=$keyapi;
		 	$data_log["api_apps_secret"]=$secret;
		 	$data_log["api_token"]=$feedback['login_token'];
		 	$data_log["api_token_start_time"]=date("Y-m-d H:i:s");
		 	$data_log["api_token_end_time"]=date("Y-m-d H:i:s", strtotime('+1 hour'));
		 	$data_log["api_token_status"]=1;
		 	$data_log["api_token_create_time"]=date("Y-m-d H:i:s");
		 	$data_log["create_date"]=date("Y-m-d H:i:s");
		 	$data_log["create_member_sn"]=$apps_member_val;
		 	//$data_log["last_time_update"]=date("Y-m-d H:i:s");
		 	//$data_log["update_member_sn"]="99999";
		 	
		 	$this->sql_api_token($data_log);
		 	
		 /*}else{
				 	$feedback['login_token']=$this->getToken($username);
				 	$feedback['expire']=time()+3600;//expire  1
				 	
				 	$data_log["api_sn"]=1;
				 	$data_log["api_apps_sn"]=$apps_check_val;
				 	$data_log["api_apps_key"]=$keyapi;
				 	$data_log["api_apps_secret"]=$secret;
				 	$data_log["api_token"]=$feedback['login_token'];
				 	$data_log["api_token_start_time"]=date("Y-m-d H:i:s");
				 	$data_log["api_token_end_time"]=date("Y-m-d H:i:s", strtotime('+1 hour'));
				 	$data_log["api_token_status"]=1;
				 	$data_log["api_token_create_time"]=date("Y-m-d H:i:s");
				 	$data_log["create_date"]=date("Y-m-d H:i:s");
				 	$data_log["create_member_sn"]="99999";
				 	$data_log["last_time_update"]=date("Y-m-d H:i:s");
				 	$data_log["update_member_sn"]="99999";
				 	$this->api_token_log($data_log);
			*/
		 }
		
		 //$this->CI->output->set_content_type('application/json')->set_output(json_encode($feedback));
		 $this->ajax_feedback($feedback);
                
	}
  
  
  
  public function member_data()
  {
		$feedback = "";
		$apps_check_val=0;$apps_member_val=0;$apps_member_val=0;
		//if(!$member_sn = $this->input->post('member_sn'))
		//{
			//$member_sn = $this->input->get('member_sn');
		//}
		if(!$login_token = $this->input->post('login_token'))
		{
			$login_token = $this->input->get('login_token');
		}
				
		//if(!$member_sn || !$login_token )
		if(!$login_token )
		{
				$feedback['response_code'] = 'FAIL';
		}else{
					
			$query = $this->db->select('*')
				->from('ij_api_token')
				->where('api_token_status', 1)
				->where('api_token ', $login_token)
				->get();
		//echo $this->db->last_query();exit();
			if($query->num_rows() > 0)
			{
				$apps_check = $query->row_array();

				foreach($apps_check as $key => $value)
				{
        			if($key=="create_member_sn")
        				$apps_member_val=$value;

					if($key=="api_token_sn")
						$apps_check_val=$value;

					if($key=="api_token_end_time")
						$api_token_end_time=$value;

				}


				if($api_token_end_time < date("Y-m-d H:i:s"))
				{
					$feedback['response_code'] = 'TIME_EXPIRE';
				}
				else
				{

					$query = $this->db->select('*')
						->from('ij_member')
						->where('member_sn', $apps_member_val)
						//->where('member_level_type', 1)
						//->where('system_user_flag', 0)
						->get();
					if($query->num_rows() > 0)
					{
						$apps_member = $query->row_array();
						foreach($apps_member as $key => $value)
						{

							if($key=="member_sn")
								$apps_member_val = $value;
							if($key=="member_sn")
								$feedback['member_sn']=$value;
							if($key=="last_name")
								$feedback['name']=$value;
							if($key=="first_name")
								$feedback['name'].=$value;
							if($key=="birthday")
								$feedback['birthday']=$value;
							if($key=="email")
								$feedback['mail']=$value;
							if($key=="cell")
								$feedback['mobile']=$value;
							if($key=="tel")
								$feedback['tel']=$value;
						}
						$feedback['response_code'] = 'OK';
					}
					else
					{
						$feedback['response_code'] = 'FAIL:202';
					}



				}
			}else{
				$feedback['response_code'] = 'FAIL:201';
			}

		}
  	
		if($feedback['response_code']=="OK")
		{
			$data_log["server_reply_action"]="";
			$data_log["api_token_sn"]=$apps_check_val;
			$data_log["client_action"]="[member_data]::$apps_member_val::$login_token";
			foreach($feedback as $key => $value)
			{
				$data_log["server_reply_action"].=$value."::";
			}
			$data_log["create_date"]=date("Y-m-d H:i:s");
			$data_log["create_member_sn"]="99999";
			$data_log["last_time_update"]=date("Y-m-d H:i:s");
			$data_log["update_member_sn"]="99999";

			$this->api_token_log($data_log);
		}
		//$this->CI->output->set_content_type('application/json')->set_output(json_encode($feedback));
		$this->ajax_feedback($feedback);

  }
  
  function token_renew()
  {
  	$feedback = "";
				$apps_check_val=0;$apps_member_val=0;$apps_member_val=0;
				
				if(!$login_token = $this->input->post('login_token'))
				{
					$login_token = $this->input->get('login_token');
				}
				
				if(!$login_token )
				{
						$feedback['response_code'] = 'FAIL';
				}else{
					
						$query = $this->db->select('*')
		                        ->from('ij_api_token')
		                        ->where('api_token_status', 1)
		                        ->where('api_token ', $login_token)
											      ->get();
											        if($query->num_rows() > 0)
											        {
															        	$apps_check = $query->row_array();
														        		
														        		foreach($apps_check as $key => $value)
																		    {
																		        			if($key=="api_token_sn")
																				        		$apps_check_val = $value;
																				        	if($key=="api_token_end_time")
																				        		$api_token_end_time = $value;
																		    }
														        		if($api_token_end_time < date("Y-m-d H:i:s"))
														        		{
														        			$feedback['response_code'] = 'TIME_EXPIRE';
														        	  }else{
														        	  	
														        	  		$api_token_end_time=date("Y-m-d H:i:s", strtotime('+1 hour'));
														        	  		$data = array(
																						               'api_token_end_time' => $api_token_end_time,
																						            );
																						$this->db->where('api_token_sn', $apps_check_val);
																						$this->db->update('ij_api_token', $data); 
														        	  	  $feedback['response_code'] = 'OK';
														        	  }
														   }else{
														   	
														   				$feedback['response_code'] = 'FAIL';
														  }
					
				}
				
			if($feedback['response_code']=="OK")
		 {
			$data_log["api_token_sn"]=$apps_check_val;
			$data_log["server_reply_action"]="";
		 	$data_log["client_action"]="[renew_token]::$login_token";
		 	foreach($feedback as $key => $value)
		 	{
		 		$data_log["server_reply_action"].=$value."::";
		 	}
		 	$data_log["create_date"]=date("Y-m-d H:i:s");
		 	$data_log["create_member_sn"]="99999";
		 	$data_log["last_time_update"]=date("Y-m-d H:i:s");
		 	$data_log["update_member_sn"]="99999";
		 	
		 	$this->api_token_log($data_log);
		}
		  //$this->CI->output->set_content_type('application/json')->set_output(json_encode($feedback));
		  $this->ajax_feedback($feedback);

  }
  function api_token_log($data_log)
  {
  	
  	
		 return $this->db->insert('ij_api_token_log', $data_log);
  	
  }
  
  function sql_api_token($data_log)
  {
  	
  	
		 return $this->db->insert('ij_api_token', $data_log);
  	
  }
  function getToken($username)
  {
    $_token1 = "";
    
    $_token1=$username."-".time()."-";
    $_token1.=mt_rand(10000, 99999);
    $token=base64_encode($_token1);
    return $token;
 }
	
	public function get_dealer_ad()
	{
				$data_log = array();
        $data_log['ip'] = $this->input->ip_address();
        $data_log['time_used'] = date('Y-m-d H:i:s');
				$feedback = "";
				$apps_check_val=0;$apps_member_val=0;$apps_member_val=0;
				
				if(!$wedding_website_sn = (int)$this->input->post('wedding_website_sn'))
				{
					$wedding_website_sn = (int)$this->input->get('wedding_website_sn');
				}
				if(!$wedding_website_sn)
				{
					$feedback['response_code'] = 'FAIL:no wedding_website_sn input';
				}else{

					$query = $this->db->select('stock_out_dealer_sn')
						->from('ij_serial_card')
						->where('associated_wedding_website_sn', $wedding_website_sn)
						//->join('ij_coupon_code','ij_coupon_code.coupon_code=ij_order.coupon_code','left')
						//->join('ij_serial_card','ij_serial_card.associated_order_sn=ij_order.order_sn')
						->get();
		//echo $this->db->last_query();
						//echo $query->num_rows();
			        if($query->num_rows() > 0)
			        {
								$dealers = $query->result_array();
								//$dealer_array=explode(',',$row['apply_dealer']);
								foreach($dealers as $key=>$dealer){
								$dealer_sn=$dealer['stock_out_dealer_sn'];
								//get dealer data
									$query = $this->db->select('dealer_sn,website_url,dealer_name,dealer_blessing,dealer_thanksgiving,page_profile_pic_save_dir,customize_website_url')
										->from('ij_dealer')
										->where('dealer_sn', $dealer_sn)
										->get();
							        if($query->num_rows() > 0)
							        {
												$row = $query->row_array();
												//GET dealer domain_name
												$query = $this->db->select('domain_name')
													->from('ij_member_domain_relation')
													->where('associated_dealer_sn', $dealer_sn)
													->where('ij_member_domain_relation.status', '1')
													->join('ij_biz_domain','ij_biz_domain.domain_sn=ij_member_domain_relation.domain_sn')
													->get();
										        if($query->num_rows() > 0)
										        {
										        $domaintypes=	$query->result();
			        							$domain_name='';
										        foreach($domaintypes as $dt){
										        	$domain_name[]=$dt->domain_name;
										        }
														$row['domain_name']=implode(',',$domain_name);
													}else{
														$row['domain_name']='';
													}
													$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,strpos( $_SERVER["SERVER_PROTOCOL"],'/'))).'://';
													$row['page_profile_pic_save_dir']=$protocol.$_SERVER['HTTP_HOST'].'/public/uploads/'.substr($row['page_profile_pic_save_dir'], 0, -1);
												
												$feedback[$key] = $row;
											}
								}
												//var_dump($feedback).'<br>';
												//exit();												
							}else{
								$feedback['response_code'] = 'FAIL:no dealer found';
							}

				}
		  //$this->CI->output->set_content_type('application/json')->set_output(json_encode($feedback));				
		  $this->ajax_feedback($feedback);
	}
  private function ajax_feedback($result){
      header('Content-Type: text/plain; charset=utf-8');
      echo json_encode($result);
  }
}
