<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 客服中心往返的 msg 列表
*/
?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
  <h4 class="modal-title">婚禮網站參數設定</h4>
</div>
<form role="form" name="reply_form" method="post" action="admin/admin/weddingWebsite_item">
<div class="modal-body">
    <div class="row">
		  <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	          <label class="col-lg-3 control-label">婚禮網站代號</label>
	      			<div class="col-lg-4 col-md-4 col-sm-4">
		        			<?php echo $_result[0]['wedding_website_sn']?>
		      		</div>
	      			<div class="col-lg-3 col-md-3 col-sm-3">
	      				&nbsp;
		      		</div>
	      			<div class="col-lg-2 col-md-2 col-sm-2">
	      				&nbsp;
		      		</div>
		      </div>
		  </div>
		  <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	          <label class="col-lg-3 control-label">使用期限(起)</label>
                  <div class="col-md-9">
                    <div class="input-group date datepicker">
                      <input type="text" class="form-control" name="wedding_website[website_start_date]" value="<?php echo (@$_result[0]['website_start_date']!='0000-00-00 00:00:00')? @$_result[0]['website_start_date']:''; ?>">
                      <span class="input-group-btn">
                        <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                      </span>
                    </div>
                  </div>
	          <label class="col-lg-3 control-label">使用期限(止)</label>
                  <div class="col-md-9">
                    <div class="input-group date datepicker">
                      <input type="text" class="form-control" name="wedding_website[website_end_date]" value="<?php echo (@$_result[0]['website_end_date']!='0000-00-00 00:00:00')? @$_result[0]['website_end_date']:''; ?>">
                      <span class="input-group-btn">
                        <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                      </span>
                    </div>
                  </div>
		      </div>
	    </div> 		  
		  <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	          <label class="col-lg-3 control-label">可上傳相片數量</label>
	      			<div class="col-lg-4 col-md-4 col-sm-4">
		        			<input type="text" class="form-control" name="wedding_website[upload_pic_total_amount]" value="<?php echo $_result[0]['upload_pic_total_amount']?>">
		      		</div>
	      			<div class="col-lg-3 col-md-3 col-sm-3">
	      				已使用數量  
		      		</div>
	      			<div class="col-lg-2 col-md-2 col-sm-2">
	      				<?php echo $_result[0]['used_upload_pic_amount']?>  
		      		</div>
		      </div>
	    </div> 
		  <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	          <label class="col-lg-3 control-label">可分享相片數量</label>
	      			<div class="col-lg-4 col-md-4 col-sm-4">
		        			<input type="text" class="form-control" name="wedding_website[album_pic_total_amount]" value="<?php echo $_result[0]['album_pic_total_amount']?>">
		      		</div>
	      			<div class="col-lg-3 col-md-3 col-sm-3">
	      				已使用數量  
		      		</div>
	      			<div class="col-lg-2 col-md-2 col-sm-2">
	      				<?php echo $_result[0]['used_album_pic_amount']?>  
		      		</div>
		      </div>
	    </div>
		  <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	          <label class="col-lg-3 control-label">可使用影片數量</label>
	      			<div class="col-lg-4 col-md-4 col-sm-4">
		        			<input type="text" class="form-control" name="wedding_website[album_video_total_amount]" value="<?php echo $_result[0]['album_video_total_amount']?>">
		      		</div>
	      			<div class="col-lg-3 col-md-3 col-sm-3">
	      				已使用數量  
		      		</div>
	      			<div class="col-lg-2 col-md-2 col-sm-2">
	      				<?php echo $_result[0]['used_album_video_amount']?>  
		      		</div>
		      </div>
	    </div>
		  <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	          <label class="col-lg-3 control-label">可使用簡訊數</label>
	      			<div class="col-lg-4 col-md-4 col-sm-4">
		        			<input type="text" class="form-control" name="wedding_website[sms_total_amount]" value="<?php echo $_result[0]['sms_total_amount']?>">
		      		</div>
	      			<div class="col-lg-3 col-md-3 col-sm-3">
	      				已使用數量  
		      		</div>
	      			<div class="col-lg-2 col-md-2 col-sm-2">
	      				<?php echo @$_result[0]['sms_used_amount']?>  
		      		</div>
		      </div>
	    </div>
		  <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	          <label class="col-lg-3 control-label">獨立網址</label>
	      			<div class="col-lg-4 col-md-4 col-sm-4">
		        			<input type="text" class="form-control" name="wedding_website[customize_website_url_flag]" value="<?php echo $_result[0]['customize_website_url_flag']?>">
		      		</div>
	      			<div class="col-lg-3 col-md-3 col-sm-3">
	      				  （1：有，0：無）
		      		</div>
	      			<div class="col-lg-2 col-md-2 col-sm-2">
		      		</div>
		      </div>
	    </div>	    
      <!--div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	        <label class="control-label col-lg-3">關聯廠商</label>
	        <div class="col-lg-9">
	          <input type="hidden" name="dealer_short_name" class="form-control select2_dealer" value="<?php echo $_member_dealer;?>">
	        </div>
	        <!-- select option 在 page_item_js 中 ->
      	</div>
      </div-->
    </div>
</div>

  <div class="modal-footer">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6">
        <button data-dismiss="modal" style="margin-top: 20px;margin-right: 100px;" type="button" id="cancelbutton" class="btn yellow">取消</button> &nbsp; 
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6">
        <button style="margin-top: 20px;margin-right: 100px;" type="submit" id="replybutton" class="btn blue">送出</button>
        <input type="hidden" name="wedding_website_sn" value="<?php echo $_result[0]['wedding_website_sn']?>">
        <input type="hidden" name="member_sn" value="<?php echo $_result[0]['member_sn']?>">
      </div>
    </div>
  </div>
</form>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/components-form-tools.js"></script>
<script src="public/metronic/admin/pages/scripts/components-pickers.js"></script>
<script>
jQuery(document).ready(function() {       
   	//Metronic.init(); // init metronic core components
	  //Layout.init(); // init current layout
    ComponentsPickers.init();
	 jQuery(".datepicker").datetimepicker({
        autoclose: true,
        isRTL: Metronic.isRTL(),
        format: "yyyy-mm-dd hh:ii",
        pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left")
    });
    //jQuery('.datepicker').datetimepicker({ format: 'yyyy-mm-dd hh:ii' });
    
	/*$(".select2_dealer").select2({
	    //tags: ["典華1", "典華2", "典華3", "典華4", "典華5"]
	    tags: [<?php echo @$_dealer_list;?>]
	});*/
});
</script>