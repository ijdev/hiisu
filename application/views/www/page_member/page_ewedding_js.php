<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容的 page level js
*/
?>

<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
<script src="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->
<script src="public/metronic/global/plugins/zoom/jquery.zoom.min.js" type="text/javascript"></script><!-- product zoom -->
<script src="public/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script><!-- Quantity -->
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>


<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/frontend/layout/scripts/layout.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init();
        Layout.initFixHeaderWithPreHeader();
        Layout.initNavScrolling();

	    jQuery('.various').fancybox({
			maxWidth	: 800,
			maxHeight	: 600,
			fitToView	: false,
			width		: '70%',
			autoSize	: true,
			closeClick	: false,
			openEffect	: 'none',
			closeEffect	: 'none'
		});

				$('.add-cart').click(function(e) {
					var product_sn=$(this).attr('product_sn');
					var order_sn=$(this).attr('order_sn');
					var product_package_name=$(this).attr('product_package_name');
					var product_spec=$(this).attr('product_spec');
					var url = "shop/add";
					    $.ajax({
					           type: "POST",
					           url: url,
										 dataType:'json',
					           data: { product_sn:product_sn,qty:'1',original_order_sn:order_sn,product_spec:product_spec },
					           success: function(data)
					           {
					//console.log(data);
					           	if (data.error){
					           		alert(data.error);
					           	}else{
					           		$('.top-cart-info-count').text(data.total_items+' 件');
					           		$('.top-cart-info-value').text('$ '+data.total);
					           		//更新購物車popup
					           		$('.top-cart-content').load('shop/top_cart');
					           		location.href="shop/shopView";
					           		//console.log(data.total);
					           	}
					           }
					         });

					    e.preventDefault();
				});        
			$("#ajax_msg").on("show.bs.modal", function(e) {
			    var link = $(e.relatedTarget);
			    $(this).find(".modal-content").load(link.attr("href"));
			});
			$('.various').click(function(e) {
					var product_sn=$(this).attr('product_sn');
					var order_sn=$(this).attr('order_sn');
					if(product_sn && order_sn){
						$('#product_sn').val(product_sn);
						$('#order_sn').val(order_sn);
					}else{
						$('#helpword').text('請注意英文字母大小寫視為不同，輸入後若有效將自動新增婚禮網站．');
					}
  		});
	   $(document).on('click', '.deleteSite', function(e) {
	        var button = $(this);
	        	var r = confirm('請確認是否刪除此網站?');
	            if(r)
	            {
	                		button.parents().parents().filter('tr').remove();
							    /*$.get(button.attr('href'), function(data){
					          console.log(data);
	                	if (!data){
	                		button.parents().parents().filter('tr').remove();
	                	}else{
	                		alert('刪除失敗');
	                	}
							    });*/
	            }else{
				    		e.preventDefault();
				  		}
	    });
    });
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->