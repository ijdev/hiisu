<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容
*/
?>
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<div class="container mob_padding">

<!-- Page level plugin styles START -->
<!-- Page level plugin styles END -->
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <li class="active">婚禮網站列表</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar col-md-2 col-sm-3">
    <div class="desktop_view_sidebar">
		<ul class="list-group margin-bottom-25 sidebar-menu">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
	<div class="mobile_view_sidebar">
		<div class="side-nav-head border_title_bottom">
			<button class="fa fa-bars collapsed" data-toggle="collapse" data-target="#mob_sidebar_drop_down"></button>
			<h4 class="bfont">會員中心<!--會員中心--></h4>
		</div>
		<ul class="list-group margin-bottom-25 sidebar-menu collapse" id="mob_sidebar_drop_down">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="col-md-10 col-sm-9">
    <div class="product-page mob_padding">
        <h1>
          <i class="icon-globe"></i> 婚禮網站列表
        </h1>
      <?php $this->load->view('www/general/flash_error');?>
      <div class="table-responsive">
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th style="text-align:center;">編號</th>
                    <th style="text-align:center;">網站名稱</th>
                    <th style="text-align:center;">款型</th>
                    <th style="text-align:center;">版型</th>
                    <th style="text-align:center;">訂單編號</th>
                    <th style="text-align:center;">建立時間</th>
                    <th style="text-align:center;">功能</th>
                </tr>
            </thead>
            <tbody>
<?php if($wedding_website_list){foreach($wedding_website_list as $key=>$_Item){?>
                <tr>
                    <td align="center">
												<?php echo $_Item['wedding_website_sn']?>
                    </td>
                    <td>
                        <a href="<?php echo $_Item['wedding_website_url'];?>" target="_blank">
                            <?php echo $_Item['wedding_website_title'];?> <i class="fa fa-eye"></i>
                        </a>
                    </td>
                    <td align="center">
                        <?php echo @$_Item['product_package_name'];?>
                    </td>
                    <td align="center">
                        <?php echo @$_Item['product_name'];?>
                    </td>
                    <td align="center">
                        <?php echo $_Item['sub_order_sn']?>
                    </td>
                    <td align="center">
                        <small><?php echo $_Item['create_date'];?></small>
                    </td>
                    <td align="center">
                        <a href="<?php echo $_Item['wedding_website_edit_url'];?>" class="btn default btn-xs yellow-stripe" target="_blank">編輯</a>&nbsp;
                        <a data-toggle="modal" href="#serialNo" class="btn default btn-xs green-stripe" disabled>公佈</a>&nbsp;
                        <!--a data-toggle="modal" href="#upgradeForm" class="btn default btn-xs blue-stripe">升級</a>&nbsp;-->
                        <a product_spec="<?php echo $_Item['product_spec'];?>" product_package_name="<?php echo $_Item['product_package_name'];?>" order_sn="<?php echo $_Item['associated_order_sn'];?>" product_sn="<?php echo $_Item['associated_product_sn'];?>" href="<?php echo (@$_Item['product_spec']=='')?'#':'';?>" class="btn default btn-xs blue-stripe add-cart" <?php echo (@$_Item['product_spec']=='')?'disabled':'';?>>升級</a>&nbsp;
                        <a data-remote="false" data-target="#ajax_msg" data-toggle="modal" href="member/ewedding_addon/<?php echo $_Item['wedding_website_sn']?>" title="尚未" class="btn default btn-xs blue-stripe" <?php echo (@$_Item['product_package_name']=='試用版')?'disabled':'';?>>加購</a>&nbsp;
												<a class="various" order_sn="<?php echo $_Item['associated_order_sn'];?>" product_sn="<?php echo $_Item['associated_product_sn'];?>" href="#coupon_input"><button type="button" class="btn btn-circle btn-xs red-sunglo">輸入序號</button></a>
                        <a href="<?php echo $_Item['wedding_website_delete_url'];?>" target="_blank" class="deleteSite btn default btn-xs red-stripe">刪除</a>
                    </td>
                </tr>
<?php }}?>
            </tbody>
        </table>
        <a href="#coupon_input" class="various btn green btn-block">
            使用序號券新增我的婚禮網站 <i class="fa fa-plus"></i>
        </a>
      </div>
    </div>
  </div>
  <!-- END CONTENT -->
</div>
<!-- END SIDEBAR & CONTENT -->
</div>
<div class="modal fade" id="ajax_msg" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">婚禮網站加購</h4>
      </div>
      <div class="modal-body">
        <img src="public/metronic/global/img/loading-spinner-grey.gif" alt="" class="loading">
        <span>
        &nbsp;&nbsp;內容載入中... </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn blue">Save changes</button>
      </div>
    </div>
  </div>
</div>
<div id="coupon_input" style="display:none;">
  <div style="width:75%">
    <div style="height:32px;"></div>
    <strong>請輸入序號券</strong>
    <form class="form-horizontal" role="form" method="post">
      <div class="form-body">
        <div class="form-group">
          <div class="col-md-12">
            <div class="input-group">
<div style="display:-webkit-box;text-align:center;">
	              <input type="text" name="serialnumber" style="width:420px;" class="form-control" placeholder="請輸入序號並按確定">
              <span class="input-group-btn">
              <button class="btn blue couponcode" type="submit">確定</button>
              </span>
<div class="note note-success note-bordered" id="note_couponcode" style="text-align:center;width:250px;display:none;"></div>
</div>
            </div>
            <span class="help-block" id="helpword">
              請注意英文字母大小寫視為不同，輸入後若有效將增加該婚禮網站的規格．
            </span>
          </div>
        </div>
      </div>
<input type="hidden" id="order_sn" name="order_sn" value="">
<input type="hidden" id="product_sn" name="product_sn" value="">
    </form>
  </div>
</div>