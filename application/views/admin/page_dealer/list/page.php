<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
if($Item['page_home_slider_save_dir']){
	$slider=explode(':',$Item['page_home_slider_save_dir']);
}
if($Item['page_aboutus_pic_save_dir']){
	$aboutus=explode(':',$Item['page_aboutus_pic_save_dir']);
}
if($Item['page_cover_photo_save_dir']){
	$banner=explode(':',$Item['page_cover_photo_save_dir']);
	//var_dump($slider);
}
if($Item['page_profile_pic_save_dir']){
	$profile=explode(':',$Item['page_profile_pic_save_dir']);
	//var_dump($slider);
}
if($Item['page_login_wallpaper_save_dir']){
	$login=explode(':',$Item['page_login_wallpaper_save_dir']);
	//var_dump($slider);
}
if($Item['page_aboutus_description']){
	$aboutus_description=explode(':::',$Item['page_aboutus_description']);
	//var_dump($slider);
}
?>
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>會員管理 <small>經銷商專頁管理</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        會員管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/Member/dealerTable">經銷商</a>
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         經銷商專頁設定
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">經銷商專頁設定</span>
            </div>
            <div class="tools">
              <a href="admin/Member/dealerTable/edit/<?php echo $Item['dealer_sn'];?>">經銷商編輯</span></a> &nbsp; 
              <a href="admin/Member/dealerTable"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
          	<?php $this->load->view('sean_admin/general/flash_error');?>
                  <form id="page_form" class="form-horizontal error" method="post" enctype="multipart/form-data"> 
                    <div class="form-body">
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">專頁名稱</label>
                        <div class="col-lg-8">
                          <input type="text" name="dealer[dealer_name]" id="dealer_name" class="form-control" value="<?php echo $Item['dealer_name'];?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">服務描述(Meta)</label>
                        <div class="col-lg-8">
                          <textarea class="ckeditor form-control" name="dealer[dealer_description]" id="dealer_description" rows="3"><?php echo $Item['dealer_description'];?></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">關鍵字設定(KeyWords)</label>
                        <div class="col-lg-8">
                          <textarea class="ckeditor form-control" name="dealer[page_keywords]" id="page_keywords" rows="3"><?php echo $Item['page_keywords'];?></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">客製化子域名</label>
                        <div class="col-lg-8">
                          	<?php if($Item['customize_website_url']){ ?>
                         		 <p class="form-control-static">
                          		http://<?php echo $Item['customize_website_url'].'.qwedding.club</p> &nbsp; <a href="http://'.$Item['customize_website_url'].'.qwedding.club" target="_blank">&nbsp;<i class="fa fa-external-link"></i></a>';
                          		}else{?>
                          		<label class="col-lg-1 control-label">http://</label><input style="width:120px;" type="text" class="form-control inline" id="customize_website_url" name="dealer[customize_website_url]" placeholder="">.qwedding.club &nbsp; <div class="note note-success note-bordered inline" id="showresult" style="width:200px;display:none;"></div>
                          	<?php }?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-md-3 control-label">專頁頭像</label>
                        <div class="col-md-8">
												<?php if(@$profile[0]){?>
															<div id="profile1">
			                          <a class="fancybox" rel="profile" title="專頁頭像" href="/public/uploads/<?php echo @$profile[0];?>">
			                          	<img src="/public/uploads/<?php echo @$profile[0];?>" width="180" style="vertical-align: bottom; margin-right:12px; padding:2px; border:1px solid #EFEFEF;" >
			                          </a>
		                             <i class="fa fa-trash-o image_del" Itemid="profile1" kind="page_profile_pic_save_dir" filename="<?php echo @$profile[0];?>" style="cursor: pointer;"></i>
		                           </div>
		                    <?php }?>
		                          <input class="form-control" name="profile[]" id="page_profile_pic_save_dir1" accept="jpeg,jpg,gif,png" type="file">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">紛絲專頁網址</label>
                        <div class="col-lg-8">
                          <input type="url" class="form-control" id="page_facebook_url_set" name="dealer[page_facebook_url_set]" value="<?php echo $Item['page_facebook_url_set'];?>" placeholder="https://www.facebook.com/kutewedding">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">紛絲專頁id</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" id="page_facebook_id" name="dealer[page_facebook_id]" value="<?php echo $Item['page_facebook_id'];?>" placeholder="空白則由程式自動抓取">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">其他社群媒體</label>
                        <div class="col-lg-8">
	                      <?php if(@$soical_media){ foreach($soical_media as $key=>$sm){?>
	                      <input type="hidden" name="social_media[<?php echo $key;?>][dealer_soical_media_sn]" value="<?php echo @$sm['dealer_soical_media_sn']; ?>">
                            <div class="form-group">
                              <label for="name" class="col-lg-1 control-label">名稱</label>
                              <div class="col-lg-4">
                                <input type="text" class="form-control" name="social_media[<?php echo $key;?>][soical_media_name]" id="page_social_media_url_title_1" value="<?php echo @$sm['soical_media_name'];?>">
                              </div>
                              <label for="name" class="col-lg-2 control-label">連結</label>
                              <div class="col-lg-5">
                                <input type="url" class="form-control" name="social_media[<?php echo $key;?>][soical_media_hyperlink]" id="page_social_media_url_value_1" value="<?php echo @$sm['soical_media_hyperlink'];?>">
                              </div>
                            </div>
                       <?php }}else{?>
                            <div class="form-group">
                              <label for="name" class="col-lg-1 control-label">名稱</label>
                              <div class="col-lg-4">
                                <input type="text" class="form-control" name="social_media[0][soical_media_name]" value="" placeholder="空白＝刪除">
                              </div>
                              <label for="name" class="col-lg-2 control-label">連結</label>
                              <div class="col-lg-5">
                                <input type="url" class="form-control" name="social_media[0][soical_media_hyperlink]" value="">
                              </div>
                            </div>
                        <?php }?>
	                      <div id="sample_spec" style="display:none;">
                            <div class="form-group">
                              <label for="name" class="col-lg-1 control-label">名稱</label>
                              <div class="col-lg-4">
                                <input type="text" class="form-control" name="Count[soical_media_name]" placeholder="空白＝刪除">
                              </div>
                              <label for="name" class="col-lg-2 control-label">連結</label>
                              <div class="col-lg-5">
                                <input type="url" class="form-control" name="Count[soical_media_hyperlink]" placeholder="">
                              </div>
                            </div>
                        </div>
                            <div id="social_media">
                            </div>
                            <div class="form-group">
                              <div class="col-lg-12">
                                <button id="media_add" type="button" class="btn btn-sm pull-right" value="">新增媒體</button>
                              </div>
                            </div>
                        </div>
                      </div>
                      <!--hr>
                      <h4>婚禮網站廣告設定</h4>
                       <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">感謝有您預設內容</label>
                        <div class="col-lg-8">
                          <textarea class="ckeditor form-control" name="dealer[dealer_blessing]" id="dealer_blessing" rows="3"><?php echo $Item['dealer_blessing'];?></textarea>
                          <span class="help-block">描述將會顯示在新人結婚網站『感謝有您』功能中，最多 140 個中文字</span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">訪客留言預設內容</label>
                        <div class="col-lg-8">
                          <textarea class="ckeditor form-control" name="dealer[dealer_thanksgiving]" id"dealer_thanksgiving" rows="3"><?php echo $Item['dealer_thanksgiving'];?></textarea>
                          <span class="help-block">將會顯示在新人結婚網站『訪客留言』功能中，最多 140 個中文字</span>
                        </div>
                      </div-->
                      <hr>
                      <h4>首頁</h4>
                      <div class="form-group">
                        <label for="name" class="col-md-3 control-label">首頁 Slides
                            <span class="help-block">(1800 x 845)</span></label>
                        <div class="col-md-8">
													<div class="col-md-4">
											<?php if(@$slider[0]){?>
														<div id="slider1">
		                          <a class="fancybox" rel="slider" title="首頁 Slides-1" href="/public/uploads/<?php echo @$slider[0];?>">
		                          	<img src="/public/uploads/<?php echo @$slider[0];?>" width="180" style="vertical-align: bottom; margin-right:12px; padding:2px; border:1px solid #EFEFEF;" >
		                          </a>
	                             <i class="fa fa-trash-o image_del" Itemid="slider1" kind="page_home_slider_save_dir" filename="<?php echo @$slider[0];?>" style="cursor: pointer;"></i>
	                           </div>
	                    <?php }?>
	                          <input class="form-control" name="slider[]" id="page_home_slider_save_dir1" accept="jpeg,jpg,gif,png" type="file">
	                        </div>
													<div class="col-md-4">
											<?php if(@$slider[1]){?>
														<div id="slider2">
		                          <a class="fancybox" rel="slider" title="首頁 Slides-2" href="/public/uploads/<?php echo @$slider[1];?>">
			                          <img src="/public/uploads/<?php echo @$slider[1];?>" width="180" style="vertical-align: bottom; margin-right:12px; padding:2px; border:1px solid #EFEFEF;" >
			                        </a>
		                             <i class="fa fa-trash-o image_del" Itemid="slider2" kind="page_home_slider_save_dir" filename="<?php echo @$slider[1];?>" style="cursor: pointer;"></i>
		                        </div>
	                    <?php }?>
	                          <input class="form-control" name="slider[]" id="page_home_slider_save_dir2" accept="jpeg,jpg,gif,png" type="file">
	                        </div>
													<div class="col-md-4">
											<?php if(@$slider[2]){?>
														<div id="slider3">
		                          <a class="fancybox" rel="slider" title="首頁 Slides-3" href="/public/uploads/<?php echo @$slider[2];?>">
		                         	 <img src="/public/uploads/<?php echo @$slider[2];?>" width="180" style="vertical-align: bottom; margin-right:12px; padding:2px; border:1px solid #EFEFEF;" >
		                         </a>
	                             <i class="fa fa-trash-o image_del" Itemid="slider3" kind="page_home_slider_save_dir" filename="<?php echo @$slider[2];?>" style="cursor: pointer;"></i>
	                           </div>
	                    <?php }?>
	                          <input class="form-control" name="slider[]" id="page_home_slider_save_dir3" accept="jpeg,jpg,gif,png" type="file">
	                        </div>
                        </div>
                      </div>
                      <hr>
                      <h4>關於我們設定</h4>
                      <div class="form-group">
                        <label for="name" class="col-md-3 control-label">關於我們介紹圖
                        <span class="help-block">(360 x 均可)</span></label>
                        <div class="col-md-8">
													<div class="col-md-4">
											<?php if(@$aboutus[0]){?>
														<div id="aboutus1">
		                          <a class="fancybox" rel="aboutus" title="關於我們-1" href="/public/uploads/<?php echo @$aboutus[0];?>">
		                          	<img src="/public/uploads/<?php echo @$aboutus[0];?>" width="180" style="vertical-align: bottom; margin-right:12px; padding:2px; border:1px solid #EFEFEF;" >
		                          </a>
	                             <i class="fa fa-trash-o image_del" Itemid="aboutus1" kind="page_aboutus_pic_save_dir" filename="<?php echo @$aboutus[0];?>" style="cursor: pointer;"></i>
	                           </div>
	                    <?php }?>
	                          <input class="form-control" name="aboutus[]" id="page_aboutus_pic_save_dir1" accept="jpeg,jpg,gif,png" type="file">
	                        </div>
													<div class="col-md-4">
											<?php if(@$aboutus[1]){?>
														<div id="aboutus2">
		                          <a class="fancybox" rel="aboutus" title="關於我們-2" href="/public/uploads/<?php echo @$aboutus[1];?>">
			                          <img src="/public/uploads/<?php echo @$aboutus[1];?>" width="180" style="vertical-align: bottom; margin-right:12px; padding:2px; border:1px solid #EFEFEF;" >
			                        </a>
		                             <i class="fa fa-trash-o image_del" Itemid="aboutus2" kind="page_aboutus_pic_save_dir" filename="<?php echo @$aboutus[1];?>" style="cursor: pointer;"></i>
		                        </div>
	                    <?php }?>
	                          <input class="form-control" name="aboutus[]" id="page_aboutus_pic_save_dir2" accept="jpeg,jpg,gif,png" type="file">
	                        </div>
													<div class="col-md-4">
											<?php if(@$aboutus[2]){?>
														<div id="aboutus3">
		                          <a class="fancybox" rel="aboutus" title="關於我們-3" href="/public/uploads/<?php echo @$aboutus[2];?>">
		                         	 <img src="/public/uploads/<?php echo @$aboutus[2];?>" width="180" style="vertical-align: bottom; margin-right:12px; padding:2px; border:1px solid #EFEFEF;" >
		                         </a>
	                             <i class="fa fa-trash-o image_del" Itemid="aboutus3" kind="page_aboutus_pic_save_dir" filename="<?php echo @$aboutus[2];?>" style="cursor: pointer;"></i>
	                           </div>
	                    <?php }?>
	                          <input class="form-control" name="aboutus[]" id="page_aboutus_pic_save_dir3" accept="jpeg,jpg,gif,png" type="file">
	                        </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-md-3 control-label">介紹圖描述</label>
                        <div class="col-md-8">
													<div class="col-md-4">
	                          <input class="form-control" name="dealer[page_aboutus_description][0]" id="page_aboutus_pic_save_description1" type="text" value="<?php echo @$aboutus_description[0];?>">
	                        </div>
													<div class="col-md-4">
	                          <input class="form-control" name="dealer[page_aboutus_description][1]" id="page_aboutus_pic_save_description2" type="text" value="<?php echo @$aboutus_description[1];?>">
	                        </div>
													<div class="col-md-4">
	                          <input class="form-control" name="dealer[page_aboutus_description][2]" id="page_aboutus_pic_save_description3" type="text" value="<?php echo @$aboutus_description[2];?>">
	                        </div>
                        </div>
                      </div>                      
                      <div class="form-group">
                        <label for="name" class="col-md-3 control-label">關於我們背景圖
                        <span class="help-block">(1800 x 965)</span></label>
                        <div class="col-md-8">
												<?php if(@$banner[0]){?>
															<div id="banner1">
			                          <a class="fancybox" rel="banner" title="專頁大 Banner" href="/public/uploads/<?php echo @$banner[0];?>">
			                          	<img src="/public/uploads/<?php echo @$banner[0];?>" width="180" style="vertical-align: bottom; margin-right:12px; padding:2px; border:1px solid #EFEFEF;" >
			                          </a>
		                             <i class="fa fa-trash-o image_del" Itemid="banner1" kind="page_cover_photo_save_dir" filename="<?php echo @$banner[0];?>" style="cursor: pointer;"></i>
		                           </div>
		                    <?php }?>
		                          <input class="form-control" name="banner[]" id="page_cover_photo_save_dir1" accept="jpeg,jpg,gif,png" type="file">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-md-3 control-label">登入頁背景圖
                        <span class="help-block">(1800 x 965)</span></label>
                        <div class="col-md-8">
												<?php if(@$login[0]){?>
															<div id="login1">
			                          <a class="fancybox" rel="login" title="登入頁背景圖" href="/public/uploads/<?php echo @$login[0];?>">
			                          	<img src="/public/uploads/<?php echo @$login[0];?>" width="180" style="vertical-align: bottom; margin-right:12px; padding:2px; border:1px solid #EFEFEF;" >
			                          </a>
		                             <i class="fa fa-trash-o image_del" Itemid="login1" kind="page_login_wallpaper_save_dir" filename="<?php echo @$login[0];?>" style="cursor: pointer;"></i>
		                           </div>
		                    <?php }?>
		                          <input class="form-control" name="login[]" id="page_login_wallpaper_save_dir1" accept="jpeg,jpg,gif,png" type="file">
                        </div>
                      </div>
                      <hr>
                      <h4>聯絡我們設定</h4>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">說明內容</label>
                        <div class="col-lg-8">
                          <input class="form-control"  name="dealer[page_contactus_content]" id="page_contactus_content" value="<?php echo $Item['page_contactus_content'];?>">
                          <span class="help-block">聯絡我們頁面中提醒客戶的文字資訊 </span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">聯絡電話</label>
                        <div class="col-lg-8">
                         <input class="form-control" name="dealer[office_tel]" id="office_tel" value="<?php echo $Item['office_tel'];?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">聯繫地址</label>
                        <div class="col-lg-8">
                           <input class="form-control" name="dealer[addr1]" id="addr1" value="<?php echo $Item['addr1'];?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">營業時間</label>
                        <div class="col-lg-8">
                          <textarea class="ckeditor form-control" id="dealer_opening_time"  name="dealer[dealer_opening_time]" rows="7"><?php echo trim($Item['dealer_opening_time']);?></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                          <button type="submit" class="btn green">儲存</button>
                          <button type="reset" class="btn default">Cancel</button>
                        </div>
                      </div>
                    </div>                    
                  </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->