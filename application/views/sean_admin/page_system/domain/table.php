<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>產業屬性 <small>屬性列表</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main/">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        系統設定
        <i class="fa fa-circle"></i>
      </li>
      <li>
        產業屬性設定
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">產業屬性列表</span>
            </div>
            <div class="actions btn-set">
              <a href="<?php echo $_action_link;?>" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>產業屬性名稱</th>
                  <th>是否啟用</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
              	
              	  <?php
               
              	 if(is_array($_result01))
                	{                                  
                 			foreach($_result01 as $key => $value)
                    		{                    			                    			                    			
                    			if($value->status == 1)
                    				{
                    					$_active = '<i class="fa fa-check"></i>';
                    				}
                    			else
                    				{
                    					$_active = '<i class="fa fa-times"></i>';
                    				}
               
               ?>
                
                  <tr class="odd gradeX">
                    <td><?php echo $value->domain_name;?></td>
                    <td class="center"><?php echo $_active;?></td>
                    <td class="center">
                      <a href="<?php echo $_edit_link.$value->domain_sn;?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                      <a href="<?php echo $_delete_link.$value->domain_sn;?>" onclick="return deletechecked();"><i class="fa fa-trash-o"></i>刪除</a>
                    </td>
                  </tr>
               <?php
               
			             }
			           }
           			?>
              	
              	
              
              </tbody>
            </table>

            <div class="row">
             
             
            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->