<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容
*/
?>
<?php $_web_member_data=$this->session->userdata('web_member_data'); //var_dump($_web_member_data);?>
<!-- Page level plugin styles START -->
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<style type="text/css">
  .select2_dealer{
    padding: 0px;
  }
</style>
<div class="container mob_padding">
<!-- Page level plugin styles END -->
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <li class="active">修改密碼</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar col-md-2 col-sm-3">
    <div class="desktop_view_sidebar">
		<ul class="list-group margin-bottom-25 sidebar-menu">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
	<div class="mobile_view_sidebar">
		<div class="side-nav-head border_title_bottom">
			<button class="fa fa-bars collapsed" data-toggle="collapse" data-target="#mob_sidebar_drop_down"></button>
			<h4 class="bfont">會員中心<!--會員中心--></h4>
		</div>
		<ul class="list-group margin-bottom-25 sidebar-menu collapse" id="mob_sidebar_drop_down">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="col-md-10 col-sm-9">
    <div class="product-page prd_paswrd">
      <h1 class="display_none">
        <i class="fa fa-user"></i> 修改密碼
      </h1>
      <div class="row">
          <?php $this->load->view('www/general/flash_error');?>
          <form name="register-form" id="register-form" novalidate method="post" action="member/memberModifyPassword">
		<div class="row chnge_pass">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label><b>帳號</b></label>
									<p class="form-control-static"><p><?php echo $_web_member_data["user_name"];?></p></p>
								</div>
              </div>
							<div class="col-xs-12 col-sm-12">
								<div class="form-group has-feedback">
									<label><b>原密碼</b></label>
									<input name="old_pwd" type="password" class="form-control" id="old_pwd"></div>
							</div>
              <div class="col-xs-12 col-sm-12">
								<div class="form-group has-feedback">
									<label><b>新密碼</b></label>
									<input name="password" type="password" class="form-control" id="password"></div>
							</div>
							<div class="col-xs-12 col-sm-12">
								<div class="form-group has-feedback">
									<label><b>確認密碼</b></label>
									<input name="confirm_password" type="password" class="form-control" id="confirm_password"></div>
							</div>
							<div class="col-xs-12 col-sm-12">
								<button type="submit" name="Submit" class="col-xs-12 col-sm-4 col-sm-offset-4 btn btn-black">確定</button>
							</div>
						</div>
            </form>
      </div>
    </div>
  </div>
  <!-- END CONTENT -->
</div>
</div>
<!-- END SIDEBAR & CONTENT -->
<style>
.display_none{
	display:none;
}
.chnge_pass{
	float:left;
	width:40%;
}
.btn-black {
	margin-top:20px;
    background-color: #111;
    color: #FFF;
}
</style>