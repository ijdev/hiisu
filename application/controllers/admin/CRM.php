<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Crm extends MY_Controller {
 	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->library('sean_lib/Sean_form_general');
		$this->load->model('general/My_general_ct');
		$this->load->config('ij_config');
		$this->load->config('ij_sys_config');
		$this->load->model('sean_models/Sean_general');
		$this->load->model('sean_models/Sean_db_tools');
		$this->load->model('libraries_model');
		$this->load->helper('cookie');
		$this->page_data['unreadmessage']= $this->libraries_model->get_unread_message_count();
		$this->page_data['init_control']="admin/";
		$this->init_control=$this->page_data['init_control'];
	}
	public function ticketTable()
	{
		$flg=$this->input->post('flg',true);
		if($flg=='reset'){
			redirect(base_url('admin/CRM/ticketTable'));
			exit();
		}else{
			$from=($this->input->post('from',true))? $this->input->post('from',true):date('Y-m-d', strtotime(date('Y-m-d'). ' - 3 days'));
			$to=($this->input->post('to',true))? $this->input->post('to',true):date('Y-m-d');
		}
		//$where=array('message_status'=>'1','ij_member_message_center_log.send_count >'=>'0');
		$where=array();
		if($from) $where['issue_date >=']=$from.' 00:00:00';
		if($to) $where['issue_date <=']=$to.' 23:59:59';
		$page_data = array();

		// 內容設定

		//echo $this->session->userdata['member_data']['associated_supplier_sn'];
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['from'] = $from;
		$page_data['page_content']['to'] = $to;
		$page_data['page_content']['messages'] = $this->libraries_model->_get_callcenter_list($where);
		//echo $this->db->last_query();

		$page_data['page_content']['view_path'] = 'admin/page_callcenter/ticket_content'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_callcenter/ticket_content_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}
	/**
	 * 客服問題狀態管理 Template
	 *
	 */
	public function ticketStatus()
	{
		if($data_array=$this->input->post('qs')){
			if($this->input->post('member_question_status')){ //判斷新增或修改
					//$data_array = $this->libraries_model->CheckUpdate($data_array,0);
					$this->libraries_model->_update('ij_member_question_status_ct',$data_array,'member_question_status',$this->input->post('member_question_status'));
					$this->session->set_userdata(array('Mes'=>'更新成功！'));
					redirect(base_url('/admin/CRM/ticketStatus'));
			}else{
					$data_array['sort_order']='1';
					$data_array['default_flag']='0';
					$data_array['display_flag']='1';
					//$data_array = $this->libraries_model->CheckUpdate($data_array,1);
					$this->libraries_model->_insert('ij_member_question_status_ct',$data_array);
					$this->session->set_userdata(array('Mes'=>'新增成功！'));
					redirect(base_url('/admin/CRM/ticketStatus'));
			}
		}
		$page_data = array();
		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_callcenter/ticket_status_table'; // 頁面主內容 view 位置, 必要

		$page_data['page_content']['Items']=$this->libraries_model->_select('ij_member_question_status_ct','display_flag','1',0,0,0,0,'result_array');

		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_callcenter/ticket_status_table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}
	 public function Ticketcategory()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Ticketcategory";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by ccapply_code asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_config';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Ticketcategory_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Ticketcategory";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."修改";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";


    //取得下一個排序值
    $_next_sort=1;
    //$_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where,$_id);

			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



	  /* Begin 修改 */

    /*		產生表單 			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
*/

    $this->page_data["em_columns"] = array(
		  'ccapply_name'=>array('header'=>$_table_field["ccapply_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'ccapply_eng_name'=>array('header'=>$_table_field["ccapply_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'ccapply_description'=>array('header'=>$_table_field["ccapply_description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );
   /* End 修改 */




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}
	public function TicketCategory2($_id = 0,$_name = null)
	{

		$this->LoginCheck();
		$this->page_data['_page_title']="<h1>問題次分類 <small>列表</small></h1>";
    	$this->page_data['_caption_subject']= "問題次分類列表";

    	if($_id > 0)
    	$this->page_data['_caption_subject']= "<a href=admin/CRM/TicketCategory2/>問題次分類列表</a> - <font color=red>[".urldecode($_name)."]</font>";

    	$this->session->set_userdata('sys_program_father_id',$_id);
    	$this->session->set_userdata('sys_program_father_title',$_name);

        $this->page_data['_father_link']="TicketCategory2";
        $this->page_data['_action_link']=$this->page_data['init_control']."TicketCategory_item2/add/$_id";

	    /*
	    	Begin  model 查詢結果
	      回傳  _table_thead_name _body_result
	    */
	    $this->load->model('backend_system/Ticketcategory2');
	    //特殊條件  $_where="  order by sort_order asc";
	    $_where="";
	    $_result=$this->Ticketcategory2->table_value_list($_where,$_id);

	    /* End model 查詢結果，回傳 */

	    // 列表標頭
	    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
	    // 列表內容
	    $this->page_data['_body_result']= $_result["_body_result"];

		// general view setting
		$this->page_data['page_content']['view_path'] = 'admin/page_callcenter/ticket_category2_table'; // 頁面主內容 view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'admin/page_callcenter/ticket_category2_table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $this->page_data);
	}

	public function Ticketcategory2_item($_action,$_id = 0){
		$this->LoginCheck();

    $this->page_data['_page_title']="<h1>問題次分類 <small>列表與搜尋</small></h1>";
    $this->page_data['_caption_subject']="問題次分類編輯";
    $this->page_data['_father_link']="/admin/CRM/TicketCategory2/".$this->session->userdata('sys_program_father_id')."/".$this->session->userdata('sys_program_father_title');
    $this->page_data['_action_link']="/admin/CRM/Ticketcategory2_item/adding/$_id";

    $this->load->model('backend_system/Ticketcategory2');
	$_table_field=$this->Ticketcategory2->get_define_value("_table_field");
	($_id > 0 ) ? $this->Ticketcategory2->set_define_value("_key_id",$_id) : "";
	(strlen($_action) > 0 ) ? $this->Ticketcategory2->set_define_value("_action",$_action) : "";

    $_next_sort=1;
    $_next_sort=$this->Ticketcategory2->get_next_sort($_id);
    //編輯
    if($_action=="edit")
		{
			$_where=" where ccapply_detail_code='".$_id."'";
			$_result01=$this->Ticketcategory2->action_edit($_where);
			$this->page_data['_action_link']="/admin/CRM/Ticketcategory2_item/update/$_id";
			$this->page_data['_result01']=$_result01;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ccapply_detail_code='".$_id."'";
			$_result01=$this->Ticketcategory2->action_update($_where,$_id);
			$this->session->set_flashdata("success_message","更新完成！");
			redirect(base_url("/admin/CRM/TicketCategory2/"));
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ccapply_detail_code ='".$_id."' ";
			$_result01=$this->Ticketcategory2->action_delete($_where,$_id);
			if(!is_array($_result01))
			{
			$this->session->set_flashdata("success_message","刪除完成！");
		  }else{
				$_error_message=$this->Sean_db_tools->db_message_handle($_result01["message"]);
				$this->session->set_flashdata("message","刪除失敗 ==> ".$_error_message);
		  }
		  redirect("/admin/CRM/TicketCategory2/".$this->session->userdata('sys_program_father_id')."/".$this->session->userdata('sys_program_father_title'));
			exit();
		}


    //新增
		if($_action == "adding")
		{

			$_result=$this->Ticketcategory2->action_insert($_id);
			$this->session->set_flashdata("success_message","新增完成！");

		}


		$ccapply_code = $this->libraries_model->_select('ij_ccapply_code_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','ccapply_code,ccapply_name');
		//var_dump($ccapply_code);

    /*
    item  list
    */
    $this->page_data["em_columns"] = array(
		  'ccapply_code'=>array('header'=>$_table_field["ccapply_code"],'type'=>($_action=="view") ? 'label':'enum', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50','source'=>$ccapply_code),
		  'ccapply_detail_code_name'=>array('header'=>$_table_field["ccapply_detail_code_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'ccapply_detail_code_eng_name'=>array('header'=>$_table_field["ccapply_detail_code_eng_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'ccapply_detail_description'=>array('header'=>$_table_field["ccapply_detail_description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			//'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );


		$this->page_data['page_content']['view_path'] = 'admin/page_callcenter/ticket_category2_item'; // 頁面主內容 view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'admin/page_callcenter/ticket_category2_item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $this->page_data);
	}

	/**
	 * 全站通知 Template
	 *
	 */
	public function notification($type="sender",$member_message_center_sn=0)
	{
		$page_data = array();
		if($this->input->post('notification')){
			if(!$this->input->post('notify_ta')){
				//$this->session->set_userdata(array('Mes'=>'通知對象必須選擇'));
				//$this->session->set_flashdata("message","通知對象必須選擇");
				//redirect(base_url('admin/CRM/notification/'.$type));
				$this->ijw->_wait(base_url("admin/CRM/notification/").$type , 2 , '通知對象必須選擇',1);
			}
			if(is_array($this->input->post('notify_ta'))){
				$notify_ta=$this->input->post('notify_ta');
		        $this->db->select('member_sn,email');
		        $this->db->from('ij_member');
		        $this->db->where_in('member_level_type',$notify_ta);
		        $this->db->where('member_status','2');
		        $this->db->order_by('member_sn');
				$member_sn_array=$this->db->get()->result_array();
				//echo $this->db->last_query();
				//echo $notify_ta;
			}else{
				$member_sn_array=$this->libraries_model->_select('ij_member',array('ij_member.member_status'=>'2','ij_member.member_level_type'=>$this->input->post('notify_ta')),0,0,0,'ij_member.member_sn',0,'result_array','ij_member.member_sn,email');
			}
			//var_dump($notify_ta);
			//var_dump($member_sn_array);
			//exit();
			$data_array=$this->input->post('notification');
			if(!$member_message_center_sn=$this->input->post('member_message_center_sn')){ //判斷新增或修改
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				if($data_array['default_display_time'] && $data_array['default_display_time']!='0000-00-00 00:00:00'){
					$data_array['transmit_date']=$data_array['default_display_time'];
				}else{
					$data_array['transmit_date']=@$data_array['last_time_update'];
				}
				$data_array['send_count']=count($member_sn_array);
				$data_array['message_status']='1';
				$data_array['main_config_flag']='1'; //新增主訊息為1
				$data_array['notify_ta']=implode(':',$notify_ta);
				$member_message_center_sn=$this->libraries_model->_insert('ij_member_message_center_log',$data_array);
				foreach($member_sn_array as $_Item){
					$data_array['send_count']=0;
					$data_array['associated_member_message_center_sn']=$member_message_center_sn;
					$data_array['member_sn']=$_Item['member_sn'];
					$this->libraries_model->_insert('ij_member_message_center_log',$data_array);
					$this->load->library("ijw");
					$this->ijw->send_mail('系統訊息',$_Item["email"],$_Item['member_sn'],$data_array['message_content']);
				}
				$this->session->set_userdata(array('Mes'=>'系統訊息新增成功，一共送出 '.count($member_sn_array).' 筆會員訊息'));
			}else{
				$olddata = $this->libraries_model->_select('ij_member_message_center_log',array('message_status'=>'1','member_message_center_sn'=>$member_message_center_sn),0,0,0,'member_message_center_sn',0,'row');
				$ifchange=false; //判斷是否有更改資料
				foreach($data_array as $key=>$_Item){
					foreach($olddata as $key2=>$_Item2){
						//echo $key.' x '.$key2;
						if($key==$key2){
							if($_Item!=$_Item2){
								$ifchange=true;
								echo $key;
								break;
							}
						}
					}
				}
				//var_dump($ifchange);
				//exit();
				if($ifchange){ //一併異動
					//主訊息
					$data_array = $this->libraries_model->CheckUpdate($data_array,0);
					if($data_array['default_display_time'] && $data_array['default_display_time']!='0000-00-00 00:00:00'){
						$data_array['transmit_date']=$data_array['default_display_time'];
					}else{
						$data_array['transmit_date']=$data_array['last_time_update'];
					}
					$this->libraries_model->_update('ij_member_message_center_log',$data_array,'member_message_center_sn',$member_message_center_sn);
					//會員訊息先刪除(改為2)再重發
					$data_array['message_status']='2';
					$this->libraries_model->_update('ij_member_message_center_log',$data_array,array('associated_member_message_center_sn'=>$member_message_center_sn));
					foreach($member_sn_array as $_Item){
						$data_array['send_count']=0;
						$data_array['associated_member_message_center_sn']=$member_message_center_sn;
						$data_array['message_status']='1';
						$data_array['member_sn']=$_Item['member_sn'];
						$this->libraries_model->_insert('ij_member_message_center_log',$data_array);
					}
					$this->session->set_userdata(array('Mes'=>'系統訊息修改且重置訊息成功，筆數：'.count($member_sn_array)));
				}else{
					$this->session->set_userdata(array('Mes'=>'系統訊息無異動!'));
				}
			}
			redirect(base_url('admin/CRM/notification'));
		}
		// 內容設定

		switch($type){
			case "form":
				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
			  $send_obj = $this->libraries_model->_select('ij_member_level_type_ct','display_flag','1',0,0,0,0,'result_array');
				if($member_message_center_sn){
					$page_data['page_content']['Item'] = $this->libraries_model->_select('ij_member_message_center_log',array('message_status'=>'1','member_message_center_sn'=>$member_message_center_sn),0,0,0,'member_message_center_sn',0,'row');
				}
			  foreach($send_obj as $key=>$_Item){
					if(strrpos(@$page_data['page_content']['Item']['notify_ta'],$_Item['member_level_type']) !== false){
						$send_obj[$key]['checked']='1';
					}else{
						$send_obj[$key]['checked']='0';
					}
			  }
			  //var_dump($send_obj);
				$page_data['page_content']['send_obj']=$send_obj;
				$page_data['page_content']['view_path'] = 'admin/page_callcenter/notification/item'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'admin/page_callcenter/notification/item_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('admin/general/main', $page_data);

			break;
			case "sender":
				$flg=$this->input->get('flg',true);
				if($flg=='reset'){
					redirect(base_url('admin/CRM/notification'));
					exit();
				}else{
					$from=$this->input->get('from',true);
					$to=$this->input->get('to',true);
					$message_subject=$this->input->get('message_subject',true);
					$ifshow=$this->input->get('ifshow',true);
					//$page=$this->input->get('page',true);
				}
				$where=array('message_status'=>'1','ij_member_message_center_log.send_count >'=>'0');
				if($from) $where['default_display_time >=']=$from.'-01';
				if($to) $where['default_display_time <=']=$to.'-'.date("t", strtotime($to));
				if($message_subject) $where['message_subject']=$message_subject;
				if($ifshow==1) $where['default_display_time <']=date('Y-m-d H:i:s');
				if($ifshow==2) $where['default_display_time >=']=date('Y-m-d H:i:s');
				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['from'] = $from;
				$page_data['page_content']['to'] = $to;
				$page_data['page_content']['message_subject'] = $message_subject;
				$page_data['page_content']['ifshow'] = $ifshow;
				$page_data['page_content']['messages'] = $this->libraries_model->_select('ij_member_message_center_log',$where,0,0,0,'ij_member_message_center_log.last_time_update','desc','result_array');
				$page_data['page_content']['view_path'] = 'admin/page_callcenter/notification/table'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'admin/page_callcenter/notification/table_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('admin/general/main', $page_data);

			break;
			case "item":
				// general view setting
				/*$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'admin/page_member/notification_item'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'admin/page_member/notification_item_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('admin/general/main', $page_data);*/

				if($member_message_center_sn){
					$data_array['read_flag'] = '1';
					$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				  $this->libraries_model->_update('ij_member_message_center_log',$data_array,'member_message_center_sn',$member_message_center_sn);

					$page_data['general_header'] = array(); // header 可以透過這個參數帶入
					$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				  $page_data['page_content']['view_path'] = 'admin/page_member/notification_item'; // 頁面主內容 view 位置, 必要
					$page_data['page_content']['message'] = $this->libraries_model->_select('ij_member_message_center_log',array('message_status'=>'1','ij_member_message_center_log.member_sn'=>$this->session->userdata['member_data']['member_sn'],'member_message_center_sn'=>$member_message_center_sn),0,0,0,'member_message_center_sn',0,'row');
					if($page_data['page_content']['message']['message_link_url']){
						redirect($page_data['page_content']['message']['message_link_url']);
					}
					$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				  $page_data['page_level_js']['view_path'] = 'admin/page_member/notification_item_js'; // 頁面主內容的 JS view 位置, 必要
					$this->load->view('admin/general/main', $page_data);
				}else{
						$this->session->set_flashdata("fail_message","很抱歉！查無該筆通知的詳細資料");
					  redirect(base_url('admin/CRM/notification/'));
				}
			break;
			default:
				if($this->input->post('flg')){
					//var_dump($this->input->post());
					if($this->input->post('flg')=='read'){ //判斷已讀或刪除
						$data_array['read_flag'] = '1';
						$this->session->set_userdata(array('Mes'=>'標為已讀成功!'));
					}elseif($this->input->post('flg')=='del'){
						$data_array['message_status'] = '2';
						$this->session->set_userdata(array('Mes'=>'刪除成功!'));
					}
					$data_array = $this->libraries_model->CheckUpdate($data_array,0);

					foreach($this->input->post('member_message_center_sn') as $_Item){
					//exit();

						$this->libraries_model->_update('ij_member_message_center_log',$data_array,'member_message_center_sn',$_Item);
					}
					redirect(base_url('admin/CRM/notification/'));
				}
				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

				$page=($this->input->get('page',true))? $this->input->get('page',true):'1';
				$this->load->library('pagination');
				$this->load->library('ijw');
				$page_data['page_content']['total'] = count($this->libraries_model->_select('ij_member_message_center_log',array('default_display_time <='=>date('Y-m-d H:i:s'),'message_status'=>'1','ij_member_message_center_log.member_sn'=>$this->session->userdata['member_data']['member_sn']),0,0,0,'ij_member_message_center_log.last_time_update','desc','result'));
				$page_data['page_content']['perpage'] = 50;
				if($page_data['page_content']['total']==0) $page=0;
				$page_data['page_content']['start'] = ($page > 1)? ($page-1)*$page_data['page_content']['perpage']:'0';
				$page_data['page_content']['end'] = ($page)? $page*$page_data['page_content']['perpage']:'0';
				$config = $this->ijw->_pagination('admin/CRM/notification', $page_data['page_content']['total'],$page_data['page_content']['perpage']);
				$this->pagination->initialize($config);
				//var_dump($page_data['page_content']['perpage']);
        		$page_data['page_content']['page_links'] = $this->pagination->create_links();
				$page_data['page_content']['messages'] = $this->libraries_model->_select('ij_member_message_center_log',array('default_display_time <='=>date('Y-m-d H:i:s'),'message_status'=>'1','ij_member_message_center_log.member_sn'=>$this->session->userdata['member_data']['member_sn']),0,$page_data['page_content']['start'],$page_data['page_content']['perpage'],'ij_member_message_center_log.last_time_update','desc','result_array');


				$page_data['page_content']['view_path'] = 'admin/page_member/notification'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'admin/page_member/notification_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('admin/general/main', $page_data);

			break;
		}
	}
}