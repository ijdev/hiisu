<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容
*/
?>
<div class="container">

<!-- Page level plugin styles START -->
<!-- Page level plugin styles END -->
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <li class="active">全站通知</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar col-md-2 col-sm-3">
    <div class="desktop_view_sidebar">
		<ul class="list-group margin-bottom-25 sidebar-menu">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
	<div class="mobile_view_sidebar">
		<div class="side-nav-head border_title_bottom">
			<button class="fa fa-bars collapsed" data-toggle="collapse" data-target="#mob_sidebar_drop_down"></button>
			<h4 class="bfont">會員中心<!--會員中心--></h4>
		</div>
		<ul class="list-group margin-bottom-25 sidebar-menu collapse" id="mob_sidebar_drop_down">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="col-md-10 col-sm-9">
    <div class="product-page">
      <h1>
        最新消息
      </h1>
      <?php $this->load->view('www/general/flash_error');?>
      <div class="row inbox">
        <div class="inbox-content">
          <table class="table table-striped table-advance table-hover">
            <thead>
              <tr>
			   
           

               
               <!-- <th class="pagination-control" colspan="3" style="text-align:right;">
                	<?php echo $page_links;?>
                  <!--span class="pagination-info"> <?php echo @$start;?>-<?php echo @$end;?> of <?php echo @$total;?> </span>
                  <a class="btn btn-sm blue">
                    <i class="fa fa-angle-left"></i>
                  </a>
                  <a class="btn btn-sm blue">
                    <i class="fa fa-angle-right"></i>
                  </a>-->
                <!-- </th>-->
              </tr>
            </thead>
            <form method="post" id="form1">
            <tbody>
              <?php if($messages){ foreach($messages as $key=>$_Item){?>
              <tr class="unread" data-messageid="<?php echo $key+1;?>">
                <td class="inbox-small-cells">
                  <input id="chk1<?php echo $key+1;?>" type="checkbox" class="fdcheck mail-checkbox" name="member_message_center_sn[]" value="<?php echo $_Item['member_message_center_sn'];?>">
				  <label class="sphov pull-left" for="chk1<?php echo $key+1;?>"></label>
				  
                </td>
                <td class="view-message hidden-xs">
                   <?php switch($_Item['message_code']){
                   		case 1:
                   			echo '客服:'.$_Item['update_member_sn'];
                   			break;
                   		default:
                   			echo '系統訊息';
                   		}?>
                </td>
                <td class="view-message" <?php echo (!$_Item['read_flag'])? 'style="font-weight: 700;font-size: 120%;"':'';?> >
                	<?php if($_Item['message_link_url']){?>
	                  <!--<a href="<?php echo $_Item['message_code'];?>" target="_blank"-->
                  		<a target="_blank" href="member/notification/detail/<?php echo $_Item['member_message_center_sn'];?>">
	                    <?php echo $_Item['message_content'];?>&nbsp;
	                    <i class="fa fa-external-link"></i>
	                  </a>
	                  <?php }else{?>
                  		<a href="member/notification/detail/<?php echo $_Item['member_message_center_sn'];?>"><?php echo $_Item['message_subject'];?></a>
	                  <?php }?>
                </td>
                <td class="view-message text-right">
                   <?php echo $_Item['transmit_date'];?>
                </td>
              </tr>
              <?php }}?>
              <!--tr class="unread" data-messageid="2">
                <td class="inbox-small-cells">
                  <input type="checkbox" class="mail-checkbox">
                </td>
                <td class="view-message hidden-xs">
                   客服人員
                </td>
                <td class="view-message ">
                  <a href="http://dev.ijwedding.com/www/memberOrderList" target="_blank">
                    您的訂單已成立&nbsp;
                    <i class="fa fa-external-link"></i>
                  </a>
                </td>
                <td class="view-message text-right">
                   16:30 PM
                </td>
              </tr>
              <?php for($i=0;$i<18;$i++):?>
              <tr data-messageid="<?php echo $i+3;?>">
                <td class="inbox-small-cells">
                  <input type="checkbox" class="mail-checkbox">
                </td>
                <td class="view-message hidden-xs">
                  系統訊息
                </td>
                <td class="view-message">
                  <a href="www/notification/detail">您的密碼已修改</a>
                </td>
                <td class="view-message text-right">
                   March 15
                </td>
              </tr>
              <?php endfor;?>-->
            </tbody>
            <input type='hidden' name='flg' id='flg' value=''>
          </form>
          </table>
        </div>
        <!-- /.inbox-content -->
      </div>
    </div>
  </div>
  <!-- END CONTENT -->
</div>
</div>
<!-- END SIDEBAR & CONTENT -->
<style>
a.btn.btn-sm.blue.dropdown-toggle {
    background: #ab8b64;
}

</style>