<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Page 主架構
*/
?>

    <div class="main-container with-padding">

        <!-- Begin Wrapper -->
        <div class="wrapper">

            <!-- Begin Inner Wrapper -->
            <div class="inner-wrapper">

                <!-- Begin Gallery -->
                <div class="gallery-albums grid">

                    <div class="row">

                        <div class="col full">

                            <div class="filters clearfix">

                                <a href="#" data-filter="*" class="active">ALL</a>
                                <a href="#" data-filter=".filter-indoors">分類一</a>
                                <a href="#" data-filter=".filter-outdoors">分類二</a>

                            </div>
                            
                        </div>

                    </div>

                    <div class="albums clearfix cols-4">
<?php if($wedding_websites){ foreach($wedding_websites as $key=>$photo){?>
                        <div class="album size-regular filter-indoors filter-models">
                            <div class="album-cont">
                                <img src="<?php echo $photo['main_picture_save_dir'];?>" alt="<?php echo $photo['wedding_website_content'];?>">
                                <a href="dealer/album/<?php echo $photo['associated_wedding_website_sn'];?>" class="overlay">
                                    <h5><?php echo $photo['wedding_website_title'];?> (#<?php echo $key+1;?>)</h5>
                                    <span class="btn-love love icon1-heart-empty"></span>
                                    <span class="love-2 icon1-heart"></span>
                                </a>
                            </div>
                        </div>
<?php }}?>
                        <!--div class="album size-regular filter-outdoors filter-models filter-fashion">
                            <div class="album-cont">
                                <img src="public/uno/images/gallery/g54.jpg" alt="">
                                <a href="dealer/album" class="overlay">
                                    <h5>網站名稱網站名稱 (#333)</h5>
                                    <span class="btn-love love icon1-heart-empty"></span>
                                    <span class="love-2 icon1-heart"></span>
                                </a>
                            </div>
                        </div>

                        <div class="album size-regular filter-indoors filter-models">

                            <div class="album-cont">

                                <img src="public/uno/images/gallery/g60.jpg" alt="">

                                <a href="dealer/album" class="overlay">

                                    <h5>網站名稱網站名稱 (#12)</h5>

                                    <span class="btn-love love icon1-heart-empty"></span>
                                    <span class="love-2 icon1-heart"></span>

                                </a>

                            </div>

                        </div>

                        <div class="album size-regular filter-outdoors ">

                            <div class="album-cont">

                                <img src="public/uno/images/gallery/g54.jpg" alt="">

                                <a href="dealer/album" class="overlay">

                                    <h5>網站名稱網站名稱 (#12)</h5>

                                    <span class="btn-love love icon1-heart-empty"></span>
                                    <span class="love-2 icon1-heart"></span>

                                </a>

                            </div>

                        </div>

                        <div class="album size-regular filter-indoors filter-models">

                            <div class="album-cont">

                                <img src="public/uno/images/gallery/g51.jpg" alt="">

                                <a href="dealer/album" class="overlay">

                                    <h5>網站名稱網站名稱 (#12)</h5>

                                    <span class="btn-love love icon1-heart-empty"></span>
                                    <span class="love-2 icon1-heart"></span>

                                </a>

                            </div>

                        </div>

                        <div class="album size-regular filter-indoors filter-models">

                            <div class="album-cont">
                                <img src="public/uno/images/gallery/g54.jpg" alt="">

                                <a href="dealer/album" class="overlay">

                                    <h5>網站名稱網站名稱 (#12)</h5>

                                    <span class="btn-love love icon1-heart-empty"></span>
                                    <span class="love-2 icon1-heart"></span>

                                </a>

                            </div>

                        </div>

                        <div class="album size-regular filter-indoors filter-portraits">

                            <div class="album-cont">

                                <img src="public/uno/images/gallery/g51.jpg" alt="">

                                <a href="dealer/album" class="overlay">

                                    <h5>網站名稱網站名稱 (#12)</h5>

                                    <span class="btn-love love icon1-heart-empty"></span>
                                    <span class="love-2 icon1-heart"></span>

                                </a>

                            </div>

                        </div-->

                    </div>

                </div>
                <!-- End Gallery -->

            </div>
            <!-- End Inner Wrapper -->

        </div>
        <!-- End Wrapper -->

    </div>