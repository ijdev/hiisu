<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品評價 <small>評價列表</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="preview">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="preview/productAssess">商品評價</a>
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         商品評價編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">評價編輯</span>
            </div>
            <div class="actions btn-set">
              <a href="preview/productAssess" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">產品名稱</label>
                  <div class="col-lg-8">
                    <span class="form-control-static"><a href="http://dev.ijwedding.com/preview_jerry/shopItem" target="_blank">商品一二三&nbsp;<i class="fa fa-external-link"></i></a></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">星等</label>
                  <div class="col-lg-8">
                    <div class="radio-list">
                      <?php for($i=0;$i<10;$i++):?>
                        <label class="radio-inline">
                        <input type="radio" name="optionsRadios" id="optionsRadios4" value="option1" checked>  <?php echo ($i+1)*0.5;?></label>
                      <?php endfor;?>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">會員姓名</label>
                  <div class="col-lg-8">
                    <span class="form-control-static"><a href="javascript:;" onclick="alert('前往個人檔案');">DS&nbsp;<i class="fa fa-external-link"></i></a></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">Email</label>
                  <div class="col-lg-8">
                    <span class="form-control-static"><a href="mailto:;" target="_blank">ds@gmail.com&nbsp;<i class="fa fa-envelope-o"></i></a></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-3 control-label">內容</label>
                  <div class="col-lg-8">
                    <textarea class="form-control" rows="3"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">是否顯示 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <select class="form-control">
                      <option>是</option>
                      <option>否</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-lg-3 control-label">更新者</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="administrator" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-3 control-label">更新時間</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="2015-05-25 10:00" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="button" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->