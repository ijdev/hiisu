<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/typeahead/typeahead.css">
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/pages/css/portfolio.css" rel="stylesheet" type="text/css"/>


<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>系統設定 <small>系統文件內容管理</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin-ijwedding/">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        系統設定
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin-ijwedding/doc">系統文件</a>
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         系統文件內容管理
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">文件名稱文件名稱 >> 編輯</span>
            </div>
            <div class="tools">
              <a href="admin-ijwedding/mail"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">文件名稱</label>
                  <div class="col-lg-10">
                    <p class="form-control-static">會員條款</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-2 control-label">內容</label>
                  <div class="col-lg-10">
                    <textarea class="ckeditor form-control" name="editor1" rows="12"><span style="font-family:新細明體;">第一條<br>
  在您申請成為本網站會員之前，請您詳閱會員規章，本規章明訂會員與本網站之間權利與義務關係，在您同意本規章後，方可成為本網站會員。<br>
  <br>
  第二條<br>
  本網站所提供之會員電子服務皆為免費，服務的所有權和運作權歸本網站，本網站有權修改服務項目或於任何時間停止各項服務。<br>
  <br>
  第三條<br>
  用戶隱私制度尊重用戶個人隱私是本網站的一項基本政策。所以，對於註冊會員資料本網站一定不會在未經會員合法授權時公開、編輯或透露其註冊資料及保存在本網站中的非公開內容，除非有法律許可要求。<br>
  <br>
  第四條<br>
  本網站為保障交易安全，將以您所填寫之基本資料中的電子郵件信箱作為帳號，並由您自行選定一組密碼作為個人登入辨識使用。您可依需要自行變更個人密碼。會員若發現任何非法使用會員帳號或存在安全漏洞的情況，請立即通告本網站。若因會員自行洩露帳號及密碼而造成會員損失及法律責任者，本網站不負連帶責任。<br>
  <br>
  第五條<br>
  電子信件本網站會不定期發送本網站電子報，提供會員相關技術、產品等訊息，但絕對保證會員之電子信箱不外流，避免會員受到不必要之干擾。<br>
  <br>
  第六條<br>
  本網站保留修訂本規章內容之權利與服務，如有更新事項將於網站上公布訊息，自公布日起1個月內可提出異議，若無回應本網站者，本網站視同該會員同意本網站規章之更動。<br>
  <br>
  第七條<br>
  本網站僅接受同一人之單筆資料，也就是說，您只能建立一次您的個人資料，請勿重覆登錄。<br>
  <br>
  第八條<br>
  會員不得以任何方式破壞及干擾本網站上各項資料與功能，且嚴禁入侵或破壞網路上任何系統之企圖或行為。<br>
  <br>
  第九條<br>
  會員同意其訂購行為，以本網站所示之電子交易資料為準，如有糾紛，並以該電子交易資料為認定標準。<br>
  <br>
  第十條<br>
  法律會員服務條款要與中華民國的法律解釋相一致，會員和本網站一致同意服從高等法院所有管轄。如發生本網站服務條款與中華民國法律相牴觸時，則這些條款將完全按法律規定重新解釋，而其它條款則依舊保持對會員產生法律效力和影響。<br>
  <br>
  第十一條<br>
  在本網站中各項商品交易，以中華民國法律為準據法，如有糾紛發生時，以台北地方法院為第一審管轄法院。<br>
  <br>
  第十二條<br>
  會員除應遵守本約定書外，亦同意遵守本網站各項交易規定。<br>
  <br>
  第十三條<br>
  一但被取消會員資格，將喪失所有會員權利，包含之前所有的會員福利。 本約定書之內容如有調整時，會員同意遵守調整後之約定書。<br>
  <br>
  第十四條<br>
  若有疑問，請至本網站客服中心詢問。<br>
  <br>
  第十五條<br>
  其他未盡訴之事項，依中華民國法律之相關規定之。</span></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-lg-2 control-label">更新者</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control"  placeholder="administrator" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-2 control-label">更新時間</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control"  placeholder="2015-05-25 10:00" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-2 col-md-10">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="button" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->