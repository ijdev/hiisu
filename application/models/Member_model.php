<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Member_model extends CI_Model{
    public function _select($table,$where = 0,$where_val = 0,$offset = 0,$per_page = 0,$order = 0,$desc = 0,$result = 'result',$select = 0,$like = 0,$distinct=0,$status=1){
        if (!$select) $select = $table.'.*';

        $this->db->from($table);
        if($this->db->field_exists('update_member_sn', $table)){ //有更新者欄位抓其user_name代替update_member_sn
	        $this->db->select($select.',ij_member.first_name as update_member_sn');
	        $this->db->join('ij_member','ij_member.member_sn='.$table.'.update_member_sn','left');
	      }else{
        	$this->db->select($select);
      	}

        if($where && $where_val && !$like){
            $this->db->where($where,$where_val);
        }elseif($where && !$where_val && !$like){
            $this->db->where($where);
        }elseif($where && $like && !$where_val){
            $this->db->like($where, $like);
        }else{
        	if($this->db->field_exists('status', $table)){
            $this->db->where('status','1');
          }
        }
        	if($status){
	        	if($this->db->field_exists('status', $table)){  //test除了指定以外都只抓1的
	            $this->db->where('status','1');
	          }
			    }
        if($order && $desc){
            $this->db->order_by($order,'desc');
        }elseif($order && !$desc){
            $this->db->order_by($order,'asc');
        }else{
            $this->db->order_by('sort_order','asc');
        }

        if($offset && $per_page){
            $this->db->limit($per_page,$offset);
        }elseif(!$offset && $per_page){
            $this->db->limit($per_page);
        }
        if($distinct){
            $this->db->distinct($distinct);
        }

        $query = $this->db->get();
		//echo $this->db->last_query();

        if($result == 'row'){
            return $query->row_array();
        }elseif($result == 'rows'){
        	foreach($query->result_array() as $row){
        		$rows[]=$row[$select];
        	}
          return @$rows;
        }elseif($result == 'result'){
        	$return_array=$query->result();
        	$query->free_result();
          return $return_array;
        }elseif($result == 'result_array'){
        	$return_array=$query->result_array();
        	$query->free_result();
          return $return_array;
        }elseif($result == 'num_rows'){
            return $query->num_rows();
        }elseif($result == 'strings'){
					if ($select){
            return implode('、',$query->row_array());
          }
        }
    }

    public function _update($table,$data,$where = 0,$where_val = 0 ){

        if($where && $where_val){
            $this->db->where($where,$where_val);
        }elseif($where && !$where_val){
            $this->db->where($where);
        }

        $this->db->update($table,$data);
		//echo $this->db->last_query();
        return ($this->db->affected_rows() > 0);
    }


    public function _insert($table,$data){

        $this->db->insert($table,$data);
        return $this->db->insert_id();

    }

    public function _delete($table,$where,$where_val = 0){

        if($where && $where_val){
            $this->db->where($where,$where_val);
        }elseif($where && !$where_val){
            $this->db->where($where);
        }

        $this->db->delete($table);

        return ($this->db->affected_rows() > 0);

    }

    public function _count_all($table){
        return $this->db->count_all($table);
    }
    public function _selectifsame($table,$where = 0,$where_val = 0,$wherenot = 0,$wherenot_val = 0){

        $this->db->select($where);
        $this->db->from($table);

        if($where && $where_val){
            $this->db->where($where,$where_val);
				}

        if($wherenot && $wherenot_val){
            $this->db->where($wherenot.' !=',$wherenot_val);
				}

        $query = $this->db->get();
        if ($query->num_rows()>0){
        	return true;
        }else{
        	return false;
        }

    }

    public function CheckUpdate($array,$ifnew=0)
    {
    	if(!$ifnew){
        $array['last_time_update']=date("Y-m-d H:i:s",time());
        $array['update_member_sn']=( isset($this->session->userdata['member_data']['member_sn'] ) ) ? $this->session->userdata['member_data']['member_sn'] : "1";
      }else{
        $array['create_date']=date("Y-m-d H:i:s",time());
        $array['create_member_sn']=( isset($this->session->userdata['member_data']['member_sn'] ) ) ? $this->session->userdata['member_data']['member_sn'] : "2";
      }
        return $array;
    }
	//抓取訂單s
  public function get_orders($member_sn=0,$page=0,$perpage=0){
  	if($member_sn){
      $this->db->select('*');
      $this->db->from("ij_order");
      $this->db->join("ij_payment_method_ct","ij_payment_method_ct.payment_method=ij_order.payment_method","left");
      $this->db->join("ij_payment_status_ct","ij_payment_status_ct.payment_status=ij_order.payment_status","left");
      $this->db->where('member_sn',$member_sn);
      $this->db->order_by('order_create_date','desc');
      if($page >= 1)$page=$page-1;
      if($page && $perpage){
          $this->db->limit($perpage,$page*$perpage);
      }elseif(!$page && $perpage){
          $this->db->limit($perpage);
      }
			//$result['sub_order']=$this->_select('ij_sub_order','order_sn',$Item2['order_sn'],0,0,'sub_order_sn',0,'result_array');
			if($page==0 && $perpage==0){
				$result=$this->db->get()->num_rows();
			}else{
				$result=$this->db->get()->result_array();
		//echo $this->db->last_query();
				foreach($result as $key=>$Item){
			      $this->db->select('*');
			      $this->db->from("ij_sub_order");
			      $this->db->join("ij_delivery_method_ct","ij_delivery_method_ct.delivery_method=ij_sub_order.delivery_method","left");
			      $this->db->join("ij_delivery_status_ct","ij_delivery_status_ct.delivery_status=ij_sub_order.delivery_status","left");
			      $this->db->join("ij_sub_order_status_ct","ij_sub_order_status_ct.sub_order_status=ij_sub_order.sub_order_status","left");
            $this->db->join("ij_supplier","ij_supplier.supplier_sn=ij_sub_order.associated_supplier_sn","left");
            $this->db->join("ij_payment_status_ct","ij_payment_status_ct.payment_status=ij_sub_order.pay_status","left");
			      $this->db->where('order_sn',$Item['order_sn']);
			      $result[$key]['sub_order']=$this->db->get()->result_array();
						foreach($result[$key]['sub_order'] as $key2=>$Item2){
							//$result[$key]['sub_order'][$key2]['order_item']=$this->_select('ij_order_item','sub_order_sn',$Item2['sub_order_sn'],0,0,'order_item_sn',0,'result_array');

				      $this->db->select('*,ij_order_item.order_item_sn as order_item_sn');
				      $this->db->from("ij_order_item");
				      $this->db->join("ij_order_cancel_log","ij_order_cancel_log.order_item_sn=ij_order_item.order_item_sn","left");
              //add
              //$this->db->join("ij_addon_config","ij_addon_config.addon_config_sn=ij_order_item.payment_status","left");
				      $this->db->where('sub_order_sn',$Item2['sub_order_sn']);
				      $result[$key]['sub_order'][$key2]['order_item']=$this->db->get()->result_array();
    //echo $this->db->last_query();
				      if($Item2['channel_name']=='婚禮網站'){
						    if(!$wedding_website_sn=$this->Shop_model->_select('ij_wedding_website',array('associated_order_sn'=>$Item['order_sn']),0,0,0,'wedding_website_sn',0,'row','wedding_website_sn',0,0,0)['wedding_website_sn']){
						    	$original_order_sn=$this->Shop_model->_get_order($Item['order_sn'],'original_order_sn')['original_order_sn'];
						    	if(!$wedding_website_sn=$this->Shop_model->_select('ij_wedding_website',array('associated_order_sn'=>$original_order_sn),0,0,0,'wedding_website_sn',0,'row','wedding_website_sn',0,0,0)['wedding_website_sn']){
						    		$wedding_website_sn=$this->Shop_model->_select('ij_wedding_website',array('associated_order_sn'=>$this->Shop_model->_get_order($original_order_sn,'original_order_sn')['original_order_sn']),0,0,0,'wedding_website_sn',0,'row','wedding_website_sn',0,0,0)['wedding_website_sn'];
	    						}
						    }
						  }else{
						  	$wedding_website_sn=0;
						  }

					    $result[$key]['sub_order'][$key2]['wedding_website_sn']=$wedding_website_sn;

							//foreach($result[$key]['sub_order'][$key2]['order_item'] as $key3=>$Item3){
							//	$result[$key]['sub_order'][$key2]['order_item'][$key3]
							//}
							$result[$key]['sub_order'][$key2]['sub_sum']=$this->_select('ij_order_item','sub_order_sn',$Item2['sub_order_sn'],0,0,'order_item_sn',0,'row','sum(actual_sales_amount) as sub_sum')['sub_sum']+$Item2['shipping_charge_amount'];
						}
						$result[$key]['sub_count']=$this->_select('ij_sub_order','order_sn',$Item['order_sn'],0,0,'order_sn',0,'num_rows');
				}
			}
			//var_dump($result);
			//exit();
			return $result;
		}else{
			return array();
		}
  }
    public function if_all_return($sub_order_sn=0){
        $this->db->select('*,ij_order_item.order_item_sn as order_item_sn');
        $this->db->from("ij_order_item");
        $this->db->join("ij_order_cancel_log","ij_order_cancel_log.order_item_sn=ij_order_item.order_item_sn","left");
        $this->db->where('sub_order_sn',$sub_order_sn);
        $this->db->where('order_cancel_sn!=','');
        $result=$this->db->get()->num_rows();
        //echo $this->db->last_query();
        return ($result>0)?true:false;
    }
}
?>