<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>行銷管理 <small>電子報發送</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
<div class="page-content" id="allpage">
  <div class="container">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">首頁</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        優惠活動
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/marketing/edm">電子報管理</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        電子報發送
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/marketing/edmItem/<?php echo $edm_sn;?>" target="_blank"><?php echo $edm_title;?></a>
      </li>
    </ul>
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">發送電子報</span>
            </div>
            <div class="actions btn-set">
            </div>
          </div>
          <div class="table-actions-wrapper pull-right">
          </div>
          <div class="portlet-body">
            <div class="note note-info">
              <div class="table-actions-wrapper form">
                <form class="form-horizontal" role="form" method="post">
                  <div class="form-body">
                    <div class="form-group">
                      <div class="radio-list col-md-12">
                        <label class="radio-inline">
                          <input type="radio" id="skip_cancel_edm_flag0" name="skip_cancel_edm_flag" value="0" <?php echo (@$Item['skip_cancel_edm_flag']=='0' || @$Item['skip_cancel_edm_flag']=='')?'checked':'';?>>
                          <label for="skip_cancel_edm_flag0">僅發送給有訂閱者</label>
                          <input type="radio" id="skip_cancel_edm_flag1" name="skip_cancel_edm_flag" value="1" <?php echo (@$Item['skip_cancel_edm_flag']=='1')?'checked':'';?>>
                          <label for="skip_cancel_edm_flag1">符合條件全部發送</label>
                        </label>
                      </div>
                    </div>                  	
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label class="radio-inline">
                          <input type="radio" required name="sendfilter" value="send_member_level_type" <?php echo (@$Item['send_member_level_type']=='9999')?'checked':'';?>> 按會員發送
                        </label>
                      </div>
                      <div class="col-md-9">
								<?php $edm_send_lists='';if(@$Item['edm_send_lists']){ foreach($Item['edm_send_lists'] as $key=>$_Item){
									$edm_send_lists.= trim($_Item['receiver_email']);
								if($key+1 < count($Item['edm_send_lists'])) $edm_send_lists.=',';}}?>                      	
                        <textarea class="form-control send_member_level_type" name="send_member_level_type[send_member_level_type]" rows="3" placeholder="輸入會員 Email, 多個請以逗號隔開"><?php echo $edm_send_lists;?></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label class="radio-inline">
                          <input type="radio" required name="sendfilter" value="send_product_set" <?php echo (@$Item['send_product_set'])?'checked':'';?>> 按商品發送
                        </label>
                      </div>
                      <div class="col-md-9">
                        <div class="input-group">
                          	<span class="input-group-addon">由</span>
                          	<input type="text" class="form-control form-control-inline input-small date-picker send_product_set" date="2016-05-14" name="send_product_set[order_start_date]" value="<?php echo (@$Item['order_start_date']!='0000-00-00' && @$Item['send_product_set'])?@$Item['order_start_date']:'';?>">
	                          <span class="input-group-addon">到</span>
	                          <input type="text" class="form-control form-control-inline input-small date-picker send_product_set" name="send_product_set[order_end_date]" value="<?php echo (@$Item['order_end_date']!='0000-00-00' && @$Item['send_product_set'])?@$Item['order_end_date']:'';?>">
	                          <span class="input-group-addon"> 購買過商品編號 </span>
	                          <input type="text" class="form-control form-control-inline input-small send_product_set" name="send_product_set[send_product_set]" value="<?php echo @$Item['send_product_set'];?>" placeholder="商品編號">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label class="radio-inline">
                          <input type="radio" required name="sendfilter" value="over_order_amount" <?php echo (@$Item['over_order_amount'])?'checked':'';?>> 按訂單金額發送
                        </label>
                      </div>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon">由</span>
                          <input type="text" class="form-control form-control-inline input-small date-picker over_order_amount" name="over_order_amount[order_start_date]" value="<?php echo (@$Item['order_start_date']!='0000-00-00' && @$Item['over_order_amount'])?@$Item['order_start_date']:'';?>">
                          <span class="input-group-addon">到</span>
                          <input type="text" class="form-control form-control-inline input-small date-picker over_order_amount" name="over_order_amount[order_end_date]" value="<?php echo (@$Item['order_end_date']!='0000-00-00' && @$Item['over_order_amount'])?@$Item['order_end_date']:'';?>">
                          <span class="input-group-addon"> 訂單金額超過 </span>
                          <input type="number" class="form-control form-control-inline input-small over_order_amount" name="over_order_amount[over_order_amount]" value="<?php echo @$Item['over_order_amount'];?>" placeholder="金額下限">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label class="radio-inline">
                          <input type="radio" required name="sendfilter" disabled > 按會員產業屬性發送
                        </label>
                      </div>
                      <div class="col-md-9">
                        <input type="hidden" class="form-control select2_category" disabled value="">
                        <!-- select option 在 send_js 中 -->
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label class="radio-inline">
                          <input type="radio" required name="sendfilter" disabled> 按聯盟會員發送
                        </label>
                      </div>
                      <div class="col-md-9">
                        <input type="text" class="form-control" disabled placeholder="輸入聯盟會員編號">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label class="radio-inline">
                          <input type="radio" required name="sendfilter" disabled> 按聯盟會員產業屬性發送
                        </label>
                      </div>
                      <div class="col-md-9">
                        <input type="hidden" class="form-control select2_category" disabled value="">
                        <!-- select option 在 send_js 中 -->
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label class="radio-inline">
                          <input type="radio" required name="sendfilter" value="send_dealer_set" <?php echo (@$Item['send_dealer_set'])?'checked':'';?>> 按經銷商發送
                        </label>
                      </div>
                      <div class="col-md-9">
                        <input type="text" class="form-control send_dealer_set" name="send_dealer_set[send_dealer_set]" value="<?php echo @$Item['send_dealer_set'];?>" placeholder="輸入經銷商編號">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label class="radio-inline">
                          <input type="radio" required name="sendfilter" value="send_biz_domain_set" <?php echo (@$Item['send_biz_domain_set'])?'checked':'';?>> 按經銷商產業屬性發送
                        </label>
                      </div>
                      <div class="col-md-9">
                        <input type="hidden" class="form-control select2_category send_biz_domain_set" name="send_biz_domain_set[send_biz_domain_set]" value="<?php echo @$Item['send_biz_domain_set'];?>">
                        <!-- select option 在 send_js 中 -->
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label class="radio-inline">
                          <input type="radio" required name="sendfilter" value="wedding_year_month" <?php echo (@$Item['wedding_year_month'])?'checked':'';?>> 按結婚月份
                        </label>
                      </div>
                      <div class="col-md-9">
                        <input type="text" class="form-control form-control-inline input-small date-picker1 wedding_year_month" name="wedding_year_month[wedding_year_month]" value="<?php echo @$Item['wedding_year_month'];?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label class="radio-inline">
                          <input type="radio" required name="sendfilter" value="wedding_date" <?php echo ($edm_send_sn && (@$Item['wedding_start_date']!='0000-00-00' || @$Item['wedding_end_date']!='0000-00-00'))?'checked':'';?>> 按結婚日期
                        </label>
                      </div>
                      <div class="col-md-9">
                        <div class="input-group input-large date-picker input-daterange  pull-left" data-date="2015-05-01" data-date-format="yyyy-mm-dd">
                          <input type="text" class="form-control input-sm wedding_date" name="wedding_date[wedding_start_date]" value="<?php echo (@$Item['wedding_start_date']!='0000-00-00')?@$Item['wedding_start_date']:'';?>">
                          <span class="input-group-addon"> 到 </span>
                          <input type="text" class="form-control input-sm wedding_date" name="wedding_date[wedding_end_date]" value="<?php echo (@$Item['wedding_end_date']!='0000-00-00')?@$Item['wedding_end_date']:'';?>">
                        </div>
                      </div>
                    </div>
                  </div>
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label>
                          　　備註
                        </label>
                      </div>
                      <div class="col-md-9">
                        <textarea class="form-control" rows="3" name="memo"><?php echo @$Item['memo'];?></textarea>
                      </div>
                    </div>
                  <div class="form-actions right1">
                    <button class="btn btn-sm yellow"><i class="fa fa-check"></i> <?php echo ($edm_send_sn)?'修改':'加入';?></button> &nbsp; 
                    <?php if($edm_send_sn){?>
                    	<button type="button" onclick="location.href='admin/marketing/edmSend/<?php echo $edm_sn;?>';" class="btn btn-sm blue"><i class="fa fa-check"></i> 新增 </button>
                  	<?php }?>
   			            <input type="hidden" name="edm_id" value="<?php echo $edm_sn;?>">
   			            <input type="hidden" name="edm_send_sn" value="<?php echo $edm_send_sn;?>">
   			            <input type="hidden" name="send_time" value="<?php echo @$Item['send_time'];?>">
               </div>
                </div>
              </form>
              <div style="clear:both;"></div>
            </div>
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>發送條件</th>
                  <th>僅訂閱者</th>
                  <th>發送數量</th>
                  <th>開信數</th>
                  <th>寄送日期</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
								<?php if($Items){ foreach($Items as $key=>$_Item){?>
                  <tr class="odd gradeX">
                    <td class="center"><?php echo @$_Item['send_config'];?></td>
                    <td class="center">
                      <?php if($_Item['skip_cancel_edm_flag']=='0'):?>
                        <i class="fa fa-check"></i>
                      <?php else:?>
                        <i class="fa fa-times"></i>
                      <?php endif;?>
                    </td>
                    <td class="center"><?php echo $_Item['send_amount'];?></td>
                    <td class="center"><?php echo $_Item['edm_open_amount'];?></td>
                    <td>
                      <?php echo ($_Item['send_time']!='0000-00-00 00:00:00')? $_Item['send_time']:'尚未發送'; ?>
                    </td>
                    <td class="center">
                      	<a href="admin/marketing/edmSendList/<?php echo $_Item['edm_id'];?>/<?php echo $_Item['edm_send_sn'];?>"><i class="fa fa-users"></i>瀏覽</a>&nbsp;&nbsp;&nbsp;
                      <?php if($_Item['send_time']=='0000-00-00 00:00:00'){ ?>
                      	<a href="javascript:;" onclick="SendEdm('<?php echo $_Item['edm_send_sn'];?>','<?php echo $edm_title.'-'.@$_Item['send_config'];?>');"><i class="fa fa-envelope-o"></i>寄送</a>&nbsp;&nbsp;&nbsp;
                      	<a href="admin/marketing/edmSend/<?php echo $_Item['edm_id'];?>/<?php echo $_Item['edm_send_sn'];?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                      	<a href="javascript:void(0);" class="deleteItem" ItemId="<?php echo $_Item['edm_send_sn'];?>"><i class="fa fa-trash-o"></i>刪除</a>
                      <?php }?>
                    </td>
                  </tr>
                <?php }}?>
              </tbody>
            </table>

          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->