<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="public/metronic/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/pages/css/inbox.css" rel="stylesheet" type="text/css"/>

<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>全站通知</h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">總覽</a><i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/CRM/notification">全站通知</a><i class="fa fa-circle"></i>
      </li>
      <li class="active">
        訊息內容
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-bell-o font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">訊息內容</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/CRM/notification" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body">
            <div class="inbox-content">
              <div class="inbox-header inbox-view-header">
              	<h1><?php echo $message['message_subject'];?></h1>
              </div>
              <div class="inbox-view-info">
                <div class="row">
                  <div class="col-md-7">
                    <span class="bold"> <?php echo $message['update_member_sn'];?> </span>
                    on <?php echo $message['transmit_date'];?>
                  </div>
                  <div class="col-md-5 inbox-info-btn">
         					<form method="post" id="form1">
                   <div class="btn-group">
                      <button onclick="$('#form1').submit();" class="btn blue reply-btn"><i class="fa fa-trash-o"></i> Delete </button>
                    </div>
					            <input type='hidden' name='flg' id='flg' value='del'>
					            <input type='hidden' name='member_message_center_sn[]' id='member_message_center_sn' value='<?php echo $message['member_message_center_sn'];?>'>
					        </form>
                    </div>
                  </div>
                </div>
                <div class="inbox-view">
                	<?php echo $message['message_content'];?>
                </div>
              </div>
            </div>
            <!-- /.inbox-content -->
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->