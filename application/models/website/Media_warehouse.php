<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Media_warehouse extends CI_Model {


    public $limit = 10; // query limit setting
    public $until = 0; // last query sn

    public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
    }
    /*
    * member 媒體 folder 列表
    * 全數拉出管理
    */
    public function get_member_media_warehouse( $member_sn = 0 )
    {
        $this->db->select('*');
        $this->db->from('ij_media_warehouse');
        $this->db->where('member_sn', $member_sn);
        $this->db->where('status', 1);
        $this->db->order_by('media_warehouse', 'desc');
        $query = $this->db->get();
        return $result = $query->result_array();
    }
    /*
    * wedding_website 媒體 folder 列表
    * 全數拉出管理
    */
    public function get_wedding_website_media_warehouse( $wedding_website_sn = 0 )
    {
        $this->db->select('*');
        $this->db->from('ij_media_warehouse');
        $this->db->where('associated_wedding_website_sn', $wedding_website_sn);
        $this->db->where('status', 1);
        $this->db->order_by('media_warehouse', 'desc');
        $query = $this->db->get();
        return $result = $query->result_array();
    }
    /*
    * 媒體 folder 下媒體列表
    */
    public function get_member_media_warehouse_items( $member_sn = 0, $media_warehouse = 0 )
    {
        $this->db->select('*');
        $this->db->from('ij_media_detail');
        $this->db->where('create_member_sn', $member_sn);
        $this->db->where('media_warehouse', $media_warehouse);
        $this->db->where('status', 1);
        $this->db->order_by('media_detail_sn', 'desc');
        $query = $this->db->get();
        return $result = $query->result_array();
    }
}