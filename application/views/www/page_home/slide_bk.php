<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 首頁 slider
*/
?>
<!-- BEGIN SLIDER -->
<div class="page-slider margin-bottom-35">
  <!-- LayerSlider start -->
  <div id="layerslider" style="width: 100%; height: 494px; margin: 0 auto;">

    <!-- slide one start -->
    <div class="ls-slide ls-slide1" data-ls="offsetxin: right; slidedelay: 7000; transition2d: 24,25,27,28;">

      <img src="public/metronic/frontend/pages/img/layerslider/slide1/bg.jpg" class="ls-bg" alt="Slide background">

      <div class="ls-l ls-title" style="top: 96px; left: 35%; white-space: nowrap;" data-ls="
        fade: true; 
        fadeout: true; 
        durationin: 750; 
        durationout: 750; 
        easingin: easeOutQuint; 
        rotatein: 90; 
        rotateout: -90; 
        scalein: .5; 
        scaleout: .5; 
        showuntil: 4000;
      ">Happy Wedding<strong>幸福啟程</strong>Just Married</div>

      <div class="ls-l ls-mini-text" style="top: 338px; left: 35%; white-space: nowrap;" data-ls="
      fade: true; 
      fadeout: true; 
      durationout: 750; 
      easingin: easeOutQuint; 
      delayin: 300; 
      showuntil: 4000;
      ">訂製獨一無二<br >專屬的線上婚禮喜訊
      </div>
    </div>
    <!-- slide one end -->

    <!-- slide two start -->
    <div class="ls-slide ls-slide2" data-ls="offsetxin: right; slidedelay: 7000; transition2d: 110,111,112,113;">

      <img src="public/metronic/frontend/pages/img/layerslider/slide2/bg.jpg" class="ls-bg" alt="Slide background">
      
      <div class="ls-l ls-title" style="top: 40%; left: 21%; white-space: nowrap;" data-ls="
      fade: true; 
      fadeout: true;  
      durationin: 750; durationout: 109750; 
      easingin: easeOutQuint; 
      easingout: easeInOutQuint; 
      delayin: 0; 
      delayout: 0; 
      rotatein: 90; 
      rotateout: -90; 
      scalein: .5; 
      scaleout: .5; 
      showuntil: 4000;
      "><strong>凝聚永恆</strong> 多款線上主題任您挑選 <em>Time Capsule</em>
        </div>

      <div class="ls-l ls-price" style="top: 50%; left: 45%; white-space: nowrap;" data-ls="
      fade: true; 
      fadeout: true;  
      durationout: 109750; 
      easingin: easeOutQuint; 
      delayin: 300; 
      scalein: .8; 
      scaleout: .8; 
      showuntil: 4000;"><b>免費試用</b> <strong><span>$</span>0</strong>
      </div>

      <a href="preview_jerry/ewedding" class="ls-l ls-more" style="top: 72%; left: 21%; display: inline-block; white-space: nowrap;" data-ls="
      fade: true; 
      fadeout: true; 
      durationin: 750; 
      durationout: 750; 
      easingin: easeOutQuint; 
      easingout: easeInOutQuint; 
      delayin: 0; 
      delayout: 0; 
      rotatein: 90; 
      rotateout: -90; 
      scalein: .5; 
      scaleout: .5; 
      showuntil: 4000;">更多訊息
      </a>
    </div>
    <!-- slide two end -->

    <!-- slide three start -->
    <div class="ls-slide ls-slide3" data-ls="offsetxin: right; slidedelay: 7000; transition2d: 92,93,105;">

      <img src="public/metronic/frontend/pages/img/layerslider/slide3/bg.jpg" class="ls-bg" alt="Slide background">
      
      <div class="ls-l ls-title" style="top: 83px; left: 33%; white-space: nowrap;" data-ls="
      fade: true; 
      fadeout: true; 
      durationin: 750; 
      durationout: 750; 
      easingin: easeOutQuint; 
      rotatein: 90; 
      rotateout: -90; 
      scalein: .5; 
      scaleout: .5; 
      showuntil: 4000;
      ">婚禮流程一指搞定 <strong>Easy &amp; Funny</strong> 賓客接待與喜宴場控輕鬆完成
      </div>
      <div class="ls-l" style="top: 333px; left: 33%; white-space: nowrap; font-size: 20px; font: 20px 'Open Sans Light', sans-serif;" data-ls="
      fade: true; 
      fadeout: true; 
      durationout: 750; 
      easingin: easeOutQuint; 
      delayin: 300; 
      scalein: .8; 
      scaleout: .8; 
      showuntil: 4000;
      ">
        <a href="preview_jerry/checkIn" class="ls-buy">
          詳細說明
        </a>
        <div class="ls-price">
          <span>新人不再慌亂</span>
        </div>
      </div>
    </div>
    <!-- slide three end -->

  </div>
  <!-- LayerSlider end -->
</div>
<!-- END SLIDER -->