<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>行銷管理 <small>電子報管理</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        電子報管理
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">電子報列表</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/marketing/edmItem" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>電子報標題</th>
                  <th>最後修改日期</th>
                  <th>最後發送日期</th>
                  <th>已發送次數</th>
                  <th>開信統計</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
								<?php if($Items){ foreach($Items as $key=>$_Item){?>
                  <tr class="odd gradeX">
                    <td><?php echo $_Item['edm_titile'];?></td>
                    <td class="center">
                      <?php echo $_Item['last_time_update'];?>
                    </td>
                    <td class="center">
                      <?php echo ($_Item['last_time_send']!='0000-00-00 00:00:00')? $_Item['last_time_send']:'尚未發送'; ?>
                    </td>
                    <td class="center"><?php echo $_Item['send_total_amount'];?></td>
                    <td class="center"><?php echo $_Item['edm_open_amount'];?></td>
                    <td class="center">
                      <a href="admin/marketing/edmItem/<?php echo $_Item['edm_sn'];?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                      <a href="admin/marketing/edmSend/<?php echo $_Item['edm_sn'];?>"><i class="icon-envelope-letter"></i>發送</a> &nbsp;&nbsp;
<?php if(stripos($this->ijw->check_permission(1,$this->router->fetch_class(),$this->router->fetch_method()),'del')){?>
                      <a href="javascript:void(0);" class="deleteItem" ItemId="<?php echo $_Item['edm_sn'];?>"><i class="fa fa-trash-o"></i>刪除</a>
<?php }?>
                    </td>
                  </tr>
                <?php }}?>
              </tbody>
            </table>
					</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->