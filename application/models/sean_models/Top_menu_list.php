<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
     *
     * Database_tools_model: contains generic functions, used in several controllers
     *
     */
 
class Top_menu_list extends CI_Model 
{

    public function __construct() {
        parent::__construct();
      /* load 資料庫 */
      $this->load->database();
    }

    
		public function get_top_menu()
		{	
				
				$_one="";
				$action_one="SELECT * FROM product WHERE type = 6 and parent = 0 and dep =0 and active = 1  ORDER BY sort ASC";
				$query = $this->db->query($action_one);

				if ($query->num_fields() > 0)
				{
				   	$_one=$query->result();
				} 
				$query->free_result();
				return $_one;
		} 
	
}

/* End of file database_tools_model.php */
/* Location: ./application/models/database_tools_model.php */
