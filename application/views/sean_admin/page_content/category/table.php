<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>內容分類 <small>分類列表</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">內容分類列表</span>
            </div>
            <div class="actions btn-set">
              <a href="admin-ijwedding/Content/contentCategoryItem/add/0" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>名稱</th>
                  <th>頻道</th>
                  <th>部落格 / FAQ</th>
                  <th>排序</th>
                  <th>是否啟用</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
              	<?php
              		if(is_array($_result01))
                		{ 
                			$i=1;                                 
                 			foreach($_result01 as $key => $value)
                    		{                    			                    			                    			
                    			if($value->status == 1)
                    				{
                    					$_active = '<i class="fa fa-check"></i>';
                    				}
                    			else
                    				{
                    					$_active = '<i class="fa fa-times"></i>';
                    				}
                    			
                    			$_blog_faq_type =  $this->config->item("blog_faq_type");
                    			
                    		  print '
																	<tr class="odd gradeX">                    		  
																		<td>'.$value->content_title.'</td>
	                    							<td>'.$value->post_top_category_sn.'</td>
	                    							<td>'.$_blog_faq_type[$value->blog_faq_flag] .'</td>                                                                                        
	                    							<td>'.$value->sort_order.'</td>
	                    							<td class="center">'.$_active.'</td>      
								                    <td class="center">
								                      <a href="admin-ijwedding/Content/contentCategoryItem/edit/'.$value->content_typel_sn.'"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
								                      <a href="admin-ijwedding/Content/contentCategoryItem/delete/'.$value->content_typel_sn.'"><i class="fa fa-trash-o"></i>刪除</a>
								                    </td>
								                  </tr>  	                    							                                        						                        						                                          							                    		  				
                    		  			';
                    		  			
                    		  $i++;
                    		}
                  	}
              	?>                 	                                    
              </tbody>
            </table>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->