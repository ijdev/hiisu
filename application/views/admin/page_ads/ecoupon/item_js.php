<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script>
jQuery(document).ready(function() {       
   	Metronic.init(); // init metronic core components
	  Layout.init(); // init current layout
    $('.date-picker').datepicker({
        rtl: Metronic.isRTL(),
        format: "yyyy-mm-dd",
        orientation: "left",
        autoclose: true
    });
	/*$(".select2_category").select2({
	    tags: ["分類1", "分類2", "分類3", "分類4", "分類5"]
	});*/
});
		function checkForm(){
			console.log($('.checkboxs').is(':checked'));
			if($('#discount_type1').is(':checked')){
				if($('#discount_amount').val() <= 0){
					alert('折扣類型為金額抵用，金額必須填寫！');
					$('#discount_amount').focus();
					return false;
				}
			}
			if($('#discount_type2').is(':checked')){
				if($('#discount_percent').val() <= 0){
					alert('折扣類型為百分比抵用，折扣必須填寫！');
					$('#discount_percent').focus();
					return false;
				}else if($('#discount_percent').val() >= 1){
					alert('折扣類型為百分比抵用，折扣必須介於 0～1 之間！');
					$('#discount_percent').focus();
					return false;
				}
			}
			if(!$('.checkboxs').is(':checked')){
					alert('適用對象必須選擇');
					return false;
			}
					return true;
    };
</script>

