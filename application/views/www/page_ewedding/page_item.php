<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 商品內頁
*/
 							 $_2_name="";
 							 $_3_name="";
 							 $_1_name="";
 							 $_2_name_id="";
 							 $_3_name_id="";
 							 $_1_name_id="";
 							 
 							if(is_array($_category_array_left))
			        {
			        	foreach($_category_array_left as $key => $value)
			        	{
			        		if($value->category_sn == $_key_id && $value->category_status==1)
			        		{
			        			if($value->upper_category_sn){
						         $_3_name_id=$value->category_sn;
			        			 $_3_name=$value->category_name;
			        			 $_3_name_parent=$value->upper_category_sn;
						        	foreach($_category_array as $key => $value)
						        	{
						        		if($value->category_sn == $_3_name_parent && $value->category_status==1)
						        		{
						        			 $_2_name=$value->category_name;
						        			 $_2_name_id=$value->category_sn;
			        			       $_2_name_parent=$value->upper_category_sn;
			        			     }
						        	}
			        			 }else{
						        			 $_2_name=$value->category_name;
						        			 $_2_name_id=$value->category_sn;
			        			}
			        		}
			        	}
			        	if(@$_2_name_parent){
					        	foreach($_category_array as $key => $value)
					        	{
					        		if($value->category_sn == $_2_name_parent && $value->category_status==1)
					        		{
					        			 $_1_name=$value->category_name;
					        			 $_1_name_id=$value->category_sn;
					        		}
					        	}
					        }      	
        			}
?>
<div class="container">

<style>
  .pi-img-wrapper img{border:1px solid #EFEFEF;}
  .sale-product{background-color: #EFEFEF;padding: 12px;}
  .fa-usd-box{border-radius: 50%;border:2px solid #dfba49;display: inline-block;border-radius: 50% !important;    width: 20px;height: 20px;text-align: center;}
</style>


<!-- Page level plugin styles START -->
<link href="public/metronic/global/css/easyzoom.css" rel="stylesheet">
<script src="public/js/easyzoom.js"></script>
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<link href="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="public/metronic/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<link href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css"><!-- for slider-range -->
<link href="public/metronic/global/plugins/rateit/src/rateit.css" rel="stylesheet" type="text/css">
<!-- Page level plugin styles END -->

<ul class="breadcrumb">
    <li><a href="shop">婚禮網站</a></li>
    <?php if($_1_name){?><li><a href="shop/shopCatalog/<?php echo $_1_name_id;?>"><?php echo $_1_name;?></a></li><?php }?>
    <?php if($_2_name){?><li><a href="shop/shopCatalog/<?php echo $_2_name_id;?>"><?php echo $_2_name;?></a></li><?php }?>
    <?php if($_3_name){?><li><a href="shop/shopCatalog/<?php echo $_3_name_id;?>"><?php echo $_3_name;?></a></li><?php }?>
    <li class="active"><?php echo $product['product_name'];?></li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN CONTENT -->
  <div class="row margin-bottom-40 ">
    <!-- BEGIN CONTENT -->
    <div class="col-md-12 col-sm-12">
      <div class="product-page">
        <div class="row">
          <div class="col-md-6 col-sm-6">
           	
				<div class="product-main-image">
					<div style="float: right;  background: #EEE;"></div>
					  <div class="easyzoom easyzoom--adjacent easyzoom--with-thumbnails">
						   <a href="public/uploads/<?php echo $product['image_path'];?>">
							<img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $product['image_path'];?>" alt="<?php echo $product['image_alt'];?>" width="310" height="400" />
						   </a>
					  </div>
				</div>
			<div class="product-other-images">
					<div class="thumbnails">
						 <a class="thumb_cover" href="public/uploads/product/10/d3b7137ad68e4e50e290425fcc20042c.jpg" data-standard="public/uploads/product/10/d3b7137ad68e4e50e290425fcc20042c.jpg">
						  <img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/product/10/d3b7137ad68e4e50e290425fcc20042c.jpg" alt="客製喜帖_1">
						 </a>
					</div>
					<div class="thumbnails">
						 <a class="thumb_cover" href="public/uploads/product/10/9ff7b427236ab73f0fe63b130deb7504.jpg" data-standard="public/uploads/product/10/9ff7b427236ab73f0fe63b130deb7504.jpg">
						  <img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/product/10/9ff7b427236ab73f0fe63b130deb7504.jpg" alt="客製喜帖_1">
						 </a>
					</div>
					<div class="thumbnails">
						 <a class="thumb_cover" href="public/uploads/product/10/55214780b65c03db1855a9eb9dff528a.jpg" data-standard="public/uploads/product/10/55214780b65c03db1855a9eb9dff528a.jpg">
						  <img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/product/10/55214780b65c03db1855a9eb9dff528a.jpg" alt="客製喜帖_1">
						 </a>
					</div>
					<div class="thumbnails">
						 <a class="thumb_cover" href="public/uploads/product/10/61722bd5458c35a177a724570a045b29.jpg" data-standard="public/uploads/product/10/61722bd5458c35a177a724570a045b29.jpg">
						  <img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/product/10/61722bd5458c35a177a724570a045b29.jpg" alt="客製喜帖_1">
						 </a>
					</div>
			</div>
				 <!-- <div class="product-main-image">
							  <img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $product['image_path'];?>" alt="<?php echo $product['image_alt'];?>" class="img-responsive" data-BigImgsrc="public/uploads/<?php echo $product['image_path'];?>" id="box_thumb_cover">
							</div>-->
							<!--<div class="product-other-images">
				<?php if(@$images=$product['images']){ foreach($images as $key=>$Item){ if($key >-1){?>
							  <a href="public/uploads/<?php echo $Item['image_path'];?>" class="thumb_cover" rel="photos-lib"><img onerror="this.src='public/uploads/notyet.gif'" alt="<?php echo $Item['image_alt'];?>" src="public/uploads/<?php echo $Item['image_path'];?>"></a>
				<?php }}}?>
							</div>
							-->
			
			<div class="col-md-12 col-sm-12 social_box">
											<a class="btn btn-social-icon btn-facebook">
												<span class="fa fa-facebook"></span>
											</a>
											<a class="btn btn-social-icon google-plus">
												<span class="fa fa-google-plus"></span>
											</a>
											<a class="btn btn-social-icon line_icon">
												<span class="fa_line"><img src="public/img/line-icon.png"></span>
											</a>
			</div>
        </div>
		<div class="col-md-6 col-sm-6">
            <h1><?php echo $product['product_name'];?></h1>
            <div class="price-availability-block clearfix">
              <div id="showprice" class="price">
                <strong><span>$</span><?php echo number_format(@$product['price1']);?></strong><?php echo @$product['price2'];?>
                <?php if(@$product['fixed_price']){?><em>$<span><?php echo number_format(@$product['fixed_price']);?></span></em><?php }?>
              </div>
              <!--<div class="availability">
                編號: <strong><?php echo $product['product_sn'];?></strong><br>
                <a class="addwish" product_sn="<?php echo $product['product_sn'];?>" title="加入追蹤" href="javascript:void(0);">加入追蹤 <i class="fa fa-heart"></i></a>
              </div>-->
			  
			   <div class="availability ewedngpg">
									編號: <strong><?php echo $product['product_sn'];?></strong><br>
							  
									<a class="addwish" product_sn="<?php echo $product['product_sn'];?>" title="加入追蹤" href="javascript:void(0);">
										<p class="addwish_txt">加入追蹤</p>
										<i class="fa fa-heart prd_icon"></i>
									</a>
									
								  </div>
            </div>
<div class="note note-success note-bordered heart<?php echo $product['product_sn'];?>" style="display:none;"></div>
      <?php $this->load->view('www/general/flash_error');?>
            <form id="shop" method="POST" action="/shop/shopView">
            <div class="description">
              <p style="word-wrap: break-word;"><?php echo $product['special_item'];?></p>
              <p>
                <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#d1d1d1">
                  <tbody>
<?php if($product['pricing_method']=='2'){ ?>
                    <tr>
                      <td height="23" align="center" valign="middle" style="border-bottom:1px dotted #d1d1d1;" bgcolor="#eeeeee">&nbsp;購買數量</td>
                      <td align="center" valign="middle" style="border-bottom:1px dotted #d1d1d1;" bgcolor="#eeeeee">優惠價格(單價)</td>
                    </tr>
<?php if(@$price_tier=$product['price_tier']){ foreach($price_tier as $Item){?>
                    <tr>
                      <td height="23" align="center" bgcolor="#ffffff" valign="middle" style="border-bottom:1px dotted #d1d1d1;">&nbsp;<?php echo $Item['start_qty'];?>~<?php echo $Item['end_qty'];?> 張</td>
                      <td align="center" bgcolor="#ffffff" valign="middle" style="border-bottom:1px dotted #d1d1d1;"><div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">NT$ <span itemprop="price"><?php echo $Item['price'];?></span><link itemprop="itemCondition" href="http://schema.org/NewCondition"></div></td>
                    </tr>
<?php }}
}elseif($product['pricing_method']=='1'){?>
                   <!--tr>
                      <td height="23" align="center" valign="middle" style="border-bottom:1px dotted #d1d1d1;" bgcolor="#eeeeee">&nbsp;規格</td>
                      <td align="center" valign="middle" style="border-bottom:1px dotted #d1d1d1;" bgcolor="#eeeeee">定價</td>
                      <td align="center" valign="middle" style="border-bottom:1px dotted #d1d1d1;" bgcolor="#eeeeee">售價</td>
                    </tr-->
<?php }?>

                  </tbody>
                </table>
              </p>
            </div>
            <div class="row" style="margin:20px 0px 32px 0px; ">
              <div class="padding_left0 col-md-5"><?php //var_dump($product['mutispec']);?>
                <select class="form-control" name="product_spec" id="product_spec">
                  <option value="">請選擇規格</option>
                  <?php if($product['unispec_flag']=='1'){
                  	$unispec_qty_in_stock=intval($product['unispec_qty_in_stock']);
                  	$qty_in_stock=$unispec_qty_in_stock;
                  	?>
                  	<option selected>單一規格</option>
                  <?php }else{ ?>
<?php if(@$mutispec=$product['Packages']){ foreach($mutispec as $Item){ if($Item['price'] > 0){
                  	  if($product['product_type']=='2') $qty_in_stock=intval($product['unispec_qty_in_stock']);
	                  	//$qty_in_stock = 1;
?>
	                  <option qty_in_stock="<?php echo $qty_in_stock;?>" price="<?php echo $Item['price'];?>" <?php echo (count($mutispec)==1)?'selected':'';?> value="<?php echo $Item['product_package_config_sn'];?>"><?php echo $Item['product_package_name'];?></option>
                  <?php }}} }?>
                </select>
              </div>
              <div class="padding_left0 col-md-12 row" style="margin:20px 0px 32px 0px; ">
                    <?php if(@$qty_in_stock){?>
                <button id="shopadd" class="btn btn-primary btn-lg col-md-5" type="submit">立即結帳</button>
                <button id="shopaddcart" class="btn btn-warning btn-lg col-md-5" type="submit">放入購物車</button>
                    <?php }?>
              </div>
            </div>
            <div class="review">
              <div><label class="control-label">加購商品</label></div>
<?php if(@$addons=$product['addons']){ foreach($addons as $key=>$Item){?>
              <div class="row">
                <div class="col-xs-1">
                  <input class="addon" addon_log_sn="<?php echo $Item['addon_log_sn'];?>" id="addon<?php echo $Item['addon_log_sn'];?>" type="checkbox" name="addon[<?php echo $key;?>][addon_log_sn]" value=<?php echo $Item['addon_log_sn'];?>>
                </div>
                <div class="col-xs-8">
                  <span class="btn btn-warning btn-xs">加購</span>
                  <?php echo $Item['product_name'];?> ($<?php echo number_format($Item['addon_price']);?>)
                </div>
                <div class="col-xs-3">
                  <select id="addon_amount<?php echo $Item['addon_log_sn'];?>" addon_log_sn="<?php echo $Item['addon_log_sn'];?>" addon_limitation="<?php echo $Item['addon_limitation'];?>" unispec_qty_in_stock="<?php echo $Item['unispec_qty_in_stock'];?>" name="addon[<?php echo $key;?>][addon_amount]" class="addon_amount">
                      <option value="">0</option>
                  </select>
                </div>
              </div>
<input type="hidden" name="addon[<?php echo $key;?>][associated_product_sn]" value="<?php echo $Item['associated_product_sn']; ?>">
<input type="hidden" name="addon[<?php echo $key;?>][addon_price]" value="<?php echo $Item['addon_price']; ?>">
<input type="hidden" name="addon[<?php echo $key;?>][product_name]" value="<?php echo $Item['product_name']; ?>">
<input type="hidden" name="addon[<?php echo $key;?>][addon_limitation]" value="<?php echo $Item['addon_limitation']; ?>">
<?php }}?>
<input type="hidden" name="qty" id="qty" value="1">
            </div>
            <div class="review">
              <strong>付款方式 : <?php echo $product['payment_method']['row_list'];?>、<span class="fa-usd-box"><i class="fa fa-usd text-warning"></i></span> 結婚購物金</strong>
            </div>
            <div class="review">
              <strong>配送方式 : <?php echo $product['delivery_method']['row_list'];?></strong>
            </div>
          </div>
<input type="hidden" id="product_sn" name="product_sn" value="<?php echo @$product['product_sn']; ?>">
        </form>
		
	
	<!--product-page-content php-->
        <div class="product-page-content">
            <ul id="myTab" class="nav nav-tabs">
              <li class="active"><a href="#Information" data-toggle="tab">商品介紹</a></li>
              <li><a href="#Reviews" data-toggle="tab">商品規格</a></li>
			  <li class=""><a href="#Precautions" data-toggle="tab" aria-expanded="false">注意事項</a></li>
			  <li ><a href="#QA" data-toggle="tab" aria-expanded="true">Q&amp;A</a></li>
			</ul>
		
            <div id="myTabContent" class="tab-content">
              <div class="tab-pane fade in active " id="Information">
                <!--table class="datasheet margin-bottom-40" style="border:1px dotted #EFEFEF;">
 <?php if(@$attributes=$product['attributes']){ foreach($attributes as $key=>$Item){?>
                  <tr>
                    <td class="datasheet-features-type"><?php echo $Item['attribute_name'];?></td>
                    <td>
 <?php if(@$attribute_values=$Item['attribute_values']){ foreach($attribute_values as $key=>$av){?>
                      <label><i class="fa <?php echo ($av['status'])?'fa-check':'fa-times';?>"></i><a><?php echo $av['attribute_value_name'];?></a></label>&nbsp;&nbsp;
 <?php }}?>
                    </td>
                  </tr>
 <?php }}?>
                </table-->

             
                <!--table class="datasheet margin-bottom-40" style="border:1px dotted #EFEFEF;">
                  <tr>
                    <th width="120"></th>
                    <th>基本款</th>
                    <th>進階款</th>
                    <th>豪華款</th>
                  </tr>
                  <tr>
                    <td class="datasheet-features-type">彩色列印</td>
                    <td>-</td>
                    <td>Y</td>
                    <td>Y</td>
                  </tr>
                  <tr>
                    <td class="datasheet-features-type">燙金</td>
                    <td>-</td>
                    <td>Y</td>
                    <td>Y</td>
                  </tr>
                  <tr>
                    <td class="datasheet-features-type">張數</td>
                    <td>100</td>
                    <td>200</td>
                    <td>300</td>
                  </tr>
                </table-->
               
              </div>
              <div class="tab-pane fade" id="Reviews">
								   <table class="datasheet margin-bottom-40" style="border:1px dotted #EFEFEF;">
                  <tr>
                    <th width="120"></th>
<?php if(@$Packages=$product['Packages']){ 
	foreach($Packages as $Item){ 
		if($Item['price'] > 0){
		echo '<th>'.$Item['product_package_name'].'</th>';
		//var_dump($Item['specs']);
 }}}
?>
                  </tr>
<?php if(@$specs=$product['Packages']){ //var_dump(count($Item['specs']));
  	for($i=0; $i<count($Item['specs']); $i++):?>
                  <tr>
                    <td class="datasheet-features-type"><?php echo $Item['specs'][$i]['spec_option_name'];?></td>
			<?php foreach($Packages as $Item){ if($Item['price'] > 0){
					$spec_option_limitation=(@$Item['specs'][$i]['spec_option_limitation']==-1)? '無限制':@$Item['specs'][$i]['spec_option_limitation'];
					$uselimit=(@$Item['specs'][$i]['usage_limitation']==-1)? '無限制':@$Item['specs'][$i]['usage_limitation'];
                    echo '<td class="datasheet-features-type">'.$spec_option_limitation.'&nbsp;</td>';
}} echo '</tr>'; endfor; }?>
                </table>
				 <p class=" margin-bottom-25"><?php echo $product['product_info'];?></p>
              </div>
			  
			   <div class="tab-pane fade" id="Precautions">
								<pre><?php echo $_category_array_left[0]->return_desc;?></pre>
              </div>
			    <div class="tab-pane fade " id="QA">
						
                   <?php if(!empty($_web_member_data["user_name"])){?>
					<div class="qamain pull-left">
							<h4 class="qatitle pull-left">問與答：</h4>
							<p class="col-md-12 pull-left qatext">如果您對本項商品有任何問題，歡迎您留下寶貴意見，客服人員會盡速回覆</p>
							
							<span class="col-md-12 pull-left qatext2">
							商品名稱：中式雷雕႐帖  - KC017V 紫梅囍鵲</span>
							<span class="col-md-12 pull-left qatext2">問題內容：</span>
							
							<textarea  rows="5" placeholder="請輸入您的問題內容(限200字)您的提問內容會在此網頁顯示，為維護您個人資料安全，建議留言時請勿填寫姓名、地址、市話/手機號碼、銀行號碼...等資料，避免遭有心人士利用。" class="qatextarea col-md-12 pull-left" ></textarea>
							<div class="qabuttons">
							<a class="btn btn-default prd_btn_lft col-md-2 pull-left" href="">確定送出</a>
							
							</div>
							</div>
				   <?php }else {?>
				   
				   	<div class="qamain pull-left">
							<h4 class="qatitle pull-left">問與答：</h4>
							<p class="col-md-12 pull-left qatext">如果您對本項商品有任何問題，歡迎您留下寶貴意見，客服人員會盡速回覆</p>
							<span class="col-md-12 pull-left qatext2">
							商品名稱:中式雷雕喜帖 - KC017V 紫梅囍鵲</span>
							<span class="col-md-12 pull-left qatext2">您的問題：</span>
							
							<div  class="qatextarea qatextarea2 col-md-12 pull-left" >
							<div class="qatwobtns col-md-6">
							<span class="qatextsm pull-left col-md-6">請您先登入再提問 </span>
							<input class="qabuttred col-md-6" type="button" value="立即登入">
							</div>
							</div>
							<div class="qabuttons">
							<a class="btn btn-default prd_btn_lft col-md-2 pull-left" href="">確定送出</a>
							<!--<a class="col-md-2 pull-left qabtnse" href="#">(立即結帳)</a>-->
							</div>
							</div>
				   
				   
				   
				   <?php }?>
							
								
								<!--<pre><?php echo $_category_array_left[0]->return_desc;?></pre>-->
              </div>
            </div>
          </div>
<!--product-page-content end php-->
        </div>
      </div>
    </div>
    <!-- END CONTENT -->
  </div>
  <!-- END SALE PRODUCT & NEW ARRIVALS -->


  <?php for($i=0; $i<0; $i++):?>
    <!-- BEGIN fast view of a product -->
    <div id="product-pop-up-<?php echo $i;?>" style="display: none; width: 700px;">
      <div class="product-page product-pop-up">
        <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-3">
				<div class="product-main-image">
						<img src="public/img/cover_sample.png" alt="Cool green dress with red bell" class="img-responsive">
				</div>
				
				<div class="product-other-images">
						  <a href="#" class="active"><img alt="Berry Lace Dress" src="public/img/cover_sample.png"></a>
						  <a href="#"><img alt="Berry Lace Dress" src="public/img/cover_sample.png"></a>
						  <a href="#"><img alt="Berry Lace Dress" src="public/img/cover_sample.png"></a>
				</div>
				
				
          </div>
          <div class="col-md-6 col-sm-6 col-xs-9">
            <h2>Cool green dress with red bell</h2>
            <div class="price-availability-block clearfix">
              <div class="price">
                <strong><span>$</span>47.00</strong>
                <em>$<span>62.00</span></em>
              </div>
              <div class="availability">
                編號: <strong>1000001</strong><br>
              </div>
            </div>
            <div class="description">
              <p>Lorem ipsum dolor ut sit ame dolore  adipiscing elit, sed nonumy nibh sed euismod laoreet dolore magna aliquarm erat volutpat Nostrud duis molestie at dolore.</p>
            </div>
            <div class="product-page-options">
              <div class="pull-left">
                <label class="control-label">Size:</label>
                <select class="form-control input-sm">
                  <option>L</option>
                  <option>M</option>
                  <option>XL</option>
                </select>
              </div>
              <div class="pull-left">
                <label class="control-label">Color:</label>
                <select class="form-control input-sm">
                  <option>Red</option>
                  <option>Blue</option>
                  <option>Black</option>
                </select>
              </div>
            </div>
            <div class="product-page-cart">
              <div class="product-quantity">
                  <input id="product-quantity" type="text" value="1" readonly name="product-quantity" class="form-control input-sm">
              </div>
              <button class="btn btn-primary" type="submit">Add to cart</button>
              <a href="preview_jerry/shopItem" class="btn btn-default">More details</a>
            </div>
          </div>

          <div class="sticker sticker-sale"></div>
        </div>
      </div>
    </div>
    <!-- END fast view of a product -->
  <?php endfor;?>
  <!-- END SALE PRODUCT & NEW ARRIVALS -->
  <!-- END CONTENT -->
</div>
<!-- END SIDEBAR & CONTENT -->
<!-- BEGIN SIMILAR PRODUCTS -->



<div class="row margin-bottom-40">
  <div class="col-md-12 col-sm-12">
    <h2>其它版型</h2>

	  <div id="page_itemprodct" class="owl-carousel owl-carousel4 slider_itm">
<?php if(@$promo_product6){ foreach($promo_product6 as $Item){ //var_dump($Item['promotion'])?>
          <!-- box product-item 1推薦 2熱賣 3特價 4獨家-->
     <div>
		  <div class="slider_data"> 
            <div class="product-item">
              <div class="pi-img-wrapper">
                <a class="img_wrp" href="home/eweddingItem/<?php echo $Item['product_sn'];?>"><img onerror="this.src='public/uploads/notyet.gif'" style="height:250px;width:250px;" src="public/uploads/<?php echo $Item['image_path'];?>" class=" img-responsive sl_pics" alt="<?php echo @$Item['image_alt'];?>"></a>
				<a class="product_icon" title="加入追蹤" href="javascript:void(0);"><i product_sn="9" class="fa fa-heart"></i></a>
                <!--div>
                  <a href="public/img/cover_sample.png" class="btn btn-default fancybox-button">放大</a>
                </div-->
           </div>
		  
             
			 <h3><a style="color: #c70003;" href="home/eweddingItem/<?php echo $Item['product_sn'];?>"><?php echo $Item['product_name'];?></a></h3>
			  <div class="pi-textsp"><a href="shop/shopItem/13/42">進口喜帖 - CW519WH 珍珠白蕾絲婚卡</a>		</div>
			  
              <div class="pi-price">$<?php echo $Item['price1'];?>&nbsp;<font style="font-size:12px;color:#333333;"><?php echo $Item['price2'];?></font></div>
			 <div class="prd_btn_box">
					<a class="btn btn-default prd_btn_lft" href="">立即結帳</a>
						<a href="#" product_package_config_sn="<?php echo @$Item['product_package_config_sn'];?>" product_sn="<?php echo $Item['product_sn'];?>" class=" btn btn-default prd_btn_rgt add2cart">放入購物車</a>
				 </div>
              <?php echo ($promotion_label_eng_name=@$Item['promotion'][0]['promotion_label_eng_name'])? '<div class="sticker sticker-'.$promotion_label_eng_name.'"></div>':'';?>
            </div>
			
		</div>
       </div>
	   
	   <!-- demo product start-->
	   <div class="owl-item active" style="width: 278px;"><div class="slider_data"> 
            <div class="product-item">
              <div class="pi-img-wrapper">
                <a class="img_wrp" href="shop/shopItem/13">
				<img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/product/13/1a079d367b0c77763dc9a7383d8d9d1b.jpg" class="img-responsive sl_pics" alt="中式雷雕喜帖 - KC017V 紫梅囍鵲"></a>
				
                <!--div>
                  <a href="public/img/cover_sample.png" class="btn btn-default fancybox-button">放大</a>
                </div-->
				  <a class="product_icon" title="加入追蹤" href="javascript:void(0);"><i product_sn="9" class="fa fa-heart"></i></a>
              </div>
			
              <h3><a style="color: #c70003;" href="shop/shopItem/13">中式雷雕喜帖 - KC017V 紫梅囍鵲</a></h3>
              <div class="pi-textsp"><a href="shop/shopItem/13/42">進口喜帖 - CW519WH 珍珠白蕾絲婚卡</a>		</div>				 
			  <div class="pi-price"><span>$</span>55&nbsp;<font style="font-size:12px;color:#b52b28;">起</font></div>
						 <div class="prd_btn_box">
							<a class="btn btn-default prd_btn_lft" href="">立即結帳</a>
							<a class="btn btn-default prd_btn_rgt add2cart" product_sn="98">放入購物車</a>
													
						 </div>
		   </div>
          </div></div>
		  
		  <div class="owl-item active" style="width: 278px;"><div class="slider_data"> 
            <div class="product-item">
              <div class="pi-img-wrapper">
                <a class="img_wrp" href="shop/shopItem/11">
				<img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/product/11/1bbc7514ad231312ff78557865b97d02.jpg" class="img-responsive sl_pics" alt="喜帖_3"></a>
				
                <!--div>
                  <a href="public/img/cover_sample.png" class="btn btn-default fancybox-button">放大</a>
                </div-->
				  <a class="product_icon" title="加入追蹤" href="javascript:void(0);"><i product_sn="9" class="fa fa-heart"></i></a>
              </div>
			
              <h3><a style="color: #c70003;" href="shop/shopItem/11">喜帖_3</a></h3>
              <div class="pi-textsp"><a href="shop/shopItem/11/42">進口喜帖 - CW519WH 珍珠白蕾絲婚卡</a>		</div>				 
			  <div class="pi-price"><span>$</span>69&nbsp;<font style="font-size:12px;color:#b52b28;">起</font></div>
						 <div class="prd_btn_box">
							<a class="btn btn-default prd_btn_lft" href="">立即結帳</a>
							<a class="btn btn-default prd_btn_rgt add2cart" product_sn="98">放入購物車</a>
													
						 </div>
		   </div>
          </div></div>
		  
		  <div class="owl-item active" style="width: 278px;"><div class="slider_data"> 
            <div class="product-item">
              <div class="pi-img-wrapper">
                <a class="img_wrp" href="shop/shopItem/16">
				<img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/product/16/604ad9e1c0c1c946e3e8451d25f17173.jpg" class="img-responsive sl_pics" alt="【信封燙金版費】喜帖信封燙金"></a>
				
                <!--div>
                  <a href="public/img/cover_sample.png" class="btn btn-default fancybox-button">放大</a>
                </div-->
				  <a class="product_icon" title="加入追蹤" href="javascript:void(0);"><i product_sn="9" class="fa fa-heart"></i></a>
              </div>
			
              <h3><a style="color: #c70003;" href="shop/shopItem/16">【信封燙金版費】喜帖信封燙金</a></h3>
              <div class="pi-textsp"><a href="shop/shopItem/16/42">進口喜帖 - CW519WH 珍珠白蕾絲婚卡</a>		</div>				 
			  <div class="pi-price"><span>$</span>300&nbsp;<font style="font-size:12px;color:#b52b28;"></font></div>
						 <div class="prd_btn_box">
							<a class="btn btn-default prd_btn_lft" href="">立即結帳</a>
							<a class="btn btn-default prd_btn_rgt add2cart" product_sn="98">放入購物車</a>
													
						 </div>
		   </div>
          </div></div>
		    <!-- demo product End-->
		  
          <!-- End of box product-item -->
<?php }}?>    	

</div>
</div>
</div>

<script>
  // Instantiate EasyZoom instances
  var $easyzoom = $('.easyzoom').easyZoom();

  // Setup thumbnails example
  var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');

  $('.thumbnails').on('click', 'a', function(e) {
   var $this = $(this);

   e.preventDefault();

   // Use EasyZoom's `swap` method
   api1.swap($this.data('standard'), $this.attr('href'));
  });

  // Setup toggles example
  var api2 = $easyzoom.filter('.easyzoom--with-toggle').data('easyZoom');

  $('.toggle').on('click', function() {
   var $this = $(this);

   if ($this.data("active") === true) {
    $this.text("Switch on").data("active", false);
    api2.teardown();
   } else {
    $this.text("Switch off").data("active", true);
    api2._init();
   }
  });
 </script>

<!-- END SIMILAR PRODUCTS -->