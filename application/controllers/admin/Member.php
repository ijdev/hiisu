<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Member extends MY_Controller {
	 	public function __construct()
		{
			parent::__construct();
			$this->load->library('session');
			$this->load->helper('form');
			$this->load->helper('url');
			$this->load->library('form_validation');
			$this->load->library('sean_lib/Sean_form_general');
			$this->load->model('general/My_general_ct');
			$this->load->config('ij_config');
			$this->load->config('ij_sys_config');
		    $this->load->model('sean_models/Sean_general');
			$this->load->model('sean_models/Sean_db_tools');
			$this->load->model('libraries_model');
			$this->page_data = array();
			$this->page_data['unreadmessage']= $this->libraries_model->get_unread_message_count();
			$this->page_data['init_control']="admin/";
			$this->page_data['view_control']="admin/";
			$this->init_control=$this->page_data['init_control'];
			$this->page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
			$this->page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
			$this->page_data['page_content']['view_path'] = 'admin/page_home/page_content'; // 頁面主內容 view 位置, 必要
			$this->page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$this->page_data['page_level_js']['view_path'] = 'admin/page_home/page_content_js'; // 頁面主內容的 JS view 位置, 必要
			$this->lang->load('general', 'zh-TW');
			$this->page_data["_per_page"]=10;
			$this->LoginCheck();
		}

 /*
	*
  * 檢查是否有登入
	*/

	//$this->LoginCheck();
	public function LoginCheck()
	{

			if($this->session->userdata('member_login')== true)
			return true;
			else
			redirect(base_url("/admin/login"));
			exit();
	}

 /**
	 * 經銷商列表 Template
	 *
	 */
	public function dealerTable($_action="",$_id="")
	{
		/* Begin 修改 */
		//載入 model
		$_model_name="Dealer_table";
		$this->page_data['_id']=$_id;// 功能名稱
		$this->page_data['_father_name']="會員管理";// 功能名稱
		$this->page_data['_ct_name']="聯盟會員列表";// 功能名稱
		$this->page_data['_ct_name_item']="聯盟會員列表編輯";// 功能子名稱
		$this->page_data['_url']="Member/dealerTable/";//列表功能url
		$this->page_data['_table']="ij_dealer";//table name
		$this->page_data['_table_key']="dealer_sn";//table key
		$this->page_data['_member_data']=$this->session->userdata('member_data');
		$this->page_data['_table_field']=array(
					"dealer_sn",
					"dealer_name",
					"dealer_status",
				);
		//var_dump($_action);exit;

		$this->page_data['page_content']['view_path'] = 'admin/page_dealer/list/table';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'admin/page_dealer/list/table_js';

		//產業屬性
		$_where=" where status='1' ";
		$_biz_domain_list=$this->Sean_db_tools->db_get_max_record("*","ij_biz_domain",$_where);
		$this->page_data['_biz_domain_list']=$_biz_domain_list;

		//列出全部的dealer level
		$_where=" where status='1' order by dealer_level_sn";
		$_dealer_level_list=$this->Sean_db_tools->db_get_max_record("*","ij_dealer_level_config",$_where);
		$this->page_data['_dealer_level_list']=$_dealer_level_list;

		//客服代表
		$_where=" where member_status='2' and system_user_flag=1 ";
		$_associated_sales_membe_list=$this->Sean_db_tools->db_get_max_record("*","ij_member",$_where);
		$this->page_data['_associated_sales_membe_list']=$_associated_sales_membe_list;


		$_table_where=" ";//table list where
		$_page_content="page_dealer/list/table";
		$_page_level_js="page_dealer/list/table_js";
		$_page_content_item="page_dealer/list/item";
		$_page_level_js_item="page_dealer/list/item_js";

		/* END 修改 */
		$this->page_data['_father_link']=$this->page_data['init_control'].$this->page_data['_url'];
		$this->load->model('member/'.$_model_name);


		switch($_action){
			case "add" :
			case "item" :
			case "edit" :
			case "adding" :
			case "update" :

				// 內容設定
				 $this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."adding";

				//產業屬性
				$_where=" where status='1' ";
				$_biz_domain_list=$this->Sean_db_tools->db_get_max_record("*","ij_biz_domain",$_where);
				$this->page_data['_biz_domain_list']=$_biz_domain_list;




				 //編輯
		    if($_action=="edit")
				{
					$this->Check_page_permission('search','查詢');
					//新增管理員
					if($this->input->post('associated_member_sn') && $_id){
						$associated_member_sn=$this->input->post('associated_member_sn');
	    			$_data["associated_dealer_sn"]=$_id;
	    			$_where="where member_sn=$associated_member_sn";
		 				$update_result=$this->Sean_db_tools->db_update_record($_data,'ij_member',$_where,'member_sn',$associated_member_sn);
						if($update_result)
							$this->session->set_flashdata("message","新增完成！");
						else
							$this->session->set_flashdata("fail_message","新增失敗！");
						redirect($this->page_data['_father_link'].'edit/'.$_id.'#tab_manager');
						//var_dump($update_result);
						exit();
					}
					//刪除管理員
					if($this->input->post('member_sn') && $_id){
						$del_member_sn=$this->input->post('member_sn');
						//var_dump($del_member_sn);
	    			$_data["associated_dealer_sn"]='null';
	    			$_where="where member_sn=$del_member_sn";
		 				$update_result=$this->Sean_db_tools->db_update_record($_data,'ij_member',$_where,'member_sn',$del_member_sn);
						if($update_result)
							echo true;
							//$this->session->set_flashdata("message","刪除完成！");
						else
							echo false;
							//$this->session->set_flashdata("fail_message","刪除失敗！");
						//redirect($this->page_data['_father_link'].'edit/'.$_id.'#tab_manager');
						//var_dump($update_result);
						exit();
					}
				  //關聯會員s
					$_where=" left join ij_member_dealer_relation on ij_member.member_sn=ij_member_dealer_relation.member_sn where dealer_sn='".$_id."' order by last_time_update desc";
					$_result02=$this->Sean_db_tools->db_get_max_record("ij_member.*,ij_city_ct.city_name,ij_town_ct.town_name","ij_member left join ij_city_ct on ij_city_ct.city_code=ij_member.addr_city_code left join ij_town_ct on ij_town_ct.town_code=ij_member.addr_town_code",$_where);
					$this->page_data['_result02']=$_result02;

			    //經銷商管理員s
			    $_where=" where (associated_dealer_sn=".$_id." and member_status=2 and (member_level_type=4 or member_level_type=6)) order by last_time_update desc"; //有效經銷會員
					$_result03=$this->Sean_db_tools->db_get_max_record("member_sn,last_name,first_name,email","ij_member",$_where);
					$this->page_data['_result03']=$_result03;

			    //經銷會員s
			    $_where=" where (associated_dealer_sn is null or associated_dealer_sn=0) and member_status=2 and (member_level_type=4 or member_level_type=6) order by last_name"; //有效經銷會員 associated_dealer_sn=0
					$_result04=$this->Sean_db_tools->db_get_max_record("member_sn,last_name,first_name,email","ij_member",$_where);
					$this->page_data['_result04']=$_result04;
				//echo $this->db->last_query().'<br>';
					//var_dump($_result04);

					$_where=" where status='1' and associated_dealer_sn=$_id ";
					$_bank_info=$this->Sean_db_tools->db_get_max_record("*","ij_bank_info",$_where);
					$this->page_data['_bank_info']=$_bank_info;

					$_where=" where associated_dealer_sn=$_id ";
					$_contact_info=$this->Sean_db_tools->db_get_max_record("*","ij_contact_info",$_where);
					$this->page_data['_contact_info']=$_contact_info;


					//產業屬性 內容

					//$_where=" where associated_member_sn='".$_id."' and status =1 "; ?associated_member_sn
					$_where=" where associated_dealer_sn='".$_id."' and status =1 ";
					$_member_domain_relation=$this->Sean_db_tools->db_get_max_record("*","ij_member_domain_relation",$_where);
					$this->page_data['_member_domain_relation']=$_member_domain_relation;



					$this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."update/".$_id."";

					$_result="";
					$_where=" where ".$this->page_data['_table_key']."='".$_id."'";
					//$_result=$this->Sean_db_tools->db_get_max_record("*",$this->page_data['_table'],$_where);
					$_result=$this->Sean_db_tools->db_get_max_record("ij_dealer.*,email,user_name","ij_dealer left join ij_member on ij_dealer.dealer_sn=ij_member.associated_dealer_sn",$_where);
					$this->page_data['_aCtion_link']=$this->page_data['init_control'].$this->page_data['_url']."update/$_id";
					$this->page_data['_result01']=$_result;
				}

				//更新
				if($_action=="update")
				{
					$this->Check_page_permission('edit','修改');

					$_where=" where dealer_sn='".$_id."'";
					$_result=$this->$_model_name->action_update($_where,$_id,"dealer_sn");
					//var_dump($_result);
					//exit();
					if($_result)
						$this->session->set_flashdata("success_message","更新完成！");
					else
						$this->session->set_flashdata("fail_message","更新失敗！");


					redirect($this->page_data['_father_link']);
					exit();
				}



				//新增
				if($_action == "adding")
				{
					$this->Check_page_permission('add','新增');
					$_result=$this->$_model_name->action_insert($this->page_data);
					if($_result)
						$this->session->set_flashdata("success_message","新增完成！");
					else
						$this->session->set_flashdata("fail_message","新增失敗！");



				}
				//新增
				if($_action == "add"){
					$this->Check_page_permission('add','新增');
				}


				$this->page_data['page_level_js']['status'] = array(
														array('label' => '未發送', 'num' => 200),
														array('label' => '已發送', 'num' => 100),
														array('label' => '已審核', 'num' => 300),
														array('label' => '已使用', 'num' => 300),
														array('label' => '失效', 'num' => 100)
														);
				$this->page_data['page_level_js']['enable'] = array(
														array('label' => '尚未使用', 'num' => 30000),
														array('label' => '已使用', 'num' => 70000)
														);


				// 頁面主內容 view 位置, 必要
				$this->page_data['page_content']['view_path'] = $this->page_data['view_control'].$_page_content_item;
				// 頁面主內容的 JS view 位置, 必要
				$this->page_data['page_level_js']['view_path'] = $this->page_data['view_control'].$_page_level_js_item;

				$this->page_data['_city_ct'] = $this->libraries_model->_select('ij_city_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','city_code,city_name'); // city
				//var_dump($this->page_data['_city_ct']);

				$this->load->view($this->page_data['view_control'].'general/main', $this->page_data);

			break;

			default:
				//刪除
				if($_action=="delete")
				{
					$this->Check_page_permission('del','停用');

					$_where=" where ".$this->page_data['_table_key']."='".$_id."'";
					$_result=$this->$_model_name->action_delete($_where,$this->page_data);


					if($_result)
						$this->session->set_flashdata("success_message","停用完成！");
					else
						$this->session->set_flashdata("fail_message","停用失敗！");
				}
				if($_action=="search")
				{


							$_table_where="";
							if($this->input->get_post("status") || strlen($this->input->get_post("status")) > 0)
							{
								if(strlen($_table_where) < 2 )
								$_table_where .=" where";
								else
								$_table_where .= " and ";

								$_data["status"]=$this->input->get_post("status");
								$_table_where .="  dealer_status = '".$_data["status"]."'";
								$this->session->set_userdata('search_location_status',$_data["status"]);
							}

							if($this->input->get_post("wedding_location_name") || strlen($this->input->get_post("wedding_location_name")) > 0)
							{
								if(strlen($_table_where) < 2 )
								$_table_where .=" where";
								else
								$_table_where .= " and ";

								$_data["wedding_location_name"]=$this->input->get_post("wedding_location_name");
								$_table_where .="  wedding_location_name  like '%".$_data["wedding_location_name"]."%'";
								$this->session->set_userdata('search_location_wedding_location_name',$_data["wedding_location_name"]);
							}

							$search_term = ''; // default when no term in session or POST


				}else{
					$this->session->set_flashdata("message",$this->session->userdata('search_location_status'));
					if ( $this->session->userdata('search_location_status') or  strlen($this->session->userdata('search_location_status')) > 0  )
					{

								if(strlen($_table_where) < 2 )
									$_table_where .=" where";
								else
									$_table_where .= " and ";

								$search_term = $this->session->userdata('search_location_status');
								$_table_where .="  dealer_status = '".$search_term."'";
					}

					if ($this->session->userdata('search_location_wedding_location_name') )
					{
								if(strlen($_table_where) < 2 )
									$_table_where .=" where";
								else
									$_table_where .= " and ";

								$search_term = $this->session->userdata('search_location_wedding_location_name');
								$_table_where .="  wedding_location_name  like '%".$search_term."%'";
					}

				}
				// 返回連結
		    $this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."add";
		    $this->page_data['_edit_link']=$this->page_data['init_control'].$this->page_data['_url']."edit/";
		    $this->page_data['_delete_link']=$this->page_data['init_control'].$this->page_data['_url']."delete/";
		    $this->page_data['_search_link']=$this->page_data['init_control'].$this->page_data['_url']."search/";

		    $config = array();
		    $config['total_rows'] = $this->Sean_db_tools->db_get_max_num($this->page_data['_table_key'],$this->page_data['_table'],$_table_where);

		    if(is_numeric($_action))
		    {
		    	$_start_num=  $_action + 1;
		    	if($_start_num <  1 )
		    	$_start_num=1;

		    	$_end_num=$_start_num + $this->page_data["_per_page"] - 1;

		    	if($_end_num  > $config['total_rows'])
		    	$_end_num=$config['total_rows'];
		    	$_table_where .= " limit $_start_num , ".$this->page_data["_per_page"];
		    }else{
		    	$_start_num=1;
		    	$_end_num=$this->page_data["_per_page"];

		    	if($_end_num  > $config['total_rows'])
		    	$_end_num=$config['total_rows'];

		    	$_table_where .=" limit  ".$this->page_data["_per_page"];
		    }
				//$_result01=$this->Sean_db_tools->db_get_max_record("*",$this->page_data['_table'],$_table_where);
				//$_result01=$this->Sean_db_tools->db_get_max_record("*","ij_dealer left join ij_member on ij_dealer.dealer_sn=ij_member.associated_dealer_sn",$_table_where);
				$_result01=$this->$_model_name->get_dealer_list($_table_where);
				$this->page_data['_result01']=$_result01;

				$this->load->library("pagination");

				$config['base_url'] = $this->page_data['_father_link'];
				$config['full_tag_open'] = '<div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing '.$_start_num.' to '.$_end_num.' of '.$config['total_rows'].' entries</div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                 <ul class="pagination" style="visibility: visible;"> ';
				$config['full_tag_close'] = '</ul></div>
              </div>
            </div>';

        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class="previous">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><span>';
        $config['cur_tag_close'] = '</span></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

				$config['per_page'] = $this->page_data["_per_page"];
				$this->pagination->initialize($config);
				$this->page_data["_pagination"]=$this->pagination->create_links();

				$this->page_data['_city_ct'] = $this->libraries_model->_select('ij_city_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','city_code,city_name'); // city
				//var_dump($this->page_data['_city_ct']);
				// 頁面主內容 view 位置, 必要
				$this->page_data['page_content']['view_path'] = $this->page_data['view_control'].$_page_content;
				// 頁面主內容的 JS view 位置, 必要
				$this->page_data['page_level_js']['view_path'] = $this->page_data['view_control'].$_page_level_js;
				$this->load->view($this->page_data['view_control'].'general/main', $this->page_data);
			break;
		}
	}




  /**
	 * 會員列表 Template
	 *
	 */

	 public function dealerTable2()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Member_table";
		/* END 修改 */


		$this->load->model('member/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item="Member/dealerTable_item/"; //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control']."Member/dealerTable_item/add";

    $_where=" where ( member_level =2 and system_user_flag=0)  order by member_sn desc";

		$_result01=$this->Sean_db_tools->db_get_max_record("*","ij_member",$_where);
		$this->page_data['_result01']=$_result01;




		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'admin/page_dealer/list/table';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'admin/page_dealer/list/table_js';
		$this->load->view('admin/general/main', $this->page_data);;
	}


	/**
	 * 會員編輯 Template
	 *
	 */
	public function dealerTable_item($_action,$_id = 0)
	{


		/* Begin 修改 */
		//載入 model
		$_model_name="Member_table";
		/* END 修改 */


		$this->load->model('member/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item="Member/dealerTable_item/"; //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");


		// 內容設定
		$this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."/adding";


		 //編輯
    if($_action=="edit")
		{
			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where);
			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;

		}

		//更新
		if($_action=="update")
		{

			$_where=" where member_sn='".$_id."'";
			$this->$_model_name->action_update($_where,"member_sn",$_id);
			$this->session->set_flashdata("success_message","更新完成！");
			redirect("/admin/Member/dealerTable");
			exit();
		}
		$this->input->get_post('some_data', TRUE);
		//新增
		if($_action == "adding")
		{

			$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");


		}
		$_dealer_status_ct=$this->config->item("dealer_status_ct");
		$_dealer_level_type_ct=$this->config->item("dealer_level_type_ct");

    $this->page_data["em_columns"] = array(
		  //'delimiter_1'=>array('inner_html'=>'<span style="font-size:13px;font-weight:bold;color:#336699"><i>'.$_lang["delimiter_1"].'</i></span><br />'),
			'user_name'=>array('header'=>$this->lang->line("user_name"),'type'=>(($_action=="edit") ? 'label' : 'textbox'), 'required'=>'required', "data_validation_email_message"=>"請輸入email!",'required_message'=>"請輸入email!",'form_control'=>'', 'placeholder'=> (($_action=="edit") ? '' : '請輸入email') , 'post_addition'=> (($_action=="edit") ? '' : 'email@example.com (登入時使用) '),'pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'last_name'=>array('header'=>$this->lang->line("last_name"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>"", 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'first_name'=>array('header'=>$this->lang->line("first_name"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>"", 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'tel'=>array('header'=>$this->lang->line("tel"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'','required_message'=>"", 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'cell'=>array('header'=>$this->lang->line("cell"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>"", 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'wedding_date'=>array('header'=>$this->lang->line("wedding_date"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>"", 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'10', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'birthday'=>array('header'=>$this->lang->line("birthday"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>"", 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'10', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'addr1'=>array('header'=>$this->lang->line("address"), 'type'=>'address',"data_validation_email_message"=>"",'required'=>'','required_message'=>"", 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			//'owner_dealer'=>array('header'=>$this->lang->line("owner_dealer"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'','required_message'=>"", 'form_control'=>'select2_dealer', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'dealer_level_type'=>array('header'=>'帳號角色', 'type'=>'enum','source'=>$_dealer_level_type_ct,'required'=>'','required_message'=>"", 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'dealer_status'=>array('header'=>$this->lang->line("member_status"), 'type'=>'enum', 'source'=>$_dealer_status_ct, 'required_message'=>$this->lang->line("member_status"),'required'=>'required','form_control'=>'','maxlength'=>'', 'default'=>'2',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	  'memo'=>array('header'=>$this->lang->line("memo"), 'type'=>'textarea',"data_validation_email_message"=>"",'required'=>'','required_message'=>"", 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'password'=>array('header'=>$this->lang->line("password"), 'type'=>'password',"data_validation_number_message"=>"", 'required'=>'','required_message'=>"",'form_control'=>'','maxlength'=>'30', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			//'password2'=>array('header'=>$this->lang->line("password2"), 'type'=>'password',"data_validation_number_message"=>"", 'required'=>'','required_message'=>"",'form_control'=>'','maxlength'=>'30', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );

		$this->page_data['page_content']['view_path'] = 'admin/page_dealer/list/item'; // 頁面主內容 view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'admin/page_dealer/list/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->page_data['_city_ct'] = $this->libraries_model->_select('ij_city_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','city_code,city_name'); // city
		//var_dump($this->page_data['_city_ct']);
		$this->load->view('admin/general/main', $this->page_data);;
	}

/**
	 * 經銷商類別編輯 Template
	 *
	 */
	public function dealerLevel($_action="",$_id = 0)
	{

		/* Begin 修改 */

		//載入 model
		$_model_name="Dealerlevel";
		$this->page_data['_id']=$_id;// 功能名稱
		$this->page_data['_father_name']="經銷商分級列表";// 功能名稱
		$this->page_data['_ct_name']="分級列表";// 功能名稱
		$this->page_data['_ct_name_item']="經銷商分級列表";// 功能子名稱
		$this->page_data['_url']="Member/dealerLevel/";//列表功能url
		$this->page_data['_table']="ij_dealer_level_config";//table name
		$this->page_data['_table_key']="dealer_level_sn";//table key
		$this->page_data['_member_data']=$this->session->userdata('member_data');
		$this->page_data['_table_field']=array(
					"id"=>"dealer_level_sn",
					"名稱"=>"dealer_level_name",
					"經銷商專頁功能"=>"dealer_page_profile_config",
					"經銷商後台功能"=>"dealer_backend_config",
					//"排序"=>"sort_order", 2016-1-20 bridina說不用排序 wdj
					"是否啟用"=>"status",
					"說明"=>"description",

				);

		$_table_where=" order by last_time_update asc ";//table list where
		$_page_content="page_dealer/level/table";
		$_page_level_js="page_dealer/level/table_js";
		$_page_content_item="page_dealer/level/item";
		$_page_level_js_item="page_dealer/level/item_js";


		/* END 修改 */
		$this->page_data['_father_link']=$this->page_data['init_control'].$this->page_data['_url'];
		$this->load->model('member/'.$_model_name);


		switch($_action){
			case "add" :
			case "item" :
			case "edit" :
			case "adding" :
			case "update" :

				// 內容設定
				 $this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."adding";

				//$_option_limitation=$this->Sean_db_tools->db_get_max_record("*","ij_config"," where type='product_option_limitation' and active='Y' order by sort asc");
				//$this->page_data['_option_limitation']=$_option_limitation;


				 //編輯
		    if($_action=="edit")
				{
					$this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."update/".$_id."";

					$_result="";
					$_where=" where ".$this->page_data['_table_key']."='".$_id."'";
					$_result=$this->Sean_db_tools->db_get_max_record("*",$this->page_data['_table'],$_where);

					//var_dump($_result);
					$this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."update/$_id";
					$this->page_data['_result01']=$_result;

				}

				//更新
				if($_action=="update")
				{

					$_where= "  where  ".$this->page_data['_table_key']."='".$_id."'";
					$_result=$this->$_model_name->action_update($_where,$this->page_data['_table_key'],$_id);
					if($_result)
						$this->session->set_flashdata("success_message","更新完成！");
					else
						$this->session->set_flashdata("fail_message","更新失敗！");
					redirect($this->page_data['_father_link']);
					exit();
				}



				//新增
				if($_action == "adding")
				{
					$_result=$this->$_model_name->action_insert($this->page_data);
					if($_result)
						$this->session->set_flashdata("success_message","新增完成！");
					else
						$this->session->set_flashdata("fail_message","新增失敗！");



				}

				// 頁面主內容 view 位置, 必要
				$this->page_data['page_content']['view_path'] = $this->page_data['view_control'].$_page_content_item;
				// 頁面主內容的 JS view 位置, 必要
				$this->page_data['page_level_js']['view_path'] = $this->page_data['view_control'].$_page_level_js_item;
				$this->load->view($this->page_data['view_control'].'general/main', $this->page_data);

			break;

			default:
				//刪除
				if($_action=="delete")
				{

					$_where=" where ".$this->page_data['_table_key']."='".$_id."'";
					$_result=$this->$_model_name->action_delete($_where,$this->page_data);
					if($_result)
						$this->session->set_flashdata("success_message","刪除完成！");
					else
						$this->session->set_flashdata("fail_message","刪除失敗！");
				}

						$_result="";
					$_where=" order by last_time_update desc ";
					$_result=$this->Sean_db_tools->db_get_max_record("*",$this->page_data['_table'],$_where);


					$this->page_data['_result01']=$_result;


				// 返回連結
		    $this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."add";
		    $this->page_data['_edit_link']=$this->page_data['init_control'].$this->page_data['_url']."edit/";
		    $this->page_data['_delete_link']=$this->page_data['init_control'].$this->page_data['_url']."delete/";
		    $this->page_data['_search_link']=$this->page_data['init_control'].$this->page_data['_url']."search/";




				// 頁面主內容 view 位置, 必要
				$this->page_data['page_content']['view_path'] = $this->page_data['view_control'].$_page_content;
				// 頁面主內容的 JS view 位置, 必要
				$this->page_data['page_level_js']['view_path'] = $this->page_data['view_control'].$_page_level_js;
				$this->load->view( $this->page_data['view_control'].'general/main', $this->page_data);
			break;
		}



	}

	/**
	 * 經銷商類別項目編輯 Template
	 *
	 */
	public function dealerLevelItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_dealer/level/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_dealer/level/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}


	/**
	 * 會員列表 Template
	 *
	 */

	 public function memberTable($_action="")
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Member_table";
		/* END 修改 */


		$this->load->model('member/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";
    $_sql_search="";
    if($_action=="search")
		{
			$_search_array=array(
					"member_status",
					"wedding_date_from",
					"wedding_date_to",
					"user_name",
					"first_name",
					"last_name",
					"addr_city_code",
					"addr_town_code",
					"addr1",
					"member_level_type",
			);
			$_data="";
			foreach($_search_array as $key => $value){
				if($this->input->get_post($value) || strlen($this->input->get_post($value)) > 0){
					$_data.=$this->input->get_post($value);
					if($value=="user_name" || $value=="wedding_date_from" || $value=="wedding_date_to" ){
						if($value=="user_name"){
							$_sql_search.=" and user_name like '%".$this->input->get_post($value)."%'";
							//$_sql_search.=" and first_name='".substr($this->input->get_post($value), 1)."'";
					    }
						if($value=="wedding_date_from"){
							$_sql_search.=" and wedding_date>='".$this->input->get_post($value)."'";
						}
					    if($value=="wedding_date_to"){
							$_sql_search.=" and wedding_date<='".$this->input->get_post($value)."'";
  					    }
					}else{
						if($value=="first_name" || $value=="last_name" ){
							$_sql_search.=" and ".$value." like '".$this->input->get_post($value)."%'";
						}else{
							$_sql_search.=" and ij_member.".$value."='".$this->input->get_post($value)."'";
						}
					}
				}
			}
			 //$this->session->set_flashdata("fail_message",$_data);
		}
        switch(@$this->session->userdata('member_role_system_rules')[0]['role_name']){
        case '經銷商管理員':
            $_associated_dealer_sn=@$this->session->userdata['member_data']['associated_dealer_sn'];
        	$_where=" where (ij_member.member_level_type=1 and system_user_flag=0 and dealer_sn=$_associated_dealer_sn and ij_member_dealer_relation.status='1')  ";
		    $_where.=$_sql_search."  order by member_sn desc";
			$_result01=$this->Sean_db_tools->db_get_max_record("ij_member.*,ij_member_level_type_ct.member_level_type_name,ij_city_ct.city_name,ij_town_ct.town_name","ij_member left join ij_city_ct on ij_city_ct.city_code=ij_member.addr_city_code left join ij_town_ct on ij_town_ct.town_code=ij_member.addr_town_code left join ij_member_level_type_ct on ij_member_level_type_ct.member_level_type=ij_member.member_level_type left join ij_member_dealer_relation on ij_member_dealer_relation.member_sn=ij_member.member_sn",$_where);
        break;
        case '供應商管理員':
        break;
        default:
        	$_where=" where (ij_member.member_level_type!=9 and system_user_flag=0 and (ij_member.member_level_type in ('1','4','5','6')))  ";
			$_where.=$_sql_search."  order by member_sn desc";
			$_result01=$this->Sean_db_tools->db_get_max_record("ij_member.*,ij_member_level_type_ct.member_level_type_name,ij_city_ct.city_name,ij_town_ct.town_name","ij_member left join ij_city_ct on ij_city_ct.city_code=ij_member.addr_city_code left join ij_town_ct on ij_town_ct.town_code=ij_member.addr_town_code left join ij_member_level_type_ct on ij_member_level_type_ct.member_level_type=ij_member.member_level_type",$_where);
        break;
        }

		//$_result01=$this->Sean_db_tools->db_get_max_record("*","ij_member",$_where);
		foreach($_result01 as $key=>$_Item){
			//關聯經銷商短名
			$_where=" left join ij_dealer on ij_dealer.dealer_sn=ij_member_dealer_relation.dealer_sn where member_sn='".$_Item->member_sn."' ";
			$_member_dealers=$this->Sean_db_tools->db_get_max_record("dealer_short_name","ij_member_dealer_relation",$_where);
			$_member_dealer='';
			foreach($_member_dealers as $Item){
				$_member_dealer.=$Item->dealer_short_name.'、';
			}
			$_result01[$key]->member_dealer=substr($_member_dealer, 0, -3);
		}
		$this->page_data['_result01']=$_result01;
		$this->page_data['city_ct'] = $this->libraries_model->_select('ij_city_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','city_code,city_name'); // city
		$_where=" where display_flag='1' and member_level_type < 7 order by sort_order";
		$_member_level_types=$this->Sean_db_tools->db_get_max_record("*","ij_member_level_type_ct",$_where);
		unset($_member_level_types[1]);
		unset($_member_level_types[2]);
		$this->page_data['_member_level_types']=$_member_level_types;
		//$this->page_data['member_level_types']=$member_level_types;
		//var_dump($this->page_data['_member_level_types']);
		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'admin/page_member/page_table';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'admin/page_member/page_table_js';
		$this->load->view('admin/general/main', $this->page_data);
	}


  /*
  送信
  */
  function send_mail($_mail_template="",$member_sn,$user_name,$_back=0)
  {
  	if($member_sn && $user_name){
		  $this->load->library("ijw");
		  if($this->ijw->send_mail($_mail_template,urldecode($user_name),$member_sn,''))
		  {
				$this->session->set_flashdata("success_message","發送完成");
		  }else{
		  	$this->session->set_flashdata("fail_message","發送失敗");
		  }
		}else{
		  	$this->session->set_flashdata("fail_message","發送失敗-資料不足");
		}
		if($_back){
 			redirect(base_url("admin/Member/partnerTable"));
		}else{
 			redirect(base_url("admin/Member/memberTable"));
 		}
		exit();

  }

	/**
	 * 會員編輯 Template
	 *
	 */
	public function memberTable_item($_action,$_id = 0)
	{
		/* Begin 修改 */
		//載入 model
		$_model_name="Member_table";
		/* END 修改 */


		$this->load->model('member/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");


		// 內容設定
		$this->page_data['_father_link']=$_url;
        $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."/adding";
		$this->page_data['page_level_js']['view_path'] = 'admin/page_member/page_item_add_js'; // 頁面主內容的 JS view 位置, 必要

		 //編輯
    if($_action=="edit"){
		//全部的 ij_member_level_type_ct
		$_where=" where display_flag='1' order by sort_order";
		$_member_level_types=$this->Sean_db_tools->db_get_max_record("*","ij_member_level_type_ct",$_where);
		$this->page_data['_member_level_types']=$_member_level_types;
			$this->Check_page_permission('search','查詢');
			$_result="";
			$_where=" where ij_member.".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where);
			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;
			//關聯經銷商短名
			$_where=" left join ij_dealer on ij_dealer.dealer_sn=ij_member_dealer_relation.dealer_sn where member_sn='".$_id."' ";
			$_member_dealers=$this->Sean_db_tools->db_get_max_record("dealer_short_name","ij_member_dealer_relation",$_where);
			$_member_dealer='';
			foreach($_member_dealers as $Item){
				$_member_dealer.=$Item->dealer_short_name.',';
			}
			//echo substr($_member_dealer, 0, -1);
			$this->page_data['_member_dealer']=substr($_member_dealer, 0, -1);
			//全部有效經銷商短名
			$_where=" where dealer_status='1' ";
			$_dealers=$this->Sean_db_tools->db_get_max_record("dealer_short_name","ij_dealer",$_where);
			$_dealer_list='';
			foreach($_dealers as $Item){
				$_dealer_list.='"'.$Item->dealer_short_name.'",';
			}

			$this->page_data['_dealer_list']=substr($_dealer_list, 0, -1);
			$this->page_data['_orders'] = $this->libraries_model->get_orders(array('member_sn'=>$_id));
		    $this->page_data['_messages'] = $this->libraries_model->_get_callcenter_list(array('ij_member_question.member_sn'=>$_id));

			$this->page_data['page_level_js']['view_path'] = 'admin/page_member/page_item_js'; // 頁面主內容的 JS view 位置, 必要

		}

		//更新
		if($_action=="update")
		{
			$this->Check_page_permission('edit','修改');
			$_where=" where member_sn='".$_id."'";
			$this->$_model_name->action_update($_where,"member_sn",$_id);
			$this->session->set_flashdata("success_message","更新完成！");
			redirect("/admin/Member/memberTable");
			exit();
		}
		$this->input->get_post('some_data', TRUE);
		//新增
		if($_action == "adding")
		{
			//一般、聯盟
			$_where=" where display_flag='1' and member_level_type < 3 order by sort_order";
			$_member_level_types=$this->Sean_db_tools->db_get_max_record("*","ij_member_level_type_ct",$_where);
			$this->page_data['_member_level_types']=$_member_level_types;
			$this->Check_page_permission('add','新增');
			$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");
			$this->page_data['page_level_js']['view_path'] = 'admin/page_member/page_item_add_js'; // 頁面主內容的 JS view 位置, 必要
		}
		if($_action == "add")
		{
			$this->Check_page_permission('add','新增');
		}
		$_member_status_ct=$this->config->item("member_status_ct");

    $this->page_data["em_columns"] = array(
		  //'delimiter_1'=>array('inner_html'=>'<span style="font-size:13px;font-weight:bold;color:#336699"><i>'.$_lang["delimiter_1"].'</i></span><br />'),
			'user_name'=>array('header'=>$this->lang->line("user_name"),'type'=>(($_action=="edit") ? 'label' : 'textbox'), 'required'=>'required', "data_validation_email_message"=>"請輸入email!",'required_message'=>"請輸入email!",'form_control'=>'', 'placeholder'=> (($_action=="edit") ? '' : '請輸入email') , 'post_addition'=> (($_action=="edit") ? '' : 'email@example.com (登入時使用) '),'pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'openid_name'=>array('header'=>"FB ID",'type'=>'label', 'required'=>'', "data_validation_email_message"=>"請輸入email!",'required_message'=>"請輸入email!",'form_control'=>'', 'placeholder'=>"", 'post_addition'=>"",'pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'openid_nickname'=>array('header'=>"FB 暱稱",'type'=>'label', 'required'=>'', "data_validation_email_message"=>"請輸入email!",'required_message'=>"請輸入email!",'form_control'=>'', 'placeholder'=> "" , 'post_addition'=>"",'pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'last_name'=>array('header'=>'姓名', 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>"", 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'gender'=>array('header'=>'性別', 'type'=>'radio',"data_validation_email_message"=>"",'required'=>'required','required_message'=>"", 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'email'=>array('header'=>"Email", 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>"", 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'64', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'tel'=>array('header'=>$this->lang->line("tel"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'','required_message'=>"", 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'cell'=>array('header'=>$this->lang->line("cell"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>"", 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'30', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'wedding_date'=>array('header'=>$this->lang->line("wedding_date"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'','required_message'=>"", 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'10', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50','autocomplete'=>'off'),
			'birthday'=>array('header'=>$this->lang->line("birthday"), 'type'=>'textbox',"data_validation_email_message"=>'','required'=>'','required_message'=>'', 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'10', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50','autocomplete'=>'off'),
			'addr1'=>array('header'=>$this->lang->line("address"), 'type'=>'address',"data_validation_email_message"=>"",'required'=>'','required_message'=>"", 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'80', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			//'owner_dealer'=>array('header'=>$this->lang->line("owner_dealer"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'','required_message'=>"", 'form_control'=>'select2_dealer', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'member_status'=>array('header'=>$this->lang->line("member_status"), 'type'=>'enum', 'source'=>$_member_status_ct, 'required_message'=>$this->lang->line("member_status"),'required'=>'required','form_control'=>'','maxlength'=>'2', 'default'=>'1',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	  'memo'=>array('header'=>$this->lang->line("memo"), 'type'=>'textarea',"data_validation_email_message"=>"",'required'=>'','required_message'=>"", 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'256', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'password'=>array('header'=>$this->lang->line("password"), 'type'=>'password',"data_validation_number_message"=>"", 'required'=>'','required_message'=>"",'form_control'=>'','maxlength'=>'30', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'confirm_password'=>array('header'=>$this->lang->line("password2"), 'type'=>'password',"data_validation_number_message"=>"", 'required'=>'','required_message'=>"",'form_control'=>'','maxlength'=>'30', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );

		$this->page_data['page_content']['view_path'] = 'admin/page_member/page_item.php'; // 頁面主內容 view 位置, 必要

		$this->load->view('admin/general/main', $this->page_data);
	}

	public function partnerTable($apply=""){
		$_model_name="partner_table";
		$this->load->model('member/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱

		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
	    $this->page_data['_caption_subject']= $_ct_name."列表";

	    // 返回連結
	    $this->page_data['_father_link']=$_url;
	    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

		$_like=array();
		$_where=array();
		if($apply=="search" or $apply=="applysearch" )
		{
			$_search_array=array(
					"member_status",
					"wedding_date_from",
					"wedding_date_to",
					"user_name",
					"first_name",
					"last_name",
					"addr_city_code",
					"addr_town_code",
					"addr1",
			);
			$_sql_search="";
			foreach($_search_array as $key => $value){
				if($this->input->get_post($value) || strlen($this->input->get_post($value)) > 0){
					//$_data.=$this->input->get_post($value);
					if($value=="user_name" || $value=="wedding_date_from" || $value=="wedding_date_to" ){
						if($value=="user_name"){
							//$_sql_search.=" and user_name like '%".$this->input->get_post($value)."%'";
							$_like['ij_member.user_name']=$this->input->get_post($value);
					    }
						if($value=="wedding_date_from"){
							//$_sql_search.=" and wedding_date>='".$this->input->get_post($value)."'";
							$_where['wedding_date>=']=$this->input->get_post($value);
						}
					    if($value=="wedding_date_to"){
							$_sql_search.=" and wedding_date<='".$this->input->get_post($value)."'";
							$_where['wedding_date<=']=$this->input->get_post($value);
  					    }
					}else{
						if($value=="first_name" || $value=="last_name" ){
							//$_sql_search.=" and ".$value." like '".$this->input->get_post($value)."%'";
							$_like['ij_member.last_name']=$this->input->get_post($value);
						}else{
							//$_sql_search.=" and ".$value."='".$this->input->get_post($value)."'";
							$_where[$value]=$this->input->get_post($value);
						}
					}
				}
			}
			/*if($this->input->get_post("keyword") || strlen($this->input->get_post("keyword")) > 0){
				$_like['ij_member.first_name']=$this->input->get_post("keyword");
			}*/

		}
		$_result01=$this->$_model_name->get_list(0 , 0 , $_where ,  $_like);
		//echo $this->db->last_query();
		//var_dump($_result01);

		$this->page_data['_result01']=$_result01;

		$this->page_data['_apply']=$apply;

		$_where=" where status='1' ";
		$_biz_domain_list=$this->Sean_db_tools->db_get_max_record("*","ij_biz_domain",$_where);
		$this->page_data['_biz_domain_list']=$_biz_domain_list;
		$this->page_data['member_status']=$this->input->get_post("member_status");
		$this->page_data['keyword']=$this->input->get_post("keyword");

		$this->page_data['city_ct'] = $this->libraries_model->_select('ij_city_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','city_code,city_name'); // city

		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'admin/page_partner/list/table';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'admin/page_partner/list/table_js';
		$this->load->view('admin/general/main', $this->page_data);;
	}

	public function partnerItem($_action,$_id = 0)
	{
		$_model_name="partner_table";
		$this->load->model('member/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");


		// 內容設定
		$this->page_data['_action']=$_action;
		$this->page_data['_father_link']=$_url;
    	$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding";
		//產業屬性
		$_where=" where status='1' ";
		$_biz_domain_list=$this->Sean_db_tools->db_get_max_record("*","ij_biz_domain",$_where);
		$this->page_data['_biz_domain_list']=$_biz_domain_list;

		//產業屬性 內容
		$_where=" where associated_member_sn='".$_id."' and status =1 ";
		$_member_domain_relation=$this->Sean_db_tools->db_get_max_record("*","ij_member_domain_relation",$_where);
		$this->page_data['_member_domain_relation']=$_member_domain_relation;
		$this->page_data['_city_ct'] = $this->libraries_model->_select('ij_city_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','city_code,city_name');
	    $_where=" where status=1  order by sort_order asc";
	    $_result03=$this->Sean_db_tools->db_get_max_record("*","ij_partner_level_config",$_where);
	    $this->page_data['_result03']=$_result03;
		$this->page_data['_associated_sales_membe_list'] = $this->libraries_model->_select('ij_member',array('member_status'=>'2','system_user_flag'=>'1','member_level_type'=>'9'),0,0,0,'last_name',0,'list','member_sn,last_name',0,0,0,0);

		 //編輯
    	if($_action=="edit"){
			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where);
			$this->page_data['_action_link']="admin/member/partnerItem/update/$_id";
			$this->page_data['_result01']=$_result;
			//var_dump($_result);
			//partner 資料
			$_where=" where member_sn='".$_id."' ";
			$_partner_list=$this->Sean_db_tools->db_get_max_record("*","ij_partner",$_where);
			$this->page_data['_partner_list']=$_partner_list;
			//var_dump($_partner_list[0]->partner_sn);

		    //關聯會員s
			$_where=" where associated_partner_sn='".$_partner_list[0]->partner_sn."' order by last_time_update desc";
			$_result04=$this->Sean_db_tools->db_get_max_record("ij_member.*,ij_city_ct.city_name,ij_town_ct.town_name","ij_member left join ij_city_ct on ij_city_ct.city_code=ij_member.addr_city_code left join ij_town_ct on ij_town_ct.town_code=ij_member.addr_town_code",$_where);
			$this->page_data['_result04']=$_result04;
			//銀行
			$_where=" where associated_member_sn ='".$_id."'  order by bank_info_sn desc limit 1";
		    $_result02=$this->Sean_db_tools->db_get_max_record("*","ij_bank_info",$_where);
		    $this->page_data['_result02']=$_result02;

			//var_dump($this->page_data['_associated_sales_membe_list']);

			if(!$yearmonth=$this->input->post('yearmonth')){
				$yearmonth=date('Y-m');
			}
			$data_array=array('from'=>date('Y-m').'-01 00:00:00','to'=>date('Y-m-t').' 23:59:59','ij_order.associated_member_sn'=>$_id);
			$orders=$this->libraries_model->get_orders($data_array);
			$this->page_data['cash'] = $orders['total'];
			$this->load->model('Shop_model');
			$cash2=$this->Shop_model->get_gift_cash($_id,$yearmonth,3);
			$this->page_data['cash2'] =$cash2;
			$this->page_data['yearmonth'] =$yearmonth;
			$_where="date_format(order_create_date, '%Y-%m') = '$yearmonth' and ij_order.associated_member_sn=$_id and (checkstatus=1 or checkstatus=2)";
			$this->page_data['total_ok_orders']=$this->libraries_model->get_order_statics('count(*) as t',$_where)['t'];
			$_where="date_format(order_create_date, '%Y-%m') = '$yearmonth' and ij_order.associated_member_sn=$_id and checkstatus<>1 and checkstatus<>2";
			$total_orders=$this->libraries_model->get_order_statics('count(*) as t',$_where,0)['t'];
			//echo $this->db->last_query();
			$this->page_data['total_no_ok_orders']=$total_orders;

			$this->page_data['total_members']=$this->libraries_model->get_member_by_partner_sn($_partner_list[0]->partner_sn,'count(*) as t',$yearmonth)['t'];
			$this->page_data['total_hits']=$this->libraries_model->get_hits_by_partner_sn($_partner_list[0]->partner_sn,'count(*) as t',$yearmonth)['t'];


		}elseif($_action=="update"){
			$_where=" where member_sn='".$_id."'";
			if($this->$_model_name->action_update($_where,"member_sn",$_id)){
				$this->session->set_flashdata("success_message","更新完成！");
				redirect("/admin/member/partnerTable");
			}else{
				$this->ijw->_wait(base_url("admin/member/partnerItem/edit/").$_id , 2 , '帳號重複，請更改',1);
			}
			exit();
		}elseif($_action == "adding"){
			if($this->$_model_name->action_insert()){
				$this->session->set_flashdata("success_message","新增完成！");
			}else{
				$this->session->set_flashdata("success_message","新增失敗！");
			}
			//exit;
			redirect("/admin/member/partnerTable");
		}
		$_member_status_ct=$this->config->item("member_status_ct");

		$this->page_data['page_content']['view_path'] = 'admin/page_partner/list/item'; // 頁面主內容 view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'admin/page_partner/list/item_js'; // 頁面主內容的 JS view 位置, 必要

		$this->load->view('admin/general/main', $this->page_data);;
	}




	public function partnerLevel($_action="",$_id = 0){

		//載入 model
		$_model_name="Partner_level";
		$this->page_data['_id']=$_id;// 功能名稱
		$this->page_data['_father_name']="聯盟會員分級";// 功能名稱
		$this->page_data['_ct_name']="分級列表";// 功能名稱
		$this->page_data['_ct_name_item']="聯盟會員分級列表";// 功能子名稱
		$this->page_data['_url']="Member/partnerLevel/";//列表功能url
		$this->page_data['_table']="ij_partner_level_config";//table name
		$this->page_data['_table_key']="partner_level_sn";//table key
		$this->page_data['_member_data']=$this->session->userdata('member_data');
		$this->page_data['_table_field']=array(
					"id"=>"partner_level_sn",
					"名稱"=>"partner_level_name",
					"是否啟用"=>"status",
					"說明"=>"description",
					"排序"=>"sort_order"

				);

		$_table_where=" order by sort_order asc ";//table list where
		$_page_content="page_partner/level/table";
		$_page_level_js="page_partner/level/table_js";
		$_page_content_item="page_partner/level/item";
		$_page_level_js_item="page_partner/level/item_js";


		/* END 修改 */
		$this->page_data['_father_link']=$this->page_data['init_control'].$this->page_data['_url'];
		$this->load->model('member/'.$_model_name);


		switch($_action){
			case "add" :
			case "item" :
			case "edit" :
			case "adding" :
			case "update" :

				// 內容設定
				 $this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."adding";

				//$_option_limitation=$this->Sean_db_tools->db_get_max_record("*","ij_config"," where type='product_option_limitation' and active='Y' order by sort asc");
				//$this->page_data['_option_limitation']=$_option_limitation;


				 //編輯
		    if($_action=="edit")
				{
					$this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."update/".$_id."";

					$_result="";
					$_where=" where ".$this->page_data['_table_key']."='".$_id."'";
					$_result=$this->Sean_db_tools->db_get_max_record("*",$this->page_data['_table'],$_where);


					$this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."update/$_id";
					$this->page_data['_result01']=$_result;

				}

				//更新
				if($_action=="update")
				{


					$_result=$this->$_model_name->action_update($this->page_data['_table_key'],$_id);
					if($_result)
						$this->session->set_flashdata("success_message","更新完成！");
					else
						$this->session->set_flashdata("fail_message","更新失敗！");


					redirect($this->page_data['_father_link']);
					exit();
				}



				//新增
				if($_action == "adding")
				{
					$_result=$this->$_model_name->action_insert($this->page_data);
					if($_result)
						$this->session->set_flashdata("success_message","新增完成！");
					else
						$this->session->set_flashdata("fail_message","新增失敗！");



				}

				// 頁面主內容 view 位置, 必要
				$this->page_data['page_content']['view_path'] = $this->page_data['view_control'].$_page_content_item;
				// 頁面主內容的 JS view 位置, 必要
				$this->page_data['page_level_js']['view_path'] = $this->page_data['view_control'].$_page_level_js_item;
				$this->load->view($this->page_data['view_control'].'general/main', $this->page_data);

			break;

			default:
				//刪除
				if($_action=="delete")
				{

					$_where=" where ".$this->page_data['_table_key']."='".$_id."'";
					$_result=$this->$_model_name->action_delete($_where,$this->page_data);
					if($_result)
						$this->session->set_flashdata("success_message","刪除完成！");
					else
						$this->session->set_flashdata("fail_message","刪除失敗！");
				}

						$_result="";
					//$_where=" order by sort_order asc ";
					$_where="";
					$_result=$this->Sean_db_tools->db_get_max_record("*",$this->page_data['_table'],$_where);


					$this->page_data['_result01']=$_result;


				// 返回連結
		    $this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."add";
		    $this->page_data['_edit_link']=$this->page_data['init_control'].$this->page_data['_url']."edit/";
		    $this->page_data['_delete_link']=$this->page_data['init_control'].$this->page_data['_url']."delete/";
		    $this->page_data['_search_link']=$this->page_data['init_control'].$this->page_data['_url']."search/";




				// 頁面主內容 view 位置, 必要
				$this->page_data['page_content']['view_path'] = $this->page_data['view_control'].$_page_content;
				// 頁面主內容的 JS view 位置, 必要
				$this->page_data['page_level_js']['view_path'] = $this->page_data['view_control'].$_page_level_js;
				$this->load->view( $this->page_data['view_control'].'general/main', $this->page_data);
			break;
		}


		}
			//---------------------------------
			// EMAIL EXISTS (true or false)
			//---------------------------------
			private function Email_exists($email)
			{

				$_result=0;
				$_where= " where user_name='".$email."' ";
				$_result=$this->Sean_db_tools->db_get_max_num("member_sn","ij_member",$_where);

				return $_result;
			}

			//---------------------------------
			// AJAX REQUEST, IF EMAIL EXISTS
			//---------------------------------
			function Register_email_exists()
			{
				if (array_key_exists('user_name',$_POST))
				{


					if( $this->Email_exists($this->input->post('user_name')) > 0 )
					{
							echo '<span style="color:#f00">此Email已註冊。 </span>';
					} else {
						 echo '<span style="color:#0c0">此Email可註冊。</span>';
					}
				}
				die();// or exit
			}

		public function dealerPage($_id = 0){
			$_where=" where dealer_sn='".$_id."'";
			$_result=$this->Sean_db_tools->db_get_one_record("dealer_sn",'ij_dealer',$_where);
			if($_id && $_result){
				if($data_array=$this->input->post('dealer')){

					//首頁 Slides
					$slider=$this->do_upload($_FILES['slider'],$_id,'page_home_slider_save_dir');
					if($slider['Mes']!=''){
						$Mes.='首頁 Slides:'.$slider['Mes'];
					}else{
						$data_array['page_home_slider_save_dir']=$slider['filenames'];
					}
					//關於我們
					$aboutus=$this->do_upload($_FILES['aboutus'],$_id,'page_aboutus_pic_save_dir');
					if($aboutus['Mes']!=''){
						$Mes.='<br>關於我們:'.$aboutus['Mes'];
					}else{
						$data_array['page_aboutus_pic_save_dir']=$aboutus['filenames'];
					}
					//專頁大 Banner
					$banner=$this->do_upload($_FILES['banner'],$_id,'page_cover_photo_save_dir');
					if($banner['Mes']!=''){
						$Mes.='<br>專頁大 Banner:'.$banner['Mes'];
					}else{
						$data_array['page_cover_photo_save_dir']=$banner['filenames'];
					}
					//專頁頭像
					$profile=$this->do_upload($_FILES['profile'],$_id,'page_profile_pic_save_dir');
					if($profile['Mes']!=''){
						$Mes.='<br>專頁頭像:'.$profile['Mes'];
					}else{
						$data_array['page_profile_pic_save_dir']=$profile['filenames'];
					}
					//專頁大 Banner
					$login=$this->do_upload($_FILES['login'],$_id,'page_login_wallpaper_save_dir');
					if($login['Mes']!=''){
						$Mes.='<br>關於我們:'.$login['Mes'];
					}else{
						$data_array['page_login_wallpaper_save_dir']=$login['filenames'];
					}
					if(!$data_array['page_facebook_id'] && $data_array['page_facebook_url_set']){
						$this->load->library("ijw");
						$data_array['page_facebook_id']=$this->ijw->get_facebook_page_id($data_array['page_facebook_url_set']);
					}
					//var_dump($data_array['page_aboutus_description']);
					if(is_array($data_array['page_aboutus_description'])){
						$data_array['page_aboutus_description']=implode(":::",$data_array['page_aboutus_description']);
					}
					//var_dump($data_array['page_aboutus_description']);
					//exit();
					$_result=$this->Sean_db_tools->db_update_record($data_array,'ij_dealer',$_where,'','');

					//其他社群媒體設定
					$social_medias=$this->input->post('social_media[]');
					foreach($social_medias as $key=>$social_media){
						$social_medias[$key]['status']='1';
						$social_medias[$key]['dealer_sn']=$_id;
						if($social_media['dealer_soical_media_sn']){  //判斷新增或更新
						  if($social_media['soical_media_name']){ //有名稱才更新否則刪除
						  	$social_medias[$key] = $this->Sean_db_tools->CheckUpdate($social_medias[$key],0);
						  	//var_dump($social_media['dealer_soical_media_sn']);
						  	//exit();
								$_where1="where dealer_soical_media_sn='".$social_media['dealer_soical_media_sn']."'";
								//echo $_where1;
								echo $this->Sean_db_tools->db_update_record($social_medias[$key],'ij_dealer_soical_media_info',$_where1,'','');
							}else{
								$_where1="where dealer_soical_media_sn='".$social_media['dealer_soical_media_sn']."'";
				  			$this->Sean_db_tools->db_delete_record('ij_dealer_soical_media_info',$_where1,'dealer_soical_media_sn',$social_media['dealer_soical_media_sn']);
							}
						}else{
						  if($social_media['soical_media_name']){ //有名稱才新增
						  	$social_medias[$key] = $this->Sean_db_tools->CheckUpdate($social_medias[$key],1);
								$this->Sean_db_tools->db_insert_record($social_medias[$key],'ij_dealer_soical_media_info');
							}
						}
					}

					$this->session->set_flashdata("success_message","更新完成！".'<br>'.$Mes);
				  redirect(base_url('admin/Member/dealerTable/'));
				  exit();
				}
				$page_data = array();
				// 內容設定
				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'admin/page_dealer/list/page'; // 頁面主內容 view 位置, 必要
				$_result=$this->Sean_db_tools->db_get_one_record("ij_dealer.*,email,user_name","ij_dealer left join ij_member on ij_dealer.dealer_sn=ij_member.associated_dealer_sn",$_where);
				$page_data['page_content']['Item'] = $_result;
				//其他社群媒體
				$_where=" where dealer_sn='".$_id."' and status='1'";
				$dealer_soical_media_info=$this->Sean_db_tools->db_get_max_array("*","ij_dealer_soical_media_info",$_where);
				//var_dump($dealer_soical_media_info);
				$page_data['page_content']['soical_media']=$dealer_soical_media_info;
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'admin/page_dealer/list/page_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('admin/general/main', $page_data);
			}else{
				$this->session->set_flashdata("fail_message","查無此經銷商編號！");
				redirect(base_url('admin/Member/dealerTable/'));
			}
		}

	public function dealerAccounting()
	{
		//var_dump($this->input->post('checkstatus'));exit;
		if($sub_order_sn=$this->input->post('sub_order_sn')){
			$dealer_checkstatus=$this->input->post('dealer_checkstatus');
			$data_array=array('dealer_checkstatus'=>$dealer_checkstatus);
			$data_array = $this->libraries_model->CheckUpdate($data_array,0);
			$this->libraries_model->_update('ij_sub_order',$data_array,'sub_order_sn',$sub_order_sn);
			$this->session->set_flashdata("success_message","修改成功");
			redirect(base_url('admin/member/dealerAccounting'));
			exit;
		}
		$page_data = array();
		// 內容設定
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['sub_order_status']=$this->libraries_model->_select('ij_sub_order_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array','sub_order_status,sub_order_status_name');
		$page_data['page_content']['payment_status']=$this->libraries_model->_select('ij_payment_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array','payment_status,payment_status_name');
		$page_data['page_content']['check_status']=$this->libraries_model->_select('ij_dealer_order_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array','dealer_order_status,dealer_order_status_name');
		$page_data['page_content']['checkstatus']=$this->libraries_model->_select('ij_dealer_order_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','dealer_order_status,dealer_order_status_name');

		$data_array=array('from'=>date('Y-m').'-01','to'=>date('Y-m-t'));
		if($this->input->get_post('from')) $data_array['from']=date("Y-m-d", strtotime($this->input->get_post('from')));
		if($this->input->get_post('to')) $data_array['to']=date("Y-m-d", strtotime($this->input->get_post('to')));
		if($this->input->get_post('sub_order_status')) $data_array['sub_order_status']=(int)$this->input->get_post('sub_order_status');
		if($this->input->get_post('payment_status')) $data_array['payment_status']=(int)$this->input->get_post('payment_status');
		if(strlen($this->input->get_post('dealer_checkstatus'))!=0) $data_array['dealer_checkstatus']=$this->input->get_post('dealer_checkstatus');
		if($this->input->get_post('partner_name')) $data_array['partner_name']=$this->input->get_post('partner_name');
		$orders=$this->libraries_model->get_orders($data_array,3);
        //echo '<pre>' . var_export($orders, true) . '</pre>';

		//$this->load->model('member/partner_table');
		$page_data['page_content']['total'] = $orders['total'];
		unset($orders['total']);
		if($this->input->get_post('flg')=='export'){
	        $this->load->library('io_excel');
	        $this->io_excel->dealerAccounting_export($orders);
	        exit;
		}
		$page_data['page_content']['orders'] = $orders;
		$page_data['page_content']['data_array']=$data_array;
		$page_data['page_content']['view_path'] = 'admin/page_dealer/accounting'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_dealer/accounting_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	public function dealerAccountingItem()
	{
		$page_data = array();

		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_dealer/level/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_dealer/level/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	public function partnerAccounting()
	{
		//var_dump($this->input->post('checkstatus'));exit;
		if($order_sn=$this->input->post('order_sn')){
			$original_order_sn=$this->input->post('original_order_sn');
			$data_array=array('original_order_sn'=>$original_order_sn);
			$data_array = $this->libraries_model->CheckUpdate($data_array,0);
			$this->libraries_model->_update('ij_order',$data_array,'order_sn',$order_sn);
			$this->session->set_flashdata("success_message","修改成功");
			redirect(base_url('admin/member/partnerAccounting'));
			exit;
		}
		$page_data = array();
		// 內容設定
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['sub_order_status']=$this->libraries_model->_select('ij_sub_order_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array','sub_order_status,sub_order_status_name');
		$page_data['page_content']['payment_status']=$this->libraries_model->_select('ij_payment_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array','payment_status,payment_status_name');
		$page_data['page_content']['check_status']=$this->libraries_model->_select('ij_dealer_order_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array','dealer_order_status,dealer_order_status_name');

		$data_array=array('from'=>date('Y-m').'-01','to'=>date('Y-m-t'),'ij_order.associated_member_sn>'=>'0');
		if($this->input->get_post('from')) $data_array['from']=date("Y-m-d", strtotime($this->input->get_post('from')));
		if($this->input->get_post('to')) $data_array['to']=date("Y-m-d", strtotime($this->input->get_post('to')));
		if($this->input->get_post('sub_order_status')) $data_array['sub_order_status']=(int)$this->input->get_post('sub_order_status');
		if($this->input->get_post('payment_status')) $data_array['payment_status']=(int)$this->input->get_post('payment_status');
		if(strlen($this->input->get_post('original_order_sn'))!=0) $data_array['original_order_sn']=$this->input->get_post('original_order_sn');
		if($this->input->get_post('partner_name')) $data_array['partner_name']=$this->input->get_post('partner_name');
		$orders=$this->libraries_model->get_orders($data_array);
		$page_data['page_content']['total'] = $orders['total'];
		unset($orders['total']);
		if($this->input->get_post('flg')=='export'){
	        $this->load->library('io_excel');
	        $this->io_excel->partnerAccounting_export($orders);
	        exit;
		}
        //echo '<pre>' . var_export($orders, true) . '</pre>';
		$page_data['page_content']['orders'] = $orders;
		$page_data['page_content']['data_array']=$data_array;
		$page_data['page_content']['view_path'] = 'admin/page_partner/accounting'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_partner/accounting_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	public function partnerAccountingItem()
	{
		$page_data = array();

		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['_action_link']=$this->init_control.'member/partnerAccounting';
		$page_data['page_content']['_father_link']=$this->init_control.'member/partnerAccounting';
		$page_data['page_content']['view_path'] = 'admin/page_partner/level/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_partner/level/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	public function gift_cash($type='table',$gift_cash_sn=0)
	{
		if($data_array=$this->input->post('Item')){
			//var_dump($data_array);exit;
			if(!$gift_cash_sn=$this->input->post('gift_cash_sn')){ //新增
				$data_array['status']= '1';
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				//$data_array = $this->libraries_model->CheckUpdate($data_array,0);//方便秀最後更新
				//var_dump($this->input->post('Item'));
				$gift_cash_sn=$this->libraries_model->_insert('ij_gift_cash',$data_array);
				$this->session->set_flashdata("success_message",'新增成功!');
			}else{
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				$this->libraries_model->_update('ij_gift_cash',$data_array,'gift_cash_sn',$gift_cash_sn);
				$this->session->set_flashdata("success_message",'修改成功!');
			}
			redirect(base_url('admin/member/gift_cash/'));
		}

		$flg=$this->input->get('flg',true);
		if($flg=='reset'){
			redirect(base_url('admin/member/gift_cash'));
			exit();
		}else{
			$data_array=array('from'=>date('Y-m').'-01','to'=>date('Y-m-t'));
			if($this->input->get('from')) $data_array['from']=date("Y-m-d", strtotime($this->input->get('from')));
			if($this->input->get('to')) $data_array['to']=date("Y-m-d", strtotime($this->input->get('to')));
			if($this->input->get('status')) $data_array['status']=(int)$this->input->get('status');
			if($this->input->get('search')) $data_array['search']=$this->input->get('search');
			if($this->input->get('cash_type')) $data_array['cash_type']=$this->input->get('cash_type');
		}
		$page_data = array();
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['cash_types'] = $this->config->item('cash_type');
		$page_data['page_content']['edit_cash_type'] = array();
		//var_dump($page_data['page_content']['cash_type']);
		if($type=="table"){
			$page_data['page_content']['from'] = $data_array['from'];
			$page_data['page_content']['to'] = $data_array['to'];
			$page_data['page_content']['search'] =@$data_array['search'];
			$page_data['page_content']['status'] =@$data_array['status'];
			$page_data['page_content']['cash_type'] =@$data_array['cash_type'];
			$page_data['page_content']['status_ct']=$this->libraries_model->_select('ij_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array','status,status_name',0,0,0);
			//var_dump($page_data['page_content']['status_ct']);
			$Items = $this->libraries_model->get_gift_cash($data_array);
			$page_data['page_content']['Items'] = $Items;
			//$page_data['page_content']['cash_sum'] = $this->libraries_model->_get_gift_Cash_list(@$member_sn,@$cash_type,$search,$from,$to,1);
			$page_data['page_content']['view_path'] = 'admin/page_member/gift_cash/cash_table'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'admin/page_member/gift_cash/cash_table_js'; // 頁面主內容的 JS view 位置, 必要
		}elseif($type=="add"){
			$page_data['page_content']['view_path'] = 'admin/page_member/gift_cash/cash_add'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'admin/page_member/gift_cash/cash_add_js'; // 頁面主內容的 JS view 位置, 必要
		}else{
			if(@$gift_cash_sn){
				$Item = $this->libraries_model->get_gift_cash(0,$gift_cash_sn);
            //echo $this->db->last_query();
				//var_dump($Item);
			}else{
				//$this->session->set_userdata(array('Mes'=>'查無資料!'));
				redirect(base_url('admin/member/gift_cash/'));
			}
			$page_data['page_content']['Item'] = $Item;
			$this->load->model('Shop_model');
			$cash=$this->Shop_model->get_gift_cash($Item['associated_member_sn']);
			$page_data['page_content']['cash_money'] = $cash;
			//var_dump($page_data['page_content']['cash_money']);
			$page_data['page_content']['view_path'] = 'admin/page_member/gift_cash/cash_item'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'admin/page_member/gift_cash/cash_item_js'; // 頁面主內容的 JS view 位置, 必要
		}
		$this->load->view('admin/general/main', $page_data);
	}
	function auto_uno() {
		$result = $this->libraries_model->_select('ij_member',array('member_status'=>'2'),0,0,0,'member_sn',0,'result_array','member_sn,user_name,last_name',0,0,0,0);

		$uno_arr = array();
		foreach ($result as $key => $value) {
			$uno_arr[$key]['label'] = $value['member_sn'].'-'.$value['last_name'].'  【'.$value['user_name'].'】';
			$uno_arr[$key]['value'] = $value['member_sn'];
		}
		//var_dump($result);
		//exit();
		echo json_encode($uno_arr);
	}
	function get_member_name() {
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$data = (int)trim($this->input->post("data",true));
			$result=$this->libraries_model->get_member($data,'last_name,user_name');
			if($result) {
				echo $data.'-'.$result['last_name'].' 【'.$result['user_name'].'】';
			} else {
				echo "0";
			}
		}
	}
	function do_upload($files,$_id,$field)
	{
			$_where=" where dealer_sn='".$_id."'";
			//var_dump($this->Sean_db_tools->db_get_one_record($field,'ij_dealer',$_where));
			$origin=explode(':',$this->Sean_db_tools->db_get_one_record($field,'ij_dealer',$_where)[$field]);

			$_tmp['filenames']='';
			$_tmp['Mes']='';
	    $this->load->library('upload');
	    //$files = $_FILES;
	    $cpt = count($files['name']);
	    //var_dump($files['name']);

	    for($i=0; $i<$cpt; $i++)
	    {
	    	//var_dump($files['name'][$i]);
	    	if($files['name'][$i]){
	        $_FILES['userfile']['name']= $files['name'][$i];
	        $_FILES['userfile']['type']= $files['type'][$i];
	        $_FILES['userfile']['tmp_name']= $files['tmp_name'][$i];
	        $_FILES['userfile']['error']= $files['error'][$i];
	        $_FILES['userfile']['size']= $files['size'][$i];

	        $this->upload->initialize($this->set_upload_options($_id));
	        if($this->upload->do_upload()){
            $img	 = $this->upload->data();
            $_tmp['filenames'] .=  'dealer/'.$_id.'/'.$img['file_name'];

	        }else{
							$_tmp['Mes'].= strip_tags($this->upload->display_errors());
	        }
	      }else{
         $_tmp['filenames'] .= (@$origin[$i])? $origin[$i]:':'; //原本檔名
	      }
		          if($i< $cpt){
		          	$_tmp['filenames'] .=':';
		          }
	    }
			return $_tmp;
	}
	private function set_upload_options($_id)
	{
	    //upload an image options
	    $config = array();
    $config['upload_path']  = ADMINUPLOADPATH.'dealer/'.$_id.'/' ;
    $config['allowed_types']= 'gif|jpg|png|jpeg';
    $config['max_size']     = '4196';
    //$config['max_width']    = '1000';
    //$config['max_height']   = '900';
    $config['encrypt_name']   = true;

	    return $config;
	}
}
