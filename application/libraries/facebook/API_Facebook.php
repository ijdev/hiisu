<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(session_status() == PHP_SESSION_NONE)
{
  session_start();
}

require_once('autoload.php');

class API_Facebook
{
	var $ci;
	var $fb;
	var $fbApp;
	public function __construct()
	{
		$this->ci =& get_instance();
		$this->fb = new Facebook\Facebook
		([
  			'app_id' => FACEBOOK_APP_ID,
  			'app_secret' => FACEBOOK_APP_SECRET
  		]);
  		$this->fbApp = $this->fb->getApp();
  		//
	}
	 function getLoginUrl($callBackUrl="")
	{
		$helper = $this->fb->getRedirectLoginHelper();
		$permissions = ['public_profile','email'];
		$loginUrl = $helper->getLoginUrl($callBackUrl, $permissions);
		return $loginUrl;
	}
	 function get_profile($token,$fields="")
	{
		$fields = ($fields!="")?("?fields=".$fields):("");
		$this->fb->setDefaultAccessToken((string) $token);
		try {
			$response = $this->fb->get('/me'.$fields);
			return $response->getGraphUser();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			return false;
		}
	}

	function get_destroySession()
	{

		$this->fb->destroySession();

	}


 function get_fbid($token)
{
	$profile = $this->get_profile($token);
	if($profile!=false)
	{
		return $profile['id'];
	}
	else
	{
		return 0;
	}
}

}