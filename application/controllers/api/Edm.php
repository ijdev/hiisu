<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edm extends CI_Controller {
	/*
	* 	get_upg_products : [商城][API] 可升級商品與費用列表
	*	member_data	: 
	*/
  public function __construct(){
      parent::__construct();
      $this->CI =& get_instance();
      $this->load->database();
			$this->load->helper('url');
  }

	public function index()
	{
		$this->load->view('welcome_message');
	}
  //[Edm][API] UPDATE OPEN 
	public function update_edm_open()
	{
				$data_log = array();
				$feedback = "";
				
				if(!$timestamp = $this->input->post('timestamp',true)){
					$timestamp = $this->input->get('timestamp',true);
				}
				if(!$token = $this->input->post('token',true)){
					$token = $this->input->get('token',true);
				}
				if(!$signature = $this->input->post('signature',true)){
					$signature = $this->input->get('signature',true);
				}
				if(!$edm_send_list_sn = (int)base64_decode($this->input->post('edm_send_list_id',true))){
					$edm_send_list_sn = (int)base64_decode($this->input->get('edm_send_list_id',true));
				}
				if(!$edm_sn = (int)base64_decode($this->input->post('edm_id',true))){
					$edm_sn = (int)base64_decode($this->input->get('edm_id',true));
				}
				$apiKey='key-c9f5336346107f2f2d24036f2f86599d';
				//echo hash_hmac('sha256', '14636547483230c45caae222eb673657f0ce547a9e3097bd6610f44d559b', $apiKey);
				$this->load->model('libraries_model');
 				if($this->_verify($apiKey, $token, $timestamp, $signature)){
 					if($this->libraries_model->get_edm_send_list(0,0,0,$edm_send_list_sn)){
						$this->libraries_model->_update('ij_edm_send_list_log',array('edm_open_flag'=>'1'),'edm_send_list_sn',$edm_send_list_sn);
						$this->libraries_model->_update_field_amount('ij_edm','edm_sn',$edm_sn,'send_total_amount','+1');
						$data_log["server_reply_action"]="200";
					}else{
						//錯誤紀錄
						$IP=$this->input->ip_address();
						$data_log["server_reply_action"]="406";
					 	$data_log["client_action"]="[update_edm_open]::edm_send_list_sn-$edm_send_list_sn::edm_sn-$edm_sn::IP-$IP";
					 	$data_log["create_date"]=date("Y-m-d H:i:s");
					 	$data_log["create_member_sn"]="99999";
					 	$data_log["last_time_update"]=date("Y-m-d H:i:s");
					 	$data_log["update_member_sn"]="99999";
					 	$this->api_token_log($data_log);
					}
				}else{
					//var_dump($feedback);
					//$data_log["api_token_sn"]='1';
					//錯誤紀錄
					$IP=$this->input->ip_address();
					$data_log["server_reply_action"]="406";
				 	$data_log["client_action"]="[update_edm_open]::token-$token::timestamp-$timestamp::signature-$signature::IP-$IP";
				 	//$data_log["server_reply_action"].=json_encode($feedback);
				 	$data_log["create_date"]=date("Y-m-d H:i:s");
				 	$data_log["create_member_sn"]="99999";
				 	$data_log["last_time_update"]=date("Y-m-d H:i:s");
				 	$data_log["update_member_sn"]="99999";
				 	$this->api_token_log($data_log);
				}
			$this->CI->output->set_status_header($data_log["server_reply_action"]);
			//echo $data_log["server_reply_action"];

	}
	
  
  function api_token_log($data_log)
  {
		 return $this->db->insert('ij_api_token_log', $data_log);
  }
  
	private function _verify($apiKey, $token, $timestamp, $signature)
	{
	    //check if the timestamp is fresh
	    if (time()-$timestamp>150) {
	        return false;
	    }

	    //returns true if signature is valid
	    return hash_hmac('sha256', $timestamp.$token, $apiKey) === $signature;
	}

}
