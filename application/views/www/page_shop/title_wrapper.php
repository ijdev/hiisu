<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* SHOP TITLE WRAPPER
*/
?>

<div class="title-wrapper" <?php if(@$_category_array_left[0]->banner_save_dir){?>style="background: url('public/uploads/<?php echo @$_category_array_left[0]->banner_save_dir;?>') no-repeat 100% 100%;"<?php }?> >
  <div class="container"><div class="container-inner">
    <h1><span></span> <?php echo @$_category_array_left[0]->category_name;?></h1>
    <em><?php echo @$_category_array_left[0]->category_sub_name;?></em>
  </div></div>
</div>