<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>聯盟會員 <small>管理列表</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        會員管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        聯盟會員
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">聯盟會員列表</span>
            </div>
            <div class="actions btn-set">
             <a href="admin/member/partnerItem/add" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <div class="portlet-body">
            <div class="note note-warning">
              <div class="alert-warning pull-left">
                <!-- 有 <strong>5</strong> 位申請 聯盟資格尚未審核！ -->
              </div>
              <?php
              if($_apply =="apply" || $_apply =="applysearch")
             	echo '<form method="post" action="admin/member/partnerTable/applysearch">';
              else
               echo '<form method="post" action="admin/member/partnerTable/search">';
              ?>
              <div class="form-group">
                <div class="row">
                  <div class="col-md-4">
                    <!--div class="input-group input-large date-picker input-daterange" data-date="2015-05-07" data-date-format="yyyy-mm-dd">
                      <span class="input-group-addon" onclick="alert('Start Search');"> 結婚日期 </span>
                      <input type="text" class="form-control input-sm date-picker" name="wedding_date_from" value="">
                      <span class="input-group-addon"> 到 </span>
                      <input type="text" class="form-control input-sm date-picker" name="wedding_date_to" value="">
                    </div-->
                  </div>
                  <div class="col-md-8">
                    <input id="user_name" name="user_name" type="text" class="pagination-panel-input form-control input-inline input-sm" size="10" placeholder="會員帳號" value="">
                <input id="last_name" name="last_name" type="text" class="pagination-panel-input form-control input-inline input-sm" size="8" style="margin: 0 5px;" placeholder="姓名" value="">
                <!--input id="first_name" name="first_name" type="text" class="pagination-panel-input form-control input-inline input-sm" size="8" style="margin: 0 5px;" placeholder="名"-->
                <select id="addr_city_code" name="addr_city_code"  class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;">
                  <option value="">縣市</option>
                  <?php
                    //$_city_list=$this->config->item("city_ct");
                 while (list($keys, $values) = each ($city_ct)) {
                  echo  ' <option value="'.$keys.'">'.$values.'</option>';

                }

                ?>
                </select>
                <select  id="addr_town_code" name="addr_town_code" class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;width: 92px !important;">
                  <option value="">鄉鎮市區</option>
                </select>
                <select id="member_status" name="member_status" class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;width: 142px !important;">
                  <option value="">帳號狀態</option>
                  <option value="1">已註冊,未驗證成功</option>
                  <option value="2">已驗證成功</option>
                  <option value="3">列入黑名單中</option>
                  <option value="4">失效</option>
                </select>
                <button type="submit" class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button>
                <button id="reset_form" onclick="this.form.flg.value='reset';" class="btn btn-sm purple"><i class="fa"></i> 清除</button>
                  </div>
                </div>
              </div>
            </form>

              <div style="clear:both;"></div>
            </div>


             <?php $this->load->view('sean_admin/general/flash_error');?>
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <!--th>網站名稱</th-->
                  <th>會員名稱</th>
                  <th>產業屬性</th>
                  <th>縣市</th>
                  <th>鄉鎮市區</th>
                  <th>電話</th>
                  <th>Email</th>
                  <th>帳號狀態</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
                 <?php
                 $_member_status_ct=$this->config->item("member_status_ct");
								$_member_level_type_ct=$this->config->item("member_level_type_ct");
								$_city_ct=$this->config->item("city_ct");
                if(is_array($_result01))
                	{
                 		foreach($_result01 as $key => $value){
                			 if($value->member_status == 2)
                				{
                					$_active = '<span class="label label-success">有效</span>';
                				}
                				elseif($value->member_status == 3)
                				{
                					$_active = '<span class="label label-danger">停用</span></i>';
                				}else{
                					$_active='<span class="label label-warning">審核中</span>';
                				}
                			?>
                  <tr class="odd gradeX">
                    <!--td><?php echo $_partner_website_name;?></td-->
                    <td width="10%"><?php echo $value->last_name;?></td>
                    <td><?php echo $value->domain_name;?></td>
                    <td width="8%"><?php echo $value->city_name;?></td>
                    <td width="8%"><?php echo $value->town_name;?></td>
                    <td><?php echo $value->cell;?></td>
                    <td><?php echo $value->email;?></td>
                    <td class="center" id="member_status<?php echo $value->member_sn;?>"><?php echo $_active;?></td>
                    <td class="center">
                      <a href="admin/member/partnerItem/edit/<?php echo $value->member_sn;?>"><i class="fa fa-edit"></i>編輯</a>&nbsp;
                      <a target="_blank" href="admin/member/partnerAccounting?partner_name=<?php echo $value->last_name;?>"><i class="fa fa-cogs"></i>帳務管理</a>&nbsp;
                      <a href="<?php echo $init_control;?>Member/send_mail/<?php echo urlencode("忘記密碼");?>/<?php echo $value->member_sn;?>/<?php echo urlencode($value->user_name);?>/1" ><i class="fa fa-lightbulb-o"></i>忘記密碼</a>&nbsp;
<?php if(stripos($this->ijw->check_permission(1,$this->router->fetch_class(),$this->router->fetch_method()),'del')){?>
                      <a href="javascript:void(0);" class="deleteItem" ItemId="<?php echo $value->member_sn;?>"><i class="fa fa-trash-o"></i><?=($value->member_status=='3')?'刪除':'停用'?>
                      </a>
<?php }?>
                    </td>
                  </tr>
                <?php } } ?>
              </tbody>
            </table>
            <!-- page nav
            <div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
              </div>

            </div>-->
            <!-- end of page nav -->
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->