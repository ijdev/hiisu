<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 首頁 slider
*/
?>
<!-- BEGIN SLIDER -->
<div class="page-slider margin-bottom-35">
  	<!-- LayerSlider start -->
	<a href="javascript:;">
	  	<div id="layerslider" style="width: 100%; height: 494px; margin: 0 auto;">

		    <!-- slide one start -->
		    <div class="ls-slide ls-slide1" data-ls="offsetxin: right; slidedelay: 5000; transition2d: 110,111,112,113;">
		      <img src="public/img/slide-ewedding-home01" class="ls-bg" alt="Slide background">
		    </div>
		    <!-- slide one end -->

	  	</div>
  	</a>
  	<!-- LayerSlider end -->
</div>
<!-- END SLIDER -->