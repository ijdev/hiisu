<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/pages/css/portfolio.css" rel="stylesheet" type="text/css"/>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>FAQ內容 <small>文章編輯</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">FAQ編輯</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/content/contentFaq" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-md-2 control-label">是否發佈</label>
                  <div class="col-md-10">
                    <div class="radio-list">
                        <label class="radio-inline">
                          <input type="radio" required name="Item[post_flag]" id="optionsRadios4" value="1" <?php echo (@$Item['post_flag']=='1') ? 'checked':''; ?> >  發佈
                        </label>
                        <label class="radio-inline">
                          <input type="radio" required name="Item[post_flag]" id="optionsRadios4" value="2" <?php echo (@$Item['post_flag']=='2') ? 'checked':''; ?> >  不發佈
                        </label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-md-2 control-label">Question</label>
                  <div class="col-md-10">
                    <input name="Item[faq_titile]" required type="text" class="form-control" value="<?php echo @$Item['faq_titile'];?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-md-2 control-label">頻道 <span class="require">*</span></label>
                  <div class="col-md-3">
                    <?php echo $Store;?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">內容分類: <span class="required"> * </span></label>
                  <div class="col-md-3">
                  <?php echo $content_types_array;?>
                    <?php /*<div class="radio-list" id="content_types_area">
                    <?php if($content_types){foreach($content_types as $key=>$_Item){ if($_Item['post_channel_name_set']==$Item['post_channel_name_set']){?>
                      <label class="radio-inline">
                      <input type="radio" name="Item[associated_content_type_sn_set]" <?php echo (strrpos(@$Item['associated_content_type_sn_set'], $_Item['content_title'])!==false)?'checked':'';?> value="<?php echo $_Item['content_title'];?>"> <?php echo $_Item['content_title'];?> </label>
                    <?php }}}?>
                    </div>*/?>
                  </div>
                </div>

                <div class="form-group">
                 <label class="col-md-2 control-label">商品分類 <span class="require">*</span></label>
                 <div class="col-md-10">
                  <?php echo $categorys;?>
                </div>
                </div>
               <div class="form-group">
                  <label for="note" class="col-md-2 control-label">Answer</label>
                  <div class="col-md-10">
                    <textarea name="Item[faq_description]" required class="form-control" rows="6"><?php echo @$Item['faq_description'];?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-md-2 control-label">排序 <span class="required"> * </span></label>
                  <div class="col-md-10">
                    <input name="Item[sort_order]" maxlength="4" type="text" class="form-control" value="<?php echo (@$Item['sort_order']) ? @$Item['sort_order']:'9999'; ?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-lg-2 control-label">更新者</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['update_member_sn'])?@$Item['update_member_sn']:@$Item['create_member_sn']; ?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-2 control-label">更新時間</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['last_time_update']!='0000-00-00 00:00:00')?@$Item['last_time_update']:@$Item['create_date']; ?>" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <input type="hidden" name="faq_sn" value="<?php echo @$Item['faq_sn']; ?>">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="reset" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->

<!-- Modal -->
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">上傳圖片</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-without-legend" role="form">
          <fieldset>
            <div class="form-group">
              <label for="email" class="col-lg-4 control-label">圖片標題 <span class="require">*</span></label>
              <div class="col-md-10">
                <input type="text" class="form-control" id="email">
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-4 control-label">顏色</label>
              <div class="col-md-10">
                <select class="form-control">
                  <option>粉紅色</option>
                  <option>黑色</option>
                  <option>金色</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-4 control-label">選擇檔案</label>
              <div class="col-md-10">
                <input class="form-control" name="photo" type="file" id="photo" onchange="">
              </div>
            </div>
          </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">上傳</button>
      </div>
    </div>
  </div>
</div>
