/*
 Copyright (c) 2007-2016, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.html or http://cksource.com/ckfinder/license
 */

var config = {};

// Set your configuration options below.

// Examples:
// config.skin = 'jquery-mobile';
//config.language = 'zh';
//config.defaultLanguage = 'zh';
//config.languages = { en: 1, zh: 1 };
CKFinder.define( config );
