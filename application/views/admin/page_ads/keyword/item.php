<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<!-- END PAGE LEVEL STYLES -->
<style>
  .feature-img{vertical-align: bottom;}
  .adType{display: none;}
</style>


<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>行銷工具</h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        行銷工具
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/marketing/keyword">熱門關鍵字</a><i class="fa fa-circle"></i>
      </li>
      <li class="active">
        熱門關鍵字設定
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">熱門關鍵字設定</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/marketing/keyword" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <div class="tabbable">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_general" data-toggle="tab"> 一般設定 </a>
                </li>
                <!--li>
                  <a href="#tab_schedule" data-toggle="tab"> <?php echo @$Item['banner_content_name'];?> 檔期紀錄 </a>
                </li-->
              </ul>
              <div class="tab-content no-space">
                <!-- Tab 資料設定 -->
                <div class="tab-pane active" id="tab_general">
			            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
			              <div class="form-body">
			                <div class="form-group">
			                  <label for="name" class="col-lg-3 control-label">名稱</label>
			                  <div class="col-lg-8">
			                    <input name="Item[banner_content_name]" type="text" class="form-control" value="<?php echo @$Item['banner_content_name'];?>">
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="name" class="col-lg-3 control-label">起始時間</label>
			                  <div class="col-lg-4">
			                    <div class="input-group date form_datetime">
			                      <input name="Item[banner_eff_start_date]" type="text" class="form-control" value="<?php echo (@$Item['banner_eff_start_date']!='0000-00-00 00:00:00')?@$Item['banner_eff_start_date']:'';?>">
			                      <span class="input-group-btn">
			                        <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
			                      </span>
			                    </div>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="name" class="col-lg-3 control-label">結束時間</label>
			                  <div class="col-lg-4">
			                    <div class="input-group date form_datetime">
			                    	<input name="Item[banner_eff_end_date]" type="text" class="form-control" value="<?php echo (@$Item['banner_eff_end_date']!='0000-00-00 00:00:00')?@$Item['banner_eff_end_date']:'';?>">
			                      <span class="input-group-btn">
			                        <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
			                      </span>
			                    </div>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="name" class="col-lg-3 control-label">客戶名稱</label>
			                  <div class="col-lg-8">
			                    <select name="Item[banner_customer_sn]" required class="form-control">
			                      <option value="">請選擇</option>
			                    <?php if($banner_customers){foreach($banner_customers as $key=>$_Item){ ?>
			                      <option value="<?php echo $_Item['banner_customer_sn'];?>" <?php echo (@$Item['banner_customer_sn']==$_Item['banner_customer_sn'])?'selected':'';?>><?php echo $_Item['banner_customer_name'];?></option>
			                    <?php }}?>
			                    </select>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="active" class="col-lg-3 control-label">頻道</label>
			                  <div class="col-lg-8">
			                   <select name="Item[published_locaiotn_type]" id="published_locaiotn_type" class="form-control">
			                      <option value="">請選擇</option>
			                    <?php if($published_locaiotn_types){foreach($published_locaiotn_types as $key=>$_Item){ ?>
			                      <option value="<?php echo $_Item['published_locaiotn_type'];?>" <?php echo (@$Item['published_locaiotn_type']==$_Item['published_locaiotn_type'])?'selected':'';?>><?php echo $_Item['published_locaiotn_type_name'];?></option>
			                    <?php }}?>
			                    </select>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="active" class="col-lg-3 control-label">版位</label>
			                  <div class="col-lg-8">
			                   <select name="Item[banner_location_sn]" id="banner_location_sn" class="form-control">
			                      <option value="">請先選擇頻道</option>
			                    <?php if($banner_locations){foreach($banner_locations as $key=>$_Item){ ?>
			                      <option value="<?php echo $_Item['banner_location_sn'];?>" <?php echo (@$Item['banner_location_sn']==$_Item['banner_location_sn'])?'selected':'';?>><?php echo $_Item['banner_location_name'];?></option>
			                    <?php }}?>
			                    </select>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="note" class="col-lg-3 control-label">敘述</label>
			                  <div class="col-lg-8">
			                    <textarea name="Item[banner_content_description]" class="form-control" rows="3"><?php echo @$Item['banner_content_description'];?></textarea>
			                  </div>
			                </div>
			                <!-- 型態的部分由版位選擇確定後鎖死 -->
			                <div class="form-group">
			                  <label for="active" class="col-lg-3 control-label">型態</label>
			                  <div class="col-lg-8">
			                    <select class="form-control" id="adType" disabled="">
			                      <option value="" selected>請選擇版位</option>
			                    <?php if($banner_types){foreach($banner_types as $key=>$_Item){ ?>
			                      <option banner_type_eng_name="<?php echo $_Item['banner_type_eng_name'];?>" value="<?php echo $_Item['banner_type'];?>" <?php echo (@$Item['banner_type']==$_Item['banner_type'])?'selected':'';?>><?php echo $_Item['banner_type_name'];?></option>
			                    <?php }}else{?>
			                      <option value="">請選擇版位</option>
			                    <?php }?>
			                      <!--option value="img" selected="">圖片</option>
			                      <option value="text">文字</option>
			                      <option value="html">HTML</option-->
			                    </select>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="name" class="col-lg-3 control-label">廣告內容</label>
			                  <div class="col-lg-8">
			                    <div class="adType imgAD">
			                      <input class="form-control" name="banner" type="file" id="banner">
				                    <?php if(@$Item['banner_content_save_dir'] && @$Item['banner_type_eng_name']=='img'){?>
					                    <a class="fancybox" rel="banner" title="<?php echo @$Item['blog_article_titile'];?>" href="public/uploads/<?php echo $Item['banner_content_save_dir'];?>"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['banner_content_save_dir'];?>" height="160" class="feature-img" style="vertical-align: bottom;"></a>
					                    <a href="javascript:void(0);" class="deleteTemplateDivider" banner_content_save_dir="<?php echo $Item['banner_content_save_dir'];?>" ItemId="<?php echo @$Item['banner_location_sn']; ?>"><i class="fa fa-trash-o"></i>刪除</a><br><br>
				                  	<?php }?>
			                    </div>
			                    <div class="adType textAD">
			                      <input type="text" name="textAD" class="form-control" placeholder="廣告顯示文字" value='<?php echo (@$Item['banner_type_eng_name']=='text')? @$Item['banner_content_save_dir']:'';?>'>
			                    </div>
			                    <div class="adType htmlAD">
			                      <textarea name="htmlAD" class="form-control" rows="3" placeholder="請貼入 HTML 代碼"><?php echo (@$Item['banner_type_eng_name']=='html')? @$Item['banner_content_save_dir']:'';?></textarea>
			                    </div>
			                  </div>
			                </div>
			                <!--div class="form-group">
			                  <label for="active" class="col-lg-3 control-label">連結類型</label>
			                  <div class="col-lg-8">
			                    <select name="Item[banner_link_type]" class="form-control">
			                    <?php if($link_types){foreach($link_types as $key=>$_Item){ ?>
			                      <option value="<?php echo $_Item['banner_link_type'];?>" <?php echo (@$Item['banner_link_type']==$_Item['banner_link_type'] || (!@$Item['banner_link_type'] && $_Item['default_flag']=='1'))?'selected':'';?>><?php echo $_Item['banner_link_type_name'];?></option>
			                    <?php }}?>
			                    </select>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="name" class="col-lg-3 control-label">廣告連結</label>
			                  <div class="col-lg-8">
			                    <input type="url" name="Item[banner_link]" class="form-control" placeholder="必須為 http:// 或 https:// 開頭的有效連結" value="<?php echo @$Item['banner_link'];?>">
			                  </div>
			                </div-->
			                <div class="form-group">
			                  <label class="col-md-3 control-label">產業屬性: </label>
			                  <div class="col-md-8">
			                    <div class="checkbox-list">
                             <?php
							       					$_member_domain_relation=explode("、",@$Item['domain_sn_set']);
							       					//var_dump($_member_domain_relation);
							                foreach($_biz_domain_list as $keys=>$values) 
							                {
							    							$_checked="";
							    							if(isset($_member_domain_relation))
							    							{
								    							foreach($_member_domain_relation as $ckey => $cvalue)
								    							{
								    									if($cvalue==$values->domain_name)
								    									$_checked=" checked";
								    							}
								    						}
							    							echo  '  <label class="checkbox-inline"><input type="checkbox" name="domain_sn_set[]" value="'.$values->domain_name.'" '.$_checked.'>'.$values->domain_name.'</label>';
															}
                						?>
 			                    </div>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="active" class="col-lg-3 control-label">View</label>
			                  <div class="col-lg-8">
			                    <span class="form-control-static"><?php echo @$Item['view_total_amount'];?></span>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="active" class="col-lg-3 control-label">Click</label>
			                  <div class="col-lg-8">
			                    <span class="form-control-static"><?php echo @$Item['click_total_amount'];?></span>
			                  </div>
			                </div>
				              <div class="form-group">
				                <label for="email" class="col-lg-3 control-label">排序</label>
				                <div class="col-lg-8">
				                  <input type="text" class="form-control" name="Item[sort_order]" value="<?php echo (@$Item['sort_order']) ? @$Item['sort_order']:'9999'; ?>" required>
				                </div>
				              </div>
			                <div class="form-group">
			                  <label for="active" class="col-lg-3 control-label">是否啟用 <span class="require">*</span></label>
			                  <div class="col-lg-8">
			                    <select name="Item[status]" class="form-control">
			                      <option value="1" <?php if(@$Item['status']=='1'){ ?>selected<?php }?>>是</option>
			                      <option value="2" <?php if(@$Item['status']=='2'){ ?>selected<?php }?>>否</option>
			                    </select>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="updater" class="col-lg-3 control-label">更新者</label>
			                  <div class="col-lg-8">
			                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['update_member_sn'])?@$Item['update_member_sn']:@$Item['create_member_sn']; ?>" Disabled>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="time-update" class="col-lg-3 control-label">更新時間</label>
			                  <div class="col-lg-8">
			                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['last_time_update']!='0000-00-00 00:00:00')?@$Item['last_time_update']:@$Item['create_date']; ?>" Disabled>
			                  </div>
			                </div>
			              </div>
			              <div class="form-actions">
			                <div class="row">
			                  <div class="col-md-offset-3 col-md-9">
			                    <input type="hidden" name="banner_content_sn" value="<?php echo @$Item['banner_content_sn']; ?>">
			                    <input type="hidden" id="banner_type" name="Item[banner_type]" value="<?php echo @$Item['banner_type']; ?>">
			                    <input type="hidden" name="banner_content_save_dir" value="<?php echo @$Item['banner_content_save_dir']; ?>">
			                    <input type="hidden" id="banner_type_eng_name" name="banner_type_eng_name" value="<?php echo @$Item['banner_type_eng_name']; ?>">
			                    <button type="submit" class="btn green">儲存</button>
			                    <button type="button" class="btn default">Cancel</button>
			                  </div>
			                </div>
			              </div>
			            </form>
                </div>
                <!-- End of Tab 資料設定 -->
                <!-- Tab 檔期紀錄 -->
                <div class="tab-pane" id="tab_schedule">

				      		<form class="form-horizontal" role="form" method="get" action="/admin/marketing/keywordItem/<?php echo @$Item['banner_content_sn']; ?>#tab_schedule">
				          <div class="portlet-body">
				            <div class="note note-warning">
				              <div class="input-group input-large date-picker input-daterange pull-left" data-date="2015-05-07" data-date-format="yyyy-mm-dd">
				                <span class="input-group-addon"> 期間 </span>
				                <input type="text" class="form-control input-sm daterange" name="from" value="<?php echo @$from;?>">
				                <span class="input-group-addon"> 到 </span>
				                <input type="text" class="form-control input-sm daterange" name="to" value="<?php echo @$to;?>">
				          			<input name="flg" type="hidden" value="">
				              </div>
				              <div class="table-actions-wrapper inline">
				                 &nbsp; <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button> &nbsp; 
				           		  <button id="reset_form" onclick="this.form.flg.value='reset';" class="btn btn-sm purple"><i class="fa"></i> 清除</button> &nbsp; 
				           		</div>
				              <div class="table-actions-wrapper pull-right">
				                <!--input type="text" class="pagination-panel-input form-control input-inline input-sm" size="12" style="margin: 0 5px;" placeholder="檔期名稱">
				                <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="12" style="margin: 0 5px;" placeholder="客戶名稱">
				                <select id="published_locaiotn_type" name="published_locaiotn_type" class="table-group-action-input form-control input-inline input-sm" style="font-size:11px;">
				                  <option value="">頻道</option>
				                    <?php if($published_locaiotn_types){foreach($published_locaiotn_types as $key=>$_Item){ ?>
				                      <option value="<?php echo $_Item['published_locaiotn_type'];?>" <?php echo ($published_locaiotn_type==$_Item['published_locaiotn_type'])?'selected':'';?>><?php echo $_Item['published_locaiotn_type_name'];?></option>
				                    <?php }}?>
				                </select>
				                <select id="banner_location_sn" name="banner_location_sn" class="table-group-action-input form-control input-inline input-sm" style="font-size:11px;">
				                  <option value="">版位</option>
				                    <?php if($banner_locations){foreach($banner_locations as $key=>$_Item){ ?>
				                      <option value="<?php echo $_Item['banner_location_sn'];?>" <?php echo (@$banner_location_sn==$_Item['banner_location_sn'])?'selected':'';?>><?php echo $_Item['banner_location_name'];?></option>
				                    <?php }}?>
				                </select-->
              					<a href="admin/marketing/adsScheduleItem/<?php echo @$Item['banner_content_sn']; ?>" class="btn green"><i class="fa fa-plus"></i> 新增</a>
				              </div>
				              <div style="clear:both;"></div>
				            </div>
				            <table class="table table-striped table-bordered table-hover" id="table_member">
				              <thead>
				                <tr>
				                  <th>編號</th>
				                  <th><small>起始時間~結束時間</small></th>
				                  <th>連結類型</th>
				                  <th>ga_code</th>
				                  <th>View</th>
				                  <th>Click</th>
				                  <th>啟用</th>
				                  <th>功能</th>
				                </tr>
				              </thead>
				              <tbody>
												<?php if($Items){ foreach($Items as $key=>$_Item){?>
				                  <tr class="odd gradeX">
				                    <td><?php echo $_Item['banner_schedule_sn'];?></td>
				                    <td><small><?php echo $_Item['banner_list_start_date'];?>~<br><?php echo $_Item['banner_list_end_date'];?></small></td>
				                    <td>
				                    	<?php if($_Item['banner_link_type_eng_name']=='search_keyword'){?>
					                    	<a href="shop/shopSearch?Search=<?php echo $_Item[$_Item['banner_link_type_eng_name']];?>" target="_blank">
				                    	<?php }elseif($_Item['banner_link_type_eng_name']=='link_product_sn'){?>
					                    	<a href="shop/shopItem/<?php echo $_Item[$_Item['banner_link_type_eng_name']];?>" target="_blank">
				                    	<?php }elseif($_Item['banner_link_type_eng_name']=='link_category_sn'){?>
					                    	<a href="shop/shopCatalog/<?php echo $_Item[$_Item['banner_link_type_eng_name']];?>" target="_blank">
				                    	<?php }elseif($_Item['banner_link_type_eng_name']=='banner_link'){?>
					                    	<a href="<?php echo $_Item[$_Item['banner_link_type_eng_name']];?>" target="_blank">
														  <?php }?>
					                    		<?php echo $_Item['banner_link_type_name'];?>
					                    	</a>
				                    </td>
														<td><?php echo $_Item['mkt_ga_code'];?></td>
				                    <td><?php echo $_Item['view_amount'];?></td>
				                    <td><?php echo $_Item['click_amount'];?></td>
				                    <td class="center">
				                      <?php if($_Item['status']=='1'):?>
				                        <i class="fa fa-check"></i>
				                      <?php else:?>
				                        <i class="fa fa-times"></i>
				                      <?php endif;?>
				                    </td>
				                    <td class="center">
				                      <a href="admin/marketing/adsScheduleItem/<?php echo $_Item['banner_content_sn'];?>/<?php echo $_Item['banner_schedule_sn'];?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
				                      <a href="admin/marketing/adsMaterialReport/<?php echo $_Item['banner_content_sn'];?>"><i class="fa fa-bar-chart"></i>報表</a> &nbsp;&nbsp;
				                      <a href="javascript:void(0);" class="deleteItem" ItemId="<?php echo $_Item['banner_schedule_sn'];?>"><i class="fa fa-trash-o"></i>刪除</a>
				                    </td>
				                  </tr>
				                <?php }}?>
				              </tbody>
				            </table>
				          </div>
				        </form>

                </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->