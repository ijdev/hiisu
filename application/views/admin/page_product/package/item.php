<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品規格 <small>規格組合</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/admin">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品規格
        <i class="fa fa-circle"></i>
      </li>
      <li>
         <a href="admin/product/productPackage">規格組合</a>
         <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         規格組合編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">規格組合</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/product/productPackage" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">規格組合名稱 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" name="package[product_package_name]" value="<?php echo @$Item['product_package_name']; ?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-md-3 control-label">館別</label>
                  <div class="col-md-8">
                    <!-- 選項在 table ij_config 中的 type = shop_menu -->
                    <?php echo $Store;?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">分類: </label>
                  <div class="col-md-8">
                    <div class="table-scrollable table-scrollable-borderless">
                  		<?php echo $categorys;?>
                      <!--span class="help-block"> 按住 control 進行複選 </span-->
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="module" class="col-lg-3 control-label">規格</label>
                  <div class="col-lg-8">
                    <div id="SpecList" class="checkbox-list form-md-line-input">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-3 control-label">備註</label>
                  <div class="col-lg-8">
                    <textarea name="package[description]" class="form-control" rows="3"><?php echo @$Item['description']; ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="seq" class="col-lg-3 control-label">排序 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" maxlength="4" name="package[sort_order]" value="<?php echo (@$Item['sort_order']) ? @$Item['sort_order']:'9999'; ?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">是否啟用 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <select name="package[status]" class="form-control" required>
                      <option value="1" <?php if(@$Item['status']){ ?>selected<?php }?>>是</option>
                      <option value="0" <?php if(@$Item['status']==='0'){ ?>selected<?php }?>>否</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-lg-3 control-label">更新者</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['update_member_sn'])?@$Item['update_member_sn']:@$Item['create_member_sn']; ?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-3 control-label">更新時間</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['last_time_update']!='0000-00-00 00:00:00')?@$Item['last_time_update']:@$Item['create_date']; ?>" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <input type="hidden" id="product_package_config_sn" name="product_package_config_sn" value="<?php echo @$Item['product_package_config_sn']; ?>">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="reset" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->