<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容
*/
?>
<div class="container">

<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/pages/css/inbox.css" rel="stylesheet" type="text/css"/>
<!-- Page level plugin styles END -->
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <!--<li>
      <a href="/member/notification">全站通知</a>
    </li>
    <li class="active">
      訊息內容
    </li>-->
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
   <!-- BEGIN SIDEBAR -->
 <div id="shopcetlog"  class=" padding_left0 sidebar col-md-3 col-sm-4">
     <div class="sidebar-filter categories-menu">
		<h3>商品分類</h3>
		<ul class="padding_left0 categories_list">
			<li class="categories_list_sub_li" data-toggle="collapse" data-target="#side_menu1">商品分類 1
				<ul id="side_menu1" class="collapse"> 
					<li><a href="/shop/shopCatalog/41">商品分類 1.1</a></li>
					<li><a href="/shop/shopCatalog/42">商品分類 1.2</a></li>
					<li><a href="/shop/shopCatalog/43">商品分類 1.3</a></li>
					<li><a href="/shop/shopCatalog/47">商品分類 1.4</a></li>
				</ul>
			</li>
			<li class="categories_list_sub_li" data-toggle="collapse" data-target="#side_menu2">商品分類 2
				<ul id="side_menu2" class="collapse"> 
					<li><a href="/shop/shopCatalog/42">商品分類 2.1</a></li>
					<li><a href="/shop/shopCatalog/47">商品分類 2.2</a></li>
					<li><a href="/shop/shopCatalog/41">商品分類 2.3</a></li>
					<li><a href="/shop/shopCatalog/43">商品分類 2.4</a></li>
				</ul>
			</li>
			<li class="categories_list_sub_lastli"><a href="/shop/shopCatalog/42">商品分類 3</a></li>
		</ul>
	 </div>
	 
	 <div class="sidebar-filter filter2">
      		
		<h3 class="filter-txt " >商品篩選
		<i id="icon-filter" data-toggle="collapse" data-target="#side_menu3"  onclick="myFunction3()"  class="fa fa-minus-square" ></i>
		
		<i id="icon-filter3" data-toggle="collapse" data-target="#side_menu3" onclick="myFunction4()"  class="fa fa-plus-square"  ></i>
		</h3>		
				<script>
		
					function myFunction3() {
						document.getElementById("icon-filter3").style.display = "block";
						document.getElementById("icon-filter").style.display = "none";
						}
					function myFunction4() {
    
						document.getElementById("icon-filter3").style.display = "none";
						document.getElementById("icon-filter").style.display = "block";
						}
				</script>

		<div class="inner_filter2 collapse in" id="side_menu3">
		 <script>
  $(document).ready(function(){
    $('.fdcheck').on('change', function(){
      var category_list = [];
      $('#filterschk :input:checked').each(function(){
        var category = $(this).val();
        category_list.push(category);
      });

      if(category_list.length == 0)
        $('.filterpro').fadeIn();
      else {
        $('.filterpro').each(function(){
          var item = $(this).attr('data-tag');
          if(jQuery.inArray(item,category_list) > -1)
            $(this).fadeIn('slow');
          else
            $(this).hide();
        });
      }   
    });
  }); 
  </script>
			<div id="filterschk" class="menu2_chk_box">
				
				<h4>分類(預設1)</h4>
				
					<div class="padding_left0 chk_box_lbl">
						<input  type="checkbox" id="tab1_check" class="fdcheck" value="基礎保養" name="cate1">
						<label  class="sphov" for="tab1_check"><span>基礎保養</span></label>
					</div>
					<div class=" padding_left0 chk_box_lbl">
						<input type="checkbox" id="tab2_check" class="fdcheck" value="加強護理" name="cate2">
						<label   class="sphov"for="tab2_check"><span>加強護理</span></label>
					</div>
					<div class="padding_left0 chk_box_lbl">
						<input type="checkbox" id="tab3_check" class="fdcheck" value="臉部其他" name="cate3">
						<label  class="sphov" for="tab3_check"><span>臉部其他</span></label>
					</div>
			</div>
		
	
		</div>
      </div>
    </div>

  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="col-md-9 col-sm-8">
    <div class="product-page">
      <h1 class="row">
        <div class="col-md-6 col-sm-6">
          <i class="fa fa-comment-o"></i> 最新消息
        </div>
        <form method="post" id="form1">
        <div class="col-md-6 col-sm-6">
          <div class="actions btn-set pull-right">
            <a class="nibtndlt" onclick="$('#form1').submit();"><i class="fa fa-trash-o"></i>  </a>
            <a href="member/notification" class="btn nibtnnoti"><i class="fa fa-list"></i> </a>
          </div>
            <input type='hidden' name='flg' id='flg' value='del'>
            <input type='hidden' name='member_message_center_sn[]' id='member_message_center_sn' value='<?php echo $message['member_message_center_sn'];?>'>
        </form>
        </div>
      </h1>
      <div class="portlet light">
        <div class="portlet-body">
          <div class="inbox-content">
            <div class="inbox-header inbox-view-header">
              <h3>網站啟用通知！</h3>
            </div>
            <div class="inbox-view-info">
              <div class="row">
                <div class="col-md-7">
                  <span class="bold"> 測試  </span>
                  on  2017-01-17 12:11:48
                </div>
              </div>
              <div class="inbox-view">
                您的網站已經設定完成，感謝您的支持～如有網站相關問題歡迎聯繫客服哦。
              </div>
            </div>
          </div>
          <!-- /.inbox-content -->
        </div>
      </div>
      <!-- END EXAMPLE TABLE PORTLET-->
    </div>
  </div>
  <!-- END CONTENT -->
</div>
</div>
<!-- END SIDEBAR & CONTENT -->