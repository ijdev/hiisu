<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Page 主架構
*/
?>
<div class="main-container">
  <div class="flexslider full-screen normal-nav">
      <ul class="slides">
<?php if($album_photos){ foreach($album_photos as $photo){?>
          <li style="background-image:url(<?php echo API_INVSITE.$photo['picture_save_dir'];?>);"></li>
<?php }}?>
          <!--li style="background-image:url(http://d39fioes0388et.cloudfront.net/public/img/gallery/052.jpg);"></li>
          <li style="background-image:url(http://d39fioes0388et.cloudfront.net/public/img/gallery/053.jpg);"></li>
          <li style="background-image:url(http://d39fioes0388et.cloudfront.net/public/img/gallery/054.jpg);"></li>
          <li style="background-image:url(http://d39fioes0388et.cloudfront.net/public/img/gallery/055.jpg);"></li-->
      </ul>
      <div class="nav">
          <a href="#" class="prev icon4-leftarrow23"></a>
          <a href="#" class="next icon4-chevrons"></a>
          <a href="#" class="pause icon4-pause49"></a>
          <a href="#" class="play icon4-play87"></a>
          <a href="#" class="full icon4-fullscreen6"></a>
      </div>
  </div>
</div>