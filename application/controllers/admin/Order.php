<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Order extends MY_Controller {
 	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->library('sean_lib/Sean_form_general');
		$this->load->model('general/My_general_ct');
		$this->load->config('ij_config');
		$this->load->config('ij_sys_config');
		$this->load->model('sean_models/Sean_general');
		$this->load->model('sean_models/Sean_db_tools');
		$this->load->model('libraries_model');
		$this->load->helper('cookie');
		$this->page_data['unreadmessage']= $this->libraries_model->get_unread_message_count();
		$this->page_data['init_control']="admin/login/";
	}
	public function index(){
		$this->orderTable();
	}
	/**
	 * 訂單列表 Template
	 *
	 */
	public function orderTable()
	{
		$this->libraries_model->auto_update_orders();//自動異動訂單
		if($data_array=$this->input->get('orderTable',true)){
			//var_dump($data_array);
		}
		$page_data = array();
		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_order/list/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_content']['ccapply'] = $this->libraries_model->_select('ij_ccapply_code_ct',array('display_flag'=>'1'),0,0,0,'ccapply_code','asc','result_array');
		$page_data['page_content']['orders'] = $this->libraries_model->get_orders($data_array);
		unset($page_data['page_content']['orders']['total']);
		//$Store=$this->libraries_model->get_channels();
		//$page_data['page_content']['Store']=form_dropdown('orderTable[channel_name]',$Store,@$data_array['channel_name'],'id="channel_name" class="table-group-action-input form-control input-inline input-sm" style="font-size:11px;"');
		$Supplier=$this->libraries_model->_select('ij_supplier',array('supplier_status'=>'1'),0,0,0,'supplier_sn',0,'list','supplier_sn,supplier_name');
		//var_dump($Supplier);
		$Supplier[0]='供應商';
		//array_splice($Supplier, 0, 0);
		if(@$data_array['supplier_sn']) {
			$supplier_sn=$data_array['supplier_sn'];
		}else{
			$supplier_sn='0';
		}
		$page_data['page_content']['Supplier']=form_dropdown('orderTable[supplier_sn]',$Supplier,$supplier_sn,'id="supplier_sn" class="table-group-action-input form-control input-inline input-sm" style="font-size:11px;"');
		$page_data['page_content']['sub_order_status']=$this->libraries_model->_select('ij_sub_order_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array','sub_order_status,sub_order_status_name');
		$page_data['page_content']['payment_status']=$this->libraries_model->_select('ij_payment_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array','payment_status,payment_status_name');
		$page_data['page_content']['admin_users']=$this->libraries_model->_get_admin_users();
		$page_data['page_content']['data_array']=$data_array;

		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_order/list/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 訂單編輯 Template
	 *
	 */
	public function orderItem($sub_order_sn,$ifprint='')
	{
		if($sub_order_sn && $this->input->get('set_invoice',true)=='yes' && $this->input->is_ajax_request()){
			$this->load->library('ezpay_invoice');
			$page_data['page_content']['order'] = $this->libraries_model->get_orders(array('sub_order_sn'=>$sub_order_sn),1);
			$result=$this->ezpay_invoice->set_invoice($page_data['page_content']['order'][0],0);//立即開立
			//var_dump($page_data['page_content']['order'][0]);
			if($result['Status']=='SUCCESS'){
				echo json_encode('ok');
			}else{
				echo json_encode($result['Message']);
			}
			exit;
		}
		if($data_array=$this->input->post('order',true)){
			$this->Check_page_permission('edit','修改');
			//var_dump($data_array);
			if($this->input->post('order_sn',true)){
				$data_array['receiver_addr_city']=$this->libraries_model->_select('ij_city_ct','city_code',$data_array['receiver_addr_city'],0,0,0,0,'row','city_name')['city_name'];
				$data_array['receiver_addr_town']=$this->libraries_model->_select('ij_town_ct','town_code',$data_array['receiver_addr_town'],0,0,0,0,'row','town_name')['town_name'];
	        //紀錄log
					/*$_trans_log["_table"]='ij_order';
					//$_trans_log["_before"]='更新收件人資料:::'.implode(':',$this->libraries_model->_select('ij_order','order_sn',$this->input->post('order_sn',true),0,0,'order_sn',0,'row'));
					$_trans_log["_before"]='更新收件人資料:::'.implode(':::',$this->input->post('old_data_order',true));
					$_trans_log["_after"]='更新收件人資料:::'.implode(':::',$data_array);
					$_trans_log["_type"]="0";
					$_trans_log["_key_id"]=$this->input->post('order_sn',true);
					$this->libraries_model->trans_log($_trans_log);*/
				$this->libraries_model->_update('ij_order',$data_array,'order_sn',$this->input->post('order_sn',true));
				$this->session->set_flashdata("success_message",'更新收件人資料成功！');
				redirect(base_url('admin/order/orderItem/'.$sub_order_sn));
			}
		}
		if($data_array=$this->input->post('invoice',true)){//發票資料編輯
			if(!@$data_array['invoice_receiver_middle_name']){ //沒序號才能改
				if(isset($data_array['invoice']) && isset($data_array['tax_ID'])){	//有抬頭、統編
					$invoice = $data_array['invoice'];
					$tax_ID = $data_array['tax_ID'];
				}
				else{
					$invoice = "";
					$tax_ID = "";
				}
				$invoice_array = array();
				$invoice_array['invoice_num'] = $data_array['invoice_num'];//暫時可以輸入
				if(@$data_array['invoice_time']){
					$invoice_array['invoice_time'] = $data_array['invoice_time'];//暫時可以輸入
				}
				//var_dump($invoice_array);exit;
				//$invoice_array['open_invoice_type'] = $data_array['invoice_formats'];	//二聯or三聯
				$invoice_array['invoice_title'] = $invoice;				//抬頭
				$invoice_array['company_tax_id'] = $tax_ID;				//統編
				$invoice_array['invoice_receiver_zipcode'] = $data_array['invoice_receiver_zipcode'];	//區號
				$invoice_array['invoice_receiver_addr_city'] =$this->libraries_model->_select('ij_city_ct','city_code',$data_array['invoice_receiver_addr_city'],0,0,0,0,'row','city_name')['city_name'];	//縣市
				$invoice_array['invoice_receiver_addr_town'] = $this->libraries_model->_select('ij_town_ct','town_code',$data_array['invoice_receiver_addr_town'],0,0,0,0,'row','town_name')['town_name'];	//縣市
				$invoice_array['invoice_receiver_addr1'] = $data_array['invoice_receiver_addr1'];	//地址
				//$invoice_array['invoice_carrie_type'] = $data_array['invoice_carrie'];	//隨貨、另寄、自取
				//$invoice_array['email_for_eletronic_invoce'] = $data_array['email_for_eletronic_invoce'];	//載具編號
				//$invoice_array['invoice_donor'] = $data_array['invoice_donor_ct'];	//捐贈單位
			}else{
				//$invoice_array['invoice_num'] = $data_array['invoice_num'];//暫時可以輸入
				//if(@$data_array['invoice_time']){
				//	$invoice_array['invoice_time'] = $data_array['invoice_time'];//暫時可以輸入
				//}
				$invoice_array['invoice_receiver_zipcode'] = $data_array['invoice_receiver_zipcode'];	//區號
				$invoice_array['invoice_receiver_addr_city'] =$this->libraries_model->_select('ij_city_ct','city_code',$data_array['invoice_receiver_addr_city'],0,0,0,0,'row','city_name')['city_name'];	//縣市
				$invoice_array['invoice_receiver_addr_town'] = $this->libraries_model->_select('ij_town_ct','town_code',$data_array['invoice_receiver_addr_town'],0,0,0,0,'row','town_name')['town_name'];
				$invoice_array['invoice_receiver_addr1'] = $data_array['invoice_receiver_addr1'];	//地址
			}
			$this->libraries_model->_update('ij_order',$invoice_array, array("order_num"=>$data_array['order_num']));
			$this->session->set_flashdata("success_message",'更新發票資料成功！');
			redirect(base_url('admin/order/orderItem/'.$sub_order_sn));
		}
		//var_dump($this->input->post());exit;
		if($this->input->post('order2',true) || $this->input->post('sub_order')){
			$this->Check_page_permission('edit','修改');
			if($this->input->post('order_sn',true)){
				$old_data=$this->libraries_model->_select('ij_order','order_sn',$this->input->post('order_sn',true),0,0,'order_sn',0,'row');
			    $sub_old_data=$this->libraries_model->_select('ij_sub_order',array('sub_order_sn'=>$this->input->post('sub_order_sn',true)),0,0,0,'sub_order_sn',0,'row','delivery_status,shipping_charge_amount,pay_status,associated_member_sn,sub_order_num');
			    $delivery_status=$sub_old_data['delivery_status'];
				$data_array2['delivery_status']=$this->input->post('sub_order[delivery_status]',true);
        		//加入供應商、經銷商機制
	            switch(@$this->session->userdata('member_role_system_rules')[0]['role_name']){
	            case '經銷商管理員':
					$this->session->set_flashdata("success_message",'很抱歉！經銷商無此權限。');
	            break;
	            case '供應商管理員':
					if($this->input->post('sub_order_sn',true)){
						if($data_array2['delivery_status']=='3'){ //已出貨
							$data_array2['designated_delivery_date']=date('Y-m-d H:i:s');
		                    $this->ijw->send_mail('商品已出貨','',$sub_old_data['associated_member_sn'],'',$sub_old_data['sub_order_num']);

						//已付款、退訂審核中=>取消訂單
						}elseif($delivery_status=='8' && @$data_array2['delivery_status']=='9' && $sub_old_data['pay_status']=='2'){ //取消配送+取消訂單
							if(!$this->libraries_model->if_card($old_data['payment_method'])){ //非刷卡已取消退購物金
								$sub_sum=$this->libraries_model->_select('ij_order_item','sub_order_sn',$this->input->post('sub_order_sn',true),0,0,'order_item_sn',0,'row','sum(actual_sales_amount) as sub_sum')['sub_sum']+$sub_old_data['shipping_charge_amount'];
								$cash_array['associated_order_sn']= $this->input->post('order_sn',true);
								$cash_array['associated_member_sn']= $old_data['member_sn'];
								$cash_array['wedding_website_sn']= $this->input->post('sub_order_sn',true);
								$cash_array['cash_amount']= $sub_sum;
								$cash_array['memo']= '訂單取消系統自動轉購物金';
								$cash_array['status']= '1';
								$cash_array['cash_type']= '1'; //退款購物金
								if($gift_cash_sn=@$this->libraries_model->_select('ij_gift_cash',array('wedding_website_sn'=>$this->input->post('sub_order_sn',true),'status'=>'1'),0,0,0,0,0,'row','gift_cash_sn')['gift_cash_sn']){
									$cash_array = $this->libraries_model->CheckUpdate($cash_array,0);
									$this->libraries_model->_update('ij_gift_cash',$cash_array,'gift_cash_sn',$gift_cash_sn);
								}else{
									$cash_array = $this->libraries_model->CheckUpdate($cash_array,1);
									$gift_cash_sn=$this->libraries_model->_insert('ij_gift_cash',$cash_array);
								}
								if($gift_cash_sn){
									$this->session->set_flashdata("success_message",'取消配送-取消訂單系統自動轉購物金');
								}else{
									$this->session->set_flashdata("success_message",'取消配送-取消訂單系統自動轉購物金失敗');
								}
								$data_array['payment_status']='9'; //已轉購物金
								$data_array2['pay_status']='9'; //已轉購物金
								$data_array2['checkstatus']='3'; //供應商出帳狀態已轉購物金
								$data_array['original_order_sn']='3'; //聯盟會員出帳狀態已轉購物金
								$data_array2['free_shipping_flag']='1'; //退運費
							}else{
								$data_array['payment_status']='7'; //待刷退
								$data_array2['pay_status']='7'; //待刷退
								$this->session->set_flashdata("success_message",'取消配送-取消訂單付款狀態已改待刷退');
							}
							$this->libraries_model->_update('ij_order',$data_array,'order_sn',$this->input->post('order_sn',true));
							$data_array2['sub_order_status']='2';
						}elseif(($delivery_status=='5' || $delivery_status=='6' || $delivery_status=='7') && @$data_array2['delivery_status']=='9' && $sub_old_data['pay_status']=='2'){ //已退貨退部份購物金
							$sub_sum=$this->libraries_model->_select('ij_order_cancel_log','associated_sub_order_sn',$this->input->post('sub_order_sn',true),0,0,'order_item_sn',0,'row','sum(return_amount) as sub_sum')['sub_sum'];
							$total_sub_sum=$this->libraries_model->_select('ij_order_item','sub_order_sn',$this->input->post('sub_order_sn',true),0,0,'order_item_sn',0,'row','sum(actual_sales_amount) as sub_sum')['sub_sum'];
							if(!$this->libraries_model->if_card($old_data['payment_method'])){ //非刷卡已取消退購物金
								if($sub_sum==$total_sub_sum){//全退
									$data_array2['sub_order_status']='2';
									$cash_array['memo']= '訂單退貨系統自動轉購物金';
									$data_array2['checkstatus']='3'; //供應商出帳狀態已轉購物金
									$data_array['original_order_sn']='3'; //聯盟會員出帳狀態已轉購物金
								}else{
									$data_array2['sub_order_status']='8';//部份退貨
									$cash_array['memo']= '訂單部份退貨系統自動轉購物金';
									$data_array2['checkstatus']='1'; //供應商出帳狀態待結帳
									$data_array['original_order_sn']='1'; //聯盟會員出帳狀態待結帳
								}
								$cash_array['associated_order_sn']= $this->input->post('order_sn',true);
								$cash_array['associated_member_sn']= $old_data['member_sn'];
								$cash_array['wedding_website_sn']= $this->input->post('sub_order_sn',true);
								$cash_array['cash_amount']= $sub_sum;
								$cash_array['cash_type']= '1'; //退款購物金
								$cash_array['status']= '1';
								if($gift_cash_sn=@$this->libraries_model->_select('ij_gift_cash',array('wedding_website_sn'=>$this->input->post('sub_order_sn',true),'status'=>'1'),0,0,0,0,0,'row','gift_cash_sn')['gift_cash_sn']){
									$cash_array = $this->libraries_model->CheckUpdate($cash_array,0);
									$this->libraries_model->_update('ij_gift_cash',$cash_array,'gift_cash_sn',$gift_cash_sn);
								}else{
									$cash_array = $this->libraries_model->CheckUpdate($cash_array,1);
									$gift_cash_sn=$this->libraries_model->_insert('ij_gift_cash',$cash_array);
								}
								if($gift_cash_sn){
									$this->session->set_flashdata("success_message",'取消訂單系統自動轉購物金');
								}else{
									$this->session->set_flashdata("success_message",'取消訂單自動轉購物金失敗');
								}
								$data_array2['checkstatus']='3'; //供應商出帳狀態已轉購物金
								$data_array['original_order_sn']='3'; //聯盟會員出帳狀態已轉購物金
								$data_array2['pay_status']='9'; //已轉購物金
								$data_array['payment_status']='9'; //已轉購物金
							}else{
								$data_array['payment_status']='7'; //待刷退
								$data_array2['pay_status']='7'; //待刷退
								if($sub_sum==$total_sub_sum){//全退
									$data_array2['sub_order_status']='2';
									$data_array2['checkstatus']='3'; //供應商出帳狀態已轉購物金
									$data_array['original_order_sn']='3'; //聯盟會員出帳狀態已轉購物金
								}else{
									$data_array2['sub_order_status']='8';//部份退貨
									$data_array2['checkstatus']='1'; //供應商出帳狀態待結帳
									$data_array['original_order_sn']='1'; //聯盟會員出帳狀態待結帳
								}
								$this->session->set_flashdata("success_message",'取消訂單付款狀態已改待刷退');
							}
							if($data_array2['pay_status']) $data_array['payment_status']=$data_array2['pay_status'];
							$this->libraries_model->_update('ij_order',$data_array,'order_sn',$this->input->post('order_sn',true));
						//}elseif(($data_array2['sub_order_status']=='5' || $data_array2['sub_order_status']=='8') && $data_array2['pay_status']=='2'){
						//	$data_array2['checkstatus']='1'; //供應商出帳狀態待結帳
						//	$data_array['original_order_sn']='1'; //聯盟會員出帳狀態待結帳
						//	$this->libraries_model->_update('ij_order',$data_array,'order_sn',$this->input->post('order_sn',true));
						}
						$data_array2['logistics_ticket_num']=$this->input->post('sub_order[logistics_ticket_num]',true);
						//var_dump($data_array2);exit;
						$this->libraries_model->_update('ij_sub_order',$data_array2,'sub_order_sn',$this->input->post('sub_order_sn',true));
						unset($data_array2['logistics_ticket_num']);
		        		//紀錄log
						/*$_trans_log["_table"]='ij_sub_order';
					    $_trans_log["_before"]='運送狀態('.$this->input->post('old_data',true)['delivery_status_name'].')';
						$data_array2['delivery_status']=$this->libraries_model->_select('ij_delivery_status_ct','delivery_status',$data_array2['delivery_status'],0,0,0,0,'row','delivery_status_name')['delivery_status_name'];
					    $_trans_log["_after"]='運送狀態('.$data_array2['delivery_status'].')';
						$_trans_log["_type"]="0";
						$_trans_log["_key_id"]=$this->input->post('sub_order_sn',true);
						$this->libraries_model->trans_log($_trans_log);*/
						$this->session->set_flashdata("success_message",'更新訂單配送狀態成功！');
					}
	            break;
	            default:
					$data_array=$this->input->post('order2',true);
					//$old_data=$this->libraries_model->_select('ij_order','order_sn',$this->input->post('order_sn',true),0,0,'order_sn',0,'row');
				    //$sub_old_data=$this->libraries_model->_select('ij_sub_order',array('sub_order_sn'=>$this->input->post('sub_order_sn',true)),0,0,0,'sub_order_sn',0,'row','delivery_status,shipping_charge_amount,associated_member_sn,sub_order_num,pay_status');
				    //$delivery_status=$sub_old_data['delivery_status'];
					//$payment_status_name=$this->libraries_model->_select('ij_payment_status_ct','payment_status',$old_data['payment_status'],0,0,0,0,'row','payment_status_name')['payment_status_name'];
	        		//var_dump($data_array);
	        		if($data_array['payment_status']=='2'){
	        			$data_array['payment_done_date']=date('Y-m-d H:i:s');
	        		}
	        		if(is_array($data_array))	$this->libraries_model->_update('ij_order',$data_array,'order_sn',$this->input->post('order_sn',true));
	        		//寄通知信
					$data_array2=$this->input->post('sub_order',true);
	        		if($data_array2['pay_status']=='2' && !@$data_array2['delivery_status'] && $data_array2['sub_order_status']=='1'){
	        			$this->libraries_model->send_order_mail($this->input->post('order_sn',true));
	        			unset($data_array2['delivery_status']);
	        		}
					if($this->input->post('sub_order_sn',true)){
						$this->session->set_flashdata("success_message",'更新訂單狀態成功！');
						//var_dump($delivery_status);
						//var_dump($data_array2['delivery_status']);
						//var_dump($old_data['payment_status']);exit;
						if($data_array2['sub_order_status']=='1' && @$data_array2['delivery_status']=='3'){ //已出貨
							$data_array2['designated_delivery_date']=date('Y-m-d H:i:s');
		                    $this->ijw->send_mail('商品已出貨','',$sub_old_data['associated_member_sn'],'',$sub_old_data['sub_order_num']);
						}elseif($delivery_status=='8' && @$data_array2['delivery_status']=='9' && $sub_old_data['pay_status']=='2'){ //取消配送+取消訂單
							if(!$this->libraries_model->if_card($old_data['payment_method'])){ //非刷卡已取消退購物金
								$sub_sum=$this->libraries_model->_select('ij_order_item','sub_order_sn',$this->input->post('sub_order_sn',true),0,0,'order_item_sn',0,'row','sum(actual_sales_amount) as sub_sum')['sub_sum']+$sub_old_data['shipping_charge_amount'];
								$cash_array['associated_order_sn']= $this->input->post('order_sn',true);
								$cash_array['associated_member_sn']= $old_data['member_sn'];
								$cash_array['wedding_website_sn']= $this->input->post('sub_order_sn',true);
								$cash_array['cash_amount']= $sub_sum;
								$cash_array['memo']= '訂單取消系統自動轉購物金';
								$cash_array['status']= '1';
								$cash_array['cash_type']= '1'; //退款購物金
								if($gift_cash_sn=@$this->libraries_model->_select('ij_gift_cash',array('wedding_website_sn'=>$this->input->post('sub_order_sn',true),'status'=>'1'),0,0,0,0,0,'row','gift_cash_sn')['gift_cash_sn']){
									$cash_array = $this->libraries_model->CheckUpdate($cash_array,0);
									$this->libraries_model->_update('ij_gift_cash',$cash_array,'gift_cash_sn',$gift_cash_sn);
								}else{
									$cash_array = $this->libraries_model->CheckUpdate($cash_array,1);
									$gift_cash_sn=$this->libraries_model->_insert('ij_gift_cash',$cash_array);
								}
								if($gift_cash_sn){
									$this->session->set_flashdata("success_message",'取消配送-取消訂單系統自動轉購物金');
								}else{
									$this->session->set_flashdata("success_message",'取消配送-取消訂單系統自動轉購物金失敗');
								}
								$data_array['payment_status']='9'; //已轉購物金
								$data_array2['pay_status']='9'; //已轉購物金
								$data_array2['checkstatus']='3'; //供應商出帳狀態已轉購物金
								$data_array['original_order_sn']='3'; //聯盟會員出帳狀態已轉購物金
								$data_array2['free_shipping_flag']='1'; //退運費
							}else{
								$data_array['payment_status']='7'; //待刷退
								$data_array2['pay_status']='7'; //待刷退
								$this->session->set_flashdata("success_message",'取消配送-取消訂單付款狀態已改待刷退');
							}
							$this->libraries_model->_update('ij_order',$data_array,'order_sn',$this->input->post('order_sn',true));
							$data_array2['sub_order_status']='2';
						}elseif(($delivery_status=='5' || $delivery_status=='6' || $delivery_status=='7') && @$data_array2['delivery_status']=='9' && $sub_old_data['pay_status']=='2'){ //已退貨退部份購物金
							$sub_sum=$this->libraries_model->_select('ij_order_cancel_log','associated_sub_order_sn',$this->input->post('sub_order_sn',true),0,0,'order_item_sn',0,'row','sum(return_amount) as sub_sum')['sub_sum'];
							$total_sub_sum=$this->libraries_model->_select('ij_order_item','sub_order_sn',$this->input->post('sub_order_sn',true),0,0,'order_item_sn',0,'row','sum(actual_sales_amount) as sub_sum')['sub_sum'];
							if(!$this->libraries_model->if_card($old_data['payment_method'])){ //非刷卡已取消退購物金
								if($sub_sum==$total_sub_sum){//全退
									$data_array2['sub_order_status']='2';
									$cash_array['memo']= '訂單退貨系統自動轉購物金';
									$data_array2['checkstatus']='3'; //供應商出帳狀態已轉購物金
									$data_array['original_order_sn']='3'; //聯盟會員出帳狀態已轉購物金
								}else{
									$data_array2['sub_order_status']='8';//部份退貨
									$cash_array['memo']= '訂單部份退貨系統自動轉購物金';
									$data_array2['checkstatus']='1'; //供應商出帳狀態待結帳
									$data_array['original_order_sn']='1'; //聯盟會員出帳狀態待結帳
								}
								$cash_array['associated_order_sn']= $this->input->post('order_sn',true);
								$cash_array['associated_member_sn']= $old_data['member_sn'];
								$cash_array['wedding_website_sn']= $this->input->post('sub_order_sn',true);
								$cash_array['cash_amount']= $sub_sum;
								$cash_array['cash_type']= '1'; //退款購物金
								$cash_array['status']= '1';
								if($gift_cash_sn=@$this->libraries_model->_select('ij_gift_cash',array('wedding_website_sn'=>$this->input->post('sub_order_sn',true),'status'=>'1'),0,0,0,0,0,'row','gift_cash_sn')['gift_cash_sn']){
									$cash_array = $this->libraries_model->CheckUpdate($cash_array,0);
									$this->libraries_model->_update('ij_gift_cash',$cash_array,'gift_cash_sn',$gift_cash_sn);
								}else{
									$cash_array = $this->libraries_model->CheckUpdate($cash_array,1);
									$gift_cash_sn=$this->libraries_model->_insert('ij_gift_cash',$cash_array);
								}
								if($gift_cash_sn){
									$this->session->set_flashdata("success_message",'取消訂單系統自動轉購物金');
								}else{
									$this->session->set_flashdata("success_message",'取消訂單自動轉購物金失敗');
								}

								$data_array2['pay_status']='9'; //已轉購物金
								$data_array['payment_status']='9'; //已轉購物金
							}else{
								$data_array['payment_status']='7'; //待刷退
								$data_array2['pay_status']='7'; //待刷退
								if($sub_sum==$total_sub_sum){//全退
									$data_array2['sub_order_status']='2';
									$data_array2['checkstatus']='3'; //供應商出帳狀態已轉購物金
									$data_array['original_order_sn']='3'; //聯盟會員出帳狀態已轉購物金
								}else{
									$data_array2['sub_order_status']='8';//部份退貨
									$data_array2['checkstatus']='1'; //供應商出帳狀態待結帳
									$data_array['original_order_sn']='1'; //聯盟會員出帳狀態待結帳
								}
								$this->session->set_flashdata("success_message",'取消訂單付款狀態已改待刷退');
							}
							if($data_array2['pay_status']) $data_array['payment_status']=$data_array2['pay_status'];
							$this->libraries_model->_update('ij_order',$data_array,'order_sn',$this->input->post('order_sn',true));
						}elseif(($data_array2['sub_order_status']=='5' || $data_array2['sub_order_status']=='8') && $data_array2['pay_status']=='2'){
							//部份退貨或訂單完成
							$data_array2['checkstatus']='1'; //供應商出帳狀態待結帳
							$data_array['original_order_sn']='1'; //聯盟會員出帳狀態待結帳
							$this->libraries_model->_update('ij_order',$data_array,'order_sn',$this->input->post('order_sn',true));
						}
						//var_dump($data_array2);exit;
						$this->libraries_model->_update('ij_sub_order',$data_array2,'sub_order_sn',$this->input->post('sub_order_sn',true));
					}
	            break;
	            }
				redirect(base_url('admin/order/orderItem/'.$sub_order_sn));
			}
		}
		if($sub_order_sn){
			$this->Check_page_permission('search','查詢');
			$page_data = array();
			// 內容設定
			// general view setting
			$page_data['general_header'] = array(); // header 可以透過這個參數帶入
			$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
			if($ifprint=='print'){
				$page_data['page_content']['view_path'] = 'admin/page_order/list/print'; // 頁面主內容 view 位置, 必要
			}else{
				$page_data['page_content']['view_path'] = 'admin/page_order/list/item'; // print
			}
			$order_data = $this->libraries_model->get_orders(array('sub_order_sn'=>$sub_order_sn),1);
			//echo '<pre>' . var_export($page_data['page_content']['order'][0], true) . '</pre>';
        	//echo '<pre>' . var_export(json_decode($page_data['page_content']['order'][0]['invoice_aReturn_Info'],true), true) . '</pre>';

			/*if(!$page_data['page_content']['order'][0]['invoice_num'] && $page_data['page_content']['order'][0]['payment_status']=='2'){
				$this->load->library('ezpay_invoice');
				$result=$this->ezpay_invoice->set_invoice($page_data['page_content']['order'][0],INVOICE_DELAY_DAYS);
				//echo '<pre>' . var_export($result, true) . '</pre>';
        		if($result['Status']!='SUCCESS' && $result['Status']!='LIB10003'){
        			$page_data['page_content']['order'][0]['Message']=$result['Message'];
        		}elseif($result['Status']=='SUCCESS'){
					//echo '<pre>' . var_export($result, true) . '</pre>';
        			$invoice_data=(is_array($result['Result']))?$result['Result']:json_decode($result['Result'],true);
        			$page_data['page_content']['order'][0]['invoice_aReturn_Info']=json_encode($invoice_data);
                    //$_order=array();
                    if($invoice_data['InvoiceNumber']){
                    	$_order['invoice_num'] = $invoice_data['InvoiceNumber'];
        				$page_data['page_content']['order'][0]['invoice_num']=$invoice_data['InvoiceNumber'];
                    }
                    if($invoice_data['RandomNum']){
                    	$_order['invoice_receiver_first_name'] = $invoice_data['RandomNum'];
        				$page_data['page_content']['order'][0]['invoice_receiver_first_name']=$invoice_data['RandomNum'];
                    }
                    if($invoice_data['CreateTime']){
                    	$_order['invoice_time'] = $invoice_data['CreateTime'];
        				$page_data['page_content']['order'][0]['invoice_time']=$invoice_data['CreateTime'];
                    }
                    if($invoice_data['InvoiceTransNo']) $_order['invoice_receiver_middle_name'] = $invoice_data['InvoiceTransNo'];
                    $_order['invoice_aReturn_Info'] = $result['Result'];
                    //$this->libraries_model->_update('ij_order',$_order,'order_sn',$page_data['page_content']['order'][0]['order_sn']);
        		}
			}*/
			$page_data['page_content']['buyer_city_code']=$this->libraries_model->_select('ij_city_ct','city_name',$order_data[0]['buyer_addr_city'],0,0,0,0,'row','city_code')['city_code'];
			$page_data['page_content']['buyer_town_code']=$this->libraries_model->_select('ij_town_ct','town_name',$order_data[0]['buyer_addr_town'],0,0,0,0,'row','town_code')['town_code'];

			$page_data['page_content']['receiver_addr_city_code']=$this->libraries_model->_select('ij_city_ct','city_name',$order_data[0]['receiver_addr_city'],0,0,0,0,'row','city_code')['city_code'];
			$page_data['page_content']['receiver_addr_town_code']=$this->libraries_model->_select('ij_town_ct','town_name',$order_data[0]['receiver_addr_town'],0,0,0,0,'row','town_code')['town_code'];
			$page_data['page_content']['invoice_receiver_addr_city_code']=$this->libraries_model->_select('ij_city_ct','city_name',$order_data[0]['invoice_receiver_addr_city'],0,0,0,0,'row','city_code')['city_code'];
			$page_data['page_content']['invoice_receiver_addr_town_code']=$this->libraries_model->_select('ij_town_ct','town_name',$order_data[0]['invoice_receiver_addr_town'],0,0,0,0,'row','town_code')['town_code'];
			if(!$page_data['page_content']['invoice_receiver_addr_city_code']) $page_data['page_content']['invoice_receiver_addr_city_code']=$page_data['page_content']['buyer_city_code'];
			if(!$page_data['page_content']['invoice_receiver_addr_town_code']) $page_data['page_content']['invoice_receiver_addr_town_code']=$page_data['page_content']['buyer_town_code'];

			//參數
			$page_data['page_content']['city_ct'] = $this->libraries_model->_select('ij_city_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','city_code,city_name');
			$page_data['page_content']['town_ct'] = $this->libraries_model->_select('ij_town_ct',array('display_flag'=>'1','city_code'=>$page_data['page_content']['invoice_receiver_addr_city_code']),0,0,0,0,0,'list','town_code,town_name');

			$page_data['page_content']['payment_status'] = $this->libraries_model->_select('ij_payment_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','payment_status,payment_status_name');
			$page_data['page_content']['sub_order_status'] = $this->libraries_model->_select('ij_sub_order_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','sub_order_status,sub_order_status_name');
			$page_data['page_content']['delivery_status'] = $this->libraries_model->_select('ij_delivery_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','delivery_status,delivery_status_name');
			if($order_data[0]['delivery_status']=='8'){
				unset($page_data['page_content']['delivery_status'][2]);
				unset($page_data['page_content']['delivery_status'][3]);
				unset($page_data['page_content']['delivery_status'][4]);
				unset($page_data['page_content']['delivery_status'][5]);
				unset($page_data['page_content']['delivery_status'][6]);
				unset($page_data['page_content']['delivery_status'][7]);
			}elseif($order_data[0]['delivery_status']=='5'){
				unset($page_data['page_content']['delivery_status'][2]);
				unset($page_data['page_content']['delivery_status'][3]);
				unset($page_data['page_content']['delivery_status'][4]);
				unset($page_data['page_content']['delivery_status'][8]);
			}elseif($order_data[0]['delivery_status']=='1' || $order_data[0]['delivery_status']=='2' || $order_data[0]['delivery_status']=='3' || $order_data[0]['delivery_status']=='4'){
				unset($page_data['page_content']['delivery_status'][5]);
				unset($page_data['page_content']['delivery_status'][6]);
				unset($page_data['page_content']['delivery_status'][7]);
				unset($page_data['page_content']['delivery_status'][8]);
				unset($page_data['page_content']['delivery_status'][9]);
			}else{
				unset($page_data['page_content']['delivery_status'][2]);
				unset($page_data['page_content']['delivery_status'][3]);
				unset($page_data['page_content']['delivery_status'][4]);
				unset($page_data['page_content']['delivery_status'][5]);
				unset($page_data['page_content']['delivery_status'][6]);
				unset($page_data['page_content']['delivery_status'][7]);
				unset($page_data['page_content']['delivery_status'][8]);
			}
			$page_data['page_content']['order'] = $order_data;
			$page_data['page_content']['admin_users']=$this->libraries_model->_get_admin_users();
			$page_data['page_content']['ccapply'] = $this->libraries_model->_select('ij_ccapply_code_ct',array('display_flag'=>'1'),0,0,0,'ccapply_code','asc','result_array');

			$page_data['page_content']['question_qna'] = $this->libraries_model->_get_question_qna_log(@$page_data['page_content']['order'][0]['member_question_sn']);
			$page_data['page_content']['trans_log'] = $this->libraries_model->_get_trans_log('/admin/order/orderItem/'.$sub_order_sn);
			$page_data['page_content']['_question_status'] = $this->libraries_model->_select('ij_member_question_status_ct','display_flag','1',0,0,0,0,'result_array');
			$page_data['page_content']['_question_close_status'] = $this->libraries_model->_select('ij_cc_question_close_ct','display_flag','1',0,0,'cc_question_close_code',0,'result_array');

			//var_dump($page_data['page_content']['question_qna']);
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'admin/page_order/list/item_js'; // 頁面主內容的 JS view 位置, 必要
			if($ifprint=='print'){
				$this->load->view('admin/general/print', $page_data);
			}else{
				$this->load->view('admin/general/main', $page_data);
			}
		}else{
				$this->session->set_userdata(array('Mes'=>'查無訂單編號'));
				redirect(base_url('admin/order/orderTable'));
		}
	}
}