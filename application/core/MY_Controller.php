<?php
class MY_Controller extends CI_Controller{
    public function __construct() {
        parent::__construct();
			if($this->uri->segment(1)=='admin'){
				$default_permission_pages=array('main/managerSignOut','login/index','main/managerSignAction','main/index','main/role_select','main/member_roles','main/sys_member_item','admin/get_unread_message_count','admin/ChgStoreName','product/ChgStoreName','product/ChgExtStoreName','product/AddAttribute','product/GetPackage','dashboard/index','admin/ajaxdel','admin/Chg_locaiotn_type');
				if(!in_array($this->router->fetch_class().'/'.$this->router->fetch_method(),$default_permission_pages)){
					//var_dump($this->router->fetch_class().'/'.$this->router->fetch_method());exit;
					$this->LoginCheck();
				    $this->load->library('ijw');
					$this->page_data = array();
					if(!$permission=$this->ijw->check_permission(1)){
						$this->ijw->_wait(base_url("/admin/main") , 2 , '很抱歉，您無權進入該頁面（'.$this->router->fetch_method().'/'.urldecode($this->uri->segment(4)).'），如剛新增權限請從新選擇登入角色，如有疑問請洽詢系統管理員',1);
						exit;
					}else{
						$this->page_data['permission'] = $permission;
					}
				}
			}
		$data['Mes'] = $this->session->userdata('Mes');
		if($data['Mes']){$this->load->view('Message',$data);$this->session->unset_userdata('Mes');}
    }

	public function LoginCheck()
	{
		//var_dump($this->session->userdata('member_role_system_rules'));
		//exit();
			if($this->session->userdata('member_role_system_rules') || $this->session->userdata('member_login')== true){
				return true;
			}else{
	      		$this->load->library('ijw');
				$this->ijw->_wait(base_url("admin/main/managerSignOut") , 2 , '很抱歉！查無您帳號的權限資料，請重新登入或洽詢系統管理員。');
				exit();
			}
	}

	public function Check_admin_level() {
		if($this->session->userdata('member_data')['member_level_type'] > 8) {
			return true;
		} else {
			//redirect(base_url("/admin/main"));
      $this->load->library('ijw');
			$this->ijw->_wait(base_url("/admin/main") , 2 , '權限需為系統管理員以上才可進入');
			exit();
		}
	}
	public function Check_page_permission($_function='',$_function_name='') {
		//echo $_function;
		if($this->session->userdata('member_role_system_rules')[0]['role_sn']!='1'){ //超級管理員
			if(stripos($this->ijw->check_permission(1),$_function)===false){
		//var_dump(stripos($this->ijw->check_permission(1),$_function));exit;
				if (!$this->input->is_ajax_request()) {
						$this->ijw->_wait(base_url("/admin/main") , 2 , '很抱歉，您無權進入該行為（'.urldecode($this->ijw->get_sys_program_name($this->router->fetch_method())).'--'.$_function_name.'），如剛新增權限請從新選擇登入角色，如有疑問請洽詢系統管理員');
						exit;
				}else{
					echo false;
					exit;
				}
			}
		}
	}
    //製作驗證碼
	  public function captcha_img()
    {
        $this->load->helper('captcha');
				$pool = '0123456789';
        $word = '';
        for ($i = 0; $i < 4; $i++){
            $word .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
        }

        $this->session->set_userdata('captcha', $word);

        $vals = array(
            'word'  => $word,
            'img_path'  => FCPATH.'public/uploads/captcha/',
            'img_url'  => base_url().'public/uploads/captcha/',
            'font_path' => FCPATH.'public/fonts/Ostrich-black-webfont.ttf',
						'img_width' => '150',
						'img_height' => 40,
						'font_size'  => 22,
						'expiration' => 7200
            );
        $cap = create_captcha($vals);
        return $cap['image'];
    }
}
