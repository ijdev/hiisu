<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>訂單列表</h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container" style="width: 100%;">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">訂單列表</span>
            </div>
            <div class="actions btn-set">
            </div>
          </div>
<form id="form1" class="form-horizontal" role="form" method="get">
          <div class="note note-warning">
            <div class="input-group input-large date-picker input-daterange pull-left" data-date="2015-05-07" data-date-format="yyyy-mm-dd">
              <span class="input-group-addon"> 期間 </span>
              <input type="text" class="form-control input-sm" name="orderTable[from]" value="<?php echo @$data_array['from'];?>">
              <span class="input-group-addon"> 到 </span>
              <input type="text" class="form-control input-sm" name="orderTable[to]" value="<?php echo @$data_array['to'];?>">
            </div>
            <div class="table-actions-wrapper pull-right">
              <span>
              </span>
              <input name="orderTable[user_name]" type="text" value="<?php echo @$data_array['user_name'];?>" class="pagination-panel-input form-control input-inline input-sm" size="8" style="margin: 0 5px;width: 200px;" placeholder="會員帳號">
              <?php echo $Supplier;?>
              <select name="orderTable[sub_order_status]" class="table-group-action-input form-control input-inline  input-sm" style="font-size:11px;">
                <option value="">訂單狀態</option>
              <?php foreach($sub_order_status as $_Item){ ?>
                <option <?php echo (@$data_array['sub_order_status']==$_Item['sub_order_status'])?'selected':'';?> value="<?php echo $_Item['sub_order_status']?>"><?php echo $_Item['sub_order_status_name']?></option>
              <?php }?>
              </select>
              <select name="orderTable[pay_status]" class="table-group-action-input form-control input-inline  input-sm" style="font-size:11px;">
                <option value="">付款狀態</option>
              <?php foreach($payment_status as $_Item){?>
                <option <?php echo (@$data_array['pay_status']==$_Item['payment_status'])?'selected':'';?> value="<?php echo $_Item['payment_status']?>"><?php echo $_Item['payment_status_name']?></option>
              <?php }?>
              </select>
              <select name="orderTable[admin_user]" class="table-group-action-input form-control input-inline  input-sm" style="font-size:11px;">
                <option value="">客服人員</option>
              <?php foreach($admin_users as $_Item){?>
                <option <?php echo (@$data_array['admin_user']==$_Item['member_sn'])?'selected':'';?> value="<?php echo $_Item['member_sn']?>"><?php echo $_Item['last_name'];?></option>
              <?php }?>
              </select>
              <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button>
            </div>
            <div style="clear:both;"></div>
          </div>
</form>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>訂單編號/發票號碼</th>
                  <th>訂單日期</th>
                  <th>訂購人</th>
                  <th>收貨人</th>
                  <th>付款方式</th>
                  <th>配送方式</th>
                  <th>運費</th>
                  <th>總價</th>
<?php	if($this->session->userdata('member_role_system_rules')[0]['role_name']!='供應商管理員'){?>
                  <th> 供 應 商 </th>
<?php }?>
<?php if($this->session->userdata('member_role_system_rules')[0]['role_name']!='經銷商管理員'){?>
                  <th> 經 銷 商 </th>
<?php }?>
                  <th>訂單狀態</th>
                  <th>付款狀態</th>
                  <th>配送狀態</th>
                  <th style="width:100px !important;">功能</th>
                </tr>
              </thead>
              <tbody>
                <?php if($orders){foreach($orders as $key=>$_Item){?>
                  <tr class="odd gradeX">
                    <td><?php echo $_Item['sub_order_num'];?><br><?php echo $_Item['invoice_num'];?></td>
                    <td><small><?php echo $_Item['order_create_date'];?></small></td>
                    <td>
<?php if($this->session->userdata('member_role_system_rules')[0]['role_name']!='經銷商管理員' && $this->session->userdata('member_role_system_rules')[0]['role_name']!='供應商管理員'){?>
                      <a target="_blank" href="/admin/Member/memberTable_item/edit/<?php echo $_Item['member_sn'];?>">
<?php }?>
                        <?php echo $_Item['buyer_last_name'].$_Item['buyer_first_name'];?>&nbsp;
<?php if($this->session->userdata('member_role_system_rules')[0]['role_name']!='經銷商管理員' && $this->session->userdata('member_role_system_rules')[0]['role_name']!='供應商管理員'){?>
                        <i class="fa fa-external-link"></i></a>
<?php }?>
                    </td>
                    <td><?php echo $_Item['receiver_last_name'];?></td>
                    <td><?php echo $_Item['payment_method_name'];?></td>
                    <td><?php echo $_Item['delivery_method_name'];?></td>
                    <td><?php echo $_Item['shipping_charge_amount'];?></td>
                    <td><?php echo $_Item['sub_sum'];?></td>
<?php	if($this->session->userdata('member_role_system_rules')[0]['role_name']!='供應商管理員'){?>
                    <td><?php echo $this->ijw->truncate2(($_Item['supplier_name'])?$_Item['supplier_name']:'囍市集',8,'');?></td>
<?php }?>
<?php if($this->session->userdata('member_role_system_rules')[0]['role_name']!='經銷商管理員'){?>
                    <td><?php echo $this->ijw->truncate2((@$_Item['dealer_name'])?$_Item['dealer_name']:'囍市集',8,'');?></td>
<?php }?>
                    <td class="text-center">
                        <span class="label label-default"><?php echo $_Item['sub_order_status_name'];?></span>
                    </td>
                    <td class="text-center">
                        <span class="label label-warning"><?php echo $_Item['pay_status_name'];?></span>
                    </td>
                    <td><?php echo $_Item['delivery_status_name'];?></td>
                    <td class="text-left">
                      <a href="admin/order/orderItem/<?php echo $_Item['sub_order_sn'];?>"><i class="fa fa-edit"></i>明細</a> &nbsp;&nbsp;
                      <a href="admin/admin/callcenterMsg/<?php echo @$_Item['member_question_sn']?>" member_sn="<?php echo $_Item['member_sn'];?>" sub_order_sn="<?php echo $_Item['sub_order_sn'];?>" data-remote="false" data-target="#<?php echo (@$_Item['member_question_sn'])?'ajax_msg':'ticket_form';?>" class="addnew" data-toggle="modal"><i class="fa fa-edit">客服記錄</i></a>
<?php if(stripos($this->ijw->check_permission(1,$this->router->fetch_class(),$this->router->fetch_method()),'del')){?>
                  <a href="javascript:void(0);" class="deleteItem" ItemId="<?php echo $_Item['sub_order_sn'];?>"><i class="fa fa-trash-o"></i>刪除</a>
<?php }?>
                    </td>
                  </tr>
                <?php }}?>
              </tbody>
            </table>

          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
<!-- Modal 顯示與客服的訊息列表 -->
<div class="modal fade" id="ajax_msg" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">訊息記錄</h4>
      </div>
      <div class="modal-body">
        <img src="public/metronic/global/img/loading-spinner-grey.gif" alt="" class="loading">
        <span>
        &nbsp;&nbsp;內容載入中... </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn blue">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- /Modal 顯示與客服的訊息列表 -->
      <!-- Modal for 表單填寫 -->
       <form role="form" name="ticket_form" method="post" action="/admin/admin/addNewCallcenter">
      <div class="modal fade" id="ticket_form" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">請說明您的問題</h4>
            </div>
            <div class="modal-body">
               <div class="form">

                  <div class="form-body">
                    <div class="form-group">
                      <label>問題主題<span class="require">*</span></label>
                      <div class="input-icon">
                        <i class="fa fa-question-circle"></i>
                        <input type="text" required name="question_subject" class="form-control" placeholder="請輸入您的問題主題">
                      </div>
                    </div>
                   <div class="form-group">
                      <label>訂單編號</label>
                      <div class="input-icon">
                        <i class="fa fa-file-o"></i>
                        <input type="text" name="associated_sub_order_sn" id="associated_sub_order_sn" class="form-control" placeholder="若您欲詢問的問題與訂單有關，請您儘可能提供">
                      </div>
                    </div>
                    <div class="form-group">
                      <label>問題大類</label>
                      <select name="ccapply_code" id="ccapply_code" required class="form-control">
                        <option value="">請選擇</option>
                        <?php foreach($ccapply as $_Item){?>
                        <option <?php echo ($_Item['ccapply_name']=='訂單問題')?'selected':'';?> value="<?php echo $_Item['ccapply_code']?>"><?php echo $_Item['ccapply_name']?></option>
                        <?php }?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>問題類型</label>
                      <select name="ccapply_detail_code" id="ccapply_detail_code" required class="form-control">
                        <option value="">請先選擇大類</option>
                      </select>
                    </div>
                    <!-- <div class="form-group">
                      <label>附加檔案</label>
                      <input type="file" id="exampleInputFile1">
                      <p class="help-block">
                         ※ 檔案大小2MB為限，附件格式僅接受jpeg、png。
                      </p>
                    </div>-->
                    <div class="form-group">
                      <label>問題說明 <span class="require">*</span></label>
                      <div class="input-icon">
                        <textarea required name="question_description" class="form-control" rows="5"></textarea>
                      </div>
                    </div>
                  </div>

               </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn default" data-dismiss="modal">關閉</button>
              <input type="hidden" name="member_sn" id="member_sn" value="">
              <button type="submit" id="ticket_submit" class="btn blue">送出表單</button>
            </div>
          </div>
          </form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- Modal for 表單填寫 -->