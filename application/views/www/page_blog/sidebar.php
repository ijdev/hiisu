<style>
.blog-tags li a.active :hover{
  color: #fff;
  background: #E84D1C;
  text-decoration: none;
}
.blog-tags li a.active :hover i {
  color: #fff;
}
.blog-tags li a.active,.blog-tags li a.active > i{
  color: #FFF;
  background: #CEBEA2;
  display: inline-block;
  /*padding: 3px 5px 3px 3px;*/
}
.blog-tags li a.active:after{ border-left-color: #cebea2;}

</style>
<!-- BEGIN RIGHT SIDEBAR -->
<div class="col-md-3 col-sm-3 blog-sidebar">
  <div class="search_block_right">
		<form name="search" method="get" action="home/blogList" class="blog_widget_search">
				<input placeholder="請輸入欲搜尋的關鍵字" style="width: 185px;" id="search" name="search" class="blog_widget_input" autocomplete="off" type="search" value="<?=$search?>">
				<button type="submit">
					<i class="fa fa-search"></i>
				</button>
		</form>
								</div>
  <!-- CATEGORIES START -->
  <h2 class="no-top-space">文章分類</h2>
  <ul class="nav sidebar-categories margin-bottom-40">
<?php if($content_types){foreach($content_types as $key=>$_Item){?>
    <li <?php echo ($_Item['content_title']==$content_type) ? 'class="active"':''; ?>><a href="home/blogList/<?php echo $_Item['content_title'];?>"><?php echo $_Item['content_title'];?> (<?php echo $_Item['numrows'];?>)</a></li>
<?php }}?>
  </ul>
  <!-- CATEGORIES END -->

  <!-- BEGIN BLOG TAGS -->
  <div class="blog-tags margin-bottom-20">
    <h2>關鍵字</h2>
    <ul>
<?php if(@$Items[0]['keywords'][0]){foreach($Items[0]['keywords'] as $key=>$_Item){?>
      <li><a <?php echo ($_Item==$keyword_tag) ? 'class="active"':''; ?> href="home/blogKeyword/<?php echo $_Item;?>"><img class="fa-tags" src="public/img/icon_tag.png"/><?php echo $_Item;?></a></li>
<?php }}?>
    </ul>
  </div>
  <!-- END BLOG TAGS -->
</div>
<!-- END RIGHT SIDEBAR -->