<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/

 /*共用語言*/
  $_lang["sort_order"]="排序順次";
	$_lang["status"]="使用狀態";
	/*共用語言*/
	
	
  /*admin 系統程式*/
  $_lang["sys_program_name"]="功能中文名稱";
	$_lang["sys_program_eng_name"]="功能英文名稱";
	/*admin 系統程式*/
	
	
	
?>

<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <?php echo $_page_title;?>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase"><?php echo $_caption_subject;?></span>
            </div>
            <div class="actions btn-set">
              <a href="<?php echo $init_control;?><?php echo $_father_link;?>" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
          
                 <form class="form-horizontal error" novalidate=""> 
									
 					
	            
<?php

	//data-validation-email-message
	//data-validation-number-message
	
	
	$_form_one= new sean_form_general();
	$em_columns = array(										
		  //'delimiter_1'=>array('inner_html'=>'<span style="font-size:13px;font-weight:bold;color:#336699"><i>'.$_lang["delimiter_1"].'</i></span><br />'),						
			'sys_program_name'=>array('header'=>$_lang["sys_program_name"],'type'=>'textbox', 'required'=>'required', 'required_message'=>$_lang["sys_program_name"],'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'3', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),  	
			'sys_program_eng_name'=>array('header'=>$_lang["sys_program_eng_name"], 'type'=>'email',"data_validation_email_message"=>"請輸入email格式!",'required'=>'required','required_message'=>$_lang["sys_program_eng_name"], 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'3', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),  	
			'sort_order'=>array('header'=>$_lang["sort_order"], 'type'=>'number',"data_validation_number_message"=>"請輸入數字!", 'required'=>'required','required_message'=>$_lang["sort_order"],'form_control'=>'','maxlength'=>'3', 'default'=>'1',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),  	
			'status'=>array('header'=>$_lang["status"], 'type'=>'status', 'required_message'=>$_lang["status"],'required'=>'required','form_control'=>'','maxlength'=>'3', 'default'=>'1',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),  	
	);
	
	
 	 $_form_one->add_item($em_columns);
									
									
?>
                    
                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                          <button type="submit" class="btn green">儲存</button>
                          <button type="button" class="btn default">Cancel</button>
                        </div>
                      </div>
                    </div>
                  </form>
                
               
              
               
            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->


