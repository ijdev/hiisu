<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>行銷管理 <small>EDM寄送名單</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
<div class="page-content">
  <div class="container">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">首頁</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        優惠活動
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/admin/edm">電子報管理</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/admin/edmSend">發送</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <?php echo @$Item['send_config'];?>
      </li>
    </ul>
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">EDM寄送名單</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/admin/edmSend/<?php echo $edm_sn;?>" class="btn green"><i class="fa fa-level-up"></i> 回寄送設定</a>
            </div>
          </div>
          <div class="table-actions-wrapper pull-right">
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>EMAIL</th>
                  <th>姓名</th>
                  <th>寄送時間</th>
                  <th>是否會員</th>
                  <th>是否開啟</th>
                </tr>
              </thead>
              <tbody>
								<?php if($Items){ foreach($Items as $key=>$_Item){?>
                  <tr class="odd gradeX">
                    <td><?php echo $_Item['receiver_email'];?></td>
                    <td><?php echo $_Item['receiver_last_name'];?><?php echo $_Item['receiver_first_name'];?></td>
                    <td class="center">
                      <?php echo ($_Item['send_time']!='0000-00-00 00:00:00')? $_Item['send_time']:'尚未發送'; ?>
                    </td>
                    <td class="center">
                      <?php if($_Item['associated_member_sn']):?>
                        <i class="fa fa-check"></i>
                      <?php else:?>
                        <i class="fa fa-times"></i>
                      <?php endif;?>
                    </td>
                    <td class="center">
                      <?php if($_Item['edm_open_flag']=='1'):?>
                        <i class="fa fa-check"></i>
                      <?php else:?>
                        <i class="fa fa-times"></i>
                      <?php endif;?>
                    </td>
                  </tr>
                <?php }}?>
              </tbody>
            </table>

          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->