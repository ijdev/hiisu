<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link href="public/metronic/global/plugins/jquery-minicolors/jquery.minicolors.css" rel="stylesheet" type="text/css"/>


<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品規格 <small>規格單位編輯</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/admin">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品規格
        <i class="fa fa-circle"></i>
      </li>
      <li>
         <a href="admin/product/productModule">規格單位</a>
         <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         規格單位編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">編輯規格單位</span>
            </div>
            <div class="actions btn-set">
              <a href="/admin/product/productModule" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post">
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">名稱 <span class="require">*</span></label>
                <div class="col-md-10">
                  <input type="text" name="module[spec_option_name]" value="<?php echo @$Item['spec_option_name']; ?>" class="form-control" placeholder="如相簿模組、燙金等規格定義" required>
                </div>
              </div>
              <div class="form-group">
                <label for="note" class="col-md-2 control-label">館別 <span class="require">*</span></label>
                <div class="col-md-10">
                  <!-- 選項在 table ij_config 中的 type = shop_menu -->
                    <?php echo $Store;?>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-2 control-label">分類 <span class="require">*</span></label>
                <div class="col-md-10">
                  <?php echo $categorys;?>
                  <!--select multiple name="categorys" class="form-control" size="10" required>
                    <option value="2001"> ├─喜帖 邀請卡</option>
                    <option value="3289"> │ ├─本館強檔推薦</option>
                    <option value="3288"> │ │ ├─最新上市</option>
                    <option value="3295"> │ │ ├─test3</option>
                    <option value="3294"> │ │ ├─test2</option>
                    <option value="3305"> │ ├─電子喜帖</option>
                    <option value="3286"> │ ├─西式喜帖設計</option>
                    <option value="3297"> │ ├─中式精選喜帖</option>
                    <option value="3298"> │ ├─傳統燙金喜帖</option>
                    <option value="3208"> │ ├─客製創意喜帖</option>
                    <option value="3215"> │ ├─喜帖週邊製作</option>
                    <option value="3212"> │ │ ├─RSVP 回函卡</option>
                    <option value="3234"> │ │ ├─信封貼紙</option>
                    <option value="3264"> │ │ ├─喜帖信封</option>
                    <option value="3235"> │ │ ├─喜帖製作費</option>
                    <option value="3081"> │ ├─活動請柬/邀請卡</option>
                    <option value="1001"> ├─婚禮小物</option>
                    <option value="3292"> │ ├─本館強檔推薦[未發佈]</option>
                    <option value="3299"> │ │ ├─test</option>
                  </select-->
                  <span class="help-block"> 按住 control 進行複選 </span>
                </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">使用限制</label>
                <div class="col-md-10">
                  <?php echo $uselimit;?>
                  <span class="help-block"> 選項由 Table : ij_config 中的 product_option_limitation 拉出</span>
                </div>
              </div>
              <div class="form-group">
                <label for="password" class="col-md-2 control-label">備註</label>
                <div class="col-md-10">
                  <textarea name="module[description]" class="form-control" rows="3"><?php echo @$Item['description']; ?></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-md-2 control-label">排序</label>
                <div class="col-md-10">
                  <input name="module[sort_order]" maxlength="4" type="text" class="form-control" value="<?php echo (@$Item['sort_order']) ? @$Item['sort_order']:'9999'; ?>" required>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-md-2 control-label">是否啟用 <span class="require">*</span></label>
                <div class="col-md-10">
                  <div class="radio-list">
                      <label class="radio-inline">
                        <input type="radio" name="module[status]" id="optionsRadios4" value="1" <?php if(@$Item['status']){ ?>checked<?php }?> required>  啟用  
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="module[status]" id="optionsRadios4" value="0" <?php if(@$Item['status']==='0'){ ?>checked<?php }?> required>  不啟用 
                      </label>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-md-2 control-label">更新者</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['update_member_sn'])?@$Item['update_member_sn']:@$Item['create_member_sn']; ?>" Disabled>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-md-2 control-label">更新時間</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['last_time_update']!='0000-00-00 00:00:00')?@$Item['last_time_update']:@$Item['create_date']; ?>" Disabled>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <input type="hidden" name="spec_option_config_sn" value="<?php echo @$Item['spec_option_config_sn']; ?>">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="reset" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->