<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 class Sean_table_general
 {


	protected $_elements = array();
	protected $_value_data = array();
	private $CI;

	public function __construct()
	{

		$this->CI =& get_instance();
		$this->CI->lang->load('general', 'zh-TW');

	}
	/* 產生列表的標頭 */
	public function table_thead($_value)
 	{
 		echo '<thead>
                <tr>';
		foreach($_value as $key => $value)
		{
		echo  '<th>'.$value.'</th>';
		}

         echo'  <th>功能</th></tr>
              </thead>';
 	}

	/* 內容列表 */
	public function table_tbody($_value)
 	{
 		echo '<tr>';
			foreach($_value as $key => $value)
			{
			echo  '<td '.$value["_class"].' >'.$value["_value"].'</td>';
			}
        echo'</tr>';
 	}


 }
 class Sean_form_general
 {


	protected $_elements = array();
	protected $_value_data = array();
	private $CI;
	public function __construct()
	{

		$this->CI =& get_instance();
		$this->CI->lang->load('general', 'zh-TW');

	}

  public function add_item(array $properties = null,array $_result01 = null )
  {

  	$this->_value_data=$_result01;

  	foreach($properties as $key => $value)
  	{
		  	$this->_elements=$value;
		  	$this->_elements["id"]=$key;
			if($this->_elements["type"]=="hide_flag")
		  	$this->hide_flag_form();

			if($this->_elements["type"]=="default_flag")
		  	$this->default_flag_form();

			if($this->_elements["type"]=="display_flag")
		  	$this->display_flag_form();


			if($this->_elements["type"]=="enum")
		  	$this->enum_form();

			if($this->_elements["type"]=="radio")
		  	$this->radio_form();

			if($this->_elements["type"]=="checkbox")
		  	$this->checkbox_form();


		  	if($this->_elements["type"]=="textbox")
		  	$this->textbox_form("text");

		  	if($this->_elements["type"]=="textarea")
		  	$this->textarea_form();

		  	if($this->_elements["type"]=="status")
		  	$this->status_form();

		  	if($this->_elements["type"]=="label")
		  	$this->label_form();

		  	if($this->_elements["type"]=="email")
		  	$this->textbox_form("email");

		  	if($this->_elements["type"]=="number")
		  	$this->textbox_form("number");

		  	if($this->_elements["type"]=="password")
		  	$this->password_form();

			if($this->_elements["type"]=="address")
		  	$this->address_form();
    }
  }

 	public function set_required()
 	{

 		 	$_require="";
 		 	if(isset($this->_elements["required"]) && $this->_elements["required"]=="required")
	 		$_require='<span class="require">*</span>';
	 		return $_require;
 	}

 	public function set_form_control()
 	{

 		 	$_form_control="";
 		 	if(isset($this->_elements["form_control"]) && strlen($this->_elements["form_control"])  > 1 )
	 	  $_form_control=" ".$this->_elements["form_control"];
	 	  return  $_form_control;
 	}

 	public function set_pre_addition()
 	{

 		 	$_pre_addition="";
 		 	if(isset($this->_elements["pre_addition"]) && strlen($this->_elements["pre_addition"])  > 1 )
	 	  $_pre_addition=' <p class="form-control-static">
	               '.$this->_elements["pre_addition"].'
	               </p>';
	 	  return  $_pre_addition;
 	}
 	public function set_post_addition()
 	{
 		 	$_post_addition="";
 		 	if(isset($this->_elements["post_addition"]) && strlen($this->_elements["post_addition"])  > 1 )
	 	  		$_post_addition=' <p class="form-control-static">
	               '.$this->_elements["post_addition"].'
	               </p>';
	 	  return  $_post_addition;
 	}

	 public function echo_value($_key)
	{

			if(is_array($this->_value_data))
      		{
				   foreach($this->_value_data as $key => $value)
				   {
									if(isset($value->$_key) && $value->$_key!="0000-00-00")
									return "value=\"".$value->$_key."\"";
				  }
			}else{

					$_default= $this->_elements["default"];
					if(strlen($_default) > 0 )
					{
						return "value=\"".$_default."\"";
					}

			}

	}

	 public function get_value($_key)
	{

			if(is_array($this->_value_data))
      {
           foreach($this->_value_data as $key => $value)
           {
							if($value->$_key=="0000-00-00")
							return "";

							if(isset($value->$_key))
							return $value->$_key;
					 }
			}

	}



  public function address_form()
  {
	  /*
	  addr_country_code 886
	  add_state_code 886
	  addr_country 台灣
	  addr_state 地區

	  addr_city_code
	  addr_town_code


	  addr_city
	  addr_town
	  zipcode
	  addr1
	  addr2
	  */
	  $_city_s='<option value="">請選擇</option>';
	  $_city_s2='<option value="">請先選擇縣市</option>';
				//$_city_list=$this->CI->config->item("city_ct");
				$this->CI->load->model('Shop_model');
				$_city_list=$this->CI->Shop_model->_select('ij_city_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','city_code,city_name');
                $_default=$this->get_value("addr_city_code");
								//var_dump($_city_list);

                while (list($keys, $values) = each ($_city_list)) {
                	$_checks_0=' ';

				  if($_default == $keys )
				  $_checks_0=' selected ';

				  if($_default == $keys )
				  $_checks_0=' selected ';


				  $_city_s.=  ' <option value="'.$keys.'" '.$_checks_0.' >'.$values.'</option>';

				}

				if(strlen($_default)==0 )
				//$_default=2;
				$_aa="town_ct_".$_default;


				$_default2=$this->get_value("addr_town_code");

				//$_town_list=$this->CI->config->item($_aa);
				//var_dump($_default);
				$_town_list=$this->CI->Shop_model->_select('ij_town_ct',array('display_flag'=>'1','city_code'=>$_default),0,0,0,0,0,'list','town_code,town_name');
				//var_dump($_town_list);

				while (list($keys, $values) = each ($_town_list)) {
					$_checks_0=' ';

				  if($_default2 == $keys )
				  $_checks_0=' selected ';

				  if($_default2 == $keys )
				  $_checks_0=' selected ';


				$_city_s2.=  ' <option value="'.$keys.'" '.$_checks_0.' >'.$values.'</option>';

				}
				//echo $this->get_value("addr_state");
				$_addr_state1=(@$this->get_value("addr_state")=='台灣本島')?'selected':'';
				$_addr_state2=(@$this->get_value("addr_state")=='外島地區')?'selected':'';

	echo '<div class="form-group">
                 <label  class="col-lg-3 control-label">'.$this->_elements["header"].'
	            '.$this->set_required().'
	            </label>
                  <div class="col-lg-8">
                    <div class="form-group">
                      <div class="col-md-3">
                        <select name="addr_state"  id="addr_state" class="form-control">
                    <option value="">請選擇</option>
                    <option value="台灣本島" '.$_addr_state1.'>台灣本島</option>
                    <option value="外島地區" '.$_addr_state2.'>外島地區</option>
                        </select>
                      </div>
                      <div class="col-md-3">
                        <select name="addr_city_code"  id="addr_city_code" class="form-control">
                          '.$_city_s.'
                        </select>
                      </div>
                      <div class="col-md-3">
                        <select name="addr_town_code"  id="addr_town_code"  class="form-control">
                         '.$_city_s2.'
                        </select>
                      </div>
                      <div class="col-md-3">
                      <input type="text" name="zipcode" id="zipcode" class="form-control" id="zipcode" size="8" maxlength="5" placeholder="郵遞區號" value="'.$this->get_value("zipcode").'">
                      </div>
                    </div>
                    <input type="text" class="form-control"
                    name="'.$this->_elements["id"].'"
		            		id="'.$this->_elements["id"].'"
		            		value="'.$this->get_value($this->_elements["id"]).'"
		            		'.$this->max_message().'
		            		>
                  </div>
                </div>
	  ';
  }

  public function label_form()
 	{



	 	$_value=' <div class="form-group control-group error">
	            <label for="'.$this->_elements["id"].'" class="col-lg-3 control-label">'.$this->_elements["header"].'
	            '.$this->set_required().'
	            </label>
	            <div class="col-lg-8 controlsean">
	             '.$this->set_pre_addition().'
	             '.$this->get_value($this->_elements["id"]).'
	             '.$this->set_post_addition().'

	            </div>
	            </div>';

    echo $_value;
  }
  public function textarea_form()
 	{



	 	 $_value=' <div class="form-group control-group error">
		            <label for="'.$this->_elements["id"].'" class="col-lg-3 control-label">'.$this->_elements["header"].'
		            '.$this->set_required().'
		            </label>

		            <div class="col-lg-8 controlsean">
		             '.$this->set_pre_addition().'
		            <textarea
		             name="'.$this->_elements["id"].'"
		             id="'.$this->_elements["id"].'"
		             class="form-control '.$this->set_form_control().'"
		             rows="6">'.$this->get_value($this->_elements["id"]).'</textarea>
		  						'.$this->set_post_addition().'
		            </div>
	            </div>';

    echo $_value;
  }
	public function password_form()
 	{



	 	 $_value=' <div class="form-group control-group error">
		            <label for="'.$this->_elements["id"].'" class="col-lg-3 control-label">'.$this->_elements["header"].'
		            '.$this->set_required().'
		            </label>

		            <div class="col-lg-8 controlsean">
		             '.$this->set_pre_addition().'
		             <input type="password"
		             name="'.$this->_elements["id"].'"
		             id="'.$this->_elements["id"].'"
		             class="form-control '.$this->set_form_control().'"

		            ';
		  $_value.=$this->validation_message();
		  $_value.=$this->max_message();
		  $_value.='>
		  						'.$this->set_post_addition().'
		            </div>
	            </div>';

    echo $_value;
  }

 public function textbox_form($_value)
 	{



	 	 $_value=' <div class="form-group control-group error">
		            <label for="'.$this->_elements["id"].'" class="col-lg-3 control-label">'.$this->_elements["header"].'
		            '.$this->set_required().'
		            </label>

		            <div class="col-lg-8 controlsean">
		             '.$this->set_pre_addition().'
		             <input type="'.$_value.'"
		             name="'.$this->_elements["id"].'"
		             id="'.$this->_elements["id"].'"
		             class="form-control '.$this->set_form_control().'"
		             '.$this->echo_value($this->_elements["id"]).'
		            ';
		  $_value.=$this->validation_message();
		  $_value.=$this->max_message();
		  $_value.=$this->autocomplete();
		  $_value.='>
		  						'.$this->set_post_addition().'
		            </div>
	            </div>';

    echo $_value;
  }

  function checkbox_form()
  {
  				$_checks = $this->get_value($this->_elements["id"]);

					$_value=' <div class="form-group control-group error">
					<label for="'.$this->_elements["id"].'" class="col-lg-3 control-label">'.$this->_elements["header"].'
					'.$this->set_required().'
					</label>
					<div class="col-lg-8 controlsean">
					  '.$this->set_pre_addition();

					$_default= $this->_elements["default"];
					foreach($this->_elements["source"] as $key => $value)
					{
						  $_checks_0=' ';

						  if($_default == $value )
						  $_checks_0=' checked ';

						  if($_checks == $value )
						  $_checks_0=' checked ';


						 $_value.=' <input type=checkbox  id="'.$this->_elements["id"].'" name="'.$this->_elements["id"].'" value="'.$value.'" '.$_checks_0.' >'.$value;
					}

				$_value.='
				   '.$this->set_post_addition().'
					</div>
					</div>';

   		echo $_value;
  }

  	public function enum_form()
 	{

					$_checks = $this->get_value($this->_elements["id"]);

					$_value=' <div class="form-group control-group error">
					<label for="'.$this->_elements["id"].'" class="col-lg-3 control-label">'.$this->_elements["header"].'
					'.$this->set_required().'
					</label>
					<div class="col-lg-8 controlsean">
					  '.$this->set_pre_addition().'
					<select class="form-control" id="'.$this->_elements["id"].'" name="'.$this->_elements["id"].'" >';
					$_default= $this->_elements["default"];
					foreach($this->_elements["source"] as $key => $value)
					{
						  $_checks_0=' ';

						  if($_default == $key )
						  $_checks_0=' selected ';

						  if($_checks == $key )
						  $_checks_0=' selected ';


						 $_value.=' <option value="'.$key.'" '.$_checks_0.' >'.$value.'</option>';
					}

				$_value.='	 </select>
				   '.$this->set_post_addition().'
					</div>
					</div>';

   		echo $_value;

	}
    public function display_flag_form()
 	{

 	  $_checks= $this->get_value($this->_elements["id"]);
 	  $_checks_0='';
 	  $_checks_1=' selected ';

	  if($_checks==0)
	  {
		  $_checks_0=' selected ';
		  $_checks_1=' ';
	  }

	 	$_value=' <div class="form-group control-group error">
	            <label for="'.$this->_elements["id"].'" class="col-lg-3 control-label">'.$this->_elements["header"].'
	            '.$this->set_required().'
	            </label>
	            <div class="col-lg-8 controlsean">
	              '.$this->set_pre_addition().'
	              	  <select class="form-control" id="'.$this->_elements["id"].'" name="'.$this->_elements["id"].'" >
                      <option value="1" '.$_checks_1.'>'.$this->CI->lang->line("general_display_flag_1").'</option>
					  <option value="0" '.$_checks_0.'>'.$this->CI->lang->line("general_display_flag_0").'</option>

                 </select>
               '.$this->set_post_addition().'
	            </div>
	            </div>';

    echo $_value;
   }
	public function default_flag_form()
 	{

 	  $_checks= $this->get_value($this->_elements["id"]);
 	  $_checks_1='';
 	  $_checks_0=' selected ';

	  if($_checks==1)
	  {
		  $_checks_1=' selected ';
		  $_checks_0=' ';
	  }

	 	$_value=' <div class="form-group control-group error">
	            <label for="'.$this->_elements["id"].'" class="col-lg-3 control-label">'.$this->_elements["header"].'
	            '.$this->set_required().'
	            </label>
	            <div class="col-lg-8 controlsean">
	              '.$this->set_pre_addition().'
	              	  <select class="form-control" id="'.$this->_elements["id"].'" name="'.$this->_elements["id"].'" >
                      <option value="0" '.$_checks_0.'>'.$this->CI->lang->line("general_default_flag_0").'</option>
                      <option value="1" '.$_checks_1.'>'.$this->CI->lang->line("general_default_flag_1").'</option>
                 </select>
               '.$this->set_post_addition().'
	            </div>
	            </div>';

    echo $_value;
  }

    public function hide_flag_form()
 	{

 	  $_checks= $this->get_value($this->_elements["id"]);
 	  $_checks_1='';
 	  $_checks_0=' selected ';

	   if($_checks==1)
	  {
		  $_checks_1=' selected ';
		  $_checks_0=' ';
	  }

	 	$_value=' <div class="form-group control-group error">
	            <label for="'.$this->_elements["id"].'" class="col-lg-3 control-label">'.$this->_elements["header"].'
	            '.$this->set_required().'
	            </label>
	            <div class="col-lg-8 controlsean">
	              '.$this->set_pre_addition().'
	              	  <select class="form-control" id="'.$this->_elements["id"].'" name="'.$this->_elements["id"].'" >
                      <option value="0" '.$_checks_0.'>'.$this->CI->lang->line("general_hide_flag_0").'</option>
                      <option value="1" '.$_checks_1.'>'.$this->CI->lang->line("general_hide_flag_1").'</option>
                 </select>
               '.$this->set_post_addition().'
	            </div>
	            </div>';

    echo $_value;
  }


  public function radio_form()
 	{

 	  $_checks= $this->get_value($this->_elements["id"]);
 	  //echo $this->_elements["id"];
 	  $_checks_1='';
 	  $_checks_2='';
 	  $_checks_0='';
 	  if($_checks==0) $_checks_0=' checked ';
 	  if($_checks==1) $_checks_1=' checked ';
 	  if($_checks==2) $_checks_2=' checked ';
 	  $_radio='';
 	  if($this->_elements["id"]=='gender'){
 	  	$_radio.='<label class="radio-inline"><input type="radio" value="1" '.$_checks_1.' class="st_radio" name="gender">男 </label>';
 	  	$_radio.='<label class="radio-inline"><input type="radio" value="0" '.$_checks_0.' class="st_radio" name="gender">女 </label>';
 	  }
	 	$_value=' <div class="form-group radio-list control-group error">
	            <label for="'.$this->_elements["id"].'" class="col-lg-3 control-label">'.$this->_elements["header"].'
	            '.$this->set_required().'
	            </label>
	            <div class="col-lg-8 controlsean">
	              '.$this->set_pre_addition().
	              $_radio
               .$this->set_post_addition().'
	            </div>
	            </div>';

    echo $_value;
  }

  public function status_form()
 	{

 	  $_checks= $this->get_value($this->_elements["id"]);
	  $_default= $this->_elements["default"];
 	  $_checks_1='';
 	  $_checks_2='';
 	  $_checks_0='';
 	  if(strlen($_default)  > 0 )
	  {
		  if($_default==0) $_checks_0=' selected ';
		  if($_default==1) $_checks_1=' selected ';
		  if($_default==2) $_checks_2=' selected ';
	  }
	  if($_checks==0) $_checks_0=' selected ';
 	  if($_checks==1) $_checks_1=' selected ';
 	  if($_checks==2) $_checks_2=' selected ';

	 	$_value=' <div class="form-group control-group error">
	            <label for="'.$this->_elements["id"].'" class="col-lg-3 control-label">'.$this->_elements["header"].'
	            '.$this->set_required().'
	            </label>
	            <div class="col-lg-8 controlsean">
	              '.$this->set_pre_addition().'
	              <select class="form-control" id="'.$this->_elements["id"].'" name="'.$this->_elements["id"].'" >
                      <option value=0 '.$_checks_0.'>'.$this->CI->lang->line("general_status_0").'</option>
                      <option value=1 '.$_checks_1.'>'.$this->CI->lang->line("general_status_1").'</option>
                      <option value=2 '.$_checks_2.'>'.$this->CI->lang->line("general_status_2").'</option>
                 </select>
               '.$this->set_post_addition().'
	            </div>
	            </div>';

    echo $_value;
  }

/*
* 
*
*
*/
	function max_message()
	{
		$_value="";

		if($this->_elements["maxlength"] && strlen($this->_elements["maxlength"]) > 0 )
		$_value.=' maxlength="'.$this->_elements["maxlength"].'" ';
		else
		$_value.=' maxlength="255" ';


		return $_value;
	}
	function autocomplete()
	{
		$_value="";

		if(@$this->_elements["autocomplete"]=='off' )
		$_value.=' autocomplete="off" ';


		return $_value;
	}
	function validation_message()
	{
	 	     $_value="";
	 	     if(isset($this->_elements["required"]) && $this->_elements["required"]=="required")
			   {
			             if(strlen($this->_elements["required_message"])==0)
						 $this->_elements["required_message"]="請輸入".$this->_elements["header"]."!";


						 $_value.=' '.$this->_elements["required"];

			             $_value.=' data-validation-required-message="'.$this->_elements["required_message"].'" ';

			             if($this->_elements["type"]=="number" && isset($this->_elements["data_validation_number_message"]))
			             $_value.='  data-validation-number-message="'.$this->_elements["data_validation_number_message"].'"';

			             if($this->_elements["type"]=="email" && isset($this->_elements["data_validation_email_message"]))
			             $_value.='  data-validation-email-message="'.$this->_elements["data_validation_email_message"].'"';
			   }
	 			return $_value;
	}



 }

/* End of file Sean_form_general.php */