<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sys_role extends My_general_ct {

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('sean_models/Sean_db_tools');
	  $this->load->model('libraries_model');
		$this->_table="ij_role";
		$this->_id="role_sn";
		$this->_table_field=array(
					 "role_name" => "角色中文名稱",
					 "role_eng_name" => "角色英文名稱",
					 "role_descrption" => "角色描述",
					 "status" => "使用狀態",
					 "sort_order" => "排序順次"
				);
    }


    public function table_value_list( $_where) {
        $page_data['_body_result']=array();
		if(strlen($_where)==0)
		$_where="  order by sort_order asc";

		$_result01=$this->Sean_db_tools->db_get_max_record("*",$this->_table,$_where);

		/* 列表標頭名稱 */
		$page_data['_table_thead_name']=array("角色中文名稱","角色英文名稱","角色描述","排序順次","使用狀態");
		/* 類表內容 */
		if(is_array($_result01))
		{
                 		$_i=0;
                 			foreach($_result01 as $key => $value)
                    	{

			                    		if($value->status == 1)
			                    		{
			                    					$_active = '<i class="fa fa-check"></i>';
			                    		}else{
			                    					$_active = '<i class="fa fa-times"></i>';
			                    		}

			                    		$_body_result='';

			                    		$page_data['_body_result'][$_i][0]=array("_value"=>$value->role_name,"_class"=>"");
			                    		$page_data['_body_result'][$_i][1]=array("_value"=>$value->role_eng_name,"_class"=>"");
			                    		$page_data['_body_result'][$_i][2]=array("_value"=>$value->role_descrption,"_class"=>"");
			                    		$page_data['_body_result'][$_i][3]=array("_value"=>$value->sort_order,"_class"=>"");
			                    		$page_data['_body_result'][$_i][4]=array("_value"=>$_active,"_class"=>"");
			                    		$page_data['_body_result'][$_i][5]=array("_value"=>'
												<a href="'.$this->init_control.'sys_role_item/edit/'.$value->role_sn.'"><i class="fa fa-edit"></i>編輯</a>&nbsp;&nbsp;
						                      <a href="'.$this->init_control.'sys_role_item/delete/'.$value->role_sn.'" onclick="return deletechecked();"><i class="fa fa-trash-o"></i>刪除</a>&nbsp;&nbsp;

						                  	',"_class"=>'class="center"');
                				$_i++;
                			}

    				}
			return $page_data;

    }

	public function action_insert() {
			$_result="";
			foreach($this->_table_field as $key => $value)
			{
					if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
					$_data[$key]=$this->input->get_post($key);
			}
			$_result=$this->Sean_db_tools->db_insert_record($_data,$this->_table);
			if($_result && $role_sn=$this->db->insert_id()){
				//$role_sn=$this->db->insert_id();
				//新增角色程式關係
			  $programs=$this->libraries_model->_select('ij_sys_program_config','status','1',0,0,0,0,'result_array');
				if(is_array($programs)){
					foreach($programs as $_key=>$_value){
						$data_array2['sort_order']='8'; //預設
						$data_array2['status']='1'; //預設
						$data_array2['sys_program_config_sn']=$_value['sys_program_config_sn'];
						$data_array2['role_sn']=$role_sn;
						$data_array2 = $this->libraries_model->CheckUpdate($data_array2,1);
						$data_array2 = $this->libraries_model->CheckUpdate($data_array2,0);
						$this->libraries_model->_insert('ij_role_sys_program_relation',$data_array2);
					}
				}
			}
			return $_result;
	}

	public function action_update($_where,$_key_id="") {
			$_result="";
			foreach($this->_table_field as $key => $value)
			{
					if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
					$_data[$key]=$this->input->get_post($key);

			}
		$_data = $this->libraries_model->CheckUpdate($_data,0);
		$_result=$this->Sean_db_tools->db_update_record($_data,$this->_table,$_where,$_key_id,$this->_id);

		//同步異動角色程式對應
		$system_rule =$this->input->get_post('system_rule');
    //先異動全未選取
		$this->libraries_model->_update('ij_role_sys_program_relation',array('sort_order'=>'0'),'role_sn',$_key_id);
		//echo $this->db->last_query();
		//var_dump($system_rule);
		//exit();
		if(is_array($system_rule)){
			foreach($system_rule as $_key=>$_value){
				$data_array2['sort_order']=array_sum($_value);
				$data_array2 = $this->libraries_model->CheckUpdate($data_array2,0);
		//var_dump($data_array2);
		//echo '<br>';
				$this->libraries_model->_update('ij_role_sys_program_relation',$data_array2,'role_sys_program_relation_sn',$_key);
			}
		//exit();
		}
		//$data_array2['status']=array_sum($_value);
		//$data_array2 = $this->libraries_model->CheckUpdate($data_array2,0);
		//$this->libraries_model->_update('ij_role_sys_program_relation',$data_array2,'role_sys_program_relation_sn',$_key);
		return $_result;
	}

	public function action_delete( $_where,$_key_id) {
		$_result="";
		//一併刪除對應關係檔
		$this->Sean_db_tools->db_delete_record('ij_member_role_relation',$_where,$_key_id,$this->_id);
		$this->Sean_db_tools->db_delete_record('ij_role_sys_program_relation',$_where,$_key_id,$this->_id);
		$_result=$this->Sean_db_tools->db_delete_record($this->_table,$_where,$_key_id,$this->_id);
		return $_result;
	}
}

/* End of file Sys_role.php
			                    		 		<a href="'.$this->init_control.'sys_role_item/view/'.$value->role_sn.'"><i class="fa fa-eye"></i>預覽</a>&nbsp;&nbsp;
*/
/* Location: ./application/models/backend_system/Sys_role.php */
