<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品分類 <small>分類設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/admin">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品分類
        <i class="fa fa-circle"></i>
      </li>
      <li>
         <a href="admin/product/productCategory">分類管理</a>
         <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         商品分類 - <?php echo @$CategoryItem['category_name']; ?> 編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">分類設定</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/product/productCategory" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label for="name" class="col-md-3 control-label">是否發佈</label>
              <div class="col-md-8">
                <div class="radio-list">
                    <label class="radio-inline">
                      <input type="radio" name="category[category_status]" id="optionsRadios4" value="1" <?php if(@$CategoryItem['category_status']){ ?>checked<?php }?> required>  發佈  
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="category[category_status]" id="optionsRadios5" value="0" <?php if(@$CategoryItem['category_status']==='0'){ ?>checked<?php }?> required>  不發佈 
                    </label>
                </div>
              </div>
            </div>
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-md-3 control-label">名稱 <span class="require">*</span></label>
                  <div class="col-md-8">
                    <input type="text" name="category[category_name]" class="form-control" value="<?php echo @$CategoryItem['category_name']; ?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-md-3 control-label">副標</label>
                  <div class="col-md-8">
                    <input type="text" name="category[category_sub_name]" class="form-control" value="<?php echo @$CategoryItem['category_sub_name']; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-md-3 control-label">館別 <span class="require">*</span></label>
                  <div class="col-md-8">
                    <!-- 選項在 table ij_config 中的 type = shop_menu -->
                    <?php echo $Store;?>
                    <!--select class="form-control" name="category[]" required>
                      <option>雲端婚訊</option>
                      <option>現場報到系統</option>
                      <option>結婚商店</option>
                    </select-->
                  </div>
                </div>
                <div class="form-group">
                 <label class="col-md-3 control-label">上層分類 <span class="require">*</span></label>
                 <div class="col-md-8">
                  <?php echo $categorys;?>
                </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-md-3 control-label">分類屬性</label>
                  <div class="col-md-8">
                    <div class="checkbox-list">
                    	<?php foreach($category_attributes as $ca){ ?>
                      	<label class="checkbox-inline"><input type="checkbox" name="category_attribute[]" value="<?php echo @$ca['attribute_sn'];?>" <?php if(@$ca['status']){ echo 'checked';}?>/><?php echo @$ca['attribute_name'];?></label>
                      <?php } ?>
                    </div>
                  </div>
                </div>
                <?php /*
                <div class="form-group">
                  <label for="seq" class="col-md-3 control-label">預設分潤比例 <span class="require">*</span></label>
                  <div class="col-md-8">
                    <div class="form-group">
                      <div class="col-lg-1">
                        <input name="category[profit_sharing_rate]" type="number" maxlength="2" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" style="width:55px;" class="form-control form-inline" value="<?php echo (@$CategoryItem['profit_sharing_rate']) ? intval(@$CategoryItem['profit_sharing_rate']*100):'5'; ?>" required>
                      </div>
                      <div class="col-lg-1">
                        <span class="form-control-static form-inline">%</span>
                      </div>
                    </div>
                  </div>
                </div>
                */?>
                <div class="form-group">
                  <label class="col-md-3 control-label">分類 Banner</label>
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-lg-6">
                        <input class="form-control" name="banner" type="file" id="banner">
                      </div>
                      <div class="col-lg-6">
                        <label class="col-md-4 control-label">圖片描述</label>
                        <div class="col-md-8">
                          <input name="category[banner_desc]" type="text" class="form-control" placeholder="空白則為分類名稱" value="<?php echo @$CategoryItem['banner_desc']; ?>">
                        </div>
                      </div>
                    </div>
                    <span class="help-block">最佳大小 1046px X 300px</span>
                    <?php if(@$CategoryItem['banner_save_dir']){?><img class="img-responsive" src="<?php echo ADMINUPLOADPATH.@$CategoryItem['banner_save_dir']?>"><?php }?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">分類主圖（首頁）</label>
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-lg-6">
                        <input class="form-control" name="main_picture" type="file" id="main_picture"> <br>
                        <label class="col-md-4 control-label">圖片描述</label><br>
                          <input name="category[main_picture_desc]" type="text" class="form-control" placeholder="空白則為分類名稱" value="<?php echo @$CategoryItem['main_picture_desc']; ?>"><br>
                    			<span class="help-block">最佳大小 250px X 250px</span><br>
                        <input class="form-control" value="1" type="checkbox" <?=(@$CategoryItem['publish_flag']=='1')?'checked':''; ?> name="publish_flag" id="publish_flag">顯示於首頁
                      </div>
                      <div class="col-lg-6">
                      </div>

                      <div class="col-lg-6">
                    <?php if(@$CategoryItem['main_picture_save_dir']){?><img style="width:250px;height:250px;" class="img-responsive" src="<?php echo ADMINUPLOADPATH.@$CategoryItem['main_picture_save_dir']?>"><?php }?>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">分類圖（選單）</label>
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-lg-12">
                        <input class="form-control" name="main_banner" type="file" id="main_banner">
                      </div>
                    </div>
                    <span class="help-block">最佳大小 700px X 300px</span>
                    <?php if(@$CategoryItem['main_banner']){?><img class="img-responsive" src="<?php echo ADMINUPLOADPATH.@$CategoryItem['main_banner']?>"><?php }?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Meta Title</label>
                  <div class="col-md-8">
                    <input name="category[meta_title]" type="text" class="form-control maxlength-handler" name="product[meta_title]" maxlength="100" placeholder="" value="<?php echo (@$CategoryItem['meta_title'])?@$CategoryItem['meta_title']:@$CategoryItem['category_name']; ?>">
                    <span class="help-block"> 24個字元或12個中文字 </span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Meta Keywords</label>
                  <div class="col-md-8">
                    <textarea class="form-control maxlength-handler" rows="2" name="category[meta_keywords]" maxlength="256"><?php echo @$CategoryItem['meta_keywords']; ?></textarea>
                    <span class="help-block"> 36個字元或18個中文字 </span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Meta Description</label>
                  <div class="col-md-8">
                    <textarea class="form-control maxlength-handler" rows="2" name="category[meta_content]" maxlength="255"><?php echo @$CategoryItem['meta_content']; ?></textarea>
                    <span class="help-block"> 116個字元或 58個中文字 </span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">注意事項</label>
                  <div class="col-md-8">
                    <textarea name="category[return_desc]" class="form-control maxlength-handler" rows="3" name="" maxlength="255"><?php echo @$CategoryItem['return_desc']; ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="seq" class="col-md-3 control-label">排序 <span class="require">*</span></label>
                  <div class="col-md-8">
                    <input name="category[sort_order]" maxlength="4" type="text" class="form-control" value="<?php echo (@$CategoryItem['sort_order']) ? @$CategoryItem['sort_order']:'9999'; ?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-md-3 control-label">更新者</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" placeholder="<?php echo (@$CategoryItem['update_member_sn'])?@$CategoryItem['update_member_sn']:@$CategoryItem['create_member_sn']; ?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-md-3 control-label">更新時間</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" placeholder="<?php echo (@$CategoryItem['last_time_update']!='0000-00-00 00:00:00')?@$CategoryItem['last_time_update']:@$CategoryItem['create_date']; ?>" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <input type="hidden" name="category_sn" value="<?php echo @$CategoryItem['category_sn']; ?>">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="reset" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->