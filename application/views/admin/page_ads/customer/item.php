<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<!-- END PAGE LEVEL STYLES -->
<style>
  .feature-img{vertical-align: bottom;}
  .adType{display: none;}
</style>


<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>廣告客戶設定</h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/marketing/adsCustomer">廣告客戶</a><i class="fa fa-circle"></i>
      </li>
      <li class="active">
        廣告客戶設定
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">廣告客戶設定</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/marketing/adsCustomer" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
			            <form class="form-horizontal" role="form" method="post">
			              <div class="form-body">
			                <div class="form-group">
			                  <label for="name" class="col-lg-3 control-label">客戶名稱<span class="require">*</span></label>
			                  <div class="col-lg-8">
			                    <input name="Item[banner_customer_name]" required type="text" class="form-control" value="<?php echo @$Item['banner_customer_name'];?>">
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="name" class="col-lg-3 control-label">起始時間<span class="require">*</span></label>
			                  <div class="col-lg-4">
			                    <div class="input-group date form_datetime">
			                      <input name="Item[partnership_start_date]" required type="text" class="form-control" value="<?php echo (@$Item['partnership_start_date']!='0000-00-00 00:00:00')?@$Item['partnership_start_date']:''; ?>">
			                      <span class="input-group-btn">
			                        <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
			                      </span>
			                    </div>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="name" class="col-lg-3 control-label">結束時間<span class="require">*</span></label>
			                  <div class="col-lg-4">
			                    <div class="input-group date form_datetime">
			                    	<input name="Item[partnership_end_date]" required type="text" class="form-control" value="<?php echo (@$Item['partnership_end_date']!='0000-00-00 00:00:00')?@$Item['partnership_end_date']:''; ?>">
			                      <span class="input-group-btn">
			                        <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
			                      </span>
			                    </div>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="name" class="col-lg-3 control-label">經銷商</label>
			                  <div class="col-lg-4">
			                    <select name="Item[associated_dealer_sn]" class="form-control">
			                      <option value="">請選擇</option>
				                    <?php foreach(@$_member_dealer as $_Item){?>
				                    <option <?php echo (@$Item['associated_dealer_sn']==$_Item['dealer_sn'])?'selected':'';?> value="<?php echo @$_Item['dealer_sn']?>"><?php echo @$_Item['dealer_short_name'];?></option>
				                    <?php }?>
			                    </select>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="active" class="col-lg-3 control-label">供應商</label>
			                  <div class="col-lg-4">
			                   <select name="Item[associated_supplier_sn]" id="associated_supplier_sn" class="form-control">
			                      <option value="">請選擇</option>
			                    <?php if($suppliers){foreach($suppliers as $key=>$_Item){ ?>
			                      <option value="<?php echo $_Item['supplier_sn'];?>" <?php echo (@$Item['associated_supplier_sn']==$_Item['supplier_sn'])?'selected':'';?>><?php echo $_Item['supplier_name'];?></option>
			                    <?php }}?>
			                    </select>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="active" class="col-lg-3 control-label">業務員</label>
			                  <div class="col-lg-4">
			                   <select name="Item[associated_sales_member_sn]" id="associated_sales_member_sn" class="form-control">
			                      <option value="">請選擇</option>
			                    <?php if($sales){foreach($sales as $key=>$_Item){ ?>
			                      <option value="<?php echo $_Item['member_sn'];?>" <?php echo (@$Item['associated_sales_member_sn']==$_Item['member_sn'])?'selected':'';?>><?php echo $_Item['last_name'].$_Item['first_name'];?></option>
			                    <?php }}?>
			                    </select>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="active" class="col-lg-3 control-label">客服人員</label>
			                  <div class="col-lg-4">
			                   <select name="Item[associated_ccagent_member_sn]" id="associated_ccagent_member_sn" class="form-control">
			                      <option value="">請選擇</option>
			                    <?php if($agents){foreach($agents as $key=>$_Item){ ?>
			                      <option value="<?php echo $_Item['member_sn'];?>" <?php echo (@$Item['associated_ccagent_member_sn']==$_Item['member_sn'])?'selected':'';?>><?php echo $_Item['last_name'].$_Item['first_name'];?></option>
			                    <?php }}?>
			                    </select>
			                  </div>
			                </div>
				              <div class="form-group">
				                <label for="email" class="col-lg-3 control-label">統一編號</label>
				                <div class="col-lg-4">
				                  <input type="text" maxlength="8" class="form-control" name="Item[dealer_tax_id]" value="<?php echo @$Item['dealer_tax_id']; ?>" >
				                </div>
				              </div>
					            <div class="form-group">
					              <label class="col-lg-3 control-label">電話<span class="require">*</span></label>
					              <div class="col-lg-8">
					                <div class="form-inline">
					                    <input type="text" required id="office_tel_area_code" class="form-control" name="Item[office_tel_area_code]" size="4" maxlength="3" placeholder="區碼" value="<?php echo @$Item["office_tel_area_code"];?>">
					                    <input type="text" required id="office_tel" class="form-control" name="Item[office_tel]" size="16" maxlength="10" placeholder="請輸入市話號碼" value="<?php echo @$Item["office_tel"];?>">
					                    <input type="text" id="office_tel_ext" class="form-control" name="Item[office_tel_ext]" size="4" placeholder="分機" value="<?php echo @$Item["office_tel_ext"];?>">
					                </div>
					              </div>
					            </div>
					            <div class="form-group">
					              <label class="col-lg-3 control-label">傳真</label>
					              <div class="col-md-8">
					                <div class="form-inline">
					                    <input type="text" id="office_fax_area_code" class="form-control" name="Item[office_fax_area_code]" size="4" maxlength="3" placeholder="區碼" value="<?php echo @$Item["office_fax_area_code"];?>">
				                    	<input type="text" id="office_fax" class="form-control" name="Item[office_fax]" size="16" maxlength="10" placeholder="請輸入傳真號碼" value="<?php echo @$Item["office_fax"];?>">
					                </div>
					              </div>
					            </div>
			                <div class="form-group">
			                  <label for="active" class="col-lg-3 control-label">是否啟用</label>
			                  <div class="col-lg-8">
			                    <select name="Item[status]" class="form-control">
			                      <option value="1" <?php if(@$Item['status']=='1'){ ?>selected<?php }?>>是</option>
			                      <option value="2" <?php if(@$Item['status']=='2'){ ?>selected<?php }?>>否</option>
			                    </select>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="updater" class="col-lg-3 control-label">更新者</label>
			                  <div class="col-lg-8">
			                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['update_member_sn'])?@$Item['update_member_sn']:@$Item['create_member_sn']; ?>" Disabled>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="time-update" class="col-lg-3 control-label">更新時間</label>
			                  <div class="col-lg-8">
			                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['last_time_update']!='0000-00-00 00:00:00')?@$Item['last_time_update']:@$Item['create_date']; ?>" Disabled>
			                  </div>
			                </div>
			              </div>
			              <div class="form-actions">
			                <div class="row">
			                  <div class="col-md-offset-3 col-md-9">
			                    <input type="hidden" name="banner_customer_sn" value="<?php echo @$Item['banner_customer_sn']; ?>">
			                    <button type="submit" class="btn green">儲存</button>
			                    <button type="button" class="btn default">Cancel</button>
			                  </div>
			                </div>
			              </div>
			            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->