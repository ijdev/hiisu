<?php
class Fb_user extends CI_Model
{
    private $user_table_name = 'user',
    	    $via_fb_table_name = 'user_via_fb';
 
    function __construct()
    {
        parent::__construct();
    }
 
    public function get_uid_from_fbid($fbid)
    {
    	$this->db->select('user_uid');
    	$this->db->where('fbid',$fbid);
    	$uid = $this->db->get($this->via_fb_table_name)->row();
    	return ($uid==NULL)?(0):($uid->user_uid);
    }
    public function get_user_access_token($uid)
    {
        $this->db->select('u_state,u_access_token');
        $this->db->where('uid',$uid);
        $data = $this->db->get($this->user_table_name)->row();
        if($data->u_state==1)
        {
            return $data->u_access_token;
        }
        else
        {
            return 0;
        }
    }
}
?>
