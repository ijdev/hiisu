<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script>
jQuery(document).ready(function() {       
   	Metronic.init(); // init metronic core components
	  Layout.init(); // init current layout
    $('.fancybox').fancybox();

    // init ADTypeSelector
    adTypeChange();
    jQuery('select[name="Item[banner_type]"]').change(function(){
		adTypeChange();
    });
		$(".deleteTemplateDivider").click(function(){
        var button = $(this);
        	var r = confirm('是否刪除此張圖檔?');
            if(r)
            {
						    $.post('/admin/admin/ajaxdel/', {dtable: '<?php echo base64_encode('ij_banner_location');?>',dfield:'<?php echo base64_encode('banner_location_sn');?>',dfilename:button.attr('default_banner_content_save_dir'),did:button.attr('ItemId')}, function(data){
                	if (data){
                		button.parent().html('<input class="form-control" name="banner" type="file" id="banner">');
                		//button.parent().remove();
                	}else{
                		alert('刪除失敗');
                	}
						    });
            }
    });    
});
function adTypeChange(){
	jQuery('.adType').hide();
	//console.log(jQuery('select[name="Item[banner_type]"] option:selected').attr('banner_type_eng_name'));
	jQuery('.'+jQuery('select[name="Item[banner_type]"]  option:selected').attr('banner_type_eng_name')+'AD').show();
	$('#banner_type_eng_name').val(jQuery('select[name="Item[banner_type]"]  option:selected').attr('banner_type_eng_name'));
	//jQuery('.'+jQuery('select[name="Item[banner_type]"]  option:selected').attr('banner_type_eng_name')+'AD input').prop('disabled',false);
}

</script>

