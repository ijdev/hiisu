<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Main extends MY_Controller {
	 	public function __construct()
		{
			 parent::__construct();

				$this->load->library('session');
				$this->load->helper('form');
				$this->load->helper('url');
				$this->load->library('form_validation');
				$this->load->library('sean_lib/Sean_form_general');
				$this->load->model('general/My_general_ct');
				$this->load->config('ij_config');
				$this->load->config('ij_sys_config');
				$this->load->model('sean_models/Sean_general');
				$this->load->model('sean_models/Sean_db_tools');
				$this->load->model('libraries_model');
				$this->load->helper('cookie');

				$this->page_data = array();
				$this->page_data['unreadmessage']= $this->libraries_model->get_unread_message_count();
				$this->page_data['init_control']="admin/main/";
				$this->page_data['_home_url']="";
				$this->init_control=$this->page_data['init_control'];
				$this->page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
				$this->page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$this->page_data['page_content']['view_path'] = 'sean_admin/page_home/page_content'; // 頁面主內容 view 位置, 必要
				$this->page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_home/page_content_js'; // 頁面主內容的 JS view 位置, 必要
				$this->lang->load('general', 'zh-TW');
				$this->page_data["_per_page"]=2;
				//$this->output->enable_profiler(TRUE);
		}


	public function index()
	{
		redirect(base_url('admin/dashboard'));
		$page_data = array();

		// 內容設定

	    $this->LoginCheck();
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_home/page_content'; // 頁面主內容 view 位置, 必要
		$page_data['page_content']['total_amount']=$this->libraries_model->get_order_statics('sum(total_order_amount) as t')['t'];
		$page_data['page_content']['total_orders']=$this->libraries_model->get_order_statics('count(*) as t')['t'];
		$page_data['page_content']['average_amount']=($page_data['page_content']['total_orders']>0)?$page_data['page_content']['total_amount']/$page_data['page_content']['total_orders']:'0';
		$_where="date_format(order_create_date, '%Y%m') = date_format(curdate() , '%Y%m')"; //本月
		$page_data['page_content']['current_month_total_amount']=$this->libraries_model->get_order_statics('sum(total_order_amount) as t',$_where)['t'];;
		$page_data['page_content']['current_month_total_orders']=$this->libraries_model->get_order_statics('count(*) as t',$_where)['t'];
		$page_data['page_content']['current_month_average_amount']=($page_data['page_content']['current_month_total_orders']>0)?$page_data['page_content']['current_month_total_amount']/$page_data['page_content']['current_month_total_orders']:'0';

		$page_data['page_content']['hot_sales']=$this->libraries_model->get_order_item_statics($_where);
		$page_data['page_content']['click_sales']=$this->libraries_model->get_order_item_statics($_where,'click_number desc');

		$page_data['page_content']['sub_orders']=$this->libraries_model->get_orders($_where);
		$page_data['page_content']['members']=$this->libraries_model->get_order_members($_where);
	    $chart_array1=$this->libraries_model->get_order_statics_period('count(*)');
		$page_data['page_content']['chart_array1']=json_encode($chart_array1,JSON_HEX_AMP);
	    $chart_array2=$this->libraries_model->get_order_statics_period();
		$page_data['page_content']['chart_array2']=json_encode($chart_array2,JSON_HEX_AMP);

		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_home/page_content_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}


/* ------------------------------------------------------ Begin 總控系統管理---------------------------------------------------*/


	/*
	*系統程式
	*/

	public function Sys_program($_id = 0,$_name = null)
	{

		$this->LoginCheck();
		$this->page_data['_page_title']="<h1>系統程式 <small>列表</small></h1>";
    $this->page_data['_caption_subject']= "系統程式列表";


    	if($_id > 0)
    	$this->page_data['_caption_subject']= "<a href=admin/main/Sys_program/>系統程式列表</a> - <font color=red>[".urldecode($_name)."]</font>";

    	$this->session->set_userdata('sys_program_father_id',$_id);
    	$this->session->set_userdata('sys_program_father_title',$_name);



     $this->page_data['_father_link']="Sys_program";
     $this->page_data['_action_link']=$this->page_data['init_control']."sys_program_item/add/$_id";



    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    $this->load->model('backend_system/Sys_program');
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";
    $_result=$this->Sys_program->table_value_list($_where,$_id);

    /* End model 查詢結果，回傳 */

    // 列表標頭
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// general view setting
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_role'; // 頁面主內容 view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

  /*
  * 系統程式 新增 編輯 修改 刪除
  */
	public function sys_program_item($_action,$_id = 0)
	{
		$this->LoginCheck();

    $this->page_data['_page_title']="<h1>系統程式 <small>列表與搜尋</small></h1>";
    $this->page_data['_caption_subject']="系統程式編輯";
    $this->page_data['_father_link']="Sys_program/".$this->session->userdata('sys_program_father_id')."/".$this->session->userdata('sys_program_father_title');
    $this->page_data['_action_link']=$this->page_data['init_control']."sys_program_item/adding/$_id";

    $this->load->model('backend_system/Sys_program');
($_id > 0 ) ? $this->Sys_program->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->Sys_program->set_define_value("_action",$_action) : "";

    $_next_sort=1;
    $_next_sort=$this->Sys_program->get_next_sort($_id);
    //編輯
    if($_action=="edit")
		{
			$_where=" where sys_program_config_sn='".$_id."'";
			$_result01=$this->Sys_program->action_edit($_where);
			$this->page_data['_action_link']=$this->page_data['init_control']."sys_program_item/update/$_id";
			$this->page_data['_result01']=$_result01;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where sys_program_config_sn='".$_id."'";
			$_result01=$this->Sys_program->action_update($_where,$_id);
			$this->session->set_flashdata("success_message","更新完成！");
			redirect("admin/Sysconfig/".$this->session->userdata('sys_program_father_id')."/".$this->session->userdata('sys_program_father_title'));
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where sys_program_config_sn ='".$_id."' ";
			$_result01=$this->Sys_program->action_delete($_where,$_id);
			if(!is_array($_result01))
			{
			$this->session->set_flashdata("success_message","刪除完成！");
		  }else{
				$_error_message=$this->Sean_db_tools->db_message_handle($_result01["message"]);
				$this->session->set_flashdata("message","刪除失敗 ==> ".$_error_message);
		  }
		  redirect("Sysconfig/".$this->session->userdata('sys_program_father_id')."/".$this->session->userdata('sys_program_father_title'));
			exit();
		}


    //新增
		if($_action == "adding")
		{

			$_result=$this->Sys_program->action_insert($_id);
			$this->session->set_flashdata("success_message","新增完成！");

		}




    /*
    item  list
    */
    $this->page_data["em_columns"] = array(
		  //'delimiter_1'=>array('inner_html'=>'<span style="font-size:13px;font-weight:bold;color:#336699"><i>'.$_lang["delimiter_1"].'</i></span><br />'),
			'sys_program_name'=>array('header'=>$this->lang->line("sys_program_name"),'type'=>'textbox', 'required'=>'required', 'required_message'=>$this->lang->line("val_sys_program_name"),'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sys_program_eng_name'=>array('header'=>$this->lang->line("sys_program_eng_name"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>$this->lang->line("val_sys_program_eng_name"), 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sys_program_url'=>array('header'=>$this->lang->line("sys_program_url"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>$this->lang->line("val_sys_program_url"), 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$this->lang->line("sort_order"), 'type'=>'number',"data_validation_number_message"=>"", 'required'=>'required','required_message'=>$this->lang->line("val_sort_order"),'form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'status'=>array('header'=>$this->lang->line("status"), 'type'=>'status', 'required_message'=>$this->lang->line("status"),'required'=>'required','form_control'=>'','maxlength'=>'3', 'default'=>'1',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );


		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item'; // 頁面主內容 view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('sean_admin/general/main', $this->page_data);
	}



	/*
	*系統參數
	*/

	public function Sys_param($_id = 0,$_name = null)
	{


		/* Begin 修改 */
		//載入 model
		$_model_name="Sys_param";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

   /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];



		// general view setting
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_role'; // 頁面主內容 view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

  /*
  * 系統參數 新增 編輯 修改 刪除
  */
	public function Sys_param_item($_action,$_id = 0)
	{
		$this->LoginCheck();

    $this->page_data['_page_title']="<h1>系統程式 <small>列表與搜尋</small></h1>";
    $this->page_data['_caption_subject']="系統程式編輯";
    $this->page_data['_father_link']="Sys_program/".$this->session->userdata('sys_program_father_id')."/".$this->session->userdata('sys_program_father_title');
    $this->page_data['_action_link']=$this->page_data['init_control']."sys_program_item/adding/$_id";

    $this->load->model('backend_system/Sys_program');
($_id > 0 ) ? $this->Sys_program->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->Sys_program->set_define_value("_action",$_action) : "";

    $_next_sort=1;
    $_next_sort=$this->Sys_program->get_next_sort($_id);
    //編輯
    if($_action=="edit")
		{
			$_where=" where sys_program_config_sn='".$_id."'";
			$_result01=$this->Sys_program->action_edit($_where);
			$this->page_data['_action_link']=$this->page_data['init_control']."sys_program_item/update/$_id";
			$this->page_data['_result01']=$_result01;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where sys_program_config_sn='".$_id."'";
			$_result01=$this->Sys_program->action_update($_where,$_id);
			$this->session->set_flashdata("success_message","更新完成！");
			redirect("Sysconfig/".$this->session->userdata('sys_program_father_id')."/".$this->session->userdata('sys_program_father_title'));
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where sys_program_config_sn ='".$_id."' ";
			$_result01=$this->Sys_program->action_delete($_where,$_id);
			if(!is_array($_result01))
			{
			$this->session->set_flashdata("success_message","刪除完成！");
		  }else{
				$_error_message=$this->Sean_db_tools->db_message_handle($_result01["message"]);
				$this->session->set_flashdata("message","刪除失敗 ==> ".$_error_message);
		  }
		  redirect("Sysconfig/".$this->session->userdata('sys_program_father_id')."/".$this->session->userdata('sys_program_father_title'));
			exit();
		}


    //新增
		if($_action == "adding")
		{

			$_result=$this->Sys_program->action_insert($_id);
			$this->session->set_flashdata("success_message","新增完成！");

		}




    /*
    item  list
    */
    $this->page_data["em_columns"] = array(
		  //'delimiter_1'=>array('inner_html'=>'<span style="font-size:13px;font-weight:bold;color:#336699"><i>'.$_lang["delimiter_1"].'</i></span><br />'),
			'sys_program_name'=>array('header'=>$this->lang->line("sys_program_name"),'type'=>'textbox', 'required'=>'required', 'required_message'=>$this->lang->line("val_sys_program_name"),'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sys_program_eng_name'=>array('header'=>$this->lang->line("sys_program_eng_name"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>$this->lang->line("val_sys_program_eng_name"), 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sys_program_url'=>array('header'=>$this->lang->line("sys_program_url"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>$this->lang->line("val_sys_program_url"), 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$this->lang->line("sort_order"), 'type'=>'number',"data_validation_number_message"=>"", 'required'=>'required','required_message'=>$this->lang->line("val_sort_order"),'form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'status'=>array('header'=>$this->lang->line("status"), 'type'=>'status', 'required_message'=>$this->lang->line("status"),'required'=>'required','form_control'=>'','maxlength'=>'3', 'default'=>'1',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );


		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item'; // 頁面主內容 view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('sean_admin/general/main', $this->page_data);
	}





/*
	*系統資源
	*/

	public function Sys_resource($_id = 0,$_name = null)
	{


		$this->page_data['_page_title']="<h1>系統資源 <small>列表</small></h1>";
    $this->page_data['_caption_subject']= "系統資源列表";


    $this->page_data['_father_link']="Sys_resource";
    $this->page_data['_action_link']=$this->page_data['init_control']."sys_resource_item/add/$_id";



    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    $this->load->model('backend_system/Sys_resource');
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";
    $_result=$this->Sys_resource->table_value_list($_where,$_id);

    /* End model 查詢結果，回傳 */

    // 列表標頭
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// general view setting
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_role'; // 頁面主內容 view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

  /*
  * 系統程式 新增 編輯 修改 刪除
  */
	public function sys_resource_item($_action,$_id = 0)
	{
		//$this->session->userdata('sys_program_father_title')

    $this->page_data['_page_title']="<h1>系統資源 <small>編輯</small></h1>";
    $this->page_data['_caption_subject']="系統資源編輯";

    $this->page_data['_father_link']="Sys_resource";
    $this->page_data['_action_link']=$this->page_data['init_control']."sys_resource_item/adding/$_id";

    $this->load->model('backend_system/'.$this->page_data['_father_link']);

    $_next_sort=1;
    $_next_sort=$this->Sys_resource->get_next_sort($_id);
    $_key=$this->Sys_resource->get_define_value("id");

    //編輯
    if($_action=="edit")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result01=$this->Sys_resource->action_edit($_where);
			$this->page_data['_action_link']=$this->page_data['init_control']."sys_resource_item/update/$_id";
			$this->page_data['_result01']=$_result01;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result01=$this->Sys_resource->action_update($_where);
			$this->session->set_flashdata("success_message","更新完成！");
			redirect("".$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key." ='".$_id."' ";
			$_result01=$this->Sys_resource->action_delete($_where);
			$this->session->set_flashdata("success_message","刪除完成！");
			redirect("".$this->page_data['_father_link']);
			exit();
		}


    //新增
		if($_action == "adding")
		{

			$_result=$this->Sys_resource->action_insert($_id);
			$this->session->set_flashdata("success_message","新增完成！");

		}




    /*
    item  list
    */


		$_array_sys_program="";
    $_where=" where status = 1 order by sort_order ";
    $_array_sys_program=$this->Sean_db_tools->db_get_max_record("sys_program_config_sn as kkey ,sys_program_name as vvalue","ij_sys_program_config",$_where);

    $this->page_data["em_columns"] = array(

		  'sys_program_sn'=>array('header'=>"系統程式代號",'type'=>'enum', 'required'=>'', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'source'=>$_array_sys_program,'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'64', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sys_resource_name'=>array('header'=>'資源中文名稱', 'type'=>'textbox','required'=>'required','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'60', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sys_resource_eng_name'=>array('header'=>'資源英文名稱', 'type'=>'textbox','required'=>'required','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'60', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'resource_rule_code'=>array('header'=>'資源規則代碼', 'type'=>'textbox','required'=>'required','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'32', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

			'hidden_flag'=>array('header'=>$this->lang->line("hide_flag"), 'type'=>'hide_flag', 'required_message'=>$this->lang->line("hide_flag"),'required'=>'required','form_control'=>'','maxlength'=>'3', 'default'=>'1',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	  'sort_order'=>array('header'=>$this->lang->line("sort_order"), 'type'=>'number',"data_validation_number_message"=>"", 'required'=>'required','required_message'=>$this->lang->line("val_sort_order"),'form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'status'=>array('header'=>$this->lang->line("status"), 'type'=>'status', 'required_message'=>$this->lang->line("status"),'required'=>'required','form_control'=>'','maxlength'=>'3', 'default'=>'1',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );


		$this->page_data['page_content']['view_path'] = 'sean_admin/general/general_item'; // 頁面主內容 view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('sean_admin/general/main', $this->page_data);
	}




   /*
   * 系統角色列表
   */
   public function Sys_role()
	{
	  $this->LoginCheck();


		//標題
		$this->page_data['_page_title']="<h1>管理員分類 <small>列表</small></h1>";
    $this->page_data['_caption_subject']= "管理員分類列表";

    // 返回連結
    $this->page_data['_father_link']="Sys_role";
    $this->page_data['_action_link']=$this->page_data['init_control']."sys_role_item/add";


    /*
    	Begin 載入Sys_role model 查詢結果
      回傳  _table_thead_name _body_result
    */

    $this->load->model('backend_system/Sys_role');

    //特殊條件  $_where="  order by sort_order asc";
    $_where="";
    $_result=$this->Sys_role->table_value_list($_where);

    /* End 載入Sys_role model 查詢結果，回傳 */

    // 列表標頭
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];



		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_role';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}


/*
*
* 系統角色 新增修改刪除
*
*/
	public function sys_role_item($_action,$_id = 0)
	{
	  $this->LoginCheck();

		//標題
    $this->page_data['_page_title']="<h1>系統角色 <small>列表</small></h1>";
    $this->page_data['_caption_subject']= "系統角色列表";

    // 返回連結
    $this->page_data['_father_link']="Sys_role/";
    $this->page_data['_action_link']=$this->page_data['init_control']."sys_role_item/adding/$_id";

    //載入Sys_role model
    $this->load->model('backend_system/Sys_role');
    $_next_sort=1;
    $_next_sort=$this->Sys_role->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
		{

			$_result="";
			$_where=" where role_sn='".$_id."'";
			$_result=$this->Sys_role->action_edit($_where);

			$this->page_data['_action_link']=$this->page_data['init_control']."sys_role_item/update/$_id";
			$this->page_data['_result01']=$_result;
		}

		//更新
		if($_action=="update")
		{

			$_where=" where role_sn='".$_id."'";
			$_result=$this->Sys_role->action_update($_where,$_id);

			$this->session->set_flashdata("success_message","更新完成！");
			redirect(base_url("admin/main/Sys_role/"));
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where role_sn ='".$_id."' ";
			$_result=$this->Sys_role->action_delete($_where);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect(base_url("admin/main/Sys_role/"));
			exit();
		}

    //新增
		if($_action == "adding")
		{

			$_result=$this->Sys_role->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");

		}



    /*
    		產生表單
    */
    $this->page_data["em_columns"] = array(
		  //'delimiter_1'=>array('inner_html'=>'<span style="font-size:13px;font-weight:bold;color:#336699"><i>'.$_lang["delimiter_1"].'</i></span><br />'),
			'role_name'=>array('header'=>'角色中文名稱','type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>$this->lang->line("val_role_name"),'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'role_eng_name'=>array('header'=>'角色英文名稱', 'type'=>($_action=="view") ? 'label':'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>$this->lang->line("val_role_eng_name"), 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'role_descrption'=>array('header'=>$this->lang->line("role_descrption"), 'type'=>($_action=="view") ? 'label':'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>$this->lang->line("val_role_descrption"), 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$this->lang->line("sort_order"), 'type'=>($_action=="view") ? 'label':'number',"data_validation_number_message"=>"", 'required'=>'required','required_message'=>$this->lang->line("val_sort_order"),'form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'status'=>array('header'=>$this->lang->line("status"), 'type'=>($_action=="view") ? 'label':'status', 'required_message'=>$this->lang->line("status"),'required'=>'required','form_control'=>'','maxlength'=>'3', 'default'=>'1',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 );

	 	/*if($_action=="view" or $_action=="edit" )
		{

			$this->page_data["em_columns"]['create_date']=array('header'=>$this->lang->line("create_date"),'type'=> 'label');
		  $this->page_data["em_columns"]['create_member_sn']=array('header'=>$this->lang->line("create_member_sn"),'type'=> 'label');

		  $this->page_data["em_columns"]['last_time_update']=array('header'=>$this->lang->line("last_time_update"),'type'=> 'label');
		  $this->page_data["em_columns"]['update_member_sn']=array('header'=>$this->lang->line("update_member_sn"),'type'=> 'label');
		}*/

	    $this->page_data['page_content']['category_attributes'] = $this->libraries_model->get_system_rule(63);//列出全部權限
	    if($_id!='1'){
	    	$this->page_data['page_content']['addons'] = $this->libraries_model->get_system_programs($_id);
	    }else{
			$this->page_data['page_content']['addons'] =array();
	    }

		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_role_item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_program_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}






	/**
	 * 會員列表 Template
	 *
	 */
	public function sys_member()
	{
	  $this->LoginCheck();

		//$where=array('message_status'=>'1','ij_member_message_center_log.send_count >'=>'0');

		$this->page_data['_father_link']="memberTable";
	    $this->page_data['_action_link']=$this->page_data['init_control']."sys_member_item/add";

	    $_where="
	    left join  ij_member_level_ct on  ij_member_level_ct.member_level_code=ij_member.member_level_type
	    left join  ij_member_status_ct on  ij_member_status_ct.member_status=ij_member.member_status
	    where (member_level_type >=4  and system_user_flag=1)  order by member_sn desc";

		$this->load->config('member_config');

		$_result01=$this->Sean_db_tools->db_get_max_record("ij_member.*,ij_member_level_ct.level_name as level_name_ct,ij_member_status_ct.member_status_name as member_status_ct","ij_member",$_where);
		foreach($_result01 as $_key=>$_value){
			$login_log=$this->libraries_model->_select('ij_member_login_log',array('member_sn'=>$_value->member_sn),0,0,0,'login_date','desc','result_array');
			if($login_log){
				$_result01[$_key]->last_login_time=$login_log[0]['login_date'];
				$_result01[$_key]->login_count=count($login_log);
			}else{
				$_result01[$_key]->last_login_time='';
				$_result01[$_key]->login_count='0';
			}
			$roles=implode('、',array_column($this->libraries_model->get_member_roles($_value->member_sn,1),'role_name'));
			//var_dump($roles);
			$_result01[$_key]->roles=$roles;
		}

		$this->page_data['_result01']=$_result01;
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_member'; // 頁面主內容 view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_member_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	/**
	 * 會員編輯 Template
	 *
	 */
	public function sys_member_item($_action,$_id = 0)
	{
	    $this->LoginCheck();

		// 內容設定
		$this->page_data['_father_link']="sys_member";
		$this->page_data['_action_now']=$_action;
    	$this->page_data['_action_link']=$this->page_data['init_control']."sys_member_item/adding";

		$this->page_data['page_content']['view_path'] = 'sean_admin/page_sys/sys_member_item'; // 頁面主內容 view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_sys/sys_member_item_js'; // 頁面主內容的 JS view 位置, 必要

		$this->load->library('ijw');
		if(!$permission=$this->ijw->check_permission()){ //判斷是否能修改其他id
			if($_id!=$this->session->userdata['member_data']['member_sn']){
					$this->ijw->_wait(base_url("/admin/main/sys_member_item/edit/".$this->session->userdata['member_data']['member_sn']) , 2 , '很抱歉，您登入的角色權限只能修改自己的帳號喔！');
			}
			$ifeditself=true;
		}else{
			$ifeditself=false;
		}
		$_where=" where display_flag='1' and member_level_type > 3 order by sort_order";
		$_member_level_types=$this->Sean_db_tools->db_get_max_record("*","ij_member_level_type_ct",$_where);
		$this->page_data['_member_level_types']=$_member_level_types;

		//編輯
    if($_action=="edit")
		{
			//$this->Check_page_permission('search','查詢');

			$this->page_data['page_content']['ifeditself'] = $ifeditself;

			$_where=" where member_sn='".$_id."'";
			$_result01=$this->Sean_db_tools->db_get_max_record("*","ij_member",$_where);
			$this->page_data['_action_link']=$this->page_data['init_control']."sys_member_item/update/$_id";
			$this->page_data['_result01']=$_result01;

			$flg=$this->input->post('flg',true);
			if($flg=='reset'){
				redirect(base_url('admin/admin/ticketTable'));
				exit();
			}else{
				$from=($this->input->post('from',true))? $this->input->post('from',true):date('Y-m-d', strtotime(date('Y-m-d'). ' - 7 days'));
				$to=($this->input->post('to',true))? $this->input->post('to',true):date('Y-m-d');
			}
			$this->page_data['page_content']['from'] = $from;
			$this->page_data['page_content']['to'] = $to;
			$this->page_data['page_content']['_id'] = $_id;

			$where=array();
			if($from) $where['issue_date >=']=$from.' 00:00:00';
			if($to) $where['issue_date <=']=$to.' 23:59:59';
			//$where['issue_date']=
			$this->page_data['page_content']['messages'] = $this->libraries_model->_get_callcenter_list($where,$this->session->userdata['member_data']['member_sn']);
			$this->page_data['page_content']['member_roles'] = $this->libraries_model->get_member_roles($_id);
		}

		//更新
		if($_action=="update")
		{
			$_data["email"]=$this->input->get_post("email");
			$_data["last_name"]=$this->input->get_post("last_name");
			$_data["first_name"]=$this->input->get_post("first_name");
			$_data["tel"]=$this->input->get_post("tel");
			$_data["member_level_type"]=$this->input->get_post("member_level_type");
			$_data["cell"]=$this->input->get_post("cell");
			$_data["member_status"]=$this->input->get_post("member_status");
			$_data["memo"]=$this->input->get_post("memo");
			if(strlen($this->input->get_post("password")) > 0 )
			$_data["password"]=md5($this->input->get_post("password"));

			$_data["system_user_flag"]=1; //2016-1-19 wdj 暫時方便變更會員為管理者


			$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			$_data["last_time_update"]=date("Y-m-d H:i:s");

			$_where=" where member_sn='".$_id."'";
			$_result01=$this->Sean_db_tools->db_update_record($_data,"ij_member",$_where,$_id,'member_sn');
			//權限角色設定
			if(!$ifeditself){ //權限不是只能修改自己帳號的才會有角色設定
				//$this->Check_page_permission('edit','修改');
				$member_roles=$this->input->get_post("member_roles");
				if($member_roles){
					$_data2=array('status'=>'0');
					$_data2 = $this->libraries_model->CheckUpdate($_data2,0);
					$this->libraries_model->_update('ij_member_role_relation',$_data2,'ij_member_role_relation.member_sn',$_id); //先都把status=0
					//var_dump($member_roles);
				    //exit();
					foreach($member_roles as $_key=>$_value){
						$_data2=array('status'=>'1');
						$_data2 = $this->libraries_model->CheckUpdate($_data2,0);
						$this->libraries_model->_update('ij_member_role_relation',$_data2,'member_role_relation_sn',$_key); //有選更新status=1
					}
				}
			}
			$this->session->set_flashdata("success_message","更新完成！");
			redirect(base_url("admin/main/sys_member_item/edit/".$_id));
			exit();
		}

		//新增
		if($_action == "add")
		{
			$this->Check_page_permission('add','新增');
		}
		if($_action == "adding")
		{
			$_data["user_name"]=$this->input->get_post("user_name");
			if($this->libraries_model->_selectifsame('ij_member','user_name',$_data["user_name"])){
	        	$this->load->library('ijw');
				$this->ijw->_wait('/' , 2 , '很抱歉！該帳號已被使用，請更換。',1);
			}
			$this->Check_page_permission('add','新增');
			$_data["email"]=$this->input->get_post("email");
			$_data["last_name"]=$this->input->get_post("last_name");
			$_data["first_name"]=$this->input->get_post("first_name");
			$_data["tel"]=$this->input->get_post("tel");
			$_data["member_level_type"]=$this->input->get_post("member_level_type");
			$_data["cell"]=$this->input->get_post("cell");
			$_data["member_status"]=$this->input->get_post("member_status");
			$_data["memo"]=$this->input->get_post("memo");
			if(strlen($this->input->get_post("password")) > 0 )
			$_data["password"]=md5($this->input->get_post("password"));

			$_data["system_user_flag"]=1;
			$_data["member_level_type"]=$this->input->get_post("member_level_type");
			//$_data["member_level_type"]=9;

			//basic insert
			//basic update and insert
			$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			$_data["last_time_update"]=date("Y-m-d H:i:s");

			//if(strlen($_data["sys_program_name"]) > 0 && strlen($_data["sys_program_url"]) > 0 )
			$_result01=$this->Sean_db_tools->db_insert_record($_data,"ij_member");

			$this->session->set_flashdata("success_message","新增完成！");

			redirect($this->page_data['init_control']."sys_member_item/edit/".$_result01);
			//redirect($this->page_data['init_control']."sys_member");
			//exit();

		}


		$_system_status_ct= array(
		"1" => "未啟用",
		"2" => "已啟用",
		);
		$_system_level_type_ct= array(
		"9" => "系統使用者管理員",
		);

    $this->page_data["em_columns"] = array(
		  //'delimiter_1'=>array('inner_html'=>'<span style="font-size:13px;font-weight:bold;color:#336699"><i>'.$_lang["delimiter_1"].'</i></span><br />'),
			'user_name'=>array('header'=>$this->lang->line("user_name"),'type'=>(($_action=="edit") ? 'label' : 'textbox'), 'required'=>'required', "data_validation_email_message"=>"請輸入!",'required_message'=>"請輸入!",'form_control'=>'', 'placeholder'=> (($_action=="edit") ? '' : '請輸入') , 'post_addition'=> (($_action=="edit") ? '' : '（自訂帳號）'),'pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'user_name', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'autocomplete="off"', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'email'=>array('header'=>"Email",'type'=> 'textbox', 'required'=>'required', "data_validation_email_message"=>"請輸入email!",'required_message'=>"請輸入email!",'form_control'=>'', 'placeholder'=> (($_action=="edit") ? '' : '請輸入email') , 'post_addition'=> (($_action=="edit") ? '' : 'email@example.com  '),'pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'last_name'=>array('header'=>'姓名', 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>"請輸入姓名", 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			//'first_name'=>array('header'=>$this->lang->line("first_name"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>"請輸入".$this->lang->line("first_name"), 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'tel'=>array('header'=>$this->lang->line("tel"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>"請輸入".$this->lang->line("tel"), 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'cell'=>array('header'=>$this->lang->line("cell"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>"請輸入".$this->lang->line("cell"), 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			//'wedding_date'=>array('header'=>$this->lang->line("wedding_date"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>"請輸入".$this->lang->line("wedding_date"), 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'3', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			//'birthday'=>array('header'=>$this->lang->line("birthday"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>"請輸入".$this->lang->line("birthday"), 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'3', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			//'addr1'=>array('header'=>$this->lang->line("address"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'','required_message'=>"", 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'3', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'member_level_type'=>array('header'=>'帳號角色', 'type'=>'hidden','source'=>$_system_level_type_ct,'required'=>'','required_message'=>"", 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'member_status'=>array('header'=>"帳號狀態", 'type'=>'enum', 'source'=>$_system_status_ct,'required_message'=>"",'required'=>'required','form_control'=>'','maxlength'=>'', 'default'=>'2',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	    'memo'=>array('header'=>$this->lang->line("memo"), 'type'=>'textarea',"data_validation_email_message"=>"",'required'=>'','required_message'=>"請輸入".$this->lang->line("apply_memo"), 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'password'=>array('header'=>$this->lang->line("password"), 'type'=>'password',"data_validation_number_message"=>'', 'required'=>'','required_message'=>"",'form_control'=>'','maxlength'=>'', 'default'=>'1',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'autocomplete="new-password"', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			//'password2'=>array('header'=>$this->lang->line("password2"), 'type'=>'textbox',"data_validation_number_message"=>"", 'required'=>'','required_message'=>"",'form_control'=>'','maxlength'=>'3', 'default'=>'1',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );


		$this->load->view('sean_admin/general/main', $this->page_data);
	}

/* ------------------------------------------------------ End 總控系統管理---------------------------------------------------*/






	/**
	 * 總控登入頁面 Template
	 *
	 */

	public function managerSignOut()
	{
		$this->session->set_userdata('member_login',false);
		$this->session->set_userdata('member_data',false);
		$this->session->set_userdata('member_role_system_rules',false);
		redirect(base_url('admin/login'));
    	exit();
	}

	public function role_select()
	{
	  $this->LoginCheck();
		$this->page_data['member_roles'] = $this->libraries_model->get_member_roles($this->session->userdata['member_data']['member_sn'],1);
		if(@$this->page_data['member_roles']['empty_role'] || !$this->page_data['member_roles']){
        	$this->load->library('ijw');
			$this->ijw->_wait(base_url("/admin/main/managerSignOut") , 2 , '很抱歉！您的帳號沒有設定權限角色，請洽詢系統管理員',1);
			exit();
		}
		//var_dump($this->page_data['member_roles']);exit;

		$member_data=$this->session->userdata('member_data');
		$member_data['role_count']=count($this->page_data['member_roles']);
		$this->session->set_userdata('member_data',$member_data);
		if(count($this->page_data['member_roles'])==1){ //只有一個角色直接跳過選取
			$this->member_roles($this->page_data['member_roles'][0]['role_sn']);
		}
		$this->load->view('admin/page_manager/role_select', $this->page_data);
	}
	public function member_roles($_id){
		$member_roles = $this->libraries_model->_select('ij_member_role_relation',array('status'=>'1','ij_member_role_relation.member_sn'=>$this->session->userdata['member_data']['member_sn'],'role_sn'=>$_id),0,0,0,0,0,'num_rows');
      	$this->load->library('ijw');
		if(!$member_roles){
			$this->ijw->_wait(base_url("/admin/main/role_select") , 2 , '很抱歉！您的帳號沒有該角色的權限喔！',1);
			exit();
		}
		$member_role_system_rules = $this->libraries_model->get_role_programs($_id,1);
		$member_role_system_rules[0]['role_sn']=$_id;
		$member_role_system_rules[0]['role_name'] = $this->libraries_model->_select('ij_role',array('role_sn'=>$_id),0,0,0,0,0,'row')['role_name'];
		//若角色名稱符合權限名稱則取代權限level
		/*if($member_level_type=$this->libraries_model->_select('ij_member_level_type_ct',array('member_level_type_name'=>$member_role_system_rules[0]['role_name']),0,0,0,0,0,'row')['member_level_type']){
			$member_data=$this->session->userdata('member_data');
			$member_data['member_level_type']=$member_level_type;
			$this->session->set_userdata('member_data',$member_data);
		}*/

		if($member_role_system_rules[0]['role_name']=='供應商管理員' && !@$this->session->userdata['member_data']['supplier_name']){
			$this->ijw->_wait(base_url("/admin/main/role_select") , 2 , '很抱歉！您的帳號尚未設定供應商，請重新選擇角色！',1);
			exit();
		}
		if($member_role_system_rules[0]['role_name']=='經銷商管理員' && !@$this->session->userdata['member_data']['dealer_name']){
			$this->ijw->_wait(base_url("/admin/main/role_select") , 2 , '很抱歉！您的帳號尚未設定經銷商，請重新選擇角色！',1);
			exit();
		}
		//var_dump($this->session->userdata('member_data'));
		//exit();
		//允許的權限陣列
		$this->session->set_userdata('member_role_system_rules',$member_role_system_rules);
		redirect(base_url('admin/main/'));
	}
	/*public function LoginCheck()
	{

			if($this->session->userdata('member_login')== true)
			return true;
			else
			redirect($this->page_data['init_control']."managerSignIn");

			exit();
	}*/



	public function managerSignAction()
	{

		$this->load->model('membership/Login_model');

        $_message='message';

				set_cookie('login_type',$this->input->post('login_type',true),time()+24*60*60*30*12); //一年

        $_message_cap="";
       if ( @$_POST['rycode'] != $this->session->userdata('captcha'))
			{

				if (@$_POST['rycode'] != $this->session->userdata('captcha') && strlen(@$_POST['rycode']) > 0) {
                $_message_cap.="<font color=red size=\"3px\">驗證碼不符</font>";
            }
			$this->session->set_flashdata($_message,$_message_cap);
            $data['post'] = $_POST;
            $this->session->set_flashdata($data['post']);

			//紀錄登入失敗資料
			//$this->Login_model->_member_log(0,$_POST['username'],2);

            redirect('admin/login');
            exit();
		}else{

			//會員登入檢查

			$_login_array=$this->Login_model->validate_login($this->input->post('username',true),$this->input->post('password',true),$this->input->post('login_type',true));
			if(is_array($_login_array)==false)
			{
				//紀錄登入失敗資料
				//$this->Login_model->_member_log(0,$_POST['username'],2);

				//帳號密碼錯誤
				$this->session->set_flashdata($_message,  "<font size=\"4px\">".$_login_array."</font>");
	            $data['post'] = $_POST;
				$this->session->set_flashdata($data['post']);
            	redirect(base_url('admin/login'));
           		exit();

			}else{
           		//exit();
				//$this->_update_min_price(); //更新商品最低價登入更新一次
				$this->session->set_userdata('member_login',true);
				$this->session->set_userdata('member_data',$_login_array);
				//if(@$_login_array['supplier_name']){
				//	redirect(base_url('admin/main/'));
				//}else{
					redirect(base_url('admin/main/role_select/'));
				//}
				exit();


			}
		}
	}

	/**
	 * 會員列表 Template
	 *
	 */

	 public function memberTable()
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Member_table";
		/* END 修改 */


		$this->load->model('member/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
    $this->page_data['_caption_subject']= $_ct_name."列表";

    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    // 列表內容
    $this->page_data['_body_result']= $_result["_body_result"];


		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'sean_admin/page_member/page_table';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_member/page_table_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}




	/**
	 * 會員編輯 Template
	 *
	 */
	public function memberTable_item($_action,$_id = 0)
	{


		/* Begin 修改 */
		//載入 model
		$_model_name="Member_table";
		/* END 修改 */


		$this->load->model('member/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");


		// 內容設定
		$this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."/adding";


		 //編輯
    if($_action=="edit")
		{
			$_result="";
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_edit($_where);
			$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
			$this->page_data['_result01']=$_result;

		}

		//更新
		if($_action=="update")
		{

			$_where=" where member_sn='".$_id."'";
			$this->$_model_name->action_update();
			$this->session->set_flashdata("success_message","更新完成！");
			redirect("member/memberTable/");
			exit();
		}
		$this->input->get_post('some_data', TRUE);
		//新增
		if($_action == "adding")
		{

			$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");


		}



    $this->page_data["em_columns"] = array(
		  //'delimiter_1'=>array('inner_html'=>'<span style="font-size:13px;font-weight:bold;color:#336699"><i>'.$_lang["delimiter_1"].'</i></span><br />'),
			'user_name'=>array('header'=>$this->lang->line("user_name"),'type'=>(($_action=="edit") ? 'label' : 'textbox'), 'required'=>'required', "data_validation_email_message"=>"請輸入email!",'required_message'=>"請輸入email!",'form_control'=>'', 'placeholder'=> (($_action=="edit") ? '' : '請輸入email') , 'post_addition'=> (($_action=="edit") ? '' : 'email@example.com (登入時使用) '),'pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'last_name'=>array('header'=>$this->lang->line("last_name"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>"請輸入".$this->lang->line("last_name"), 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'first_name'=>array('header'=>$this->lang->line("first_name"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>"請輸入".$this->lang->line("first_name"), 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'tel'=>array('header'=>$this->lang->line("tel"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'','required_message'=>"請輸入".$this->lang->line("tel"), 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'cell'=>array('header'=>$this->lang->line("cell"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>"請輸入".$this->lang->line("cell"), 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'wedding_date'=>array('header'=>$this->lang->line("wedding_date"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>"請輸入".$this->lang->line("wedding_date"), 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'birthday'=>array('header'=>$this->lang->line("birthday"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'required','required_message'=>"請輸入".$this->lang->line("birthday"), 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'addr1'=>array('header'=>$this->lang->line("address"), 'type'=>'address',"data_validation_email_message"=>"",'required'=>'','required_message'=>"", 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			//'owner_dealer'=>array('header'=>$this->lang->line("owner_dealer"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'','required_message'=>"", 'form_control'=>'select2_dealer', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'member_status'=>array('header'=>$this->lang->line("member_status"), 'type'=>'status', 'required_message'=>$this->lang->line("member_status"),'required'=>'required','form_control'=>'','maxlength'=>'3', 'default'=>'1',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	  'memo'=>array('header'=>$this->lang->line("memo"), 'type'=>'textbox',"data_validation_email_message"=>"",'required'=>'','required_message'=>"請輸入".$this->lang->line("apply_memo"), 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),


			'password'=>array('header'=>$this->lang->line("password"), 'type'=>'textbox',"data_validation_number_message"=>"", 'required'=>'','required_message'=>"",'form_control'=>'','maxlength'=>'', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'password2'=>array('header'=>$this->lang->line("password2"), 'type'=>'textbox',"data_validation_number_message"=>"", 'required'=>'','required_message'=>"",'form_control'=>'','maxlength'=>'', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );

		$this->page_data['page_content']['view_path'] = 'sean_admin/page_member/page_item.php'; // 頁面主內容 view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_member/page_item_js'; // 頁面主內容的 JS view 位置, 必要

		$this->load->view('sean_admin/general/main', $this->page_data);
	}




	/**
	 * 產業屬性列表 Template
	 *
	 */

	 public function domain($_action="",$_id="")
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Biz_domain";
		/* END 修改 */

		$_ct_name="產業屬性";// 功能名稱
		$_url="domain/";//列表功能url
		$_url_item="domain_item/";//處理動作 url
		$_table="ij_biz_domain";//table name
		$_table_key="domain_sn";//table key

		$this->page_data['_id']=$_id;// 功能名稱
		$this->page_data['_father_name']="系統設定";// 功能名稱
		$this->page_data['_ct_name']="婚宴地點管理";// 功能名稱
		$this->page_data['_ct_name_item']="地點編輯";// 功能子名稱
		$this->page_data['_url']="domain/";//列表功能url
		$this->page_data['_table']="ij_biz_domain";//table name
		$this->page_data['_table_key']="domain_sn";//table key
		$this->page_data['_member_data']=$this->session->userdata('member_data');
		$this->page_data['_table_field']=array(
					"domain_name",
					"status",
				);


    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";
    $this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."add";
		$this->page_data['_edit_link']=$this->page_data['init_control'].$this->page_data['_url']."edit/";
		$this->page_data['_delete_link']=$this->page_data['init_control'].$this->page_data['_url']."delete/";
		$this->page_data['_search_link']=$this->page_data['init_control'].$this->page_data['_url']."search/";

    	$_page_content="sean_admin/page_system/domain/table";
		$_page_level_js="sean_admin/page_system/domain/table_js";
		$_page_content_item="sean_admin/page_system/domain/item";
		$_page_level_js_item="sean_admin/page_system/domain/item_js";

		$this->page_data['_father_link']=$this->page_data['init_control'].$this->page_data['_url'];
		$this->load->model('backend_system/'.$_model_name);



		switch($_action){
			case "add" :
			case "item" :
			case "edit" :
			case "adding" :
			case "update" :

				// 內容設定
				 $this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."adding";


				 //編輯
		    if($_action=="edit")
				{
					$this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."update/".$_id."";

					$_result="";
					$_where=" where ".$this->page_data['_table_key']."='".$_id."'";
					$_result=$this->Sean_db_tools->db_get_max_record("*",$this->page_data['_table'],$_where);
					$this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."update/$_id";
					$this->page_data['_result01']=$_result;

				}

				//更新
				if($_action=="update")
				{


					$_result=$this->$_model_name->action_update($this->page_data);
					if($_result)
						$this->session->set_flashdata("success_message","更新完成！");
					else
						$this->session->set_flashdata("fail_message","更新失敗！");
					redirect($this->page_data['_father_link']);
					exit();
				}



				//新增
				if($_action == "adding")
				{
					$_result=$this->$_model_name->action_insert($this->page_data);
					if($_result)
						$this->session->set_flashdata("success_message","新增完成！");
					else
						$this->session->set_flashdata("fail_message","新增失敗！");



				}

				// 頁面主內容 view 位置, 必要
				$this->page_data['page_content']['view_path'] = $_page_content_item;
				// 頁面主內容的 JS view 位置, 必要
				$this->page_data['page_level_js']['view_path'] = $_page_level_js_item;
				$this->load->view('sean_admin/general/main', $this->page_data);

			break;

			default:
				//刪除
				if($_action=="delete")
				{

					$_where=" where ".$this->page_data['_table_key']."='".$_id."'";
					$_result=$this->$_model_name->action_delete($_where,$this->page_data);
					if($_result)
						$this->session->set_flashdata("success_message","刪除完成！");
					else
						$this->session->set_flashdata("fail_message","刪除失敗！");
				}
				  $_where=" ";
					$_result01=$this->Sean_db_tools->db_get_max_record("*",$_table,$_where);
					$this->page_data['_result01']=$_result01;
				// 返回連結
		    $this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."add";
		    $this->page_data['_edit_link']=$this->page_data['init_control'].$this->page_data['_url']."edit/";
		    $this->page_data['_delete_link']=$this->page_data['init_control'].$this->page_data['_url']."delete/";
		    $this->page_data['_search_link']=$this->page_data['init_control'].$this->page_data['_url']."search/";



				// 頁面主內容 view 位置, 必要
				$this->page_data['page_content']['view_path'] = $_page_content;
				// 頁面主內容的 JS view 位置, 必要
				$this->page_data['page_level_js']['view_path'] = $_page_level_js;
				$this->load->view('sean_admin/general/main', $this->page_data);
			break;
		}


	}

	/**
	 * 產業屬性編輯 Template
	 *
	 */
	public function domainItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'sean_admin/page_system/domain/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'sean_admin/page_system/domain/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('sean_admin/general/main', $page_data);
	}



	/**
	 * 婚宴地點管理 Template
	 *
	 */
	public function location($_action="",$_id="")
	{
		/* Begin 修改 */
		//載入 model
		$_model_name="Location";
		$this->page_data['_id']=$_id;// 功能名稱
		$this->page_data['_father_name']="系統設定";// 功能名稱
		$this->page_data['_ct_name']="婚宴地點管理";// 功能名稱
		$this->page_data['_ct_name_item']="地點編輯";// 功能子名稱
		$this->page_data['_url']="location/";//列表功能url
		$this->page_data['_table']="ij_wedding_location";//table name
		$this->page_data['_table_key']="wedding_location_sn";//table key
		$this->page_data['_member_data']=$this->session->userdata('member_data');
		$this->page_data['_table_field']=array(
					"wedding_location_name",
					"addr1",
					"status",
				);

		$_table_where=" ";//table list where
		$_page_content="admin/page_system/location/table";
		$_page_level_js="admin/page_system/location/table_js";
		$_page_content_item="admin/page_system/location/item";
		$_page_level_js_item="admin/page_system/location/item_js";

		/* END 修改 */
		$this->page_data['_father_link']=$this->page_data['init_control'].$this->page_data['_url'];
		$this->load->model('backend_system/'.$_model_name);


		switch($_action){
			case "add" :
			case "item" :
			case "edit" :
			case "adding" :
			case "update" :

				// 內容設定
				 $this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."adding";


				 //編輯
		    if($_action=="edit")
				{
					$this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."update/".$_id."";

					$_result="";
					$_where=" where ".$this->page_data['_table_key']."='".$_id."'";
					$_result=$this->Sean_db_tools->db_get_max_record("*",$this->page_data['_table'],$_where);
					$this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."update/$_id";
					$this->page_data['_result01']=$_result;

				}

				//更新
				if($_action=="update")
				{


					$_result=$this->$_model_name->action_update($this->page_data);
					if($_result)
						$this->session->set_flashdata("success_message","更新完成！");
					else
						$this->session->set_flashdata("fail_message","更新失敗！");
					redirect($this->page_data['_father_link']);
					exit();
				}



				//新增
				if($_action == "adding")
				{
					$_result=$this->$_model_name->action_insert($this->page_data);
					if($_result)
						$this->session->set_flashdata("success_message","新增完成！");
					else
						$this->session->set_flashdata("fail_message","新增失敗！");



				}

				// 頁面主內容 view 位置, 必要
				$this->page_data['page_content']['view_path'] = $_page_content_item;
				// 頁面主內容的 JS view 位置, 必要
				$this->page_data['page_level_js']['view_path'] = $_page_level_js_item;
				$this->load->view('sean_admin/general/main', $this->page_data);

			break;

			default:
				//刪除
				if($_action=="delete")
				{

					$_where=" where ".$this->page_data['_table_key']."='".$_id."'";
					$_result=$this->$_model_name->action_delete($_where,$this->page_data);
					if($_result)
						$this->session->set_flashdata("success_message","刪除完成！");
					else
						$this->session->set_flashdata("fail_message","刪除失敗！");
				}
				if($_action=="search")
				{


							$_table_where="";
							if($this->input->get_post("status") || strlen($this->input->get_post("status")) > 0)
							{
								if(strlen($_table_where) < 2 )
								$_table_where .=" where";
								else
								$_table_where .= " and ";

								$_data["status"]=$this->input->get_post("status");
								$_table_where .="  status = '".$_data["status"]."'";
								$this->session->set_userdata('search_location_status',$_data["status"]);
							}

							if($this->input->get_post("wedding_location_name") || strlen($this->input->get_post("wedding_location_name")) > 0)
							{
								if(strlen($_table_where) < 2 )
								$_table_where .=" where";
								else
								$_table_where .= " and ";

								$_data["wedding_location_name"]=$this->input->get_post("wedding_location_name");
								$_table_where .="  wedding_location_name  like '%".$_data["wedding_location_name"]."%'";
								$this->session->set_userdata('search_location_wedding_location_name',$_data["wedding_location_name"]);
							}

							$search_term = ''; // default when no term in session or POST


				}else{
					$this->session->set_flashdata("message",$this->session->userdata('search_location_status'));
					if ( $this->session->userdata('search_location_status') or  strlen($this->session->userdata('search_location_status')) > 0  )
					{

								if(strlen($_table_where) < 2 )
									$_table_where .=" where";
								else
									$_table_where .= " and ";

								$search_term = $this->session->userdata('search_location_status');
								$_table_where .="  status = '".$search_term."'";
					}
					if ($this->session->userdata('search_location_wedding_location_name') )
					{
								if(strlen($_table_where) < 2 )
									$_table_where .=" where";
								else
									$_table_where .= " and ";

								$search_term = $this->session->userdata('search_location_wedding_location_name');
								$_table_where .="  wedding_location_name  like '%".$search_term."%'";
					}
				}
				// 返回連結
		    $this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."add";
		    $this->page_data['_edit_link']=$this->page_data['init_control'].$this->page_data['_url']."edit/";
		    $this->page_data['_delete_link']=$this->page_data['init_control'].$this->page_data['_url']."delete/";
		    $this->page_data['_search_link']=$this->page_data['init_control'].$this->page_data['_url']."search/";

		    $config = array();
		    $config['total_rows'] = $this->Sean_db_tools->db_get_max_num($this->page_data['_table_key'],$this->page_data['_table'],$_table_where);

		    if(is_numeric($_action))
		    {
		    	$_start_num=  $_action + 1;
		    	if($_start_num <  1 )
		    	$_start_num=1;

		    	$_end_num=$_start_num + $this->page_data["_per_page"] - 1;

		    	if($_end_num  > $config['total_rows'])
		    	$_end_num=$config['total_rows'];
		    	$_table_where .= " limit $_start_num , ".$this->page_data["_per_page"];
		    }else{
		    	$_start_num=1;
		    	$_end_num=$this->page_data["_per_page"];

		    	if($_end_num  > $config['total_rows'])
		    	$_end_num=$config['total_rows'];

		    	$_table_where .=" limit  ".$this->page_data["_per_page"];
		    }
				$_result01=$this->Sean_db_tools->db_get_max_record("*",$this->page_data['_table'],$_table_where);
				$this->page_data['_result01']=$_result01;

				$this->load->library("pagination");

				$config['base_url'] = $this->page_data['_father_link'];
				$config['full_tag_open'] = '<div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing '.$_start_num.' to '.$_end_num.' of '.$config['total_rows'].' entries</div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                 <ul class="pagination" style="visibility: visible;"> ';
				$config['full_tag_close'] = '</ul></div>
              </div>
            </div>';

        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class="previous">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><span>';
        $config['cur_tag_close'] = '</span></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

				$config['per_page'] = $this->page_data["_per_page"];
				$this->pagination->initialize($config);
				$this->page_data["_pagination"]=$this->pagination->create_links();


				// 頁面主內容 view 位置, 必要
				$this->page_data['page_content']['view_path'] = $_page_content;
				// 頁面主內容的 JS view 位置, 必要
				$this->page_data['page_level_js']['view_path'] = $_page_level_js;
				$this->load->view('sean_admin/general/main', $this->page_data);
			break;
		}
	}

	/**
	 * 更新商品最低價
	 *
	 */
	private function _update_min_price()
	{
		$this->load->model('libraries_model');
		$products = $this->libraries_model->_select('ij_product','product_status','1',0,0,0,0,'result_array','product_sn,pricing_method,product_type');
		foreach($products as $p){
			$data = array();
			$price=$this->libraries_model->_get_min_price($p['product_sn'],$p['pricing_method']);
			if($price > 0){
				$data['min_price']=$price;
			}else{
				if($p['product_type']!='2'){ //虛擬允許價錢=0 所以不關閉
					$data['product_status']='0'; // 價錢=0 關閉該商品
				}
			}
			//var_dump($data).'<br>';
			$data = $this->libraries_model->CheckUpdate($data,0);
			$this->libraries_model->_update('ij_product',$data,'product_sn',$p['product_sn']);
		}
	}
	/**
	 * 系統權限
	 *
	 */
	public function sysProgram($sys_program_config_sn=0)
	{
		$this->LoginCheck();
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		if($sys_program_config_sn){
			//$this->session->set_userdata(array('upper_sys_program_config_sn'=>$sys_program_config_sn));//記住上層分類
			$page_data['page_content']['current_category']=$this->libraries_model->_select('ij_sys_program_config','sys_program_config_sn',$sys_program_config_sn,0,0,0,0,'row');//當前分類
			if($page_data['page_content']['current_category']['upper_sys_program_config_sn']){
				//當前分類的上層
				$page_data['page_content']['upper_current_category1']=$this->libraries_model->_select('ij_sys_program_config','sys_program_config_sn',$page_data['page_content']['current_category']['upper_sys_program_config_sn'],0,0,0,0,'row');
				if($page_data['page_content']['upper_current_category1']['upper_sys_program_config_sn']){
					//當前分類的上上層
					$page_data['page_content']['upper_current_category2']=$this->libraries_model->_select('ij_sys_program_config','sys_program_config_sn',$page_data['page_content']['upper_current_category1']['upper_sys_program_config_sn'],0,0,0,0,'row');
				}
			}
			//$where = array('upper_sys_program_config_sn' => $sys_program_config_sn,'category_status' => '1');
		  $CategoryItems=$this->libraries_model->programTree($sys_program_config_sn,'','','');
		}else{
			//$where = array('category_status' => '1');
		  $CategoryItems=$this->libraries_model->programTree(0,'','','','');
		}
		//$CategoryItems=$this->libraries_model->_select('ij_sys_program_config',$where,0,0,0,'upper_sys_program_config_sn',0,'result_array');
		//var_dump($CategoryItems);

		$Store=$this->libraries_model->get_channels();
		$page_data['page_content']['Store']=form_dropdown('module[channel_name]',$Store,@$Item['channel_name'],'id="StoreName" class="form-control" ');

		$page_data['page_content']['CategoryItems'] = $CategoryItems;
		$page_data['page_content']['view_path'] = 'admin/page_sys/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_sys/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 分類編輯 Template
	 *
	 */
	public function sysProgramItem($sys_program_config_sn=0,$upper_sys_program_config_sn=0)
	{
		$this->LoginCheck();
		$page_data = array();
		//var_dump( $this->libraries_model->bindecValues(63));
		//寫入資料庫
		if($data_array=$this->input->post('category')){
			if(!$sys_program_config_sn=$this->input->post('sys_program_config_sn')){ //判斷新增或修改
				$category_attribute=$this->input->post('category_attribute');
				$data_array['system_rule_set']=array_sum($category_attribute);
				$data_array['sort_order'] = '1';
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				$sys_program_config_sn=$this->libraries_model->_insert('ij_sys_program_config',$data_array);
				//新增角色程式關係
			  $roles=$this->libraries_model->_select('ij_role','status','1',0,0,0,0,'result_array');
				if(is_array($roles)){
					foreach($roles as $_key=>$_value){
						$data_array2['sort_order']='8'; //預設
						$data_array2['status']='1'; //預設
						$data_array2['sys_program_config_sn']=$sys_program_config_sn;
						$data_array2['role_sn']=$_value['role_sn'];
						$data_array2 = $this->libraries_model->CheckUpdate($data_array2,1);
						$data_array2 = $this->libraries_model->CheckUpdate($data_array2,0);
						$this->libraries_model->_insert('ij_role_sys_program_relation',$data_array2);
					}
				}
				$this->session->set_userdata(array('Mes'=>'新增成功!'));
				$this->session->set_flashdata("success_message","新增成功!");
			}else{
				$system_rule =$this->input->post('system_rule');
				if(!$system_rule && $this->libraries_model->_select('ij_role_sys_program_relation','sys_program_config_sn',$sys_program_config_sn,0,0,0,0,'num_rows')==0){
				//新增主程式角色程式關係，因為新增主程式時不會新增到角色
				$role_where=array('status'=>'1','role_sn >'=>'1');
			  $roles=$this->libraries_model->_select('ij_role',$role_where,0,0,0,0,0,'result_array');
					if(is_array($roles)){
						foreach($roles as $_key=>$_value){
							$data_array2['sort_order']='8'; //預設
							$data_array2['status']='1'; //預設
							$data_array2['sys_program_config_sn']=$sys_program_config_sn;
							$data_array2['role_sn']=$_value['role_sn'];
							$data_array2 = $this->libraries_model->CheckUpdate($data_array2,1);
							$data_array2 = $this->libraries_model->CheckUpdate($data_array2,0);
							$this->libraries_model->_insert('ij_role_sys_program_relation',$data_array2);
						}
					}
				}
				//預設一律先拿掉全部權限
				$data_array3['sort_order']=0;
				$data_array3 = $this->libraries_model->CheckUpdate($data_array3,0);
				$this->libraries_model->_update('ij_role_sys_program_relation',$data_array3,'sys_program_config_sn',$sys_program_config_sn);
				//有設定再異動權限
				if(is_array($system_rule)){
					foreach($system_rule as $_key=>$_value){
						$data_array2['sort_order']=array_sum($_value);
						$data_array2 = $this->libraries_model->CheckUpdate($data_array2,0);
						$this->libraries_model->_update('ij_role_sys_program_relation',$data_array2,'role_sys_program_relation_sn',$_key);
					}
				}
				$category_attribute=$this->input->post('category_attribute');
				$data_array['system_rule_set']=array_sum($category_attribute);
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				$this->libraries_model->_update('ij_sys_program_config',$data_array,'sys_program_config_sn',$sys_program_config_sn);
				$this->session->set_userdata(array('Mes'=>'修改成功!'));
				$this->session->set_flashdata("success_message","修改成功!");
			}
			redirect(base_url('admin/main/sysProgramItem/'.$sys_program_config_sn));
		}

		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		if($upper_sys_program_config_sn){ //預設上層分類
			$upper_CategoryItem=$this->libraries_model->_select('ij_sys_program_config','sys_program_config_sn',$upper_sys_program_config_sn,0,0,0,0,'row');
			$page_data['page_content']['upper_CategoryItem']=$upper_CategoryItem;
		}
		if($sys_program_config_sn){
			$CategoryItem=$this->libraries_model->_select('ij_sys_program_config','sys_program_config_sn',$sys_program_config_sn,0,0,0,0,'row');
			$page_data['page_content']['CategoryItem'] = $CategoryItem;
			$upper_sys_program_config_sn=$CategoryItem['upper_sys_program_config_sn'];
		  $page_data['page_content']['category_attributes'] = $this->libraries_model->get_system_rule($CategoryItem['system_rule_set']);
		  $page_data['page_content']['addons'] = $this->libraries_model->get_system_roles($sys_program_config_sn,$this->libraries_model->get_status1_Array($page_data['page_content']['category_attributes']));
		}else{ //新增預設
			$page_data['page_content']['CategoryItem']['status']='1'; //預設發佈
		  $page_data['page_content']['category_attributes'] = $this->libraries_model->get_system_rule(15,0);
		  $page_data['page_content']['addons'] = $this->libraries_model->get_system_roles(0,$this->libraries_model->get_status1_Array($page_data['page_content']['category_attributes']));
		}
//var_dump($select_Array);
		$categorys=$this->libraries_model->_select('ij_sys_program_config',array('class1_program_flag'=>1,'status'=>1),0,0,0,0,0,'list','sys_program_config_sn,sys_program_name');
		$page_data['page_content']['categorys']=form_dropdown('category[upper_sys_program_config_sn]',$categorys,@$upper_sys_program_config_sn,'id="Categorys" class="form-control" required');
		$page_data['page_content']['upper_sys_program_config_sn']=@$upper_sys_program_config_sn;
		$page_data['page_content']['view_path'] = 'admin/page_sys/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_sys/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}
	public function AddMainProgram(){   //AJAX 新增主程式
		$main_name = $this->input->post('main_name');
		$main_engname = $this->input->post('main_engname');
		if($this->libraries_model->_select('ij_sys_program_config','sys_program_name',$main_name,1,0,0,0,'num_rows') >=1){
				//echo $this->db->last_query();
				$data_array3['error'] = '這個主程式名稱已經有建檔嚕！';
				echo json_encode($data_array3);
				exit();
		}
		//exit();
		$sys_program_config_sn = $this->input->post('sys_program_config_sn');
		$data_array['sys_program_name'] = $main_name;
		$data_array['sys_program_eng_name'] = $main_engname;
		$data_array['class1_program_flag'] = '1';
		$data_array['upper_sys_program_config_sn'] = '0';
		$data_array['status'] = '1';
		if($main_name){
			if($sys_program_config_sn){ //修改先不用
					$data_array = $this->libraries_model->CheckUpdate($data_array,0);
					//var_dump($data_array);
					$this->libraries_model->_update('ij_sys_program_config',$data_array,'addon_log_sn',$addon_log_sn);
			}else{
					$data_array = $this->libraries_model->CheckUpdate($data_array,0);
					$data_array = $this->libraries_model->CheckUpdate($data_array,1);
					$sys_program_config_sn=$this->libraries_model->_insert('ij_sys_program_config',$data_array);
			}
			$data_array3= "<option selected value='".$sys_program_config_sn."'>".$main_name."</option>";
			echo json_encode($data_array3);
		}else{
				$data_array3['error'] = '您沒有輸入主程式名稱喔！';
				echo json_encode($data_array3);
				exit();
		}
	}
	public function Update_Programs(){ //抓取主選單更新系統程式未完成
		$this->load->helper('simple_html_dom');
		//$html = file_get_html('http://testijw.systemnet.tw/admin/main');
		$html = str_get_html($this->load->view('admin/general/header','',true));
		//echo $this->load->view('admin/general/header','',true);
		$res=$html->find('a');
		foreach($res as $e) {
			  $e->innertext;
		    if(strpos($e->innertext, '婚禮網站') !== false) {
		    	if($e->href!='javascript:;'){
		        echo $e->href;
		    	}
		    }
		}
		/*$res=$html->find('ul[id=list]');
		foreach($html->find('li') as $li)
		{
        foreach($li->find('a') as $key_li=>$c) {
					if($c->href!='admin/main' && $c->href!='javascript:;'){
						$result[$key_li]['href']=$c->href;
						$result[$key_li]['title']=$c->plaintext;
					}
				}
       foreach($li->find('ul') as $ul)
       {
        foreach($ul->find('li a') as $key_ul=>$c) {
					if($c->href!='admin/main' && $c->href!='javascript:;'){
						$result[$key_li][$key_ul]['href']=$c->href;
						$result[$key_li][$key_ul]['title']=$c->plaintext;
					}
				}
       }
		}*/
		/*foreach($res as $k=>$a){
		  $b = $a->find("li a");
			foreach($b as $_key=>$c){
				//echo $c->href.$c->plaintext;
				if($c->href!='admin/main' && $c->href!='javascript:;'){
					$result[$k][$_key]['href']=$c->href;
					$result[$k][$_key]['title']=$c->plaintext;
				}
			}
		}*/
		//var_dump($result);
	}
	public function get_chart_data($_kind){
		$_result=array('test'=>'123','tt'=>'456');
		$_result=$this->libraries_model->_select('ij_order',0,0,0,7,'order_create_date',0,'list','order_create_date,total_order_amount');
		var_dump($_result);
	}
}
