<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容
*/
?>
<div class="container">

<!-- Page level plugin styles START -->
<!-- Page level plugin styles END -->
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <li class="active">全站通知</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar col-md-2 col-sm-3">
    <div class="desktop_view_sidebar">
		<ul class="list-group margin-bottom-25 sidebar-menu">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
	<div class="mobile_view_sidebar">
		<div class="side-nav-head border_title_bottom">
			<button class="fa fa-bars collapsed" data-toggle="collapse" data-target="#mob_sidebar_drop_down"></button>
			<h4 class="bfont">會員中心<!--會員中心--></h4>
		</div>
		<ul class="list-group margin-bottom-25 sidebar-menu collapse" id="mob_sidebar_drop_down">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="col-md-10 col-sm-9">
    <div class="product-page" style="padding-top: 0;">
      <?php $this->load->view('www/general/flash_error');?>
      <div class="row inbox">
        <div class="inbox-content">
          <table class="table table-striped table-advance table-hover">
            <thead>
              <tr>
                 <div class="btn-group dltmessg">
        				   <input type="checkbox" id="select_all" class="fdcheck pull-left mail-checkbox mail-group-checkbox"/>
        				   <label class="pull-left" for="select_all"></label>
                   <a class="btn btn-sm btn-primary pull-left" href="javascript:;" data-toggle="dropdown" aria-expanded="false" /> 將選擇之通知 <i class="fa fa-angle-down"></i></a>
                    <ul class="dropdown-menu">
                      <li>
                        <a href="javascript:;" onclick="$('#flg').val('read');$('#form1').submit();">
                        <span class="glyphicon glyphicon-envelope col-md-1 pull-left"></span> 標示為已讀 </a>
                      </li>
                      <li class="divider"></li>
                      <li>
                        <a href="javascript:;" onclick="$('#flg').val('del');$('#form1').submit();">
                        <span class="glyphicon glyphicon-trash col-md-1 pull-left"></span> 刪除 </a>
                      </li>
                    </ul>
                  </div>

                <th colspan="2" style="text-align:center;">主題</th>
                <th>內容</th>
                <th>類別</th>
                <th style="width: 150px;text-align:center;">顯示時間</th>
              </tr>
            </thead>
            <form method="post" id="form1">
            <tbody>
              <?php if($messages){ foreach($messages as $key=>$_Item){?>
              <tr class="unread" data-messageid="<?php echo $key+1;?>">
                <td class="inbox-small-cells">
                  <input id="chk1<?php echo $key+1;?>" type="checkbox" class="fdcheck mail-checkbox" name="member_message_center_sn[]" value="<?php echo $_Item['member_message_center_sn'];?>">
				  <label class="sphov pull-left" for="chk1<?php echo $key+1;?>"></label>
                </td>
                <td class="view-message" <?php echo (!$_Item['read_flag'])? 'style="font-weight: 700;font-size: 120%;"':'';?> >
                	<?php if($_Item['message_link_url']){?>
	                  <!--<a href="<?php echo $_Item['message_code'];?>" target="_blank"-->
                  		<a target="_blank" href="member/notification/detail/<?php echo $_Item['member_message_center_sn'];?>">
	                    <?php echo $_Item['message_subject'];?>&nbsp;
	                    <i class="fa fa-external-link"></i>
	                  </a>
	                  <?php }else{?>
                  		<a href="member/notification/detail/<?php echo $_Item['member_message_center_sn'];?>"><?php echo $_Item['message_subject'];?></a>
	                  <?php }?>
                </td>
                <td><?php echo $_Item['message_content'];?></td>
                <td class="view-message hidden-xs">
                   <?php switch($_Item['message_code']){
                      case 1:
                        echo '客服:'.$_Item['update_member_sn'];
                        break;
                      default:
                        echo '系統訊息';
                      }?>
                </td>
                <td class="view-message text-right">
                   <?php echo $_Item['transmit_date'];?>
                </td>
              </tr>
              <?php }}?>
            </tbody>
            <tfooter><?php echo $page_links;?></tfooter>
            <input type='hidden' name='flg' id='flg' value=''>
          </form>
          </table>
        </div>
        <!-- /.inbox-content -->
      </div>
    </div>
  </div>
  <!-- END CONTENT -->
</div>
</div>
<!-- END SIDEBAR & CONTENT -->
<style>
a.btn.btn-sm.blue.dropdown-toggle {
    background: #ab8b64;
}
td.view-message #alread {
    border-bottom: 0px;
}
.product-page h1 {

    padding-bottom: 8px;
}
.dltmessg {
   margin-top: 10px;
}
</style>