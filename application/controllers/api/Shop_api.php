<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop_api extends CI_Controller {
	/*
	* 	get_upg_products : [商城][API] 可升級商品與費用列表
	*	member_data	: 
	*/
  public function __construct(){
      parent::__construct();
      $this->CI =& get_instance();
      $this->load->database();
			$this->load->helper('url');
  }

	public function index()
	{
		$this->load->view('welcome_message');
	}
  //[商城][API] 可升級商品與費用列表
	public function get_upg_products()
	{
				$data_log = array();
        //$data_log['ip'] = $this->input->ip_address();
        //$data_log['time_used'] = date('Y-m-d H:i:s');
				$feedback = "";
				$apps_check_val=0;$apps_member_val=0;$apps_member_val=0;
				
				if(!$order_sn = (int)$this->input->post('order_sn'))
				{
					$order_sn = (int)$this->input->get('order_sn');
				}
				if(!$product_sn = (int)$this->input->post('product_sn'))
				{
					$product_sn = (int)$this->input->get('product_sn');
				}				
				if(!$order_sn || !$product_sn)
				{
					$feedback['response_code'] = 'FAIL:no order_sn or product_sn input';
				}else{
					$this->load->model('Shop_model');
					$feedback=$this->Shop_model->get_upg_products($order_sn,$product_sn);
					//var_dump($feedback);
	        if(!$feedback)
	        {
						$feedback['response_code'] = 'FAIL:no order data found';
					}else{
						//$data_log["api_token_sn"]='1';
						$data_log["server_reply_action"]="";
					 	$data_log["client_action"]="[get_upg_products]::order_sn-$order_sn::product_sn-$product_sn";
					 	$data_log["server_reply_action"].=json_encode($feedback);
					 	$data_log["create_date"]=date("Y-m-d H:i:s");
					 	$data_log["create_member_sn"]="99999";
					 	$data_log["last_time_update"]=date("Y-m-d H:i:s");
					 	$data_log["update_member_sn"]="99999";
					 	$this->api_token_log($data_log);
					}
				}
		  $this->CI->output->set_content_type('application/json')->set_output(json_encode($feedback));				
	}
	
  //[商城][API] 可加購商品與費用列表
	public function get_addon_products()
	{
				$data_log = array();
        //$data_log['ip'] = $this->input->ip_address();
        //$data_log['time_used'] = date('Y-m-d H:i:s');
				$feedback = "";
				$apps_check_val=0;$apps_member_val=0;$apps_member_val=0;
				
				if(!$order_sn = (int)$this->input->post('order_sn'))
				{
					$order_sn = (int)$this->input->get('order_sn');
				}
				if(!$product_sn = (int)$this->input->post('product_sn'))
				{
					$product_sn = (int)$this->input->get('product_sn');
				}				
				if(!$order_sn || !$product_sn)
				{
					$feedback['response_code'] = 'FAIL:no order_sn or product_sn input';
				}else{
					$this->load->model('Shop_model');
					$feedback=$this->Shop_model->get_addon_products($order_sn,$product_sn);
					//$feedback['response_code'] = 'OK';
					//var_dump($feedback);
					//exit();
	        if(!$feedback)
	        {
						$feedback['response_code'] = 'FAIL:no order data found';
					}else{
						//$data_log["api_token_sn"]='1';
						$data_log["server_reply_action"]="";
					 	$data_log["client_action"]="[get_addon_products]::order_sn-$order_sn::product_sn-$product_sn";
					 	//foreach($feedback as $key => $value)
					 	//{
					 		$data_log["server_reply_action"].=json_encode($feedback);
					 	//}
					 	$data_log["create_date"]=date("Y-m-d H:i:s");
					 	$data_log["create_member_sn"]="99999";
					 	$data_log["last_time_update"]=date("Y-m-d H:i:s");
					 	$data_log["update_member_sn"]="99999";
					 	$this->api_token_log($data_log);
					}
				}
		  $this->CI->output->set_content_type('application/json')->set_output(json_encode($feedback));				
	}

   //[商城][API] 升級 / 加購付款項目加入購物車
   public function add_cart(){
				$data_log = array();
				$feedback = "";
				$apps_check_val=0;$apps_member_val=0;$apps_member_val=0;
				
				if(!$qty = intval($this->input->post('qty',TRUE))){
					if(!$qty = intval($this->input->get('qty',TRUE))){
						$qty=1;
					}
				}
				if(!$product_sn = intval($this->input->post('product_sn',TRUE))){
					$product_sn = intval($this->input->get('product_sn',TRUE));
				}
				if(!$order_sn = intval($this->input->post('order_sn',TRUE))){
					$order_sn = intval($this->input->get('order_sn',TRUE));
				}
				$this->load->model('Shop_model');
				if($order_sn){
       		$actual_sales_amount=$this->Shop_model->get_upg_products($order_sn,$product_sn,1);
       	}else{
       		$order_sn=0;
					$actual_sales_amount=0;
				}
				if(!$product_spec = intval($this->input->post('product_spec',TRUE))){
					if(!$product_spec = intval($this->input->get('product_spec',TRUE))){
						$product_spec='單一規格';
					}
				}

			$this->load->library('cart');
      if($product = $this->Shop_model->_select('ij_product','product_sn',$product_sn,1,0,0,0,'row','product_name,min_order_qt,default_channel_name')){
					//if(strtolower(filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest') {
            //$result = $this->Shop_model->_get_product($this->input->post("product_sn",TRUE));
            if($product['min_order_qt'] > $qty) $qty=$product['min_order_qt']; //最小訂購量
            /*$addon = $this->input->post("addon",TRUE);
						$addon_count=0;
            if($addon){ //計算加購商品數
            	foreach($addon as $addon_item){
            		if(@$addon_item['addon_log_sn']){
            			$addon_count++;
            		}
            	}
            }*/
            //var_dump($addon);
            //商品售價
            $price=$this->Shop_model->_get_price($qty,$product_sn,$product_spec);
            if(@$order_sn && @$actual_sales_amount){
       				//var_dump($actual_sales_amount);
            	 $price = $price-$actual_sales_amount;
            	 if($price<0) $price=0; //避免負數
            }
            $data = array(
                'id'      => $product_sn.'_'.$product_spec,
                'qty'     => $qty,
                'price'   => $price,
                'name'    => $product['product_name'],
                'gift_cash'    => 0,
                'addon_log_sn'  => '',  //加購用
			          'addon_from_rowid'  => '', //加購用
                'addon_count'  => 0, //加購筆數
                'status'  => '1', 
                'coupon_code'  => '',  // 優惠券
                'coupon_money'  => 0,  // 優惠金額
                'mutispec_stock_sn'  => $product_spec,
                'original_order_sn'  => $order_sn,
                'upgrade_discount_amount'  => $actual_sales_amount,  //升級價差金額
                'channel_name'  => $product['default_channel_name'] 
            );
            if(!$rowid=$this->_chk_cart($product_sn,$this->cart->contents(),$product_spec)){
	            $rowid=$this->cart->insert($data);
	          }else{
							$data['rowid'] = $rowid;
							$this->cart->update($data);
	          	//改為override
							//$error['error']='已有相同產品，如須更改數量請至購物車修改';
	          }
	          //寫入暫存購物車-有登入的話
            if(@$this->session->userdata['web_member_data']){
							$this->Shop_model->save_temp_order(@$this->session->userdata['web_member_data']['member_sn'],$data);
          	}
            	//var_dump($addon);
            /*
            if(count($addon)>=1){ //有加購
            	$this->_del_cart_addon($rowid); //先刪除所有已加購商品再新增
            	foreach($addon as $Item){
            		if(@$Item['addon_log_sn']){ //有加購
			            $data = array(
			                'id'      => $Item['associated_product_sn'].'_'.$rowid,
			                'qty'     => $Item['addon_amount'],
			                'price'   => $Item['addon_price'],
			                'name'    => $Item['product_name'],
                      'gift_cash'    => 0,
                			'addon_count'  => $qty, //主商品購買數
			                'addon_log_sn'  => $Item['addon_log_sn'],
			                'addon_limitation'  => $Item['addon_limitation'],
			                'addon_from_rowid'  => $rowid,
                			'status'  => '1', 
			                'coupon_code'  => '',  // 優惠券
			                'coupon_money'  => 0,  // 優惠金額
			                'mutispec_stock_sn'  => $this->input->post("product_spec",true), //主商品規格
                			'channel_name'  => $product['default_channel_name'] //跟隨主商品館別
			            );
            			if(!$add_rowid=$this->_chk_cart($Item['associated_product_sn'],$this->cart->contents(),0,$rowid)){
			            	//var_dump($add_rowid);
				            $result_addon=$this->cart->insert($data);
				          }else{
					          	//$row=$this->cart->get_item($add_rowid);
											$data['rowid'] = $add_rowid;
			            //var_dump($data);
											$this->cart->update($data);
					          	//改為override
											//$error['error']='已有相同加購產品，如須更改數量請至購物車修改';
				          }
          				//寫入暫存購物車-有登入的話
            			if(@$this->session->userdata['web_member_data']){
										$this->Shop_model->save_temp_order($this->session->userdata['web_member_data']['member_sn'],$data);
									}
	            	}
            	}
            }*/
            //echo $this->Shop_model->_get_price($qty,$product_sn);
       			if(!@$error){
            	//echo 'ok';
	            $feedback['cart'] = array(
	                'total_items'      => count($this->cart->contents()),
	                'total'     => number_format($this->cart->total())
	            );
	            $feedback['response_code'] = 'OK';
							//echo json_encode($cart_array);
            }else{
            	//$this->cart->destroy();
            	//$feedback=$error;
	            $feedback['response_code'] = $error;
							//echo json_encode($error);
           }
      	//}
	    }else{
					//$feedback=array('error'=>'no product');
					$feedback['response_code'] = 'FAIL:no product';
	    }
		//$this->load->helper('url');
		redirect(base_url("shop/shopView"));
		//$this->CI->output->set_content_type('application/json')->set_output(json_encode($feedback));				
    }
    
	private function _chk_cart($product_sn,$cart,$mutispec_stock_sn=0,$rowid=0){   //比對購物車是否已有該產品
		if($product_sn && $cart){
			foreach($cart as $key=>$Item){
				if($mutispec_stock_sn){
					if($Item['id']==$product_sn.'_'.$mutispec_stock_sn){
						return $key;
					}
				}elseif($rowid){
					if($Item['id']==$product_sn.'_'.$rowid){
						return $key;
					}
				}else{
					if($Item['id']==$product_sn.'_'){
						return $key;
					}
				}
			}
			return false;
		}else{
			return false;
		}
	}
	
  function token_renew()
  {
  	$feedback = "";
				$apps_check_val=0;$apps_member_val=0;$apps_member_val=0;
				
				if(!$login_token = $this->input->post('login_token'))
				{
					$login_token = $this->input->get('login_token');
				}
				
				if(!$login_token )
				{
						$feedback['response_code'] = 'FAIL';
				}else{
					
						$query = $this->db->select('*')
		                        ->from('ij_api_token')
		                        ->where('api_token_status', 1)
		                        ->where('api_token ', $login_token)
											      ->get();
											        if($query->num_rows() > 0)
											        {
															        	$apps_check = $query->row_array();
														        		
														        		foreach($apps_check as $key => $value)
																		    {
																		        			if($key=="api_token_sn")
																				        		$apps_check_val = $value;
																				        	if($key=="api_token_end_time")
																				        		$api_token_end_time = $value;
																		    }
														        		if($api_token_end_time < date("Y-m-d H:i:s"))
														        		{
														        			$feedback['response_code'] = 'TIME_EXPIRE';
														        	  }else{
														        	  	
														        	  		$api_token_end_time=date("Y-m-d H:i:s", strtotime('+1 hour'));
														        	  		$data = array(
																						               'api_token_end_time' => $api_token_end_time,
																						            );
																						$this->db->where('api_token_sn', $apps_check_val);
																						$this->db->update('ij_api_token', $data); 
														        	  	  $feedback['response_code'] = 'OK';
														        	  }
														   }else{
														   	
														   				$feedback['response_code'] = 'FAIL';
														  }
					
				}
				
			if($feedback['response_code']=="OK")
		 {
			$data_log["api_token_sn"]=$apps_check_val;
			$data_log["server_reply_action"]="";
		 	$data_log["client_action"]="[renew_token]::$login_token";
		 	foreach($feedback as $key => $value)
		 	{
		 		$data_log["server_reply_action"].=$value."::";
		 	}
		 	$data_log["create_date"]=date("Y-m-d H:i:s");
		 	$data_log["create_member_sn"]="99999";
		 	$data_log["last_time_update"]=date("Y-m-d H:i:s");
		 	$data_log["update_member_sn"]="99999";
		 	
		 	$this->api_token_log($data_log);
		}
		  $this->CI->output
                ->set_content_type('application/json')
                ->set_output(json_encode($feedback));
  	
  }
  
  function api_token_log($data_log)
  {
		 return $this->db->insert('ij_api_token_log', $data_log);
  }
  
  function sql_api_token($data_log)
  {
		 return $this->db->insert('ij_api_token', $data_log);
  }
  
  function getToken($username)
  {
    $_token1 = "";
    
    $_token1=$username."-".time()."-";
    $_token1.=mt_rand(10000, 99999);
    $token=base64_encode($_token1);
    return $token;
  }
}
