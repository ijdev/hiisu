<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 供應商聯絡窗口
*/
?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">編輯聯絡窗口</h4>
      </div>
      <div class="modal-body">
                  <form action="admin/supplier/supplierTable/contact_update/<?php echo $_supplier_sn;?>"  class="form-horizontal" > 
                    <div class="form-body">
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">姓名 </label>
                        <div class="col-lg-8">
                          <input type="text" name="contact_name" class="form-control"  value="<?php if(isset($_contact_info_item->contact_name)) echo $_contact_info_item->contact_name;?>">
                        </div>
                      </div>
                     <div class="form-group">
                        <label class="col-lg-3 control-label">類別 </label>
                        <div class="col-lg-8">
                          <div class="radio-list">
                             <?php
							                foreach($_contact_type_list as $keys=>$values) 
							                {
							    							$_checked="";
								    									if($_contact_info_item->contact_type==$values->contact_type)
								    									$_checked=" checked";
								    									if($_checked=="" && $values->default_flag=='1')
								    									$_checked=" checked";
							    							echo  '  <label class="radio-inline"><input type="radio" required name="contact_type" value="'.$values->contact_type.'" '.$_checked.'>'.$values->contact_type_name.'</label>';
															}
                						?>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">公司電話 </label>
                        <div class="col-lg-8">
                            <div class="col-lg-6" style="padding-left: 0px;">
                              <input type="text" class="form-control" name="office_tel" value="<?php if(isset($_contact_info_item->office_tel)) echo $_contact_info_item->office_tel;?>">
                            </div>
                            <div class="col-lg-6">
                              <label for="name" class="col-lg-5 control-label">分機</label>
                              <div class="col-lg-7">
                                <input type="text" class="form-control" name="office_tel_ext"value="<?php if(isset($_contact_info_item->office_tel_ext)) echo $_contact_info_item->office_tel_ext;?>">
                              </div>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">手機 </label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="cell" value="<?php if(isset($_contact_info_item->cell)) echo $_contact_info_item->cell;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">電子郵件 </label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="email" value="<?php if(isset($_contact_info_item->email)) echo $_contact_info_item->email;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="active" class="col-lg-3 control-label">狀態 </label>
                        <div class="col-lg-8">
                          <select name="status" class="form-control">
                            <option value=1 <?php if(isset($_contact_info_item->status) && $_contact_info_item->status==1) echo ' selected="selected"';?>>有效</option>
                            <option value=0 <?php if(isset($_contact_info_item->status) && $_contact_info_item->status==0) echo ' selected="selected"';?>>無效</option>
                          </select>
                        </div>
                      </div>
                    </div>
      <div class="modal-footer">
        <div class="form-group">
          <div class="col-lg-6">
		        <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
        	</div>
          <div class="col-lg-6">
		        <button type="submit" class="btn blue">確認修改</button>
        	</div>
        </div>
        <div class="form-group">
          <label for="updater" class="col-md-3 control-label">更新者</label>
          <div class="col-md-8">
            <input type="text" class="form-control" placeholder="<?php echo (@$_contact_info_item->update_last_name)?@$_contact_info_item->update_last_name:''; ?>" Disabled>
          </div>
        </div>
        <div class="form-group">
          <label for="time-update" class="col-md-3 control-label">更新時間</label>
          <div class="col-md-8">
            <input type="text" class="form-control" placeholder="<?php echo ($_contact_info_item->last_time_update!='0000-00-00 00:00:00')?$_contact_info_item->last_time_update:$_contact_info_item->create_date;?>" Disabled>
          </div>
        </div>
        <div class="form-group">
          <label for="time-update" class="col-md-3 control-label">建立時間</label>
          <div class="col-md-8">
            <input type="text" class="form-control" placeholder="<?php echo ($_contact_info_item->create_date!='0000-00-00 00:00:00')?$_contact_info_item->create_date:'';?>" Disabled>
          </div>
        </div>
      </div>
      <input type="hidden" name="contact_sn" value="<?php if(isset($_contact_info_item->contact_sn)) echo $_contact_info_item->contact_sn;?>">
        </form>
    </div>