<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 官網合作夥伴
*/
?>

<!-- Page level plugin styles START -->
<!-- Page level plugin styles END -->
<style>
  body.ecommerce{
    background-color: #FFF;
  }

  #section-partners{
    min-height: 600px;
  }
  #section-partners{
    text-transform: initial;
  }
  .feature-box-03 .product-item{
    box-shadow: 1px 2px 5px 1px #B5B5B5;
    border-radius: 5px;
    background-color: #F2F2F2;
    padding: 20px 0px 24px 24px;
    position: relative;
  }
  .feature-box-03 .product-item img{
    width:100%;
  }
    .feature-box-03 .product-item h4{
      text-align: right;
      position: absolute;
      right:10px;
      background-color: #cebea2;
      color: #FFF;
      line-height: 2;
      font-size: 20px;
      display: block;
      width: 92%;
      padding-right: 32px;
      box-shadow: 1px 1px 1px 1px #CCC;
    }
    .feature-box-03 .product-item p{
      color: #605b58;
      font-size: 14px;
      line-height: 1.5;
      margin: 50px 10px 10px 24px;
      min-height: 72px;
    }
</style>
<!-- BEGIN SERVICE BOX -->
<div id="section-partners" style="">
  <div class="container">
    <h3><span>合作夥伴</span> Partners</h3>
    <div class="row padding-top-25 margin-bottom-40 feature-box-03">
      <div class="col-md-6 col-sm-12">
        <div class="product-item">
          <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-4">
              <img src="public/img/logo-partner-03.png" >
            </div>
            <div class="col-md-9 col-sm-8 col-xs-8">
              <h4>愛婚享 ilikeus</h4>
              <p>
                婚紗APP、婚禮日報、人行立牌。
                歡迎來電&私訊預約商品諮詢服務
                >>>02-2501-4388 #187
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-sm-12">
        <div class="product-item">
          <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-4">
              <img src="public/img/logo-partner-02.jpg" >
            </div>
            <div class="col-md-9 col-sm-8 col-xs-8">
              <h4>可艾婚禮 Kute</h4>
              <p>
                可艾提供最豐富的婚禮小物、喜帖及結婚週邊用品的選擇。
                以創新的設計、透明的價格和優良的品質協助新人完成個人化的婚禮想法。
                是您婚禮採購的最佳選擇。
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="clear;"></div>
    </div>
    <!-- END SERVICE BOX --> 
  </div>
</div>