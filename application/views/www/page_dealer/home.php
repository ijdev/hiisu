<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Page 主架構
*/
?>
<div class="main-container">
  <div class="flexslider full-screen normal-nav">
      <ul class="slides">
<?php if($slider){ foreach ($slider as $_item) {
	echo '<li style="background-image:url(/public/uploads/'.$_item.');"></li>';
	}} ?>
      </ul>
      <div class="nav">
          <a href="#" class="prev icon4-leftarrow23"></a>
          <a href="#" class="next icon4-chevrons"></a>
          <a href="#" class="pause icon4-pause49"></a>
          <a href="#" class="play icon4-play87"></a>
          <a href="#" class="full icon4-fullscreen6"></a>
      </div>
  </div>
</div>
