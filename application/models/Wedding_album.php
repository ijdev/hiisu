<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wedding_album extends CI_Model {

    public $limit = 10; // query limit setting
    public $until = 0; // last query sn

    public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
    }
    /*
    * 婚紗相冊相片列表
    * 全數拉出管理
    * 限制的部分 toto
    */
    public function get_result( $wedding_website_sn = 0 )
    {
        $this->db->select('*');
        $this->db->from('ij_wedding_album');
        $this->db->where('wedding_website_sn', $wedding_website_sn);
        $this->db->where('status', 1);
        $this->db->order_by('sort_order', 'desc');
        $this->db->order_by('wedding_album_sn', 'desc');
        $query = $this->db->get();
        return $result = $query->result_array();
    }

    /*
    * Update tag
    */
    public function update_row( $wedding_album_sn = 0, $member_sn=0, $data_update = array() )
    {
        $wedding_album_sn = (int)$wedding_album_sn;
        //echo  $this->db->update_string('ij_wedding_album', $data_update, 'wedding_album_sn = '.$wedding_album_sn.' AND create_member_sn = '. $member_sn);
        return  $this->db->update('ij_wedding_album', $data_update, 'wedding_album_sn = '.$wedding_album_sn.' AND create_member_sn = '. $member_sn);
    }
    /*
    * insert photo into album
    */
    public function insert_row( $data_insert = array())
    {
        return $this->db->insert('ij_wedding_album', $data_insert);
    }
}