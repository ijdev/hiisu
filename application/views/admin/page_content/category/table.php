<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>內容分類 <small>分類列表</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">內容分類列表</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/content/contentCategoryItem" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>編號</th>
                  <th>名稱</th>
                  <th>頻道</th>
                  <th>部落格 / FAQ</th>
                  <th>排序</th>
                  <th>是否啟用</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
								<?php if($Items){ foreach($Items as $_Item){?>
                  <tr class="odd gradeX">
                    <td><?php echo $_Item['content_type_sn']?></td>
                    <td><?php echo $_Item['content_title']?></td>
                    <td><?php echo $_Item['post_channel_name_set']?></td>
                    <td>
                      <?php if($_Item['blog_faq_flag']==1):?>
                        部落格
                      <?php else:?>
                        FAQ
                      <?php endif;?>
                    </td>
                    <td><?php echo $_Item['sort_order']?></td>
                    <td class="center">
                      <?php if($_Item['post_flag']==1):?>
                        <i class="fa fa-check"></i>
                      <?php else:?>
                        <i class="fa fa-times"></i>
                      <?php endif;?>
                    </td>
                    <td class="center">
                      <a href="admin/content/contentCategoryItem/<?php echo $_Item['content_type_sn']?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
    <?if(stripos($this->page_data['permission'],'del')!==false){?>
                      <a href="javascript:void(0);" class="deleteItem" ItemId="<?php echo $_Item['content_type_sn'];?>"><i class="fa fa-trash-o"></i>刪除</a>
                      <?}?>
                    </td>
                  </tr>
                <?php }}?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->