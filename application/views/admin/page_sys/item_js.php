<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script>
jQuery(document).ready(function() {       
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
<?php if(@$channel_name){?>
	ChgStoreName('<?php echo @$channel_name;?>','<?php echo @$upper_category_sn;?>');
<?php }?>
});
		$('#Add_addon').click(function(event) { //新增加購送出
			if(!$('#main_name').val()) { alert('請輸入加購價');$('#main_name').focus();return false;}
  		$.post('admin/main/AddMainProgram', {
          main_name: $('#main_name').val(),
          main_engname: $('#main_engname').val(),
      }, function(data) {
      	if(data){
      		$('#basic').modal("hide");
      		if(data.error){
      			alert(data.error);
      		}else{
	      		$('#Categorys').append(data);
					}
				}
      }, 'json');			
		});
</script>

