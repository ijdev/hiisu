<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容的 page level js
*/
?>

<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
<script src="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->
<script src="public/metronic/global/plugins/zoom/jquery.zoom.min.js" type="text/javascript"></script><!-- product zoom -->
<script src="public/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script><!-- Quantity -->
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
  




<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/frontend/layout/scripts/layout.js" type="text/javascript"></script>

<script>
  
  // When the browser is ready...
  $(function() {


 				
  jQuery.validator.addMethod("mobileTaiwan", function( value, element ) {
	var str = value;
	var result = false;
	if(str.length > 0){
		//是否只有數字;
		var patt_mobile = /^[\d]{1,}$/;
		result = patt_mobile.test(str);
 
		if(result){
			//檢查前兩個字是否為 09
			//檢查前四個字是否為 8869
			var firstTwo = str.substr(0,2);
			var firstFour = str.substr(0,4);
			var afterFour = str.substr(4,str.length-1);
			if(firstFour == '8869'){
				$(element).val('09'+afterFour);
				if(afterFour.length == 8){
					result = true;
				} else {
					result = false;
				}
			} else if(firstTwo == '09'){
				if(str.length == 10){
					result = true;
				} else {
					result = false;
				}
			} else {
				result = false;
			}
		}
	} else {
		result = true;
	}
	return result;
}, "手機號碼不符合格式，僅允許09開頭的10碼數字");

    // Setup form validation on the #register-form element
    $("#register-form").validate({
    
        // Specify the validation rules
        rules: {
            affiliate_website: "required",
            affiliate_url: "required",
           
        },
        
        // Specify the validation error messages
        messages: {
            affiliate_website: "請輸入網站名稱！",
            affiliate_url: "請輸入網站位置！"
        },
        
        submitHandler: function(form) {
            form.submit();
        }
        
	
    });

  });
  
  </script>


<script type="text/javascript">
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init();
        Layout.initImageZoom();
        Layout.initTouchspin();
        Layout.initFixHeaderWithPreHeader();
        Layout.initNavScrolling();
				
    });
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->
