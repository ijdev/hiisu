<?php
class MY_Exceptions extends CI_Exceptions {

    public function __construct() {
        parent::__construct();
    }

    function show_404($page = '', $log_error = TRUE) {
        //redirect(base_url('home/p404'),'auto',404);
        $CI = &get_instance();
        $CI->output->set_status_header('404');
        $page_data = array();

        // 內容設定

        // page level setting
        $page_data['slider'] = NULL; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，非必要


        // general view setting
        $page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
        $page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
        $page_data['page_content']['about']=$CI->Shop_model->_select('ij_system_file_config','system_file_name','404',0,0,0,0,'row')['system_file_ontent'];
        //排序前10有圖片分類
        $CI->load->library("ijw");
        $where = array('category_status' => '1','main_picture_save_dir <>' => '','publish_flag'=>'1');
        $promo_category=$CI->Shop_model->_select('ij_category',$where,0,0,0,0,0,'result_array');
        foreach ($promo_category as $_key => $_value) {
            $promo_category[$_key]['category_attributes']=$CI->Shop_model->category_attributes($_value['category_sn'],0,1);
            //var_dump($promo_category[$_key]['category_attributes']);
        }
        $page_data['page_content']['promo_category']=$promo_category;
        $page_data['page_content']['view_path'] = 'www/page_home/page_404'; // 頁面主內容 view 位置, 必要
        $page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
        $page_data['page_level_js']['view_path'] = 'www/page_home/page_404_js'; // 頁面主內容的 JS view 位置, 必要
        echo $CI->load->view('www/general/main', $page_data,true);
    }

}
?>