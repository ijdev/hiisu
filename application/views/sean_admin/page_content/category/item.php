<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/

foreach($_result01 as $key => $value)
	{
		$_data_info=$value;
	}
	
	$_post_top_category_sn = explode("、", $_data_info->post_top_category_sn);		

	$st1="";
	$st2="";
	$st3="";
	$st4="";

	foreach($_post_top_category_sn as $key => $value)
		{			
			
			if($value=="首頁")
					{
						$st1="checked";
					}

			if($value=="雲端婚訊")
					{
						$st2="checked";
					}
					
			if($value=="結婚商店")
					{
						$st3="checked";												
					}			

			if($value=="現場報到系統")
					{
						$st4="checked";
					}								
					
		}
		
	
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>內容分類 <small>分類設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">分類設定</span>
            </div>
            <div class="tools">
              <a href="admin-ijwedding/Content/contentCategory"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">            
            <form class="form-horizontal" method="post" role="form" action="admin-ijwedding/Content/contentCategoryItem/update/<?php echo $_data_info->content_typel_sn;?>">
              <div class="form-body">
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">部落格 / FAQ</label>
                  <div class="col-lg-8">
                    <select name ="blog_faq_flag" class="form-control">
                      <option value="0" <?php if($_data_info->blog_faq_flag=="0"){print'selected';}?> >部落格</option>
                      <option value="1" <?php if($_data_info->blog_faq_flag=="1"){print'selected';}?> >FAQ</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">頻道</label>
                  <div class="col-lg-8">
                    <div class="checkbox-list">
                      <label class="checkbox-inline">
                      <input type="checkbox" id="inlineCheckbox1" name="post_top_category_sn[]" value="首頁"  <?php print $st1 ?> > 首頁 </label>
                      <label class="checkbox-inline">
                      <input type="checkbox" id="inlineCheckbox4" name="post_top_category_sn[]" value="雲端婚訊" <?php print $st2 ?> > 雲端婚訊 </label>
                      <label class="checkbox-inline">
                      <input type="checkbox" id="inlineCheckbox2" name="post_top_category_sn[]" value="結婚商店" <?php print $st3 ?> > 結婚商店 </label>
                      <label class="checkbox-inline">
                      <input type="checkbox" id="inlineCheckbox3" name="post_top_category_sn[]" value="現場報到系統" <?php print $st4 ?> > 現場報到系統 </label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">名稱 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input type="text" name="content_title" value="<?php print $_data_info->content_title; ?>" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-3 control-label">說明</label>
                  <div class="col-lg-8">
                    <textarea name="content_description" class="form-control" rows="3"><?php print $_data_info->content_description; ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="seq" class="col-lg-3 control-label">排序 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input type="text" name="sort_order" class="form-control" value="<?php echo $_data_info->sort_order;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">是否啟用 <span class="require">*</span></label>
                  <div class="col-lg-8">
                  	<select name="status" class="form-control">
                      <option value="1" <?php if($_data_info->status=="1"){print'selected';}?>>是</option>
                      <option value="0" <?php if($_data_info->status=="0"){print'selected';}?>>否</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-lg-3 control-label">更新者</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="<?php echo $this->session->userdata['member_data']['user_name'];?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-3 control-label">更新時間</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="<?php echo date("Y-m-d H:i:s");?>" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                    <a href="admin-ijwedding/Content/contentCategory"><button type="button" class="btn default">Cancel</button></a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->