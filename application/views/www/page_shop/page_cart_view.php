<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 線上商店商品內頁
*/
$blue='';
$purple='';
$yellow='';

//var_dump($cart);exit;
?>
<style>
  .pi-img-wrapper img{border:1px solid #EFEFEF;}
  .sale-product{background-color: #EFEFEF;padding: 12px;}
  .goods-page-image img{
    -webkit-box-shadow: 0 1px 8px rgba(0, 0, 0, 0.5);
    box-shadow: 0 1px 8px rgba(0, 0, 0, 0.5);
  }
  .del-goods-col{text-align: right;line-height: 2;}
  .del-goods{margin:0px;}
  .box_gift_cash{float: right; font-size: 18px; padding-right:24px;}
  .fa-exclamation-circle{font-size: 18px;background-color: #FFF;color: #FF0000;}
  .fa-usd-box{border-radius: 50%;border:2px solid #dfba49;display: inline-block;border-radius: 50% !important;    width: 18px;height: 18px;text-align: center; font-size: 12px;}
  .fa-usd-box .text-warning{font-size: 10px;}

  .shopping-total-price{border-top: 1px solid #cebea2 !important;font-weight: bold !important;padding-top: 24px !important;}
  .shopping-total{ width: 92%;}

  .table-shipping{
      border:0px; width:96% !important;margin:0 auto;
  }
  .table-shipping td{border:0px;padding: 6px 0px 6px 0;}
  #showshopping input.use_credit {
      display: none;
  }
  label.sphov_credit {
    width: 100%;
 }
 #shop .portlet.box{
    margin-top: 0px;
 }
 #shop div:nth-of-type(2){
    margin-bottom: 0px;
 }
</style>
<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<link href="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css"><!-- for slider-range -->
<link href="public/metronic/global/plugins/rateit/src/rateit.css" rel="stylesheet" type="text/css">
<!-- Page level plugin styles END -->

			<ul class="breadcrumb">
				<li><a href="/">首頁</a></li>
				<li><a href="shop">線上商店</a></li>
				<li class="active">購物車</li>
			</ul>
	<!-- shopping steps -->
			<div class="shopping_steps clearfix" style="width:521px;">
				 <div class="step_over blkfont"><div class="step_number">1</div><span>商品及配送</span></div>
				 <div class="step_arrow"><i class="icon-caret-right"></i></div>
				 <!--div class="step_normal"><div class="step_normal_number">2</div><span>折扣運費</span></div>
				 <div class="step_arrow"><i class="icon-caret-right"></i></div-->
				 <div class="step_normal"><div class="step_normal_number">2</div><span>填寫資料</span></div>
				 <div class="step_arrow"><i class="icon-caret-right"></i></div>
				 <div class="step_normal"><div class="step_normal_number">3</div><span>訂單完成</span></div>
			</div>
      <span class="box_gift_cash">
        <a class="various" href="#gift_cash_exclamation">囍市錢包餘額： </a>
				<?php if($gift_cash >=0){ ?>
        <strong>
          <span class="fa-usd-box"><i class="fa fa-usd text-warning"></i></span>
          <span class="fa-usd-text text-warning" id="show_gift_cash"><?php echo number_format($gift_cash);?></span>
        </strong>
        <?php }else{ //var_dump($gift_cash)?>
        <span class="fa-usd-text text-warning">請先登入</span>
        <?php }?>
      </span>
<div id="showshopping">
<form id="shop" method="POST" action="shop/shopView2">
<?php if(@$cart){ foreach($cart as $_key=>$_Item){?>
       <div class="trans_name mob_trans">&nbsp;</div>
        <div class="portlet box"><!-- margin-bottom-40-->
          <div class="portlet-title" style="border: 1px solid;border-color: chocolate;margin-bottom: 4px;">
            <div class="caption" style="width: 76%;font-weight: bold;color:#000000;">
              <!--div class="chk_box_lbl">
                <input type="checkbox" class="checkboxrowid" id="ckbCheckAll<?=$_key?>" value="<?=$_key?>" <?=($_Item['cart_check'])?'checked':'';?>>
                <label class="sphov" for="ckbCheckAll<?=$_key?>">結帳</label>
              </div-->
              <div class="col-md-3" style="display:inline-block;color:#000000;">賣家：<?=$_Item['supplier_name']?></div>
            <?php if($_Item['min_amount_for_noshipping']>0){?>
              <div class="col-md-3" style="display:inline-block;color:#000000;">滿額免運：<?=number_format($_Item['min_amount_for_noshipping'])?> 圓</div>
              <input type="hidden" id="min_amount_for_noshipping_<?=$_key?>" value="<?=$_Item['min_amount_for_noshipping']?>">
              <div class="col-md-3" id="supplier_subtotal_<?=$_key?>" style="display:inline-block;color:#000000;">小計：<?=number_format($_Item['supplier_subtotal']);
              $supplier_noship=$_Item['min_amount_for_noshipping']-$_Item['supplier_subtotal'];?></div>
              <div class="col-md-3" id="supplier_noship_<?=$_key?>" style="display:inline-block;color:#000000;"><?=($supplier_noship<=0)?'已達免運':'還差 '.number_format($supplier_noship).' 元'?></div>
            <?php }else{?>
              <div class="col-md-3" style="display:inline-block;color:#000000;">一律免運</div>
            <?php }?>
            </div>
            <div style="float:right;color:#000000;"><font style="font-size:18px;font-weight: bold;">配送方式：</font>
            <select required name="delivery_method[<?=$_key?>]" supplier_sn="<?=$_key?>" id="delivery_method_<?=$_key?>" supplier_name="<?=$_Item['supplier_name']?>" class="delivery_method nopadding height-30 margin-top-6 margin-bottom-6" oninvalid="this.setCustomValidity('請選取配送方式')" oninput="setCustomValidity('')">
              <option value="">請選擇</option>
              <?php if($_Item['delivery']){ foreach($_Item['delivery'] as $key2=>$Item2){ ?>
              <option delivery_amount="<?=$Item2['delivery_amount']?>" outland_amount="<?=$Item2['outland_amount']?>" description="<?=$Item2['description']?>" value="<?=$Item2['delivery_method']?>" <?=(count($_Item['delivery'])==1 || $Item2['delivery_method']==$_Item['delivery_method'])?'selected':'';?>><?=$Item2['delivery_method_name']?></option>
              <?}}?>
            </select>
            </div>
            
          </div>
          <div class="chk_box_lbl sm-chk">
              <input type="checkbox" data-set=".fdcheck<?=$_key?>" class="checkall" id="ckbCheckAll-<?=$_key?>" value="<?=$_key?>" <?=($_Item['cart_check'])?'checked':'';?>>
              <label class="sphov" for="ckbCheckAll-<?=$_key?>">全選</label>
          </div>
    <table class="table-hover responsive mob_tab">
			<thead>
				 <tr>
					<th class="span_hidden">
						<div class="chk_box_lbl">
							<input type="checkbox" data-set=".fdcheck<?=$_key?>" class="checkall" id="ckbCheckAll<?=$_key?>" value="<?=$_key?>" <?=($_Item['cart_check'])?'checked':'';?>>
							<label class="sphov" for="ckbCheckAll<?=$_key?>">全選</label>
						</div>
					</th>
					<th>圖片</th>
					<th> 商品名稱 </th>
					<th>規格</th>
					<th>單價 NT$</th>
					<th>數量</th>
					<th>小計</th>
					<th>使用購物金</th>
					<th style="min-width:50px">刪除</th>
					<th style="min-width:50px"><span class="span_hidden">移入</span>追蹤<span class="span_hidden"></span>
				   </th>
				   </tr>
			</thead>
			<tbody class="mob_tbody">
				<?php if(@$_Item['cart']){ foreach($_Item['cart'] as $key=>$Item){
					if(strpos($Item['id'],'_')!==false) $Item['id']=substr($Item['id'],0,strpos($Item['id'],'_')); // 過濾_	?>
                <tr class="addon_from_rowid<?php echo @$Item['addon_from_rowid']?> tr_<?php echo @$key;?> sm-flex">
   					<td class="span_hidden mob_td1">
						<?php if(!$Item['addon_log_sn']){?>
						<input type="checkbox" id="rowid_<?php echo $key?>" supplier_sn="<?=$_key?>" class="fdcheck fdcheck<?=$_key?>" name="rowid[]" value="<?php echo $key?>" <?php echo ($Item['status']=='3')?'checked':''?> >
							<label class="sphov" for="rowid_<?php echo $key?>"></label>
						<?php }?>
					</td>
				    <td class="mob_td2" data-label="圖片">
						<a href="/shop/shopItem/<?php echo $Item['id'];?>">
						<?php if(!$Item['addon_log_sn']){?><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['product']['image_path'];?>" alt="<?php echo $Item['product']['image_alt'];?>" width="70" >
							<?php }else{?>
								<span>加購</span>
							<?php }?>
						</a>
				    </td>
				    <td class="mob_td3 receivePay_text" style="text-align:left;" data-label="商品名稱"><a href="/shop/shopItem/<?php echo $Item['id'];?>"><?php echo $Item['name'];?></a>&nbsp;<? if($Item['product']['free_delivery']=='0'){?><i title="非滿額免運商品" class="fa fa-exclamation-circle" aria-hidden="true"></i><?}?>
<br><a class="various" supplier_sn="<?php echo $_key?>" rowid="<?php echo $key?>" href="#coupon_input"><button type="button" class="btn btn-circle btn-xs red-sunglo"><?php echo ($Item['coupon_money']>0)?'已優惠：'.$Item['coupon_money']:'優惠券？';?></button></a>
              <?php echo ($Item['coupon_money']>0)? '<div style="margin-top:4px;" class="del-coupon_'.$Item['rowid'].'">刪除優惠<a class="del-goods down-del-coupon" supplier_sn="'.$_key.'" rowid="'.$Item['rowid'].'" href="javascript:void(0);">&nbsp;</a></div>':''?>
					</td>
					<td class="mob_td8 receivePay_text" data-label="規格">
						<select class="product_spec" rowid="<?php echo $key?>" supplier_sn="<?=$_key?>" min_order_qt="<?php echo $Item['product']['min_order_qt'];?>" incremental="<?php echo $Item['product']['incremental'];?>" name="Item[<?php echo $key?>]product_spec" style="width:100px;">
								  <?php if($Item['product']['unispec_flag']=='1'){
                    if($Item['product']['open_preorder_flag']=='0'){
									    $unispec_qty_in_stock=$Item['product']['unispec_qty_in_stock'];
                    }else{
                      $unispec_qty_in_stock=$Item['product']['max_order_qt'];
                    }
										//var_dump($unispec_qty_in_stock);exit();
									?>
								<option selected>單一規格</option>
									<?php }else{ $unispec_qty_in_stock=-1;?>
                <option value="">請選擇規格</option>
									<?php if($Item['product']['mutispec']){ foreach($Item['product']['mutispec'] as $mu){
                    //var_dump($Item['product']['max_order_qt']);
									//$unispec_qty_in_stock=(count($mutispec)==1)? $Item['qty_in_stock']:'0';
									//exit();
									//$qty_in_stock = @$qty_in_stock+intval($mu['qty_in_stock']);
									if($mu['mutispec_stock_sn']==$Item['mutispec_stock_sn']) $unispec_qty_in_stock=$mu['qty_in_stock'];
                    if($Item['product']['open_preorder_flag']=='1'){
                      $unispec_qty_in_stock=$Item['product']['max_order_qt'];
                    }
									?>
								<option qty_in_stock="<?php echo $unispec_qty_in_stock;?>" <?php echo (count($Item['product']['mutispec'])==1 || $mu['mutispec_stock_sn']==$Item['mutispec_stock_sn'])?'selected':'';?> value="<?php echo $mu['mutispec_stock_sn'];?>"><?php echo $mu['color_name'];?></option>
									<?php }} }?>
						</select>
					</td>
          <td class="goods-page-price mob_td10" id="price_<?php echo $key?>" price="<?php echo $Item['price'];?>" data-label="單價">
            <span>$</span><?php echo $Item['price'];?>
          </td>
					<td class="drpvalue mob_td4" data-label="數量" style="padding: 0;">
						  <select class="qty" supplier_sn="<?=$_key?>" addon_limitation="<?php echo @$Item['addon_limitation'];?>" unispec_qty_in_stock="<?php echo @$unispec_qty_in_stock;?>" name="qty" id="qty_<?php echo $key?>" addon_from_rowid="<?php echo @$Item['addon_from_rowid']?>" rowid="<?php echo $key?>">
							<?php if(@$unispec_qty_in_stock=='0' && $Item['product']['open_preorder_flag']=='0'){?>
								<option value="0" selected >已售完</option>
							<?php }elseif(@$unispec_qty_in_stock=='-1'){ //尚未選擇規格預設數量為最小採購量 ?>
								<option value="<?php echo $Item['product']['min_order_qt'];?>" selected ><?php echo $Item['product']['min_order_qt'];?></option>
							<?php }else{
									if(!$Item['addon_log_sn']){   //主商品
										$main_qty=$Item['qty'];
									for($i=$Item['product']['min_order_qt'];$i<= @$unispec_qty_in_stock;$i+=$Item['product']['incremental']):?>
								    <option value="<?php echo $i;?>" <?php echo ($i==$Item['qty'])?'selected':'';?> ><?php echo $i;?></option>
								<?php endfor;
								}else{ //加購
                  if($Item['addon_limitation']==0){
                    $start=$main_qty;
                    $addon_limitation=(@$unispec_qty_in_stock < @$main_qty)? @$unispec_qty_in_stock:@$main_qty;
                  }else{
                    $start=1;;
                    $addon_limitation=(@$unispec_qty_in_stock < @$Item['addon_limitation'])? $unispec_qty_in_stock:@$Item['addon_limitation'];
                  }
									for($i=$start;$i<= $addon_limitation;$i++):?>
								 <option value="<?php echo $i;?>" <?php echo ($i==$Item['qty'])?'selected':'';?> ><?php echo $i;?></option>
								<?php endfor;
								}
							 }?>
						  </select>
						</td>
            <td class="mob_td5 goods-page-total" data-label="小計"><span>$<?php echo number_format($Item['subtotal']);?></span></td>
            <td class="mob_td9" data-label="使用購物金">
              <input type="checkbox" id="use_credit_<?php echo $key?>" name="use_credit" class="use_credit" addon_from_rowid="<?php echo @$Item['addon_from_rowid']?>" rowid="<?php echo $key?>" subtotal="<?php echo $Item['subtotal'];?>" <?php echo ($Item['gift_cash'] > 0)?'checked':'';?>/>
              <label class="sphov_credit" for="use_credit_<?php echo $key?>"></label>
            </td>
						<td class="mob_td6" data-label="刪除"><a class="down-del-goods" supplier_sn="<?php echo $_key?>" addon_from_rowid="<?php echo @$Item['addon_from_rowid']?>" rowid="<?php echo $key?>" href="javascript:void(0);"><span class="important-txt"><i class="fa fa-times"></i></span></a></td>
						<td class="mob_td7" data-label="移入追蹤">
						<div class="note note-success note-bordered heart<?php echo $Item['id'];?>" style="display:none;"></div>
						<a product_sn="<?php echo $Item['id'];?>" supplier_sn="<?=$_key?>" addon_from_rowid="<?php echo @$Item['addon_from_rowid']?>" rowid="<?php echo $key?>" class="addfavorite" title="移入追蹤" href="javascript:void(0);"><span class="important-txt graycol"><i class="fa fa-heart"></i></span></a></td>
				  </tr>
				  <?php $supplier_name=$Item['supplier_name'];}

					}else{
					echo '<ul>購物車是空的喔，快去採購吧^^</ul>';}?>

          </tbody>
		</table>
</div>
<?}}
//var_dump($this->session->userdata('method_shipping'));?>
        <div class="margin-bottom-25">
          <div class="row">
            <div class="col-md-8">
              <div class="note note-warning">
                <h3>配送地區與運費</h3>
                <p>
                  <div class="form-body">
                    <div class="form-group row ship_area">
                      <label class="col-md-2 control-label">
                        <strong>配送地區：</strong>
                      </label>
                      <div class="col-md-6">
                        <label class="radio inline fbtnsgrn">
                          <input type="radio" required class="receiver_addr_state" name="receiver_addr_state" value="台灣本島" <?=($this->session->userdata('method_shipping')[0]=='台灣本島' || ($this->session->userdata('method_shipping')=='' && $this->session->userdata('web_member_data')['addr_state']=='台灣本島'))?'checked':''?> ><span>台灣本島</span>
                        </label> &nbsp;
                        <label class="radio inline fbtnsgrn">
                          <input type="radio" required class="receiver_addr_state" name="receiver_addr_state" value="外島地區" <?=($this->session->userdata('method_shipping')[0]=='外島地區' || ($this->session->userdata('method_shipping')=='' && $this->session->userdata('web_member_data')['addr_state']=='外島地區'))?'checked':''?> ><span>外島地區</span>
                        </label>
                      </div>
                      <label class="col-md-12 control-label">
                        <strong>配送運費：</strong>
                      </label>
                      <div id="supplier_shipment" class="col-md-12">
                      </div>
                    </div>
                  </div>
                  <div style="clear:both;"></div>
                </p>
              </div>
            </div>
            <div class="col-md-4">
              <div class="shopping-total">
                <ul>
                  <li>
                    <em>總計 <span style="margin-left: 60px;"> (商品<span id="buy_amount"> <?=$down_cart['total_items']?> </span>件) </span></em>
                    <strong class="price" id="sub_total"><span>$</span><?php echo number_format($down_cart['sub_total']+$down_cart['coupon_total']);?></strong>
                  </li>
                  <li>
                    <em>購物金折抵 (商品<span id="use_gift_cash_amount"><?=$down_cart['gift_cash_items']?></span>件)</em>
                    <strong class="price" id="use_gift_cash"><span>$</span><?php echo number_format($down_cart['gift_cash_total']);?></strong>
                  </li>
                  <li>
                    <em>優惠折扣 <!--span style="margin-left: 24px;"> (商品<span id="use_coupon_amount"> 0 </span>件) </span--></em>
                    <strong class="price" id="use_coupon"><span>$</span><?php echo number_format($down_cart['coupon_total']);?></strong>
                  </li>
                  <li class="ship_area">
                    <em>運費 &nbsp;</em>
                    <strong class="price" id="shipping_charge_amount"><span>$</span><?=$down_cart['shipping_charge_amount']?><?php //echo $shipping_charge_amount;?></strong>
                  </li>
                  <li class="shopping-total-price">
                    <em style="font-weight: bold;">總計金額</em>
                    <strong class="price" id="total_order_amount"><span>$</span><?php echo number_format($down_cart['total_order_amount']);?></strong>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
	<table width="100%">
    <tbody>
				<!--tr>
					<td height="34" align="right"><div class="shop-item-price height-50">商品總金額：$
						<span id="total_order_amount">
							<?php
								if(@$this->cart->total()){
									echo number_format($this->cart->total());
								}else{
									echo "0";
								}
							?></span> 元</div>
					</td>
				</tr-->
				<tr>
					<td height="23" class="padding-top-20" align="center">
						<input type="button" name="button3" id="button3" value="繼續購物" onclick="location.href='/';" class="btn size-15 cart_botton">
						<input type="submit" name="button" id="checkout_submit" value="開始結帳" class="btn size-15 btn-primary cart_botton_r">
					</td>
				</tr>
		</tbody>
	</table>
 </form>
 </div>
<!-- copy data -->





<!-- END SIDEBAR & CONTENT -->
<div id="gift_cash_exclamation" style="display:none;">
  <div class="alert alert-success">
    <ul>
      <li>每次消費可百分百(100%)，1點折抵新台幣1元，全額折抵訂購金額．</li>
      <li>您可前往「會員中心」查看您持有的購物金餘額，使用紀錄．</li>
    </ul>
  </div>
</div>
<div id="coupon_input" style="display:none;">
  <div style="width:75%">
    <div style="height:32px;"></div>
    <strong>請輸入代碼（折扣券、活動代碼皆適用）</strong>
    <form class="form-horizontal" role="form" id="coupon_form">
      <div class="form-body">
        <div class="form-group">
          <div class="col-md-12">
            <div class="input-group">
<div style="display:-webkit-box;text-align:center;">
	              <input required type="text" id="couponcode" style="width:420px;" class="form-control" placeholder="請輸入活動代碼並按確定">
              <span class="input-group-btn">
              <button class="btn blue couponcode" type="submit">確定</button>
              </span>
</div>
<div class="note note-success note-bordered" id="note_couponcode" style="text-align:center;width:auto;display:none;"></div>
            </div>
            <span class="help-block">
              請注意英文字母大小寫視為不同，單次結帳恕無法重複折扣．
            </span>
          </div>
        </div>
      </div>
  <input type="hidden" id="rowid">
  <input type="hidden" id="supplier_sn">
    </form>
  </div>
</div>
<style>
.numinp{
	 padding: 5px 11px;
    border: 1px solid rgb(169, 169, 169);

}
button.arrowdown {
    height: 16px;
	border:1px solid rgb(169, 169, 169);
	background:transparent;
	border-top:none;
	border-left:none;
}
button.arrowup:focus, button.arrowdown:focus {
	outline:none;
}
button.arrowup {
    display: block;
    height: 15px;
	border:1px solid rgb(169, 169, 169);
    margin-top: 3px;
    background: transparent;
	border-bottom:none;
	border-left:none;
}
input#qty1 {
    padding: 5px;
    margin-top: 3px;
    width: 40%;
    margin-left: 18%;
}
td.drpvalue {
    width: 9%;
}
.shopping_steps {
    width: 695px;
    text-align: center;
    font-size: 1.2em;
    color: #ccc;
    height: 52px;
    margin: 20px auto;
}
.shopping_steps .step_over {
    width: 150px;
    line-height: 12px;
    height: 50px;
    float: left;
    position: relative;
    font-weight: 700;
    margin-right: 5px;
    margin-left: 5px;
	color:#ccc;
}
.shopping_steps .step_arrow {
    width: 15px;
    float: left;
}
.shopping_steps .step_normal {
    width: 150px;
    line-height: 12px;
    height: 50px;
    float: left;
    position: relative;

    color: #ccc;
    margin-right: 5px;
    margin-left: 5px;
}
.shopping_steps .step_number {
    background-color: #DB4A4A;
    color: #fff;
    position: absolute;
    top: 25px;
    left: 68px;
    font-size: 15px;
    line-height: 20px;
    height: 20px;
    width: 20px;
    border-radius: 10px !important;
}
.shopping_steps .step_normal_number {
    background-color: #ccc;
    color: #fff;
    position: absolute;
    top: 25px;
    left: 65px;
    font-size: 15px;
    line-height: 20px;
    height: 20px;
    width: 20px;
    border-radius: 10px !important;
}
i.icon-caret-right:before {
    content: "\f0da";
    font-family: fontAwesome;
    font-style: normal;
    font-size: 18px;
}
.margin-bottom-25.display_none {
    display: none;
}
.clear_shopcart {
    display: table;
    line-height: 25px;
    width: 100%;
    margin: auto;
    background-color: rgba(210,210,210,0.2);
    margin-bottom: 15px;
    border: 1px solid rgba(210, 210, 210, 0.2);
    text-align: right;
}
.clear_shopcart a {
    display: block;
    margin: 5px;
    float: right;
    cursor: pointer;
    color: #8F8F8F;
    line-height: 25px;
    height: 25px;
    padding: 5px;
    padding-top: 0px;
    padding-bottom: 0px;
}
.clear_shopcart i {
    margin-right: 5px;
    font-size: 14px;
    color: #333333;
}
i.icon-trashhs:before {
    content: "\f014";
    font-family: fontAwesome;
    font-style: normal;
}
.trans_name {
    width: 100%;
    margin: auto;
    text-align: left;
    font-size: 16px;
    color: #333333;
    margin-bottom: 10px;
}



.border_left {
    border-left: 1px solid rgba(100,100,100,0.1) !important;
}
table.responsive {
    width: 100%;
    margin: 0;
    padding: 0;
    border-collapse: collapse;
    border-spacing: 0;
}
table {
    background-color: transparent;
}
table.responsive tr {
    border: 1px solid #ddd;
}
table.responsive th {
    background-color: #F3F3F3;
}
table.responsive th {
    text-transform: uppercase;
    font-size: 14px;
    letter-spacing: 1px;
}
table.responsive th, table.responsive td {
    padding: 5px;
    text-align: center;
}
.shopping_present {
    padding: 10px;
    border: 1px solid rgba(100,100,100,0.1);
}
#cart_add_items .pics {
    width: 70px;
}
td.padding-top-20 {
    padding-bottom: 20px;
}
.shop-item-price.height-50 {
    color: #de4a50;
    font-size: 18px;
}
.shopping_present a.addcart.btn.btn-primary.btn-sm {
    border-radius: 3px !important;
    color: #fff !important;
}
.cart_botton {
    border-radius: 3px !important;
	color:#66667c;
}
.cart_botton_r {
    border-radius: 3px !important;
	color:#fff;
}
.cart_botton:hover {
    border-radius: 3px !important;
	color:#000;
}
.cart_botton_r:hover {
	color:#fff;
}
.shopping_steps .step_over {

    color: #ccc;
}
.blkfont {
	font-weight: 700;
   color: #000!important;
}
.radio{
  color:#999;
  font-size:15px;
  position:relative;
}
.radio span {
    cursor: pointer;
    position: relative;
    padding-left: 28px;
}

.radio span:after{
  content:'';
  width:19px;
  height:19px;
  border: 2px solid rgba(0,0,0,0.3);
  position:absolute;
  left:0;
  top:1px;
  border-radius:100%;
  -ms-border-radius:100%;
  -moz-border-radius:100%;
  -webkit-border-radius:100%;
  box-sizing:border-box;
  -ms-box-sizing:border-box;
  -moz-box-sizing:border-box;
  -webkit-box-sizing:border-box;
}
.radio span:hover:after{

border: 2px solid #333333;
}

.radio input[type="radio"]{
   cursor: pointer;
  position:absolute;
  width:100%;
  height:100%;
  z-index: 1;
  opacity: 0;
  filter: alpha(opacity=0);
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)"
}
.radio input[type="radio"]:checked + span{
  color:#000;

}
.radio input[type="radio"]:checked + span:after{
    width:19px;
  height:19px;
  border: 2px solid #000!important;
}
.radio input[type="radio"]:checked + span:before{
  content:'';
  border:2px solid #000!important;
  width:5px;
  height:5px;
  position:absolute;
  background:#000!important;
  left:7px;
  top:8px;
  border-radius:100%;
  -ms-border-radius:100%;
  -moz-border-radius:100%;
  -webkit-border-radius:100%;
}
.fbtnsgrn:hover span:after{
   border: 2px solid #333333;
}
</style>