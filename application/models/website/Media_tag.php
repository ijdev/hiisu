<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Media_tag extends CI_Model {

    public $limit = 10; // query limit setting
    public $until = 0; // last query sn

    public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
    }
    /*
    * 禮金選項列表
    * 全數拉出管理
    * 限制的部分 toto
    */
    public function get_result( $member_sn = 0, $media_warehouse =0 )
    {
        $this->db->select('*');
        $this->db->from('ij_media_tag');
        $this->db->where('create_member_sn', $member_sn);
        if($media_warehouse > 0){
            $this->db->where('media_warehouse', $media_warehouse);
        }
        $this->db->where('status', 1);
        $this->db->order_by('media_tag_name', 'asc');
        $this->db->order_by('media_tag_sn', 'desc');
        $query = $this->db->get();
        return $result = $query->result_array();
    }
}