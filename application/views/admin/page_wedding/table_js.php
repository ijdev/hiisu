<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script src="public/metronic/admin/pages/scripts/components-pickers.js"></script>
<script>
jQuery(document).ready(function() {       
   	Metronic.init(); // init metronic core components
	  Layout.init(); // init current layout
    ComponentsPickers.init();
    
	var initTableMember = function () {
        var table = $('#table_member');
        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": 依此欄位做順向排序",
                    "sortDescending": ": 依此欄位做逆向排序"
                },
                "emptyTable": "沒有資料可以顯示",
                "info": "顯示 _TOTAL_ 中的第 _START_ 到 _END_ 筆",
                "infoEmpty": "查無結果",
                "infoFiltered": "(從總筆數 _MAX_ 筆過濾出)",
                "lengthMenu": "顯示 _MENU_ 項目",
                "search": "搜尋:",
                "zeroRecords": "查無結果",
                "paginate": {
                    "previous":"上一頁",
                    "next": "下一頁",
                    "last": "最後",
                    "first": "最前"
                }
            },

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "columns": [{
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            },{
                "orderable": true
            },{
                "orderable": true
            },{
                "orderable": true
            },{
                "orderable": true
            }, {
                "orderable": false
            }],
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,            
            "pagingType": "bootstrap_full_number",
            "columnDefs": [{  // set default column settings
                'orderable': false,
                'targets': [0]
            }, {
                "searchable": true,
                "targets": [0]
            }],
            "order": [
                [0, "desc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = jQuery('#table_member_wrapper');

        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('tr').addClass("active");
                } else {
                    $(this).attr("checked", false);
                    $(this).parents('tr').removeClass("active");
                }
            });
            jQuery.uniform.update(set);
        });

        table.on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });

        tableWrapper.find('.dataTables_length select').addClass("form-control input-xsmall input-inline"); // modify table per page dropdown
    }
    initTableMember();
		$("#ajax_msg").on("show.bs.modal", function(e) {
		    var link = $(e.relatedTarget);
		    $(this).find(".modal-content").load(link.attr("href"));
		});
		$(".addnew").on("click", function(e) {
		    $('#member_sn').val($(this).attr("member_sn"));
		    //console.log($('#member_sn').val());
		    $('#associated_order_sn').val($(this).attr("sub_order_sn"));
				$('#associated_order_sn').attr('readonly',true);
		});
	  $('select[id="ccapply_code"]').change(function() {
	        var data = $(this).val(); // Get Selected Value
					get_data(data);

    });
 		$("#member_sn").on("change", function(e) {
		    $.ajax({
					  type: 'POST',
		        url: 'Member/get_member_name',
		        data: 'data=' + $(this).val(),
		        dataType: 'text',
		        success: function(result) {
		    			$('#member_name').text(result);
		        }
		    });
		});
 		$("#associated_product_sn").on("change", function(e) {
		    $('#associated_product_template_sn').val($('option:selected', this).attr('associated_product_template_sn'));
		    $('#product_name').val($('option:selected', this).text());
		    //console.log($('#product_name').val());
		    $.ajax({
					  type: 'POST',
		        url: 'Member/get_product_package_config',
		        data: 'product_sn=' + $(this).val(),
		        dataType: 'json',
		        success: function(result) {
		         	$('#product_package_config_sn').empty();
							$(result).appendTo("#product_package_config_sn");
					 		$("#product_package_config_sn").on("change", function(e) {
		    				$('#upload_pic_total_amount').val($('option:selected', this).attr('upload_pic_total_amount'));
		    				$('#website_usable_duration').val($('option:selected', this).attr('website_usable_duration'));
		    				$('#album_pic_total_amount').val($('option:selected', this).attr('album_pic_total_amount'));
		    				$('#album_video_total_amount').val($('option:selected', this).attr('album_video_total_amount'));
		    				$('#sms_total_amount').val($('option:selected', this).attr('sms_total_amount'));
		    				$('#promo_price').val($('option:selected', this).attr('promo_price'));
		    				$('#product_package_name').val($('option:selected', this).text());
		    //console.log($('#product_package_name').val());
		    				if($('option:selected', this).attr('password_flag')=='1'){
		    					$('#password_flag1').attr('checked',true);
		    				}else{
		    					$('#password_flag2').attr('checked',true);
		    				}
								$.uniform.update();
							});							
		        }
		    });
		});

    

		/*$(".addnew").on("click", function(e) {
				info = [];
				info['member_sn'] = $(this).attr("member_sn");
				info['associated_product_template_sn'] = $(this).attr("associated_product_template_sn");
		    $.ajax({
					  type: 'POST',
		        url: 'http://test-invitation.ijwedding.com/api/create_wedding_website',
   					data: {info:info},
		        dataType: 'json',
		        success: function(result) {
		    			console.log(result);
		        }
		    });
		});*/
});
</script>

