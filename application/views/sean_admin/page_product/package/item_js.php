<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script>
jQuery(document).ready(function() {       
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	
	
	

    /* Action On Select Box Change */
    $('select[id="category_spec_relation"]').change(function() {
 
        var data = $(this).val(); // Get Selected Value
 
        // Display Loader
 /*
        $('.loader').ajaxStart(function() {
           $(this).removeClass('hide');
        });
        $('.loader').ajaxComplete(function(){
        $(this).addClass('hide');
    });
 */
    /* Send Ajax Request on Every
    * Select Box Change Event
    */
    $.ajax({
        url: 'product/package_items',
        data: 'data=' + data,
        dataType: 'json',
        success: function(data) {
            /*
            var str = "<option value=''>Please Select</option>";
 
            $.each(data, function(i, items) {
                str += "<option value='"+items.id+"'>"+items.subject+"</option>";
            });
 
            $('select[id="category_spec_relation"]').html( str );
            */
            //$('div[id="checkbox-list-id"]').html( str );
            
						
						$("#checkbox-list-id").html(data);


            
            
        }
    });
 
});
 
 
 
  
  
  
});

  





</script>

