<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Member SignUp sidebar Important Information
*/
?>
<h2><em>貼心</em> 提醒</h2>
<p>您可以透過表單與我們的客服聯繫，系統在收到您的申請後會通知客服人員儘快為您解決您的問題．</p>

<a type="button" class="btn btn-default" data-toggle="modal" href="#ticket_form">填寫問題</a>
<!-- Modal for 表單填寫 -->
<div class="modal fade" id="ticket_form" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">請說明您的問題</h4>
			</div>
			<div class="modal-body">
				 <div class="form">
				 	<form role="form">
						<div class="form-body">
							<div class="form-group">
								<label>訂單編號</label>
								<div class="input-icon">
									<i class="fa fa-file-o"></i>
									<input type="text" class="form-control" placeholder="若您欲詢問的問題與訂單有關，請您儘可能提供">
								</div>
							</div>
							<div class="form-group">
								<label>問題類型</label>
								<select class="form-control">
									<option>退換貨</option>
									<option>帳號 / 密碼 / 設定</option>
								</select>
							</div>
							<div class="form-group">
								<label>附加檔案</label>
								<input type="file" id="exampleInputFile1">
								<p class="help-block">
									 ※ 檔案大小2MB為限，附件格式僅接受jpeg、png。
								</p>
							</div>
							<div class="form-group">
								<label>問題說明 <span class="require">*</span></label>
								<div class="input-icon">
									<textarea class="form-control" rows="5"></textarea>
								</div>
							</div>
						</div>
					</form>
				 </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">關閉</button>
				<button type="button" class="btn blue">送出表單</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
