<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容
*/
?>
<div class="container mob_padding">

<!-- Page level plugin styles START -->
<!-- Page level plugin styles END -->
<ul class="breadcrumb cre_breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <li class="active">禮金點數</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar col-md-2 col-sm-3">
    <div class="desktop_view_sidebar">
		<ul class="list-group margin-bottom-25 sidebar-menu">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
	<div class="mobile_view_sidebar">
		<div class="side-nav-head border_title_bottom">
			<button class="fa fa-bars collapsed" data-toggle="collapse" data-target="#mob_sidebar_drop_down"></button>
			<h4 class="bfont">會員中心<!--會員中心--></h4>
		</div>
		<ul class="list-group margin-bottom-25 sidebar-menu collapse" id="mob_sidebar_drop_down">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div id="credit_tab_contant" class="col-md-10 col-sm-9">
  
		

  <ul class="nav nav-tabs credit_tab_buttons">
    <li class="active"><a data-toggle="tab" href="#home">紅利紀錄</a></li>
    <li><a data-toggle="tab" href="#menu1">紅利消費紀錄</a></li>
       
  </ul>

  <div   class="tab-content credit_tab_cont mob_padding">
    <div id="home" class="tab-pane fade in active credit_tab">
			<h5>您目前有<span class="goldfont">9</span>紅利可用</h5>
		<table class="col-md-12 credit_tab_table">
		 <tr class="cre_tb_head">
			<th class="col-md-1 col-sm-1"> 編號</th>
			<th class="col-md-3 col-sm-3"> 說明</th>
			<th class="col-md-1 col-sm-1"> 積分</th>
			<th class="col-md-2 col-sm-2"> 交易類型</th>
			<th class="col-md-2 col-sm-2"> 使用狀態 </th>
			<th class="col-md-3 col-sm-3"> 使用期限</th>
		 </tr>
		 
		 <tr class="cre_tb_desc">
			<td class="col-md-1 col-sm-1 mob_title2" data-title="編號"> 3363</td>
			<td class="col-md-3 col-sm-3 mob_title2" data-title="說明"> fiywind@yahoo.com.tw</td>
			<td class="col-md-1 col-sm-1 mob_title2" data-title="積分"> 1</td>
			<td class="col-md-2 col-sm-2 mob_title2" data-title="交易類型"> 用狀態使用期限</td>
			<td class="col-md-2 col-sm-2 mob_title2" data-title="使用狀態"> 用期限</td>
			<td class="col-md-3 col-sm-3 mob_title2" data-title="使用期限"> 2016-11-25 - 2017-11-25</td>
		 </tr>
		 
		  <tr class="cre_tb_desc">
			<td class="col-md-1 col-sm-1 mob_title2" data-title="編號"> 3358</td>
			<td class="col-md-3 col-sm-3 mob_title2" data-title="說明"> fiywind@yahoo.com.tw</td>
			<td class="col-md-1 col-sm-1 mob_title2" data-title="積分"> 1</td>
			<td class="col-md-2 col-sm-2 mob_title2" data-title="交易類型"> 用狀態使用期限</td>
			<td class="col-md-2 col-sm-2 mob_title2" data-title="使用狀態"> 用期限</td>
			<td class="col-md-3 col-sm-3 mob_title2" data-title="使用期限"> 2016-11-25 - 2017-11-24</td>
		 </tr>
		  <tr class="cre_tb_desc">
			<td class="col-md-1 col-sm-1 mob_title2" data-title="編號"> 3363</td>
			<td class="col-md-3 col-sm-3 mob_title2" data-title="說明"> fiywind@yahoo.com.tw</td>
			<td class="col-md-1 col-sm-1 mob_title2" data-title="積分"> 1</td>
			<td class="col-md-2 col-sm-2 mob_title2" data-title="交易類型"> 用狀態使用期限</td>
			<td class="col-md-2 col-sm-2 mob_title2" data-title="使用狀態"> 用期限</td>
			<td class="col-md-3 col-sm-3 mob_title2" data-title="使用期限"> 2016-11-25 - 2017-11-25</td>
		 </tr>
		 
		  <tr class="cre_tb_desc">
			<td class="col-md-1 col-sm-1 mob_title2" data-title="編號"> 3358</td>
			<td class="col-md-3 col-sm-3 mob_title2" data-title="說明"> fiywind@yahoo.com.tw</td>
			<td class="col-md-1 col-sm-1 mob_title2" data-title="積分"> 1</td>
			<td class="col-md-2 col-sm-2 mob_title2" data-title="交易類型"> 用狀態使用期限</td>
			<td class="col-md-2 col-sm-2 mob_title2" data-title="使用狀態"> 用期限</td>
			<td class="col-md-3 col-sm-3 mob_title2" data-title="使用期限"> 2016-11-25 - 2017-11-24</td>
		 </tr>
		  <tr class="cre_tb_desc">
			<td class="col-md-1 col-sm-1 mob_title2" data-title="編號"> 3363</td>
			<td class="col-md-3 col-sm-3 mob_title2" data-title="說明"> fiywind@yahoo.com.tw</td>
			<td class="col-md-1 col-sm-1 mob_title2" data-title="積分"> 1</td>
			<td class="col-md-2 col-sm-2 mob_title2" data-title="交易類型"> 用狀態使用期限</td>
			<td class="col-md-2 col-sm-2 mob_title2" data-title="使用狀態"> 用期限</td>
			<td class="col-md-3 col-sm-3 mob_title2" data-title="使用期限"> 2016-11-25 - 2017-11-25</td>
		 </tr>
		 
		  <tr class="cre_tb_desc">
			<td class="col-md-1 col-sm-1 mob_title2" data-title="編號"> 3358</td>
			<td class="col-md-3 col-sm-3 mob_title2" data-title="說明"> fiywind@yahoo.com.tw</td>
			<td class="col-md-1 col-sm-1 mob_title2" data-title="積分"> 1</td>
			<td class="col-md-2 col-sm-2 mob_title2" data-title="交易類型"> 用狀態使用期限</td>
			<td class="col-md-2 col-sm-2 mob_title2" data-title="使用狀態"> 用期限</td>
			<td class="col-md-3 col-sm-3 mob_title2" data-title="使用期限"> 2016-11-25 - 2017-11-24</td>
		 </tr>
		  <tr class="cre_tb_desc">
			<td class="col-md-1 col-sm-1 mob_title2" data-title="編號"> 3358</td>
			<td class="col-md-3 col-sm-3 mob_title2" data-title="說明"> fiywind@yahoo.com.tw</td>
			<td class="col-md-1 col-sm-1 mob_title2" data-title="積分"> 1</td>
			<td class="col-md-2 col-sm-2 mob_title2" data-title="交易類型"> 用狀態使用期限</td>
			<td class="col-md-2 col-sm-2 mob_title2" data-title="使用狀態"> 用期限</td>
			<td class="col-md-3 col-sm-3 mob_title2" data-title="使用期限"> 2016-11-25 - 2017-11-24</td>
		 </tr>
	</table>
		
		</div>
  
  <div id="menu1" class="tab-pane fade credit_tab">
      <h5>您目前有<span class="goldfont">9</span>紅利可用   </h5>
	  <table class="col-md-12 credit_tab_table">
		 <tr class="cre_tb_head">
			<th class="col-md-2 col-sm-1"> 編號 </th>
			<th class="col-md-4 col-sm-3">說明</th>
			<th class="col-md-2 col-sm-1"> 積分</th>
			<th class="col-md-4 col-sm-3"> 使用期限</th>
		 </tr>
		 
		 <tr class="cre_tb_desc">
			<td class="col-md-2 col-sm-1 mob_title2" data-title="編號"> 473</td>
			<td class="col-md-3 col-sm-3 mob_title2" data-title="說明"> 期限20161200033期限</td>
			<td class="col-md-2 col-sm-1 mob_title2" data-title="積分"> 1</td>
			<td class="col-md-4 col-sm-3 mob_title2" data-title="使用期限"> 2016-11-22</td>
			
		 </tr>
		  <tr class="cre_tb_desc">
			<td class="col-md-2 col-sm-1 mob_title2" data-title="編號"> 472</td>
			<td class="col-md-3 col-sm-3 mob_title2" data-title="說明"> 期限20161200033期限</td>
			<td class="col-md-2 col-sm-1 mob_title2" data-title="積分"> 1</td>
			<td class="col-md-4 col-sm-3 mob_title2" data-title="使用期限"> 2016-11-12</td>
			
		 </tr>
		 </table>
		 
		 <h5 class="cre_outer_text">期限:2,期1-2</h5>
      
    </div>
   </div>


  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
    <!-- <div class="product-page">
      <h1>
        <i class="fa fa-dollar"></i> 禮金點數明細
      </h1>
      <div class="alert alert-success">
         目前可用點數 <strong>$ 1,300</strong>
      </div>
      <?php if($this->uri->segment(3)!="history"):?>
        <div class="portlet box red">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-gift"></i> 電子禮金明細
            </div>
            <div class="actions">
              <?php if($this->uri->segment(3)):?>
                <a href="www/credit" class="btn btn-default btn-sm">
                <i class="fa fa-list-alt"></i> 回上層 </a>
              <?php else:?>
                <a href="www/credit/provide" class="btn btn-default btn-sm">
                <i class="fa fa-list-alt"></i> More </a>
              <?php endif;?>
            </div>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-hover" id="table_member">
              <thead>
                <tr>
                  <th>贈送人姓名</th>
                  <th>贈送日期</th>
                  <th>贈送點數</th>
                  <th>使用狀態</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=6;$i>0;$i--):?>
                  <tr class="odd gradeX">
                    <td>2482368723</td>
                    <td>2015-07-06</td>
                    <td>1,000</td>
                    <?php if($i==6):?>
                      <td>300</td>
                    <?php else:?>
                      <td>1,000</td>
                    <?php endif;?>
                  </tr>
                <?php endfor;?>
              </tbody>
            </table>
            <?php if($this->uri->segment(3)):?>
              <!-- pager -->
              <!--  <div class="row">
                <div class="col-md-5 col-sm-12">
                  <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
                </div>
                <div class="col-md-7 col-sm-12">
                  <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                    <ul class="pagination" style="visibility: visible;">
                      <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                      <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li class="active"><a href="#">5</a></li>
                      <li><a href="#">6</a></li>
                      <li><a href="#">7</a></li>
                      <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                      <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <!-- End of pager -->
            <?php endif;?>
           <!-- </div>
        </div>
      <?php endif;?>
      <?php if($this->uri->segment(3)!="provide"):?>
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-gift"></i> 使用紀錄
            </div>
            <div class="actions">
              <?php if($this->uri->segment(3)):?>
                <a href="www/credit" class="btn btn-default btn-sm">
                <i class="fa fa-list-alt"></i> 回上層 </a>
              <?php else:?>
                <a href="www/credit/history" class="btn btn-default btn-sm">
                <i class="fa fa-list-alt"></i> More </a>
              <?php endif;?>
            </div>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-hover" id="table_member">
              <thead>
                <tr>
                  <th>訂單編號</th>
                  <th>訂單日期</th>
                  <th>使用金額</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=6;$i>0;$i--):?>
                  <tr class="odd gradeX">
                    <td>2482368723</td>
                    <td>2015-07-06</td>
                    <td>1,000</td>
                  </tr>
                <?php endfor;?>
              </tbody>
            </table>
            <?php if($this->uri->segment(3)):?>
              <!-- pager -->
              <!--  <div class="row">
                <div class="col-md-5 col-sm-12">
                  <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
                </div>
                <div class="col-md-7 col-sm-12">
                  <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                    <ul class="pagination" style="visibility: visible;">
                      <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                      <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li class="active"><a href="#">5</a></li>
                      <li><a href="#">6</a></li>
                      <li><a href="#">7</a></li>
                      <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                      <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <!-- End of pager -->
            <?php endif;?>
           <!-- </div>
        </div>
        <!-- /.portlet box blue -->
      <?php endif;?>
     <!-- </div>-->
  </div>
  <!-- END CONTENT -->
</div>
</div>
<!-- END SIDEBAR & CONTENT -->