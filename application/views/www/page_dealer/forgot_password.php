<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容
*/
?>
<div class="main-container scrollbar-space" style="background-image:url(/public/uploads/<?php echo $banner[0]?>)">
  <div class="med-style">
      <div class="content-inner clearfix">
  <!-- BEGIN CONTENT -->
  <div class="col-md-12 col-sm-12">
    <div class="product-page sign_in_frm">
      <?php $this->load->view('www/general/flash_error');?>
      <h1>
        <i class="fa fa-question-circle"></i> 忘記密碼了嗎？
      </h1>
      <div class="row">
        <div class="col-md-7 col-sm-7">
          <form class="form-horizontal form-without-legend" role="form" method="post">
            <div class="form-group">
              <label for="email" class="col-lg-4 control-label">Email <span class="require">*</span></label>
              <div class="col-lg-8">
                <input type="text" class="form-control" name="email" id="email" required>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-4 control-label">驗證碼 <span class="require">*</span></label>
              <div class="col-lg-8">
                <input type="text" class="form-control" name="rycode" id="rycode" size="6" required><br>
               <?php echo $_captcha_img;?>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-5">
                <button type="submit" class="btn btn-primary">重設密碼</button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-4 col-sm-4 pull-right">
          <div class="form-info">
          <h2><em>重要</em>說明</h2>
          <p><?php echo $this->page_data['dealer']['dealer_name']?>會寄發一封信給您註冊時使用的，使用信件中的連結便可重新設定您的密碼。</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END CONTENT -->
</div>
</div>
</div>
<!-- END SIDEBAR & CONTENT -->
<style>
.row.chnge_pass label {
    font-weight: 600;
}

</style>