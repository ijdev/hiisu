<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
//require_once "lang.php";
?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <?php echo $_page_title;?>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase"><?php echo $_caption_subject;?></span>
            </div>
            <div class="actions btn-set">
              <a href="<?php echo $init_control;?><?php echo $_father_link;?>" class="btn green"><i class="fa fa-list"></i> <?php echo $this->lang->line("back_list");?></a>


            </div>
          </div>
          <div class="portlet-body form">
          <?php $this->load->view('sean_admin/general/flash_error');?>
                 <form <?php
                 	if(isset($_action_link))
                 	echo " action=\"".$_action_link."\" ";
                 	?> class="form-horizontal error" novalidate="">
									<?php
										if(isset($em_columns) && is_array($em_columns))
										{
										 	$_form_one= new sean_form_general();

										 	if(isset($_result01) && is_array($_result01))
										 	{
										 		$_form_one->add_item($em_columns,$_result01);

											}else{
												$_form_one->add_item($em_columns);
											}
										}
										//var_dump($_result01);
									?>
                <div class="form-group">
                  <label for="updater" class="col-md-3 control-label">程式與行為</label>
                  <div class="col-md-8" >
                    <?php if($addons){?>
                        <table class="table table-striped table-bordered table-hover" id="table_member">
                          <thead>
                            <tr>
				                    	<?php if($addons){?>
                              <th style="text-align:center;">程式</th>
				                      <?php }else{?>
                              <th style="text-align:center;">請先新增角色</th>
				                      <?php }?>
				                    	<?php foreach($category_attributes as $ca){ if(@$ca['status']){?>
				                      	<th style="text-align:center;"><?php echo @$ca['system_rule_name'];?></th>
				                      <?php } }?>
                            </tr>
                          </thead>
                          <tbody id="Addon_list">
                          	<?php if($addons){foreach($addons as $addon){?>
                            <tr class="odd gradeX" id="Addon_tr<?php echo $addon['sys_program_config_sn'];?>">
                              <td align="center" <?php if($addon['system_rule_set']=='0' || $addon['upper_sys_program_config_sn']=='0'){ echo 'style="background:#dddddd;"';}?> ><?php echo @$addon['sys_program_name'];?></td>
				                    	<?php if($addon['system_rule_set']!='0' && $addon['upper_sys_program_config_sn']!='0'){ foreach($addon['system_rule'] as $_key_Sr=>$sr){ ?>
				                      	<td align="center">
				                      		<label class="checkbox-inline"><input type="checkbox" name="system_rule[<?php echo $addon['role_sys_program_relation_sn'];?>][]" value="<?php echo @$sr['system_rule_type'];?>" <?php if(@$sr['status']){ echo 'checked';}?>/></label>
																</td>
				                      <?php }
				                      for ($i = $_key_Sr; $i < 5; $i++) {
				                       	  echo '<td>&nbsp;</td>';
				                      }
				                      }else{ ?>
				                      	<td align="center" style="border-left-style: hidden;background:#dddddd;" colspan="6">
																</td>
				                      <?php } ?>
                            </tr>
                            <?php } }?>
                          </tbody>
                        </table>
                        <?}else{
                          echo '<label class="control-label">無須設定</label>';
                        }?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-md-3 control-label">更新者</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" placeholder="<?php echo (@$_result01[0]->update_member_sn)?@$_result01[0]->update_member_sn:@$_result01[0]->create_member_sn; ?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-md-3 control-label">更新時間</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" placeholder="<?php echo (@$_result01[0]->last_time_update!='0000-00-00 00:00:00')?@$_result01[0]->last_time_update:@$_result01[0]->create_date; ?>" Disabled>
                  </div>
                </div>
                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                          <button type="submit" class="btn green">儲存</button>
                          <a href="<?php echo $init_control;?><?php echo $_father_link;?>" class="btn green"><i class="fa fa-list"></i> <?php echo $this->lang->line("cencel_list");?></a>

                        </div>
                      </div>
                    </div>
                  </form>
            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
