<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>行銷管理 <small>實體券列印</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="preview">首頁</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        優惠活動
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="preview/certificate">實體券管理</a>
      </li>
    </ul>
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN SETTING TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">新增實體券列印</span>
            </div>
            <div class="tools">
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form">
              <div class="form-body">
                <div class="row">
                  <div class="col-md-5 col-sm-12">
                    <div class="form-group">
                      <label for="name" class="col-lg-4 control-label">列印名稱</label>
                      <div class="col-lg-8">
                        <input type="text" class="form-control" placeholder="列印日期或活動名稱">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-5 col-sm-12">
                    <div class="form-group">
                      <label for="name" class="col-lg-4 control-label">數量</label>
                      <div class="col-lg-8">
                        <input type="text" class="form-control" placeholder="10000">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-12">
                    <button type="submit" class="btn green" onclick="alert('新增完成'); return false;">新增兌換券列印</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END SETTING TABLE PORTLET-->
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">管理兌換券列印</span>
            </div>
            <div class="actions btn-set">
              <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="10" maxlenght="10" style="margin: 0 5px;" placeholder="關鍵字">
              <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button>
            </div>
          </div>
          <div class="table-actions-wrapper pull-right">
            <span>
            </span>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>列印名稱</th>
                  <th>兌換券數量</th>
                  <th>發送數量</th>
                  <th>啟用數量</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=1;$i<=10;$i++):?>
                  <tr class="odd gradeX">
                    <td>
                      第<?php echo $i;?>次列印
                    </td>
                    <td class="center">100</td>
                    <td class="center">80</td>
                    <td class="center">20</td>
                    <td class="center">
                      <a href="javascript:;" onclick="alert('開始下載');return false;"><i class="fa fa-download"></i>編號下載</a> &nbsp;&nbsp;
                      <a href="preview/certificateItem"><i class="fa fa-list"></i>列表</a> &nbsp;&nbsp;
                      <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                    </td>
                  </tr>
                <?php endfor;?>
              </tbody>
            </table>

            <div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                  <ul class="pagination" style="visibility: visible;">
                    <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                    <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li class="active"><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                    <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->