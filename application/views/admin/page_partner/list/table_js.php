<?php defined('BASEPATH') OR exit('No direct script access allowed');
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script>
jQuery(document).ready(function() {
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout

	 /* Action On Select Box Change */
    $('select[id="addr_city_code"]').change(function()
    {

        var data = $(this).val(); // Get Selected Value

			    $.ajax({
			        url: '/member/get_town_zip',
			        data: 'data=' + data,
			        dataType: 'json',
			        success: function(data) {
			         		 $('#addr_town_code').empty();
									 $(data).appendTo("#addr_town_code");
			        }
			    });
		});
    /* Action On Select Box Change */

		var initTableMember = function () {
        var table = $('#table_member');
        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": 依此欄位做順向排序",
                    "sortDescending": ": 依此欄位做逆向排序"
                },
                "emptyTable": "沒有資料可以顯示",
                "info": "顯示 _TOTAL_ 中的第 _START_ 到 _END_ 筆",
                "infoEmpty": "查無結果",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "顯示 _MENU_ 項目",
                "search": "搜尋:",
                "zeroRecords": "查無結果"
            },

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "columns": [{
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
             }, {
                "orderable": true
             }, {
                "orderable": true
            }, {
                "orderable": false
            }],
            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            "pagingType": "bootstrap_full_number",
            "language": {
                "search": "快速搜尋: ",
                "lengthMenu": "  _MENU_ 顯示",
                "paginate": {
                    "previous":"上一頁",
                    "next": "下一頁",
                    "last": "最後",
                    "first": "最前"
                }
            },
            "columnDefs": [{  // set default column settings
                'orderable': false,
                'targets': [0]
            }, {
                "searchable": false,
                "targets": [0]
            }],
            "order": [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = jQuery('#table_member_wrapper');

        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('tr').addClass("active");
                } else {
                    $(this).attr("checked", false);
                    $(this).parents('tr').removeClass("active");
                }
            });
            jQuery.uniform.update(set);
        });

        table.on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });

        tableWrapper.find('.dataTables_length select').addClass("form-control input-xsmall input-inline"); // modify table per page dropdown
    }
    initTableMember();
    $(document).on('click', '.deleteItem', function(event) {
        var button = $(this);
        item_id=button.attr('ItemId');
        //console.log(button);
        var r = confirm('是否停用此筆聯盟會員?');
        if(r){
            $.post('/admin/admin/ajaxdel/', {dtable: '<?php echo base64_encode('ij_member');?>',dfield:'<?php echo base64_encode('ij_member.member_sn');?>',did:button.attr('ItemId')}, function(data){
                if (data){
                    if($('#member_status'+item_id).html()=='<span class="label label-danger">停用</span>'){
                        button.parents().parents().filter('tr').remove();
                    }else{
                        $('#member_status'+item_id).load('<?=$actual_link?> #member_status'+item_id);
                    }
                }else{
                    alert('很抱歉，刪除失敗。');
                }
            });
        }
    });
});
</script>

