<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>系統設定 <small>婚宴地點管理</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="<?php echo $_home_url;?>">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        系統設定
        <i class="fa fa-circle"></i>
      </li>
      <li>
        婚宴地點管理
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
       
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-map-marker font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">婚宴地點管理</span>
            </div>
            <div class="actions btn-set">
              <a href="<?php echo $_action_link;?>" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <div class="note note-warning">
            <div class="table-actions-wrapper pull-right">
              <span>
              </span>
              <form class="form-horizontal" role="form" method="POST" action="<?php echo $_search_link;?>">
              <input type="text" name="wedding_location_name" id="wedding_location_name" class="pagination-panel-input form-control input-inline input-sm" size="8" style="margin: 0 5px;" placeholder="地點">
             
              <select onchange="return CheckAjaxCall();" name="addr_city_code" id="addr_city_code" class="table-group-action-input form-control input-inline input-small input-sm" style="font-size:11px;">
                
                <option value="">選擇縣市</option>
                <?php
       					$_city_list=$this->config->item("city_ct");
                 while (list($keys, $values) = each ($_city_list)) {
    							echo  ' <option value="'.$keys.'">'.$values.'</option>';

								}
                
                ?>
              </select>
             
              <select name="addr_town_code" id="addr_town_code"  class="table-group-action-input form-control input-inline input-small input-sm" style="font-size:11px;">
                <option value="">選擇鄉鎮市區</option>
                
              </select>
           
              <select name="status" id="status" class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;">
                <option value="">選擇狀態</option>
                <option value="1">啟用</option>
                <option value="0">非啟用</option>
              </select>
              <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button>
            </form>
            </div>
            <div style="clear:both;"></div>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>地點名稱</th>
                  <th>地址</th>
                  <th>啟用</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
               
               <?php
               
              	 if(is_array($_result01))
                	{                                  
                 			foreach($_result01 as $key => $value)
                    		{                    			                    			                    			
                    			if($value->status == 1)
                    				{
                    					$_active = '<i class="fa fa-check"></i>';
                    				}
                    			else
                    				{
                    					$_active = '<i class="fa fa-times"></i>';
                    				}
               
               ?>
                
                  <tr class="odd gradeX">
                    <td><?php echo $value->wedding_location_name;?></td>
                    <td>台北市內湖區樂群xxxxxxxxxx</td>
                    <td class="center">
                      <?php echo $_active;?>
                    </td>
                    <td class="center">
                      <a href="<?php echo $_edit_link.$value->$_table_key;?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                      <a href="<?php echo $_delete_link.$value->$_table_key;?>" onclick="return deletechecked();"><i class="fa fa-trash-o"></i>刪除</a>
                    </td>
                  </tr>
               <?php
               
			             }
			           }
           			?>
                
                
                
                
              </tbody>
            </table>

            <!-- page nav -->
            <?php
						echo $_pagination;
						?>
            <!-- end of page nav -->
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->

 