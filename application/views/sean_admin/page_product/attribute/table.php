<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品分類 <small>分類屬性</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="<?php echo $init_control;?>">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品分類
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         分類屬性
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">分類屬性列表</span>
            </div>
            <div class="actions btn-set">
              <a href="<?php echo $_action_link;?>" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>分類屬性名稱</th>
                  <th>排序</th>
                  <th>屬性值數量</th>
                  <th>是否啟用</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
              <?php
               
              	 if(is_array($_result01))
                	{                                  
                 			foreach($_result01 as $key => $value)
                    		{                    			                    			                    			
                    			if($value->status == 1)
                    				{
                    					$_active = '<i class="fa fa-check"></i>';
                    				}
                    			else
                    				{
                    					$_active = '<i class="fa fa-times"></i>';
                    				}
               
               ?>
                
                  <tr class="odd gradeX">
                    <td><?php echo $value->$_table_field["分類屬性名稱"];?></td>
                    <td><?php echo $value->$_table_field["排序"];?></td>
                    <td><?php echo $this->Sean_db_tools->db_get_max_num("attribute_value_sn","ij_attribute_value_config"," where attribute_sn='".$value->$_table_field["id"]."'");?></td>
                    <td class="center"><?php echo $_active;?></td>
                    <td class="center">
                      <a href="<?php echo $_edit_link.$value->$_table_key;?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                      <a href="<?php echo $_delete_link.$value->$_table_key;?>" onclick="return deletechecked();"><i class="fa fa-trash-o"></i>刪除</a>&nbsp;&nbsp;
                      <a href="<?php echo $_sp01_link.$value->$_table_key;?>" "><i class="fa fa-edit"></i>設定屬性值</a>
                    
                    
                    
                    </td>
                  </tr>
               <?php
               
			             }
			           }
           			?>
                
                 	
              	<!--
                <?php for($i=0;$i<12;$i++):?>
                  <tr class="odd gradeX">
                    <td>屬性<?php echo $i;?></td>
                    <td><?php echo $i;?></td>
                    <td>
                      5
                    </td>
                    <td class="center">
                      <?php if($i%2==0):?>
                        <i class="fa fa-check"></i>
                      <?php else:?>
                        <i class="fa fa-times"></i>
                      <?php endif;?>
                    </td>
                    <td class="center">
                      <a href="preview/productAttributeItem"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                      <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a> &nbsp;&nbsp;
                      <a href="preview/productAttributeValue?attr=<?php echo $i;?>"><i class="fa fa-edit"></i>設定屬性值</a>
                    </td>
                  </tr>
                <?php endfor;?>
                -->
              </tbody>
            </table>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->