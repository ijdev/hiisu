<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<!-- END PAGE LEVEL STYLES -->
<style>
  .feature-img{vertical-align: bottom;}
  .adType{display: none;}
</style>


<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>廣告素材設定</h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin-ijwedding/Ads">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin-ijwedding/Ads/adsMaterial">廣告素材</a><i class="fa fa-circle"></i>
      </li>
      <li class="active">
        廣告素材設定
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">廣告素材設定</span>
            </div>
            <div class="tools">
              <a href="admin-ijwedding/Ads/adsMaterial"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">廣告名稱</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" value="廣告名稱廣告名稱">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">起始時間</label>
                  <div class="col-lg-4">
                    <div class="input-group date form_datetime">
                      <input type="text" class="form-control" value="2015-05-08 12:00">
                      <span class="input-group-btn">
                        <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">結束時間</label>
                  <div class="col-lg-4">
                    <div class="input-group date form_datetime">
                      <input type="text" class="form-control" value="2015-05-08 12:00">
                      <span class="input-group-btn">
                        <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">客戶名稱</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" value="客戶名稱客戶名稱">
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">頻道</label>
                  <div class="col-lg-8">
                    <select class="form-control">
                      <option>首頁</option>
                      <option>新人編輯首頁</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">版位</label>
                  <div class="col-lg-8">
                    <select class="form-control">
                      <option>版位一</option>
                      <option>版位二</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-3 control-label">敘述</label>
                  <div class="col-lg-8">
                    <textarea class="form-control" rows="3"></textarea>
                  </div>
                </div>
                <!-- 型態的部分由版位選擇確定後鎖死 -->
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">型態</label>
                  <div class="col-lg-8">
                    <select class="form-control" name="adType" disabled="">
                      <option value="img" selected="">圖片</option>
                      <option value="text">文字</option>
                      <option value="html">HTML</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">廣告內容</label>
                  <div class="col-lg-8">
                    <div class="adType imgAD">
                      <img src="public/metronic/frontend/pages/img/works/img3.jpg" height="160" class="feature-img">
                      <i class="fa fa-trash-o"></i>
                    </div>
                    <div class="adType textAD">
                      <input type="text" class="form-control" placeholder="廣告顯示文字">
                    </div>
                    <div class="adType htmlAD">
                      <textarea class="form-control" rows="3" placeholder="請貼入 HTML 代碼"></textarea>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">廣告連結</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" value="http://www.keenthemes.com/admin-ijwedding/Ads/metronic/theme/templates/frontend/page-faq.html">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">產業屬性: <span class="required"> * </span></label>
                  <div class="col-md-8">
                    <div class="checkbox-list">
                      <label class="checkbox-inline"><input type="checkbox" name="" value="">飯店</label>
                      <label class="checkbox-inline"><input type="checkbox" name="" value="">婚紗</label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">View</label>
                  <div class="col-lg-8">
                    <span class="form-control-static">0</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">Click</label>
                  <div class="col-lg-8">
                    <span class="form-control-static">0</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">是否啟用 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <select class="form-control">
                      <option>是</option>
                      <option>否</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-lg-3 control-label">更新者</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="administrator" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-3 control-label">更新時間</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="2015-05-25 10:00" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="button" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->