<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>企業採購訂單列表</h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">企業採購訂單列表</span>
            </div>
            <div class="actions btn-set">
              <a href="javascript:;" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <div class="note note-warning">
            <div class="input-group input-large date-picker input-daterange  pull-left" data-date="2015-05-07" data-date-format="yyyy-mm-dd">
              <span class="input-group-addon" onclick="alert('Start Search');"> 期間 </span>
              <input type="text" class="form-control input-sm" name="from" value="2015-05-07">
              <span class="input-group-addon"> 到 </span>
              <input type="text" class="form-control input-sm" name="to" value="2015-05-13">
            </div>
            <div class="table-actions-wrapper pull-right">
              <span>
              </span>
              <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="8" style="margin: 0 5px;" placeholder="會員帳號">
              <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="8" style="margin: 0 5px;" placeholder="經銷商名稱">
              <select class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;">
                <option value="">商店頻道</option>
                <option value="">雲端婚訊</option>
                <option value="">現場報到系統</option>
                <option value="">結婚商店</option>
              </select>
              <select class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;">
                <option value="">訂單狀態</option>
                <option value="">未處理</option>
                <option value="">處理中</option>
                <option value="">處理完畢</option>
              </select>
              <select class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;">
                <option value="">付款狀態</option>
                <option value="">未付款</option>
                <option value="">已付款</option>
              </select>
              <select class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;">
                <option value="">客服人員</option>
                <option value="">Administrator</option>
                <option value="">Lucile</option>
                <option value="">Jenny</option>
              </select>
              <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button>
            </div>
            <div style="clear:both;"></div>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>訂單編號</th>
                  <th>會員帳號</th>
                  <th>經銷商名稱</th>
                  <th>商品數量</th>
                  <th>應收總價</th>
                  <th>訂單日期</th>
                  <th>訂單狀態</th>
                  <th>付款狀態</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=0;$i<8;$i++):?>
                  <tr class="odd gradeX">
                    <td>2345682</td>
                    <td><a href="javascript:;" onclick="alert('前往經銷商編輯');">DSDS&nbsp;<i class="fa fa-external-link"></i></a></td>
                    <td>DSDS</td>
                    <td class="text-center">
                      20
                    </td>
                    <td>870</td>
                    <td><small>2015-05-20 12:00:00</small></td>
                    <td class="text-center">
                      <?php if($i%3==0):?>
                        <span class="label label-default">已結束</span>
                      <?php elseif($i%3==1):?>
                        <span class="label label-warning">處理中</span>
                      <?php else:?>
                        <span class="label label-danger">未處理</span>
                      <?php endif;?>
                    </td>
                    <td class="text-center">
                      <?php if($i%2==0):?>
                        <span class="label label-warning">未付款</span>
                      <?php else:?>
                        <span class="label label-default">已付款</span>
                      <?php endif;?>
                    </td>
                    <td class="text-center">
                      <a href="preview/orderItem"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                      <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                    </td>
                  </tr>
                <?php endfor;?>
              </tbody>
            </table>
            <!-- page nav -->
            <div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                  <ul class="pagination" style="visibility: visible;">
                    <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                    <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li class="active"><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                    <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- end of page nav -->
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->