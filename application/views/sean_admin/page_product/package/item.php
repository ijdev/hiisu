<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
if(isset($_result01) && is_array($_result01))
{
 	foreach($_result01 as $key => $value)
  {
  	
  	$_result_item=$value;
  	
  }   
}

?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品規格 <small>規格組合</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="<?php echo $init_control;?>">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品規格
        <i class="fa fa-circle"></i>
      </li>
      <li>
         <a href="product/productPackage">規格組合</a>
         <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         規格組合編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">規格組合</span>
            </div>
            <div class="actions btn-set">
              <a href="<?php echo $_father_link;?>" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <?php $this->load->view('admin-ijwedding/general/flash_error');?>
            <form class="form-horizontal" role="form" action="<?php echo $_action_link;?>"  method="POST">
          
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">規格組合名稱 <span class="require">*</span></label>
                  <div class="col-lg-8">
                     <input type="text" name="product_package_name" id="product_package_name" maxlength="64" class="form-control" value="<?php if(isset($_result_item->product_package_name)) echo $_result_item->product_package_name;?>">
              
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">規格組合代號 <span class="require">*</span></label>
                  <div class="col-lg-8">
                     <input type="text" name="product_package_code" id="product_package_code" maxlength="32" class="form-control" value="<?php if(isset($_result_item->product_package_code)) echo $_result_item->product_package_code;?>">
              
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-md-3 control-label">商店</label>
                  <div class="col-md-8">
                    <!-- 選項在 table ij_config 中的 type = shop_menu -->
                    <select id="product_package_type" name="product_package_type" class="form-control">
                     <!-- <option value=1 <?php if(isset($_result_item->product_package_type) && $_result_item->product_package_type==1) echo "selected";?>>雲端婚訊</option>
                      <option value=2 <?php if(isset($_result_item->product_package_type) && $_result_item->product_package_type==2) echo "selected";?>>現場報到系統</option>
                     --> <option value=3 <?php if(isset($_result_item->product_package_type) && $_result_item->product_package_type==3) echo "selected";?>>結婚商店</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">分類: </label>
                  <div class="col-md-8">
                    <div class="table-scrollable table-scrollable-borderless">
                      <select id="category_spec_relation" name="category_spec_relation[]" multiple class="form-control">
                   	 <?php  echo $_upper_category_sn;?>
                  	</select>
                      <span class="help-block"> 按住 control 進行複選 </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="module" class="col-lg-3 control-label">規格</label>
                  <div class="col-lg-8">
                    <div id="checkbox-list-id"  class="checkbox-list form-md-line-input">
                      <!--
                      <label>
                        <div class="checkbox-list">
                          <label class="checkbox-inline">
                            <input class="checkbox-list" type="checkbox"> 婚紗相簿 : 使用數量(個)
                            <input type="text" size="10" value="" style="vertical-align: middle;height:18px;font-size: 12px;" >
                            <span style="color:#999;">(-1 代表無限制)</span>
                          </label>
                        </div>
                      </label>
                      <label>
                        <input type="checkbox"> 愛的剪影 : 使用時間(天) 
                        <input type="text" size="10" value="" style="vertical-align: middle;height:18px;font-size: 12px;">
                            <span style="color:#999;">(-1 代表無限制)</span>
                      </label>
                      <label>
                        <input type="checkbox"> 婚宴回函 : 使用數量(個)
                        <input type="text" size="10" value="" style="vertical-align: middle;height:18px;font-size: 12px;">
                            <span style="color:#999;">(-1 代表無限制)</span>
                      </label>
                      -->
                      <span class="help-block"> 規格項目隨著分類選擇不同 </span>
                      
                      
                    </div>
                  </div>
                </div>
               <div class="form-group">
                <label for="password" class="col-lg-3 control-label">備註</label>
                <div class="col-lg-8">
                 <textarea class="form-control maxlength-handler" rows="8" id="description" name="description" maxlength="255"><?php if(isset($_result_item->description)) echo $_result_item->description;?></textarea>
                 <span class="help-block"> max 255 chars </span>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-lg-3 control-label">排序</label>
                <div class="col-md-8">
                    <input type="text" id="sort_order" name="sort_order" class="form-control" value="<?php if(isset($_result_item->sort_order)) echo $_result_item->sort_order;?>">
                  </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-lg-3 control-label">是否啟用</label>
                <div class="col-lg-8">
                  <label class="radio-inline">
                        <input type="radio" name="status" id="status" value="1" <?php if(isset($_result_item->status) && $_result_item->status==1 || !isset($_result_item->status)) echo "checked";?>>  啟用  
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="status" id="status" value="0" <?php if(isset($_result_item->status) && $_result_item->status==0) echo "checked";?>>  不啟用 
                  </label>
                </div>
              </div>
              <!--
              <div class="form-group">
                <label for="email" class="col-lg-3 control-label">更新者</label>
                <div class="col-lg-8">
                  <input type="text" class="form-control"  placeholder="administrator" Disabled>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-lg-3 control-label">更新時間</label>
                <div class="col-lg-8">
                  <input type="text" class="form-control"  placeholder="2015-05-25 10:00" Disabled>
                </div>
              </div>
              -->
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                    <a href="<?php echo $_father_link;?>"><button type="button" class="btn default">Cancel</button></a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->