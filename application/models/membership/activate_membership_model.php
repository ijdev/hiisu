<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Activate_membership_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     *
     * activate_member
     *
     * @param string $email e-mail addres of member
     * @param string $nonce the member nonce
     * @return boolean
     *
     */

    public function activate_member($email, $activation_key) {
        $data = array('in_active' => 1,'activation_date' => date("Y-m-d H:i:s"));
        $where = array('email' => $email, 'activation_key' => $activation_key,'in_active' => 0);
        $this->db->where($where);
        $this->db->update('member', $data);
        if($this->db->affected_rows() == 1) {
            return true;
        }
        return false;
    }
}

/* End of file activate_membership_model.php */
/* Location: ./application/models/membership/activate_membership_model.php */