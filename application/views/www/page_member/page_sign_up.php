<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Page level plugin styles START -->
<link href="public/metronic/global/css/plugins.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<!-- Page level plugin styles END -->
<style type="text/css">
  .select2_dealer{
    padding: 0px;
  }
  .btn-facebook {
    color: #fff;
    background-color: #3b5998;
    border-color: rgba(0,0,0,0.2);
}
#btnblk{color:white;background-color:black;}
/************************************************/
.s_link_btn {
    padding: 0px;
}
.s_link_btn button#btnblk {
    width: 45%;
}
.s_link_btn a.btn.btn-facebook {
    width: 53%;
}
.outer_s_frm {
    margin: 0 auto;
    width: 80%;
}
.outer_s_frm label.col-xs-12.col-sm-12 {
    font-weight: 600;
}
.s_input {
    margin-left: 20px;
}
/************************************************/
.col-xs-12.col-sm-12.col-md-6.margin-bottom-0 label {
    display: block;
}

label.radio {
    margin-left: 33px!important;
}
.sidep.col-xs-12.col-sm-12.col-md-6.margin-bottom-0 {
    display: block;
    padding-left: 51px;
}
.imgrt { display:inline-block!important; margin-right:-1px;}

.col-xs-12.col-sm-12.nomargin label{display:block;}
label.error{
	color:red;
	font-weight:bold;
	font-size:120%;
}

/*
.c2form{padding-left:0px!important;}
.c1form{padding-right:0px!important;}*/
.or-line {
    margin: 20px 0;
    color: #999;
    text-align: center;
    position: relative;max-width: 300px;
}
.or-line:before {
    content: "";
    position: absolute;
    height: 1px;
    width: 100%;
    background-color: #999;
    left: 0;
    top: 9px;
}
.or-line span {
    display: inline-block;
    padding: 0 2em;
    background-color: #fff;
    position: relative;
}
a.btn.btn-block.btn-social.btn-facebook {
    background-color: #2d4373;
    color: white;
	text-align: left;
    margin-left: 0px;
    text-transform: none;
}
i.fa.fa-facebook {
    font-size: 23px;
    margin-right: 10px;
    float: left;
    width: 42px;
    font-size: 1.6em;
    text-align: center;
    border-right: 1px solid rgba(0,0,0,0.2);
}
.btn-facebook-r {
    max-width: 300px;
    margin-top: 15px;
}
</style>
<div class="container mob_padding">
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <li class="active">新會員註冊</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">



  <!-- BEGIN CONTENT -->
  <div class="outer_s_frm">
  <?php $this->load->view('www/general/flash_error');?>
  <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 s_frm"><!-- margin-bottom-60--><h2 class="size-16 margin-bottom-0">免費註冊會員</h2>
	<small class="text-muted">加入會員擁有獨家網站優惠與優寵福利</small>
	<div class="btn-facebook-r">
		<a href="member/fb_login" class="btn btn-block btn-social btn-facebook"><i class="fa fa-facebook"></i>facebook帳號註冊</a>
	</div>
    <div class="or-line"><span>OR</span></div>
  </div>
  <div class="col-md-10 col-sm-9">
  	<div class="col-xs-12 col-sm-12 col-sm-offset-1 col-md-12 col-md-offset-1 sign_up_frm">
	<form name="register-form" id="register-form" method="post" action="member/memberSignUp_Action" class="form-horizontal bv-form">
					<div class="row">
					<div class="col-xs-12 col-sm-12 margin-bottom-0"><h3 class="margin-bottom-20"><span>設定會員帳號及密碼</span></h3></div>
						<div class="row nomargin">
							<div class="col-xs-12 col-sm-12 margin-bottom-0">
							<div class="form-group  has-feedback">
								<label class="col-xs-12 col-sm-12">* 請輸入Email</label>
								<div class="col-xs-12 col-sm-12">
								<input type="email" name="email" id="email" autocomplete="off" class="form-control radius-0" placeholder="Email是您未來的帳號，不得更改" >
                  <div id="showemail" class="note note-success note-bordered" style="display:none;"></div>
								</div>
							</div>
							</div>
						</div>
						<div class="row nomargin">
							<div class="col-xs-12 col-sm-12 col-md-6 margin-bottom-0">
							<div class="form-group has-feedback">
								<label class="col-xs-12 col-sm-12">* 請輸入密碼</label>
								<div class="col-xs-12 col-sm-12">
									<input type="password" name="password" autocomplete="new-password" id="password" class="form-control radius-0">
								</div>
							</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 margin-bottom-0">
							<div class="form-group has-feedback">
								<label class="col-xs-12 col-sm-12">* 請再輸入一次</label>
								<div class="col-xs-12 col-sm-12">
									<input type="password" name="confirm_password" id="confirm_password" class="form-control radius-0">
								</div>
							</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 margin-bottom-0"><h3 class="margin-bottom-20" style="color:#0092FD;"><span>填寫會員基本資料</span></h3></div>
						<div class="row nomargin">
							<div class="col-xs-12 col-sm-12 col-md-6 margin-bottom-0">
								<div class="form-group has-feedback">
									<label class="col-xs-12 col-sm-12 ">* 姓名</label>
									<div class="col-xs-12 col-sm-12">
										<input type="text" name="last_name" id="last_name" class="form-control radius-0"></div>
								</div>
							</div>
						<div class="col-xs-12 col-sm-12 col-md-6 margin-bottom-0" style="margin-top: 26px;">
							<div class="form-group has-feedback">
									<label class="radio" style="padding-top:2px;float: left;margin-right: 5px;margin-left: 10px;"><input class="radio_btn" type="radio" name="gender" id="gender1" value="1"><i></i>男</label>
									<label class="radio" style="padding-top:2px;margin: 0;float: left;"><input class="radio_btn" type="radio" name="gender" id="gender0" value="0"><i></i>女</label>
							</div>
						</div>
						</div>
						<div class="row nomargin">
							<div class="col-xs-12 col-sm-12 col-md-6 margin-bottom-0">
							<div class="form-group has-feedback">
								<label class="col-xs-12 col-sm-12">* 行動電話</label>
								<div class="col-xs-12 col-sm-12">
									<input type="mobile" name="cell" id="cell" class="form-control radius-0" placeholder="請勿輸入-等其他符號" >
								</div>
							</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 margin-bottom-0" style="margin-top: 30px;">
							<div class="form-group has-feedback" style="margin-left: -2px;">
								<label for="member_agree_id">
								<input class="checkbx_btn" type="checkbox" name="member_agree" id="member_agree_id" value="Y">
								 我同意貴公司的<a href="member/rule" target="_blank"><u><strong>會員條款</strong></u></a>及<a href="member/privacy" target="_blank"><u><strong>隱私權政策</strong></u></a></label>
								<!--label class="col-xs-12 col-sm-12"> 市話</label>
								<div class="col-xs-12 col-sm-12">
									<input type="phone" name="tel" id="tel" class="form-control radius-0" placeholder="請加區域號碼，格式為0221345678">
								</div-->
							</div>
							</div>
						</div>
					</div>
					<!--div class="row">
					<div class="row nomargin">
						<div class="col-xs-12 col-sm-12 nomargin">
							<label>*  聯絡地址</label>
							<div class="col-xs-12 col-sm-12 col-md-4">
								<div class="form-group margin-bottom-10 has-feedback">
									<select id="addr_state" name="addr_state" class="form-control select radius-0">
					                    <option value="">請選擇</option>
					                    <option name="台灣本島">台灣本島</option>
					                    <option name="外島地區">外島地區</option>
                    				</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-4">
								<div class="form-group margin-bottom-10 has-feedback">
									<select id="province" name="province" class="form-control select radius-0" data-bv-field="province" style="display: none;"></select><i class="form-control-feedback bv-no-label" data-bv-icon-for="province" style="display: none;"></i>
									<small class="help-block" data-bv-validator="notEmpty" data-bv-for="province" data-bv-result="NOT_VALIDATED" style="display: none;">請選擇城市</small>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-4">
								<div class="form-group margin-bottom-10">
									<select id="city" name="city" class="form-control select radius-0" style="display: none;"></select>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 margin-bottom-0">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<div class="form-group margin-bottom-10">
									<input type="text" name="othercity" id="othercity" class="form-control radius-0" placeholder="郵遞區號">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-1 s_input">
								<div class="form-group has-feedback">
								<input type="text" name="address" id="address" class="form-control radius-0" data-bv-field="address"><i class="form-control-feedback bv-no-label" data-bv-icon-for="address" style="display: none;"></i>
								<small class="help-block" data-bv-validator="notEmpty" data-bv-for="address" data-bv-result="NOT_VALIDATED" style="display: none;">請輸入地址，不可空白</small><small class="help-block" data-bv-validator="regexp" data-bv-for="address" data-bv-result="NOT_VALIDATED" style="display: none;">為方便宅配按時送達，請填寫正確詳細地址喔！至少需填至號!</small></div>
							</div>
						</div>
					</div>
					</div-->
					<div class="row">
						<!--div class="row nomargin">
							<div class="c2form col-xs-12 col-sm-12 col-md-6 margin-bottom-0">
								<div class="form-group">
								<label class="col-xs-12 col-sm-12">企業團購認證密碼</label>
								<div class="col-xs-12 col-sm-12">
									<input type="text" name="companypassword" id="companypassword" class="form-control radius-0">
								</div>
							</div>
							</div>
							<div class="c1form col-xs-12 col-sm-12 col-md-6 margin-bottom-0">
							<div class="form-group">
								<label class="col-xs-12 col-sm-12">推薦人會員編號</label>
								<div class="col-xs-12 col-sm-12">
									<input type="text" name="u_recommendno" id="u_recommendno" class="form-control radius-0" value="">
								</div>
							</div>
							</div>
						</div-->

						<!--div class="row nomargin">
							<div class="col-xs-12 col-sm-12 col-md-6 margin-bottom-0">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12">
									<label class="radio col-lg-4" style="padding-top:2px"><input class="radio_btn" type="radio" name="accept_edm_flag" id="accept_edm_flag1" value="1" checked=""><i></i>訂閱電子報</label>
									<label class="radio col-lg-4" style="margin-top:0;padding-top:2px"><input class="radio_btn" type="radio" name="accept_edm_flag" id="accept_edm_flag0" value="0"><i></i>取消電子報</label>
								</div>
							</div>
							</div>
							<div class="sidep col-xs-12 col-sm-12 col-md-6 margin-bottom-0">
							<div class="form-group has-feedback">
								<div class="col-xs-12 col-sm-12">

								</div>
							</div>
							</div>
						</div-->

						<div class="row nomargin">
							<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
							<div class="col-xs-6 col-sm-6 nopadding">
							<div class="form-group has-feedback">
								<label class="col-xs-12 col-sm-12">*驗證碼</label>
								<div class="col-xs-12 col-sm-12">
									<input type="text" name="rycode" id="rycode" class="form-control radius-0">
								</div>
							</div>
							</div>
							<div class="col-xs-6 col-sm-6 margin-top-20">
              					<?php echo $_captcha_img;?>
							</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 margin-top-20 s_link_btn">
								<button type="submit" id='submit_button' class="btn btn-black" style="width:80%;">註冊</button>
								<!--a href="member/fb_login" class="btn btn-facebook"><i class="fa fa-facebook"></i>acebook</a-->
							</div>

						</div>
					</div><!--row div-->
					</div>
				</form>
				</div>
      </div>
	  </div><!--outer_s_frm-->
    </div>

  <!-- END CONTENT -->


<!-- END SIDEBAR & CONTENT -->
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v5.0"></script>