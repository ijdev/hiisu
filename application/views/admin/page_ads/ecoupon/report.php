<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<!--link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/-->
<!-- END PAGE LEVEL STYLES -->
<style>
.fileUpload {
    position: relative;
    overflow: hidden;
    margin: 10px;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
</style>


<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>行銷管理 <small>優惠代碼明細資料</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">首頁</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/marketing/ecoupon">優惠代碼</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        使用明細資料
      </li>
    </ul>
    <!-- BEGIN PAGE CONTENT INNER ->
    <div class="row">
      <div class="col-md-6">
        <!-- BEGIN ITEM TABLE PORTLET->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">活動名稱 > 狀態統計</span>
            </div>
            <div class="actions btn-set">
            </div>
          </div>
          <div class="portlet-body">
            <div id="chart_status" class="chart" style="height: 300px;"></div>
          </div>
        </div>
        <!-- END SETTING TABLE PORTLET->
      </div>
      <div class="col-md-6">
        <!-- BEGIN ITEM TABLE PORTLET->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">活動名稱 > 啟用比例</span>
            </div>
            <div class="actions btn-set">
            </div>
          </div>
          <div class="portlet-body">
            <div id="chart_enable" class="chart" style="height: 300px;"></div>
          </div>
        </div>
        <!-- END SETTING TABLE PORTLET->
      </div>
    </div-->

    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN ITEM TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase"><?php echo $Item['coupon_code_event_name'];?> > 優惠代碼使用明細</span>
            </div>
          </div>
      		<form class="form-inline" role="form" method="post" action="admin/marketing/do_uploadcsv" enctype="multipart/form-data">
      			<input name="coupon_code_config_sn" type="hidden" value="<?php echo $Item['coupon_code_config_sn'];?>">
          <div class="portlet-body form">
            <div class="note note-warning">
              <div class="pull-left">
              	<p>代碼類型：<b><?php echo $Item['type_name'];?></b></p>
              	<p>使用狀態：未使用有 <b><?php echo $no_use;?></b>  筆、已寄出有 <b><?php echo $send_out;?></b> 筆、已使用有 <b><?php echo $used;?></b> 筆</p>
              </div>
              <div class="table-actions-wrapper pull-right">
              	匯入優惠代碼：<div class="fileUpload btn btn-sm btn-danger"><span>匯入</span><input type="file" name="csvfile" class="upload" accept=".csv" /></div><br>
              	功能說明：使用預定的CSV格式，
								將設定好的優惠代碼匯入資料庫
              	<!--
                <select class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size: 11px;">
                  <option>是否發送</option>
                  <option>已發送</option>
                  <option>未發送</option>
                </select>
                <select class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size: 11px;">
                  <option>是否使用</option>
                  <option>已使用</option>
                  <option>未使用</option>
                </select>
                <select class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size: 11px;">
                  <option>是否失效</option>
                  <option>已失效</option>
                  <option>未失效</option>
                </select>
                <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="30" style="margin: 0 5px;" placeholder="使用 barcode scanner 掃 code 搜尋">
                <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋優惠代碼</button>
                -->
              </div>
              <div style="clear:both;"></div>
            </div>
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>編號</th>
                  <th>優惠代碼</th>
                  <th>驗證碼</th>
                  <th>狀態</th>
                  <th>訂單代號</th>
                  <th>使用經銷商</th>
                  <th>分類代號</th>
                  <th>產品代號</th>
                  <th>使用會員</th>
                  <th>使用日期</th>
                  <th>實付金額</th>
                  <th>折扣金額</th>
                  <th>更新時間</th>
                  <th>更新者</th>
                </tr>
              </thead>
              <tbody>
								<?php if($Items){ foreach($Items as $_Item){?>
                  <tr class="odd gradeX">
                    <td><?php echo $_Item['coupon_code_id'];?></td>
                    <td><?php echo $_Item['coupon_code'];?></td>
                    <td><?php echo $_Item['check_code'];?></td>
                    <td><?php echo $_Item['coupon_code_status'];?></td>
                    <td><?php echo $_Item['used_order_sn'];?></td>
                    <td><?php echo $_Item['dealer_short_name'];?></td>
                    <td><?php echo $_Item['used_catergory_sn'];?></td>
                    <td><?php echo $_Item['used_product_sn'];?></td>
                    <td><?php echo $_Item['last_name'];?><?php echo $_Item['first_name'];?></td>
                    <td><?php echo $_Item['used_date'];?></td>
                    <td><?php echo $_Item['actual_amount'];?></td>
                    <td><?php echo $_Item['discount_amount'];?></td>
                    <td><?php echo $_Item['last_time_update'];?></td>
                    <td><?php echo $_Item['update_member_sn'];?></td>
                    <!--td class="center">
                      <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>棄用</a>
                    </td-->
                  </tr>
                <?php }}?>
              </tbody>
            </table>

          </div>
        </form>
        </div>
        <!-- END SETTING TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
