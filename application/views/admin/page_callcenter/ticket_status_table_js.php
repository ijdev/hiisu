<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script>
jQuery(document).ready(function() {       
  Metronic.init(); // init metronic core components
	Layout.init(); // init current layout

    var table = $('#table_status');
    table.on('click', '.edit', function (e) {
        e.preventDefault();
        //console.log($('input:radio[name="qs[description]"]').filter('[value=info]'));
        var des=$(this).attr('description');
        $('#member_question_status').val($(this).attr('ItemId'));
        $('#status_name').val($(this).attr('status_name'));
 				$('input:radio[name="qs[description]"]').filter('[value='+des+']').attr('checked', true);
  			$.uniform.update();
         //$(':input:checked').parent('.radio-inline').addClass('active');
    });
    /*table.on('click', '.delete', function (e) {
        e.preventDefault();

        if (confirm("確定刪除 ?") == false) {
					return;
        }
        
				    $.post('/admin/admin/ajaxdel/', {dtable: '<?php echo base64_encode('ij_member_question_status_ct');?>',dfield:'<?php echo base64_encode('member_question_status');?>',did:$(this).attr('ItemId')}, function(data){
            	if (data){
        console.log($(this).attr('ItemId'));
            		$(this).parents().parents().filter('tr').remove();
            	}else{
            		alert('很抱歉，刪除失敗。');
            	}
				    });
    });*/
		$(".delete").click(function(){
        var button = $(this);
        //console.log(button);
        	var r = confirm('是否刪除此筆狀態?');
            if(r)
            {
        				//console.log(button.attr('ItemId'));
				    		$.post('/admin/admin/ajaxdel/', {dtable: '<?php echo base64_encode('ij_member_question_status_ct');?>',dfield:'<?php echo base64_encode('member_question_status');?>',did:$(this).attr('ItemId')}, function(data){
                	if (data){
                		button.parents().parents().filter('tr').remove();
                	}else{
                		alert('很抱歉，刪除失敗。');
                	}
						    });
            }
    });    
		$("#sample_editable_1_new").click(function(){
				$('#form')[0].reset();
  			//$.uniform.update();
        $('#member_question_status').val('');
        //console.log($('#member_question_status').val());
		});
});
</script>

