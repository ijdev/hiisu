<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>會員帳務管理</h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">會員帳務管理 - 經銷會員</span>
            </div>
            <div class="actions btn-set">
              <?/*<button type="button" class="btn btn-info" id="export">匯出Excel</button> &nbsp;*/?>
              有效分潤獎金合計：<span style="color:red;font-size:18px;font-weight: bold;"><?=number_format($total)?></span>
            </div>
          </div>
          <form id="form1" class="form-horizontal form-without-legend" role="form" method="get">
          <div class="note note-warning">
            <div class="input-group input-large date-picker input-daterange  pull-left" data-date="2015-05" data-date-format="yyyy-mm-dd">
              <span class="input-group-addon"> 期間 </span>
              <input type="text" class="form-control input-sm" name="from" value="<?=$data_array['from']?>">
              <span class="input-group-addon"> 到 </span>
              <input type="text" class="form-control input-sm" name="to" value="<?=$data_array['to']?>">
            </div>
            <div class="table-actions-wrapper pull-right">
              <span>
              </span>
              <input name="partner_name" type="text" class="pagination-panel-input form-control input-inline input-sm" size="12" style="margin: 0 5px; font-size:10px;" placeholder="經銷會員名稱" value="<?=@$data_array['partner_name']?>">
              <select name="sub_order_status" class="table-group-action-input form-control input-inline input-sm" style="font-size:11px;">
                <option value="">訂單狀態</option>
              <?php foreach($sub_order_status as $_Item){ ?>
                <option <?php echo (@$data_array['sub_order_status']==$_Item['sub_order_status'])?'selected':'';?> value="<?php echo $_Item['sub_order_status']?>"><?php echo $_Item['sub_order_status_name']?></option>
              <?php }?>
              </select>
              <select name="payment_status" class="table-group-action-input form-control input-inline input-sm" style="font-size:11px;">
                <option value="">付款狀態</option>
              <?php foreach($payment_status as $_Item){?>
                <option <?php echo (@$data_array['payment_status']==$_Item['payment_status'])?'selected':'';?> value="<?php echo $_Item['payment_status']?>"><?php echo $_Item['payment_status_name']?></option>
              <?php }?>
              </select>
              <select name="dealer_checkstatus" class="table-group-action-input form-control input-inline input-sm" style="font-size:11px;">
                <option value="">出帳狀態</option>
              <?php foreach($check_status as $_Item){?>
                <option <?php echo (@$data_array['dealer_checkstatus']==$_Item['dealer_order_status'])?'selected':'';?> value="<?php echo $_Item['dealer_order_status']?>"><?php echo $_Item['dealer_order_status_name']?></option>
              <?php }?>
              </select>
              <button type="submit" class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button>&nbsp;
              <button type="button" onclick="location.href='/admin/member/partnerAccounting'" class="btn btn-sm green table-group-action-submit"><i class="fa fa-reset"></i> 清除</button>
              <!--button type="button" class="btn btn-sm green table-group-action-submit"><i class="fa fa-download"></i> 下載</button-->
            </div>
            <div style="clear:both;"></div>
          </div>
          <input type="hidden" name="flg" id="flg" value="search">
        </form>
          <div class="portlet-body">
             <?php $this->load->view('sean_admin/general/flash_error');?>
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>訂單編號</th>
                  <th>經銷會員</th>
                  <th>訂單日期</th>
                  <th>訂單狀態</th>
                  <th>付款狀態</th>
                  <th>訂單金額</th>
                  <th>分潤％數</th>
                  <th>經銷獎金</th>
                  <th>出帳狀態</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
     <?php if(is_array(@$orders)){
        foreach($orders as $key => $value){
          /*if($value['sub_order_status']=='1' && $value['payment_status']=='2'){
            $share_total=round($value['sub_sum']*$value['share_percent']*$value['share_percent']);
          }else{
            $share_total=0;
          }*/
        ?>
            <tr class="odd gradeX">
              <td><?=$value['sub_order_num']?></td>
              <td><?=$value['dealer_name']?></td>
              <td><?=substr($value['order_create_date'],0,10)?></td>
              <td><?=$value['sub_order_status_name']?></td>
              <td><?=$value['payment_status_name']?></td>
              <td style="text-align: right;"><?=$value['sub_sum']?></td>
              <td style="text-align: right;"><?=($value['share_percent']*@$value['dealer_bonus'])*100?>％</td>
              <td style="text-align: right;"><?=$value['share_total']?></td>
              <td class="center"><span class="label label-<?=($value['dealer_checkstatus']==0)?'success':'warning'?>"><?=$checkstatus[$value['dealer_checkstatus']]?></span></td>
              <td class="text-center">
              <?php if($value['sub_order_status']=='1' && $value['payment_status']=='2'){?>
                <a href="#form_edit" class="edit" sub_order_sn="<?=$value['sub_order_sn']?>" check-status="<?=$value['dealer_checkstatus']?>" data-toggle="modal" ><i class="fa fa-edit"></i>編輯</a>
              <?php }?>
              </td>
            </tr>
<?php }}?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
<!-- Modal for 表單填寫 -->
<div class="modal fade" id="form_edit" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">出帳狀態異動</h4>
      </div>
          <form class="form-horizontal form-without-legend" role="form" method="post">
      <div class="modal-body">
         <div class="form">
            <div class="form-body">
              <!--div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-5 control-label">結算年月</label>
                    <div class="col-md-7">
                      <span class="form-control-static">201506</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-5 control-label">經銷會員</label>
                    <div class="col-md-7">
                      <span class="form-control-static"></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-5 control-label">經銷商</label>
                    <div class="col-md-7">
                      <span class="form-control-static">典華</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-5 control-label">訂單數量</label>
                    <div class="col-md-7">
                      <span class="form-control-static">20</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-5 control-label">分潤金額</label>
                    <div class="col-md-7">
                      <span class="form-control-static">2000</span>
                    </div>
                  </div>
                </div>
              </div>
              <hr-->
              <div class="form-group">
                <label class="col-md-3 control-label">出帳狀態 <span class="require">*</span></label>
                <div class="col-md-9">
                  <select class="form-control" name="dealer_checkstatus" id="checkstatus">
              <?php foreach($check_status as $_Item){?>
                <option <?php echo (@$data_array['dealer_checkstatus']==$_Item['dealer_order_status'])?'selected':'';?> value="<?php echo $_Item['dealer_order_status']?>"><?php echo $_Item['dealer_order_status_name']?></option>
              <?php }?>
                  </select>
                </div>
              </div>
              <!--div class="form-group">
                <label class="col-md-3 control-label">出帳日期 <span class="require">*</span></label>
                <div class="col-md-9">
                  <input type="text" class="form-control form-control-inline input-small date-picker" value="2015-05-08">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">實際出帳金額 <span class="require">*</span></label>
                <div class="col-md-9">
                  <input type="text" class="form-control form-control-inline input-small" value="">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">備註</label>
                <div class="col-md-9">
                  <textarea class="form-control" rows="3"></textarea>
                </div>
              </div-->
            </div>
            <div style="clear:both;"></div>
         </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">關閉</button>
        <button type="submit" class="btn blue">更新</button>
      </div>
      <input type="hidden" name="sub_order_sn" id="sub_order_sn" value="">
          </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- Modal for 表單填寫 -->