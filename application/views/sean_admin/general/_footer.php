<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Page 主架構
*/
 
?>
<?php //$this->load->view('admin-ijwedding/general/flash_error');?>

<!-- BEGIN FOOTER -->
<div class="page-footer">
  <div class="container">
     2018-<?=date('Y')?> &copy; HiiSU. All Rights Reserved.
  </div>
</div>
<div class="scroll-to-top">
  <i class="icon-arrow-up"></i>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="public/metronic/global/plugins/respond.min.js"></script>
<script src="public/metronic/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="public/metronic/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="public/metronic/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="public/metronic/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
