<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>客戶關係</h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li class="active">
        客服中心
      </li>
      <li class="active">
        系統通知管理
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
<form id="form1" class="form-horizontal" role="form" method="get">
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">系統通知列表</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/CRM/notification/form" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <div class="note note-warning">
            <div class="input-group input-large date-picker input-daterange  pull-left" data-date="2015-05" data-date-format="yyyy-mm">
              <span class="input-group-addon"> 期間 </span>
              <input type="text" class="form-control input-sm" name="from" value="<?php echo $from;?>">
              <span class="input-group-addon"> 到 </span>
              <input type="text" class="form-control input-sm" name="to" value="<?php echo $to;?>">
            </div>
            <div class="table-actions-wrapper pull-right">
              <span>
              </span>
              <input type="text" value="<?php echo $message_subject;?>" name="message_subject" class="pagination-panel-input form-control input-inline input-sm" size="12" style="margin: 0 5px; font-size:10px;" placeholder="主題">
              <select name="ifshow" class="table-group-action-input form-control input-inline input-small input-sm" style="font-size:11px;">
                <option value="">是否顯示</option>
                <option value="1" <?php echo ($ifshow==1)?'selected':'';?> >已顯示</option>
                <option value="2" <?php echo ($ifshow==2)?'selected':'';?> >未顯示</option>
              </select>
              <button type="submit" class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button> &nbsp; 
              <button id="reset_form" onclick="this.form.flg.value='reset';" class="btn btn-sm purple"><i class="fa"></i> 清除</button>
              <input type="hidden" name="flg" value="">
            </div>
            <div style="clear:both;"></div>
          </div>
            <?php $this->load->view('admin/general/flash_error');?>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>主題</th>
                  <th>內容</th>
                  <th>編輯日期</th>
                  <th>顯示時間</th>
                  <th>發送人數</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
              <?php foreach($messages as $key=>$_Item){?>
                  <tr class="odd gradeX">
                    <td><?php echo $_Item['message_subject'];?></td>
                    <td>
	                		<?php if($_Item['message_link_url']){?>
		                  	<a href="<?php echo $_Item['message_link_url'];?>" target="_blank">
		                    <?php echo $_Item['message_content'];?>&nbsp;
		                    <i class="fa fa-external-link"></i>
		                  </a>
		                  <?php }else{?>
	                  		<?php echo $_Item['message_content'];?>
		                  <?php }?>
                    </td>
                    <td class="center">
                      <small><?php echo ($_Item['last_time_update']!='0000-00-00 00:00:00')? $_Item['last_time_update']:$_Item['create_date'];?></small>
                    </td>
                    <td class="center">
                      <small><?php echo $_Item['default_display_time'];?></small>
                    </td>
                    <td class="center"><?php echo @$_Item['send_count'];?></td>
                    <td class="center">
                      <a href="admin/CRM/notification/form/<?php echo $_Item['member_message_center_sn'];?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                      <a href="javascript:void(0);" class="deleteItem" ItemId="<?php echo $_Item['member_message_center_sn'];?>"><i class="fa fa-trash-o"></i>刪除</a>
                    </td>
                  </tr>
                <?php }?>
              </tbody>
            </table>

          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </form>
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->