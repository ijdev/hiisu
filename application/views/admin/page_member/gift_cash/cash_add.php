<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<style>
    .ui-autocomplete {
        max-height: 100px;
        overflow-y: auto;
        /* prevent horizontal scrollbar */
        overflow-x: hidden;
    }
    /* IE 6 doesn't support max-height
    * we use height instead, but this forces the menu to always be this tall
    */
    * html .ui-autocomplete {
        height: 100px;
    }
</style>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>會員管理 <small>囍市錢包管理</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/admin">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        會員管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
         <a href="admin/member/gift_cash">囍市錢包管理</a>
         <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         囍市錢包新增
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">囍市錢包新增</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/member/gift_cash" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post">
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">會員流水編號*</label>
                <div class="col-md-2">
                  <input type="text" data-area="LCSC" name="Item[associated_member_sn]" id="uno" placeholder="請輸入編號或姓名" class="form-control" required >
                </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">會員名稱</label>
                <div class="col-md-10">
                  <p class="form-control-static" id="show_uno_name"></p>
                </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">類別*</label>
                <div class="col-md-10">
                  <select name="Item[cash_type]" required class="table-group-action-input form-control input-inline input-sm">
                    <option value="">類別</option>
                     <?php foreach($cash_types as $_key=>$_value){
                          echo '<option value="'.$_key.'"';
                          if(@$Item['cash_type']==$_key)
                          echo ' selected="selected"';
                          echo '>'.$_value.'</option>';
                       }?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">金額*</label>
                <div class="col-md-2">
                  <input type="number" required class="form-control" name="Item[cash_amount]" value="">
                </div>
                  <label class="col-lg-3 help-block" style="display:inline;">&nbsp;發放＋，扣除－</label>
              </div>
              <div class="form-group">
                <label for="password" class="col-md-2 control-label">內容備註</label>
                <div class="col-md-6">
                  <textarea class="form-control" rows="3" name="Item[memo]"></textarea>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" disabled id="submit_button" class="btn green">儲存</button>
                    <button type="reset" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->