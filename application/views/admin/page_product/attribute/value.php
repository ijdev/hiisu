<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品分類 <small>分類屬性</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/admin">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品分類
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/product/productAttribute">分類屬性</a>
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         分類屬性名稱 <?php echo @$current['attribute_name'];?>
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">分類屬性 <?php echo @$current['attribute_name'];?> 屬性值列表</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/product/productAttributeValueItem/0/<?php echo @$current['attribute_sn'];?>" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>分類屬性值</th>
                  <th>商品數量</th>
                  <th>排序</th>
                  <th>是否啟用</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
<?php if($Items){ ?>
<?php	foreach($Items as $Item){?>
                  <tr class="odd gradeX">
                    <td><?php echo @$Item['attribute_value_name'];?></td>
                    <td><?php echo @$Item['amount'];?></td>
                    <td><?php echo @$Item['sort_order'];?></td>
                    <td class="center">
                      <?php if($Item['status']):?>
                        <i class="fa fa-check"></i>
                      <?php else:?>
                        <i class="fa fa-times"></i>
                      <?php endif;?>
                    </td>
                    <td class="center">
                      <a href="admin/product/productAttributeValueItem/<?php echo @$Item['attribute_value_sn'];?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                      <?php if(!$Item['amount']){?><a href="javascript:void(0);" class="deleteItem" ItemId="<?php echo $Item['attribute_value_sn'];?>" Amount="<?php echo @$Item['amount'];?>" /><i class="fa fa-trash-o"></i>刪除</a><?php }?>
                    </td>
                  </tr>
<?php } }else{?>
              <tr><td align="center" colspan="5">尚無屬性值</td></tr>
<?php }?>
              </tbody>
            </table>
              <!--P.S.若該分類屬性值已有商品參考，將無法刪除。-->
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->