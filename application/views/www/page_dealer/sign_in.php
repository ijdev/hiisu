<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Member SignIn 主內容
*/
?>

<style>
.subbtn {
    color: white;
    background-color: black;
}
.divider.divider-center.margin-bottom-20.margin-top-20 {
    text-align: center;
}
a.btn.btn-block.btn-social.btn-facebook {
    background-color: #2d4373;
    color: white;
}
div.divider:after {
    content: '';
    position: absolute;
    top: 8px;
    left: 0;
    right: 0;
    height: 0;
    border-top: 1px solid #ddd;
}
div.divider {
    margin: 40px 0;
    position: relative;
    display: block;
    min-height: 20px;
}

div.divider.divider-center:before {
    left: 0 !important;
    right: 50%;
    margin-right: 20px;
    content: '';
    position: absolute;
    top: 8px;
    height: 0;
    border-top: 1px solid #ddd;
}
div.divider.divider-center:after {
    left: 50% !important;
    right: 0;
    margin-left: 20px;
}

i.fa.fa-facebook {
    font-size: 23px; margin-right:10px;
    float: left;
       width: 42px;

    font-size: 1.6em;
    text-align: center;
    border-right: 1px solid rgba(0,0,0,0.2);
}
a.btn.btn-block.btn-social.btn-facebook {
    text-align: left;
    margin-left: 0px;
    text-transform: none;
}
h2.size-16.margin-bottom-10.text-center {
    font-weight: bold;
}
h2.size-16.margin-bottom-10.text-left {
    font-weight: bold;
}
</style>
<div class="main-container scrollbar-space" style="background-image:url(/public/uploads/<?php echo $banner[0]?>)">
  <div class="med-style">
      <div class="content-inner clearfix">
  <!-- BEGIN CONTENT -->
  <div class="col-md-12 col-sm-12">
    <div class="product-page sign_in_frm">
     <!-- <h1>
        <i class="fa fa-sign-in"></i> 登入
      </h1>
-->
	<div class="row">
        <div class="col-md-5 col-sm-7">
        	<?php $this->load->view('www/general/flash_error');?>
		<!--form start here -->
				<div class="col-xs-12 col-sm-12 col-md-12 login_windows">
					<h2 class="size-16 margin-bottom-10 text-center">會員登入</h2>
					<form action="member/memberSignInAction" method="post" name="form_login" onsubmit="return checklogin(this,'請填寫E-MAIL','請填寫E-MAIL','請輸入密碼');" class="form-horizontal margin-bottom-20" role="form">
						<input type="hidden" name="Action" value="Login">
						<input type="hidden" name="Url" value="">
						<div class="row form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 margin-bottom-0">
								<input type="email" name="username" id="username" class="form-control" placeholder="請輸入註冊之電子郵件地址" required  data-validation-required-message="請輸入Email!" maxlength="60">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 margin-bottom-0">
								<input type="password" name="password" id="password" class="form-control" placeholder="密碼" required data-validation-required-message="請輸入密碼!" maxlength="30">
							</div>
						</div>
						<div class="row margin-bottom-0">
							<div class="col-xs-12 col-sm-12 col-md-12 text-right margin-bottom-0">
								<a href="dealer/memberForgotPassword"><p><i class="fa fa-unlock-alt"></i> 忘記密碼</p><!--忘記密碼--></a>
							</div>
						</div>
						<div class="form-group">
              <label for="password" class="col-lg-3 control-label captcha_title">驗證碼 <span class="require">*</span></label>
              <div class="col-lg-9 captcha_input_box">
                <input type="text" class="form-control" name="rycode" id="rycode" autocomplete="off" size="6" required data-validation-required-message="請輸入驗證碼!" maxlength="10"><br>
                <?php echo $_captcha_img;?> &nbsp;&nbsp;
              </div>
            </div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 margin-bottom-0">
								<button type="submit" name="Submit2" class="subbtn col-xs-12 col-sm-12 btn btn-black">登入</button>
							</div>
						</div>

                        <div class="divider divider-center margin-bottom-20 margin-top-20"><!-- divider -->
							OR
						</div>

						<!--<div class="row">
							<div class="col-xs-12 col-sm-12">
								<a href="reg_resend.php" class="col-xs-12 col-sm-12 btn btn-black"><i class="fa fa-envelope-o"></i>重新寄註冊驗證信</a>
							</div>
						</div>-->
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 margin-bottom-0">
								<a href="member/fb_login" class="btn btn-block btn-social btn-facebook"><i class="fa fa-facebook"></i>facebook帳號登入</a>
							</div>
						</div>
						<!--<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<a href="login_yahoo_openid.php" class="btn btn-block btn-social btn-yahoo"><i class="fa fa-yahoo"></i>yahoo帳號登入</a>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<a href="login_google.php?logout" class="btn btn-block btn-social btn-google"><i class="fa fa-google"></i>google帳號登入</a>
							</div>
						</div>-->
					</form>
					<div class="col-xs-12 col-sm-12 col-md-12 nopadding text-right">   *登入等同您同意我們的 <a href="member/rule" class="bold"><i class="fa fa-external-link"></i> 服務條款</a></div>
                    </div>

		<!--form end here-->





        </div>
		<!--
        <div class="col-md-4 col-sm-4 pull-right">
          <div class="form-info">
            <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/page_sign_in_info');?>
          </div>
        </div>
     -->


	 <!--rightside-->
			<div class="col-xs-12 col-sm-12 col-md-6 signin_btm_box">
					<div class="col-xs-12 col-sm-12 col-md-12 login_windows">
						<div class="size-16 margin-bottom-10 text-left s_in_ttl_box">
            <h2 class="size-16 margin-bottom-0" style="float: left; margin-right:10px; margin-top:1px;">還不是會員嗎？</h2>
            <a class="s_in_link" href="dealer/memberSignUp">立即註冊</a></div>
						<ul class="margin-bottom-0" style="padding: 0 25px;">
							<li>如果您是會員在購物前請先登入！</li>
							<li>如果還沒有註冊建議您先註冊可享有更多的網站服務！</li>
                        </ul>
					<div class="divider margin-top-20 margin-bottom-20"><!-- divider -->
					</div>
            <div class="size-16 margin-bottom-10 text-left s_in_ttl_box">
  						<h2 class="size-16 margin-bottom-0">特別提醒</h2>
  						<p>我們不會主動打電話要求您操作ATM或透露存款餘額，如果您接到類似可疑電話，請拒絕回應，並與本公司客服中心聯絡，或直接撥打刑事局詐騙165專線通報，以確保您的權益。請您定期更新密碼，以保障您的資料安全。</p>
              </div>
					</div>
				</div>

	 <!--rightside-->
	</div>
    </div>
  </div>
  <!-- END CONTENT -->
</div>
</div>
</div>
<!-- END SIDEBAR & CONTENT -->