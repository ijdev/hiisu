<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 線上商店商品內頁
*/
$blue='';
$purple='';
$yellow='';
if(@$cart){
	foreach($cart as $key=>$Item){
		if($Item['channel_name']=='結婚商店'){
			$blue='Y';
		}elseif($Item['channel_name']=='婚禮網站'){
			$purple='Y';
		}elseif($Item['channel_name']=='婚宴管理'){
			$yellow='Y';
		}
	}
}
?>
<style>
  .pi-img-wrapper img{border:1px solid #EFEFEF;}
  .sale-product{background-color: #EFEFEF;padding: 12px;}
  .goods-page-image img{
    -webkit-box-shadow: 0 1px 8px rgba(0, 0, 0, 0.5);
    box-shadow: 0 1px 8px rgba(0, 0, 0, 0.5);
  }
  .del-goods-col{text-align: right;line-height: 2;}
  .del-goods{}
  .box_gift_cash{float: right; font-size: 18px; padding-right:24px;}
  .fa-exclamation-circle{font-size: 18px;background-color: #FFF;color: #FF0000;}
  .fa-usd-box{border-radius: 50%;border:2px solid #dfba49;display: inline-block;border-radius: 50% !important;    width: 18px;height: 18px;text-align: center; font-size: 12px;}
  .fa-usd-box .text-warning{font-size: 10px;}

  .shopping-total-price{border-top: 1px solid #cebea2 !important;font-weight: bold !important;padding-top: 24px !important;}
  .shopping-total{ width: 92%;}

  .table-shipping{
      border:0px; width:96% !important;margin:0 auto;
  }
  .table-shipping td{border:0px;padding: 6px 0px 6px 0;}
</style>
<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<link href="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="public/metronic/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css"><!-- for slider-range -->
<link href="public/metronic/global/plugins/rateit/src/rateit.css" rel="stylesheet" type="text/css">
<!-- Page level plugin styles END -->

			<ul class="breadcrumb">
				<li><a href="/">首頁</a></li>
				<li><a href="shop">線上商店</a></li>
				<li class="active">購物車一覽</li>
			</ul>
	<!-- shopping steps -->
			<div class="shopping_steps">
				 <div class="step_over"><div class="step_normal_number">1</div>商品確認</div>
				 <div class="step_arrow"><i class="icon-caret-right"></i></div>
				 <div class="step_normal blkfont"><div class="step_number">2</div>折扣運費</div>
				 <div class="step_arrow"><i class="icon-caret-right"></i></div>
				 <div class="step_normal"><div class="step_normal_number">3</div>填寫資料</div>
				 <div class="step_arrow"><i class="icon-caret-right"></i></div>
				 <div class="step_normal"><div class="step_normal_number">4</div>訂單完成</div>
			</div>
			<!--<div class="clear_shopcart"><a onclick="location.href='cart.php?Action=clear';"><i class="icon-trashhs"></i>清空購物車</a></div>-->
	<!-- shopping steps -->
	<!-- BEGIN SIDEBAR & CONTENT -->
<div id="showshopping">
       <div class="trans_name mob_trans">購買商品清單</div>
       <table class="table-hover responsive mob_tab">
			<thead>
				 <tr>
					<th class="span_hidden">序號
					</th><th>圖片
          </th><th>商品名稱
          </th><th>規格
					</th><th>單價
					</th><th>數量
					</th><th>小計
					</th></tr>
			</thead>
			<tbody class="mob_tbody">
							<?php
								$count=1;
								if($this->page_data['cart']){foreach($this->page_data['cart'] as $Item){?>
							<tr>
							  <td data-label="序號" class="span_hidden mob_td1">
							  <?php echo $count;?>
							  </td>
							  <td class="mob_td2" data-label="圖片">
								<a href="/shop/shopItem/<?php echo $Item['product_sn'];?>">
								<?php if(!$Item['addon_log_sn']){?><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['image_path'];?>" alt="<?php echo $Item['image_alt'];?>" width="70" >
								<?php }else{?>
									<span>加購</span>
								<?php }?>
							  </a>
							  </td>
								<td class="mob_td3" data-label="商品名稱" class="receivePay_text"><a href="/shop/shopItem/<?php echo $Item['product_sn'];?>"><?php echo $Item['name'];?></a></td>
                <td><?php echo @$Item['product_package_name'];?> <?php echo @$Item['mutispec_color_name'];?></td>

								<td class="mob_td4_1" data-label="單價 NT$ "><span>$<?php echo $Item['price'];?></span></td>

								<td class="mob_td5_1" data-label="數量">                <!--{ html_options name="count" options=$Cart_item[list].goods[list1].storagelist selected=$Cart_item[list].goods[list1].count }-->
									<span>x<?php echo $Item['qty'];?></span>
								</td>
								<td class="mob_td5" data-label="小計"><span>$<?php echo $Item['subtotal'];?></span></td>
								</tr>
								<?php $supplier_name=$Item['supplier_name'];$count++; }

								}else{
								echo '<ul>購物車是空的喔，快去採購吧^^</ul>';}?>
          </tbody>
		</table>
<form class="form-horizontal" role="form" action="shop/shopView3" method="post">
	<table width="100%">
          <tbody>
				<tr>
					<td height="34" align="right"><div class="shop-item-price height-50">商品總金額：$
							<?php
								if(@$this->cart->total()){
									echo number_format($this->cart->total());
								}else{
									echo "0";
								}
							?>元
					</div></td>
				</tr>
				<tr>
					<td height="24" align="right" valign="middle">&nbsp;</td>
				</tr>
        <!--tr>
          <td class="uhea"><div class="trans_name" style="width:auto;"><i class="fa fa-arrow-circle-right"></i> 折扣計算</div>
          <ul class="nav nav-tabs navshop2 mob_padding">
            <li class="active"><a id="homeshow" data-toggle="tab" href="#home">使用紅利購物金 (目前有200點)</a></li>
            <li><a data-toggle="tab"  id="menu1show" href="#menu1">使用折價券 (目前有4張)</a></li>
          </ul>

          </td>
        </tr>
        <tr>
  <td align="center" class="uhea2">

        <div class="tab-content">
          <div id="home" class="tab-pane fade in active">
           <table width="70%" height="30" border="0" cellpadding="3" cellspacing="0" class=" tbnew1tb go_left red_bonus">
            <thead>
                <tr class="t_align_left display_block go_left W100 padding13"><th class="W100">您目前有 <span class="prices">200</span> 點紅利購物金，本次交易最多可折抵<span class="prices">50</span> 點 <span class="go_right closetb cursor_pointer">X</span></th></tr>
                  <tr class="county_title ttl2">
                    <th class="W40 web_view_tab1">使用購物金 </th>
                    <th class="W40"> 商品名稱 </th>

                  </tr>
            </thead>
            <tbody>
              <tr>
                <td class="display_block marginlb">
                      <input type="checkbox"  value="1" id="tongdinggou" class="fdcheck" name="tongdinggou">
                      <label class="sphov" for="tongdinggou"><span class="golden_f web_view_txt">(product title)中式雷雕喜帖 -    KC017V    紫梅囍鵲
                      【特急件費】一般喜帖製作
                      </span>
                      <span class="display_block margin_left5p web_view_txt">
                      【特急件費】一般喜帖製作
                      </span>
                      </label>
                    </td>
                    <td class="display_block marginlb">
                      <input type="checkbox"  value="1" id="tongdinggou2" class="fdcheck" name="tongdinggou">
                      <label class="sphov" for="tongdinggou"><span class="golden_f web_view_txt">(product title)中式雷雕喜帖 -    KC017V    紫梅囍鵲
                      【特急件費】一般喜帖製作
                      </span>
                      <span class="display_block margin_left5p web_view_txt">
                      【特急件費】一般喜帖製作
                      </span>
                      </label>
                    </td>
                  </tr>
                </tbody>
                </table>
                  <div class="W70 display_block go_left t_align_right buttonstbnew">
                    <input type="button" name="button" id="button5" value="使用"  class="W10 btn size-15 btn-primary cart_botton_r" data-toggle="collapse" data-target="#cart_botton_r_drop_down" aria-expanded="flase">
                    <input type="button" name="button3" id="button4" value=" 取消 "  class=" W10 btn size-15 cart_botton">
                  </div>
                  <div class="W70 display_block go_left t_align_left buttonshow_tab collapse" id="cart_botton_r_drop_down">
                    <div class="W100 go_left t_align_left buttonshow_tabinner_box">
                      <h2><strong>紫梅囍鵲 【特急件費】一般喜帖製作</strong></h2>
                      <p>【特急件費】一般喜帖製作1</p>
                      <p>【特急件費】一般喜帖製作2</p>
                      <p>【特急件費】一般喜帖製作3</p>
                      <span class="amt">消費總金額：  1450元</span>
                    </div>
                    <div class="W100 go_left t_align_left buttonshow_tabinner_box">
                      <h2><strong>紫梅囍鵲 【特急件費】一般喜帖製作</strong></h2>
                      <p>【特急件費】一般喜帖製作</p>
                      <p>【特急件費】一般喜帖製作</p>
                      <span class="amt">消費總金額：  1450元</span>
                    </div>
                  </div>
                  <script>
$( ".closetb" ).click(function() {
     $( "#home" ).hide();

  });
  $( "#homeshow" ).click(function() {
     $( "#home" ).show();

  });

  $( "#menu1show" ).click(function() {
     $( "#home" ).hide();

  });



</script>
      </div>





          <div id="menu1" class="tab-pane fade">
           <table width="100%" cellpadding="3" cellspacing="0" class="tickits">
                    <tbody><tr >
                    <td class="margin_bottom2"><strong>您可使用的折價券</strong></td>
                    </tr>

                    <tr>
                    <td align="left">
                      <label class="radio inline fbtnsgrn">
                          <input name="ticketuseid" type="radio" style="" value="0" checked="" />
                          <span> 折價券001現折200 </span>
                       </label>
                    </td>
                    </tr>
                    <tr>
                      <td align="left">
                        <label class="radio inline fbtnsgrn" style="float:left; margin-right: 2%;">
                            <input name="ticketuseid" type="radio" style="" value="-1">
                            <span>折價券0020005現折250 </span>
                          </label>
                      </td>
                    </tr>
                      <tr>
                      <td align="left">
                        <label class="radio inline fbtnsgrn" style="float:left; margin-right: 2%;">
                            <input name="ticketuseid2" type="radio" style="" checked="" value="-1">
                            <span>折價券003現折200 </span>
                          </label>
                      </td>
                    </tr>
                      <tr>
                      <td align="left">
                        <label class="radio inline fbtnsgrn" style="float:left; margin-right: 2%;">
                            <input name="ticketuseid2" type="radio" style="" value="-1">
                            <span>折價券004現折200 </span>
                          </label>
                      </td>
                    </tr>
                    <tr>
                    <td align="right">
                      <div class="W50 display_block go_left t_align_right buttonstbnew mob_btn_box">
                        <input type="button" name="button" id="button5" value="使用"  class=" margin_left20  W15 btn size-15 btn-primary cart_botton_r mob_cart1" data-toggle="collapse" data-target="#cart_botton_r_drop_down2">
                        <input type="button" name="button3" id="button4" value=" 取消 "  class="margin_left2 W15 btn size-15 cart_botton mob_cart2">

                      </div>
                    </td>
                    </tr>
                    <tr>
                    <td>
                    <div class="W70 display_block go_left t_align_left buttonshow_tab collapse" id="cart_botton_r_drop_down2">
                    <div class="W100 go_left t_align_left buttonshow_tabinner_box">
                      <h2><strong>您可使用的折價券</strong></h2>
                      <p>折價券001現折200</p>
                      <p>折價券0020005現折250 </p>
                      <p>折價券003現折200</p>
                      <p>折價券004現折200</p>
                      <span class="amt">消費總金額：  1450元</span>
                    </div>


                    <div class="W100 go_left t_align_left buttonshow_tabinner_box">
                      <h2><strong>您可使用的折價券</strong></h2>
                      <p>折價券001現折200</p>
                      <p>折價券0020005現折250 </p>
                      <p>折價券003現折200</p>
                      <p>折價券004現折200</p>
                      <span class="amt">消費總金額：  1450元</span>
                    </div>
                  </div>
                  </td>
                  </tr>
                  </tbody>
                </table>

                </div>
              </div>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr-->

				<tr>
					<td><div class="trans_name" style="width:auto;">配送地區及運費</div></td>
				</tr>
				<tr>
          <td height="35" align="left">
          <div class="county_title col-lg-12 col-md-12 col-sm-12 col-xs-12">
          配送地區：
            <select id="receiver_addr_state" required name="receiver_addr_state" class="nopadding height-30 margin-top-6 margin-bottom-6">
              <option value="">請選擇</option>
              <option value="台灣本島" <?=($receiver_addr_state=='台灣本島')?'selected':'';?>>台灣本島</option>
              <option value="外島地區" <?=($receiver_addr_state=='外島地區')?'selected':'';?>>外島地區</option>
            </select>
            <!--select id="province" name="province" class="nopadding height-30 margin-top-6 margin-bottom-6"><option value="">請選擇</option><option name="臺北市">臺北市</option><option name="基隆市">基隆市</option><option name="新北市">新北市</option><option name="宜蘭縣">宜蘭縣</option><option name="新竹市">新竹市</option><option name="新竹縣">新竹縣</option><option name="桃園市">桃園市</option><option name="苗栗縣">苗栗縣</option><option name="台中市">台中市</option><option name="彰化縣">彰化縣</option><option name="南投縣">南投縣</option><option name="嘉義市">嘉義市</option><option name="嘉義縣">嘉義縣</option><option name="雲林縣">雲林縣</option><option name="臺南市">臺南市</option><option name="高雄市">高雄市</option><option name="屏東縣">屏東縣</option><option name="臺東縣">臺東縣</option><option name="花蓮縣">花蓮縣</option></select-->
            &nbsp;&nbsp;<span class="prices">＊請務必選擇配送地區，謝謝。</span>
            </div>
            </td>
        </tr>
				<tr>
          <td align="center" id="showtransport"><div class="trans_inside col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
	        <!--普通配送-->
<?php if(@$delivery_method){ foreach($delivery_method as $key=>$Item){ ?>
<div class="col-md-12 col-sm-12 margin-bottom-10 mob_padding" <?if($key>0) echo 'style="border-top: 1px solid #cccccc;padding-top: 10px;"';?>>
  <div class="col-xs-6 col-md-2 col-sm-2 padding-0 mob_padding">
  			<label class="radio inline fbtnsgrn">
      <input type="radio" name="method_shipping" class="method_shipping" required value="<?php echo $Item['delivery_method'];?>" delivery_amount="<?php echo $Item['delivery_amount'];?>" outland_amount="<?php echo $Item['outland_amount'];?>" delivery_method_name="<?php echo $Item['delivery_method_name'];?>" <?=($method_shipping==$Item['delivery_method'])?'checked':'';?>>
        <span> <?php echo $Item['delivery_method_name'];?> </span>
     </label>
  </div>
  <div class="col-xs-6 col-md-2 col-sm-2 padding-0 mob_padding delivery_amount">
  	運費：<?php echo $Item['delivery_amount'];?> 元
  </div>
  <div class="col-xs-6 col-md-2 col-sm-2 padding-0 mob_padding outland_amount">
    運費：<?php echo $Item['outland_amount'];?> 元
  </div>
  <div class="col-xs-12 col-md-8 col-sm-8 padding-0 mob_padding" >
    <?php echo ($Item['description'])? str_replace('[free_shop_money]',number_format($free_shop_money),$Item['description']):'';?>
  </div>
  <div style="clear:both"></div>
</div>
<?php }}?>
<div class="shop-item-price" id="delivery" style="text-align:right !important">配送方式及運費：<span id="delivery_method">宅配</span>&nbsp;&nbsp;&nbsp;運費：<span id="delivery_money"> 元</span></div></div>
<div class="shopping2_totalnow col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right padding-0">
            <table align="right">
                          <tbody><tr>
                            <td colspan="2" align="right" style="padding-right:2px;">商品總金額：</td>
                            <td align="right" id="sub_total">元</td>
                          </tr>
                          <!--tr>
                            <td colspan="2" align="right" style="padding-right:2px;">折扣後金額：</td>
                            <td align="right">元</td>
                          </tr-->
                          <tr>
                            <td colspan="2" align="right" style="padding-right:2px;">配送費用：</td>
                            <td align="right" id="shipping_charge_amount">元</td>
                          </tr>
                          <tr>
                            <td colspan="2" align="right" class="shop-item-price">消費總金額：</td>
                            <td align="right" class="shop-item-price" id="total_order_amount">元</td>
                          </tr>
                     </tbody></table>
</div>

</td>
        </tr>
				<tr>
					<td height="23" class="padding-top-20" align="center">
						<input type="button" name="button3" id="button3" value="上一步" onclick="location.href='/shop/shopView';" class="btn size-15 cart_botton">
						<input type="submit" name="button" id="button" value="下一步" class="btn size-15 btn-primary cart_botton_r">
					</td>
				</tr>
		</tbody>
	</table>
</form>
 </div>
<!-- copy data -->




<!-- END SIDEBAR & CONTENT -->
<div id="gift_cash_exclamation" style="display:none;">
  <div class="alert alert-success">
    <ul>
      <li>每次消費可百分百(100%)，1點折抵新台幣1元，全額折抵訂購金額．</li>
      <li>系統依購物金的使用期限，自動從最接近過期的點數開始折抵．</li>
      <li>您可前往「會員中心」查看您持有的購物金餘額，使用期限與紀錄．</li>
    </ul>
  </div>
</div>
<div id="coupon_input" style="display:none;">
  <div style="width:75%">
    <div style="height:32px;"></div>
    <strong>請輸入代碼（折扣券、活動代碼皆適用）</strong>
    <form class="form-horizontal" role="form" id="coupon_form">
      <div class="form-body">
        <div class="form-group">
          <div class="col-md-12">
            <div class="input-group">
<div style="display:-webkit-box;text-align:center;">
	              <input required type="text" id="couponcode" style="width:420px;" class="form-control" placeholder="請輸入活動代碼並按確定">
              <span class="input-group-btn">
              <button class="btn blue couponcode" type="submit">確定</button>
              </span>
</div>
<div class="note note-success note-bordered" id="note_couponcode" style="text-align:center;width:auto;display:none;"></div>
            </div>
            <span class="help-block">
              請注意英文字母大小寫視為不同，單次結帳恕無法重複折扣．
            </span>
          </div>
        </div>
      </div>
	<input type="hidden" id="rowid">
    </form>
  </div>
</div>
<style>

.radio{
  color:#999;
  font-size:15px;
  position:relative;
}
.radio span {
    cursor: pointer;
    position: relative;
    padding-left: 28px;
}

.radio span:after{
  content:'';
  width:19px;
  height:19px;
  border: 2px solid rgba(0,0,0,0.3);
  position:absolute;
  left:0;
  top:1px;
  border-radius:100%;
  -ms-border-radius:100%;
  -moz-border-radius:100%;
  -webkit-border-radius:100%;
  box-sizing:border-box;
  -ms-box-sizing:border-box;
  -moz-box-sizing:border-box;
  -webkit-box-sizing:border-box;
}
.radio span:hover:after{

border: 2px solid #333333;
}

.radio input[type="radio"]{
   cursor: pointer;
  position:absolute;
  width:100%;
  height:100%;
  z-index: 1;
  opacity: 0;
  filter: alpha(opacity=0);
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)"
}
.radio input[type="radio"]:checked + span{
  color:#000;

}
.radio input[type="radio"]:checked + span:after{
	  width:19px;
  height:19px;
  border: 2px solid #000!important;
}
.radio input[type="radio"]:checked + span:before{
    content:'';
	 border:2px solid #000!important;
  width:5px;
  height:5px;
  position:absolute;
  background:#000!important;
  left:7px;
  top:8px;
  border-radius:100%;
  -ms-border-radius:100%;
  -moz-border-radius:100%;
  -webkit-border-radius:100%;
}



.shopping_steps {
    width: 695px;
    text-align: center;
    font-size: 1.2em;
    color: #333;
    height: 52px;
    margin: 20px auto;
}
.shopping_steps .step_over {
    width: 150px;
    line-height: 12px;
    height: 50px;
    float: left;
    position: relative;
    font-weight: 700;
    margin-right: 5px;
    margin-left: 5px;
}
.shopping_steps .step_arrow {
    width: 15px;
    float: left;
}
.shopping_steps .step_normal {
    width: 150px;
    line-height: 12px;
    height: 50px;
    float: left;
    position: relative;

    color: #ccc;
    margin-right: 5px;
    margin-left: 5px;
}
.shopping_steps .step_number {
    background-color: #DB4A4A;
    color: #fff;
    position: absolute;
    top: 25px;
    left: 68px;
    font-size: 15px;
    line-height: 20px;
    height: 20px;
    width: 20px;
    border-radius: 10px !important;
}
.shopping_steps .step_normal_number {
    background-color: #ccc;
    color: #fff;
    position: absolute;
    top: 25px;
    left: 65px;
    font-size: 15px;
    line-height: 20px;
    height: 20px;
    width: 20px;
    border-radius: 10px !important;
}
i.icon-caret-right:before {
    content: "\f0da";
    font-family: fontAwesome;
    font-style: normal;
    font-size: 18px;
}
.margin-bottom-25.display_none {
    display: none;
}
.clear_shopcart {
    display: table;
    line-height: 25px;
    width: 100%;
    margin: auto;
    background-color: rgba(210,210,210,0.2);
    margin-bottom: 15px;
    border: 1px solid rgba(210, 210, 210, 0.2);
    text-align: right;
}
.clear_shopcart a {
    display: block;
    margin: 5px;
    float: right;
    cursor: pointer;
    color: #8F8F8F;
    line-height: 25px;
    height: 25px;
    padding: 5px;
    padding-top: 0px;
    padding-bottom: 0px;
}
.clear_shopcart i {
    margin-right: 5px;
    font-size: 14px;
    color: #333333;
}
i.icon-trashhs:before {
    content: "\f014";
    font-family: fontAwesome;
    font-style: normal;
}
.trans_name {
    width: 100%;
    margin: auto;
    text-align: left;
    font-size: 16px;
    color: #333333;
    margin-bottom: 10px;
}



.border_left {
    border-left: 1px solid rgba(100,100,100,0.1) !important;
}
table.responsive {
    width: 100%;
    margin: 0;
    padding: 0;
    border-collapse: collapse;
    border-spacing: 0;
}
table {
    background-color: transparent;
}
table.responsive tr {
    border: 1px solid #ddd;
}
table.responsive th {
    background-color: #F3F3F3;
}
table.responsive th {
    text-transform: uppercase;
    font-size: 14px;
    letter-spacing: 1px;
}
table.responsive th, table.responsive td {
    padding: 5px;
    text-align: center;
}
.shopping_present {
    padding: 10px;
    border: 1px solid rgba(100,100,100,0.1);
}
#cart_add_items .pics {
    width: 70px;
}
td.padding-top-20 {
    padding-bottom: 20px;
}
.shop-item-price.height-50 {
    color: #de4a50;
    font-size: 18px;
}
/*************/
.uhea span.price {
    font-size: 14px;
    line-height: 22px;
}
.uhea input.form-control {
    border-radius: 3px !important;
}
.uhea input.btn.btn-primary.btn-sm {
    border-radius: 3px !important;
    color: #fff;
}
.cart_botton {
    border-radius: 3px !important;
	color:#66667c;
}
.cart_botton_r {
    border-radius: 3px !important;
	color:#fff;
}
.cart_botton:hover {
    border-radius: 3px !important;
	color:#000;
}
.cart_botton_r:hover {
	color:#fff;
}


/******************/
.shopping_present, .shopping2_present, #shopping_table .uhea, #shopping_table .county_title, #shopping_table .trans_inside, .shopping_border, table.responsive tr {
    border: 1px solid #ddd;
}
#shopping_table .trans_inside {
    padding: 10px;
    border-top: none;
}
.county_title.col-lg-12.col-md-12.col-sm-12.col-xs-12 {
    border: 1px solid #ddd;
}
.shopping2_totalnow.col-lg-12.col-md-12.col-sm-12.col-xs-12.text-right.padding-0 {
    padding: 10px 0px;
}
.trans_inside.col-lg-12.col-md-12.col-sm-12.col-xs-12.text-left {
    border: 1px solid #ddd;
    border-top: none;
    padding: 10px;
}
#shopping_table .county_title {
    padding-left: 10px;
    background: rgba(200, 200, 200, 0.2);
}
#shopping_table .county_title select {
    border: 1px solid #ffffff;
    box-shadow: 0 1px 3px #cccccc;
}
#shopping_table .trans_inside {
    padding: 10px;
    border-top: none;
}
.shopping_steps .step_over {

    color: #ccc;
}
.blkfont {
   font-weight: 700;
    color: #000!important;
}
.fbtnsgrn:hover span:after{
	 border: 2px solid #333333;
}

</style>