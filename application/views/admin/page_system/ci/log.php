<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* CI Log View
*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>System Log Viwer</title>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css" integrity="sha384-aUGj/X2zp5rLCbBxumKTCw2Z50WgIr1vs/PFN4praOTvYXWlVyh2UtNUU0KAUhAX" crossorigin="anonymous">


  <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url');?>public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>


</head>
<body>

<div>
  <div class="container">
    <div class="row" style="margin-top:24px;">
      <div class="col-md-6">
        <form class="form-signin" method="GET">
          <div class="row">
            <div class="col-md-6">
              <input type="text" id="inputDate" class="form-control date-picker" placeholder="選擇查詢日期" value="<?php echo $log_date;?>" name="date" required>
            </div>
            <div class="col-md-3">
              <button class="btn btn-primary btn-block" type="submit">查詢</button>
            </div>
          </div>
        </form>
      </div>
    </div>
      <h2>error log</h2>
      <code>
      <?php if($log_content):?>
        <?php echo nl2br(htmlentities($log_content));?>
      <?php else:?>
        無紀錄
      <?php endif?>
      </code>
    </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script>
  jQuery(document).ready(function() { 
    $('.date-picker').datepicker({
        format: "yyyy-mm-dd",
        orientation: "left",
        autoclose: true
    });
  });
</script>

</body>
</html>
