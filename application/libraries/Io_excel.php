<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Io_excel {
	var $CI;
    var $rowId = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

	function __construct()
    {
        $this->CI =& get_instance();
	}

    function partnerAccounting_export($page_data=array()){
        //var_dump($page_data);
        //引入PHPExcel函式庫
        require_once("PHPExcel.php");
        require_once("PHPExcel/IOFactory.php");
        include 'PHPExcel/Writer/Excel2007.php';
        $filename = "partnerAccounting_" . date('YmdHis');
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        // 設置屬性
        $objPHPExcel->getProperties()->setCreator("Hiisu")//作者
            ->setLastModifiedBy("Hiisu")//最後修改者
            ->setTitle($filename);//類別


        $sheetNo = 0;
        $objPHPExcel->setActiveSheetIndex($sheetNo++);
        //行號
        $excel_line = 1;
        //產生第一列
        $rowNo=0;
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "訂單編號");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "聯盟會員");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "訂單日期");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "訂單狀態");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "付款狀態");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "訂單金額");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "分潤%數");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "分潤獎金");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "出帳狀態");

        $excel_line = 2;
        foreach($page_data as $item)
        {
            $rowNo=0;
            $objPHPExcel->getActiveSheet()->getCell(PHPExcel_Cell::stringFromColumnIndex($rowNo++)."{$excel_line}")->setValueExplicit((string)$item['sub_order_num'], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", $item['partner_name']);
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", substr($item['order_create_date'],0,10));
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", $item['sub_order_status_name']);
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", $item['payment_status_name']);
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", $item['sub_sum']);
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", $item['share_percent']*@$item['bonus']);
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", $item['share_total']);
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", $item['dealer_order_status_name']);

            $excel_line++;
        }
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);

        //Excel檔名
        $filename = $filename.".xlsx";

        //ob_end_clean();
        //產生header
        header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
        //header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=$filename" );
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Pragma: public");

        //產生Excel下載檔
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }

    function supplierAccounting_export($page_data=array(),$checkstatus){
        //var_dump($page_data);
        //引入PHPExcel函式庫
        require_once("PHPExcel.php");
        require_once("PHPExcel/IOFactory.php");
        include 'PHPExcel/Writer/Excel2007.php';
        $filename = "supplierAccounting_" . date('YmdHis');
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        // 設置屬性
        $objPHPExcel->getProperties()->setCreator("Hiisu")//作者
            ->setLastModifiedBy("Hiisu")//最後修改者
            ->setTitle($filename);//類別

        $sheetNo = 0;
        $objPHPExcel->setActiveSheetIndex($sheetNo++);
        //行號
        $excel_line = 1;
        //產生第一列
        $rowNo=0;
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "訂單編號");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "供應商");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "訂單日期");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "訂單狀態");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "付款狀態");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "訂單金額");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "入賬％數");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "入賬金額");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "出帳狀態");

        $excel_line = 2;
        foreach($page_data as $item)
        {
            $rowNo=0;
            $objPHPExcel->getActiveSheet()->getCell(PHPExcel_Cell::stringFromColumnIndex($rowNo++)."{$excel_line}")->setValueExplicit((string)$item['sub_order_num'], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", $item['supplier_name']);
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", substr($item['order_create_date'],0,10));
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", $item['sub_order_status_name']);
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", $item['payment_status_name']);
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", $item['sub_sum']);
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", (1-$item['share_percent']));
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", $item['share_total']);
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", $checkstatus[$item['checkstatus']]);

            $excel_line++;
        }
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);

        //Excel檔名
        $filename = $filename.".xlsx";

        //ob_end_clean();
        //產生header
        header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
        //header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=$filename" );
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Pragma: public");

        //產生Excel下載檔
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }

    function dealerAccounting_export($page_data=array()){
        //var_dump($page_data);
        //引入PHPExcel函式庫
        require_once("PHPExcel.php");
        require_once("PHPExcel/IOFactory.php");
        include 'PHPExcel/Writer/Excel2007.php';
        $filename = "partnerAccounting_" . date('YmdHis');
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        // 設置屬性
        $objPHPExcel->getProperties()->setCreator("Hiisu")//作者
            ->setLastModifiedBy("Hiisu")//最後修改者
            ->setTitle($filename);//類別


        $sheetNo = 0;
        $objPHPExcel->setActiveSheetIndex($sheetNo++);
        //行號
        $excel_line = 1;
        //產生第一列
        $rowNo=0;
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "訂單編號");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "經銷會員");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "訂單日期");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "訂單狀態");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "付款狀態");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "訂單金額");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "分潤%數");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "經銷獎金");
        $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", "出帳狀態");

        $excel_line = 2;
        foreach($page_data as $item)
        {
            $rowNo=0;
            $objPHPExcel->getActiveSheet()->getCell(PHPExcel_Cell::stringFromColumnIndex($rowNo++)."{$excel_line}")->setValueExplicit((string)$item['sub_order_num'], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", $item['dealer_name']);
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", substr($item['order_create_date'],0,10));
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", $item['sub_order_status_name']);
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", $item['payment_status_name']);
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", $item['sub_sum']);
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", $item['share_percent']*@$item['dealer_bonus']);
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", $item['share_total']);
            $objPHPExcel->getActiveSheet()->setCellValue($this->rowId[$rowNo++]."{$excel_line}", $item['dealer_order_status_name']);

            $excel_line++;
        }
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);

        //Excel檔名
        $filename = $filename.".xlsx";

        //ob_end_clean();
        //產生header
        header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
        //header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=$filename" );
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Pragma: public");

        //產生Excel下載檔
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }
}