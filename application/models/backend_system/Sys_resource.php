<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sys_resource extends My_general_ct {

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('sean_models/Sean_db_tools');
		$this->_table="ij_sys_resource";
		$this->_id="sys_resource_sn";
		
		$this->table_field=array(
					 "sys_program_sn" => "系統程式代號",
					 "sys_resource_name" => "資源中文名稱",
					 "sys_resource_eng_name" => "資源英文名稱",
					 "resource_rule_code" => "資源規則代碼",
					 "sort_order" => "排序順次",
					 "hide_flag" => "是否隱藏",
					 "status" => "使用狀態",
				);
    }
    
   
    public function table_value_list( $_where,$_id = 0) 
	{
		$_table_key=$this->_id;    
	    $page_data['_body_result']=array();
		if(strlen($_where)==0)
		{
			$_where="  order by sort_order asc";
			 
		}
		 	 	 	 	 	 	
		$_result01=$this->Sean_db_tools->db_get_max_record("*",$this->_table,$_where);
		
		/* 列表標頭名稱 */
		$page_data['_table_thead_name']=array("系統程式代號","資源中文名稱","資源英文名稱","資源規則代碼","排序順次","是否隱藏","使用狀態","功能");
		$page_data['_table_thead_field']=array("sys_program_sn","sys_resource_name","sys_resource_eng_name","resource_rule_code","sort_order","hide_flag","status","功能");
		/* 類表內容 */
		if(is_array($_result01))
		{                                  
                 		$_i=0;
                 			foreach($_result01 as $key => $value)
                    	{                    			                    			                    			
			                    		
			                    		if($value->status == 1)
			                    		{
			                    					$_active = '<i class="fa fa-check"></i>';
			                    		}else{
			                    					$_active = '<i class="fa fa-times"></i>';
			                    		}
										if($value->hide_flag == 1)
			                    		{
			                    					$_hidden = '<i class="fa fa-check"></i>';
			                    		}else{
			                    					$_hidden = '<i class="fa fa-times"></i>';
			                    		}
			                    				
			                    		$_body_result='';
			            
						
			                    		$page_data['_body_result'][$_i][0]=array("_value"=>$value->sys_program_sn,"_class"=>"");
			                    		$page_data['_body_result'][$_i][1]=array("_value"=>$value->sys_resource_name,"_class"=>"");
			                    		$page_data['_body_result'][$_i][2]=array("_value"=>$value->sys_resource_eng_name,"_class"=>"");
			                    		$page_data['_body_result'][$_i][3]=array("_value"=>$value->resource_rule_code,"_class"=>"");
			                    		$page_data['_body_result'][$_i][4]=array("_value"=>$value->sort_order,"_class"=>"");
										$page_data['_body_result'][$_i][5]=array("_value"=>$_hidden,"_class"=>"");	
									    $page_data['_body_result'][$_i][6]=array("_value"=>$_active,"_class"=>"");
			                    		$page_data['_body_result'][$_i][7]=array("_value"=>'
			                    		 		<a href="'.$this->init_control.'sys_resource_item/edit/'.$value->$_table_key.'"><i class="fa fa-edit"></i>編輯</a>&nbsp;&nbsp;
						                      <a href="'.$this->init_control.'sys_resource_item/delete/'.$value->$_table_key.'" onclick="return deletechecked();"><i class="fa fa-trash-o"></i>刪除</a>&nbsp;&nbsp;
						                  	',"_class"=>'class="center"');
                				$_i++;
                			}
                	
    				}
			return $page_data;
			
    }

	
	
}

/* End of file Sys_role.php */
/* Location: ./application/models/backend_system/Sys_role.php */
