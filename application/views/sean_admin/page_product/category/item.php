<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/

if(isset($_result01) && is_array($_result01))
{
 	foreach($_result01 as $key => $value)
  {
  	
  	$_result_item=$value;
  	
  }   
}


?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品分類 <small>分類設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="<?php echo $init_control;?>">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品分類
        <i class="fa fa-circle"></i>
      </li>
      <li>
         <a href="product/productCategory">分類管理</a>
         <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         商品分類編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">分類設定</span>
            </div>
            <div class="actions btn-set">
              <a href="<?php echo $_father_link;?>" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <?php $this->load->view('admin-ijwedding/general/flash_error');?>
            <form class="form-horizontal" role="form" action="<?php echo $_action_link;?>"  method="POST" enctype="multipart/form-data" >
        	<div class="form-group">
              <label for="name" class="col-md-3 control-label">是否發佈</label>
              <div class="col-md-8">
                <div class="radio-list">
                    <label class="radio-inline">
                      <input type="radio" name="category_status" id="category_status" value="1" <?php if(isset($_result_item->category_status) && $_result_item->category_status==1) echo "checked";?>>  發佈  
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="category_status" id="category_status" value="0" <?php if(isset($_result_item->category_status) && $_result_item->category_status==0) echo "checked";?>>  不發佈 
                    </label>
                </div>
              </div>
            </div>
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-md-3 control-label">名稱 <span class="require">*</span></label>
                  <div class="col-md-8">
                    <input type="text" id="category_name"  name="category_name" maxlength="64" class="form-control" value="<?php if(isset($_result_item->category_name)) echo $_result_item->category_name;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-md-3 control-label">副標</label>
                  <div class="col-md-8">
                    <input type="text"  id="category_sub_name"  name="category_sub_name"  maxlength="120" class="form-control" value="<?php if(isset($_result_item->category_sub_name)) echo $_result_item->category_sub_name;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-md-3 control-label">商店 <span class="require">*</span></label>
                  <div class="col-md-8">
                    <!-- 選項在 table ij_config 中的 type = shop_menu -->
                    <select id="root_category_flag" name="root_category_flag" class="form-control">
                     <!-- <option value=1 <?php if(isset($_result_item->root_category_flag) && $_result_item->root_category_flag==1) echo "selected";?>>雲端婚訊</option>
                      <option value=2 <?php if(isset($_result_item->root_category_flag) && $_result_item->root_category_flag==2) echo "selected";?>>現場報到系統</option>
                     --> <option value=3 <?php if(isset($_result_item->root_category_flag) && $_result_item->root_category_flag==3) echo "selected";?>>結婚商店</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">上層分類 <span class="require">*</span></label>
                  <div class="col-md-8">
                  	<select id="upper_category_sn" name="upper_category_sn" class="form-control">
                   	 <?php  echo $_upper_category_sn;?>
                  	</select>
                  	<!--
                    <select id="upper_category_sn" name="upper_category_sn" class="form-control">
                      <option value="0">├─/</option>
                     
                      <option value="2001">├─喜帖 邀請卡</option>
                      <option value="3289"> │ ├─本館強檔推薦</option>
                      <option value="3288"> │  │ ├─最新上市</option>
                      <option value="3295"> │  │ ├─test3</option>
                      <option value="3294"> │  │ ├─test2</option>
                      <option value="3305"> │ ├─電子喜帖</option>
                      <option value="3286"> │ ├─西式喜帖設計</option>
                      <option value="3297"> │ ├─中式精選喜帖</option>
                      <option value="3298"> │ ├─傳統燙金喜帖</option>
                      <option value="3208"> │ ├─客製創意喜帖</option>
                      <option value="3215"> │ ├─喜帖週邊製作</option>
                      <option value="3212"> │  │ ├─RSVP 回函卡</option>
                      <option value="3234"> │  │ ├─信封貼紙</option>
                      <option value="3264"> │  │ ├─喜帖信封</option>
                      <option value="3235"> │  │ ├─喜帖製作費</option>
                      <option value="3081"> │ ├─活動請柬/邀請卡</option>
                      <option value="1001">├─婚禮小物</option>
                      <option value="3292"> │ ├─本館強檔推薦[未發佈]</option>
                      <option value="3299"> │  │ ├─test</option>
                     
                    </select>
                     -->
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-md-3 control-label">分類屬性</label>
                  <div class="col-md-8">
                    <div class="checkbox-list">
                      <?php 
                      foreach($_ij_attribute as $kkey => $vvalue)
                      {
                      	$_checked="";
                      	if(isset($_ij_attribute_value))
                      	{
	                      	foreach($_ij_attribute_value as $kkey2 => $vvalue2)
	                      	{
	                      		if($vvalue->attribute_sn== $vvalue2->attribute_sn && $vvalue2->status==1)
	                      		$_checked=" checked=\"checked\"";
	                      	}
                        }
                      	echo '<label class="checkbox-inline"><input type="checkbox" name="attr[]" value="'.$vvalue->attribute_sn.'" '.$_checked.'>'.$vvalue->attribute_name.'</label>';
                      
                      }
                      ?>
                      
                       </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="seq" class="col-md-3 control-label">經銷折扣 <span class="require">*</span></label>
                  <div class="col-md-8">
                    <div class="form-group">
                      <div class="col-lg-1">
                        <input type="text" id="profit_sharing_rate" name="profit_sharing_rate"   maxlength="3" class="form-control form-inline" value="<?php if(isset($_result_item->profit_sharing_rate)) echo $_result_item->profit_sharing_rate;?>">
                      </div>
                      <div class="col-lg-1">
                        <span class="form-control-static form-inline">%</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">分類 Banner</label>
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-lg-6">
                        <input class="form-control" name="banner_save_dir" id="banner_save_dir"  type="file" id="banner_save_dir" value="<?php if(isset($_result_item->banner_save_dir)) echo $_result_item->banner_save_dir;?>">
                       <?php 
                        if(isset($_result_item->banner_save_dir) && strlen($_result_item->banner_save_dir) > 2 ) 
                        {
                       	 	echo '<div class="image_wrap"><img width="200px"  src="./public/img/'.$_result_item->banner_save_dir.'">';
                      		echo '<a href="javascripr:void(0)" class="delete_file_link" data-file_id="'.$_result_item->category_sn.'"> <i class="fa fa-trash-o"></i></a></div>';
                      	}
                        ?>
                        
                      </div>
                      <div class="col-lg-6">
                        <label class="col-md-4 control-label">圖片描述</label>
                        <div class="col-md-8">
                          <input type="text" name="banner_save_alt" id="banner_save_alt"  maxlength="120" class="form-control" placeholder="預設為分類名稱" value="<?php if(isset($_result_item->banner_save_alt)) echo $_result_item->banner_save_alt;?>">
                       
                        </div>
                      </div>
                    </div>
                    <span class="help-block">最佳大小 1920px X 280px</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Meta Title</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control maxlength-handler" id="meta_title" name="meta_title" maxlength="100" placeholder="" value="<?php if(isset($_result_item->meta_title)) echo $_result_item->meta_title;?>">
                    <span class="help-block"> max 100 chars </span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Meta Keywords</label>
                  <div class="col-md-8">
                    <textarea class="form-control maxlength-handler" rows="8" id="meta_keywords" name="meta_keywords"  maxlength="1000"><?php if(isset($_result_item->meta_keywords)) echo $_result_item->meta_keywords;?></textarea>
                    <span class="help-block"> max 1000 chars </span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Meta Description</label>
                  <div class="col-md-8">
                    <textarea class="form-control maxlength-handler" rows="8" id="meta_content" name="meta_content" maxlength="255"><?php if(isset($_result_item->meta_content)) echo $_result_item->meta_content;?></textarea>
                    <span class="help-block"> max 255 chars </span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">退貨說明</label>
                  <div class="col-md-8">
                    <textarea name="return_description" id="return_description" class="form-control maxlength-handler" rows="8"  maxlength="1000"><?php if(isset($_result_item->return_description)) echo $_result_item->return_description;?></textarea>
                     <span class="help-block"> max 1000 chars </span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="seq" class="col-md-3 control-label">排序 <span class="require">*</span></label>
                  <div class="col-md-8">
                    <input type="text" id="sort_order" name="sort_order" class="form-control" value="<?php if(isset($_result_item->sort_order)) echo $_result_item->sort_order;?>">
                  </div>
                </div>
                
               
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                    <a href="<?php echo $_father_link;?>"><button type="button" class="btn default">Cancel</button></a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->