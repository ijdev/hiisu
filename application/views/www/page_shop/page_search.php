<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div><!-- class="container"-->
<style>
  /*.pi-img-wrapper img{border:1px solid #EFEFEF;}*/
  .sale-product{background-color: #EFEFEF;padding: 12px;}
  @media (max-width: 768px){
	  #shop_catepg .product-item .pi-img-wrapper a{position: relative; padding-top: 100%;}
	  #shop_catepg .product-item .pi-img-wrapper a img{position: absolute; top: 0; left: 0; width: 100% !important;}
  }
  .share{
    float: right;margin-top: 12px;font-size: 14px;
  }
</style>
<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<link href="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="public/metronic/global/plugins/slider-layer-slider/css/layerslider.css" rel="stylesheet">
<link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css"><!-- for slider-range -->
<!-- Page level plugin styles END -->

<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li class="active">搜尋結果</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row"><!-- margin-bottom-40-->
  <!-- BEGIN CONTENT -->
  <div class="clearfix"><!--row margin-bottom-40 -->
    <!-- BEGIN SIDEBAR -->
    <div class="sidebar col-md-3 col-sm-4" style="display:none;">
       <div class="sidebar-products margin-bottom-40 clearfix">
        <h2>優惠商品</h2>
<?php if(@$promo_product3){ foreach($promo_product3 as $Item){?>
        <div class="item">
          <a href="shop/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $Item['default_root_category_sn'];?>"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['image_path'];?>" alt="<?php echo $Item['image_alt'];?>"></a>
          <h3><a href="shop/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $Item['default_root_category_sn'];?>"><?php echo $Item['product_name'];?></a></h3>
          <div class="price">$<?php echo $Item['price1'];?>&nbsp;<font style="font-size:12px;color:#333333;"><?php echo $Item['price2'];?></font></div>
        </div>
<?php }}?>
      </div>

      <div class="sidebar-products margin-bottom-40 clearfix">
        <h2>本類熱賣</h2>
<?php if(@$promo_product2){ foreach($promo_product2 as $Item){?>
        <div class="item">
          <a href="shop/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $Item['default_root_category_sn'];?>"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['image_path'];?>" alt="<?php echo $Item['image_alt'];?>"></a>
          <h3><a href="shop/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $Item['default_root_category_sn'];?>"><?php echo $Item['product_name'];?></a></h3>
          <div class="price">$<?php echo $Item['price1'];?>&nbsp;<font style="font-size:12px;color:#333333;"><?php echo $Item['price2'];?></font></div>
        </div>
<?php }}?>
      </div>

      <div class="sidebar-products clearfix">
        <h2>本類熱門</h2>
<?php if(@$promo_product5){ foreach($promo_product5 as $Item){?>
        <div class="item">
          <a href="shop/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $Item['default_root_category_sn'];?>"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['image_path'];?>" alt="<?php echo $Item['image_alt'];?>"></a>
          <h3><a href="shop/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $Item['default_root_category_sn'];?>"><?php echo $Item['product_name'];?></a></h3>
          <div class="price">$<?php echo $Item['price1'];?>&nbsp;<font style="font-size:12px;color:#333333;"><?php echo $Item['price2'];?></font></div>
        </div>
<?php }}?>
      </div>
   
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
<form id="form1" class="form-horizontal" role="form" method="get">
    <div><!-- class="col-md-12 col-sm-12"-->
      <div class="content-search margin-bottom-20">
        <div class="row">
          <div class="col-md-12">
            <h1 style="font-size:18px;">搜尋結果 :  <em><?php echo $search;?></em></h1>
          </div>
            <div class="col-md-12 text-right"><!-- pull-right" style="display:inline-flex;"-->
              <label class="control-label">排序依據：</label>
               <select class="form-control input-sm" name="sort" style="width:175px; display: inline-block;" onchange="chk_search();">
                  <option value="sort_order" <?php if($sort=='sort_order'){?>selected="selected"<?php }?>>最新商品</option>
                  <option value="name_ASC" <?php if($sort=='name_ASC'){?>selected="selected"<?php }?>>名稱 (A - Z)</option>
                  <option value="name_DESC" <?php if($sort=='name_DESC'){?>selected="selected"<?php }?>>名稱 (Z - A)</option>
                  <option value="price_ASC" <?php if($sort=='price_ASC'){?>selected="selected"<?php }?>>價格 (Low &gt; High)</option>
                  <option value="price_DESC" <?php if($sort=='price_DESC'){?>selected="selected"<?php }?>>價格 (High &gt; Low)</option>
                  <option value="rating_DESC" <?php if($sort=='rating_DESC'){?>selected="selected"<?php }?>>訂購數量 (Highest)</option>
                  <option value="rating_ASC" <?php if($sort=='rating_ASC'){?>selected="selected"<?php }?>>訂購數量 (Lowest)</option>
                </select>
            </div>
          <!--div class="col-md-6">
            <form action="#">
              <div class="input-group">
                <input type="text" name="Search" id="Search1" placeholder="Search again" class="form-control">
                <span class="input-group-btn">
                  <button class="btn btn-primary" type="submit">Search</button>
                </span>
              </div>
            </form>
          </div-->
        </div>
      </div>
      <!--div class="row list-view-sorting clearfix">
        <div class="col-md-2 col-sm-2 list-view">
          <a href="#"><i class="fa fa-th-large"></i>x</a>
          <a href="#"><i class="fa fa-th-list"></i>y</a>
        </div>
        <div class="col-md-10 col-sm-10">
        </div>
      </div-->
    </form>
      <input type="hidden" id="Search2" value="<?php echo $search;?>" name="Search">
      <!-- BEGIN PRODUCT LIST -->
      <div id="shop_catepg" class="product-list shop_ctnew clearfix"><!--row -->
        <?php if(@$products){ foreach($products as $Item){ //var_dump($products);?>
        <!-- PRODUCT ITEM-2 START -->
        <div class="shop-item_outer col-md-2 col-sm-4 col-xs-6 filterpro" money="<?=$Item['price1']?>" data-tag="<?=$Item['attribute_values']?>">
          <div class="product-item">
              <div class="pi-img-wrapper">
                <a href="shop/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $Item['default_root_category_sn'];?>"><img onerror="this.src='public/uploads/notyet.gif'" style="/*height:250px;width:250px;*/" src="public/uploads/<?php echo $Item['image_path'];?>" class="img-responsive" alt="<?php echo $Item['image_alt'];?>"></a>
              </div>
            <div class="product-item-btn-box">
              <h3 class="item-name"><?php echo $Item['product_name'];?></h3>
          <div class="note note-success note-bordered heart<?php echo $Item['product_sn'];?>" style="display:none;"></div>
              <div class="pi-textsp" style="height: 51px !important;overflow: hidden;">
                          <a href="shop/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $Item['default_root_category_sn'];?>"><?=$this->ijw->truncate($Item['special_item'],45)?></a>
             </div>
              <a class="product_icon" title="加入追蹤" href="javascript:void(0);"><i product_sn="<?php echo $Item['product_sn'];?>" class="fa fa-heart"></i></a>
              <div class="pi-price"><span>$</span><?php echo number_format($Item['price1']);?>&nbsp;<font style="font-size:12px;color:#b52b28;"><?php echo $Item['price2'];?></font>
<?php if($this->session->userdata('web_member_data')['bonus']){?>
                  <div class="share">
                    分潤％數：<?=($Item['share_percent']*$this->session->userdata('web_member_data')['bonus']/100)*100;?>％
                  </div>
<?php }?>
              </div>
             <div class="prd_btn_box">
              <?php if(($Item['unispec_flag'] && $Item['unispec_qty_in_stock'])||($Item['unispec_flag']=='0' && count($Item['mutispec'])>=1) || $Item['open_preorder_flag']=='1'){?>
              <a class="btn btn-default prd_btn_lft shopadd" product_sn="<?php echo $Item['product_sn'];?>" category_sn="<?php echo $Item['default_root_category_sn'];?>">立即結帳</a>
              <a class="btn btn-default prd_btn_rgt addcart" product_sn="<?php echo $Item['product_sn'];?>" category_sn="<?php echo $Item['default_root_category_sn'];?>">放入購物車</a>
              <?php }else{?>
                        <a class="btn btn-default" style="cursor:default;" href="javascript:void(0);">補貨中</a>
              <?php }?>
             </div>
            </div>
<?php if($promotion_label_name=@$Item['promotion'][0]['promotion_label_name']){?>
            <p class="h_item2"><?=$promotion_label_name;?></p>
<?php }?>
          </div>
        </div>
  <!-- PRODUCT ITEM-2 END -->
        <?php }}?>
      </div>
      <!-- END PRODUCT LIST -->
      <!-- BEGIN PAGINATOR -->
      <div class="row">
        <div class="col-md-4 col-sm-4 items-info">筆數：<?php echo $total;?></div>
        <div class="col-md-8 col-sm-8">
          <ul class="pagination pull-right">
									<?php echo $page_links;?>
          </ul>
        </div>
      </div>
      <!-- END PAGINATOR -->
    </div>
    <!-- END CONTENT -->
  </div>
  <!-- END SALE PRODUCT & NEW ARRIVALS -->


  <?php for($i=0; $i<12; $i++):?>
    <!-- BEGIN fast view of a product -->
    <div id="product-pop-up-<?php echo $i;?>" style="display: none; width: 700px;">
      <div class="product-page product-pop-up">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-3">
            <div class="product-main-image">
              <img src="public/img/cover_sample.png" alt="Cool green dress with red bell" class="img-responsive">
            </div>
            <div class="product-other-images">
              <a href="#" class="active"><img alt="Berry Lace Dress" src="public/img/cover_sample.png"></a>
              <a href="#"><img alt="Berry Lace Dress" src="public/img/cover_sample.png"></a>
              <a href="#"><img alt="Berry Lace Dress" src="public/img/cover_sample.png"></a>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-9">
            <h2>Cool green dress with red bell</h2>
            <div class="price-availability-block clearfix">
              <div class="price">
                <strong><span>$</span>47.00</strong>
                <em>$<span>62.00</span></em>
              </div>
              <div class="availability">
                Availability: <strong>In Stock</strong>
              </div>
            </div>
            <div class="description">
              <p>Lorem ipsum dolor ut sit ame dolore  adipiscing elit, sed nonumy nibh sed euismod laoreet dolore magna aliquarm erat volutpat Nostrud duis molestie at dolore.</p>
            </div>
            <div class="product-page-options">
              <div class="pull-left">
                <label class="control-label">Size:</label>
                <select class="form-control input-sm">
                  <option>L</option>
                  <option>M</option>
                  <option>XL</option>
                </select>
              </div>
              <div class="pull-left">
                <label class="control-label">Color:</label>
                <select class="form-control input-sm">
                  <option>Red</option>
                  <option>Blue</option>
                  <option>Black</option>
                </select>
              </div>
            </div>
            <div class="product-page-cart">
              <div class="product-quantity">
                  <input id="product-quantity" type="text" value="1" readonly name="product-quantity" class="form-control input-sm">
              </div>
              <button class="btn btn-primary" type="submit">Add to cart</button>
              <a href="preview_jerry/shopItem" class="btn btn-default">More details</a>
            </div>
          </div>

          <div class="sticker sticker-sale"></div>
        </div>
      </div>
    </div>
    <!-- END fast view of a product -->
  <?php endfor;?>
  <!-- END SALE PRODUCT & NEW ARRIVALS -->
  <!-- END CONTENT -->
</div>
</div>

<!-- END SIDEBAR & CONTENT -->