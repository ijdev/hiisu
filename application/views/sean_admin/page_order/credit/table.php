<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>電子禮金管理</h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="preview">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        訂單管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        電子禮金管理
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
        電子禮金列表
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">電子禮金列表</span>
            </div>
            <div class="actions btn-set">
            </div>
          </div>
          <div class="note note-warning">
            <div class="input-group input-large date-picker input-daterange  pull-left" data-date="2015-05" data-date-format="yyyy-mm">
              <span class="input-group-addon" onclick="alert('Start Search');"> 期間 </span>
              <input type="text" class="form-control input-sm" name="from" value="2015-05">
              <span class="input-group-addon"> 到 </span>
              <input type="text" class="form-control input-sm" name="to" value="2015-06">
            </div>
            <div class="table-actions-wrapper pull-right">
              <span>
              </span>
              <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="12" style="margin: 0 5px;" placeholder="付款人姓名">
              <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="12" style="margin: 0 5px;" placeholder="受款人姓名">
              <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button>
            </div>
            <div style="clear:both;"></div>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>系統編號</th>
                  <th>授權交易編號</th>
                  <th>付款人姓名</th>
                  <th>付款人電話</th>
                  <th>受款人姓名</th>
                  <th>金額</th>
                  <th>付款時間</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=0;$i<10;$i++):?>
                  <tr class="odd gradeX">
                    <td><?php echo $i+1;?></td>
                    <td>6554652</td>
                    <td>KRTHHRTYH</td>
                    <td>0922222222</td>
                    <td><a>DRFC</a></td>
                    <td>3,200</td>
                    <td><small>2015-06-10 05:00:00</small></td>
                    <td><a href="javascript:void(0);"><i class="fa fa-trash-o"></i>退費</a></td>
                  </tr>
                <?php endfor;?>
              </tbody>
            </table>

            <!-- page nav -->
            <div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                  <ul class="pagination" style="visibility: visible;">
                    <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                    <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li class="active"><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                    <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- end of page nav -->
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->