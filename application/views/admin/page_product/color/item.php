<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link href="public/metronic/global/plugins/jquery-minicolors/jquery.minicolors.css" rel="stylesheet" type="text/css"/>


<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>版型顏色 <small>編輯版型顏色</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/admin">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品規格
        <i class="fa fa-circle"></i>
      </li>
      <li>
         <a href="admin/product/productColor">版型顏色</a>
         <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         版型顏色編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">編輯版型顏色</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/product/productColor" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post">
              <div class="form-group">
                <label for="firstname" class="col-lg-3 control-label">名稱 <span class="require">*</span></label>
                <div class="col-lg-8">
                  <input type="text" class="form-control" name="Item[color_name]" value="<?php echo @$Item['color_name']; ?>" required>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-lg-3 control-label">色碼 <span class="require">*</span></label>
                <div class="col-lg-8">
                  <input type="text" id="hue-demo" class="form-control demo" data-control="hue" name="Item[color_code]" value="<?php echo @$Item['color_code']; ?>" required>
                </div>
              </div>
              <div class="form-group">
                <label for="password" class="col-lg-3 control-label">備註</label>
                <div class="col-lg-8">
                  <textarea class="form-control" rows="3" name="Item[description]"><?php echo @$Item['description']; ?></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-lg-3 control-label">排序</label>
                <div class="col-lg-8">
                  <input type="text" class="form-control" name="Item[sort_order]" value="<?php echo (@$Item['sort_order']) ? @$Item['sort_order']:'9999'; ?>" required>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-lg-3 control-label">是否啟用</label>
                <div class="col-lg-8">
                  <select class="form-control" name="Item[status]">
                      <option value="1" <?php if(@$Item['status']){ ?>selected<?php }?>>是</option>
                      <option value="0" <?php if(@$Item['status']==='0'){ ?>selected<?php }?>>否</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-lg-3 control-label">更新者</label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['update_member_sn'])?@$Item['update_member_sn']:@$Item['create_member_sn']; ?>" Disabled>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-lg-3 control-label">更新時間</label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['last_time_update']!='0000-00-00 00:00:00')?@$Item['last_time_update']:@$Item['create_date']; ?>" Disabled>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <input type="hidden" id="website_color_sn" name="website_color_sn" value="<?php echo @$Item['website_color_sn']; ?>">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="reset" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->