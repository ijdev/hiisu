<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/typeahead/typeahead.css">
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/pages/css/portfolio.css" rel="stylesheet" type="text/css"/>


<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>行銷管理 <small>Mail內容管理</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="preview">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        系統設定
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="preview/mail">Mail內容管理</a>
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         Mail內容編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">Mail內容編輯</span>
            </div>
            <div class="tools">
              <a href="preview/mail"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
            <div class="note note-danger col-lg-10 pull-right">
              <h4 class="block">變數內容定義由工程師填寫</h4>
              <p>
                說明:在郵件模版中,設定了郵件內容變數(形如@XXXX@),在發出郵件時,系統會用實際值替換本變數,本範本的變數定義如下:  <br>
                @username@ 帳號<br>
                @passwd@ 密碼<br>
                @truename@ 會員姓名<br>
              </p>
            </div>
            <div style="clear:both;"></div>
            <form class="form-horizontal" role="form">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">Mail類型</label>
                  <div class="col-lg-10">
                    <p class="form-control-static">註冊確認信</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">Mail標題</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="寄送出時 User 看到的標題，可帶變數" value="IJWedding 會員註冊確認函">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">最後修改日期</label>
                  <div class="col-lg-10">
                    <p class="form-control-static">2015-06-10 12:00</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">最後發送日期</label>
                  <div class="col-lg-10">
                    <p class="form-control-static">0000-00-00 00:00</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-2 control-label">內容</label>
                  <div class="col-lg-10">
                    <textarea class="ckeditor form-control" name="editor1" rows="12">親愛的會員您好 :<br>
 <br>
感謝您對 HiiSU 的支持及愛護，您方才已於 HiiSU 申請帳號成功，歡迎您加入 HiiSU 會員。<br>
<br>
以下是您在 HiiSU 註冊的會員帳號及密碼。<br>
會員帳號：@mail@<br>
密    碼：@passwd@<br>
 <br>
您可以使用上述帳號及密碼登入系統，暢遊 HiiSU 網站。<br>
提醒您妥善保存您的帳號密碼或保留此函，以便日後查詢。<br>
 <br>
若您有任何疑問，歡迎洽詢 HiiSU<br>
客服中心 www.HiiSU.shop<br>
 <br>
<br>
※此信件由系統自動發送至會員登錄的電子信箱，請勿直接回覆，感謝您的配合。</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-lg-2 control-label">更新者</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control"  placeholder="administrator" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-2 control-label">更新時間</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control"  placeholder="2015-05-25 10:00" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-2 col-md-10">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="button" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->