<?php 
/*
* 版型客製化項目參數，取消，直接使用 API 存取相關資料
*/
if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
*	版型與 product_gallery_sn 對應表
* 	1 : forever - coral-red
*	2 : luv - pink
*	3 : luv - purple
*	4 : luv - brown
*	5 : luv - green
*/

// setting for forever
$forever = array();

/*
*	setting for luv
*/
$luv = array();
$luv[2]['map_bg_color'] = 'd56363';
$luv[3]['map_bg_color'] = 'b06c8b';
$luv[4]['map_bg_color'] = 'ad8763';
$luv[5]['map_bg_color'] = '00c0b6';


// setting for yes
$yes = array();

$config['template_setting'] = array(
	'forever' => $forever,
	'luv' => $luv,
	'yes' => $yes
);