<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shipping extends My_general_ct {

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('sean_models/Sean_db_tools');
		/* Begin 修改  */
		$this->_ct_name="配送與運費設定";// 功能名稱
		$this->_url="supplier/Shipping/";//列表功能url
		$this->_url_item="supplier/Shipping_item/";//處理動作 url
		$this->_table="ij_delivery_method_ct";//table name 2015-12-31 wdj old:ij_shipping
		/*$this->_id="shipping_sn";//table key
		$this->_table_field=array(
					 "shipping_sn" => "運費設定代碼",
					 "shipping_location" => "運送地區",
					 "shipping_price" => "費用",
					 "description" => "敘述",
					 "sort_order" => "排序",
					 "status" => "是否啟用",
					 "create_date" => "建立時間",
					 "create_member_sn" => "建立者",
					 "update_member_sn" => "更新者",
					 "last_time_update" => "更新時間",
				);*/
		$this->_id="delivery_method";//table key
		$this->_table_field=array(
					 "delivery_method" => "代碼",
					 "delivery_method_name" => "運送方式",
					 "delivery_amount" => "本島",
					 "outland_amount" => "外島",
					 "description" => "敘述",
					 "sort_order" => "排序",
					 "default_flag" => "預設",
					 "display_flag" => "啟用",
					 "supplier_sn" => "供應商",
				);
		/* End 修改  */

    }

    //取得列表內容
    public function table_value_list( $_where,$_id = 0)
	{
		$_table_key=$this->_id;
	    $page_data['_body_result']=array();
		//判斷是否有帶入特殊查詢
		if(strlen($_where)==0)
		{
		    if($this->session->userdata('member_role_system_rules')[0]['role_name']!='供應商管理員'){
				$_where=" left join ij_supplier on ij_supplier.supplier_sn=ij_delivery_method_ct.supplier_sn order by sort_order asc";
			}else{
				$_supplier_sn=$this->session->userdata['member_data']['associated_supplier_sn'];
				$_where=" left join ij_supplier on ij_supplier.supplier_sn=ij_delivery_method_ct.supplier_sn where ij_delivery_method_ct.supplier_sn=$_supplier_sn order by sort_order asc";
			}

		}
		// 取得列表
		$_result01=$this->Sean_db_tools->db_get_max_record("*",$this->_table,$_where);

		/* 列表標頭名稱 */
		$page_data['_table_thead_name']=$this->_table_field;


		/* 類表內容 */
		if(is_array($_result01))
		{
                 		$_i=0;
                 		foreach($_result01 as $key => $value)
                    	{
                    		if($value->default_flag == 1)
                    		{
                    			$_default_status = '<i class="fa fa-check"></i>';
                    		}else{
                    			$_default_status = '<i class="fa fa-times"></i>';
                    		}
                    		if($value->display_flag == 1)
                    		{
                    			$_display_status = '<i class="fa fa-check"></i>';
                    		}else{
                    			$_display_status = '<i class="fa fa-times"></i>';
                    		}

			                $_body_result='';
						/* Begin 修改  */

	                        $page_data['_body_result'][$_i][0]=array("_value"=>$value->delivery_method,"_class"=>"");
			                $page_data['_body_result'][$_i][1]=array("_value"=>$value->delivery_method_name,"_class"=>"");
							$page_data['_body_result'][$_i][2]=array("_value"=>$value->delivery_amount,"_class"=>"");
							$page_data['_body_result'][$_i][3]=array("_value"=>$value->outland_amount,"_class"=>"");
							$page_data['_body_result'][$_i][4]=array("_value"=>$value->description,"_class"=>"");
							$page_data['_body_result'][$_i][5]=array("_value"=>$value->sort_order,"_class"=>"");
							$page_data['_body_result'][$_i][6]=array("_value"=>$_default_status,"_class"=>"");
							$page_data['_body_result'][$_i][7]=array("_value"=>$_display_status,"_class"=>"");
    if($this->session->userdata('member_role_system_rules')[0]['role_name']!='供應商管理員'){
		$page_data['_body_result'][$_i][8]=array("_value"=>$value->supplier_name,"_class"=>"");
    }
						  //$page_data['_body_result'][$_i][6]=array("_value"=>$value->create_date,"_class"=>"");
						  //$page_data['_body_result'][$_i][7]=array("_value"=>$value->create_member_sn,"_class"=>"");
						  //$page_data['_body_result'][$_i][8]=array("_value"=>$value->update_member_sn,"_class"=>"");
						  //$page_data['_body_result'][$_i][9]=array("_value"=>$value->last_time_update,"_class"=>"");

						/* End 修改  */
							$page_data['_body_result'][$_i][10]=array("_value"=>'
			                    		 		<a href="'.$this->init_control.$this->_url_item.'edit/'.$value->$_table_key.'"><i class="fa fa-edit"></i>編輯</a>&nbsp;&nbsp;
						                      <a href="'.$this->init_control.$this->_url_item.'delete/'.$value->$_table_key.'" onclick="return deletechecked();"><i class="fa fa-trash-o"></i>刪除</a>&nbsp;&nbsp;
						                  	',"_class"=>'class="center"');
                				$_i++;

                			}

    				}
			return $page_data;

    }

	public function action_insert() {
		$_result="";
		foreach($this->_table_field as $key => $value)
		{
			if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
			$_data[$key]=$this->input->get_post($key);
		}
		//var_dump($_data);exit;
		$_result=$this->Sean_db_tools->db_insert_record($_data,$this->_table);

		return $_result;

	}

}
