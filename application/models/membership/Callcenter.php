<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Callcenter extends CI_Model {

    public function __construct() {
        parent::__construct();
       $this->load->database();
        }

    /**
     *
     * create_member
     *
     * @param string $username
     * @param string $password
     * @param string $email
     * @param string $first_name
     * @param string $last_name
     * @return mixed
     *
     */

    public function create_callcenter( $data ) {
//var_dump($data);

       // email  password first_name  last_name  time_register

        //$this->db->set('issue_date', 'NOW()', FALSE);
        $this->db->insert('ij_member_question', $data);
        
        if ($this->db->affected_rows() == 1) {
            return true;
        }
        return false;
    }


}

/* End of file register_model.php */
/* Location: ./application/models/membership/register_model.php */