<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
* Home controller
* Create by Sean
* Modify by Sean, Jerry
* ============================
* Function Description (in use, still updateing....)
* index : official home page
* partners : 官網合作夥伴
* blogList : 婚禮情報文章列表
* blogItem : 婚禮情報文章文章單文頁
* faq : 首頁常見問題頁
* ewedding : 婚禮網站所有頁面
* templatePreview : 光箱瀏覽 detail
* checkIn : 婚宴管理 APP 頁面
*/

class Home extends MY_Controller {

	 public function __construct()
	{
		 parent::__construct();
		//$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('form_validation');
		//$this->load->library('sean_lib/Sean_form_general');
		$this->load->model('sean_models/Sean_general');
		$this->load->model('sean_models/Sean_db_tools');
		$this->load->model('Shop_model');
		$this->load->model('libraries_model');
		$this->page_data = array();
		$this->page_data['cart']=$this->Shop_model->Get_Cart(); //購物車
		$this->page_data['unreadmessage']= $this->Shop_model->get_unread_message_count();
		if(!$this->session->userdata('category_array')){
			$_category_array=$this->Shop_model->categoryParentChildTree('囍市集',0,'','',''); //分類
			$this->page_data['_category_array']=$this->Shop_model->get_banner($_category_array); //分類抓圖
			$this->session->set_userdata('category_array',$this->page_data['_category_array']);
		}else{
			$this->page_data['_category_array']=$this->session->userdata('category_array');
		}
		if(!$this->session->userdata('general_header')){
			$this->page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
			$where=array('upper_category_sn'=>'0','category_status'=>'1','channel_name'=>'囍市集');
			$this->page_data['general_header']['root_category']=$this->Shop_model->_select('ij_category',$where,0,0,10,0,0,'result_array');
			$this->page_data['general_header']['content_types']=$this->libraries_model->_get_blog_faq_categorys(1,'囍市集');
			$this->page_data['general_header']['left_up']=$this->Shop_model->get_adsMaterials('首頁-跑馬燈文字');
			//$this->page_data['general_header']['up_left_banner']=$this->Shop_model->get_adsMaterials('首頁-左小方格');
			$this->page_data['general_header']['up_left_banner']=array();
			$this->page_data['general_header']['up_right_banner']=$this->Shop_model->get_adsMaterials('首頁-右小方格-右側');
			$this->page_data['general_header']['up_banner']=$this->Shop_model->get_adsMaterials('首頁_寬版長方格_H1_右側');
			$this->page_data['general_header']['hot_search']=$this->Shop_model->get_adsMaterials('首頁-熱門關鍵字',8)['banners'];
			$this->page_data['general_header']['top_right_word']=$this->Shop_model->get_adsMaterials('首頁_文字_右上',1)['banners'];
			$this->session->set_userdata('general_header',$this->page_data['general_header']);
		}else{
			$this->page_data['general_header']=$this->session->userdata('general_header');
		}
		/*$this->page_data['init_control']="www/";
		$this->page_data['_home_url']="www/";
		$this->init_control=$this->page_data['init_control'];
		$this->page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$this->page_data['page_content']['view_path'] = 'admin-ijwedding/page_home/page_content'; // 頁面主內容 view 位置, 必要
		$this->page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$this->page_data['page_level_js']['view_path'] = 'admin-ijwedding/page_home/page_content_js'; // 頁面主內容的 JS view 位置, 必要
		//$this->lang->load('general', 'zh-TW');
		$this->page_data["_per_page"]=2;*/
		$data['Mes'] = $this->session->userdata('Mes');
		if($data['Mes']){$this->load->view('Message',$data);$this->session->unset_userdata('Mes');}
	}



	/**
	 * 官網首頁
	 *
	 */

	public function index()
	{
		redirect(base_url('shop'));
		$page_data = array();

		// 內容設定

		// page level setting
		$page_data['slider'] = 'www/page_home/slider'; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，非必要
		$page_data['sliders']['sliders']=$this->Shop_model->get_adsMaterials('首頁_輪播大圖_H3全版');


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_home/page_content'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_home/page_content_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 * 官網合作夥伴
	 *
	 */

	public function partners()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		$page_data['slider'] = NULL; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，非必要


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_home/page_partners'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_home/page_partners_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}
	/*public function newslist()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/notification_list'; // 頁面主內容 view 位置, 必要
		$page_data['page_content']['Item']=$this->Shop_model->_select('ij_system_file_config','system_file_name','使用條款',0,0,0,0,'row');
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/notification_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}*/
	public function newslist($type='',$member_message_center_sn=0)
	{


		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		switch($type){
			case "detail":

					$data_array['read_flag'] = '1';
					$data_array = $this->Shop_model->CheckUpdate($data_array,0);
				  $this->Shop_model->_update('ij_member_message_center_log',$data_array,'member_message_center_sn',$member_message_center_sn);

				  $this->page_data['unreadmessage']= $this->Shop_model->get_unread_message_count();

					$page_data['general_header'] = array(); // header 可以透過這個參數帶入
					$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
					$page_data['page_content']['view_path'] = 'www/news_item'; // 頁面主內容 view 位置, 必要
					$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
					$page_data['page_level_js']['view_path'] = 'www/page_member/notification_item_js'; // 頁面主內容的 JS view 位置, 必要
					$this->load->view('www/general/main', $page_data);

			break;
			default:
				// general view setting
				$page=($this->input->get('page',true))? $this->input->get('page',true):'1';
				$this->load->library('pagination');
				$this->load->library('ijw');
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['total'] = count($this->Shop_model->_select('ij_member_message_center_log',array('default_display_time <='=>date('Y-m-d H:i:s'),'message_status'=>'1','ij_member_message_center_log.member_sn'=>$this->session->userdata['web_member_data']['member_sn']),0,0,0,'ij_member_message_center_log.last_time_update','desc','result'));
				$page_data['page_content']['perpage'] = 10;
				if($page_data['page_content']['total']==0) $page=0;
				$page_data['page_content']['start'] = ($page > 1)? ($page-1)*$page_data['page_content']['perpage']:'0';
				$page_data['page_content']['end'] = ($page)? $page*$page_data['page_content']['perpage']:'0';
				$config = $this->ijw->_pagination('home/newslist', $page_data['page_content']['total'],$page_data['page_content']['perpage']);
				$this->pagination->initialize($config);
				//var_dump($page_data['page_content']['perpage']);
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content']['page_links'] = $this->pagination->create_links();
				$page_data['page_content']['view_path'] = 'www/notification_list'; // 頁面主內容 view 位置, 必要
				$page_data['page_content']['messages'] = $this->Shop_model->_select('ij_member_message_center_log',array('default_display_time <='=>date('Y-m-d H:i:s'),'message_status'=>'1','ij_member_message_center_log.member_sn'=>$this->session->userdata['web_member_data']['member_sn']),0,$page_data['page_content']['start'],$page_data['page_content']['perpage'],'ij_member_message_center_log.last_time_update','desc','result_array');
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'www/notification_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('www/general/main', $page_data);
			break;
		}
	}
	public function aboutus()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		$page_data['slider'] = NULL; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，非必要


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['about']=$this->Shop_model->_select('ij_system_file_config','system_file_name','關於我們',0,0,0,0,'row')['system_file_ontent'];
		$page_data['page_content']['view_path'] = 'www/page_home/page_aboutus'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_home/page_partners_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	public function P404()
	{
        $this->output->set_status_header('404');
		$page_data = array();

		// 內容設定

		// page level setting
		$page_data['slider'] = NULL; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，非必要


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['about']=$this->Shop_model->_select('ij_system_file_config','system_file_name','404',0,0,0,0,'row')['system_file_ontent'];
		//排序前10有圖片分類
		$this->load->library("ijw");
		$where = array('category_status' => '1','main_picture_save_dir <>' => '','publish_flag'=>'1');
		$promo_category=$this->Shop_model->_select('ij_category',$where,0,0,0,0,0,'result_array');
		foreach ($promo_category as $_key => $_value) {
			$promo_category[$_key]['category_attributes']=$this->Shop_model->category_attributes($_value['category_sn'],0,1);
			//var_dump($promo_category[$_key]['category_attributes']);
		}
		$page_data['page_content']['promo_category']=$promo_category;
		$page_data['page_content']['view_path'] = 'www/page_home/page_404'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_home/page_404_js'; // 頁面主內容的 JS view 位置, 必要
		echo $this->load->view('www/general/main', $page_data,true);
	}


	public function contactus()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		$page_data['slider'] = NULL; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，非必要


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_home/page_contactus'; // 頁面主內容 view 位置, 必要
		$page_data['page_content']['ccapply_detail_code'] = $this->Shop_model->_select('ij_ccapply_detail_code_ct',array('display_flag'=>'1','ccapply_code'=>5),0,0,0,'ccapply_detail_code',0,'result_array','ccapply_detail_code,ccapply_detail_code_name'); // town

    	$page_data['_captcha_img']=$this->captcha_img();
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_home/page_partners_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}


	public function forget_password()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		$page_data['slider'] = NULL; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，非必要


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_home/page_forget_password'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_home/page_partners_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}


	/**
	 * 婚禮情報文章列表 by分類
	 *
	 */
	public function blogList($content_type='all')
	{
		$page_data = array();
		$content_type=urldecode($content_type);
		$search=$this->input->get('search',true);
		// 內容設定
		if(!$search){
			$search_array='0';
		}else{
			$search_array=array('blog_article_titile'=>$search,'keyword'=>$search,'blog_article_content'=>$search);
		}
		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		$page=($this->input->get('page',true))? $this->input->get('page',true):'1';
		$this->load->library('pagination');
		$this->load->library('ijw');
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$where2=array('status'=>'1','post_flag'=>'1');
		if($content_type!='all'){
			$like_field='';
			//$content_type_sn=$content_type;
			$content_types=@$this->Shop_model->_select('ij_content_type_config','content_title',$content_type,0,0,0,0,'row');
			$where2['ij_blog_type_relation.content_type_sn']=$content_types['content_type_sn'];
			$where2['ij_blog_type_relation.relation_status']='1';
			$content_type_sn=$content_types['content_type_sn'];
			$title=$content_types['content_title'];
		}else{
			$like_field='';
			$content_type_sn='';
			$title='';
		}
		//var_dump($search_array);
		$this->load->model('libraries_model');
		$page_data['page_content']['total'] = count($this->libraries_model->_get_blog_faq('ij_blog_article',$where2,$like_field,$content_type_sn,$search_array));
		$page_data['page_content']['perpage'] = 5;
		if($page_data['page_content']['total']==0) $page=0;
		$page_data['page_content']['start'] = ($page > 1)? ($page-1)*$page_data['page_content']['perpage']:'0';
		$page_data['page_content']['end'] = ($page)? $page*$page_data['page_content']['perpage']:'0';
		$config = $this->ijw->_pagination('home/blogList/'.$content_type.'/', $page_data['page_content']['total'],$page_data['page_content']['perpage']);
		$this->pagination->initialize($config);

		$page_data['page_content']['Items']=$this->libraries_model->_get_blog_faq('ij_blog_article',$where2,$like_field,$content_type_sn,$search_array,$page_data['page_content']['start'],$page_data['page_content']['perpage']);
    	$page_data['page_content']['page_links'] = $this->pagination->create_links();
		//$page_data['page_content']['content_types']=$this->libraries_model->_get_blog_faq_categorys(1,'囍市集');
		//var_dump($page_data['page_content']['content_types']);
		$page_data['page_content']['content_type']=urldecode($content_type);
		$page_data['page_content']['search']=$search;
		if($search && $title) $search.='-';
		$page_data['title']=$search.$title.' | 囍市集 | 婚禮情報 | 婚禮購物商城';
		$page_data['description']=@$content_types['content_description'].' | 囍市集 | 婚禮情報 | 婚禮購物商城';
		$page_data['page_content']['keyword_tag']='';
		$page_data['page_content']['view_path'] = 'www/page_blog/list'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_blog/list_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 * 婚禮情報文章列表 by keyword
	 *
	 */
	public function blogKeyword($keyword_tag)
	{
		if(!$keyword_tag) redirect(base_url('home/blogList'));
		$keyword_tag=urldecode($keyword_tag);
		$page_data = array();
		$search=$this->input->get('search',true);
		if(!$search){
			$search_array='0';
		}else{
			$search_array=array('blog_article_titile'=>$search,'keyword'=>$search,'blog_article_content'=>$search);
		}
		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		$page=($this->input->get('page',true))? $this->input->get('page',true):'1';
		$this->load->library('pagination');
		$this->load->library('ijw');
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		if($keyword_tag!='all'){
			$like_field='keyword';
			$content_type_sn=$keyword_tag;
		}else{
			$like_field='';
			$content_type_sn='';
		}
		$this->load->model('libraries_model');
		$page_data['page_content']['total'] = $this->libraries_model->_get_blog_faq('ij_blog_article',array('status'=>'1','post_flag'=>'1'),$like_field,$content_type_sn,$search_array,0,0,1);
		$page_data['page_content']['perpage'] = 5;
		if($page_data['page_content']['total']==0) $page=0;
		$page_data['page_content']['start'] = ($page > 1)? ($page-1)*$page_data['page_content']['perpage']:'0';
		$page_data['page_content']['end'] = ($page)? $page*$page_data['page_content']['perpage']:'0';
		$config = $this->ijw->_pagination('home/blogKeyword/'.$keyword_tag.'/', $page_data['page_content']['total'],$page_data['page_content']['perpage']);
		$this->pagination->initialize($config);

		$page_data['page_content']['Items']=$this->libraries_model->_get_blog_faq('ij_blog_article',array('status'=>'1','post_flag'=>'1'),$like_field,$content_type_sn,$search_array,$page_data['page_content']['start'],$page_data['page_content']['perpage']);
        $page_data['page_content']['page_links'] = $this->pagination->create_links();
		//$page_data['page_content']['content_types']=$this->libraries_model->_get_blog_faq_categorys(1,'囍市集');
		$page_data['page_content']['content_type']='';
		$page_data['page_content']['keyword_tag']=$keyword_tag;
		$page_data['page_content']['search']=$search;
		if($search) $search.='-';
		$page_data['title']=$search.$keyword_tag.' | 囍市集 | 婚禮情報';;
		$page_data['description']=$keyword_tag.' | 囍市集 | 婚禮情報';
		$page_data['page_content']['view_path'] = 'www/page_blog/list'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_blog/list_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}
	/**
	 * 婚禮情報文章頁面
	 *
	 */
	public function blogItem($blog_article_sn=0)
	{
		if(!$blog_article_sn) redirect(base_url('home/blogList'),'auto','301');

		$page_data = array();
		$search=$this->input->get('search',true);
		if(!$search){
			$search_array='0';
		}else{
			$search_array=array('blog_article_titile'=>$search,'keyword'=>$search,'blog_article_content'=>$search);
		}
		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		$this->load->model('libraries_model');
		$page_data['page_content']['Item']=@$this->libraries_model->_get_blog_faq('ij_blog_article',array('status'=>'1','post_flag'=>'1','ij_blog_article.blog_article_sn'=>$blog_article_sn))[0];
		//var_dump($page_data['page_content']['Item']);
		if(!$page_data['page_content']['Item']){
 			redirect(base_url('home/blogList'),'auto','301');
		}
		//for sidebar
		//$page_data['page_content']['content_types']=$this->libraries_model->_get_blog_faq_categorys(1,'囍市集');
		//$like_field='associated_content_type_sn';
		//$content_type_sn=$page_data['page_content']['Item']['associated_content_type_sn'];
		//$page_data['page_content']['Items']=$this->libraries_model->_get_blog_faq('ij_blog_article',array('status'=>'1','post_flag'=>'1'));
		$page_data['page_content']['Items'][0]['keywords']=explode(chr(13).chr(10),trim($page_data['page_content']['Item']['keyword']));
		$page_data['page_content']['content_type']=$page_data['page_content']['Item']['associated_content_type_sn'];
		$page_data['page_content']['keyword_tag']='';
		$page_data['page_content']['search']=$search;
		$page_data['title']=$page_data['page_content']['Item']['blog_article_titile'].' | 囍市集 | 婚禮情報';
		$page_data['description']=str_replace(chr(13).chr(10),'、',$page_data['page_content']['Item']['keyword']).' | 囍市集 | 婚禮情報';

		$page_data['page_content']['view_path'] = 'www/page_blog/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_blog/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 * FAQ列表頁面
	 *
	 */
	public function faq($channel='囍市集',$content_type='all',$faq_titile='')
	{
		$page_data = array();
		$channel=urldecode($channel);
		$content_type=urldecode($content_type);
		$faq_titile=urldecode($faq_titile);
		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		$page=($this->input->get('page',true))? $this->input->get('page',true):'1';
		$this->load->library('pagination');
		$this->load->library('ijw');

		if($content_type!='all'){
			$like_field='associated_content_type_sn_set';
			$content_type_sn=$content_type;
		}else{
			$like_field='';
			$content_type_sn='';
		}
		$this->load->model('libraries_model');
		/*$Items=$this->libraries_model->_get_blog_faq('ij_faq',array('ij_faq.status'=>'1','ij_faq.post_channel_name_set'=>$channel),$like_field,$content_type_sn);
		$page_data['page_content']['total'] = count($Items);
		$page_data['page_content']['perpage'] = 5;
		if($page_data['page_content']['total']==0) $page=0;
		$page_data['page_content']['start'] = ($page > 1)? ($page-1)*$page_data['page_content']['perpage']:'0';
		$page_data['page_content']['end'] = ($page)? $page*$page_data['page_content']['perpage']:'0';
		$config = $this->ijw->_pagination('home/blogList/'.$content_type.'/', $page_data['page_content']['total'],$page_data['page_content']['perpage']);
		$this->pagination->initialize($config);

		$page_data['page_content']['Items']=$this->libraries_model->_get_blog_faq('ij_faq',array('ij_faq.status'=>'1','ij_faq.post_channel_name_set'=>$channel),$like_field,$content_type_sn,0,$page_data['page_content']['start'],$page_data['page_content']['perpage']);
    	$page_data['page_content']['page_links'] = $this->pagination->create_links();*/
		$page_data['page_content']['content_types']=$this->libraries_model->_get_blog_faq_categorys(2,$channel);
		//var_dump($page_data['page_content']['content_types']);
		$page_data['page_content']['content_type']=$content_type;
		$page_data['page_content']['faq_titile']=$faq_titile;
		$page_data['page_content']['keyword_tag']='';

		$page_data['page_content']['view_path'] = 'www/page_blog/faq'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_blog/faq_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 * 婚禮網站所有頁面
	 *
	 */
	public function ewedding($type='home', $segment = '')
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設

		switch($type){
			case "faq":
			//var_dump($segment);
				$this->faq('婚禮網站');
			break;
			case "template":
				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'www/page_ewedding/list'; // 頁面主內容 view 位置, 必要

				//抓開放商品-版型資料
				$this->load->model('Shop_model');
		    $this->load->library('pagination');
				$page_data['page_content']['perpage'] = 2;
				$page_data['page_content']['total'] = count($this->Shop_model->_List_Product_Templates());
		    $this->load->library("ijw");
		    $config = $this->ijw->_pagination('home/ewedding/template', $page_data['page_content']['total'],$page_data['page_content']['perpage'],0);
		    $this->pagination->initialize($config);

		    $page_data['page_content']['page_links'] = $this->pagination->create_links();
			  $page=$this->input->get('page',true);

				$page_data['page_content']['templates'] =$this->Shop_model->Get_Product_Template($page,$page_data['page_content']['perpage']);
				//var_dump($page_data['page_content']['templates']);
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'www/page_ewedding/list_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('www/general/main', $page_data);
			break;
			//新增免費試用
			//case "add_free_site":
			//	base64_encode('ij_content_type_config')

			//break;
			case "price":
				$page_data['slider'] = NULL; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，非必要

				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'www/page_ewedding/price'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'www/page_ewedding/price_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('www/general/main', $page_data);
			break;
			case "related":
				$page_data['slider'] = 'www/page_ewedding/slider_related'; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，非必要

				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'www/page_ewedding/related'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'www/page_ewedding/related_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('www/general/main', $page_data);
			break;
			default:
			case "home":
				$page_data['slider'] = 'www/page_ewedding/slider'; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，非必要

				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'www/page_ewedding/home'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'www/page_ewedding/home_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('www/general/main', $page_data);
			break;
		}
	}
	/**
	 * 婚禮網站商品頁
	 *
	 */
	public function eweddingItem($product_sn=0)
	{
			//var_dump($this->cart->contents());

		if($product_sn && $product=$this->Shop_model->_get_product($product_sn)){
			//$this->ijw->_write_product_history($product_sn); 瀏覽歷史沒地方秀了
			$this->Shop_model->_update_field_amount('ij_product','product_sn',$product_sn,'click_number','+1');

			//echo count(get_cookie('product_history[]'));
			$page_data = array();

			// 內容設定

			// page level setting
			// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設

			$page_data['_key_id']=$product['default_root_category_sn']; //商品主分類

			$_where=" where channel_name='".$product['default_channel_name']."' ";
			$_category_array=$this->Sean_db_tools->db_get_max_record("*","ij_category",$_where);
		  $page_data['_category_array']=$_category_array;

		  $_where=" where upper_category_sn='".$page_data['_key_id']."' or category_sn='".$page_data['_key_id']."' ";
		  $_category_array=$this->Sean_db_tools->db_get_max_record("*","ij_category",$_where);
		  $page_data['_category_array_left']=$_category_array;
		  $page_data['meta_title']=$product['meta_title'];
		  $page_data['meta_keywords']=$product['meta_keywords'];
		  $page_data['meta_content']=$product['meta_content'];

			// general view setting
			$page_data['general_header'] = array(); // header 可以透過這個參數帶入
			$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
			$page_data['page_content']['view_path'] = 'www/page_ewedding/page_item'; // 頁面主內容 view 位置, 必要

			//熱賣商品
			//$page_data['page_content']['promo_product2']=$this->Shop_model->_get_products($product['default_channel_name'],'2',$page_data['_key_id'],5);
			//特價商品
			//$page_data['page_content']['promo_product3']=$this->Shop_model->_get_products($product['default_channel_name'],'3',$page_data['_key_id'],5);
			//熱門商品
			//$page_data['page_content']['promo_product5']=$this->Shop_model->_get_products($product['default_channel_name'],'5',$page_data['_key_id'],5);
			//相關商品(同分類隨機)
			//$page_data['page_content']['promo_product6']=$this->Shop_model->_get_products($product['default_channel_name'],'6',$page_data['_key_id'],0);
			$page_data['page_content']['promo_product6'] =$this->Shop_model->Get_Product_Template(0,0,$product_sn);
			//商品資料
			$page_data['page_content']['product']=$product;

			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'www/page_ewedding/page_item_js'; // 頁面主內容的 JS view 位置, 必要
			$this->load->view('www/general/main', $page_data);
		}else{
				$this->session->set_userdata(array('Mes'=>'很抱歉!查無此商品，可能下架中，或尚未開賣!'));
			  redirect(base_url('shop'));
		}
	}
	/**
	 * 婚禮網站 Detail 光箱 Template
	 *
	 */
	public function templatePreview($product_sn=0,$website_color_sn=0)
	{
		if($product_sn){
			$page_data = array();

			// 內容設定
			$page_data['product_sn'] = $product_sn;

			// page level setting

			$this->load->model('Shop_model');
			$page_data['template']=$this->Shop_model->Get_Template_images($product_sn,$website_color_sn);

			// general view setting
			$this->load->view('www/page_ewedding/modal_template', $page_data);
		}
	}

	/**
	 * 婚宴管理 APP 頁面
	 *
	 */
	public function checkIn($type='home',$segment='')
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		switch($type){
			case "faq":
				$this->faq('婚宴管理');
			break;
			default:
			case "home":
				$page_data['slider'] = 'www/page_checkin/slider'; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，非必要

				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'www/page_checkin/item'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'www/page_checkin/item_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('www/general/main', $page_data);
			break;
		}
	}


	//==================================
	//==================================
	//==================================
	//==================================
	//==================================
	//==================================
	//==================================
	//==================================
	//==================================
	//==================================




	public function memberSignInAction()
	{

				$this->load->model('membership/Login_model');

        $_message='message';

        $_message_cap="";


       if ($this->input->get_post("rycode")!= $this->session->userdata('captcha'))
			{

					if ($this->input->get_post("rycode") != $this->session->userdata('captcha') && strlen($this->input->get_post("rycode")) > 0) {
                $_message_cap.="<font color=red size=\"3px\">驗證碼不符</font>";
            }
					 $this->session->set_flashdata($_message,$_message_cap);
            $data['post'] = $_POST;
            $this->session->set_flashdata($data['post']);

			//紀錄登入失敗資料
			//$this->Login_model->_member_log(0,$_POST['username'],2);

            redirect('memberSignIn');
            exit();
		}else{

			//會員登入檢查

			$_login_array=$this->Login_model->validate_login($this->input->get_post('username'), $this->input->get_post('password'));
			if($_login_array==false)
			{
				//紀錄登入失敗資料
				//$this->Login_model->_member_log(0,$_POST['username'],2);

				//帳號密碼錯誤
				$this->session->set_flashdata($_message,  "<font size=\"4px\">帳號或密碼錯誤!</font>");
				$this->session->set_flashdata($data['post']);
            	redirect('memberSignIn');
           		exit();

			}else{
				$this->session->set_userdata('web_login',true);
				//$this->session->set_userdata('customer_login',true);
				$this->session->set_userdata('web_member_data',$_login_array);
				redirect('callcenter');
				exit();


			}
		}
	}

	public function memberSignOut()
	{


		$this->session->set_userdata('web_login',false);
		$this->session->set_userdata('web_member_data',false);
		redirect('www/memberSignIn');
    exit();

	}

	/**
	 * 註冊頁面 Template
	 *
	 */
	public function memberSignUp()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_sign_up'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_sign_up_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

/**
	 * 註冊頁面 Template
	 *
	 */
	public function memberSignUp_Action()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設

			//$this->load->model('membership/Login_model');

      $this->_table_field=array(
           "member_sn" => "會員代號",
					 "user_name" => "登入狀態代碼",
					 "email" => "帳號",
					 "last_name" => "姓",
					 "first_name" => "名",
					 "tel" => "市話",
					 "cell" => "手機",
					 "wedding_date" => "結婚日期",
					 "birthday" => "生日",
					 "addr1" => "住址",
					 "memo" => "備註",
					 "password" => "密碼",
					 "member_status" => "帳號狀態",
					 "member_level_type" => "會員等級",
					 "system_user_flag" => "是否為總控人員",
				);

				$this->_table="ij_member";//table name
			  $this->_id="member_sn";//table key

        $_message_cap="";
       if ( $this->input->get_post("confirm-password") != $this->input->get_post("password") )
			{


           $_message_cap.="<font color=red size=\"3px\">密碼和重複輸入密碼不符</font>";
					 $this->session->set_flashdata("fail_message",$_message_cap);
           redirect("memberSignUp");
					 exit();
       }else{



      foreach($this->_table_field as $key => $value)
			{
					if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
					$_data[$key]=$this->input->get_post($key);
			}

			if(strlen($this->input->get_post("password")) > 0 )
			$_data["password"]=$this->input->get_post("password");



			$_data["user_name"]=$_data["email"];
			$_data["member_level_type"]=1;
			$_data["member_status"]=1;
			$_data["member_level"]=1;
			$_data["system_user_flag"]=0;

			//basic insert
			//$_data["create_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			$_data["create_date"]=date("Y-m-d H:i:s");
			//basic update and insert
			//$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			$_data["last_time_update"]=date("Y-m-d H:i:s");



		$_result=$this->Sean_db_tools->db_insert_record($_data,$this->_table);

      if($_result)
      {
     			 $_message_cap.="<font color=red size=\"3px\">註冊成功！</font>";
     			 $this->session->set_flashdata("_message",$_message_cap);
           redirect("memberSignUpOK");
					 exit();
      }



      }
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_sign_up'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_sign_up_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/*
	*
	* 會員修改
	*
	*/

	public function memberModify()
	{

		$this->_table_field=array(
					 "member_sn" => "會員代號",
					 "user_name" => "登入狀態代碼",
					 "email" => "帳號",
					 "last_name" => "姓",
					 "first_name" => "名",
					 "tel" => "市話",
					 "cell" => "手機",
					 "wedding_date" => "結婚日期",
					 "birthday" => "生日",
					 "addr1" => "住址",
					 "memo" => "備註",
					 "password" => "密碼",
					 "member_status" => "帳號狀態",
					 "member_level_type" => "會員等級",
					 "system_user_flag" => "是否為總控人員",
				);

				$this->_table="ij_member";//table name
			  $this->_id="member_sn";//table key

        $_message_cap="";

       if ( $this->input->get_post("confirm-password") != $this->input->get_post("password") )
			{


           $_message_cap.="<font color=red size=\"3px\">密碼和重複輸入密碼不符</font>";
					 $this->session->set_flashdata("fail_message",$_message_cap);
           redirect("memberAccount");
					 exit();
       }else{



      foreach($this->_table_field as $key => $value)
			{
					if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
					$_data[$key]=$this->input->get_post($key);
			}

			if(strlen($this->input->get_post("password")) > 0 )
			$_data["password"]=$this->input->get_post("password");




			//basic insert
			//$_data["create_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			//$_data["create_date"]=date("Y-m-d H:i:s");
			//basic update and insert
			$_data["update_member_sn"]=($this->session->userdata['web_member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			$_data["last_time_update"]=date("Y-m-d H:i:s");

		  $_key_id=$this->session->userdata['web_member_data']['member_sn'];

	  	$_where=" where member_sn='".$this->session->userdata['web_member_data']['member_sn']."'";
      $_result=$this->Sean_db_tools->db_update_record($_data,$this->_table,$_where,$_key_id,$this->_id);
      if($_result)
      {

     			foreach($this->_table_field as $key => $value)
					{

     			 	if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
     			 	$_data[$key]= $this->input->get_post($key);
     			 	else
     			 	$_data[$key]=$this->session->userdata['web_member_data'][$key];

     			}
     			$this->session->set_userdata("web_member_data",$_data);
     			 $_message_cap.="<font color=red size=\"3px\">修改成功！</font>";
     			 $this->session->set_flashdata("message",$_message_cap);
           redirect("memberAccount");
					 exit();
      }else{
      		$_message_cap.="<font color=red size=\"3px\">修改失敗！</font>";
     			 $this->session->set_flashdata("fail_message",$_message_cap);
           redirect("memberAccount");
					 exit();
      }



      }

	}
	/**
	 * 註冊成功 Template，失敗回到原畫面並顯示錯誤訊息
	 *
	 */
	public function memberSignUpOK()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_sign_up_result'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_sign_up_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}


	/**
	 * Wish List Template
	 *
	 */
	public function wishList()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_wish_list'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_wish_list_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}


	/**
	 * 會員查詢購物金 Template
	 *
	 */
	public function credit()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_credit'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_credit_js'; // 頁面主內容的 JS 由這個參數帶入, 必要
		$this->load->view('www/general/main', $page_data);
	}


	/**
	 * VIP 會員申請 Template
	 *
	 */
	public function affiliate($status='form')
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array('status' => $status); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_affiliate'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 * VIP 會員獎金查詢 Template
	 *
	 */
	public function benefit()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_benefit'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 * 會員資料修改 Template
	 *
	 */
	public function memberAccount()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_account'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_account_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 * 忘記密碼頁面 Template 應該拿掉？wdj
	 *
	 */
	public function memberForgotPassword()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設

	  $page_data['_captcha_img']=$this->captcha_img();

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_forgot_password'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_forgot_password_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	public function memberForgotPasswordAction()
	{

        $_message='message';

        $_message_cap="";


       if ($this->input->get_post("rycode")!= $this->session->userdata('captcha'))
			{

					if ($this->input->get_post("rycode") != $this->session->userdata('captcha') && strlen($this->input->get_post("rycode")) > 0) {
                $_message_cap.="<font color=red size=\"3px\">驗證碼不符</font>";
            }
					 $this->session->set_flashdata($_message,$_message_cap);
            $data['post'] = $_POST;
            $this->session->set_flashdata($data['post']);

            redirect('memberForgotPassword');
            exit();
			}else{

			//

		  //$this->input->get_post("email")
		  	$_where=" where email='".$this->input->get_post("email")."' ";

		 	$_result01=$this->Sean_db_tools->db_get_max_record("*","ij_member",$_where);
				$_login_array=false;
				if(is_array($_result01))
				{
					foreach($_result01 as $key => $value)
			    {
			    	$_shopname="ijWedding";
						$_username=$value->user_name;
						$_newpass="123456";

						$_site_url="http://ijWedding.com.tw/";
						$_truename=$value->last_name.$value->first_name;
						$_to_email=$value->email;
						$_login_array=true;
					}
				}

		 	if($_login_array==false)
			{

				$this->session->set_flashdata("fail_message",  "<strong>驗證失敗!</strong> 系統找不到您的帳號或Email，請重新輸入或聯絡管理員．");

            	redirect('memberForgotPassword');
           		exit();

			}else{
				//$this->session->set_userdata('web_login',true);
				//$this->session->set_userdata('customer_login',true);
				//$this->session->set_userdata('web_member_data',$_login_array);
				$this->session->set_flashdata("message","<strong>申請完成!</strong> 系統已經寄發一份密碼更改確認信件到您使用來註冊的信箱中，請前往收信並依照信件中指示完成密碼修改，IJWedding 感謝您的支持．");
				$this->send_mail("忘記密碼",$this->input->get_post("email"));

				redirect('memberForgotPassword');
				exit();


			}
		}

  }

  /*
  送信? 應該拿掉？wdj
  */
  function send_mail($_mail_template="",$_mail="")
  {


    $config['protocol'] = 'smtp' ;
    $config['smtp_host'] = 'ssl://smtp.gmail.com';
    $config['smtp_user'] = 'gnyau20@dr5.com.tw';
    $config['smtp_pass'] = 'ng53942700';
    $config['smtp_port'] = '465';
    $config['smtp_timeout'] = '5';
    $config['mailtype'] = 'html';
    $config['newline'] = "\r\n";
    $config['crlf'] = "\r\n";
    $config['charset']   = 'utf-8';


    $_mail_template=urldecode($_mail_template);

   					$_shopname="ijWedding";
						$_username="";
						$_newpass="";
						$_site_url="http://ijWedding.com.tw/";
						$_truename="";
						$_to_email="seanng17@gmail.com";

    if(strlen($_mail) > 0 )
    {
    	 	$_where=" where email='".$_mail."' ";

				$_result01=$this->Sean_db_tools->db_get_max_record("*","ij_member",$_where);

				if(is_array($_result01))
				{
					foreach($_result01 as $key => $value)
			    {
			    	$_shopname="ijWedding";
						$_username=$value->user_name;
						$_newpass="123456";

						$_site_url="http://ijWedding.com.tw/";
						$_truename=$value->last_name.$value->first_name;
						$_to_email=$value->email;
					}
				}
    }

    $search  = array('@shopname@', '@username@', '@newpass@','@passwd@', '@site_url@', '@truename@');

    $replace = array($_shopname, $_username, $_newpass, $_newpass, $_site_url,$_truename);

    if(strlen($_mail_template) > 0 )
    {
	    $_where=" where mail_template_name='".$_mail_template."' ";

			$_result01=$this->Sean_db_tools->db_get_max_record("*","ij_mail_template",$_where);

			if(is_array($_result01))
			{

			    foreach($_result01 as $key => $value)
			    {
				    $this->load->library ('email') ;
				    $this->email->initialize($config);

					  $__mail_template_contant=str_replace($search, $replace,$value->mail_template_contant);
				    $__mail_template_notes=str_replace($search, $replace,$value->mail_template_notes);



				    $this->email->from('gnyau20@dr5.com.tw', 'seanng17');
				    $this->email->to($_to_email);
				    $this->email->subject($__mail_template_contant);


				    $this->email->message($__mail_template_notes);
				    $this->email->send();
				    $this->session->set_flashdata("success_message","發送完成");
				   }
		  }else{
		  	$this->session->set_flashdata("success_fail","發送失敗");
		  }
	  }else{
	  	$this->session->set_flashdata("success_fail","發送失敗");
	  }


  }

	/**
	 * 會員條款
	 *
	 */
	public function rule()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_rule'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_rule_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}


	/**
	 * privacy policy
	 *
	 */
	public function privacy()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_privacy_policy'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_privacy_policy_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}


	/**
	 * TERMS & CONDITIONS
	 *
	 */
	public function trems()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_tc'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_tc_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 * 全站通知 Template
	 *
	 */
	public function notification($type='')
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		switch($type){
			case "detail":
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'www/page_member/notification_item'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'www/page_member/notification_item_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('www/general/main', $page_data);
			break;
			default:
				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'www/page_member/notification'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'www/page_member/notification_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('www/general/main', $page_data);
			break;
		}
	}

	/**
	 * 搜尋結果 Template
	 *
	 */
	public function shopSearch()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_shop/page_search'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_shop/page_search_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	public function ad_link($banner_schedule_sn){
		/*$where_schedule['banner_list_start_date <=']=date("Y-m-d H:i:s",time());
		$where_schedule['banner_list_end_date >=']=date("Y-m-d H:i:s",time());
		$where_schedule['banner_schedule_sn']=$banner_schedule_sn;
		$where_schedule['status']='1';
		$banner_schedule=$this->Shop_model->_select('ij_banner_schedule_log',$where_schedule,0,0,0,0,0,'row');*/

		//檢查該廣告是否有效
		$banner_schedule=$this->Shop_model->get_adsBanner($banner_schedule_sn);
		//var_dump($banner_schedule);
		//echo $this->db->last_query();exit;
		if($banner_schedule){
			//click+1
			$this->Shop_model->_update_field_amount('ij_banner_schedule_log','banner_schedule_sn',$banner_schedule_sn,'click_amount','+1');
			$this->Shop_model->_update_field_amount('ij_banner_content','banner_content_sn',$banner_schedule['banner_content_sn'],'click_total_amount','+1');
			$banner_link_type_eng_name=$this->Shop_model->_select('ij_banner_link_type_ct','banner_link_type',$banner_schedule['banner_link_type'],0,0,0,0,'row','banner_link_type_eng_name')['banner_link_type_eng_name'];
			switch ($banner_link_type_eng_name){
			case 'link_product_sn':
    		redirect(base_url('shop/shopItem/'.$banner_schedule[$banner_link_type_eng_name]));
				break;
			case 'link_category_sn':
    		redirect(base_url('shop/shopCatalog/'.$banner_schedule[$banner_link_type_eng_name]));
				break;
			case 'banner_link':
    		redirect($banner_schedule[$banner_link_type_eng_name]);
				break;
			case 'search_keyword':
    		redirect(base_url('shop/shopSearch?Search='.$banner_schedule[$banner_link_type_eng_name]));
				break;
			default:
			}
		}else{
			echo '<script>window.opener.location.reload();window.close();</script>';
			$this->session->set_userdata(array('Mes'=>'很抱歉，查無此廣告連結或該廣告已關閉!'));
		}
	}
	public function hot_search_link($banner_content_sn){
		$where_schedule['banner_content_sn']=$banner_content_sn;
		$where_schedule['status']='1';
		//檢查該廣告是否有效
		$banner_schedule=$this->Shop_model->_select('ij_banner_content',$where_schedule,0,0,0,0,0,'row');
		if($banner_schedule){
			//click+1
			//$this->Shop_model->_update_field_amount('ij_banner_schedule_log','banner_schedule_sn',$banner_schedule_sn,'click_amount','+1');
			$this->Shop_model->_update_field_amount('ij_banner_content','banner_content_sn',$banner_content_sn,'click_total_amount','+1');
    		redirect(base_url('shop/shopSearch?Search='.$banner_schedule['banner_content_save_dir']),'auto',301);
		}else{
			$this->session->set_userdata(array('Mes'=>'很抱歉，查無此熱門關鍵字！'));
			//echo '<script>window.opener.location.reload();window.close();</script>';
			redirect(base_url());
		}
	}
	//連絡我們
    public function addCallcenter()
	{
		//連絡我們分類代碼17
		if($this->input->post()){
 	        if ($this->input->get_post("rycode")== $this->session->userdata('captcha')){
				$ExpectManDay=$this->Shop_model->_select('ij_ccapply_detail_code_ct',array('ccapply_detail_code'=>'17'),0,0,0,'ExpectManDay','asc','row','ExpectManDay')['ExpectManDay'];
				$this->load->model('membership/Callcenter');
				$member_sn=(@$this->session->userdata['web_member_data']['member_sn'])?$this->session->userdata['web_member_data']['member_sn']:'0';
			    $data = array(
			        'member_sn' => $member_sn,
			        'ccapply_detail_code' => $this->input->post('ccapply_detail_code',true),
			        'issue_date'          => date("Y-m-d H:i:s",time()),
			        'issue_expected_close_date'          => date("Y-m-d H:i:s",time()+86400*$ExpectManDay),
			        'question_subject' =>  $this->input->post('question_subject',true),
			        'question_description' =>  $this->input->post('question_description',true),
			        'attached_file_path' =>  $this->input->post('first_name',true),
			        'contact_email' =>  $this->input->post('email',true),
			        'contact_phone' =>  $this->input->post('phone',true),
			        'question_status' => "1"
			    );
				$data = $this->Shop_model->CheckUpdate($data,1);
			    if($nonce = $this->Callcenter->create_callcenter($data)){
			        $this->session->set_flashdata('message'," 訊息送出成功，相關人員將在 ".$ExpectManDay." 個工作天內與您連絡！");
			    }else{
			        $this->session->set_flashdata('message',"訊息送出失敗！");
			    }
				redirect(base_url('home/contactus'));
			}else{
				$this->ijw->_wait(base_url("home/contactus") , 2 , '很抱歉，您輸入的驗證碼錯誤，請重新輸入！',1);
			}
		}
	}
	public function blogItemPreview($blog_article_sn=0)
	{
		if(!$blog_article_sn) redirect(base_url('admin/content/contentBlog'));

		$page_data = array();
		$search=$this->input->get('search',true);
		if(!$search){
			$search_array='0';
		}else{
			$search_array=array('blog_article_titile'=>$search,'keyword'=>$search,'blog_article_content'=>$search);
		}
		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		$this->load->model('libraries_model');
		$page_data['page_content']['Item']=$this->libraries_model->_get_blog_faq('ij_blog_article',array('status'=>'1','ij_blog_article.blog_article_sn'=>$blog_article_sn))[0];
		//for sidebar
		//$page_data['page_content']['content_types']=$this->libraries_model->_get_blog_faq_categorys(1,'囍市集');
		//$like_field='associated_content_type_sn';
		//$content_type_sn=$page_data['page_content']['Item']['associated_content_type_sn'];
		//$page_data['page_content']['Items']=$this->libraries_model->_get_blog_faq('ij_blog_article',array('status'=>'1','post_flag'=>'1'));
		$page_data['page_content']['Items'][0]['keywords']=explode(chr(13).chr(10),trim($page_data['page_content']['Item']['keyword']));
		$page_data['page_content']['content_type']=$page_data['page_content']['Item']['associated_content_type_sn'];
		$page_data['page_content']['keyword_tag']='';
		$page_data['page_content']['search']=$search;
		$page_data['title']=$page_data['page_content']['Item']['blog_article_titile'];

		$page_data['page_content']['view_path'] = 'www/page_blog/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_blog/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}
}
