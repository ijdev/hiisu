<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<script src="public/metronic/global/plugins/carousel-owl-carousel/OwlCarousel2/owl.carousel.min.js"></script>
<script src="public/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js"></script>
<script src="public/metronic/frontend/layout/scripts/layout.js"></script>
<script>
    jQuery(document).ready(function() {
        Layout.init();
        Layout.initOWL();
       // LayersliderInit.initLayerSlider();
        //Layout.initImageZoom();
        Layout.initTouchspin();
        //Layout.initTwitter();
        Layout.initFixHeaderWithPreHeader();
        Layout.initNavScrolling();

        $('a[data-anchor]').click(function(){
            var anchor = $(this).attr('data-anchor');
            $('html, body').animate({
                scrollTop: $("#"+anchor).offset().top-80
            }, 600);
            return false;
        });
    });
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->