<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="public/metronic/global/plugins/bootstrap-selectsplitter/bootstrap-selectsplitter.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/jquery-minicolors/jquery.minicolors.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script type="text/javascript" src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script src="public/metronic/admin/pages/scripts/components-form-tools2.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
	var ifshowdelete=false;
	var C='<?php echo @$Item['product_template_sn']; ?>';
function getColor(D){
	if(!D) D=0;
	//if(typeof(website_color_status) == "undefined") website_color_status=true;
	//console.log(C);
  $.post('admin/product/AddColor', {
          product_template_sn: C,
          //website_color_status: website_color_status,
          website_color_sn: D
      }, function(data) {
				showColor(data);
      }, 'json');
}
function showColor(data){
  	var ColorList = $('#ColorList');
  	var photoColor = $('#photoColor');
    var html = '';
    var html2 = '';
    if(data){
	    for (var x = 0; x < data.length; x++) {
	    	var template_color_relation_sn=data[x].template_color_relation_sn;
	    	var website_color_sn=data[x].website_color_sn;
	    	var ifchecked=(data[x].status=='1')?'checked':'';
	        html += '<label class="checkbox-inline">';
	        html += '<input type="hidden" name="Color['+x+'][template_color_relation_sn]" value="'+ template_color_relation_sn +'" >';
	        html += '<input class="checkbox-list AddColor" type="checkbox" name="Color['+x+'][website_color_sn]" value="'+website_color_sn+'" '+ifchecked+' >'+data[x].color_name+' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ';
	        html += '</label>';
	        if(ifchecked && data[x].amount < 5){ //有勾選且數量小於5的列入選項
	        	html2 += '<option value="'+template_color_relation_sn+'">'+data[x].color_name+'</option>';
	        }
	    }
  	}
    ColorList.html(html);
    photoColor.html(html2);
    get_template_Photo();
		jQuery('.AddColor').click(function(){
		  //console.log($(this).prop('checked'));
	    //getColor($(this).val(),$(this).prop('checked'));
	    getColor($(this).val());
		});
}
jQuery(document).ready(function() {       
   	Metronic.init(); // init metronic core components
	  Layout.init(); // init current layout
    ComponentsFormTools2.init();
    $('.fancybox').fancybox();
    getColor();
    //get_template_Photo();
    get_template_Didider();
});
	$("form#picture_form").submit(function(e) {
					e.preventDefault();
	    var formData = new FormData($(this)[0]);
		 $.ajax({
		        url: window.location.pathname,
		        type: 'POST',
		        data: formData,
　　　      dataType:'json',
		        async: false,
		        success: function (data) {
      				//$('#uploadModal').load('#uploadModal');
      				//$('#uploadModal').hide();
      				$('#uploadModal').modal('hide');
		        	if(data.error){
		        		alert(data.error);
							}else{
								//console.log(data);
								showPhoto(data,'1');
							}
							$("#picture_form")[0].reset();
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		    });
    return false;
	});
	function showPhoto(data,if_replace){
	  	var PhotoList = $('#PhotoList');
			//console.log(data);
			//console.log(data.photos);
			var photos = new Array();
	    photos=data.photos;
			if(photos){
	      var html = '<tr class="trImgColor" id="template_color'+photos[0].associated_template_color_relation_sn+'" rel="'+data.color_name+'"><td>'+data.color_name+'</td>';
		    for (var x = 0; x < photos.length; x++) {
		    	//var template_color_relation_sn=data[x].template_color_relation_sn;
		    	//var website_color_sn=data[x].website_color_sn;
		    	//var ifchecked=(data[x].status=='1')?'checked':'';
		      html += '<td align="center" id="TemplateItem'+photos[x].template_picture_sn+'"><a class="fancybox" rel="photos'+data.associated_template_color_relation_sn+'" title="'+photos[x].image_alt+'" href="public/uploads/'+photos[x].image_path+'"><img src="public/uploads/'+photos[x].image_path+'" style="width:120px;" alt="'+photos[x].image_alt+'" title="'+photos[x].image_alt+'"></a><br><a href="javascript:void(0);" class="deleteTemplateItem'+photos[0].associated_template_color_relation_sn+'" ItemId="'+photos[x].template_picture_sn+'"><i class="fa fa-trash-o"></i>刪除</a></td>';
		    }
		  }else{
				var x = 0;
		  }
	    for (var y = x; y < 5; y++) { //補空缺
        html += '<td></td>';
	    }
	    html += '</tr>';
				//console.log(PhotoList.filter('tr#'+data.associated_template_color_relation_sn).prop('id'));
	    if(if_replace=='1'){
	    	$('#template_color'+data.associated_template_color_relation_sn).remove();
    	}
	    PhotoList.prepend(html);
	    $('#loading').hide();
		  $(".deleteTemplateItem"+photos[0].associated_template_color_relation_sn).click(function(){
		      var button = $(this);
					//console.log(button);
		      	var r = confirm('是否刪除此張版型圖檔?');
		          if(r)
		          {
							    $.post('/admin/admin/ajaxdel/', {dtable: '<?php echo base64_encode('ij_template_picture');?>',dfield:'<?php echo base64_encode('template_picture_sn');?>',did:button.attr('ItemId')}, function(data){
		              	if (data){
		              		//button.parents().filter('div#image'+button.attr('ItemId')).remove();
		              		$('#TemplateItem'+button.attr('ItemId')).html('');
		              	}else{
		              		alert('刪除失敗');
		              	}
							    });
							}else{
								return false;
		          }
		  });
	    //if(if_replace=='1'){
		  //	ifshowdelete=false;
		  //	showdeleteTemplateItem();
		  //}
	}
	function get_template_Photo(){
		//console.log(C);
		$('#PhotoList').html('<tr id="loading"><td colspan="6" align="center"><img src="public/metronic/global/plugins/jquery-file-upload/img/loading.gif"></td></tr>');
		
	  $.post('admin/product/GetColor', {
	          product_template_sn: C
	      }, function(data) {
	  		if(data){
			    for (var x = 0; x < data.length; x++) {
	  //console.log(data[x].color);
						showPhoto(data[x],'0');
					}
	    		//showdeleteTemplateItem();
				}
	      }, 'json');
	}
	$('#tab_images_uploader_uploadfiles').click(function(event) { //點上傳版型圖片
			if(!$('.AddColor').is(":checked")){
				alert('請先至少選取一個顏色');
    		return false;
    	}
      //$('#uploadModal').hide();
	});
	$("form#divider_form").submit(function() {
	    var formData = new FormData($(this)[0]);
		 $.ajax({
		        url: window.location.pathname,
		        type: 'POST',
		        data: formData,
　　　      dataType:'json',
		        async: false,
		        success: function (data) {
      				$('#uploadModal2').modal('hide');
		        	if(data.error){
		        		alert(data.error);
							}else{
								//console.log(data);
								showDivider(data);
							}
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		    });
    return false;
	});
	function showDivider(data){
	  	var DividerList = $('#DividerList');
	  	var html='';
		  for (var x = 0; x < data.length; x++) {
	    	html += '<tr class="trImgColor" id="template_divider'+data[x].template_divider_sn+'" rel="'+data[x].associated_area+'">';
	    	html += '<td>'+data[x].area_name+'</td>';
	    	html += '<td>'+data[x].default_divider_greeting_text+'</td>';
	    	html += '<td><a class="fancybox" rel="devider'+data[x].product_template_sn+'" title="'+data[x].area_name+'" href="public/uploads/'+data[x].default_divider_save_dir+'"><img src="public/uploads/'+data[x].default_divider_save_dir+'" style="width:320px;" alt="'+data[x].area_name+'" title="'+data[x].area_name+'"></a><br><a href="javascript:void(0);" class="deleteTemplateDivider" ItemId="'+data[x].template_divider_sn+'"><i class="fa fa-trash-o"></i>刪除</a></td>';
	    	html += '</tr>';
	  	}
			//console.log(html);

	    DividerList.html(html);
	    $(".deleteTemplateDivider").click(function(){
	        var button = $(this);
	        	var r = confirm('是否刪除此張分頁圖檔?');
	            if(r)
	            {
							    $.post('/admin/admin/ajaxdel/', {dtable: '<?php echo base64_encode('ij_template_divider_config');?>',dfield:'<?php echo base64_encode('template_divider_sn');?>',did:button.attr('ItemId')}, function(data){
	                	if (data){
	                		button.parents().filter('tr').remove();
	                		//$('#TemplateItem'+button.attr('ItemId')).html('');
	                	}else{
	                		alert('刪除失敗');
	                	}
							    });
	            }
	    });
	}
	function get_template_Didider(){
		//console.log(C);
	  $.post('admin/product/GetDivider', {
	          product_template_sn: C
	      }, function(data) {
	  		if(data){
						showDivider(data);
				}
	      }, 'json');
	}
	/*function showdeleteTemplateItem(){
		if(!ifshowdelete){
		  $(".deleteTemplateItem").click(function(){
		      var button = $(this);
				console.log(button);
		      	var r = confirm('是否刪除此張版型圖檔?');
		          if(r)
		          {
							    $.post('/admin/admin/ajaxdel/', {dtable: '<?php echo base64_encode('ij_template_picture');?>',dfield:'<?php echo base64_encode('template_picture_sn');?>',did:button.attr('ItemId')}, function(data){
		              	if (data){
		              		//button.parents().filter('div#image'+button.attr('ItemId')).remove();
		              		$('#TemplateItem'+button.attr('ItemId')).html('');
		              	}else{
		              		alert('刪除失敗');
		              	}
							    });
							}else{
								return false;
		          }
		  });	
		  ifshowdelete=true;
		}
	}*/
</script>

