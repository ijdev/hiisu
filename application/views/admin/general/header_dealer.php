<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Page Header, include by general/main.php，變數由之傳入
*/
//var_dump($this->session->userdata['member_data']['dealer_logo']);
?>
<!-- BEGIN HEADER -->
<div class="page-header">
  <!-- BEGIN HEADER TOP -->
  <div class="page-header-top">
    <div class="container">
      <!-- BEGIN LOGO -->
      <div class="page-logo">
        <a href="admin/main"><img src="public/img/ij-logo.png" alt="logo" class="logo-default"></a>
      </div>
<?if($this->session->userdata['member_data']['dealer_logo']=='x'){?>
      <div>
        <a href="admin/main"><img src="public/uploads/<?=$this->session->userdata['member_data']['dealer_logo']?>" alt="logo" class="logo-dealer"></a>
      </div>
<?}?>
      <!-- END LOGO -->
        <div style="font-size:150%;font-weight:bold;font-family:'微軟正黑體';display: inline;top: 20px;position: relative;">經銷商管理系統 <?php echo $this->session->userdata['member_data']['dealer_name'];?></div>
      <!-- BEGIN RESPONSIVE MENU TOGGLER -->
      <a href="javascript:;" class="menu-toggler"></a>
      <!-- END RESPONSIVE MENU TOGGLER -->
      <!-- BEGIN TOP NAVIGATION MENU -->
      <div class="top-menu">
        <ul class="nav navbar-nav pull-right">
          <!-- BEGIN USER LOGIN DROPDOWN -->
          <li class="dropdown dropdown-user dropdown-dark">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <img alt="" class="img-circle" style="border-radius: unset !important;" src="/public/metronic/admin/layout3/img/avatarb.jpg">
            <span class="username username-hide-mobile"><?php echo $this->session->userdata['member_data']['last_name'];?></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-default">
              <li>
                <a href="admin/admin/notification">
                <i class="icon-envelope-open"></i> 全站通知 <span class="badge badge-danger"> <?php echo $this->page_data['unreadmessage'];?> </span>
                </a>
              </li>
<?if($this->session->userdata['member_data']['role_count'] >1){?>
              <li>
                <a href="admin/main/role_select/">
                <i class="icon-envelope-open"></i> 權限角色 </span>
                </a>
              </li>
<?}?>
              <li class="divider"></li>
             <?php
             if($this->session->userdata('member_login')== true)
             {
             	 echo  '
             	 <li>
                <a href="admin/main/managerSignOut">
                <i class="icon-logout"></i> Log Out</a>
              </li>
             	 ';
             }else{
             		echo '
             		 <li>
                <a href="admin/login">
                <i class="icon-key"></i> Log In</a>
              </li>
             		';
             }
             ?>


            </ul>
          </li>
          <!-- END USER LOGIN DROPDOWN -->
        </ul>
      </div>
      <!-- END TOP NAVIGATION MENU -->
    </div>
  </div>
  <!-- END HEADER TOP -->
  <!-- BEGIN HEADER MENU -->
  <div class="page-header-menu">
    <div class="container">
      <div class="hor-menu ">
        <ul class="nav navbar-nav">
          <li class="active">
            <a href="admin/main">控制台</a>
          </li>
           <li class="menu-dropdown classic-menu-dropdown">
            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
              商品管理 <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
              <li>
                <a href="admin/product/productTable/" class="iconify">
                <i class="icon-handbag"></i> 商品列表 </a>
              </li>
              <li class="">
                <a href="admin/product/dealerCategory">
                <i class="icon-handbag"></i> 分類上架管理</a>
              </li>
              <!--li>
                <a href="admin/product/inventory" class="iconify">
                <i class="fa fa-dashboard"></i> 商品庫存 </a>
              </li-->
            </ul>
          </li>
          <li>
            <a href="admin/order/orderTable">
              訂單管理
            </a>
          </li>
          <li>
            <a href="admin/content/contentBlog">
              部落格管理
            </a>
          </li>
          <li>
            <a href="admin/member/memberTable">
              會員管理
            </a>
          </li>
          <li>
            <a href="admin/admin/ticketTable">
              客服中心
            </a>
              </li>
		          <li>
                <a href="admin/admin/accounting" class="iconify">
                帳務管理</a>
              </li>
               <li class="menu-dropdown classic-menu-dropdown">
                <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
                  帳戶設定 <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu pull-left">
                  <li>
                    <a href="/admin/Member/dealerTable/edit/<?php echo $this->session->userdata('member_data')['associated_dealer_sn'];?>" class="iconify">
                    <i class="icon-handbag"></i> 一般設定 </a>
                  </li>
                  <li>
                    <a href="/admin/Member/dealerTable/edit/<?php echo $this->session->userdata('member_data')['associated_dealer_sn'];?>#tab_manager" class="iconify">
                    <i class="icon-handbag"></i> 管理人員 </a>
                  </li>
                  <li>
                    <a href="admin/Member/dealerPage/<?php echo $this->session->userdata('member_data')['associated_dealer_sn'];?>" class="iconify">
                    <i class="fa fa-dashboard"></i> 專頁管理 </a>
                  </li>
                </ul>
              </li>
		          <li>
		            <a href="javascript:;">Ｑ＆Ａ下載區</a>
		          </li>

        </ul>
      </div>
      <!-- END MEGA MENU -->
    </div>
  </div>
  <!-- END HEADER MENU -->
</div>
<!-- END HEADER -->