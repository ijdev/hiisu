<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script>
jQuery(document).ready(function() {       
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	//ComponentsPickers.init();
	jQuery(".form_datetime").datetimepicker({
        autoclose: true,
        isRTL: Metronic.isRTL(),
        format: "yyyy-mm-dd hh:ii",
        pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left")
    });

		$("#published_locaiotn_type").change(function(){
			$('#banner_location_sn option').remove();$('#banner_location_sn').append('<option>Loading...</option>');
			$.get('/admin/admin/Chg_locaiotn_type/'+$(this).val(),function(data){$('#banner_location_sn option').remove();$('#banner_location_sn').append(data);});
		});
		
		$("#banner_link_type").change(function(){
			//console.log($('option:selected', this).attr('banner_type_name'));
			banner_link_type_eng_name= $('option:selected', this).attr('banner_link_type_eng_name');
			//$('#banner_type').val($('option:selected', this).attr('banner_type'));
			//console.log($('#banner_type').val());
    	adTypeChange(banner_link_type_eng_name);
		});
    // init ADTypeSelector
    adTypeChange('<?php echo @$Schedule['banner_link_type_eng_name'];?>');
    //jQuery('select[name="adType"]').change(function(){
		//adTypeChange();
    //});

});
function adTypeChange(banner_link_type_eng_name){
	jQuery('.adType').hide();
	//console.log(jQuery('select[name="Item[banner_type]"] option:selected').attr('banner_type_eng_name'));
	jQuery('.'+banner_link_type_eng_name).show();
	//$('#banner_type_eng_name').val(jQuery('select[name="Item[banner_type]"]  option:selected').attr('banner_type_eng_name'));
	//jQuery('.'+jQuery('select[name="Item[banner_type]"]  option:selected').attr('banner_type_eng_name')+'AD input').prop('disabled',false);
}
</script>

