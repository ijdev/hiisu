<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
if(isset($_result01) && is_array($_result01))
{
 	foreach($_result01 as $key => $value)
  {

  	$_result_item=$value;

  }
}

?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>聯盟會員分級 <small>分級設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        會員管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/member/partnerTable">聯盟會員</a>
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         <a href="admin/member/partnerLevel">聯盟會員分級管理</a>
         <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         聯盟會員分級編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">聯盟會員分級設定</span>
            </div>
            <div class="tools">
              <a href="admin/member/partnerLevel"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
            <?php $this->load->view('sean_admin/general/flash_error');?>
            <form class="form-horizontal" role="form" action="<?php echo $_action_link;?>">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">名稱 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" required name="partner_level_name" maxlength="32" id="partner_level_name"  value="<?php if(isset($_result_item->partner_level_name)) echo $_result_item->partner_level_name;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="seq" class="col-lg-3 control-label">排序 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input type="number" class="form-control" required name="sort_order" id="sort_order" maxlength="3"  value="<?php if(isset($_result_item->sort_order)) echo $_result_item->sort_order;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-3 control-label">說明</label>
                  <div class="col-lg-8">
                    <textarea class="form-control" rows="3" name="description" id="description"  ><?php if(isset($_result_item->description)) echo $_result_item->description;?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">成交訂單分潤比例(%)  <span class="require">*</span></label>
                  <div class="col-lg-8">
	                      <div class="input-group spinner col-lg-2">
											    <input type="text" class="form-control form-inline" required name="bonus" id="bonus"  value="<?php if(isset($_result_item->bonus)) echo $_result_item->bonus;else echo "1";?>">
											    <div class="input-group-btn-vertical">
											      <button class="btn btn-default" type="button"><i class="fa fa-caret-up"></i></button>
											      <button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i></button>
											    </div>
											  </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="seq" class="col-lg-3 control-label">招募新會員獎金 <span class="require">*</span></label>
                  <div class="col-lg-1">
                    <input type="number" class="form-control" required name="newmember" id="newmember"  value="<?php if(isset($_result_item->newmember)) echo $_result_item->newmember;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">是否啟用 <span class="require">*</span></label>
                  <div class="col-lg-8">
                   <select name="status" id="status" class="form-control" >
                            <option value="1" <?php if(isset($_result_item->status) && $_result_item->status==1 || !isset($_result_item->status)) echo "selected";?>>是</option>
                            <option value="0"  <?php if(isset($_result_item->status) && $_result_item->status==0 || !isset($_result_item->status)) echo "selected";?>>否</option>
                          </select>
                  </div>
                </div>
               <!--  <div class="form-group">
                  <label for="updater" class="col-lg-3 control-label">更新者</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" name="update_member_sn" id="update_member_sn"  placeholder="<?php if(isset($_result_item->update_member_sn)) echo $_result_item->update_member_sn;?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-3 control-label">更新時間</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  name="last_time_update" id="last_time_update" placeholder="<?php if(isset($_result_item->last_time_update)) echo $_result_item->last_time_update;?>" Disabled>
                  </div>
                </div>
              </div>
              -->
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                    <a href="<?php echo $_father_link;?>"><button type="button" class="btn default">Cancel</button></a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
