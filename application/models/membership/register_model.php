<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
		$this->load->model('sean_models/Sean_general');
        }

    /**
     *
     * create_member
     *
     * @param string $username
     * @param string $password
     * @param string $email
     * @param string $first_name
     * @param string $last_name
     * @return mixed
     *
     */

    public function create_member($_data) {

        //會員參數
		$_c_member_config="member_config";
		$_c_member_data="member_data";
		//會員資料庫參數 抓config/member_config.php 裡的member_data 陣列
		$member_item=$this->Sean_general->general_data($_c_member_config,$_c_member_data);
		// 指定會員資料庫參數 至session
		$this->session->set_userdata('member_item',$member_item);
		
		$_list_item="";
		foreach ($member_item as $k => $i)
		{
				$data[$k]=$_data[$k];
		}
		
		
        $this->db->set('last_login', 'NOW()', FALSE);
        $this->db->insert('member', $data);
        
        if ($this->db->affected_rows() == 1) {
            return $nonce;
        }
        return false;
    }


}

/* End of file register_model.php */
/* Location: ./application/models/membership/register_model.php */