<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容的 page level js
*/
?>

<script src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
<script src="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->
<script src="public/metronic/global/plugins/zoom/jquery.zoom.min.js" type="text/javascript"></script><!-- product zoom -->
<script src="public/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script><!-- Quantity -->


<script src="public/metronic/frontend/layout/scripts/layout.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Layout.init();
        //Layout.initImageZoom();
        Layout.initTouchspin();
        Layout.initFixHeaderWithPreHeader();
        Layout.initNavScrolling();
    });
    
	  $('select[id="ccapply_code"]').change(function() {
	        var data = $(this).val(); // Get Selected Value
					get_data(data);

    });
      
		function get_data(data){
		    $.ajax({
		        url: 'Member/get_ccapply_detail',
		        data: 'data=' + data,
		        dataType: 'json',
		        success: function(result) {
		         		 $('#ccapply_detail_code').empty();
								 $(result).appendTo("#ccapply_detail_code");
  							 //$.uniform.update('#addr_town_code');
		        }
		    });
    }
$('#ticket_submit').click(function(){
    $('#ticket_form').submit();
});
$('#replybutton').click(function(){
    $('#reply_form').submit();
});

$("#ajax_msg_call").on("show.bs.modal", function(e) {
    var link = $(e.relatedTarget);
    //console.log($(this));
    $(this).find(".modal-content").load(link.attr("href"));
});
$("#ajax_msg_call").on("hide.bs.modal", function(e) {
    $(this).find(".modal-content").html('');
});
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->