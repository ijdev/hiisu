<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="public/metronic/global/plugins/jquery-minicolors/jquery.minicolors.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<!--script src="public/metronic/admin/pages/scripts/components-form-tools2.js"></script-->
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {       
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
<?php if(@$Item['channel_name']){?>
	ChgChannelName('<?php echo @$Item['channel_name'];?>','<?php echo @$Item['spec_option_config_sn'];?>');
<?php }?>
    //ComponentsFormTools2.init();
});
function ChgChannelName(C,D){ //變動館別、帶入值
	$('#Categorys option').remove();$('#Categorys').append('<option>Loading...</option>');
	$.get('/admin/product/ChgChannelName/'+C+'/'+D,function(data){$('#Categorys option').remove();$('#Categorys').append(data);});;
}
</script>

