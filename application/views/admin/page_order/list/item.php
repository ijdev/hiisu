<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/pages/css/portfolio.css" rel="stylesheet" type="text/css"/>
<style>
.fa-check-square-o {
  font-size:20px;
}
</style>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>訂單列表 <small>訂單編輯</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="icon-basket font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">
              訂單 #<?php echo $order[0]['sub_order_num']?> </span>
              <span class="caption-helper"><?php echo $order[0]['order_create_date']?></span>
            </div>
            <div class="actions btn-set">
              <a href="javascript:;" class="btn green print"><i class="fa fa-file-pdf-o"></i> 列印</a>
              <a href="admin/order/orderTable" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
      <?php $this->load->view('www/general/flash_error');?>
          <div class="portlet-body">
           <div class="tabbable">
            <ul class="nav nav-tabs nav-tabs-lg">
              <li class="active">
                <a href="#tab_detail" data-toggle="tab"> 訂單明細 </a>
              </li>
              <li>
                <a href="#tab_history" data-toggle="tab"> 更新歷史 </a>
              </li>
              <li>
                <a href="#<?php echo (@$order[0]['member_question_sn'])?'tab_message':'ticket_form';?>" data-toggle="tab"> 客服紀錄 </a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_detail">
                <div class="row">
                  <div class="col-md-4 col-sm-12">
                    <div class="portlet blue-hoki box">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs"></i>訂購人資料
                        </div>
                      </div>
                      <div class="portlet-body">
                        <div class="row static-info">
                          <div class="col-md-3 name">
                             姓名:
                          </div>
                          <div class="col-md-9 value">
<?php if($this->session->userdata('member_role_system_rules')[0]['role_name']!='經銷商管理員' && $this->session->userdata('member_role_system_rules')[0]['role_name']!='供應商管理員'){?>
                             <a href="/admin/Member/memberTable_item/edit/<?php echo $order[0]['member_sn']?>" target="_blank">
<?php }?>
                              <?php echo $order[0]['buyer_last_name']?><?php echo $order[0]['buyer_first_name']?>&nbsp;
<?php if($this->session->userdata('member_role_system_rules')[0]['role_name']!='經銷商管理員' && $this->session->userdata('member_role_system_rules')[0]['role_name']!='供應商管理員'){?>
                              <i class="fa fa-external-link"></i></a>
<?php }?>
                          </div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">
                             Email:
                          </div>
                          <div class="col-md-9 value">
                             <?php echo $order[0]['buyer_email']?>
                          </div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">
                             住址 :
                          </div>
                          <div class="col-md-9 value">
                             <?php echo $order[0]['buyer_addr_city']?><?php echo $order[0]['buyer_addr_town']?> <?php echo $order[0]['buyer_zipcode']?> <?php echo $order[0]['buyer_addr1']?>
                          </div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">
                             市話:
                          </div>
                          <div class="col-md-9 value">
                             <?php echo $order[0]['buyer_tel_area_code']?><?php echo $order[0]['buyer_tel']?>
                          </div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">
                             手機:
                          </div>
                          <div class="col-md-9 value">
                              <?php echo $order[0]['buyer_cell']?>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!--div class="portlet yellow-crusta box">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs"></i>訂單資訊
                        </div>
                        <div class="actions">
                          <a href="javascript:;" class="btn btn-default btn-sm" data-toggle="modal" data-target="#statusModal">
                            <i class="fa fa-pencil"></i> 編輯
                          </a>
                        </div>
                      </div>
                      <div class="portlet-body">
                        <div class="row static-info">
                          <div class="col-md-3 name">訂單編號 #:</div>
                          <div class="col-md-9 value"><?php echo $order[0]['sub_order_sn']?></span></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">成立時間:</div>
                          <div class="col-md-9 value"><?php echo $order[0]['order_create_date']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">訂單狀態:</div>
                          <div class="col-md-9 value"><span class="label label-info"><?php echo $order[0]['sub_order_status_name']?></span></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">配送狀態:</div>
                          <div class="col-md-9 value"><span class="label label-warning"><?php echo $order[0]['delivery_status_name']?></span></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">訂單客服:</div>
                          <div class="col-md-9 value"><?php echo $order[0]['order_process_member_name']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">差額設定:</div>
                          <div class="col-md-9 value"><?php echo $order[0]['specail_offer_price']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">應收總價:</div>
                          <div class="col-md-9 value">$<?php echo number_format($order[0]['sub_sum'],0)?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">發票格式:</div>
                          <div class="col-md-9 value"><?php echo ($order[0]['company_tax_id'] && $order[0]['invoice_title'])?'三':'二'?>聯</div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">發票抬頭:</div>
                          <div class="col-md-9 value"><?php echo $order[0]['invoice_title']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">統一編號:</div>
                          <div class="col-md-9 value"><?php echo $order[0]['company_tax_id']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">付款方式:</div>
                          <div class="col-md-9 value"><?php echo $order[0]['payment_method_name']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">付款狀態:</div>
                          <div class="col-md-9 value"><span class="label label-success"><?php echo $order[0]['payment_status_name']?></span></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">備註:</div>
                          <div class="col-md-9 value"><?php echo $order[0]['memo']?></div>
                        </div>
                      </div-->
                      <!-- End of portlet-body -->
                    <!--/div-->
                    <!-- End of portlet -->
                  </div>
                  <div class="col-md-4 col-sm-12">
                    <div class="portlet green-meadow box">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs"></i>收件人資料
                        </div>
                        <div class="actions">
                          <a href="#" class="btn btn-default btn-sm" data-toggle="modal" data-target="#userModal">
                          <i class="fa fa-pencil"></i> 編輯 </a>
                        </div>
                      </div>
                      <div class="portlet-body">
                        <div class="row static-info">
                          <div class="col-md-3 name">姓名:</div>
                          <div class="col-md-9 value"><?php echo $order[0]['receiver_last_name']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">住址:</div>
                          <div class="col-md-9 value"><?php echo $order[0]['receiver_addr_city']?><?php echo $order[0]['receiver_addr_town']?> <?php echo $order[0]['receiver_zipcode']?> <?php echo $order[0]['receiver_addr1']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">市話:</div>
                          <div class="col-md-9 value"><?php echo $order[0]['receiver_tel_area_code']?><?php echo $order[0]['receiver_tel']?><?php echo (@$order[0]["receiver_tel_ext"])?' #'.$order[0]["receiver_tel_ext"]:'';?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">手機:</div>
                          <div class="col-md-9 value"><?php echo $order[0]['receiver_cell']?></div>
                        </div>
                  			<div class="row static-info">
                          <div class="col-md-3 name">&nbsp;</div>
                          <div class="col-md-9 value">&nbsp;</div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-12">
                    <div class="portlet green-meadow box">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs"></i>發票資料
                        </div>
                        <div class="actions">
                          <a href="#" id="invoice_edit" class="btn btn-default btn-sm" data-toggle="modal" data-target="#invoiceModal">
                          <i class="fa fa-pencil"></i> 編輯 </a>
                        </div>
                      </div>
                      <div class="portlet-body" id="invoice_area">
<?php if($order[0]['invoice_num']){?>
                        <div class="row static-info">
                          <div class="col-md-3 name">號碼:</div>
                          <div class="col-md-9 value"><?php echo $order[0]['invoice_num']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name" style="padding-right: 0;">開立日期:</div>
                          <div class="col-md-9 value"><?php echo (@$order[0]['invoice_time']!=0)?substr($order[0]['invoice_time'],0,10):''?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name" style="padding-right: 0;">隨機碼:</div>
                          <div class="col-md-9 value"><?php echo (@$order[0]['invoice_receiver_first_name'])?$order[0]['invoice_receiver_first_name']:''?></div>
                        </div>
<?php }elseif($order[0]['payment_status']=='2' && @$order[0]['Message']){?>
                        <div class="row static-info">
                          <div class="col-md-3 name">訊息:</div>
                          <div class="col-md-9 value"><?php echo $order[0]['Message']?></div>
                        </div>
<?php }else{?>
                        <div class="row col-md-12 text-center static-info">
                          <button type="button" id="set_invoice" class="btn btn-sm btn-info">開立發票</button>
                        </div>
<?php /*}elseif($order[0]['payment_status']=='2' && @$order[0]['invoice_aReturn_Info']){
$invoice_aReturn_Info=json_decode($order[0]['invoice_aReturn_Info'],true);
?>
                        <div class="row static-info">
                          <div class="col-md-3 name" style="padding-right: 0;">預計開立:</div>
                          <div class="col-md-9 value"><?php echo(@$invoice_aReturn_Info['CreateStatusTime'])?$invoice_aReturn_Info['CreateStatusTime']:$this->ijw->date_add_days($order[0]['payment_done_date'],INVOICE_DELAY_DAYS)?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">序號:</div>
                          <div class="col-md-9 value"><?php echo $invoice_aReturn_Info['InvoiceTransNo']?></div>
                        </div>
<?php */}?>
                        <div class="row static-info">
                          <div class="col-md-3 name">格式:</div>
                          <div class="col-md-9 value">
                            <?php
                              $open_invoice_type = $order[0]['open_invoice_type'];
                              //if($order[0]['open_invoice_type']){
                                if($order[0]['company_tax_id']&&$order[0]['invoice_title']){
                                  echo '三聯';
                                }
                                else {//1捐出的情況也是二連單
                                  echo '二聯';
                                }
                              //}else{
                              //    echo '空白';
                              //}
                            ?>
                           </div>
                        </div>
<?if($order[0]['invoice_title'] && $order[0]['company_tax_id']){?>
                        <div class="row static-info">
                          <div class="col-md-3 name">抬頭:</div>
                          <div class="col-md-9 value">
                            <?php
                              //if($order[0]['open_invoice_type']=='3'){
                                echo $order[0]['invoice_title'];
                              //}
                            ?>
                          </div>
                        </div>
                          <div class="row static-info">
                          <div class="col-md-3 name">統編:</div>
                          <div class="col-md-9 value">
                            <?php
                              //if($order[0]['open_invoice_type']=='3'){
                                echo $order[0]['company_tax_id'];
                              //}
                            ?>
                            </div>
                        </div>
<?}?>
                          <div class="row static-info">
                          <div class="col-md-3 name">地址:</div>
                          <div class="col-md-9 value">
                          <?php if(@$order[0]['invoice_receiver_zipcode']) echo $order[0]['invoice_receiver_zipcode']; else echo @$order[0]['buyer_zipcode'];?>
                          <?php if(@$order[0]['invoice_receiver_addr_city']) echo $order[0]['invoice_receiver_addr_city']; else echo @$order[0]['buyer_addr_city'];?>
                          <?php if(@$order[0]['invoice_receiver_addr_town']) echo $order[0]['invoice_receiver_addr_town']; else echo @$order[0]['buyer_addr_town']?>
                          <?php if(@$order[0]['invoice_receiver_addr1']) echo $order[0]['invoice_receiver_addr1']; else echo @$order[0]['buyer_addr1'];?>
                          </div>
                        </div>
                          <div class="row static-info" style="display:none;">
                          <div class="col-md-3 name">發票處理:</div>
                          <div class="col-md-9 value">
                            <?php
                              //if($order[0]['open_invoice_type']=='1')
                              //  $Invoice_carrie_type=4;
                              //else
                              $Invoice_carrie_type = $order[0]['invoice_carrie_type'];
                              $order[0]['invoice_carrie_type']=$this->config->item("invoice_way");
                              /*foreach (@$order[0]['invoice_carrie_type'] as $key => $value) {
                                if($key==$Invoice_carrie_type)
                                  print_r($value);
                              }*/
                            ?>
                          </div>
                        </div>
                          <?if($Invoice_carrie_type=='4'){?>
                          <div class="row static-info">
                          <div class="col-md-3 name">捐贈單位:</div>
                          <div class="col-md-9 value">
                            <?=@$invoice_donor_ct[$order[0]['invoice_donor']]?>
                          </div>
                          </div>
                          <?}?>
                          <?if($Invoice_carrie_type!='4' && $order[0]['email_for_eletronic_invoce']){?>
                          <div class="row static-info">
                          <div class="col-md-3 name">載具編號:</div>
                          <div class="col-md-9 value">
                            <?=$order[0]['email_for_eletronic_invoce']?>
                          </div>
                          </div>
                          <?}?>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- portlet 購物清單 - 實體商品 -->
                <div class="row">
                  <div class="col-md-12 col-sm-12">
                    <div class="portlet red-sunglo box">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs"></i>訂單相關資料
                        </div>
<?php if($this->session->userdata('member_role_system_rules')[0]['role_name']!='經銷商管理員'){?>
                        <div class="actions">
                          <a href="javascript:;" class="btn btn-default btn-sm" data-toggle="modal" data-target="#statusModal">
                            <i class="fa fa-pencil"></i> 編輯
                          </a>
                        </div>
<?}?>
                      </div>
                <div class="portlet-body">
	                <div class="row">
                  	<div class="col-md-6 col-sm-12">
                        <div class="row static-info">
                          <div class="col-md-5 name">供應商:</div>
                          <div class="col-md-7 value"><?php echo $order[0]['supplier_name']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-5 name">訂單編號/交易序號:</div>
                          <div class="col-md-7 value"><?php echo $order[0]['sub_order_num']?> / <?php echo $order[0]['order_num']?>
                          <!-- / <?php echo ($order[0]['original_order_sn'])? $order[0]['original_order_sn']:''?>--></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-5 name">付款方式:</div>
                          <div class="col-md-7 value"><?php echo $order[0]['payment_method_name']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-5 name">付款狀態:</div>
                          <div class="col-md-7 value">
                            <?/*交易：<span class="label label-success"><?php echo $order[0]['payment_status_name']?></span>
                            訂單：*/?><span class="label label-success"><?php echo $order[0]['pay_status_name']?></span>
                          </div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-5 name">配送方式/總重量:</div>
                          <div class="col-md-7 value"><?php echo $order[0]['delivery_method_name']?> / <?php echo ($order[0]['total_weight']!='0.0000')?round($order[0]['total_weight'],1).'kg':'0'?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-5 name">配送狀態/單號:</div>
                          <div class="col-md-7 value"><span class="label label-warning"><?php echo $order[0]['delivery_status_name']?></span>&nbsp;
                          <?php echo $order[0]["logistics_ticket_num"];?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-5 name">出貨時間:</div>
                          <div class="col-md-7 value">&nbsp;<?php echo $order[0]["designated_delivery_date"];?></div>
                        </div>
	            			</div>
                  	<div class="col-md-6 col-sm-12">
                        <div class="row static-info">
                          <div class="col-md-2 name">訂單日期:</div>
                          <div class="col-md-10 value"><?php echo $order[0]['order_create_date']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-2 name">訂單狀態:</div>
                          <div class="col-md-10 value"><span class="label label-info"><?php echo $order[0]['sub_order_status_name']?></span></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-2 name">發票格式:</div>
                          <div class="col-md-10 value"><?php echo ($order[0]['company_tax_id'] && $order[0]['invoice_title'])?'三':'二'?>聯</div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-2 name">發票抬頭:</div>
                          <div class="col-md-10 value"><?php echo $order[0]['invoice_title']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-2 name">統一編號:</div>
                          <div class="col-md-10 value"><?php echo $order[0]['company_tax_id']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-2 name">訂單客服:</div>
                          <div class="col-md-10 value"><?php echo $order[0]['order_process_member_name']?></div>
                        </div>
	            			</div>
		                  <div class="col-md-12 col-sm-12">
		                        <div class="row static-info">
		                          <div class="col-md-2 name">備註:</div>
		                          <div class="col-md-10 value"><?php echo $order[0]['memo']?></div>
		                        </div>
		                  </div>
<?php if($this->session->userdata['member_data']['user_name']=='wdj315@gmail.com'){?>
                      <div class="col-md-12 col-sm-12">
                            <div class="row static-info">
                              <div class="col-md-2 name">付款回傳:</div>
                              <div class="col-md-10 value">
                                <?php echo '<pre>' . var_export(json_decode($order[0]['pay_back_result'],true), true) . '</pre>'?></div>
                            </div>
                      </div>
<?php }?>
	            		</div>
            		</div>
              </div>
                    <div class="portlet red-sunglo box">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs"></i>訂購商品明細 &nbsp; ( 共 <?php echo $order[0]['sub_count']?> 項 )
                        </div>
                        <div class="actions">
                          <!--
                          <a href="javascript:;" class="btn btn-default btn-sm" data-toggle="modal" data-target="#pproductModal">
                            <i class="fa fa-plus"></i> 新增
                          </a>
                          -->
                        </div>
                      </div>
                      <div class="portlet-body">
                        <div class="table-responsive">
                          <table class="table table-hover table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>商品編號</th>
                                <th>商品名稱</th>
                                <th>庫號</th>
                                <th>規格</th>
                                <th>數量</th>
                                <th>售價</th>
                                <th>加購</th>
                                <th>折扣</th>
																<?php echo ($order[0]['order_item'][0]['upgrade_discount_amount'])?'<th>升級折價</th>':'';?>
                                <th>囍市錢包</th>
<?php	if($this->session->userdata['member_data']['member_level_type']=='9'){?>
                                <th>分潤趴數/金額</th>
<?php }?>
                                <th>小計</th>
                                <!--th>功能</th-->
                              </tr>
                            </thead>
                            <tbody>
                              <?php $if_return=false;if($order[0]['order_item']){ foreach($order[0]['order_item'] as $order_item){
                                if($order_item['order_cancel_sn']>0) $if_return=true;?>
                                <tr>
                                  <td><a title="商品後台頁面" href="/admin/product/productItem/<?php echo $order[0]['channel_name']?>/<?php echo $order_item['product_sn'];?>" target="_blank"><?php echo $order_item['product_sn'];?></a></td>
							                    <td><a title="商品前台頁面" href="/shop/shopItem/<?php echo $order_item['product_sn'];?>" target="_blank"><?php echo $order_item['product_name'];?></a></td>
							                    <td><?php echo $order_item['unispec_warehouse_num'];?></td>
							                    <td><?php echo $order_item['product_package_name'];?><?php echo ($order_item['upgrade_discount_amount'])? '(升級)':'';?></td>
							                    <td align="center"><?php echo $order_item['buy_amount'];?></td>
							                    <td align="right">$<?php echo $order_item['sales_price'];?> &nbsp; </td>
							                    <td><?php echo $order_item['addon_flag']==1? '<i class="fa fa-check-square-o"></i>':'';?></td>
							                    <td align="right"><?php echo $order_item['coupon_code_cdiscount_amount'];?> &nbsp; </td>
																<?php echo ($order_item['upgrade_discount_amount'])?'<td align="right">'.$order_item['upgrade_discount_amount'].'</td>':'';?>
							                    <td align="right"><?php echo $order_item['gift_cash_discount_amount'];?> &nbsp; </td>
<?php	if($this->session->userdata['member_data']['member_level_type']=='9'){?>
							                    <td align="center"><?php echo $order_item['sharing_percentage']*100;?>% / <?php echo $order_item['sharing_amount'];?></td>
<?php }?>
							                    <td align="right"><?php echo $order_item['actual_sales_amount'];?> &nbsp; </td>
                                  <!--td>
                                    <a href="javascript:;" data-toggle="modal" data-target="#pproductModal">
                                      <i class="fa fa-pencil-square"></i>編輯
                                    </a>&nbsp;&nbsp;
                                    <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                                  </td-->
                                </tr>
                              <?php @$cdiscount_amount+=$order_item['coupon_code_cdiscount_amount'];@$gdiscount_amount+=$order_item['gift_cash_discount_amount'];@$actual_sales_amount+=$order_item['actual_sales_amount'];@$upgrade_discount_amount+=$order_item['upgrade_discount_amount'];}}?>
                              <tr>
                              	<td colspan="5" align="right">折價券折抵</td>
                              	<td colspan="2" align="right"><?php echo $cdiscount_amount;?> &nbsp; </td>
                              	<td colspan="2" align="right">總金額</td>
                              	<td colspan="2" align="right"><?php echo $actual_sales_amount+$upgrade_discount_amount;?> &nbsp; </td>
                              </tr>
                              <tr>
                              	<td colspan="5" align="right">囍市錢包折抵</td>
                              	<td colspan="2" align="right"><?php echo $gdiscount_amount;?> &nbsp; </td>
                              	<td colspan="2" align="right">折扣</td>
                              	<td colspan="2" align="right"><?php echo $upgrade_discount_amount;?> &nbsp; </td>
                              </tr>
                              <tr>
                              	<td colspan="9" align="right">運費</td>
                              	<td colspan="2" align="right"><?php echo $order[0]['shipping_charge_amount'];?> &nbsp; </td>
                              </tr>
                              <tr>
                              	<td colspan="9" align="right">訂單總金額</td>
                              	<td colspan="2" align="right"><?php echo $order[0]['shipping_charge_amount']+$actual_sales_amount;?> &nbsp; </td>
                              </tr>
                              <tr>
                              	<td colspan="9" align="right">實付總額</td>
                              	<td colspan="2" align="right"><?php echo $order[0]['shipping_charge_amount']+$actual_sales_amount;?> &nbsp; </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <!-- End of portlet-body -->
                    </div>
                    <!-- End of portlet -->
                  </div>
                </div>
                <!-- portlet 購物清單 - 虛擬商品 -->
                <!-- portlet 購物清單 - 虛擬商品 -->
<?php if($if_return){?>
                <div class="row">
                  <div class="col-md-12 col-sm-12">
                    <div class="portlet grey-cascade box">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs"></i>退貨明細
                        </div>
                        <div class="actions">
                          <!--
                          <a href="javascript:;" class="btn btn-default btn-sm" data-toggle="modal" data-target="#vproductModal">
                            <i class="fa fa-plus"></i> 新增
                          </a>
                          -->
                        </div>
                      </div>
                      <div class="portlet-body">
                        <div class="table-responsive">
                          <table class="table table-hover table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>商品編號</th>
                                <th>商品名稱</th>
                                <th>庫號</th>
                                <th>規格</th>
                                <th>數量</th>
                                <th>售價</th>
                                <th>加購</th>
                                <th>折扣</th>
                                <th>囍市錢包</th>
                                <th>小計</th>
                                <th>收貨</th>
                                <th>退訂原因</th>
                                <!--th>功能</th-->
                              </tr>
                            </thead>
                            <tbody>
                              <?php $return_amount=0;if($order[0]['order_item']){ foreach($order[0]['order_item'] as $order_item){ if($order_item['order_cancel_sn']>0){?>
                                <tr>
                                  <td><a title="商品後台頁面" href="/admin/product/productItem/<?php echo $order[0]['channel_name']?>/<?php echo $order_item['product_sn'];?>" target="_blank"><?php echo $order_item['product_sn'];?></a></td>
                                  <td><a title="商品前台頁面" href="/shop/shopItem/<?php echo $order_item['product_sn'];?>" target="_blank"><?php echo $order_item['product_name'];?></a></td>
                                  <td><?php echo $order_item['unispec_warehouse_num'];?></td>
                                  <td><?php echo $order_item['product_package_name'];?><?php echo ($order_item['upgrade_discount_amount'])? '(升級)':'';?></td>
                                  <td align="center"><?php echo $order_item['buy_amount'];?></td>
                                  <td align="right">$<?php echo $order_item['sales_price'];?> &nbsp; </td>
                                  <td><?php echo $order_item['addon_flag']==1? '<i class="fa fa-check-square-o"></i>':'';?></td>
                                  <td align="right"><?php echo $order_item['coupon_code_cdiscount_amount'];?> &nbsp; </td>
                                  <td align="right"><?php echo $order_item['gift_cash_discount_amount'];?> &nbsp; </td>
                                  <td align="right"><?php echo $order_item['return_amount'];?> &nbsp; </td>
                                  <td align="center"><?php echo $order_item['receive_goods_flag']=='1'? '是':'否';?></td>
                                  <td align="left"><?php echo $order_item['order_cancel_reason_name'];?> &nbsp; </td>
                                </tr>
                              <?php $return_amount+=$order_item['return_amount'];}}}?>
                              <?php if($order[0]['free_shipping_flag']){?>
                              <tr>
                                <td colspan="8" align="right">運費</td>
                                <td colspan="2" align="right"><?php echo $order[0]['shipping_charge_amount'];?> &nbsp; </td>
                                <td colspan="2" align="right"> &nbsp; </td>
                              </tr>
                            <?php }?>
                              <tr>
                                <td colspan="8" align="right">退訂總額</td>
                                <td colspan="2" align="right"><?php echo $return_amount;?> &nbsp; </td>
                                <td colspan="2" align="right"> &nbsp; </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <!-- End of portlet-body -->
                    </div>
                    <!-- End of portlet -->
                  </div>
                </div>
              <?php }?>
                <!-- portlet 購物清單 - 虛擬商品 -->
                <!--div class="row">
                  <div class="col-md-6">
                  </div>
                  <div class="col-md-6">
                    <div class="well">
                      <div class="row static-info align-reverse">
                        <div class="col-md-8 name">總價:</div>
                        <div class="col-md-3 value">$1,800</div>
                      </div>
                      <div class="row static-info align-reverse">
                        <div class="col-md-8 name">運費:</div>
                        <div class="col-md-3 value">$70</div>
                      </div>
                      <div class="row static-info align-reverse">
                        <div class="col-md-8 name">折扣:</div>
                        <div class="col-md-3 value">$200 (單品折扣 - 差額設定)</div>
                      </div>
                      <div class="row static-info align-reverse">
                        <div class="col-md-8 name">小計:</div>
                        <div class="col-md-3 value">$1,600</div>
                      </div>
                    </div>
                  </div>
                </div-->

              </div>
              <!-- End of tab_detail -->
              <div class="tab-pane" id="tab_history">
                <div class="table-responsive">
                  <table class="table table-hover table-bordered table-striped" id="table1">
                    <thead>
                      <tr>
                        <th>更新時間</th>
                        <th>更新者</th>
                        <th>表單名稱</th>
                        <th>紀錄(修改前資料)</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php if($trans_log){foreach($trans_log as $_Item){?>
                      <tr>
                        <td><?php echo $_Item['trans_date']; ?></td>
                        <td><?php echo $_Item['last_name']; ?></td>
                        <td><?php echo $_Item['trans_table'] ?></td>
                        <td>
                          <a href="#inline<?php echo $_Item['trans_sn']; ?>" class="btn btn-xs btn-info various" data-id="<?php echo $_Item['trans_sn']; ?>" title="觀看修改前、後資料">觀看修改前、後資料</a>
                          <div style="display: none;">
                              <div id="inline<?php echo $_Item['trans_sn']; ?>" style="width:600px;height:100%;overflow:auto;">
                              <?php if(is_array(json_decode($_Item['beforetrans_column_set'],true))){
                                echo '<pre>修改前：<br>' . var_export(json_decode($_Item['beforetrans_column_set'],true), true) . '</pre>';
                              }else{
                                echo '<p>修改前：<br>'.$_Item['beforetrans_column_set'].'</p>';
                              }?>
                              <?php if(is_array(json_decode($_Item['after_trans_column_set'],true))){
                                echo '<pre>修改後：<br>' . var_export(json_decode($_Item['after_trans_column_set'],true), true) . '</pre>';
                              }else{
                                echo '<p>修改後：<br>'.$_Item['after_trans_column_set'].'</p>';
                              }?>
                              </div>
                          </div>
                          </td>
                      </tr>
                    <?php }}?>
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- End of tab_history -->
              <div class="tab-pane" id="tab_message">
                <div class="portlet box">
<form role="form" name="reply_form" method="post" action="admin/admin/addCallcenter">
                    <div class="row">
		                    <div class="form-group">
			                    <label class="col-md-3 control-label" style="text-align: right;">回覆內容</label>
				      						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7 form-group">
	                        	<textarea required name="reply" class="form-control" rows="3"></textarea>
                      		</div>
                      	</div>
		                    <div class="form-group">
		                      <label class="col-md-3 control-label" style="text-align: right;">問題狀態</label>
				      						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7 form-group">
				                      <select name="question_status" id="question_status" required class="form-control">
				                        <?php foreach($_question_status as $_Item){ $ifselected='';
				                        	if($_Item['member_question_status']==$question_qna['member_question'][0]['question_status']) $ifselected='selected';?>
				                        <option value="<?php echo $_Item['member_question_status']?>" <?php echo $ifselected;?> ><?php echo $_Item['member_question_status_name']?></option>
				                        <?php }?>
				                      </select>
				                  </div>
		                    </div>
		                    <div class="form-group">
		                      <label class="col-md-3 control-label" style="text-align: right;">結案方式</label>
				      						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7 form-group">
				                      <select name="cc_faq_close_code" id="cc_faq_close_code" class="form-control">
		                        		<option value="">請選擇</option>
				                        <?php foreach($_question_close_status as $_Item){ $ifselected='';
				                        	if($_Item['cc_question_close_code']==$question_qna['member_question'][0]['cc_faq_close_code']) $ifselected='selected';?>
				                        <option value="<?php echo $_Item['cc_question_close_code']?>" <?php echo $ifselected;?> ><?php echo $_Item['cc_question_close_name']?></option>
				                        <?php }?>
				                      </select>
				                  </div>
		                  </div>
                      <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9" style="text-align: right;">
                        <button type="submit" class="btn blue">送出</button>
        <input type="hidden" name="member_question_sn" value="<?php echo $question_qna['member_question'][0]['member_question_sn']; ?>">
                      </div>
                    </div>
                  </form>
                  <hr>
                  <div class="portlet-body" id="chats">
  <h4 class="modal-title"> 主題：<?php echo $question_qna['member_question'][0]['question_subject']?></h4>
                    <ul class="chats">
        <li class="<?php echo ($question_qna['member_question'][0]['member_sn']!=$question_qna['member_question'][0]['create_member_sn'])?'in':'out';?>">
          <img class="avatar" alt="" src="public/img/<?php echo ($question_qna['member_question'][0]['member_sn']!=$question_qna['member_question'][0]['create_member_sn'])?'icon-supportfemale-48':'icon-diamond-48';?>.png">
          <div class="message">
            <span class="arrow">
            </span>
            <a href="javascript:;" class="name">
            	<?php echo ($question_qna['member_question'][0]['member_sn']==$question_qna['member_question'][0]['create_member_sn'])? $question_qna['member_question'][0]['last_name']:$question_qna['member_question'][0]['member_name'];?>
          	</a>
            <span class="datetime">
            at <?php echo $question_qna['member_question'][0]['issue_date'];?> </span>
            <span class="body">
            	<?php echo nl2br($question_qna['member_question'][0]['question_description']);?>
            </span>
          </div>
        </li>
<?php	if(@$question_qna['qna_log']){foreach(@$question_qna['qna_log'] as $Item){?>
        <li class="<?php echo ($Item['cc_reply_flag'])?'in':'out';?>">
          <img class="avatar" alt="" src="public/img/<?php echo ($Item['cc_reply_flag'])?'icon-supportfemale-48':'icon-diamond-48';?>.png">
          <div class="message">
            <span class="arrow">
            </span>
            <a href="javascript:;" class="name">
            <?php echo $Item['process_ccagent_member_name'];?> </a>
            <span class="datetime">
            at <?php echo $Item['qna_date'];?> </span>
            <span class="body">
            	<?php echo nl2br($Item['qna_content']);?>
            </span>
          </div>
        </li>
<?php }}?>
                    </ul>
                      <!--a href="admin/admin/callcenterMsg/<?php echo $question_qna['member_question'][0]['member_question_sn']; ?>" data-remote="false" data-target="#<?php echo (@$question_qna['member_question'][0]['member_question_sn'])?'ajax_msg':'ticket_form';?>" class="addnew" data-toggle="modal"><i class="fa fa-edit">回覆</i></a-->
                  </div>
                </div>
              </div>
      <!-- Modal for 表單填寫 -->
     <div class="tab-pane" id="ticket_form">
       <form role="form" name="ticket_form" method="post" action="/admin/admin/addNewCallcenter">
              <h2 class="modal-title">訊息聯絡</h2>
                  <div class="col-lg-8 form-body">
                    <div class="form-group">
                      <label>問題主題<span class="require">*</span></label>
                      <div class="input-icon">
                        <i class="fa fa-question-circle"></i>
                        <input type="text" required name="question_subject" class="form-control" placeholder="請輸入您的問題主題">
                      </div>
                    </div>
                   <div class="form-group">
                      <label>訂單編號</label>
                      <div class="input-icon">
                        <i class="fa fa-file-o"></i>
                        <input type="text" name="associated_sub_order_sn" id="associated_sub_order_sn" value="<?php echo $order[0]['sub_order_sn']?>" readonly class="form-control">
                      </div>
                    </div>
                    <div class="form-group">
                      <label>問題大類</label>
                      <select name="ccapply_code" id="ccapply_code" required class="form-control">
                        <option value="">請選擇</option>
                        <?php foreach($ccapply as $_Item){?>
                        <option <?php echo ($_Item['ccapply_name']=='訂單問題')?'selected':'';?> value="<?php echo $_Item['ccapply_code']?>"><?php echo $_Item['ccapply_name']?></option>
                        <?php }?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>問題類型</label>
                      <select name="ccapply_detail_code" id="ccapply_detail_code" required class="form-control">
                        <option value="">請先選擇大類</option>
                      </select>
                    </div>
                    <!-- <div class="form-group">
                      <label>附加檔案</label>
                      <input type="file" id="exampleInputFile1">
                      <p class="help-block">
                         ※ 檔案大小2MB為限，附件格式僅接受jpeg、png。
                      </p>
                    </div>-->
                    <div class="form-group">
                      <label>問題說明 <span class="require">*</span></label>
                      <div class="input-icon">
                        <textarea required name="question_description" class="form-control" rows="5"></textarea>
                      </div>
                    </div>
            <div class="form-group">
              <input type="hidden" name="member_sn" id="member_sn" value="<?php echo $order[0]['member_sn']?>">
              <button type="submit" id="ticket_submit" class="btn blue">送出表單</button>
            </div>
        </div>
       </form>
        <!-- /.modal-dialog -->
      </div>
      <!-- Modal for 表單填寫 -->
              <!-- End of tab_message -->
            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->

<!-- Modal for 訂單狀態變更 -->
<div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">訂單狀態編輯</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-without-legend" role="form" method="post">
          <fieldset>
<?php if($this->session->userdata('member_role_system_rules')[0]['role_name']!='經銷商管理員' && $this->session->userdata('member_role_system_rules')[0]['role_name']!='供應商管理員'){?>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">訂單客服</label>
              <div class="col-md-9">
                <select class="form-control" name="sub_order[order_process_member_sn]">
		              <?php foreach($admin_users as $_Item){?>
		                <option <?php echo ($order[0]['order_process_member_sn']==$_Item['member_sn'])?'selected':'';?> value="<?php echo $_Item['member_sn']?>"><?php echo $_Item['last_name'];?></option>
		              <?php }?>
                </select>
              </div>
            </div>
            <?/*<div class="form-group">
              <label for="password" class="col-lg-3 control-label">交易付款狀態</label>
              <div class="col-md-9">
                <select class="form-control" name="order2[payment_status]">
										<?php
			                 foreach($payment_status as $keys=>$values) { $ifselectyed=($order[0]['payment_status']==$keys)?'selected':'';
			    							echo  ' <option '.$ifselectyed.' value="'.$keys.'">'.$values.'</option>';
											}?>
                </select>
              </div>
            </div>*/?>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">訂單付款狀態</label>
              <div class="col-md-9">
                <select class="form-control" name="sub_order[pay_status]">
                    <?php reset($payment_status);
                       foreach($payment_status as $keys=>$values) { $ifselectyed2=($order[0]['pay_status']==$keys)?'selected':'';
                        echo  ' <option '.$ifselectyed2.' value="'.$keys.'">'.$values.'</option>';
                      }?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">訂單狀態</label>
              <div class="col-md-9">
                <select class="form-control" name="sub_order[sub_order_status]">
										<?php
			                 foreach ($sub_order_status as $keys=>$values) { $ifselectyed=($order[0]['sub_order_status']==$keys)?'selected':'';
			    							echo  ' <option '.$ifselectyed.' value="'.$keys.'">'.$values.'</option>';
											}?>
                </select>
              </div>
            </div>
<?}?>
<?php if($this->session->userdata('member_role_system_rules')[0]['role_name']!='經銷商管理員'){?>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">配送狀態</label>
              <div class="col-md-9">
                <select class="form-control" name="sub_order[delivery_status]">
                    <option value="">請選擇</option>
										<?php
			                 foreach($delivery_status as $keys=>$values) { $ifselectyed=($order[0]['delivery_status']==$keys)?'selected':'';
			    							echo  ' <option '.$ifselectyed.' value="'.$keys.'">'.$values.'</option>';
											}?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">配送單號</label>
              <div class="col-md-9">
                <input type="text" name="sub_order[logistics_ticket_num]" class="form-control" value="<?php echo $order[0]["logistics_ticket_num"];?>">
              </div>
            </div>

<?}?>
            <!--div class="form-group">
              <label for="password" class="col-lg-3 control-label">差額設定</label>
              <div class="col-md-9">
                <input type="text" class="form-control" value="100">
              </div>
            </div-->
<?php if($this->session->userdata('member_role_system_rules')[0]['role_name']!='經銷商管理員' && $this->session->userdata('member_role_system_rules')[0]['role_name']!='供應商管理員'){?>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">備註</label>
              <div class="col-md-9">
                <textarea class="form-control" name="sub_order[memo]" rows="3"><?php echo $order[0]['memo'];?></textarea>
              </div>
            </div>
<?}?>
          </fieldset>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">變更</button>
      </div>
      <input type="hidden" name="order_sn" value="<?php echo @$order[0]["order_sn"];?>">
      <input type="hidden" name="sub_order_sn" value="<?php echo @$order[0]["sub_order_sn"];?>">
      <input type="hidden" name="old_data[order_process_member_name]" value="<?php echo $order[0]['last_name']?>">
      <input type="hidden" name="old_data[sub_order_status_name]" value="<?php echo $order[0]['sub_order_status_name']?>">
      <input type="hidden" name="old_data[delivery_status_name]" value="<?php echo $order[0]['delivery_status_name']?>">
      <input type="hidden" name="old_data[logistics_ticket_num]" value="<?php echo $order[0]['logistics_ticket_num']?>">
      <input type="hidden" name="old_data[memo]" value="<?php echo @$order[0]["memo"];?>">
        </form>
    </div>
  </div>
</div>


<!-- Modal for 收件人資料變更 -->
<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="userModalLabel">收件人資料編輯</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-without-legend" role="form" method="post">
          <fieldset>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">姓名</label>
              <div class="col-md-9">
                <input type="text" required id="name1" name="order[receiver_last_name]" class="form-control" value="<?php echo @$order[0]["receiver_last_name"];?>">
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">手機號碼</label>
              <div class="col-md-9">
                <input type="text" required id="cell1" name="order[receiver_cell]" class="form-control" value="<?php echo @$order[0]["receiver_cell"];?>">
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-3 control-label">市話</label>
              <div class="col-md-9">
                <!--div class="form-inline">
                  <div class="form-group">
                    <input type="text" id="tel_area_code1" class="form-control" name="order[receiver_tel_area_code]" size="4" maxlength="3" placeholder="區碼" value="<?php echo @$order[0]["receiver_tel_area_code"];?>">
                  </div>
                  <div class="form-group"-->
                    <input type="text" id="tel1" class="form-control" name="order[receiver_tel]" maxlength="20" placeholder="請輸入市話號碼" value="<?php echo @$order[0]["receiver_tel"];?>">
                  <!--/div>
                  <div class="form-group">
                    <input type="text" id="tel_ext1" class="form-control" name="order[receiver_tel_ext]" size="4" placeholder="分機" value="<?php echo @$order[0]["receiver_tel_ext"];?>">
                  </div>
                </div-->
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">郵遞區號</label>
              <div class="col-md-9">
                    <input type="text" required id="zipcode" name="order[receiver_zipcode]" class="form-control" size="8" maxlength="5" placeholder="郵遞區號" value="<?php echo @$order[0]["receiver_zipcode"];?>">
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">縣市</label>
              <div class="col-md-9">
                    <select required class="form-control input-sm" id="addr_city_code" name="order[receiver_addr_city]">
													<?php //var_dump($city_ct);
						                 while (list($keys, $values) = each ($city_ct)) {
						    							echo  ' <option value="'.$keys.'">'.$values.'</option>';
														}?>
                    </select>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">鄉鎮區</label>
              <div class="col-md-9">
                    <select required class="form-control input-sm" id="addr_town_code" name="order[receiver_addr_town]">
                      <!--option value="" disable> 請選擇 </option-->
                    </select>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">地址</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="order[receiver_addr1]" value="<?php echo @$order[0]["receiver_addr1"];?>">
              </div>
            </div>
          </fieldset>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">變更</button>
      </div>
      <input type="hidden" name="order_sn" value="<?php echo @$order[0]["order_sn"];?>">
      <input type="hidden" name="old_data_order[receiver_last_name]" value="<?php echo @$order[0]["receiver_last_name"];?>">
      <input type="hidden" name="old_data_order[address]" value="<?php echo $order[0]['receiver_addr_city']?><?php echo $order[0]['receiver_addr_town']?> <?php echo $order[0]['receiver_zipcode']?> <?php echo $order[0]['receiver_addr1']?>">
      <input type="hidden" name="old_data_order[receiver_cell]" value="<?php echo @$order[0]["receiver_cell"];?>">
      <input type="hidden" name="old_data_order[receiver_phone]" value="<?php echo $order[0]['receiver_tel_area_code']?>-<?php echo $order[0]['receiver_tel']?><?php echo (@$order[0]["receiver_tel_ext"])?' #'.$order[0]["receiver_tel_ext"]:'';?>">
        </form>
    </div>
  </div>
</div>



<!-- Modal for 虛擬商品資訊變更 -->
<div class="modal fade" id="vproductModal" tabindex="-1" role="dialog" aria-labelledby="vproductModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="vproductModalLabel">虛擬商品資訊編輯</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-without-legend" role="form" method="post">
          <fieldset>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">商品名稱</label>
              <div class="col-md-9">
                <span class="form-control-static">旗艦型 Yes 版</span>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">數量</label>
              <div class="col-md-9">
                <input type="text" class="form-control" value="2">
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">折扣</label>
              <div class="col-md-9">
                <input type="text" class="form-control" value="100">
              </div>
            </div>
          </fieldset>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">變更</button>
      </div>
        </form>
    </div>
  </div>
</div>



<!-- Modal for 實體商品資訊變更 -->
<div class="modal fade" id="pproductModal" tabindex="-1" role="dialog" aria-labelledby="pproductModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="pproductModalLabel">實體商品資訊編輯</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-without-legend" role="form">
          <fieldset>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">商品名稱</label>
              <div class="col-md-9">
                <span class="form-control-static">開心肥皂泡</span>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">數量</label>
              <div class="col-md-9">
                <input type="text" class="form-control" value="1">
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">折扣</label>
              <div class="col-md-9">
                <input type="text" class="form-control" value="100">
              </div>
            </div>
          </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">變更</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal 顯示與客服的訊息列表 -->
<div class="modal fade" id="ajax_msg" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">訊息記錄</h4>
      </div>
      <div class="modal-body">
        <img src="public/metronic/global/img/loading-spinner-grey.gif" alt="" class="loading">
        <span>
        &nbsp;&nbsp;內容載入中... </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn blue">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal for 發票資料變更 -->
<div class="modal fade" id="invoiceModal" tabindex="-1" role="dialog" aria-labelledby="invoiceModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="vproductModalLabel">發票資料編輯</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-without-legend" role="form" method="post">
          <fieldset>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">發票號碼</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="invoice[invoice_num]" value="<?php echo $order[0]['invoice_num']?>" <?=($order[0]['invoice_receiver_middle_name'])?'disabled':''?>>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">發票日期</label>
              <div class="col-md-9">
                <input type="date" class="form-control" name="invoice[invoice_time]" value="<?php echo substr($order[0]['invoice_time'],0,10)?>" <?=($order[0]['invoice_receiver_middle_name'])?'disabled':''?>>
              </div>
            </div>
<?/*
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">發票格式</label>
              <div class="col-md-9">
                <select required class="form-control input-sm" name="invoice[invoice_formats]" id="invoice_format" <?=($order[0]['invoice_num'])?'':''?>>
                  <option value="0" <?php if($open_invoice_type=='0') echo 'selected="selected"';?>>空白</option>
                  <option value="2" <?php if($open_invoice_type=='2') echo 'selected="selected"';?>>二聯</option>
                  <option value="3" <?php if($open_invoice_type=='3') echo 'selected="selected"';?>>三聯</option>
                </select>
              </div>
            </div>*/?>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">發票抬頭</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="invoice[invoice]" id="invoice" value="<?php echo $order[0]['invoice_title']?>" <?=($order[0]['invoice_receiver_middle_name'])?'disabled':''?>>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">發票統編</label>
              <div class="col-md-9">
                <input type="text" maxlength="8" class="form-control" name="invoice[tax_ID]" id="tax_ID" value="<?php echo $order[0]['company_tax_id']?>" <?=($order[0]['invoice_receiver_middle_name'])?'disabled':''?>>
              </div>
            </div>
             <div class="form-group">
              <label for="password" class="col-lg-3 control-label">郵遞區號</label>
              <div class="col-md-9">
                 <input type="text" required id="invoice_zipcode" name="invoice[invoice_receiver_zipcode]" class="form-control" size="8" maxlength="5" placeholder="郵遞區號" value="<?php if(@$order[0]["invoice_receiver_zipcode"]) echo $order[0]["invoice_receiver_zipcode"]; else echo @$order[0]["receiver_zipcode"];?>">
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">縣市</label>
              <div class="col-md-9">
                <select required class="form-control input-sm" name="invoice[invoice_receiver_addr_city]" id="invoice_addr_city_code">
                  <?php reset($city_ct);
                    while (list($keys, $values) = each ($city_ct)) {
                      $_selected='';
                      if($invoice_receiver_addr_city_code==$keys) $_selected='selected';
                      echo  ' <option value="'.$keys.'" '.$_selected.'>'.$values.'</option>';
                  }?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">鄉鎮區</label>
              <div class="col-md-9">
                <select required class="form-control input-sm" name="invoice[invoice_receiver_addr_town]" id="invoice_addr_town_code">
                    <?php reset($town_ct);
                    while (list($keys, $values) = each ($town_ct)) {
                      $_selected='';
                      if($invoice_receiver_addr_town_code==$keys) $_selected='selected';
                      echo  ' <option value="'.$values.'" '.$_selected.'>'.$values.'</option>';
                    }?>
                  <!--option value="" disable> 請選擇 </option-->
                </select>
                <??>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">發票地址</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="invoice[invoice_receiver_addr1]" value="<?php if(@$order[0]['invoice_receiver_addr1']) echo $order[0]['invoice_receiver_addr1']; else echo @$order[0]["buyer_addr1"];?>">
              </div>
            </div>
<?/*
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">發票處理</label>
              <div class="col-md-9">
               <select name="invoice[invoice_carrie]" class="form-control input-sm invoice_carrie" <?=($order[0]['invoice_num'])?'':''?>>
                <?php
                  while (list($keys, $values) = each ($_invoice_way)) {
                    echo '<option value="'.$keys.'"';
                    if($Invoice_carrie_type==$keys)
                      echo ' selected="selected"';
                    echo '>'.$values.'</option>';
                  }
                ?>
              </select>
              </div>
            </div>
             <div class="form-group" id="email_for_eletronic_invoce">
              <label for="password" class="col-lg-3 control-label">載具編號</label>
              <div class="col-md-9">
                 <input type="text" name="invoice[email_for_eletronic_invoce]" class="form-control" size="16" maxlength="16" placeholder="載具編號" value="<?php echo $order[0]["email_for_eletronic_invoce"];?>" <?=($order[0]['invoice_num'])?'':''?>>
              </div>
            </div>
            <div class="form-group" id="invoice_donor_ct">
              <label class="col-lg-3 control-label">捐贈單位</label>
              <div class="col-md-9">
               <select name="invoice[invoice_donor_ct]" class="form-control input-sm invoice_carrie" <?=($order[0]['invoice_num'])?'':''?>>
                <option value="">請選擇</option>
                <?php
                  while (list($keys, $values) = each ($invoice_donor_ct)) {
                    echo '<option value="'.$keys.'"';
                    if($order[0]['invoice_donor']==$keys)
                      echo ' selected="selected"';
                    echo '>'.$values.'</option>';
                  }
                ?>
              </select>
              </div>
            </div>*/?>
            <div class="help-block text-center">P.S.1.如已有發票號碼，部份欄位無法異動。</div>
            <div class="help-block text-center">P.S.2.發票抬頭、發票統編清空則為二聯式，反之則為三聯式。</div>
          </fieldset>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">變更</button>
      </div>
      <input type="hidden" name="invoice[order_num]" value="<?php echo $order[0]['order_num'];?>">
      <input type="hidden" name="invoice[invoice_receiver_middle_name]" value="<?php echo @$order[0]['invoice_receiver_middle_name']?>">
        </form>
    </div>
  </div>
</div>