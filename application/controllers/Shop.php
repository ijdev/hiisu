<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Shop extends CI_Controller {
	 public function __construct()
		{
			 parent::__construct();
				//$this->load->library('session');
				$this->load->helper('form');
				$this->load->helper('url');
		    	//$this->load->helper('cookie');
				$this->load->library('form_validation');
		    	$this->load->library("ijw");
				//$this->load->library('cart');
				//$this->load->library('sean_lib/Sean_form_general');
				$this->load->model('sean_models/Sean_general');
				$this->load->model('sean_models/Sean_db_tools');
				$this->load->model('Shop_model');
				$this->load->config('ij_config');
				$this->load->model('libraries_model');

				$this->page_data = array();
				//$this->page_data['product_history']=$this->Shop_model->_get_product(get_cookie('product_history[]'),'ij_product.product_sn,product_name,image_path,image_alt','1'); //瀏覽紀錄
				$this->page_data['cart']=$this->Shop_model->Get_Cart(); //購物車
				if(!$this->session->userdata('category_array')){
					$_category_array=$this->Shop_model->categoryParentChildTree('囍市集',0,'','',''); //分類
					$this->page_data['_category_array']=$this->Shop_model->get_banner($_category_array); //分類抓圖
        			//echo '<pre>' . var_export($this->page_data['_category_array'], true) . '</pre>';exit;
					$this->session->set_userdata('category_array',$this->page_data['_category_array']);
				}else{
					$this->page_data['_category_array']=$this->session->userdata('category_array');
				}
				$this->page_data['unreadmessage']= $this->Shop_model->get_unread_message_count();
				$this->page_data['init_control']="www/";
				$this->page_data['_home_url']="www/";
				$this->init_control=$this->page_data['init_control'];
				if(!$this->session->userdata('general_header')){
					$this->page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
					$where=array('upper_category_sn'=>'0','category_status'=>'1','channel_name'=>'囍市集');
					$this->page_data['general_header']['root_category']=$this->Shop_model->_select('ij_category',$where,0,0,10,0,0,'result_array');
					$this->page_data['general_header']['content_types']=$this->libraries_model->_get_blog_faq_categorys(1,'囍市集');
					$this->page_data['general_header']['left_up']=$this->Shop_model->get_adsMaterials('首頁-跑馬燈文字');
					//$this->page_data['general_header']['up_left_banner']=$this->Shop_model->get_adsMaterials('首頁-左小方格');
					$this->page_data['general_header']['up_left_banner']=array();
					$this->page_data['general_header']['up_right_banner']=$this->Shop_model->get_adsMaterials('首頁-右小方格-右側');
					$this->page_data['general_header']['up_banner']=$this->Shop_model->get_adsMaterials('首頁_寬版長方格_H1_右側');
					$this->page_data['general_header']['hot_search']=$this->Shop_model->get_adsMaterials('首頁-熱門關鍵字',8)['banners'];
					$this->page_data['general_header']['top_right_word']=$this->Shop_model->get_adsMaterials('首頁_文字_右上',1)['banners'];
					$this->session->set_userdata('general_header',$this->page_data['general_header']);
				}else{
					$this->page_data['general_header']=$this->session->userdata('general_header');
				}
				/*$this->page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$this->page_data['page_content']['view_path'] = 'admin-ijwedding/page_home/page_content'; // 頁面主內容 view 位置, 必要
				$this->page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$this->page_data['page_level_js']['view_path'] = 'admin-ijwedding/page_home/page_content_js'; // 頁面主內容的 JS view 位置, 必要
				//$this->lang->load('general', 'zh-TW');
				//$this->page_data["_per_page"]=2;*/
				$data['Mes'] = $this->session->userdata('Mes');
				if($data['Mes']){$this->load->view('Message',$data);$this->session->unset_userdata('Mes');}
		}

	/**
	 * ==============   SHOP TEMPLATE AREA     ==============
	 */

	/**
	 * 線上商店首頁 Template
	 *
	 */
	public function index()
	{
		$page_data = array();

		// 內容設定
		//echo $this->Shop_model->_get_profit_sharing_rate(36);
		// page level setting
		$page_data['slider'] = 'www/page_shop/slider'; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，非必要

    	$_where=" where root_category_flag='3' and category_status='1' ";

		$_category_array=$this->Sean_db_tools->db_get_max_record("*","ij_category",$_where);
	    $page_data['_category_array']=$_category_array;
		// general view setting
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		//排序前10有圖片分類
		$where = array('category_status' => '1','main_picture_save_dir <>' => '','publish_flag'=>'1');
		$promo_category=$this->Shop_model->_select('ij_category',$where,0,0,0,0,0,'result_array');
		foreach ($promo_category as $_key => $_value) {
			$promo_category[$_key]['category_attributes']=$this->Shop_model->category_attributes($_value['category_sn'],0,1);
			//var_dump($promo_category[$_key]['category_attributes']);
		}
		$page_data['page_content']['promo_category']=$promo_category;

		//推薦商品
		$page_data['page_content']['promo_product1']=$this->Shop_model->_get_products('囍市集','1',0,10);
		//熱賣商品
		$page_data['page_content']['promo_product2']=$this->Shop_model->_get_products('囍市集','2');
		//特價商品
		$page_data['page_content']['promo_product3']=$this->Shop_model->_get_products('囍市集','3');
		//var_dump($page_data['page_content']['promo_product1']);
		//獨家商品
		$page_data['page_content']['promo_product4']=$this->Shop_model->_get_products('囍市集','4');
		$page_data['page_content']['new_product']=$this->Shop_model->_get_products('囍市集',0,0,8,0,1,0,0,0,0,7);//七天內更新商品
		$page_data['page_content']['new_product2']=$this->Shop_model->_get_products('囍市集',0,0,8,2,1,0,0,0,0,7);//七天內更新商品
		$page_data['page_content']['sale_banners']=$this->Shop_model->get_adsMaterials('囍市集首頁的優惠BANNER');
		$page_data['sliders']['sliders']=$this->Shop_model->get_adsMaterials('首頁-輪播圖');
		$page_data['page_content']['wide_banner']=$this->Shop_model->get_adsMaterials('首頁-置中廣告');
		$page_data['page_content']['bottoms']=$this->Shop_model->get_adsMaterials('首頁-頁末四格圖',4);
		$page_data['page_content']['slogan']=$this->Shop_model->get_adsMaterials('首頁-標語圖',1);
		$page_data['page_content']['news']=$this->libraries_model->_get_blog_faq('ij_blog_article',array('status'=>'1','post_flag'=>'1','ij_blog_type_relation.content_type_sn'=>'14','ij_blog_type_relation.relation_status'=>'1'),0,0,0,0,5);
		$page_data['page_content']['new_blogs']=$this->libraries_model->_get_blog_faq('ij_blog_article',array('status'=>'1','post_flag'=>'1','ij_blog_type_relation.content_type_sn !='=>'14','ij_blog_type_relation.relation_status'=>'1'),0,0,0,0,4);
		//var_dump($page_data['page_content']['bottoms']);
		$page_data['page_content']['partner']=$this->Shop_model->get_adsMaterials('首頁-合作夥伴');
		/*$page_data['page_content']['doc1']=$this->Shop_model->_select('ij_system_file_config','system_file_name','首頁文案1',0,0,0,0,'row');
		$page_data['page_content']['doc2']=$this->Shop_model->_select('ij_system_file_config','system_file_name','首頁文案2',0,0,0,0,'row');
		$page_data['page_content']['doc3']=$this->Shop_model->_select('ij_system_file_config','system_file_name','首頁文案3',0,0,0,0,'row');
		$page_data['page_content']['doc4']=$this->Shop_model->_select('ij_system_file_config','system_file_name','首頁文案4',0,0,0,0,'row');*/
		$page_data['description']='囍市集為您嚴選特色婚禮小物、設計喜帖、訂婚喜餅、喜糖蛋糕、結婚禮品、婚禮用品、訂結婚禮俗用品….等，讓準新人們可輕鬆線上採購婚禮所需，並實現您的夢想婚禮！';
		//var_dump($page_data['page_content']['news']);
		//echo count($page_data['page_content']['promo_category']);
		$page_data['page_content']['view_path'] = 'www/page_shop/page_home'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_shop/page_home_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 * 線上商店分類頁 Template
	 *
	 */
	public function shopCatalog($_id="")
	{
		$this->ijw->get_partner_sn();
		//var_dump($this->session->userdata['web_member_data']['associated_partner_sn']);
		//var_dump($this->session->userdata['partner_sn']);
		if($_id){
		  	//當前館別的下層分類
			$_down_category_array=$this->Shop_model->categoryParentChildTree('囍市集',$_id,'','',''); //分類
		  	$page_data['down_category_array']=$_down_category_array;

			$sort=$this->input->get('sort',true);
			//$limit=$this->input->get('limit',true);explode
			$attribute_values=$this->input->get('attribute_value_sn[]',true);
			$pricerange=$this->input->get('pricerange',true);
			$pricerange_array=explode('-',str_replace('$','',$pricerange));
			if (count($pricerange_array)==2){
				$min_price=$pricerange_array[0];
				$max_price=$pricerange_array[1];
			}else{
				$products=$this->Shop_model->_get_products('囍市集',0,$_id,0,0,'price_ASC',0,0,0,0,0,array_column($_down_category_array, 'category_sn'));
				if($products){
					$min_price=($products[0]['min_price'])? $products[0]['min_price']:'0';
					$max_price=($products[count($products)-1]['min_price'])? $products[count($products)-1]['min_price']:'0';
				}else{
					//$this->session->set_userdata(array('Mes'=>'很抱歉!該分類尚無開放商品!'));
					show_404();
					//$this->ijw->_wait(base_url("shop") , 2 , '很抱歉!該分類尚無開放商品!',1);
				    //redirect(base_url('shop'));
				}
			}
			//echo $min_price;
			//echo $max_price;
			$page=$this->input->get('page',true);
			$page_data = array();
			// 內容設定
			$page_data['_key_id']=$_id;
			// page level setting
			// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設
			$page_data['title_wrapper'] = 'www/page_shop/title_wrapper'; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，非必要

			$_where=" where channel_name='囍市集' and category_status='1' order by sort_order asc";
			$_category_array=$this->Sean_db_tools->db_get_max_record("*","ij_category",$_where);
		  $page_data['_category_array']=$_category_array;

		  $_where=" where upper_category_sn='".$_id."' or category_sn='".$_id."' order by sort_order";
		  $_category_array=$this->Sean_db_tools->db_get_max_record("*","ij_category",$_where);
		  $page_data['_category_array_left']=$_category_array;
		  //找出館別分類編號
			/*if($page_data['_category_array_left'][0]->upper_category_sn=='0'){
				$top_cate_id=$page_data['_category_array_left'][0]->category_sn;
			}else{
				$top_cate_id=$page_data['_category_array_left'][0]->upper_category_sn;
				foreach($page_data['_category_array'] as $key => $value){
	        		if($value->category_sn == $top_cate_id && $value->category_status==1 && $value->upper_category_sn){
			       		$top_cate_id=$value->upper_category_sn;
			     	}
	        	}
			}
			//var_dump($_2_name_parent);
		  $page_data['top_cate_id']=$top_cate_id;*/
		  //當前分類
		  $_where=" where category_sn='".$_id."'";
		  $_category_array=$this->Sean_db_tools->db_get_max_record("*","ij_category",$_where);
		  //var_dump($_category_array);
		  $page_data['main_picture_save_dir']=$_category_array[0]->main_picture_save_dir;
		  $page_data['banner_desc']=$_category_array[0]->banner_desc;
		  $page_data['banner_save_dir']=$_category_array[0]->banner_save_dir;
		  $page_data['title']=($_category_array[0]->meta_title)?$_category_array[0]->meta_title:$_category_array[0]->category_name;
		  $page_data['title'].=' | HiiSU 囍市集 | 婚禮購物商城';
		  //$page_data['keywords']=$_category_array[0]->meta_keywords;
		  $page_data['description']=$_category_array[0]->meta_keywords.$_category_array[0]->meta_content;
		  //var_dump($_category_array[0]->meta_title);
			// general view setting
			$page_data['general_header'] = array(); // header 可以透過這個參數帶入
			$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
			$page_data['page_content']['category_attributes'] = $this->Shop_model->category_attributes($_id,$attribute_values);
			//推薦商品
			$page_data['page_content']['promo_product1']=$this->Shop_model->_get_products('囍市集','1',$_id,4,0,0,0,0,0,0,0,array_column($_down_category_array, 'category_sn'));
			//echo count($page_data['page_content']['promo_product1']);
			//熱賣商品
			$page_data['page_content']['promo_product2']=$this->Shop_model->_get_products('囍市集','2',$_id,5,0,0,0,0,0,0,0,array_column($_down_category_array, 'category_sn'));
			//特價商品
			//$page_data['page_content']['promo_product3']=$this->Shop_model->_get_products('囍市集','3',$_id,5);
			//熱門商品
			//$page_data['page_content']['promo_product5']=$this->Shop_model->_get_products('囍市集','5',$_id,5);

			//該分類商品
	    	$this->load->library('pagination');
			$page_data['page_content']['perpage'] = 32;
			$page_data['page_content']['total'] = count($this->Shop_model->_get_products('囍市集',0,$_id,0,0,0,$attribute_values,@$min_price,@$max_price,0,0,array_column($_down_category_array, 'category_sn')));
		    $config = $this->ijw->_pagination('shop/shopCatalog/'.$_id, $page_data['page_content']['total'],$page_data['page_content']['perpage'],'shop');
		    $this->pagination->initialize($config);

	    	$page_data['page_content']['page_links'] = $this->pagination->create_links();

			$page_data['page_content']['products']=$this->Shop_model->_get_products('囍市集',0,$_id,$page_data['page_content']['perpage'],$page,$sort,$attribute_values,@$min_price,@$max_price,0,0,array_column($_down_category_array, 'category_sn'));
			$page_data['page_content']['sort']=$sort;
			$page_data['page_content']['min_price']=@$min_price;
			$page_data['page_content']['max_price']=@$max_price;
			//分類文章
			$page_data['page_content']['blogs']=$this->libraries_model->_get_blog_faq('ij_blog_article',array('status'=>'1','post_flag'=>'1'),'post_category_sn_set',$_category_array[0]->category_name,0,5);
			//echo $this->db->last_query();

			$page_data['page_content']['view_path'] = 'www/page_shop/page_catalog'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'www/page_shop/page_catalog_js'; // 頁面主內容的 JS view 位置, 必要
			$this->load->view('www/general/main', $page_data);
		}else{
			//$this->session->set_userdata(array('Mes'=>'請選擇分類!'));
			$this->ijw->_wait(base_url("shop") , 2 , '請選擇分類!');
			//redirect(base_url('shop'));
		}
	}








	/**
	 * 線上商店商品頁 Template
	 *
	 */
	public function shopItem($product_sn=0,$category_sn=0)
	{
			//var_dump($this->Shop_model->_get_product($product_sn));
			//var_dump($this->session->userdata['web_member_data']);
			//exit();
		//$startMemory = memory_get_usage();
        //ini_set("memory_limit","2048M");
		//$this->output->enable_profiler(TRUE);
		$this->ijw->get_partner_sn();

		if($product_sn && $product=$this->Shop_model->_get_product($product_sn)){
			//商品資料
			$page_data = array();
			$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
			//unset($product['images']);
			//$product['images']=array();
			//$product['open_preorder_flag']='0';
			$page_data['page_content']['product']=$product;
        	//echo '<pre>' . var_export($product, true) . '</pre>';exit;
		    //$this->load->helper('cookie');
			//$this->ijw->_write_product_history($product_sn);
			$this->Shop_model->_update_field_amount('ij_product','product_sn',$product_sn,'click_number','+1');
			/*if($product['default_channel_name']=='婚禮網站'){
				redirect(base_url('home/eweddingItem/'.$product_sn));
			}*/
			//echo count(get_cookie('product_history[]'));

			// 內容設定

			// page level setting
			// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設

			$page_data['_key_id']=$product['default_root_category_sn']; //商品主分類

			$_where=" where channel_name='".$product['default_channel_name']."' ";
			$_category_array=$this->Sean_db_tools->db_get_max_record("*","ij_category",$_where);
		    $page_data['_category_array']=$_category_array;

		    $_where=" where upper_category_sn='".$page_data['_key_id']."' or category_sn='".$page_data['_key_id']."' ";
		    $_category_array=$this->Sean_db_tools->db_get_max_record("*","ij_category",$_where);
		    $page_data['_category_array_left']=$_category_array;
		    $page_data['title']=$product['product_name'].' | '.$product['category_name'].' | 囍市集';
		    //$page_data['keywords']=$product['meta_keywords'];
		    $page_data['description']=$product['meta_keywords'].$product['special_item'];
		    $page_data['system_words']=$this->libraries_model->_select('ij_system_file_config','system_file_name','退換貨須知',0,0,0,0,'row')['system_file_ontent'];

			// general view setting
			$page_data['general_header'] = array(); // header 可以透過這個參數帶入
			$page_data['page_content']['view_path'] = 'www/page_shop/page_item'; // 頁面主內容 view 位置, 必要
		    $page_data['page_content']['top_ad']=$this->Shop_model->get_adsMaterials('商品頁-置頂廣告');
	        $this->load->library('user_agent');
            if($this->agent->is_mobile()){
                $page_data['page_content']['line_share']='line://msg/text/'.urlencode($product['product_name']).' '.base_url('shop/shopItem/'.$product_sn);
            }else{
                $page_data['page_content']['line_share']='https://lineit.line.me/share/ui?url='.base_url('shop/shopItem/'.$product_sn).'&text='.urlencode($product['product_name']);
            }
			//熱賣商品
			//$page_data['page_content']['promo_product2']=$this->Shop_model->_get_products($product['default_channel_name'],'2',$page_data['_key_id'],5);
			//特價商品
			//$page_data['page_content']['promo_product3']=$this->Shop_model->_get_products($product['default_channel_name'],'3',$page_data['_key_id'],5);
			//熱門商品
			//$page_data['page_content']['promo_product5']=$this->Shop_model->_get_products($product['default_channel_name'],'5',$page_data['_key_id'],5);
			//相關商品(同分類隨機)
			$page_data['page_content']['promo_product6']=$this->Shop_model->_get_products($product['default_channel_name'],'6',$page_data['_key_id'],12);
			$page_data['page_content']['category_sn']=$category_sn;

			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'www/page_shop/page_item_js'; // 頁面主內容的 JS view 位置, 必要
			//var_dump($page_data);exit;
		//echo memory_get_usage() - $startMemory, ' bytes';exit;
			$this->load->view('www/general/main', $page_data);
		}else{
			show_404();
			//$this->session->set_userdata(array('Mes'=>'很抱歉!查無此商品，可能下架中，或尚未開賣!'));
		  	//redirect(base_url('shop'));
			//$this->ijw->_wait(base_url("shop") , 2 , '很抱歉!查無此商品，可能下架中，或尚未開賣!',1);
		}
	}

	/**
	 * 線上商店商品清單 Template
	 *
	 */
	public function shopView()
	{
		//var_dump($this->session->userdata('method_shipping'));
		$this->LoginCheck(1,base_url('shop/shopView'));
		$page_data = array();
		// 內容設定
		// page level setting
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$upgrade_discount_amount='';
		//var_dump($this->Shop_model->Get_Cart_Checkout());
		//var_dump($this->Shop_model->get_temp_order($this->session->userdata['web_member_data']['member_sn']));
		$last_supplier='';
		$supplier_cart=array();
	    $shipping_charge_amount=0;
	    $coupon_code_total_cdiscount_amount=0;
	    //var_dump($this->cart->total_items()).'<br>';
		if($cart=$this->cart->contents()){
			usort($cart, array($this,'sort_by_supplier_name'));
			//usort($cart, function ($a, $b) { return strcmp($a["time"], $b["time"]); });
			foreach($cart as $key=>$Item){
				//var_dump($key);
				//$cart[$key]['image'] = $this->Shop_model->_select('ij_product_gallery',array("product_sn" => $Item['id']),1,0,0,0,0,'row','image_path,image_alt');
	    		if(strpos($Item['id'],'_')!==false){ // 過濾_
	    			$Item['id']=substr($Item['id'],0,strpos($Item['id'],'_'));
	    		}
				$cart[$key]['product'] = $this->Shop_model->_get_product($Item['id'],0,0,$Item['addon_log_sn']); //帶入加購辨別
				//exit();
				if(!$upgrade_discount_amount && @$Item['upgrade_discount_amount']){ //有升級折扣的話更新 $upgrade_discount_amount
					$upgrade_discount_amount=$Item['upgrade_discount_amount'];
				}
				if($Item['coupon_money'] > 0 && $Item['coupon_money'] < 1){ //優惠是折扣的話
					$cart[$key]['coupon_money']=round(($Item['qty']*$Item['price']) *(1-$Item['coupon_money']));
				}
				if($cart[$key]['product']['supplier_sn']!=$last_supplier){
					$supplier_cart[$cart[$key]['product']['supplier_sn']]['if_all_product_type2']=($cart[$key]['product']['product_type']=='2')?true:false;
					$supplier_amount=$this->Shop_model->get_supplier_ship($cart[$key]['product']['supplier_sn']);
					if($supplier_amount==0){ //該店無購物金額則將已選配送方式取消
						$delivery_method=$this->session->userdata('method_shipping');
						unset($delivery_method[$cart[$key]['product']['supplier_sn']]);
						$this->session->set_userdata('method_shipping',$delivery_method);
					}
					$supplier_cart[$cart[$key]['product']['supplier_sn']]['supplier_subtotal']=$supplier_amount;
					/*if($Item['status']=='3'){ 因為商品可能沒有滿額免運屬性
						$supplier_cart[$cart[$key]['product']['supplier_sn']]['supplier_subtotal']=$Item['subtotal'];
					}else{
						$supplier_cart[$cart[$key]['product']['supplier_sn']]['supplier_subtotal']=0;
					}*/
					$supplier_cart[$cart[$key]['product']['supplier_sn']]['supplier_name']=$cart[$key]['product']['brand'];
					$supplier_cart[$cart[$key]['product']['supplier_sn']]['cart_check']=($Item['status']=='3')?true:false;
					$supplier_cart[$cart[$key]['product']['supplier_sn']]['delivery_method']=@$this->session->userdata('method_shipping')[$cart[$key]['product']['supplier_sn']];
					$supplier_cart[$cart[$key]['product']['supplier_sn']]['min_amount_for_noshipping']=$cart[$key]['product']['min_amount_for_noshipping'];
					$supplier_cart[$cart[$key]['product']['supplier_sn']]['cart'][$Item['rowid']]=$cart[$key];
				}else{
					if($cart[$key]['product']['product_type']!='2'){
						$supplier_cart[$cart[$key]['product']['supplier_sn']]['if_all_product_type2']=false;
					}
					/*if($Item['status']=='3'){
						$supplier_cart[$cart[$key]['product']['supplier_sn']]['supplier_subtotal']+=$Item['subtotal'];
					}*/
					$supplier_cart[$cart[$key]['product']['supplier_sn']]['cart'][$Item['rowid']]=$cart[$key];
				}
				$last_supplier=$cart[$key]['product']['supplier_sn'];
				$coupon_code_total_cdiscount_amount+=$cart[$key]['coupon_money'];
				/*$temp_shopping_cart_order_sn=$this->Shop_model->_get_temp_shopping_cart_order_sn($this->session->userdata['web_member_data']['member_sn']);
				$where = array('temp_shopping_cart_order_sn' => $temp_shopping_cart_order_sn,'status !=' => '2','product_sn'=>$Item['id']);//1有效 2刪除 3已放入結帳清單但可能沒有完成購物所以改抓不是2的
				$cart[$key]['cart_status']=$temp_order_array=$this->Shop_model->_select('ij_temp_order_item',$where,0,0,0,'temp_order_item_sn',0,'row')['status'];*/
			}
		}else{
			$this->session->set_userdata(array('Mes'=>'很抱歉!購物車尚無商品!'));
			redirect(base_url('shop'));
		}
		foreach($supplier_cart as $_key=>$_Item){
			//$supplier_cart[$_key]['delivery']=$this->Shop_model->get_supplier_delivery($_Item['cart'],$_Item['min_amount_for_noshipping']);
			if($_Item['if_all_product_type2']){ //只有虛擬商品
				$supplier_cart[$_key]['delivery']=$this->Shop_model->_select('ij_delivery_method_ct',array('supplier_sn'=>$_key,'delivery_method_name'=>'線上開通'),0,0,0,0,0,'result_array');
			}else{
				$supplier_cart[$_key]['delivery']=$this->Shop_model->_select('ij_delivery_method_ct',array('supplier_sn'=>$_key,'display_flag'=>'1'),0,0,0,0,0,'result_array');
			}
			foreach($supplier_cart[$_key]['delivery'] as $_key2=>$_value2){
				$supplier_cart[$_key]['delivery'][$_key2]['description']=str_replace('[free_shop_money]',number_format($_Item['min_amount_for_noshipping']),$_value2['description']);
			}
			//var_dump($supplier_cart[$_key]['delivery']);
		}
		$page_data['page_content']['upgrade_discount_amount'] = $upgrade_discount_amount;

		$page_data['page_content']['cart'] = $supplier_cart;

		$page_data['page_content']['gift_cash'] = (isset($this->session->userdata['web_member_data']['member_sn'])) ? $this->Shop_model->_get_gift_cash($this->session->userdata['web_member_data']['member_sn']) : "-1";
		//if(!$this->session->userdata('gift_cash')){
			if($this->page_data['cart'][0]['used_gift_cash']){
				$page_data['page_content']['gift_cash'] = $page_data['page_content']['gift_cash']-$this->page_data['cart'][0]['used_gift_cash'];
			}
    	$this->session->set_userdata('gift_cash', $page_data['page_content']['gift_cash']);
    //}

		$page_data['page_content']['free_shop_money']=$this->Shop_model->_select('ij_system_param_config','system_param_name','滿額免運門檻',0,0,0,0,'row',0,0,0,0)['system_param_content'];
		//配送方式
		$page_data['page_content']['delivery_method']=$this->Shop_model->_select('ij_delivery_method_ct','display_flag','1',0,0,0,0,'result_array');

		//$page_data['page_content']['shipping_charge_amount'] = $shipping_charge_amount;
		$page_data['page_content']['coupon_code_total_cdiscount_amount'] = $coupon_code_total_cdiscount_amount;
		//echo abs($coupon_code_total_cdiscount_amount);
		$total_order_amount=intval($this->cart->total()+$shipping_charge_amount);
		$page_data['page_content']['total_order_amount'] = $total_order_amount;
		$page_data['page_content']['down_cart']=$this->Shop_model->get_temp_order($this->session->userdata['web_member_data']['member_sn']);

		//$page_data['page_content']['method_shipping']=$this->session->userdata('method_shipping');
		$page_data['page_content']['view_path'] = 'www/page_shop/page_cart_view'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_shop/page_cart_view_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}
	public function shopView2x(){
		$this->LoginCheck(1,'shop/shopView2x');
		//傳入確認頁
		$page_data = array();
		// 內容設定
		// page level setting
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$upgrade_discount_amount='';
		if($cart=$this->page_data['cart']){
		//var_dump($cart);
			foreach($cart as $key=>$Item){
						//$cart[$key]['image'] = $this->Shop_model->_select('ij_product_gallery',array("product_sn" => $Item['id']),1,0,0,0,0,'row','image_path,image_alt');
						//$cart[$key]['product'] = $this->Shop_model->_get_product($Item['id'],0,0,$Item['addon_log_sn']); //帶入加購辨別
		//var_dump($cart[$key]['product']);
				//var_dump($cart[$key]).'<br>';
				//exit();
				//var_dump($upgrade_discount_amount);
				if(!$upgrade_discount_amount && @$Item['upgrade_discount_amount']){ //有升級折扣的話更新 $upgrade_discount_amount
					$upgrade_discount_amount=$Item['upgrade_discount_amount'];
				}
				if($Item['coupon_money'] > 0 && $Item['coupon_money'] < 1){ //優惠是折扣的話
					$cart[$key]['coupon_money']=round(($Item['qty']*$Item['price']) *(1-$Item['coupon_money']));
				}
			}
		}else{
			$this->session->set_userdata(array('Mes'=>'很抱歉!購物車尚無商品!'));
			 redirect(base_url('shop'));
		}
		$page_data['page_content']['upgrade_discount_amount'] = $upgrade_discount_amount;

		$this->page_data['cart'] = $cart;
		//var_dump($this->page_data['cart']);
	    $shipping_charge_amount=0;
	    $coupon_code_total_cdiscount_amount=0;

		$page_data['page_content']['gift_cash'] = (isset($this->session->userdata['web_member_data']['member_sn'])) ? $this->Shop_model->_get_gift_cash($this->session->userdata['web_member_data']['member_sn']) : "-1";
		//if(!$this->session->userdata('gift_cash')){
			if($this->page_data['cart'][0]['used_gift_cash']){
				$page_data['page_content']['gift_cash'] = $page_data['page_content']['gift_cash']-$this->page_data['cart'][0]['used_gift_cash'];
			}
    	$this->session->set_userdata('gift_cash', $page_data['page_content']['gift_cash']);
    //}

		$page_data['page_content']['free_shop_money']=$this->Shop_model->_select('ij_system_param_config','system_param_name','滿額免運門檻',0,0,0,0,'row',0,0,0,0)['system_param_content'];
		//配送方式
		$page_data['page_content']['delivery_method']=$this->Shop_model->_select('ij_delivery_method_ct','display_flag','1',0,0,0,0,'result_array');

		$page_data['page_content']['shipping_charge_amount'] = $shipping_charge_amount;
		$page_data['page_content']['coupon_code_total_cdiscount_amount'] = $coupon_code_total_cdiscount_amount;
		$total_order_amount=intval($this->cart->total()+$shipping_charge_amount-$coupon_code_total_cdiscount_amount);
		$page_data['page_content']['total_order_amount'] = $total_order_amount;
		$page_data['page_content']['method_shipping']=$this->session->userdata('method_shipping');
		$page_data['page_content']['receiver_addr_state']=$this->session->userdata('receiver_addr_state');
		$page_data['page_content']['view_path'] = 'www/page_shop/page_cart_view2'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_shop/page_cart_view2_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}
	private function sort_by_supplier_name($a, $b)
	{
	    if($a['supplier_name'] == $b['supplier_name']) return 0;
	    return ($a['supplier_name'] < $b['supplier_name']) ? 1 : -1;
	}
	public function shopView2(){
		$this->LoginCheck(1,'shop/shopView2');

		/*$post_data_array = array(
		        //post_data 欄位資料
		        'RespondType' => 'JSON',
		        'Version' => '1.4',
		        'TimeStamp' => time(), // 請以   time()  格式
		        'TransNum' => '',
		        'MerchantOrderNo' => '201409170000001',
		        'BuyerName' => ' 王大品 ',
		        'BuyerUBN' => '54352706',
		        'BuyerAddress' => ' 台北市南港區南港路二段 97 號 8 樓 ',
		        'BuyerEmail' => '54352706@pay2go.com',
		        'Category' => 'B2B',//B2C=買受人為個人。
		        'TaxType' => '1',
		        'TaxRate' => '5',
		        'Amt' => '490',
		        'TaxAmt' => '10',
		        'TotalAmt' => '500',
		        'CarrierType' => '',
		        'CarrierNum' => rawurlencode(''),
		        'LoveCode' => '',
		        'PrintFlag' => 'Y',
		        'ItemName' => ' 商品一 | 商品二 ', // 多項商品時，以「 | 」分開
		        'ItemCount' => '1|2', // 多項商品時，以「 | 」分開
		        'ItemUnit' => ' 個 | 個 ', // 多項商品時，以「 | 」分開
		        'ItemPrice' => '300|100', // 多項商品時，以「 | 」分開
		        'ItemAmt' => '300|200', // 多項商品時，以「 | 」分開
		        'Comment' => ' 備註 ',
		        'CreateStatusTime' => '',
		        'Status' => '1' //1= 立即開立， 0= 待開立， 3= 延遲開立
		);
		$this->load->library('ezpay_invoice');
		$result=$this->ezpay_invoice->set_invoice($post_data_array);
        echo '<pre>' . var_export($result, true) . '</pre>';*/



		$page_data = array();
		// 內容設定
		// page level setting
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$delivery_method=$this->session->userdata('method_shipping');
		//var_dump($delivery_method);
		$page_data['page_content']['receiver_addr_state']=$this->session->userdata('method_shipping')[0];
		if(!$page_data['page_content']['receiver_addr_state']){
			$this->session->set_userdata(array('Mes'=>'很抱歉!請先選擇配送地區!'));
			redirect(base_url("shop/shopView"));
		}
		//var_dump($page_data['page_content']['receiver_addr_state']);
		/*if(!$data_array=$this->input->post()){
			if($this->session->userdata('method_shipping')){ //先前有紀錄的話
				$page_data['page_content']['method_shipping']=$this->session->userdata('method_shipping');
			}else{
				$this->session->set_userdata(array('Mes'=>'很抱歉!請先選擇配送方式!'));
				redirect(base_url("shop/shopView2"));
			}
		}else{
			$page_data['page_content']['method_shipping'] = $data_array['method_shipping'];
			//echo $page_data['page_content']['method_shipping'];
			//exit();
			$this->session->set_userdata(array('method_shipping'=>$data_array['method_shipping'])); // 配送方式保存
			$page_data['page_content']['receiver_addr_state'] = $data_array['receiver_addr_state'];
			$this->session->set_userdata(array('receiver_addr_state'=>$data_array['receiver_addr_state'])); // 保存
		}*/


		$upgrade_discount_amount='';
		if($cart=$this->page_data['cart']){
		//var_dump($cart);
			foreach($cart as $key=>$Item){
				if($Item['status']=='3'){
					$cart[$key]['product'] = $this->Shop_model->_get_product($Item['id'],0,0,$Item['addon_log_sn']); //帶入加購辨別
					//$cart[$key]['supplier_sn']=$cart[$key]['product']['supplier_sn'];
					//var_dump($cart[$key]).'<br>';
					//exit();
					//var_dump($key);
					if(!$upgrade_discount_amount && @$Item['upgrade_discount_amount']){ //有升級折扣的話更新 $upgrade_discount_amount
						$upgrade_discount_amount=$Item['upgrade_discount_amount'];
					}
					if($Item['coupon_money'] > 0 && $Item['coupon_money'] < 1){ //優惠是折扣的話
						$cart[$key]['coupon_money']=round(($Item['qty']*$Item['price']) *(1-$Item['coupon_money']));
					}
				}else{
					unset($cart[$key]);
				}
			}
		}else{
			$this->session->set_userdata(array('Mes'=>'很抱歉!購物車尚無商品!'));
			redirect(base_url('shop'));
		}
		usort($cart, array($this,'sort_by_supplier_name'));
		//$delivery_method=$this->input->post('delivery_method');
        //echo '<pre>' . var_export($delivery_method) . '</pre>';

		$page_data['page_content']['upgrade_discount_amount'] = $upgrade_discount_amount;

		$page_data['page_content']['cart'] = $cart;
		//var_dump($this->page_data['cart']);
	    $shipping_charge_amount=0;
	    $coupon_code_total_cdiscount_amount=0;

		$page_data['page_content']['gift_cash'] = (isset($this->session->userdata['web_member_data']['member_sn'])) ? $this->Shop_model->_get_gift_cash($this->session->userdata['web_member_data']['member_sn']) : "-1";
		//if(!$this->session->userdata('gift_cash')){
			if($this->page_data['cart'][0]['used_gift_cash']){
				$page_data['page_content']['gift_cash'] = $page_data['page_content']['gift_cash']-$this->page_data['cart'][0]['used_gift_cash'];
			}
    	$this->session->set_userdata('gift_cash', $page_data['page_content']['gift_cash']);
    //}

		/*$page_data['page_content']['free_shop_money']=$this->Shop_model->_select('ij_system_param_config','system_param_name','滿額免運門檻',0,0,0,0,'row',0,0,0,0)['system_param_content'];
		//配送方式
		$page_data['page_content']['delivery_method']=$this->Shop_model->_select('ij_delivery_method_ct','display_flag','1',0,0,0,0,'result_array');
		foreach($page_data['page_content']['delivery_method'] as $_item){
			if($_item['delivery_method']==$page_data['page_content']['method_shipping']){
				$page_data['page_content']['delivery_method_name']=$_item['delivery_method_name'];
			}

		}*/
		$page_data['page_content']['shipping_charge_amount'] = $shipping_charge_amount;
		$page_data['page_content']['coupon_code_total_cdiscount_amount'] = $coupon_code_total_cdiscount_amount;
		$total_order_amount=intval($this->cart->total()+$shipping_charge_amount-$coupon_code_total_cdiscount_amount);
		$page_data['page_content']['total_order_amount'] = $total_order_amount;
		if($page_data['page_content']['receiver_addr_state']=='台灣本島'){
			$money_field='delivery_amount';
			$page_data['receiver_addr_citys']=$this->Shop_model->get_citys();
		}else if($page_data['page_content']['receiver_addr_state']=='外島地區'){
			$money_field='outland_amount';
			$page_data['receiver_addr_citys']=$this->Shop_model->get_citys(1);
		}
		$page_data['page_content']['down_cart']=$this->Shop_model->get_temp_order($this->session->userdata['web_member_data']['member_sn'],0,0,0,$money_field);
		$page_data['page_content']['payments'] = $this->Shop_model->_get_payments();
		$page_data['page_content']['city_ct'] = $this->Shop_model->_select('ij_city_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','city_code,city_name'); // city
		$page_data['page_content']['city_ct1'] = $page_data['page_content']['city_ct'];
		if($this->session->userdata('order')){
			//$page_data['order']=$this->Shop_model->_select('ij_order','order_sn',$this->session->userdata('order_sn'),0,0,'order_create_date','desc','row',0,0,0,0);
			$page_data['order']=$this->session->userdata('order');
		//echo $this->db->last_query();
				//縣市鄉鎮區找回KEY
			$page_data['order']['buyer_addr_city_code']=$this->Shop_model->_select('ij_city_ct','city_name',$page_data['order']['buyer_addr_city'],0,0,0,0,'row','city_code')['city_code'];
			$page_data['order']['buyer_addr_town_code']=$this->Shop_model->_select('ij_town_ct','town_name',$page_data['order']['buyer_addr_town'],0,0,0,0,'row','town_code')['town_code'];
			$page_data['order']['receiver_addr_city_code']=$this->Shop_model->_select('ij_city_ct','city_name',@$page_data['order']['receiver_addr_city'],0,0,0,0,'row','city_code')['city_code'];
			$page_data['order']['receiver_addr_town_code']=$this->Shop_model->_select('ij_town_ct','town_name',@$page_data['order']['receiver_addr_town'],0,0,0,0,'row','town_code')['town_code'];
			//var_dump($page_data['order']);
		}
		//var_dump($page_data['page_content']['method_shipping']);
		//var_dump($page_data['page_content']['down_cart']);
		$page_data['page_content']['view_path'] = 'www/page_shop/page_cart_view2'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_shop/page_cart_view2_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 * 導向付款
	 *
	 */
	public function gotoPay($order_sn=0)
	{
		$cart=$this->Shop_model->Get_Cart_Checkout();
		if(!$cart){
			$this->session->set_userdata(array('Mes'=>'很抱歉!購物車內無商品，或是您已經結帳!'));
			redirect(base_url("shop/shopView"));
		}
		if($temp_order=$this->input->post('order',true)){
			$order=array();
			foreach($this->config->item("order_ct") as $_item){
				//var_dump($_item);
				$order[$_item]=$this->security->xss_clean($temp_order[$_item]);
			}
			//確認運費
			if($order['receiver_addr_state']=='台灣本島'){
				$money_field='delivery_amount';
			}else if($order['receiver_addr_state']=='外島地區'){
				$money_field='outland_amount';
			}else{
				$money_field='delivery_amount';
			}
			$order['total_shipping_charge_amount']=$this->Shop_model->get_temp_order($this->session->userdata['web_member_data']['member_sn'])['shipping_charge_amount'];
			//$temp_order=$this->Shop_model->get_temp_order($this->session->userdata['web_member_data']['member_sn']);

			//var_dump($temp_order);
			//exit;
			$order_sn=$this->Shop_model->_save_order($order,$money_field); //寫入訂單
		}else{
			$this->session->set_userdata(array('Mes'=>'很抱歉！購物車資料有誤，如有疑問敬請聯絡客服處理。'));
			redirect(base_url("shop/shopView"));
		}
		//$total=intval($cart['total_order_amount'])+$order['shipping_charge_amount'];
		//$this->session->set_userdata("order",$order); //紀錄order
	    /*$receiver_data = array(
	        'last_name'       => @$order['receiver_last_name'],
	        'cell'      			=> @$order['receiver_cell'],
	        'tel_area_code'   => @$order['receiver_tel_area_code'],
	        'tel'      				=> @$order['receiver_tel'],
	        'tel_ext'      		=> @$order['receiver_tel_ext'],
	        'addr_city_code'  => @$order['receiver_addr_city'],
	        'zipcode'      		=> @$order['receiver_zipcode'],
	        'addr_town_code'  => @$order['receiver_addr_town'],
	        'addr1'      			=> @$order['receiver_addr1'],
	    );
		$this->session->set_userdata("receiver_data",$receiver_data); //紀錄收件人資料*/

		if($this->input->post('update_member')=='yes' && $order=$this->input->post('order')){ //結帳成功後同步更新您的會員資料
		    $member_data = array(
		        'last_name'      	=> $order['buyer_last_name'],
		        //'first_name'      => $order['buyer_first_name'],
		        'cell'      			=> $order['buyer_cell'],
		        //'tel_area_code'   => $order['buyer_tel_area_code'],
		        'tel'      				=> $order['buyer_tel'],
		        //'tel_ext'      		=> $order['buyer_tel_ext'],
		        'addr_state'  => $order['buyer_addr_state'],
		        'addr_city_code'  => $order['buyer_addr_city'],
		        'zipcode'      		=> $order['buyer_zipcode'],
		        'addr_town_code'  => $order['buyer_addr_town'],
		        'addr1'      			=> $order['buyer_addr1'],
		        'email'      			=> $order['buyer_email']
		    );
		  	$this->Shop_model->_update('ij_member',$member_data,'ij_member.member_sn',$this->session->userdata['web_member_data']['member_sn']);
			//echo $this->db->last_query();
	    //var_dump($member_data);
	    //exit();
		}
		$this->session->set_userdata('coupon_data', ''); //清除優惠券
  		if(!@$order_sn){
			$where = array('ij_order.member_sn' => $this->session->userdata['web_member_data']['member_sn'],'payment_done_date' => '0000-00-00 00:00:00');
			if(!$order=$this->Shop_model->_select('ij_order',$where,0,1,0,'order_create_date','desc','row','order_sn,total_order_amount,order_num,payment_method,buyer_email')){
				$this->session->set_userdata(array('Mes'=>'很抱歉，訂單遺失，可能是閒置過久，敬請查看訂單查詢以確認訂單狀況'));
		  		redirect(base_url('shop'));
		    }
		  $order_sn=$order['order_sn'];
		}else{
			//確定取得訂單編號後清空購物車
			$this->cart->destroy();
			$this->Shop_model->update_new_temp_order(); //購物送出後更新暫存購物清單
			$this->Shop_model->get_temp_order_to_cart($this->session->userdata['web_member_data']['member_sn']);//沒結帳商品加回購物車
			$order=$this->Shop_model->_select('ij_order','order_sn',$order_sn,1,0,'total_order_amount',0,'row','total_order_amount,order_num,payment_method,buyer_email');
		}

		//改一律跑藍新
		//if($order['payment_method']=='8'){ //實體ATM
			//redirect(base_url('shop/shopOrder/'.$order['order_num']));
		//}else{
			$this->load->library('newwebpay');
			$this->newwebpay->ccard_pay($order['order_num'],$order['payment_method'],$order['total_order_amount'],$order['buyer_email']);
		//}
		//$this->session->set_userdata(array('order_sn'=>$order_sn)); // 記住訂單標號

		/*$order=$this->input->post('order');
		if($order && $order_sn){
			$where = array('ij_order.order_sn' => $order_sn,'member_sn' => $this->session->userdata['web_member_data']['member_sn']);
	    	$this->Shop_model->_update('ij_order',$order,$where);
		}*/
		//$this->session->set_userdata(array('method_shipping'=>'')); // 配送方式清除
		//$this->session->set_userdata(array('buyer_addr_state'=>'')); // 清除 buyer_addr_state
		//$this->session->set_userdata(array('order'=>'')); // 清除 order

	}
	public function pay_back(){
		$this->load->library('newwebpay');
		$back_data=$this->newwebpay->ccard_return();

		$update_array=array('pay_back_result'=>json_encode($back_data));
		if($back_data['Status'] == "SUCCESS"){
			$update_array['credit_card_trans_id']=$back_data['Result']['TradeNo'];
			$update_array['payment_status']='2';
			$update_array['payment_done_date']=$back_data['Result']['PayTime'];
		}
		$this->Shop_model->_update('ij_order',$update_array,'order_num',$back_data['Result']['MerchantOrderNo']);
		//$this->load->library('ezpay_invoice');
		$order=$this->libraries_model->get_orders(array('order_num'=>$back_data['Result']['MerchantOrderNo']),1)[0];
		if($order){
			$update_array2['pay_status']='2';
			$this->Shop_model->_update('ij_sub_order',$update_array2,array('order_sn'=>$order['order_sn']));
	        $this->libraries_model->send_order_mail($order['order_sn']);
			//$update_array=array('delivery_status'=>'1');
			//$this->Shop_model->_update('ij_sub_order',$update_array,'sub_order_sn',$order['sub_order_sn']);
		}
		//echo file_get_contents("php://input");exit;
		//$result=$this->ezpay_invoice->set_invoice($order,INVOICE_DELAY_DAYS);
        //echo '<pre>' . var_export($_POST,true) . '</pre>';
        //exit;
        redirect(base_url('shop/shopOrder/'.$back_data['Result']['MerchantOrderNo']));
	}

	public function notify($p=3){
		$this->load->library('newwebpay');
		$back_data=$this->newwebpay->ccard_return($p);
        //echo '<pre>' . var_export($back_data,true) . '</pre>';
        //exit;PaymentType Inst
		$update_array=array('pay_back_result'=>json_encode($back_data));
		if($back_data['Status'] == "SUCCESS"){
			$update_array['credit_card_trans_id']=$back_data['Result']['TradeNo'];
			$update_array['payment_status']='2';
			//$update_array['delivery_status']='1';
			$update_array['payment_done_date']=$back_data['Result']['PayTime'];
		}
		$_where=array('total_order_amount'=>$back_data['Result']['Amt'],'order_num'=>$back_data['Result']['MerchantOrderNo']);
		$this->Shop_model->_update('ij_order',$update_array,$_where);
		//$this->load->library('ezpay_invoice');
		$order=@$this->libraries_model->get_orders(array('order_num'=>$back_data['Result']['MerchantOrderNo']),1)[0];
		if($order){
			$update_array2['pay_status']='2';
			$this->Shop_model->_update('ij_sub_order',$update_array2,array('order_sn'=>$order['order_sn']));
	        $this->libraries_model->send_order_mail($order['order_sn']);
			//$update_array=array('delivery_status'=>'1');
			//$this->Shop_model->_update('ij_sub_order',$update_array,'sub_order_sn',$order['sub_order_sn']);
		}
		//$result=$this->ezpay_invoice->set_invoice($order,INVOICE_DELAY_DAYS);
	}

	/**
	 * 線上商店訂單明細 Template
	 *
	 */
	public function shopOrder($order_num=0)
	{
		$this->LoginCheck(1,base_url('shop/shopOrder/'.$order_num));
		$this->session->set_userdata(array('order_sn'=>'')); // 清除 order_sn
		if(!$order_num){
			$order_num=$this->input->post('order_num');
		}
		//直接先假定都付款成功
		/*$where = array('ij_order.order_sn' => $order_sn,'ij_order.member_sn' => $this->session->userdata['web_member_data']['member_sn']);
		$paymentok = array('payment_status' => '2','payment_done_date'=>date("Y-m-d H:i:s",time()));
		$paymentok = $this->Shop_model->CheckUpdate($paymentok,0);
		$this->Shop_model->_update('ij_order',$paymentok,$where);*/
		//echo $this->db->last_query();

		//$order=$this->Shop_model->chk_order_online($order_sn);
		//exit();
		$order=$this->Shop_model->_get_order($order_num,0,1);

		$page_data = array();
		// 內容設定
		// page level setting
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		//$page_data['page_content']['view_path'] = 'www/page_shop/page_order'; // 頁面主內容 view 位置, 必要
		$page_data['page_content']['view_path'] = 'www/page_shop/page_cart_view4'; // 頁面主內容 view 位置, 必要
		$page_data['page_content']['order'] = $order;
		//var_dump($page_data['page_content']['order']);
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_shop/page_order_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}












	public function shopView4(){
		$this->LoginCheck(1,'shop/shopView4');
		//傳入確認頁

		$page_data = array();
		// 內容設定
		// page level setting
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$upgrade_discount_amount='';
		if($cart=$this->cart->contents()){
		//var_dump($cart);
			foreach($cart as $key=>$Item){
						//$cart[$key]['image'] = $this->Shop_model->_select('ij_product_gallery',array("product_sn" => $Item['id']),1,0,0,0,0,'row','image_path,image_alt');
						$cart[$key]['product'] = $this->Shop_model->_get_product($Item['id'],0,0,$Item['addon_log_sn']); //帶入加購辨別
				//var_dump($cart[$key]).'<br>';
				//exit();
				//var_dump($upgrade_discount_amount);
				if(!$upgrade_discount_amount && @$Item['upgrade_discount_amount']){ //有升級折扣的話更新 $upgrade_discount_amount
					$upgrade_discount_amount=$Item['upgrade_discount_amount'];
				}
				if($Item['coupon_money'] > 0 && $Item['coupon_money'] < 1){ //優惠是折扣的話
					$cart[$key]['coupon_money']=round(($Item['qty']*$Item['price']) *(1-$Item['coupon_money']));
				}
			}
		}else{
				$this->session->set_userdata(array('Mes'=>'很抱歉!購物車尚無商品!'));
			  redirect(base_url('shop'));
		}
		$page_data['page_content']['upgrade_discount_amount'] = $upgrade_discount_amount;

		$page_data['page_content']['cart'] = $cart;
		//var_dump($this->page_data['cart']);
    $shipping_charge_amount=0;
    $coupon_code_total_cdiscount_amount=0;

		$page_data['page_content']['gift_cash'] = (isset($this->session->userdata['web_member_data']['member_sn'])) ? $this->Shop_model->_get_gift_cash($this->session->userdata['web_member_data']['member_sn']) : "-1";
		//if(!$this->session->userdata('gift_cash')){
			if($this->page_data['cart'][0]['used_gift_cash']){
				$page_data['page_content']['gift_cash'] = $page_data['page_content']['gift_cash']-$this->page_data['cart'][0]['used_gift_cash'];
			}
    	$this->session->set_userdata('gift_cash', $page_data['page_content']['gift_cash']);
    //}

		$page_data['page_content']['free_shop_money']=$this->Shop_model->_select('ij_system_param_config','system_param_name','滿額免運門檻',0,0,0,0,'row',0,0,0,0)['system_param_content'];
		//配送方式
		$page_data['page_content']['delivery_method']=$this->Shop_model->_select('ij_delivery_method_ct','display_flag','1',0,0,0,0,'result_array');

		$page_data['page_content']['shipping_charge_amount'] = $shipping_charge_amount;
		$page_data['page_content']['coupon_code_total_cdiscount_amount'] = $coupon_code_total_cdiscount_amount;
		$total_order_amount=intval($this->cart->total()+$shipping_charge_amount-$coupon_code_total_cdiscount_amount);
		$page_data['page_content']['total_order_amount'] = $total_order_amount;
		$page_data['page_content']['view_path'] = 'www/page_shop/page_cart_view4'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_shop/page_cart_view_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}
	/**
	 * 線上商店商品清單確認
	 *
	 */
	public function shopConfirm() //none
	{
		$this->LoginCheck();

		$page_data = array();

		// 內容設定

		// page level setting


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
					//echo $this->page_data['method_shipping'];

		if(!$data_array=$this->input->post()){
			if($this->session->userdata('method_shipping')){ //先前有紀錄的話
				$page_data['page_content']['method_shipping']=$this->session->userdata('method_shipping');
				$page_data['page_content']['buyer_addr_state']=$this->session->userdata('buyer_addr_state');
			}else{
				$this->session->set_userdata(array('Mes'=>'很抱歉!請先選擇配送方式!'));
				redirect(base_url("shop/shopView"));
			}
		}else{
			$page_data['page_content']['method_shipping'] = $data_array['method_shipping'];
			//echo $page_data['page_content']['method_shipping'];
			//exit();
			$this->session->set_userdata(array('method_shipping'=>$data_array['method_shipping'])); // 配送方式保存
			$page_data['page_content']['buyer_addr_state'] = $data_array['buyer_addr_state'];
			$this->session->set_userdata(array('buyer_addr_state'=>$data_array['buyer_addr_state'])); // 保存
		}
		if($cart=$this->cart->contents()){
			foreach($cart as $key=>$Item){
				if($Item['status']=='3'){
						$cart_out[$key] = $Item;
						$cart_out[$key]['product'] = $this->Shop_model->_get_product($Item['id'],0,0,$Item['addon_log_sn']);
		//var_dump($ccart[$key]['product']);
						//$cart[$key] = $Item;
					if($Item['coupon_money'] > 0 && $Item['coupon_money'] < 1){ //優惠是折扣的話
					  $cart_out[$key]['coupon_money']=round(($Item['qty']*$Item['price']) *(1-$Item['coupon_money']));
					}
				}else{
					//unset($cart[$key]);
				}

			}
		}else{
				$this->session->set_userdata(array('Mes'=>'很抱歉!購物車尚無商品!'));
			  redirect(base_url('shop'));
		}
		$page_data['page_content']['cart'] = $cart_out;


		$page_data['page_content']['down_cart']=$this->Shop_model->get_temp_order($this->session->userdata['web_member_data']['member_sn'],0,$page_data['page_content']['method_shipping']);

    /*$shipping_charge_amount=0;
    $coupon_code_total_cdiscount_amount=0;
		$page_data['page_content']['gift_cash'] = $this->session->userdata('gift_cash');
		$page_data['page_content']['shipping_charge_amount'] = $shipping_charge_amount;
		$page_data['page_content']['coupon_code_total_cdiscount_amount'] = $coupon_code_total_cdiscount_amount;
		$total_order_amount=intval($this->cart->total()+$shipping_charge_amount-$coupon_code_total_cdiscount_amount);
		$page_data['page_content']['total_order_amount'] = $total_order_amount;*/
		$page_data['page_content']['view_path'] = 'www/page_shop/page_cart_confirm'; // 頁面主內容 view 位置, 必要
		//var_dump($page_data['page_content']);
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_shop/page_cart_confirm_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 * 線上商店付款流程 Template
	 *
	 */
	public function shopCheckout() //none
	{
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		$this->LoginCheck();
		if(!$cart_confirm=$this->input->post('cart_confirm')){
			$this->session->set_userdata(array('Mes'=>'很抱歉!請先選擇配送方式!'));
			redirect(base_url("shop/shopView"));
		}

		$page_data = array();
		// 內容設定
		// page level setting
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$chk_if_all_virtual_prod=true;
		if($this->session->userdata('order')){
			//$page_data['order']=$this->Shop_model->_select('ij_order','order_sn',$this->session->userdata('order_sn'),0,0,'order_create_date','desc','row',0,0,0,0);
			$page_data['order']=$this->session->userdata('order');
		//echo $this->db->last_query();
				//縣市鄉鎮區找回KEY
			$page_data['order']['buyer_addr_city_code']=$this->Shop_model->_select('ij_city_ct','city_name',$page_data['order']['buyer_addr_city'],0,0,0,0,'row','city_code')['city_code'];
			$page_data['order']['buyer_addr_town_code']=$this->Shop_model->_select('ij_town_ct','town_name',$page_data['order']['buyer_addr_town'],0,0,0,0,'row','town_code')['town_code'];
			$page_data['order']['receiver_addr_city_code']=$this->Shop_model->_select('ij_city_ct','city_name',@$page_data['order']['receiver_addr_city'],0,0,0,0,'row','city_code')['city_code'];
			$page_data['order']['receiver_addr_town_code']=$this->Shop_model->_select('ij_town_ct','town_name',@$page_data['order']['receiver_addr_town'],0,0,0,0,'row','town_code')['town_code'];
			//var_dump($page_data['order']);
		}
		//$page_data['page_content']['receiver_data'] = @$this->session->userdata['receiver_data']; b說不要帶
		$page_data['page_content']['method_shipping'] = $cart_confirm['method_shipping'];
		$page_data['page_content']['buyer_addr_state'] = $cart_confirm['buyer_addr_state'];
		$page_data['page_content']['shipping_charge_amount'] = $cart_confirm['shipping_charge_amount'];
		$page_data['page_content']['city_ct'] = $this->Shop_model->_select('ij_city_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','city_code,city_name'); // city
		$page_data['page_content']['city_ct1'] = $page_data['page_content']['city_ct'];

		$cart=$this->cart->contents();
			foreach($cart as $key=>$Item){
				$product = $this->Shop_model->_get_product($Item['id'],'product_type');
				if($product['product_type']!='2'){ //不是虛擬就否
					$chk_if_all_virtual_prod=false;
				}
			}
		$page_data['all_virtual_prod']=$chk_if_all_virtual_prod;

    /*$shipping_charge_amount=55;
    $coupon_code_total_cdiscount_amount=0;
		$page_data['page_content']['gift_cash'] = $this->session->userdata('gift_cash');
		$page_data['page_content']['shipping_charge_amount'] = $shipping_charge_amount;
		$page_data['page_content']['coupon_code_total_cdiscount_amount'] = $coupon_code_total_cdiscount_amount;
		$total_order_amount=intval($this->cart->total()+$shipping_charge_amount-$coupon_code_total_cdiscount_amount);
		$page_data['page_content']['total_order_amount'] = $total_order_amount;
		$page_data['page_content']['payments'] = $this->Shop_model->_select('ij_payment_method_ct','display_flag','1',0,0,0,0,'result_array');*/
		//var_dump($page_data['all_virtual_prod']);


		$page_data['page_content']['view_path'] = 'www/page_shop/page_checkout'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_shop/page_checkout_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 * 線上商店付款流程 Template
	 *
	 */
	public function shopCheckoutShipping() //none
	{
			$this->LoginCheck();
			$cart=$this->Shop_model->Get_Cart_Checkout();
			$order=$this->input->post('order');
			if(!$cart){
				$this->session->set_userdata(array('Mes'=>'很抱歉!購物車內無商品，或是您已經結帳!'));
				redirect(base_url("shop/shopView"));
			}
			$total=intval($cart['total_order_amount'])+$order['shipping_charge_amount'];
			$this->session->set_userdata("order",$order); //紀錄order
	    $receiver_data = array(
	        'last_name'       => @$order['receiver_last_name'],
	        'cell'      			=> @$order['receiver_cell'],
	        'tel_area_code'   => @$order['receiver_tel_area_code'],
	        'tel'      				=> @$order['receiver_tel'],
	        'tel_ext'      		=> @$order['receiver_tel_ext'],
	        'addr_city_code'  => @$order['receiver_addr_city'],
	        'zipcode'      		=> @$order['receiver_zipcode'],
	        'addr_town_code'  => @$order['receiver_addr_town'],
	        'addr1'      			=> @$order['receiver_addr1'],
	    );
			$this->session->set_userdata("receiver_data",$receiver_data); //紀錄收件人資料

		if($this->input->post('update_member')=='yes' && $order=$this->input->post('order')){ //結帳成功後同步更新您的會員資料
		    $member_data = array(
		        'last_name'      	=> $order['buyer_last_name'],
		        'first_name'      => $order['buyer_first_name'],
		        'cell'      			=> $order['buyer_cell'],
		        'tel_area_code'   => $order['buyer_tel_area_code'],
		        'tel'      				=> $order['buyer_tel'],
		        'tel_ext'      		=> $order['buyer_tel_ext'],
		        'addr_city_code'  => $order['buyer_addr_city'],
		        'zipcode'      		=> $order['buyer_zipcode'],
		        'addr_town_code'  => $order['buyer_addr_town'],
		        'addr1'      			=> $order['buyer_addr1'],
		        'email'      			=> $order['buyer_email']
		    );
		  	$this->Shop_model->_update('ij_member',$member_data,'member_sn',$this->session->userdata['web_member_data']['member_sn']);
			//echo $this->db->last_query();
	    //var_dump($member_data);
	    //exit();
		}


		/*if($this->Shop_model->_select('ij_delivery_method_ct','delivery_method',$order['delivery_method'],0,0,'delivery_method_name',0,'row','delivery_method_name')['delivery_method_name']=='貨到付款'){
			//運送方式選貨到付款則跳過付款方式選擇頁-指定付款方式為貨到付款 2016-1-14 取消運送方式-貨到付款 wdj
			$where = array('ij_order.order_sn' => $order_sn,'member_sn' => $this->session->userdata['web_member_data']['member_sn']);
			$payment_method=$this->Shop_model->_select('ij_payment_method_ct','payment_method_name','貨到付款',0,0,'payment_method',0,'row','payment_method')['payment_method'];
    	$this->Shop_model->_update('ij_order',array('payment_method'=>$payment_method),$where);
			redirect(base_url('shop/shopOrder/'.$order_sn));
		}*/
		$page_data = array();
		// 內容設定
		// page level setting
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		$page_data['page_content']['payments'] = $this->Shop_model->_get_payments();
		//$page_data['page_content']['order_sn'] = @$order_sn;
		$page_data['page_content']['total'] = $total;
		//echo @$order_sn;
		$page_data['page_content']['view_path'] = 'www/page_shop/page_checkout_shipping'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_shop/page_checkout_shipping_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 * 搜尋結果 Template
	 *
	 */
	public function shopSearch()
	{
		$sort=$this->input->get('sort',true);
		$search=$this->input->get('Search',true);
		if(mb_strlen($search, "utf-8")>20){
			$this->session->set_userdata(array('Mes'=>'很抱歉，僅允許輸入20個中英文'));
		  	redirect(base_url('shop'));
		}
		$category_sn=$this->input->get('category_sn',true);
		$page=$this->input->get('page',true);
		$page_data = array();

		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		//排序前10有圖片分類
		$where = array('category_status' => '1','banner_save_dir <>' => '');
		$where['upper_category_sn']='0';

		//熱賣商品
		$page_data['page_content']['promo_product2']=$this->Shop_model->_get_products('囍市集','2',0,5);
		//特價商品
		$page_data['page_content']['promo_product3']=$this->Shop_model->_get_products('囍市集','3',0,5);
		//熱門商品
		$page_data['page_content']['promo_product5']=$this->Shop_model->_get_products('囍市集','5',0,5);

		//搜尋商品
    	$this->load->library('pagination');
		$page_data['page_content']['perpage'] = 32;
		$page_data['page_content']['total'] = count($this->Shop_model->_get_products('囍市集',0,$category_sn,0,0,0,0,0,0,$search));
    	$config = $this->ijw->_pagination('shop/shopSearch', $page_data['page_content']['total'],$page_data['page_content']['perpage'],0,'shop');
    	$this->pagination->initialize($config);
    	$page_data['page_content']['page_links'] = $this->pagination->create_links();
		$page_data['page_content']['products']=$this->Shop_model->_get_products('囍市集',0,$category_sn,$page_data['page_content']['perpage'],$page,$sort,0,0,0,$search);
		//紀錄搜尋關鍵字
		$this->libraries_model->add_update_hot_search(trim($search));

		$page_data['page_content']['sort']=$sort;
		$page_data['general_header']['search']=$search;
		$page_data['page_content']['search']=$search;
	    $page_data['title']=$search.'-'.date('Y').'年 | HiiSU 囍市集 | 婚禮購物商城';
	    $page_data['description']="你想要找的".$search."-婚禮人氣推薦商品就來囍市集購物，買".$search."來囍市集可找到更多設計款式及客製化的獨特商品，實現結婚新人的婚禮夢想。";
		$page_data['page_content']['view_path'] = 'www/page_shop/page_search'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_shop/page_search_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/*
	* 會員中心中個人婚禮網站與報到系統列表改在會員
	*/
	/*
    public function my($page='ewedding')
    {
        $page_data = array();
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_'.$page; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_'.$page.'_js'; // 頁面主內容的 JS 由這個參數帶入, 必要
		$this->load->view('www/general/main', $page_data);
    }*/

/**
	 * 購物車刷新用
	 *
	 */
	public function top_cart()
	{
		$this->load->view('www/general/top_cart');
	}

    //製作驗證碼
	  public function captcha_img()
    {
        $this->load->helper('captcha');
		$pool = '0123456789';
        $word = '';
        for ($i = 0; $i < 4; $i++){
            $word .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
        }

        $this->session->set_userdata('captcha', $word);

        $vals = array(
            'word'  => $word,
            'img_path'  => './public/uploads/captcha/',
            'img_url'  => base_url().'public/uploads/captcha/',
			'img_width' => '150',
			'img_height' => 45,
			'font_size'  => 22,
			'expiration' => 7200
            );
        $cap = create_captcha($vals);
        return $cap['image'];
    }
    //放入購物車
    public function add(){
    	//var_dump($this->input->post());
    	//exit();
       $wedding_website_sn = intval($this->input->post("wedding_website_sn",TRUE));
	   //var_dump($wedding_website_sn);
	   //exit;
       $product_sn = intval($this->input->post("product_sn",TRUE));
       $category_sn = intval($this->input->post("category_sn",TRUE));
       $original_order_sn = intval($this->input->post("original_order_sn",TRUE));
       if($original_order_sn){
       	$actual_sales_amount=$this->Shop_model->get_upg_products($original_order_sn,$product_sn,1);
	       if(@$actual_sales_amount['response_code']){
	       		$actual_sales_amount=0;
	       }
       }else{
       		$actual_sales_amount=0;
       }
       //if($product = $this->Shop_model->_select('ij_product','product_sn',$product_sn,1,0,0,0,'row','product_name,min_order_qt,default_channel_name,default_root_category_sn')){
       if($product = $this->Shop_model->_get_product($product_sn,'product_name,min_order_qt,supplier_name,default_root_category_sn,brand')){
			//if(strtolower(filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest') {
            //$result = $this->Shop_model->_get_product($this->input->post("product_sn",TRUE));
            $qty = intval($this->input->post("qty",TRUE));
            if($product['min_order_qt'] > $qty) $qty=$product['min_order_qt']; //最小訂購量
            if(!$category_sn) $category_sn=$product['default_root_category_sn']; //無分類帶入主分類
            $addon = $this->input->post("addon",TRUE);
			$addon_count=0;
            if($addon){ //計算加購商品數
            	foreach($addon as $addon_item){
            		if(@$addon_item['addon_log_sn']){
            			$addon_count++;
            		}
            	}
            }

            /*name若為中文會被過濾，故修改system/library/cart.php內的 product_name_rules
            * 若升級新版記得修改
            */
            //商品售價
            $price=$this->Shop_model->_get_price($qty,$product_sn,$this->input->post("product_spec",true));
            //母訂單已付金額扣除
       		//var_dump($product);
            if($original_order_sn && $actual_sales_amount){
            	 $price = $price-$actual_sales_amount;
            	 if($price<0) $price=0; //避免負數
            }
            $data = array(
                'id'      => $product_sn.'_'.$this->input->post("product_spec",true),
                'qty'     => $qty,
                'price'   => $price,
                'name'    => trim($product['product_name']),
                'gift_cash'    => 0,
                'addon_log_sn'  => '',  //加購用
			    'addon_from_rowid'  => '', //加購用
                'addon_count'  => $addon_count, //加購筆數
                'status'  => '3',
                'coupon_code_id'  => '',  // 優惠券
                'coupon_money'  => 0,  // 優惠金額
                'mutispec_stock_sn'  => $this->input->post("product_spec",true),
                'original_order_sn'  => $original_order_sn,
                'upgrade_discount_amount'  => $actual_sales_amount,  //升級價差金額
                'category_sn'  => $category_sn,
                'supplier_name'  => $product['brand']
            );

	          if(!$wedding_website_sn){ //從婚禮網站加購商品，主商品不加入購物車
	            if(!$rowid=$this->_chk_cart($product_sn,$this->cart->contents(),@$this->input->post("product_spec",true))){
		                      	//var_dump($data);
		            $rowid=$this->cart->insert($data);
		                      	//var_dump($rowid);
		          }else{
						$data['rowid'] = $rowid;
						$this->cart->update($data);
		          	//改為override
								//$error['error']='已有相同產品，如須更改數量請至購物車修改';
		          }
		          //寫入暫存購物車-有登入的話
	            if(@$this->session->userdata['web_member_data']){
					$this->Shop_model->save_temp_order(@$this->session->userdata['web_member_data']['member_sn'],$data);
	          	}
	          }else{
					$rowid=$wedding_website_sn;
	          }
	          //var_dump($rowid);
	          //var_dump($wedding_website_sn);
            if($addon){ //有加購
            	$this->_del_cart_addon($rowid); //先刪除所有已加購商品再新增
            	foreach($addon as $Item){
            		if(@$Item['addon_log_sn']){ //有加購
				            $data = array(
				                'id'      => $Item['associated_product_sn'].'_'.$rowid,
				                'qty'     => $Item['addon_amount'],
				                'price'   => $Item['addon_price'],
				                'name'    => $Item['product_name'],
	                            'gift_cash'    => 0,
	                			'addon_count'  => $qty, //主商品購買數
				                'addon_log_sn'  => $Item['addon_log_sn'],
				                'addon_limitation'  => $Item['addon_limitation'],
				                'addon_from_rowid'  => $rowid,
	                			'status'  => '3',
				                'coupon_code_id'  => '',  // 優惠券
				                'coupon_money'  => 0,  // 優惠金額
                				'original_order_sn'  => $original_order_sn,
				                'mutispec_stock_sn'  => $this->input->post("product_spec",true), //主商品規格
                				'category_sn'  => $category_sn,
	                			'supplier_name'  => $product['brand'] //跟隨主商品
				            );
	         			  if($wedding_website_sn){ //從婚禮網站加購商品，加購商品改列主商品
	         			  	$data['addon_log_sn']='';
	         			  	$data['addon_from_rowid']='';
	         			  	$data['addon_count']=0;
				          }
            			if(!$add_rowid=$this->_chk_cart($Item['associated_product_sn'],$this->cart->contents(),0,$rowid)){
				          //var_dump($data);exit;
			            	//var_dump($add_rowid);
				            $result_addon=$this->cart->insert($data);
				          //var_dump($result_addon);
				          }else{
					          	//$row=$this->cart->get_item($add_rowid);
											$data['rowid'] = $add_rowid;
			            //var_dump($data);
											$this->cart->update($data);
					          	//改為override
											//$error['error']='已有相同加購產品，如須更改數量請至購物車修改';
				          }
          				//寫入暫存購物車-有登入的話
            			if(@$this->session->userdata['web_member_data']){
										$this->Shop_model->save_temp_order($this->session->userdata['web_member_data']['member_sn'],$data);
									}
	            	}
            	}
            }
            //echo $this->Shop_model->_get_price($qty,$product_sn);
       			if(!@$error){
            	//echo 'ok';
	            $cart_array = array(
	                'total_items'      => count($this->cart->contents()),
	                'total'     => number_format($this->cart->total())
	            );
							echo json_encode($cart_array);
            }else{
            	//$this->cart->destroy();
							echo json_encode($error);
           }
      	//}
	    }else{
					echo json_encode(array('error'=>'no product'));
	    }
    }
	private function _del_cart_addon($rowid){   //清除購物車主商品rowid的所有加購商品
		if($rowid){
			foreach($this->cart->contents() as $key=>$Item){
					if($Item['addon_from_rowid']==$rowid){
							$this->cart->remove($key); //刪除加購商品
					}
			}
			return true;
		}else{
			return false;
		}
	}
	private function _chk_cart($product_sn,$cart,$mutispec_stock_sn=0,$rowid=0){   //比對購物車是否已有該產品
		if($product_sn && $cart){
			foreach($cart as $key=>$Item){
				if($mutispec_stock_sn){
					if($Item['id']==$product_sn.'_'.$mutispec_stock_sn){
						return $key;
					}
				}elseif($rowid){
					if($Item['id']==$product_sn.'_'.$rowid){
						return $key;
					}
				}else{
					if($Item['id']==$product_sn.'_'){
						return $key;
					}
				}
			}
			return false;
		}else{
			return false;
		}
	}
	public function cartdel(){
		$table=base64_decode($this->input->post('dtable'));
		$field=base64_decode($this->input->post('dfield'));
		$id=$this->input->post('did');  //rowid
		if($table=='cart' && $field && $id){
			if($row=$this->cart->get_item($id)){ //檢查是否有該商品
				if($row['addon_count'] > 0){ //有加購
					if($cart=$this->cart->contents()){
						foreach($cart as $key=>$Item){
							if($Item['addon_from_rowid']==$id){ //找出加購rowid
								$this->cart->remove($Item['rowid']); //刪除加購
		            if(@$this->session->userdata['web_member_data']){
						$this->Shop_model->update_temp_order($Item['id'],$Item,$this->session->userdata['web_member_data']['member_sn'],2); //2刪除
		          	}
							}
						}
					}
				}
		        if(@$this->session->userdata['web_member_data']){
					$this->Shop_model->update_temp_order($row['id'],$row,$this->session->userdata['web_member_data']['member_sn'],2); //2刪除
		      	}
				$this->cart->remove($id);
		        $cart_array = array(
			        'total_items'      => count($this->cart->contents()),
		            'total'     => number_format($this->cart->total())
		        );
				if($row['gift_cash'] > 0){ //有使用點數折抵加回
        		 $gift_cash=intval($this->session->userdata('gift_cash'))+intval($row['gift_cash']);
				     $this->session->set_userdata('gift_cash',$gift_cash);
        		 $cart_array['gift_cash']=$gift_cash;
				}
				echo json_encode($cart_array);
			}
		}
	}
	//更新購物車
	public function cartupdate(){
		$dval=trim($this->input->post('dval',true));
		$field=base64_decode($this->input->post('dfield'));
		$id=$this->input->post('did');  //rowid
		$supplier_sn=$this->input->post('supplier_sn');
		if($field && $id){
			if($row=$this->cart->get_item($id)){ //檢查是否有該商品
	        $data = array(
		        'rowid' => $id,
		        $field  => $dval
			);
        if($field=='gift_cash'){
	        if($row['gift_cash'] > 0 && $dval=='0'){ //取消點數折抵加回
        		 $gift_cash_after=intval($this->session->userdata('gift_cash'))+intval($row['gift_cash']);
				     //$this->session->set_userdata('gift_cash',$gift_cash_after);
	        }else{ //使用點數折抵扣除
	        	if($this->session->userdata('gift_cash') >= $dval){
	        	 	$gift_cash_after=intval($this->session->userdata('gift_cash'))-intval($dval);
	        	}else{
	        		$gift_cash_after=0;
	        		$dval=$this->session->userdata('gift_cash'); // 剩下全用
					$data[$field]=$dval;
	        	}
        		 //$cart_array['gift_cash']=$gift_cash_after;
	        }
			$this->session->set_userdata('gift_cash', $gift_cash_after);
		}elseif($field=='coupon_code'){
					if($dval=='del_coupon_code'){ //刪除優惠
						$data['coupon_code_id']='';
						$data['coupon_money']=0;
						unset($data['coupon_code']);
						//$data['qty']=$row['qty'];
						//$data['price']=$row['price'];
						//var_dump($data);
					  $this->session->set_userdata('coupon_data', '');
						//$this->cart->update($data);
						//echo json_encode(array('error'=>'該優惠已刪除'));
						//exit();
					}else{
						//var_dump($this->session->userdata('coupon_data'));
						if(!$this->session->userdata('coupon_data') || $this->session->userdata('last_coupon_code')==$dval){
							if($dval!=@$this->session->userdata('coupon_data')['coupon_code'] || $id!=@$this->session->userdata('coupon_data')['rowid']){
								//$this->session->set_userdata('coupon_data',''); //清空整筆優惠
								$data['coupon_code_id']=$this->Shop_model->_get_coupon($dval,0,0,$row['id']);
								if(is_numeric($data['coupon_code_id'])){
						//var_dump($row['coupon_code_id']);
						//var_dump($data['coupon_code_id']);  $this->Shop_model->_get_coupon(0,$row['coupon_code_id'])['coupon_code']
									if($this->Shop_model->_get_coupon(0,@$row['coupon_code_id'])['coupon_code']!=$this->Shop_model->_get_coupon(0,$data['coupon_code_id'])['coupon_code']){
										$coupon=$this->Shop_model->_get_coupon(0,$data['coupon_code_id'],1);//帶出優惠金額或折扣 0:整筆訂單
										if($coupon && $coupon < 1){ //折扣
											//$coupon_money=round($row['subtotal']*(1-$coupon));
											$coupon_money=$coupon;
											$_coupon_data=$this->Shop_model->_get_coupon(0,$data['coupon_code_id']);
											$_coupon_data['rowid']=$id;
									    $this->session->set_userdata('coupon_data', $_coupon_data);
										}elseif($coupon){ //金額
											$coupon_money=$coupon;
											$_coupon_data=$this->Shop_model->_get_coupon(0,$data['coupon_code_id']);
											$_coupon_data['rowid']=$id;
									    $this->session->set_userdata('coupon_data', $_coupon_data);
										}else{ //整筆
											$coupon_money=$coupon;
									    $this->session->set_userdata('coupon_data', $this->Shop_model->_get_coupon(0,$data['coupon_code_id']));
									    $_temp_order=$this->Shop_model->get_temp_order($this->session->userdata['web_member_data']['member_sn'],0,0,1);
									    //var_dump($_temp_order);
											if(is_array($_temp_order)){ //正常
												if($_temp_order['total_items'] > 0){
													$this->_clear_cart_item_coupon();
													echo json_encode(array('error'=>'reload')); //整筆訂單優惠清除單品優惠並重整畫面
													exit();
												}else{
													echo json_encode(array('error'=>'您輸入的優惠券適用整筆訂單，請先勾選欲結帳的商品。'));
													exit();
												}
											}else{
												echo json_encode(array('error'=>$_temp_order));
												exit();
											}
										}
										$data['coupon_money']=$coupon_money;
										if($coupon_money > 1){ //優惠是金額
											if($row['subtotal']< $data['coupon_money']){ //商品價格低於優惠金額
												$data['coupon_money']=$row['subtotal'];
											}
										}
									}else{
										echo json_encode(array('error'=>'該優惠券已經輸入而且已經套用嚕。'));
										exit();
									}
								}else{
									echo json_encode(array('error'=>$data['coupon_code_id']));
									exit();
								}
							}else{
									//var_dump(@$row['coupon_code_id']);
									echo json_encode(array('error'=>'該優惠券已經輸入而且已經套用嚕!'));
									exit();
							}
						}else{
								$this->session->set_userdata('last_coupon_code',$dval);
								echo json_encode(array('error'=>'confirm_change_coupon'));
								exit();
						}
					}
				}elseif($field=='mutispec_stock_sn'){ //更改規格
				    if(strpos($row['id'],'_')!==false){ // 過濾_
		  				$row['id']=substr($row['id'],0,strpos($row['id'],'_'));
		  		    }
          			$data['price']=$this->Shop_model->_get_price($row['qty'],$row['id'],$dval);
					$data['id']=$row['id'].'_'.$dval;
				//}elseif($field=='price'){ //更改價錢
				//		$data['price']=$dval;
				}elseif($field=='qty'){ //更改數量更新售價
					//var_dump($row);
					if(!$row['addon_log_sn']){ //加購不更新售價
						$data['price']=$this->Shop_model->_get_price($dval,$row['id'],$row['mutispec_stock_sn']);
					}
					//找是否有加購商品一併更改其主商品數量
					foreach($this->cart->contents() as $_item){
						if($_item['addon_from_rowid']==$row['rowid']){
					        $add_data = array(
						        'rowid' => $_item['rowid'],
						        'addon_count'  => $data['qty']
							);
							//var_dump($add_data);
							$this->cart->update($add_data);
						}

					}
        		}
				$this->cart->update($data);
				$this->Shop_model->update_temp_order($row['id'],$this->cart->get_item($id),$this->session->userdata['web_member_data']['member_sn'],3,$id,0,$supplier_sn);

				if($field=='qty' || $field=='mutispec_stock_sn'){ // 改變數量或規格可能變動價錢
					$row=$this->cart->get_item($id); //再抓一次購物車
					if($row['gift_cash']){ // 有勾選使用購物金更新購物金
						//$data['gift_cash']=$dval*$row['price'];
						$field='gift_cash';
	      		 $gift_cash=intval($this->session->userdata('gift_cash'))+intval($row['gift_cash']); //先加回
	      		 	if($gift_cash > $row['subtotal']){
	      		 		$gift_cash_after=$gift_cash-$row['subtotal'];
								$data = array(
								        'rowid' => $id,
								        'gift_cash'  => $row['subtotal']
								);
	      			}else{
	      				//剩下全用
								$data = array(
								        'rowid' => $id,
								        'gift_cash'  => $gift_cash
								);
	      		 		$gift_cash_after=0;
	      			}

							$this->cart->update($data);
				     $this->session->set_userdata('gift_cash',$gift_cash_after);
					}
					if($row['coupon_code_id']){ // 有使用優惠代碼
						//$data['gift_cash']=$dval*$row['price'];
							$coupon=$this->Shop_model->_get_coupon(0,$row['coupon_code_id'],1);//帶出優惠金額或折扣 0:整筆訂單
	      				//var_dump($row['subtotal']);
	      				//var_dump($row['coupon_money']);
	      				//var_dump($coupon);
	      		 	if($coupon > $row['coupon_money'] && $row['subtotal']> 0){ //金額且大於原先優惠金額需更新
								if(($row['subtotal']+$row['coupon_money']) < $coupon){ //商品價格低於優惠金額
									$coupon=$row['subtotal']+$row['coupon_money'];
								}
								$data = array(
								        'rowid' => $id,
								        'coupon_money'  => $coupon
								);
								$this->cart->update($data);
	      			}elseif($row['subtotal']< 0){ //優惠金額且大於小計需更新
								$data = array(
								        'rowid' => $id,
								        'coupon_money'  => $row['subtotal']+$row['coupon_money']
								);
								$this->cart->update($data);
	      			}
					}
				}
        //if(@$this->session->userdata['web_member_data']){
					//$cart_array=$this->Shop_model->update_temp_order($row['id'],$this->cart->get_item($id),$this->session->userdata['web_member_data']['member_sn'],$row['status']);
					//var_dump($cart_array);
	        //$gift_cash=$this->Shop_model->_get_gift_cash($this->session->userdata['web_member_data']['member_sn']); //最新餘額
      	//}
				$row=$this->cart->get_item($id); //再抓一次購物車
				//var_dump($row);
				//exit();

				if($row['coupon_money'] > 0 && $row['coupon_money'] < 1){ //優惠是折扣->重算優惠金額 $row['subtotal'] =已經扣掉優惠
					$row['coupon_money']=round(($row['qty']*$row['price'])*(1-$row['coupon_money']));
				}
        $cart_array = array(
	          'total_items'      => count($this->cart->contents()),
            'subtotal'         => $row['subtotal'],
            'price'            => $row['price'],
            'total'            => number_format($this->cart->total()),
            'coupon_money'     => $row['coupon_money'],
            'gift_cash'        => @$gift_cash_after
        );
					//var_dump($cart_array);
				echo json_encode($cart_array);
			}
		}
	}
	//清除購物車已輸入的所有單品優惠
	private function _clear_cart_item_coupon(){
		if($cart=$this->cart->contents()){
			foreach($cart as $_key=>$_Item){
					$_data = array(
					        'rowid' => $_Item['rowid'],
					        'coupon_money'  => 0,
					        'coupon_code_id'  => ''
					);
				$this->cart->update($_data);
			}
			return true;
		}else{
			  return false;
		}
	}

	//更新結帳清單商品
	public function checkoutupdate(){
		if(@$this->session->userdata['web_member_data']['member_sn']){
			$dval=$this->input->post('dval');
			$field=base64_decode($this->input->post('dfield'));
			//echo $field;
			$id=$this->input->post('did');  //rowid
			$supplier_sn_1=$this->input->post('supplier_sn');
			$money_field=$this->input->post('money_field');
			if($field=='ifcheckout' && $id){
				if($row=$this->cart->get_item($id)){ //檢查是否有該商品
					$data = array(
					    'rowid' => $id,
					);
					//var_dump($dval);
					if($dval=='true'){ //勾選商品
						$status='3';
						$data['status']='3';
					}else{
						$status='1';
						$data['status']='1';
					}
					$this->cart->update($data);
					if($row['addon_count']){ //  有加購商品
						$rowid=$id;
					}else{
						$rowid=0;
					}
					$down_cart_array = $this->Shop_model->update_temp_order($row['id'],$this->cart->get_item($id),$this->session->userdata['web_member_data']['member_sn'],$status,$rowid,0,$supplier_sn_1); //3確定購買
					//var_dump($down_cart_array);
					if($down_cart_array===null) $down_cart_array=array();
	        //$cart_array = array(
		      //    'total_items'      => count($this->cart->contents()),
	        //    'total'     => number_format($this->cart->total())
	       // );
						echo json_encode($down_cart_array);
					}else{
						if($id=='method_shipping'){
							$down_cart_array = $this->Shop_model->get_temp_order($this->session->userdata['web_member_data']['member_sn'],0,$dval,0,$money_field,$supplier_sn_1); //選擇配送方式決定運費
							//var_dump($down_cart_array);
							if(!$down_cart_array){
								$down_cart_array['error']='請先勾選欲結帳的商品!';
							}
							echo json_encode($down_cart_array);
						}else{
							//可能是從購物車刪除某樣商品，改更新購物車下方資料
							//echo json_encode(array('error'=>'-3')); //例外
			//var_dump($supplier_sn_1);
							$down_cart_array = $this->Shop_model->get_temp_order(@$this->session->userdata['web_member_data']['member_sn'],0,0,0,'',$supplier_sn_1); //純更新
							echo json_encode($down_cart_array);
						}
					}
				}
		}else{
					echo json_encode(array('error'=>'-1')); //未登入
		}
	}

	public function GetPackage_qty_in_stock(){   //AJAX 抓規格庫存
		$product_package_config_sn = $this->input->post('package',TRUE);
		$product_sn = $this->input->post('product',TRUE);
		if($product_package_config_sn && $product_sn){
			$Package=$this->libraries_model->_get_Package_by_cat($categorys,$product_sn);
			//var_dump($Package);
			//exit();
			echo json_encode($Package);
			//$this->db->stop_cache();
		}
	}
	public function Get_Template(){     	//版型列表
		//var_dump($this->Shop_model->Get_Template());
		echo json_encode($this->Shop_model->Get_Template());
		//echo json_encode($this->Shop_model->Get_Template());
	}
	/*
	*
  * 檢查是否有登入
	*/

	//$this->LoginCheck();
	public function LoginCheck($ifredirect=1,$HTTP_REFERER=0)
	{
			if($this->session->userdata('web_login')== true)
			return true;
			else
			if($ifredirect==1){
				if($HTTP_REFERER) $this->session->set_flashdata("HTTP_REFERER",  $HTTP_REFERER); //紀錄返回
				$this->session->set_flashdata('message','請先登入會員');
				redirect(base_url("member/memberSignIn"));
			}else{
				return false;
			}
			exit();
	}
	public function delivery_update(){
		$dval=trim($this->input->post('dval',true));
		$field=base64_decode($this->input->post('dfield'));
		$id=$this->input->post('did');  //rowid
		$receiver_addr_state=trim($this->input->post('receiver_addr_state',true));
		//$this->session->set_userdata('method_shipping','');
		if($field=='delivery_method' && $id && $dval){
			$delivery_method=$this->session->userdata('method_shipping');
			$delivery_method[0]=$receiver_addr_state;
			$delivery_method[$dval]=$id;
			$this->session->set_userdata('method_shipping',$delivery_method);
			if($receiver_addr_state=='台灣本島'){
				$money_field='delivery_amount';
			}else if($receiver_addr_state=='外島地區'){
				$money_field='outland_amount';
			}
			$temp_shopping_cart_order_sn=$this->Shop_model->_get_temp_shopping_cart_order_sn($this->session->userdata['web_member_data']['member_sn']);
			$supplier_ship=$this->Shop_model->get_supplier_ship($dval,$temp_shopping_cart_order_sn,$id,$money_field);
			/*if(is_array($supplier_ship)){
				echo json_encode($supplier_ship);
			}else{
				echo $supplier_ship;
			}*/
			echo json_encode($supplier_ship);
			/*if($this->session->userdata('delivery_method')){
				foreach($delivery_method as $_key=>$_item){

				}
				$delivery_method[count($delivery_method)]=array('supplier_sn'=>$dval,'delivery_method'=>$id);
				$this->session->set_userdata('delivery_method',$delivery_method);
			}else{
			}*/
			//var_dump($this->session->userdata('method_shipping'));
		}else{
			echo json_encode(array('error'=>'資料不足！'));
		}
	}
}
