<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 客服中心往返的 msg 列表
*/
$ccapply_detail_code_name=trim($_result['member_question'][0]['ccapply_detail_code_name']);
?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
  <h4 class="modal-title">訊息記錄 &nbsp; 主題：<?php echo $_result['member_question'][0]['question_subject']?></h4>
</div>
<div class="modal-body">
  <div class="portlet box">
    <div class="portlet-body" id="chats">
      <ul class="chats">問題說明：<br><?php echo nl2br($_result['member_question'][0]['question_description']);?>
<?php	if(@$_result['qna_log']){foreach(@$_result['qna_log'] as $Item){?>
        <li class="<?php echo ($Item['cc_reply_flag'])?'in':'out';?>">
          <img class="avatar" alt="" src="public/img/<?php echo ($Item['cc_reply_flag'])?'icon-supportfemale-48':'icon-diamond-48';?>.png">
          <div class="message">
            <span class="arrow">
            </span>
            <a href="javascript:;" class="name">
            <?php echo $Item['process_ccagent_member_name'];?> </a>
            <span class="datetime">
            at <?php echo $Item['qna_date'];?> </span>
            <span class="body">
            	<?php echo nl2br($Item['qna_content']);?>
            </span>
          </div>
        </li>
<?php }}?>

      </ul>
    </div>
  </div>
</div>
<?php if(!isset($done) || $done=== false):?>
  <!-- 若 ticket 狀態尚未完成則顯示 -->
<form role="form" name="reply_form" method="post" action="admin/admin/addCallcenter">
  <div class="modal-footer">
    <div class="row">
	      <div class="form-group">
          <label class="col-lg-3 control-label">回覆內容</label>
      			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8">
	        			<textarea name="reply" id="reply" class="form-control" rows="3"></textarea>
<? if($ccapply_detail_code_name=='聯絡我們'){?>
            <span class="help-block"> 此為連絡我們請直接連絡填寫者 </span>
<?}?>
	      		</div>
	      </div>
      <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9">
                    <div class="form-group">
                      <label style="width: 135px;" class="col-md-4 control-label">問題狀態</label>
		      						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
		                      <select name="question_status" id="question_status" required class="form-control">
		                        <?php foreach($_question_status as $_Item){ $ifselected='';
		                        	if($_Item['member_question_status']==$_result['member_question'][0]['question_status']) $ifselected='selected';?>
		                        <option value="<?php echo $_Item['member_question_status']?>" <?php echo $ifselected;?> ><?php echo $_Item['member_question_status_name']?></option>
		                        <?php }?>
		                      </select>
		                  </div>
                    </div>
                    <div class="form-group">
                      <label style="width: 135px;" class="col-md-4 control-label">結案方式</label>
		      						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
		                      <select name="cc_faq_close_code" id="cc_faq_close_code" class="form-control">
                        		<option value="">請選擇</option>
		                        <?php foreach($_question_close_status as $_Item){ $ifselected='';
		                        	if($_Item['cc_question_close_code']==$_result['member_question'][0]['cc_faq_close_code']) $ifselected='selected';?>
		                        <option value="<?php echo $_Item['cc_question_close_code']?>" <?php echo $ifselected;?> ><?php echo $_Item['cc_question_close_name']?></option>
		                        <?php }?>
		                      </select>
		                  </div>
                    </div>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3">
        <button style="margin-top: 20px;margin-right: 100px;" type="submit" id="replybutton" class="btn blue">送出</button>
        <input type="hidden" name="supplier_name" value="<?php echo $_result['member_question'][0]['supplier_name']; ?>">
        <input type="hidden" name="member_question_sn" value="<?php echo @$_result['member_question'][0]['member_question_sn']; ?>">
        <input type="hidden" name="member_sn" value="<?php echo $_result['member_question'][0]['q_member_sn']; ?>">
      </div>
    </div>
  </div>
</form>
  <!-- /若 ticket 狀態尚未完成則顯示 -->
<?php endif;?>