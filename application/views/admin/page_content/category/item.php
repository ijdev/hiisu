<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>內容分類 <small>分類設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">分類設定</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/content/contentCategory" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post">
              <div class="form-body">
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">部落格 / FAQ</label>
                  <div class="col-lg-8">
                    <select name="ctype[blog_faq_flag]" class="form-control">
                      <option value="1" <?php echo ($Item['blog_faq_flag']=='1')?'selected':'';?>>部落格</option>
                      <option value="2" <?php echo ($Item['blog_faq_flag']=='2')?'selected':'';?>>FAQ</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">頻道</label>
                  <div class="col-lg-8">
                    <div class="checkbox-list">
                      <?/*<label class="checkbox-inline">
                      <input type="checkbox" id="inlineCheckbox1" name="ctype[post_channel_name_set][]" <?php echo (strrpos($Item['post_channel_name_set'], "首頁")!==false)?'checked':'';?> value="首頁"> 首頁 </label>*/?>
                    <?php foreach($Store as $key=>$_Item){ if($key){?>
                      <label class="checkbox-inline">
                      <input type="checkbox" require name="ctype[post_channel_name_set][]" <?php echo (strrpos($Item['post_channel_name_set'], $_Item)!==false)?'checked':'';?> value="<?php echo $_Item;?>"> <?php echo $_Item;?> </label>
                    <?php }}?>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">名稱 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input type="text" name="ctype[content_title]" class="form-control" value="<?php echo @$Item['content_title']; ?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-3 control-label">說明</label>
                  <div class="col-lg-8">
                    <textarea class="form-control" name="ctype[content_description]" rows="3"><?php echo @$Item['content_description']; ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="seq" class="col-lg-3 control-label">排序 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input name="ctype[sort_order]" maxlength="4" type="text" class="form-control" value="<?php echo (@$Item['sort_order']) ? @$Item['sort_order']:'9999'; ?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">是否啟用 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <select name="ctype[post_flag]" class="form-control" required>
                      <option value="1" <?php echo (@$Item['post_flag']==1) ? 'selected':''; ?> >是</option>
                      <option value="0" <?php echo (@$Item['post_flag']==0) ? 'selected':''; ?> >否</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-lg-3 control-label">更新者</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['update_member_sn'])?@$Item['update_member_sn']:@$Item['create_member_sn']; ?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-3 control-label">更新時間</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['last_time_update']!='0000-00-00 00:00:00')?@$Item['last_time_update']:@$Item['create_date']; ?>" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <input type="hidden" name="content_type_sn" value="<?php echo @$Item['content_type_sn']; ?>">
                    <input type="hidden" name="old_content_title" value="<?php echo @$Item['content_title']; ?>">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="button" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->