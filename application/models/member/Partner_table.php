<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Partner_table extends CI_Model {
    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('sean_models/Sean_db_tools');
		/* Begin 修改  */
		$this->_ct_name="聯盟會員管理";// 功能名稱
		$this->_url="Member/partnerTable/";//列表功能url
		$this->_url_item="Member/partnerItem/";//處理動作 url
		$this->_table="ij_member";//table name
		$this->_id="member_sn";//table key
		$this->_table_field=array(
					 "user_name" => "姓",
					 "last_name" => "姓",
					 "first_name" => "名",
					 "tel" => "市話",
					 "cell" => "手機",
					 "birthday" => "生日",
					 "addr1" => "住址",
					 "fax" => "住址",
					 "addr_city_code" => "city",
					 "addr_town_code" => "town",
					 "zipcode" => "zipcode",
					 "email" => "email",
					 "member_status" => "帳號狀態",
					 "approval_memo" => "帳號狀態",
				);




				/*
					 address model
					addr_country_code  addr_country
					add_state_code  addr_state
					addr_city_code  addr_city
					addr_town_code  addr_town
					zipcode  addr1 addr2
				*/

				//
				//"owner_dealer" => "關聯廠商",

		/* End 修改  */

    }

    //取得列表內容
	public function get_list($offset , $perpage , $where , $_like = 0) {
		$this->db->select("
			ij_member.member_sn,
			ij_member.email,
			ij_member.last_name,
			ij_member.cell,
			partner_level_name,
			domain_name,
			ij_member.member_status,
			ij_member.user_name,
			ij_member.last_time_update,
			ij_city_ct.city_name,
			ij_town_ct.town_name,
		");
		$this->db->from("ij_member");
		$this->db->join("ij_city_ct","ij_city_ct.city_code = ij_member.addr_city_code","left");
		$this->db->join("ij_town_ct","ij_town_ct.town_code = ij_member.addr_town_code","left");
		$this->db->join("ij_partner","ij_partner.member_sn = ij_member.member_sn","left");
		$this->db->join("ij_biz_domain","ij_biz_domain.domain_sn = ij_partner.domain_sn","left");
		$this->db->join("ij_partner_level_config","ij_partner_level_config.partner_level_sn = ij_partner.partner_level_sn","left");
		$this->db->where('member_level_type',2);//聯盟會員
		$this->db->where('system_user_flag',0);//非管理者
		$this->db->where($where);
		if($_like){
			$this->db->like($_like);
		}
		$this->db->order_by("ij_partner.domain_sn",'asc');
		$this->db->order_by("ij_member.addr_city_code",'asc');
		if($perpage || $offset){
			$this->db->limit($perpage,$offset);
		}

		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}

	public function action_edit( $_where = "") {
		$_result="";
		$_result=$this->Sean_db_tools->db_get_max_record("*",$this->_table,$_where);
		return $_result;
	}

	public function action_insert() {
		$_result="";
		foreach($this->_table_field as $key => $value)
		{
			if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
			$_data[$key]=$this->input->get_post($key);
		}
		if(strlen($this->input->get_post("password")) > 0 )
		$_data["password"]=md5($this->input->get_post("password"));

		//$_data["email"]=$_data["user_name"];
		$_data["member_level_type"]=2;
		$_data["system_user_flag"]=0;
		if($this->libraries_model->_select('ij_member',array('ij_member.user_name'=>$_data["user_name"]),0,0,0,'member_sn',0,'num_rows')){
			$this->ijw->_wait(0 , 2 , '帳號重複，請更改',1);
			return false;
		}
		//basic insert
		//$_data["create_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
		//$_data["create_date"]=date("Y-m-d H:i:s");
		//basic update and insert
		$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
		$_data["last_time_update"]=date("Y-m-d H:i:s");
		$_key_data=$this->Sean_db_tools->db_insert_record($_data,$this->_table);
		$_data2["account_name"]=$this->input->get_post("account_name");
		if($_data2["account_name"]){
		    $_data2["bank_name"]=$this->input->get_post("bank_name");
		    $_data2["branch_name"]=$this->input->get_post("branch_name");
		    $_data2["bank_account"]=$this->input->get_post("bank_account");
			$_data2['associated_member_sn']=$_key_data;
			$_result2=$this->Sean_db_tools->db_insert_record($_data2,"ij_bank_info");
		}

		//ij_partner update
		$_data3=array();
		$_data3["partner_level_sn"]=$this->input->get_post("partner_level_sn");
		$_data3["domain_sn"]=$this->input->get_post("domain_sn");
		//$_data3["partnership_start_date"]=$this->input->get_post("partnership_start_date");
		//$_data3["partnership_end_date"]=$this->input->get_post("partnership_end_date");
		$_data3["partner_website_name"]=$this->input->get_post("partner_website_name");
	    $_data3["partner_website_url"]=$this->input->get_post("partner_website_url");
	    $_data3["partner_chairman"]=$this->input->get_post("partner_chairman");
	    $_data3["partner_company_name"]=$this->input->get_post("partner_company_name");
        $_data3["dealer_tax_id"]=$this->input->get_post("dealer_tax_id");
		$_data3['member_sn']=$_key_data;
		$_data3["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
		$_data3["last_time_update"]=date("Y-m-d H:i:s");
		$_data3["create_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
		$_data3["create_date"]=date("Y-m-d H:i:s");
		//var_dump($_data3);exit;
		$_result2=$this->Sean_db_tools->db_insert_record($_data3,"ij_partner");
		return $_result2;
	}

	public function action_update($_where,$_key_id,$_key_data) {

		$_result="";

		foreach($this->_table_field as $key => $value)
		{
				if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
				$_data[$key]=$this->input->get_post($key);
		}

		if($this->libraries_model->_select('ij_member',array('ij_member.user_name'=>$_data["user_name"],'ij_member.member_sn!='=>$_key_data),0,0,0,'member_sn',0,'num_rows')){
			//$this->session->set_flashdata("fail_message","帳號重複，請更改");
			return false;
		}
		//$_data["addr_country_code"] ="886";
		//$_data["add_state_code"] ="886";
		//$_data["addr_country"] ="台灣";
		//$_data["addr_state"] ="台灣";
		if(strlen($this->input->get_post("password")) > 0 )
		$_data["password"]=md5($this->input->get_post("password"));

		$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
		$_data["last_time_update"]=date("Y-m-d H:i:s");

		$_result=$this->Sean_db_tools->db_update_record($_data,$this->_table,$_where,$_key_id,$_key_data);


		$_data2["account_name"]=$this->input->get_post("account_name");
		if($_data2["account_name"]){
		    $_data2["bank_name"]=$this->input->get_post("bank_name");
		    $_data2["branch_name"]=$this->input->get_post("branch_name");
		    $_data2["bank_account"]=$this->input->get_post("bank_account");

		    $_where =" where associated_member_sn='".$_key_data."' ";
			$_key_id="associated_member_sn";
			$ij_bank_info=$this->Sean_db_tools->db_get_max_record("*","ij_bank_info",$_where);
			if($ij_bank_info){
				$_result2=$this->Sean_db_tools->db_update_record($_data2,"ij_bank_info",$_where,$_key_id,$_key_data);
			}else{
				$_data2['associated_member_sn']=$_key_data;
				$_result2=$this->Sean_db_tools->db_insert_record($_data2,"ij_bank_info");
			}
		}


		//ij_partner update
		$_data3=array();
		$_data3["partner_level_sn"]=$this->input->get_post("partner_level_sn");
		$_data3["domain_sn"]=$this->input->get_post("domain_sn");
		//$_data3["partnership_start_date"]=$this->input->get_post("partnership_start_date");
		//$_data3["partnership_end_date"]=$this->input->get_post("partnership_end_date");
		$_data3["partner_website_name"]=$this->input->get_post("partner_website_name");
	    $_data3["partner_website_url"]=$this->input->get_post("partner_website_url");
	    $_data3["partner_chairman"]=$this->input->get_post("partner_chairman");
	    $_data3["partner_company_name"]=$this->input->get_post("partner_company_name");
        $_data3["dealer_tax_id"]=$this->input->get_post("dealer_tax_id");
		$_data3["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
		$_data3["last_time_update"]=date("Y-m-d H:i:s");
		$_data3["create_member_sn"]=$this->input->get_post("create_member_sn");
		//$_data3["create_date"]=date("Y-m-d H:i:s");
		$_where =" where member_sn='".$_key_data."' ";
		$_key_id="member_sn";
		$ij_partner=$this->Sean_db_tools->db_get_max_record("*","ij_partner",$_where);
		if($ij_partner){
			$_result2=$this->Sean_db_tools->db_update_record($_data3,"ij_partner",$_where,$_key_id,$_key_data);
		}else{
			$_data3['member_sn']=$_key_data;
			$_result2=$this->Sean_db_tools->db_insert_record($_data3,"ij_partner");
		}



		//ij_member_domain_relation
		/*$_domain_sn=$this->input->get_post("domain_sn");
		$_data3["status"]=1;
		$_data3["associated_member_sn"]=$_key_data;
		$_data3["last_time_update"]=date("Y-m-d H:i:s");
		$_data5["status"]=0;
		$_where=" where associated_member_sn='".$_key_data."'";
		$_result=$this->Sean_db_tools->db_update_record($_data5,"ij_member_domain_relation",$_where,"associated_member_sn",$_key_data);
		$_data3=array();
		foreach($_domain_sn as $akey => $avalue)
		{
			$_data3["domain_sn"]=$avalue;
			$_data3["status"]=1;
			$_data3["associated_member_sn"]=$_key_data;
			$_data3["last_time_update"]=date("Y-m-d H:i:s");

			$_result=$this->Sean_db_tools->db_insert_record($_data3,"ij_member_domain_relation");
		}*/
		return $_result;
	}
	public function get_newmember($partner_sn=0,$member_sn=0){
		$this->db->select("ij_partner.member_sn,newmember,bonus");
		$this->db->from("ij_partner");
		$this->db->join("ij_member","ij_member.member_sn = ij_partner.member_sn","left");
		$this->db->join("ij_partner_level_config","ij_partner_level_config.partner_level_sn = ij_partner.partner_level_sn","left");
		$this->db->where('member_level_type',2);//聯盟會員
		$this->db->where('member_status',2);//有效
		if($partner_sn)	$this->db->where('partner_sn',$partner_sn);
		if($member_sn)	$this->db->where('ij_partner.member_sn',$member_sn);
		$query = $this->db->get()->row_array();
		if($query){
			return $query;
		}else{
			return false;
		}
	}
	public function action_delete( $_where) {

		$_result="";

		$_result=$this->Sean_db_tools->db_delete_record($this->_table,$_where);

		return $_result;

	}

	//取得新排序的值
	public function get_next_sort($_id=0 ) {

		$_result="0";

		$_where=" where ".$this->_id." > 0 order by sort_order desc ";
		$_result=$this->Sean_db_tools->db_get_one_record("sort_order",$this->_table,$_where);

		if(is_array($_result["sort_order"]) )
		$_result=1;
		else
		$_result=$_result["sort_order"]+1;

		return $_result;

	}

	//取得定義參數值
	public function get_define_value($_id)
	{
		if(isset($this->$_id))
		return $this->$_id;
		else
		return "";
	}

}
