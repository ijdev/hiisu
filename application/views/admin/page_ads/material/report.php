<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>廣告報表查詢</h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
        廣告報表查詢
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">廣告報表查詢</span>
            </div>
            <div class="actions btn-set">
            </div>
          </div>
          <div class="portlet-body">
            <div class="note note-warning">
              <div class="input-group input-large date-picker input-daterange  pull-left" data-date="2015-05-07" data-date-format="yyyy-mm-dd">
                <span class="input-group-addon" onclick="alert('Start Search');"> 期間 </span>
                <input type="text" class="form-control input-sm" name="from" value="2015-05-07">
                <span class="input-group-addon"> 到 </span>
                <input type="text" class="form-control input-sm" name="to" value="2015-05-13">
              </div>
              <div class="table-actions-wrapper pull-right">
                <span>
                </span>
                <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="12" style="margin: 0 5px;" placeholder="素材名稱" value="廣告素材名稱">
                <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="12" style="margin: 0 5px;" placeholder="客戶名稱" value="客戶名稱">
                <select class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;">
                  <option value="">頻道</option>
                  <option value="">首頁</option>
                  <option value="">新人編輯首頁</option>
                </select>
                <select class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;">
                  <option value="">版位</option>
                  <option value="">版位一</option>
                  <option value="">版位二</option>
                </select>
                <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button>
              </div>
              <div style="clear:both;"></div>
            </div>
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>客戶名稱</th>
                  <th>廣告素材名稱</th>
                  <th>廣告版位名稱</th>
                  <th>頻道</th>
                  <th>版位</th>
                  <th>View</th>
                  <th>Click</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=0;$i<12;$i++):?>
                  <tr class="odd gradeX">
                    <td>客戶名稱</td>
                    <td>素材命名廣告名稱<?php echo $i;?></td>
                    <td>版位名稱版位名稱</td>
                    <td>
                      <?php if($i%2==0):?>
                        首頁
                      <?php else:?>
                        新人編輯首頁
                      <?php endif;?>
                    </td>
                    <td>
                      <?php if($i%2==0):?>
                        版位一
                      <?php else:?>
                        版位二
                      <?php endif;?>
                    </td>
                    <td>100</td>
                    <td>10</td>
                  </tr>
                <?php endfor;?>
              </tbody>
            </table>

            <div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                  <ul class="pagination" style="visibility: visible;">
                    <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                    <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li class="active"><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                    <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->


      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->