<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<style>
  .select2_dealer,.select3_dealer{height:28px;}
</style>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>經銷商 <small>廠商列表</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">經銷商</span>
            </div>
            <div class="actions btn-set">
              <a href="<?php echo $_action_link;?>" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <div class="portlet-body">
            <div class="note note-warning" id="search_filter">
              
            
              <div class="table-actions-wrapper pull-right">
                <span>
                </span>
                <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="8" style="margin: 0 5px;" placeholder="關鍵字">
                <select class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;">
                  <option value="">產業屬性</option>
                  <option value="">飯店</option>
                  <option value="">婚紗</option>
                </select>
                <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button>
              </div>
              <div style="clear:both;"></div>
            </div>
            <!-- End of search_filter -->
            <?php $this->load->view('sean_admin/general/flash_error');?>
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>姓名</th>
                  <th>Email</th>
                  <!-- <th>縣市</th>
                  <th>鄉鎮市區</th>
                  <th>住址</th> -->
                  <th>手機</th>
                  <th>結婚日期</th>
                  <th>會員等級</th>
                  <th>帳號狀態</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
               <?php
                 $_member_status_ct=$this->config->item("member_status_ct");
								$_member_level_type_ct=$this->config->item("member_level_type_ct");
								
                if(is_array($_result01))
                	{                                  
                 			foreach($_result01 as $key => $value)
                    		{                    			                    			                    			
                    			
                  ?>
                  
                  
                  <tr class="odd gradeX">
                    <td><?php echo $value->last_name.$value->first_name;?></td>
                    <td><?php echo $value->email;?></td>
                   <!--  <td><?php echo $value->addr_city;?></td>
                    <td><?php echo $value->addr_town;?></td>
                    <td><?php echo $value->addr1;?></td>-->
                    <td class="center"><?php echo $value->cell;?></td>
                    <td class="center"><?php echo $value->wedding_date;?></td>
                    <td class="center"><?php echo $_member_level_type_ct[$value->member_level_type];?></td>
                    <td><?php echo $_member_status_ct[$value->member_status];?> </td>
                    <td class="center">
                      <a href="<?php echo $init_control;?>Member/memberTable_item/edit/<?php echo $value->member_sn;?>"><i class="fa fa-edit"></i>編輯</a>&nbsp;&nbsp;
                      <a href="javascript:;"><i class="fa fa-circle-o-notch"></i>重發驗證信</a>&nbsp;&nbsp;
                      <a href="javascript:;"><i class="fa fa-lightbulb-o"></i>忘記密碼</a>
                    </td>
                  </tr>
                <?php } } ?>
                
              </tbody>
            </table>
           
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->