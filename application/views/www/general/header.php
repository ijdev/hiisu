<?php defined('BASEPATH') OR exit('No direct script access allowed');
$_web_member_data=$this->session->userdata('web_member_data');
//var_dump(strpos($this->uri->segment(2),'shopView'));
?>
<style>
.main-nav-scrolled {
  position: fixed;
  width: 100%;
  top: 0;
  z-index: 999999;
}
.langs-block a.drop_down_menu span {
    width: 20px;
    height: 20px;
    position: absolute;
    right: 11px;
    top: 0;
}
.header-navigation li.langs-block.pro {
    padding-left: 9px;
}
.header-navigation li.langs-block.pro a {
    display: inline-block;
}
.header-navigation li.langs-block.pro a>i {
    display: inline-block;
    width: 16px;
    margin-right: 15px;
}
@media (min-width: 769px){
.hdr-logo {
    padding: 0px;
    margin-left: 0px;
    float: left;
}
.hdr-logo .main-logo .site-logo img {
    width: 180px;
}
.hdr-frm {
    margin-left: 5%;
    margin-right: 5%;
    width: calc(100% - 469px - 12%);
    float: left;
}
.top_crt_block {
    padding: 0px;
    padding: 2% 0% 0% 0%;
    float: right;
}
}
</style>
<div class="mob-header">
<div class="top_head_bar">
	<div class="container">
		<div class="left_area">
			<div class="news-tab">
				<div id="slideshow">
				   <?php if(@$left_up){?>
					<?php if(@$left_up['banners']){
						foreach($left_up['banners'] as $key=>$_Item){?>
					            <div>
					              <p><a href="home/ad_link/<?php echo $_Item['banner_schedule_sn'];?>" title="<?php echo $_Item['banner_content_name'];?>" target="_blank"><?php echo $_Item['banner_content_save_dir'];?></a></p>
					            </div>
					<?php }}else{
						if(@$left_up){?>
				            <div>
				              <p><a href="<?php echo @$left_up['default_banner_link'];?>" title="<?php echo @$left_up['banner_location_name'];?>" target="_blank"><?php echo @$left_up['default_banner_content_save_dir'];?></a></p>
				            </div>
					<?php }}?>
				<?php }?>
				</div>
			</div>
		</div>
		<div class="right_area">
			<div class="right_txt">
				<?php if(!empty($_web_member_data["user_name"])){?>
					<!--<span class="top_b_links">
					<a href="/member/memberOrderList">。訂單查詢</a>
					<a href="/member/my/ewedding">我的婚禮網站</a>
					<a href="#">電子禮金</a>
					<a href="/member/wishList">追蹤清單</a>
					<a href="/member/notification">通知</a>
					<a href="/shop/shopView">購物車</a>
					</span>-->
				<?php }else{ ?>
				<a href="/member/memberSignUp">新會員註冊</a>
				<a href="/member/memberSignIn">登入</a>
				<a href="/home/faq">常見問題</a>
				<!--a class="top_fb_icon_txt" href="#"> 加入粉絲團 <img class="top_fb_icon" src="public/img/icon_fb.png"></a-->
<?php if(@$top_right_word){ foreach($top_right_word as $key=>$_Item){?>
				<a target="_blank" title="<?=$_Item['banner_content_name']?>" href="/home/ad_link/<?=$_Item['banner_schedule_sn']?>" ><?=$_Item['banner_content_save_dir']?> </a>
<?php }}?>
				<?php } ?>
			</div>
			<div class="left_txt">
				<?php if(!empty($_web_member_data["user_name"])){?>
				<ul class="main_ul">
					<!--<li><a href=""><span class="hi_txt">Hi! 歡迎您,   </span></a></li>-->
					<li class="sub_d_down dpmenutop">
						<a href="/member"><span class="drp_mnu" id="showmenu">會員中心</span></a>
						<div class="hdr_menu2">
							<ul class="down_mnu">
								<li><a href="/member/memberOrderList">訂單查詢</a></li>
								<li><a href="/member/memberAccount">修改資料</a></li>
								<li><a href="/member/wishList">追蹤清單</a></li>
								<li><a href="/member/notification">通知</a></li>
								<!--li><a href="/member/my/ewedding">我的婚禮網站</a></li>
								<li><a href="/shop/shopView">購物車</a></li-->
							</ul>
						</div>
					</li>
					<?if($this->page_data['unreadmessage']){?>
					<li class="sub_d_down_li dpmenutop"><a href="member/notification"><span>通知</span></a>
					<p class="i_menu "><?=$this->page_data['unreadmessage']?></p>
					</li>
					<?}?>
					<li class="dpmenutop"><a href="/member/memberSignOut">登出</a></li>
					<li class="dpmenutop"><a href="/home/faq">常見問題</a></li>
				<!--a class="top_fb_icon_txt" href="#"> 加入粉絲團 <img class="top_fb_icon" src="public/img/icon_fb.png"></a-->
<?php if(@$top_right_word){ foreach($top_right_word as $key=>$_Item){?>
				<li class="dpmenutop"><a target="_blank" title="<?=$_Item['banner_content_name']?>" href="/home/ad_link/<?=$_Item['banner_schedule_sn']?>" ><?=$_Item['banner_content_save_dir']?> </a></li>
<?php }}?>
				</ul>
				<?php }else{ ?>
				<!--<a href="#"><span class="hi_txt">Hi！歡迎您,<span></a>-->
				<?php } ?>

			</div>

		</div>
	</div>
</div>
	<div class="logo_cart_section">
		<div class="container" >
			<div class="clearfix hdr-logo" style="position:relative;"><!--col-sm-2 -->
			<div class="main-logo">
				<a class="site-logo" href="/" style="height:63px;">
					<img src="public/img/ij-logo.png" alt="HiiSU喜市集">
				</a>
				<div class="mob_search">
					<a href="javascript:;" data-remote="false" data-target="#ajax_msg" data-toggle="modal">
						<i class="fa fa-search"></i>
					</a>
				</div>
				<div class="cart_icon">
				<a href="/shop/shopView" class="">
						<p class="cart_left_box">
							<span class="top-cart-info-count"><?php
								if(@$this->cart->contents()){
									echo count($this->cart->contents());
								}else{
									echo "0";
								}
								?>
							</span>
						</p>
						<span class="cart"></span>
					</a>
				</div>
			</div>
			</div>

			<div class="hdr-frm" style="/*margin-left: 6%;margin-right: 2%;*/"><!--col-sm-4 -->
            <form action="shop/shopSearch" method="get">
				<div class="form-group srch">
					<div class="icon-addon addon-lg">
						<input type="text" name="Search" maxlength="20" required placeholder="請輸入關鍵字" class="form-control srch_bar" id="search" value="<?=@$search?>">
					</div>
					<div class="search_labl">
						<label for="search_btn" class="glyphicon glyphicon-search srch_icons" rel="tooltip" title="搜尋商品"></label>
					</div>
					<div class="txt-hdr">
						<p><span class="title2">熱門：</span>
<?php if(@$hot_search){ foreach($hot_search as $key=>$_Item){?>
						<a title="熱門關鍵字：<?=$_Item['banner_content_save_dir']?>" href="/home/hot_search_link/<?=$_Item['banner_content_sn']?>" ><?=$_Item['banner_content_save_dir']?> </a>
<?}}?>
						</p>
					</div>
					<div class="select_tag">
							<input type="hidden" value="" id="search_kind" name="category_sn">
							<input type="submit" id="search_btn" style="display:none;">
							<div class="tagsel_btn" id="tagsel_btn">
								<span class="tag_selection">全分類</span>
							</div>
							<div class="tagsel_drop">
								<ul class="tag_ul">
								<?if($root_category){foreach($root_category as $_item){?>
									<li class="main_li">
										<span class="li_item" category_sn="<?=$_item['category_sn']?>"><?=$_item['category_name']?></span>
									</li>
								<?}}?>
								</ul>
							</div>
					</div>
				</div>
			</form>

			</div>

			<div class="top_crt_block"><!--col-sm-5 -->

<?/*
				<div class="icn-head">
					<ul class="nav navbar-nav">
						<li class="dropdown faq">
<?php if(@$up_left_banner['banners']){ foreach($up_left_banner['banners'] as $key=>$_Item){?>
              <a style="padding: 0;" href="home/ad_link/<?php echo $_Item['banner_schedule_sn'];?>" title="<?php echo $_Item['banner_content_name'];?>" target="_blank"><img style="width:<?php echo $up_left_banner['width'];?>px;height:<?php echo $up_left_banner['hight'];?>px;" src="public/uploads/<?php echo $_Item['banner_content_save_dir'];?>" class="dropimg" alt="<?php echo $_Item['banner_content_name'];?>"></a>
<?php }}else{ if(@$up_left_banner){?>
              <a style="padding: 0;" href="<?php echo @$up_left_banner['default_banner_link'];?>" title="<?php echo @$up_left_banner['banner_location_name'];?>" target="_blank"><img style="width:<?php echo @$up_left_banner['width'];?>px;height:<?php echo @$up_left_banner['hight'];?>px;" src="public/uploads/<?php echo @$up_left_banner['default_banner_content_save_dir'];?>" class="dropimg" alt="<?php echo @$up_left_banner['banner_location_name'];?>"></a>
<?php }}?>
						</li>
					</ul>
				</div>

				<div class="icn-head">
					<ul class="nav navbar-nav">
						<li class="dropdown faq">
<?php if(@$up_right_banner['banners']){ foreach($up_right_banner['banners'] as $key=>$_Item){?>
              <a style="padding: 0;" href="home/ad_link/<?php echo $_Item['banner_schedule_sn'];?>" title="<?php echo $_Item['banner_content_name'];?>" target="_blank"><img style="width:<?php echo $up_right_banner['width'];?>px;height:<?php echo $up_right_banner['hight'];?>px;" src="public/uploads/<?php echo $_Item['banner_content_save_dir'];?>" class="dropimg" alt="<?php echo $_Item['banner_content_name'];?>"></a>
<?php }}else{ if(@$up_right_banner){?>
              <a style="padding: 0;" href="<?php echo @$up_right_banner['default_banner_link'];?>" title="<?php echo @$up_right_banner['banner_location_name'];?>" target="_blank"><img style="width:<?php echo @$up_right_banner['width'];?>px;height:<?php echo @$up_right_banner['hight'];?>px;" src="public/uploads/<?php echo @$up_right_banner['default_banner_content_save_dir'];?>" class="dropimg" alt="<?php echo @$up_right_banner['banner_location_name'];?>"></a>
<?php }}?>
						</li>
					</ul>
				</div>*/?>
				<div class="add_img">
<?php if(@$up_banner['banners']){ foreach($up_banner['banners'] as $key=>$_Item){?>
              <a href="home/ad_link/<?php echo $_Item['banner_schedule_sn'];?>" title="<?php echo $_Item['banner_content_name'];?>" target="_blank"><img style="width:<?php echo $up_banner['width'];?>px;height:<?php echo $up_banner['hight'];?>px;" src="public/uploads/<?php echo $_Item['banner_content_save_dir'];?>" class="dropimg" alt="<?php echo $_Item['banner_content_name'];?>"></a>
<?php }}else{ if(@$up_banner){?>
              <a href="<?php echo @$up_banner['default_banner_link'];?>" title="<?php echo @$up_banner['banner_location_name'];?>" target="_blank"><img style="width:<?php echo @$up_banner['width'];?>px;height:<?php echo @$up_banner['hight'];?>px;" src="public/uploads/<?php echo @$up_banner['default_banner_content_save_dir'];?>" class="dropimg" alt="<?php echo @$up_banner['banner_location_name'];?>"></a>
<?php }}?>
				</div>
				<div class="icn-head2" style="float:right;margin-right: 20px;">
					<ul class="nav navbar-nav">
						<li class="dropdown">
						<img class="dropimg" src="public/img/icon_bullhornnew.png"/>
						<!--<i class="icon-pencil custom_pen"></i>-->
							<a href="/home/blogList" class="toggle tnt"><b>婚禮情報</b></a>
							<ul class="dropdown-menu bullhorn_li">
<?php if(@$content_types){foreach($content_types as $key=>$_Item){?>
    <li><a href="home/blogList/<?php echo $_Item['content_title'];?>"><?php echo $_Item['content_title'];?> (<?php echo $_Item['numrows'];?>)</a></li>
<?php }}?>
								<li><a href="/home/blogList" style="text-decoration: none;">更多文章 <img src="public/img/icon_more.png"/></a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!--top bar -->
<div class="pre-header">
    <div class="container">
        <div class="row">
			<div class="col-sm-12">
				<div class="nav_menu">
            <!-- BEGIN TOP BAR LEFT PART -->
           <!-- <div class="col-md-6 col-sm-6 additional-shop-info">-->
                <ul class="list-unstyled list-inline hdr_menu">
					<?php /*<li class="hdr_menu_li1"><a style="height: auto;" class="hdr_a" href="/"><img src="public/img/icon_home.png"></a></li>*/?>
<!--menu-->
					<?php $_category_array=json_decode(json_encode($this->page_data['_category_array']), FALSE);
					//print_r($_category_array);
						function list_shop_menu2($_category_array,$category_sn,$category_name)
						{
							echo '
								<li><a href="/shop/shopCatalog/'.$category_sn.'">'.$category_name.'</a>
									<ul class="sub-menu-left-inner2">
										';
							  if(is_array($_category_array))
									{
										foreach($_category_array as $key => $value)
										{
											if($value->upper_category_sn == $category_sn)
											{
													 echo '
													  <li><a class="" href="shop/shopCatalog/'.$value->category_sn.'">'.$value-> category_name.'</a></li>
												  ';
											}
										}
									}
								  echo '
									  </ul>
									</li>
						  ';
						}

						function list_shop_menu($_category_array,$category_sn,$category_name,$main_banner)
						{
							//data-target="shop/shopCatalog/'.$category_sn.'" href="/shop/shopCatalog/'.$category_sn.'" 因為要展開先拿掉
							print '
                	<li class="langs-block">
						<a class="hdr_a" href="/shop/shopCatalog/'.$category_sn.'">'.$category_name.'</a>
						<ul class="sub_menu ie_sub_menu">
							<div class="sub-menu-left">
								<ul class="sub-menu-left-inner">';
									if(is_array($_category_array))
									{
										foreach($_category_array as $key => $value)
										{
											if($value->upper_category_sn == $category_sn)
											{
												 list_shop_menu2($_category_array,$value->category_sn,$value->category_name);
											}
										}
									}
						 print'
								</ul>
							</div>
							<div class="sub-img-right">
								<li class="sub-img"><img style="height:250px;width:570px;" class="asub-img" src="public/uploads/'.$main_banner.'"></li>
							</div>
						</ul>
					</li>
						  ';
						}
						if(is_array(@$_category_array))
						{
							foreach($_category_array as $key => $value )
							{
								if($value->upper_category_sn == 0)
								{
									 list_shop_menu($_category_array,$value->category_sn,$value->category_name,$value->main_banner);
								}
							}
						}
						?>
				<li class="langs-block cartli" <?=(strpos($this->uri->segment(2),'shopView')!==false)?'style="display:none;"':'';?>>
					<a href="/shop/shopView" class="">
						<p class="cart_left_box">
							<span class="top-cart-info-count"><?php
								if(@$this->cart->contents()){
									echo count($this->cart->contents());
								}else{
									echo "0";
								}
							?></span>  <span class="top-cart-info-value">$
							<?php
								if(@$this->cart->total()){
									echo number_format($this->cart->total());
								}else{
									echo "0";
								}
							?></span>
						</p>
						<span class="cart"></span>
					</a>
					<ul class="sub_menu sub_menu1">
						<div class="top-cart-content-wrapper" style="display:block;">
							<div id="top-cart-content" class="top-cart-content" style="display:block;">
								<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: auto;">
									<div class="scroller" style="height: auto; overflow: hidden; width: auto;">
										<div class="scroller" style="height: 250px;">
											<?php if($this->page_data['cart']){foreach($this->page_data['cart'] as $Item){
										  //echo($Item['supplier_name']!=@$supplier_name && @$supplier_name)?'</ul>':'';
												if($Item['supplier_name']!=@$supplier_name){
													?>
												<!--h4>
													<button type="button" class="btn btn-primary grey"><?php echo $Item['supplier_name'];?></button>
												</h4-->
											<?php }
											//echo($Item['supplier_name']!=@$supplier_name || !@$supplier_name)?'<ul class="">':'';?>
											  <li>
												<a href="/shop/shopItem/<?php echo $Item['product_sn'];?>">
													<?php if(!$Item['addon_log_sn']){?><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['image_path'];?>" alt="<?php echo $Item['image_alt'];?>" width="37" height="34">
													<?php }else{?>
													<span>加購</span>
													<?php }?>
												</a>
												<span class="cart-content-count">x <?php echo $Item['qty'];?></span>
												<strong><a href="/shop/shopItem/<?php echo $Item['product_sn'];?>"><?php echo $Item['name'];?></a></strong>
												<em>$<?php echo $Item['subtotal'];?></em>
												<a href="javascript:void(0);" rowid="<?php echo $Item['rowid']?>" class="top-del-goods del-goods del_btn">&nbsp;</a>
											  </li>
										  <?php $supplier_name=$Item['supplier_name']; }}else{
											echo '<ul>購物車是空的喔，快去採購吧^^</ul>';}?>
										</div>
										  <div class="text-right">
											<a href="shop/shopView" class="btn btn-default">進入購物車</a>
											<!--a href="shop/shopCheckout" class="btn btn-primary">前往結帳</a-->
										  </div>
									</div>
									<div class="slimScrollBar" style="background: rgb(187, 187, 187) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 250px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(234, 234, 234) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
								</div>
								<!--<div class="text-right">
									<a href="shop/shopView" class="btn btn-default">進入購物車</a>
									<a href="shop/shopCheckout" class="btn btn-primary">前往結帳</a>
								</div>-->
							</div>
						</div>
					</ul>
				</li>
				</ul>
            <!-- END TOP BAR LEFT PART -->
			</div>
		</div>
        </div>
    </div>
</div>
<!-- END TOP BAR -->
<!-- BEGIN HEADER -->
<div class="header reduce-header">
  <div class="container">
    <!--<a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>-->
	<a href="" class="mobi-toggler"><i class="fa fa-bars"></i></a>
    <!-- BEGIN NAVIGATION -->
<div class="header_nav_wrap">
    <div class="header-navigation">
      <ul class="list-unstyled list-inline">
			<li class="hidden-lg hidden-md border_bottom" style="padding-left: 4px;">
				<?php if(!empty($_web_member_data["user_name"])){?>
					<a style="display: inline-table;;color: #000;padding-left: 18px !important;" href="/member/memberSignOut"><i class="fa fa-sign-in" style="margin-right: 14px;"></i> 登出 </a>
				<?php }else{ ?>
					<a style="display: inline-table;;color: #000;padding-left: 18px !important;" href="/member/memberSignIn"><i class="fa fa-sign-in" style="margin-right: 14px;"></i> 登入</a><a style="display: inline-table;padding:0 9px 0 0;" href="/member/memberSignUp">&nbsp; / &nbsp;註冊</a>
				<?php } ?>
			</li>
			<li class="hidden-lg border_bottom">
				<a style="color: #000;padding-left: 18px !important;" href="/"><img style="margin-right: 15px;" src="public/img/home_20x19.png"/></i>首頁</a>
			</li>
			<!--li class="hidden-lg border_bottom">
				<a style="color: #000;padding-left: 18px !important;" href="/home/ewedding"><img style="margin-right: 13px;" src="public/img/wedding_web22x21.png"/>婚禮網站</a>
			</li-->

			<!--<li class="mnu_li langs-block">
				<a class="drop_down_menu collapsed" data-toggle="collapse" data-target="#drop_down1111"><img style="margin-right: 13px;" src="public/img/main_menu_25x25.png"/>商品主選單</a>
					<ul class="sub_menu myclass collapse" id="drop_down1111">-->
					<?php $_category_array=json_decode(json_encode($this->page_data['_category_array']), FALSE);
					//print_r($_category_array);
						function list_shop_menum2($_category_array,$category_sn,$category_name,$up_category_sn,$down_nums)
						{
							//<a class="drop_down_menu collapsed" data-toggle="collapse" data-target="#tgl_sub-menu'.$category_sn.'" href="#tgl_sub-menu'.$category_sn.'">'.$category_name.'</a>原始li結構
							$ifnosort=($down_nums==0)?'nosort':'';//下層無分類
							echo '
										<li class="sub-menu-left-inner_li-1 '.$ifnosort.'">
										<a class="drop_down_menu collapsed" data-toggle="collapse" data-target="#tgl_sub-menu'.$category_sn.'" href="javascript:void(0)"><span class="ddd"></span></a>
										<a href="/shop/shopCatalog/'.$category_sn.'">'.$category_name.'</a>
											<ul class="sub-menu-left-inner2 collapse" id="tgl_sub-menu'.$category_sn.'">
										';
							  if(is_array($_category_array))
									{
										foreach($_category_array as $key => $value)
										{
											if($value->upper_category_sn == $category_sn)
											{
													 echo '
													  <li><a class="font-icons" href="shop/shopCatalog/'.$value->category_sn.'">'.$value-> category_name.'</a></li>
												  ';
											}
										}
									}
								  echo '
									  </ul>
									</li>
						  ';
						}

						function list_shop_menum($_category_array,$category_sn,$category_name,$main_banner)
						{
							//<a class="drop_down_menu collapsed" data-toggle="collapse" data-target="#tgl_menu'.$category_sn.'" href="#tgl_menu'.$category_sn.'"><span class="ddd"><a href="/shop/shopCatalog/'.$category_sn.'">'.$category_name.'</a></span></a>原始li結構
							//data-target="shop/shopCatalog/'.$category_sn.'" href="/shop/shopCatalog/'.$category_sn.'" 因為要展開先拿掉
							print '
                	<li class="langs-block pro">
						<a class="drop_down_menu collapsed" data-toggle="collapse" data-target="#tgl_menu'.$category_sn.'" href="javascript:void(0)"><span class="ddd"></span></a><a href="/shop/shopCatalog/'.$category_sn.'"><i class="fa fa-diamond"></i>'.$category_name.'</a>
								<ul class="sub_menu myclass collapse" id="tgl_menu'.$category_sn.'">
									<div class="sub-menu-left">
									<ul class="sub-menu-left-inner">
						';
									if(is_array($_category_array))
									{
										foreach($_category_array as $key => $value)
										{
											if($value->upper_category_sn == $category_sn)
											{
												 list_shop_menum2($_category_array,$value->category_sn,$value->category_name,$category_sn,$value->down_nums);
											}
										}
									}
						 print'</ul></div></ul></li>';
						}
						if(is_array(@$_category_array))
						{
							foreach($_category_array as $key => $value )
							{
								if($value->upper_category_sn == 0)
								{
									 list_shop_menum($_category_array,$value->category_sn,$value->category_name,$value->main_banner);
								}
							}
						}
						?>
					<!--</ul>
			</li>-->
			<li class="hidden-lg border_bottom">
				<a style="color: #000;padding-left: 18px !important;" href="/home/faq"><img style="margin-right: 13px;" src="public/img/faq_23x22.png"/>常見問題</a>
			</li>
			<li class="hidden-lg border_bottom">
				<a style="color: #000;padding-left: 18px !important;" href="<?php if(!empty($_web_member_data["user_name"])){?> /member/memberAccount<?php }else{?>/member/memberSignIn<?php }?>"><img style="margin-right: 13px;" src="public/img/member_23x24.png"/>會員中心</a>
			</li>

			<li class="mnu_li langs-block mob_mnu_li">
				<a class="blog_sub_1 drop_down_menu collapsed"><img style="margin-right: 6px;padding-left: 4px;" src="public/img/blog_icon_30x13.png"/>婚禮情報</a>
					<ul class="sub_menu myclass blog_sub" style="display:none;">
<?php if(@$content_types){foreach($content_types as $key=>$_Item){?>
    <li><a href="home/blogList/<?php echo $_Item['content_title'];?>"><?php echo $_Item['content_title'];?> (<?php echo $_Item['numrows'];?>)</a></li>
<?php }}?>
						<li><a href="#" style="text-decoration: none;">更多文章 <i class="angl_rgt fa fa-angle-right" aria-hidden="true"></i></a></li>
					</ul>
			</li>
<?php if(@$top_right_word){ foreach($top_right_word as $key=>$_Item){
	if($_Item['banner_content_name']=='FB粉絲團'){
		$fans_link_sn=$_Item['banner_schedule_sn'];
	}
}}?>
			<li class="hidden-lg border_bottom">
				<a style="color: #000;" target="_blank" href="home/ad_link/<?=$fans_link_sn?>">
				<img width="16" src="public/img/fb_icon_20x20.png" style="display:inline-block; margin-right:15px; margin-left:9px;"/>
				<div style="display:inline-block;">FB粉絲團</div></a>
			</li>
		</ul>
    </div>
	<a style="position:absolute; margin-top:15px;" id="navToggle" class="animated-arrow slideLeft menuopen"><span></span></a>
	</div>
</div>

    <!-- END NAVIGATION -->
  </div>
</div>
<!-- Header END -->
<div class="modal fade" id="ajax_msg" role="basic" aria-hidden="true">
<form action="shop/shopSearch" method="get">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">請輸入關鍵字搜尋</h4>
      </div>
      <div class="modal-body">
	    <div style="width:100%;" class="input-group">
	      <input type="text" name="Search" placeholder="" class="form-control">
	    </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn blue">送出</button>
      </div>
    </div>
  </div>
</form>
</div>
