<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Countries_model extends CI_Model 
{

 public function get_cities_by_country_id($country_id) 
 {
		$this->db->where('city_code', $country_id);
		$query = $this->db->get('ij_town_ct');
		
		return $query->result();
	}
	
	
}
