/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
   //config.language = 'zh-tw';
   //config.defaultLanguage = 'zh-tw';
   //config.contentsLanguage = 'zh-tw';
   //config.languages = { en: 1, 'zh-tw': 1,zh:1 };

	// config.uiColor = '#AADC6E';


config.filebrowserBrowseUrl = '/public/metronic/global/plugins/ckfinder/ckfinder.html';
config.filebrowserImageBrowseUrl = '/public/metronic/global/plugins/ckfinder/ckfinder.html?Type=Images';
config.filebrowserFlashBrowseUrl = '/public/metronic/global/plugins/ckfinder/ckfinder.html?Type=Flash';
config.filebrowserUploadUrl = '/public/metronic/global/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'; //可上傳一般檔案
config.filebrowserImageUploadUrl = '/public/metronic/global/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';//可上傳圖檔
config.filebrowserFlashUploadUrl = '/public/metronic/global/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';//可上傳Flash檔案	
};
