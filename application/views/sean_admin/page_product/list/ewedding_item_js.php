<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script src="public/metronic/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
<script type="text/javascript" src="public/metronic/global/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script src="public/metronic/admin/pages/scripts/components-form-tools.js"></script>
<script src="public/metronic/admin/pages/scripts/portfolio.js"></script>

<script>
jQuery(document).ready(function() {       
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
    ComponentsFormTools.init();
    Portfolio.init();

    // form init & event listener
    /*
    if(jQuery('input[name="productType"]:checked').val()=='ewedding'){
        jQuery('#tabImages').show();
    }else{
        jQuery('#tabImages').hide();
    }
    */
    /*
    if(jQuery('input[name="additional-purchase"]:checked').val()=='Y'){
        jQuery('#tabAdditionalPurchase').show();
    }else{
        jQuery('#tabAdditionalPurchase').hide();
    }
    */
	var colorEnable = jQuery('input[name="template-Color[]"]:checked');
	jQuery('select[name="imgColorSelect"]').html('');
	for(var i=0; i<colorEnable.length; i++){
    	jQuery('.trImgColor[rel="'+jQuery(colorEnable[i]).val()+'"]').show();
    	var tmp = jQuery('select[name="imgColorSelect"]').html();
    	jQuery('select[name="imgColorSelect"]').html(tmp + '<option value="'+jQuery(colorEnable[i]).val()+'">' +jQuery(colorEnable[i]).val()+ '</option>');
	}
	var packageEnable = jQuery('input[name="template-Package[]"]:checked');
	for(var i=0; i<packageEnable.length; i++){
    	jQuery('.trPackage[rel="'+jQuery(packageEnable[i]).val()+'"]').show();
	}

    jQuery('input[name="productType"]').change(function(){
        if(jQuery('input[name="productType"]:checked').val()=='ewedding'){
            jQuery('#tabImages').show();
        }else{
            jQuery('#tabImages').hide();
        }
    });
    /*
    jQuery('input[name="additional-purchase"]').change(function(){
        if(jQuery('input[name="additional-purchase"]:checked').val()=='Y'){
            jQuery('#tabAdditionalPurchase').show();
        }else{
            jQuery('#tabAdditionalPurchase').hide();
        }
    });*/
    jQuery('input[name="template-Color[]"]').change(function(){
    	jQuery('.trImgColor').hide();
    	jQuery('select[name="imgColorSelect"]').html('');
    	var colorEnable = jQuery('input[name="template-Color[]"]:checked');
    	for(var i=0; i<colorEnable.length; i++){
	    	jQuery('.trImgColor[rel="'+jQuery(colorEnable[i]).val()+'"]').show();
    		var tmp = jQuery('select[name="imgColorSelect"]').html();
    		jQuery('select[name="imgColorSelect"]').html(tmp + '<option value="'+jQuery(colorEnable[i]).val()+'">' +jQuery(colorEnable[i]).val()+ '</option>');
    	}
    });
    jQuery('input[name="template-Package[]"]').change(function(){
    	jQuery('.trPackage').hide();
        if(jQuery('input[name="template-Package[]"]:checked').length > 0){
            var packageEnable = jQuery('input[name="template-Package[]"]:checked');
            for(var i=0; i<packageEnable.length; i++){
                jQuery('.trPackage[rel="'+jQuery(packageEnable[i]).val()+'"]').show();
            }
        }else{
            jQuery('.trPackage[rel="原始"]').show();
        }
    });
    jQuery('input[name="setting_price"]').change(function(){
        var id=jQuery('input[name="setting_price"]:checked').val();
        var id_hide = 'area';
        if(id=='area'){
            id_hide = 'original';
        }
        jQuery('#setting_price_'+id_hide).hide();
        jQuery('#setting_price_'+id).show();
    });
    jQuery('#btn_area_add').click(function(){
        jQuery('#setting_price_area').append( jQuery('#sample_price_area').html() );
    });
});
</script>

