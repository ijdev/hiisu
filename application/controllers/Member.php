<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Member extends CI_Controller {
	 public function __construct()
		{
			parent::__construct();
			//$this->load->library('session');
			$this->load->helper('form');
			$this->load->helper('url');
			$this->load->library('form_validation');
			//$this->load->library('sean_lib/Sean_form_general');
			$this->load->model('sean_models/Sean_general');
			$this->load->model('sean_models/Sean_db_tools');
			$this->load->config('ij_config');

			$this->page_data = array();
			$this->load->model('Shop_model');
			$this->page_data['cart']=$this->Shop_model->Get_Cart(); //購物車
			$this->page_data['unreadmessage']= $this->Shop_model->get_unread_message_count();
			$this->page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
			$_category_array=$this->Shop_model->categoryParentChildTree('囍市集',0,'','',''); //分類
			$this->page_data['_category_array']=$this->Shop_model->get_banner($_category_array); //分類抓圖

			$this->load->model('libraries_model');
			$this->load->library("ijw");
			if(!$this->session->userdata('general_header')){
				$this->page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
				$where=array('upper_category_sn'=>'0','category_status'=>'1','channel_name'=>'囍市集');
				$this->page_data['general_header']['root_category']=$this->Shop_model->_select('ij_category',$where,0,0,10,0,0,'result_array');
				$this->page_data['general_header']['content_types']=$this->libraries_model->_get_blog_faq_categorys(1,'囍市集');
				$this->page_data['general_header']['left_up']=$this->Shop_model->get_adsMaterials('首頁-跑馬燈文字');
				//$this->page_data['general_header']['up_left_banner']=$this->Shop_model->get_adsMaterials('首頁-左小方格');
				$this->page_data['general_header']['up_left_banner']=array();
				$this->page_data['general_header']['up_right_banner']=$this->Shop_model->get_adsMaterials('首頁-右小方格-右側');
				$this->page_data['general_header']['up_banner']=$this->Shop_model->get_adsMaterials('首頁_寬版長方格_H1_右側');
				$this->page_data['general_header']['hot_search']=$this->Shop_model->get_adsMaterials('首頁-熱門關鍵字',8)['banners'];
				$this->page_data['general_header']['top_right_word']=$this->Shop_model->get_adsMaterials('首頁_文字_右上',1)['banners'];
				$this->session->set_userdata('general_header',$this->page_data['general_header']);
			}else{
				$this->page_data['general_header']=$this->session->userdata('general_header');
			}
			/*$this->page_data['init_control']="www/";
			$this->page_data['_home_url']="www/";
			$this->init_control=$this->page_data['init_control'];
			$this->page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
			$this->page_data['page_content']['view_path'] = 'admin-ijwedding/page_home/page_content'; // 頁面主內容 view 位置, 必要
			$this->page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$this->page_data['page_level_js']['view_path'] = 'admin-ijwedding/page_home/page_content_js'; // 頁面主內容的 JS view 位置, 必要
			//$this->lang->load('general', 'zh-TW');
			$this->page_data["_per_page"]=2;*/
		}


	public function index()
	{
		if($this->session->userdata("web_member_data")['member_level_type']=='2'){
			redirect(base_url('/member/affiliate/index'));
		}else{
			redirect(base_url('/member/memberAccount'));
		}
		$page_data = array();

		// 內容設定

		// page level setting
		$page_data['slider'] = 'www/page_home/slider'; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，非必要


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_home/page_content'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_home/page_content_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 * 登入頁面 Template
	 *
	 */
	public function memberSignIn()
	{
		$page_data = array();
		//echo $this->session->userdata('HTTP_REFERER');
		 $page_data['login_url']="";
		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設
			$page_data['_captcha_img']=$this->captcha_img();

		if($this->session->flashdata("HTTP_REFERER")){
			$this->session->keep_flashdata("HTTP_REFERER"); //紀錄返回
			//}else{
		//	if(isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'],'memberSignIn')===false) $this->session->set_flashdata("HTTP_REFERER", $_SERVER['HTTP_REFERER']); //紀錄返回
		}
		//if($this->session->userdata('web_login')== true){
		//	redirect($this->session->flashdata("HTTP_REFERER"));
		//}
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_sign_in'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_sign_in_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	// function for contact_us

	public function contact_us()
	{
		$page_data = array();
		//echo $this->session->userdata('HTTP_REFERER');
		 //$page_data['login_url']="";
		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設
			//$page_data['_captcha_img']=$this->captcha_img();

		if($this->session->flashdata("HTTP_REFERER")){
			$this->session->keep_flashdata("HTTP_REFERER"); //紀錄返回
			//}else{
		//	if(isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'],'memberSignIn')===false) $this->session->set_flashdata("HTTP_REFERER", $_SERVER['HTTP_REFERER']); //紀錄返回
		}
		//if($this->session->userdata('web_login')== true){
		//	redirect($this->session->flashdata("HTTP_REFERER"));
		//}
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/contact'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/contact_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	//function for blog page

	public function blogs()
	{
		$page_data = array();
		//echo $this->session->userdata('HTTP_REFERER');
		 //$page_data['login_url']="";
		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設
			//$page_data['_captcha_img']=$this->captcha_img();

		if($this->session->flashdata("HTTP_REFERER")){
			$this->session->keep_flashdata("HTTP_REFERER"); //紀錄返回
			//}else{
		//	if(isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'],'memberSignIn')===false) $this->session->set_flashdata("HTTP_REFERER", $_SERVER['HTTP_REFERER']); //紀錄返回
		}
		//if($this->session->userdata('web_login')== true){
		//	redirect($this->session->flashdata("HTTP_REFERER"));
		//}
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/blog'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/blog_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}


/*
	*
	* FB  登入會回傳值
	*
	*
	*/

	public function fb_login()
	{
		$this->load->library('/facebook/API_Facebook');
		header("Location:".$this->api_facebook->getLoginUrl(base_url('member/fb_login_callBack')));//base_url裡輸入call back網址
	}


	public function fb_login_callBack(){

		$page_data = array();
		//會員參數
		$_c_member_config="member_config";
		$_c_member_data="member_data";
		//會員資料庫參數 抓config/member_config.php 裡的member_data 陣列
		$member_item=$this->Sean_general->general_data($_c_member_config,$_c_member_data);
		// 指定會員資料庫參數 至session
		$this->session->set_userdata('member_item',$member_item);

		$this->load->library('/facebook/API_Facebook'); // Automatically picks appId and secret from config
       $this->load->model('member/fb_user');
   				$arr_get = $this->input->get();
		if($arr_get['code']!=""&&$arr_get['state']!="")
		{
      //處理回傳
			$helper = $this->api_facebook->fb->getRedirectLoginHelper();
			$accessToken="";
			try
			{
				$accessToken = $helper->getAccessToken();
			}
			catch(Exception $e)
			{
				$this->fb_login();
				exit;
			}
                        //取得有效時間(60天)較長的accesstoken
			if (!$accessToken->isLongLived())
			{
			  try {
			    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
			  } catch (Exception $e) {
				$this->fb_login();
				exit;
			  }
			}
			$accessToken = (string)$accessToken;
			$fbid = $this->api_facebook->get_fbid($accessToken);
                        //用accesstoken取的fbid

			//$uid = $this->fb_user->get_uid_from_fbid($fbid);
                        //用fbid去查表
			if($fbid!=0)
			{
                        //fbid!=0和uid!=0 代表資料庫有此fb user的紀錄
				//$u_access_token = $this->m_user->get_user_access_token($uid);
                                //取得這個user在user table裡我們給予他的u_access_token值

			        //$_SESSION['u_access_token'] = $u_access_token;
							//echo $fbid."登入成功!";


						try {
                $user_profile = $this->api_facebook->get_profile($accessToken,'id,name,email,first_name,last_name');
            } catch (FacebookApiException $e) {
                $user = null;
            }

			$this->session->set_userdata('web_login',true);
            $this->session->set_userdata('fb_login',true);
			$this->session->set_userdata('fb_member_data',$user_profile);

						// check  user data exit openid_name  openid_nickname  name
							$_where=" where openid_name='".$user_profile["id"]."'";
						$_check_exit_member=$this->Sean_db_tools->db_get_one_record("openid_name","ij_member",$_where);
						 if(isset($_check_exit_member) && $_check_exit_member["openid_name"]==$user_profile["id"])
					  {

					 			$_where=" where openid_name='".$user_profile["id"]."' order by member_sn desc limit 1";
					 			//$_where=" where openid_name='".$user_profile["id"]."' order by member_sn desc limit 1";
								$_result01=$this->Sean_db_tools->db_get_max_record("*","ij_member",$_where);
								//$array["user_name"]="111";
										foreach($_result01 as $key => $value)
										{
														if($value->openid_name !=$user_profile["id"])
														{


															//update user fb account
															$_data=array();
															//$_data["last_name"]=$user_profile["name"];
															$_data["openid_nickname"]=$user_profile["name"];


															$_where=" where member_sn='".$value->member_sn."'";
						//var_dump($_data);exit;

				     								  $_result=$this->Sean_db_tools->db_update_record($_data,$this->_table,$_where,"member_sn",$value->member_sn);

												 			foreach ($member_item as $k => $i){
																	 $array[$k]=$value->$k;
															}




														}else{

															foreach ($member_item as $k => $i)
															{
																	 $array[$k]=$value->$k;
															}



														}
										}
								if($array['member_level_type']=='2'){ //聯盟會員
                                    $this->db->select("partner_sn,ij_partner.partner_level_sn,bonus");
    								$this->db->from('ij_partner');
                                    $this->db->join("ij_partner_level_config","ij_partner_level_config.partner_level_sn = ij_partner.partner_level_sn","left");
            				        $this->db->where('member_sn',$array["member_sn"]);
            				        $this->db->limit(1);
            		        		$query = $this->db->get();
            		        		if($query->num_rows() == 1)
            		        		{
            		        			$row = $query->row();
                                        $array["partner_sn"]=$row->partner_sn;
                                        $array["partner_level_sn"]=$row->partner_level_sn;
                                        $array["bonus"]=$row->bonus;
            		        		}
                                }else{
                                    $array["partner_sn"]=0;
                                    $array["partner_level_sn"]=0;
                                    $array["bonus"]=0;
                                }

									 $this->session->set_userdata('web_member_data',$array);
					 				 $_message_cap="<font color=red size=\"3px\">FB 登入成功</font>";
									 $this->session->set_flashdata("_message",$_message_cap);
				           redirect("/");
									 exit();

					 }else{
						//var_dump($user_profile);exit;
						if(@$this->session->userdata('partner_sn')){
							$_data["associated_partner_sn"]=$this->session->userdata('partner_sn');
						}
								$_data["user_name"]='[FB]'.$user_profile["email"];
								$_data["email"]=$user_profile["email"];
								$_data["last_name"]=$user_profile["name"];
								//$_data["first_name"]=$user_profile["first_name"];
								//$_data["password"]=$user_profile["id"];
								$_data["member_level_type"]=1;
								$_data["system_user_flag"]=0;

								$_data["member_status"]=2;

								$_data["openid_name"]=$user_profile["id"];
								$_data["openid_nickname"]=$user_profile["name"];

								$this->_table="ij_member";//table name
					 			//var_dump($user_profile);
					 			//var_dump($_data);
					 			//exit();

							$_result=$this->Sean_db_tools->db_insert_record($_data,$this->_table);

				      if($_result){
				     			$array=array();
				     			$_where=" where openid_name='".$user_profile["id"]."' order by member_sn desc limit 1";
								$_result01=$this->Sean_db_tools->db_get_max_record("*","ij_member",$_where);
		$_c_member_config="member_config";
		$_c_member_data="member_data";
		//會員資料庫參數 抓config/member_config.php 裡的member_data 陣列
		$member_item=$this->Sean_general->general_data($_c_member_config,$_c_member_data);
										foreach($_result01 as $key => $value)
										{
											foreach ($member_item as $k => $i)
											{
												$array[$k]=@$value->$k;
											}
										}
										//adding wedding date
										 $_data=array();
										 $_data["member_sn"]=$array["member_sn"];
					     			 //$_data["product_gallery_sn"]=0; 2016-1-11 wdj
					     			 //$_data["wedding_date"]="";
					     			 //$_result=$this->Sean_db_tools->db_insert_record($_data,"ij_wedding_website");
				 		if($array["associated_partner_sn"]){
				 			//var_dump($array["associated_partner_sn"]);
							$this->load->model('member/partner_table');
							if($partner=$this->partner_table->get_newmember($array["associated_partner_sn"])){
				 				//發放招募新會員獎金
					 			//var_dump($partner);exit;
								$this->libraries_model->sav_gift_cash($partner['member_sn'],$partner['newmember'],$array["member_sn"],0,'招募新會員獎金');
							}
				 		}


				     			 $this->session->set_userdata('web_member_data',$array);
				     			 $_message_cap="<font color=red size=\"3px\">>FB 登入成功，註冊會員成功！</font>";
				     			 $this->session->set_flashdata("_message",$_message_cap);
				           		redirect(base_url("/"));
									 exit();
				      }else{
				      		$_message_cap="<font color=red size=\"3px\">FB 登入失敗。</font>";
									 $this->session->set_flashdata("fail_message",$_message_cap);
				      	   redirect(base_url('member/memberSignIn'));
           			   exit();
				      }

				    }






			}
			else
			{
				$_SESSION['fb_token'] = $accessToken;
				//資料庫無紀錄，記錄下facebook給的accesstoken跳轉註冊...(略)
			}
		}
		else
		{
			$this->fb_login();
		}






	}
/*
	*
	* FB  登出
	*
	*
	*/
    public function fb_logout(){

       //$this->load->library('/facebook/API_Facebook');

        // Logs off session from website

        // Make sure you destory website session as well.

        redirect(base_url('member/memberSignIn'));
    }

	public function memberSignInAction()
	{

		$this->load->model('membership/Login_model_www');

        $_message='message';

        $_message_cap="";


       if ($this->input->get_post("rycode")!= $this->session->userdata('captcha'))
			{

					if ($this->input->get_post("rycode") != $this->session->userdata('captcha') && strlen($this->input->get_post("rycode")) > 0) {
                $_message_cap.="<font color=red size=\"3px\">驗證碼不符</font>";
            }
					 $this->session->set_flashdata($_message,$_message_cap);
            $data['post'] = $_POST;
           // $this->session->set_flashdata($data['post']);

			//紀錄登入失敗資料
			//$this->Login_model->_member_log(0,$_POST['username'],2);

            redirect(base_url('member/memberSignIn'));
            exit();
		}else{

			//會員登入檢查

			$_login_array=$this->Login_model_www->validate_login($this->input->get_post('username'), $this->input->get_post('password'));
			//var_dump($this->input->get_post('username'));
			//var_dump($this->input->get_post('password'));
			//exit();
			if(!is_array($_login_array))
			{
				//echo $_login_array;
				//exit();
				//紀錄登入失敗資料
				//$this->Login_model->_member_log(0,$_POST['username'],2);
				if($_login_array=='-1'){ //帳號錯誤
					$_message_cap='<font size=\"4px\">帳號錯誤，請確認是否輸入正確!</font>';
				}elseif($_login_array=='-2'){
					$_message_cap='<font size=\"4px\">密碼錯誤，請確認是否輸入正確!</font>';
				}else{
				  $_member_status_ct=$this->config->item("member_status_ct");
				  //var_dump($_member_status_ct);
				  //echo $_member_status_ct[$_login_array];
				  //exit();
					$_message_cap='<font size=\"4px\">'.$_member_status_ct[$_login_array].'!</font>';
				}

				$this->session->set_flashdata($_message,  $_message_cap);
				//$this->session->set_flashdata($data['post']);
            	redirect(base_url('member/memberSignIn'));
           		exit();

			}else{
				$this->session->set_userdata('web_login',true);
				//$this->session->set_userdata('customer_login',true);
				if($cart=$this->cart->contents()){ //登入前已有購物車資料一併寫入暫存購物車
				//var_dump($cart).'<br>';
					foreach($cart as $key=>$Item){
						$this->Shop_model->save_temp_order($_login_array['member_sn'],$Item);
					}
				}
				//暫存購物車寫回
				$this->Shop_model->get_temp_order_to_cart($_login_array['member_sn']);
				$this->session->set_userdata('web_member_data',$_login_array);

					//var_dump($this->session->flashdata("HTTP_REFERER"));
					//exit();
				if($HTTP_REFERER=$this->session->flashdata("HTTP_REFERER")){
					redirect($HTTP_REFERER);
				}else{
					redirect(base_url('/'));
					//redirect(base_url('/home'));
				}
				exit();

			}
		}
	}

	public function memberSignOut()
	{
		$this->session->set_userdata('web_login',false);
		$this->session->set_userdata('web_member_data',false);
		$this->session->set_userdata("apply_date",false);
		$this->cart->destroy();
		if(isset($this->session->userdata['fb_login']) &&  $this->session->userdata['fb_login']==true)
		{
			$this->session->set_userdata('fb_login',false);
			$this->session->set_userdata('fb_member_data',false);
			//redirect(base_url('member/fb_logout'));
		}
		if($HTTP_REFERER=$this->session->flashdata("HTTP_REFERER")){
			redirect($HTTP_REFERER);
		}else{
			redirect(base_url('/'));
		}
    	exit();

	}

	/*
	*
  * 檢查是否有登入
	*/

	//$this->LoginCheck();
	public function LoginCheck($ifredirect=1)
	{
			if($this->session->userdata('web_login')== true)
			return true;
			else
			if($ifredirect==1){
				redirect(base_url("member/memberSignIn"));
			}else{
				return false;
			}
			exit();
	}

	/**
	 * 註冊頁面 Template
	 *
	 */
	public function memberSignUp()
	{
		$this->ijw->get_partner_sn();
		//var_dump($this->session->userdata['web_member_data']['associated_partner_sn']);
		//var_dump($this->session->userdata('partner_sn'));
		$page_data = array();
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_sign_up'; // 頁面主內容 view 位置, 必要
    	$page_data['_captcha_img']=$this->captcha_img();
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_sign_up_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}


/**
	 * 註冊頁面 Template
	 *
	 */
	public function memberSignUp_Action()
	{
 	    if ($this->input->get_post("rycode")!= $this->session->userdata('captcha')){
			$this->ijw->_wait(base_url("member/memberSignUp") , 2 , '很抱歉，您輸入的驗證碼錯誤，請重新輸入！',1);
 	    }
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設

			//$this->load->model('membership/Login_model');

        $this->_table_field=array(
           "member_sn" => "會員代號",
					 "user_name" => "登入狀態代碼",
					 "email" => "帳號",
					 "last_name" => "姓",
					 "first_name" => "名",
					 "tel" => "市話",
					 "cell" => "手機",
					 "gender" => "性別",
					 "wedding_date" => "結婚日期",
					 "birthday" => "生日",
					 "addr1" => "住址",
					 "memo" => "備註",
					 "password" => "密碼",
					 "member_status" => "帳號狀態",
					 "member_level_type" => "會員等級",
					 "accept_edm_flag" => "是否願意接收電子報",
					 "system_user_flag" => "是否為總控人員",
				);

				$this->_table="ij_member";//table name
			  $this->_id="member_sn";//table key

        $_message_cap="";
       if ( $this->input->get_post("confirm_password") != $this->input->get_post("password") )
			{


           $_message_cap.="<font color=red size=\"3px\">密碼和重複輸入密碼不符</font>";
					 $this->session->set_flashdata("fail_message",$_message_cap);
           redirect(base_url("member/memberSignUp"));
					 exit();
       }else{



      foreach($this->_table_field as $key => $value)
			{
				if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
				$_data[$key]=$this->input->get_post($key);
			}

			if(strlen($this->input->get_post("password")) > 0 )
			$_data["password"]=md5($this->input->get_post("password"));

			$_data["user_name"]=$_data["email"];
			$_data["member_level_type"]=1;
			$_data["member_status"]=1;

			$_data["system_user_flag"]=0;
			$_send_email_register=$_data["email"];
			//basic insert
			//$_data["create_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			//$_data["create_date"]=date("Y-m-d H:i:s");
			//basic update and insert
			//$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			$_data["last_time_update"]=date("Y-m-d H:i:s");
			if(@$this->session->userdata('partner_sn')){
				$_data["associated_partner_sn"]=$this->session->userdata['partner_sn'];
			}
			$_where=" where user_name='".$_data["email"]."'";
			$_check_dup_member=$this->Sean_db_tools->db_get_one_record("user_name","ij_member",$_where);

	  if(isset($_check_dup_member) && $_check_dup_member["user_name"]==$_data["email"])
	 {

	 			$_message_cap.="<font color=red size=\"3px\">帳號已註冊，請使用其他Email.</font>";
					 $this->session->set_flashdata("fail_message",$_message_cap);
           redirect(base_url("member/memberSignUp"));
					 exit();

	 }else{

			$_result=$this->Sean_db_tools->db_insert_record($_data,$this->_table);


      if($_result)
      {
     			 // adding wedding_date to  ij_wedding_website 取消
     			 $_where=" where user_name='".$_data["email"]."'";
     			 $_member_sn=$this->Sean_db_tools->db_get_one_record("member_sn","ij_member",$_where)['member_sn'];
     			 //var_dump($_member_sn);
     			 if($_member_sn){
     			 	if($dealer_sn=$this->ijw->get_dealer_sn()){
     			 		$_data2=array('dealer_sn'=>$dealer_sn,
     			 			'member_sn'=>$_member_sn,
     			 			'join_path_code'=>'1',
     			 			'status'=>'1',
     			 			'start_date'=>date('Y-m-d H:i:s')
     			 			);
						$_data2 = $this->libraries_model->CheckUpdate($_data2,1);
						$_data2 = $this->libraries_model->CheckUpdate($_data2,0);
     			 		$this->Sean_db_tools->db_insert_record($_data2,'ij_member_dealer_relation');
     			 	}
     			 }
     			 //$_data="";
     			 //$_data["member_sn"]=$_member_sn["member_sn"];
     			 //$_data["product_gallery_sn"]=0; 2016-1-11 wdj
     			 //$_data["wedding_date"]=$this->input->get_post("wedding_date");
     			 //$_result=$this->Sean_db_tools->db_insert_record($_data,"ij_wedding_website");


     			 //$_message_cap.="<font color=red size=\"3px\">註冊成功，請您先至填寫信箱收取驗證信並點擊驗證，會員資格才正式生效喔。</font>";
     			 //$this->session->set_flashdata("_message",$_message_cap);
           //$this->send_mail("重發驗證信",$_send_email_register);
					 $this->load->library("ijw");
					 $this->ijw->send_mail('會員驗證信',$_data["email"],$_member_sn,'');
           redirect(base_url("member/memberSignUpOK"));
					 exit();
      }

    }

      }
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_sign_up'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_sign_up_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}



	/*
	affiliate_Action
	*/
	public function affiliate_Action()
	{
		$this->LoginCheck();
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設

			//$this->load->model('membership/Login_model');

      $this->_table_field=array(
           "affiliate_website" => "會員代號",
					 "affiliate_url" => "登入狀態代碼",
					 "affiliate_user" => "帳號",
					 "affiliate_name" => "姓",
					 "affiliate_vat" => "名",
					 "account_name" => "市話",
					 "bank_name" => "手機",
					 "branch_name" => "結婚日期",
					 "bank_account" => "生日",
				);

				$this->_table="ij_member";//table name
			  $this->_id="member_sn";//table key




			$_data["approval_status"]="0";
		  $_data["update_member_sn"]=($this->session->userdata['web_member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			$_data["apply_date"]=date("Y-m-d H:i:s");

		  $_key_id=$this->session->userdata['web_member_data']['member_sn'];

	  	$_where=" where member_sn='".$this->session->userdata['web_member_data']['member_sn']."'";
      $_result=$this->Sean_db_tools->db_update_record($_data,$this->_table,$_where,$_key_id,$this->_id);

      //寫入ij_partner
			$_data="";
			$_data["partner_website_name"]=$this->input->get_post("affiliate_website");
			$_data["partner_website_url"]=$this->input->get_post("affiliate_url");
			$_data["partner_chairman"]=$this->input->get_post("affiliate_user");
			$_data["partner_company_name"]=$this->input->get_post("affiliate_name");
		  $_data["dealer_tax_id"]=$this->input->get_post("affiliate_vat");
		  $_data["partner_status"]="0";
		  $_data_tmp=$this->session->userdata['web_member_data']['member_sn']."=partner";
		  $_data["partner_promote_code"]=rtrim(base64_encode($data_tmp),'=');
		  $_data["partner_promote_url"]="http://www.ijwedding.com.tw/affiliate/fetch/".rtrim(base64_encode($data_tmp),'=');

		  $_data["member_sn"]=$this->session->userdata['web_member_data']['member_sn'];
		  $_data["create_member_sn"]=($this->session->userdata['web_member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			$_data["create_date"]=date("Y-m-d H:i:s");

		  $_result=$this->Sean_db_tools->db_insert_record($_data,"ij_partner");
		  //寫入ij_partner


		  if($_result)
      {
     			 	   $_web_member_data=$this->session->userdata("web_member_data");
     			 	   $_web_member_data["approval_status"]=0;
     			 	   $_web_member_data["apply_date"]=date("Y-m-d H:i:s");
     			 		$this->session->set_userdata("web_member_data",$_web_member_data);

     			 		$_data2["account_name"]=$this->input->get_post("account_name");
			 				$_data2["bank_name"]=$this->input->get_post("bank_name");
				 			$_data2["branch_name"]=$this->input->get_post("branch_name");
							$_data2["bank_account"]=$this->input->get_post("bank_account");
							$_data2["associated_member_sn"]=$_key_id;

					 $_result=$this->Sean_db_tools->db_insert_record($_data2,"ij_bank_info");

     			 $_message_cap="<font color=red size=\"3px\">感謝您的申請，我們會第一時間和您聯繫！</font>";
     			 $this->session->set_flashdata("message",$_message_cap);
          //$this->send_mail("重發驗證信",$_data["email"]);
          redirect(base_url("member/affiliate/form"));
					exit();
      }


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_affiliate'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_affiliate_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/*
	*
	* 會員驗證
	*
	*/

	public function register($v)
	{
		$page_data = array();
			$data_v = base64_decode($v);
			$check_v=explode("=",$data_v);
			//var_dump($check_v);
			if(isset($check_v[1]) && $check_v[1]=="ijwedding")
			{
				 //$_check_db=$this->Sean_db_tools->db_get_one_record("member_status","ij_member",$_where);
				 $_check_db=$this->libraries_model->get_member_by_username($check_v[0],'member_sn,member_status,associated_partner_sn');
				 //var_dump($check_v);
				if(isset($_check_db["member_status"])  && $_check_db["member_status"]==1){
			 		$_data["member_status"]=2;
				 	$_where=" where user_name='".$check_v[0]."'";
			 		$_result=$this->Sean_db_tools->db_update_record($_data,"ij_member",$_where,'','');
				 	if($_result){
				 		if($_check_db["associated_partner_sn"]){
				 			//var_dump($_check_db["associated_partner_sn"]);
							$this->load->model('member/partner_table');
							if($partner=$this->partner_table->get_newmember($_check_db["associated_partner_sn"])){
				 			//var_dump($newmember_money);
				 				//發放招募新會員獎金
								$this->libraries_model->sav_gift_cash($partner['member_sn'],$partner['newmember'],$_check_db["member_sn"],0,'招募新會員獎金');
							}
				 		}
				 		//exit;
						$page_data["_message"]=
						'<div class="alert alert-success">
		        		<strong>驗證成功!</strong> ，囍市集感謝您的支持．
		     		 	</div>';
		     		}else{
		     			$page_data["_message"]=
						'<div class="alert alert-danger">
			        	<strong>驗證失敗，請檢查你的驗證碼是否正確</strong>
			     	    </div>';
		     		}
				}else{

				 	if(isset($_check_db["member_status"])  && $_check_db["member_status"]==2)
				 	{
				 			$page_data["_message"]=
							 '<div class="alert alert-success">
			        		<strong>您的帳號已開通!</strong> ，囍市集感謝您的支持．
			     		 </div>
			     		 ';

				 	}else{
						 	$page_data["_message"]=
							 '<div class="alert alert-danger">
			        	<strong>驗證失敗，請檢查你的驗證碼是否正確，或敬請洽詢客服</strong>
			     	  </div>
			     	  ';
	     		}
				 }




			}else{

				$page_data["_message"]=
				 '<div class="alert alert-danger">
        	<strong>驗證失敗，請檢查你的驗證碼是否正確</strong>
     	  </div>
     	  ';
		}

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_sign_up_register'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_sign_up_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}
	/*
	*
	* 會員修改
	*
	*/

	public function memberModify()
	{
		$this->LoginCheck();
		$this->_table_field=array(
					 "member_sn" => "會員代號",
					 "user_name" => "登入狀態代碼",
					 "email" => "帳號",
					 "gender" => "性別",
					 "last_name" => "姓",
					 "first_name" => "名",
					 "tel_area_code" => "市話區碼",
					 "tel" => "市話",
					 "tel_ext" => "分機",
					 "cell" => "手機",
					 "wedding_date" => "結婚日期",
					 "birthday" => "生日",
					 "zipcode" => "郵遞區號",
					 "addr_city_code" => "縣市",
					 "addr_town_code" => "鄉鎮",
					 "addr1" => "住址",
					 "memo" => "備註",
					 "addr_state" => "本島/外島",
					 "accept_edm_flag" => "電子報",
					 "member_status" => "帳號狀態",
					 "member_level_type" => "會員等級",
					 "system_user_flag" => "是否為總控人員",
				);

				$this->_table="ij_member";//table name
			  $this->_id="member_sn";//table key

        $_message_cap="";

       if ( $this->input->get_post("confirm_password") != $this->input->get_post("password") )
			{


           $_message_cap.="<font color=red size=\"3px\">密碼和重複輸入密碼不符</font>";
					 $this->session->set_flashdata("fail_message",$_message_cap);
           redirect(base_url("member/memberAccount"));
					 exit();
       }else{



      foreach($this->_table_field as $key => $value)
			{
					if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
					$_data[$key]=$this->input->get_post($key);
			}

			if(strlen($this->input->get_post("password")) > 0 )
			$_data["password"]=md5($this->input->get_post("password"));
			//basic insert
			//$_data["create_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			//$_data["create_date"]=date("Y-m-d H:i:s");
			//basic update and insert
			$_data["update_member_sn"]=($this->session->userdata['web_member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			$_data["last_time_update"]=date("Y-m-d H:i:s");

		  $_key_id=$this->session->userdata['web_member_data']['member_sn'];

	  	$_where=" where member_sn='".$this->session->userdata['web_member_data']['member_sn']."'";
      $_result=$this->Sean_db_tools->db_update_record($_data,$this->_table,$_where,$_key_id,$this->_id);
      if($_result)
      {
     			foreach($this->_table_field as $key => $value)
				{
     			 	if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
     			 	$_data[$key]= $this->input->get_post($key);
     			 	else
     			 	$_data[$key]=@$this->session->userdata['web_member_data'][$key];

     			}
     			$_data["password"]=$this->session->userdata['web_member_data']['password'];

     			 //$_data2="";
     			 //$_data2["wedding_date"]=$this->input->get_post("wedding_date");
     			 //$_where=" where member_sn='".$this->session->userdata['web_member_data']['member_sn']."'";
     			 //$_result=$this->Sean_db_tools->db_update_record($_data2,"ij_wedding_website",$_where,"wedding_website_sn","0");
     			 //$_data["wedding_date"]=$_data2["wedding_date"];

     			$this->session->set_userdata("web_member_data",$_data);
     			 $_message_cap.="<font color=red size=\"3px\">修改成功！</font>";
     			 $this->session->set_flashdata("message",$_message_cap);
           redirect(base_url("member/memberAccount"));
					 exit();
      }else{
      		$_message_cap.="<font color=red size=\"3px\">修改失敗！</font>";
     			 $this->session->set_flashdata("fail_message",$_message_cap);
           redirect(base_url("member/memberAccount"));
					 exit();
      }



      }

	}

	/*
	*
	* 會員修改
	*
	*/

	public function memberModifyPassword()
	{
		$this->LoginCheck();
				$this->_table="ij_member";//table name
			  $this->_id="member_sn";//table key

        $_message_cap="";

       if($this->input->get_post("confirm_password") != $this->input->get_post("password")){
            $_message_cap.="<font color=red size=\"3px\">新密碼和重複輸入密碼不符，密碼無異動</font>";
			$this->session->set_flashdata("fail_message",$_message_cap);
            redirect(base_url("member/memberPassword"));
			exit();
		}elseif(md5($this->input->get_post("old_pwd")) != $this->session->userdata('web_member_data')["password"]){
            $_message_cap.="<font color=red size=\"3px\">原密碼不符，密碼無異動</font>";
			$this->session->set_flashdata("fail_message",$_message_cap);
            redirect(base_url("member/memberPassword"));
			exit();
        }else{

			if(strlen($this->input->get_post("password")) > 0 )
			$_data["password"]=md5($this->input->get_post("password"));

			$_data["update_member_sn"]=$this->session->userdata['web_member_data']['member_sn'];
			$_data["last_time_update"]=date("Y-m-d H:i:s");

		  $_key_id=$this->session->userdata['web_member_data']['member_sn'];

	  	$_where=" where member_sn='".$this->session->userdata['web_member_data']['member_sn']."'";
      $_result=$this->Sean_db_tools->db_update_record($_data,$this->_table,$_where,$_key_id,$this->_id);
      if($_result)
      {
      		//var_dump($this->session->userdata['web_member_data']['password']);
					$data = $this->session->userdata('web_member_data');
					$data['password'] = $_data["password"];
					$this->session->set_userdata('web_member_data', $data);
     			//$this->session->set_userdata("web_member_data['password']",$_data["password"]);
      		//var_dump($this->session->userdata['web_member_data']['password']);
      		//exit();
     			//$this->session->set_userdata("web_member_data",$_data);
     			 $_message_cap.="<font color=red size=\"3px\">密碼修改成功！下次登入請改用新密碼。</font>";
     			 $_message='密碼修改成功！下次登入請改用新密碼。';
     			 $this->session->set_flashdata("message",$_message_cap);
      }else{
      		$_message_cap.="<font color=red size=\"3px\">密碼修改失敗！</font>";
     			 $_message='您的密碼修改失敗，密碼無異動。';
     			 $this->session->set_flashdata("fail_message",$_message_cap);
      }
		$this->Shop_model->add_member_message($_key_id,$_message_cap,$_message);
	    redirect(base_url("member/memberPassword"));
		exit();
      }

	}

	/**
	 * 註冊成功 Template，失敗回到原畫面並顯示錯誤訊息
	 *
	 */
	public function memberSignUpOK()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_sign_up_result'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_sign_up_js'; // 頁面主內容的 JS view 位置, 必要



		$this->load->view('www/general/main', $page_data);
	}


	/**
	 * Wish List Template
	 *
	 */
	public function wishList()
	{
		$this->LoginCheck();
		$page_data = array();
		$page=$this->input->get('page',true);
		if(!$page) $page=1;
		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		$this->load->model('libraries_model');
	    $this->load->library('pagination');
	    $this->load->library('ijw');
		$page_data['page_content']['page'] = $page;
		$page_data['page_content']['perpage'] = 5;
		$page_data['page_content']['total'] = count($this->libraries_model->_get_Track_list(0,0,$this->session->userdata['web_member_data']['user_name']));
	    $config = $this->ijw->_pagination('member/wishList', $page_data['page_content']['total'],$page_data['page_content']['perpage']);
	    $this->pagination->initialize($config);

		$Items=$this->libraries_model->_get_Track_list(0,0,$this->session->userdata['web_member_data']['user_name'],0,0,@$page,@$page_data['page_content']['perpage'],1);
		$page_data['page_content']['Items'] = $Items;
    	$page_data['page_content']['page_links'] = $this->pagination->create_links();
		$page_data['page_content']['view_path'] = 'www/page_member/page_wish_list'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_wish_list_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 * Add Wish List
	 *
	 */
	public function AddwishList($product_sn,$ifback=0)
	{
		//$this->LoginCheck(1);
		if($this->LoginCheck(0)){
			$_where2=" where product_sn='".$product_sn."' and member_sn='".$this->session->userdata['web_member_data']['member_sn']."'";
			if(!$this->Sean_db_tools->db_get_one_record("track_list_sn","ij_track_list_log",$_where2)){
				$_where=" where product_sn='".$product_sn."'";
				if($product_sn && $_product_db=$this->Sean_db_tools->db_get_one_record("product_name,product_eng_name,pricing_method,min_price","ij_product",$_where)){
					//var_dump($_product_db);
					//exit();
				  $_data["member_sn"]=$this->session->userdata['web_member_data']['member_sn'];
					$_data["product_sn"]=$product_sn;
					$_data["product_name"]=$_product_db['product_name'];
					$_data["product_eng_name"]=$_product_db['product_eng_name'];
					$_data["pricing_method"]=$_product_db['pricing_method'];
					$_data["sales_price"]=$_product_db['min_price'];
					$_data["track_list_status"]='1';
				  $this->load->model('libraries_model');
					$_data = $this->libraries_model->CheckUpdate($_data,1);
		      $_result=$this->Sean_db_tools->db_insert_record($_data,'ij_track_list_log');
					if($ifback){
						echo '成功加入追蹤清單';
						exit();
					}else{
						$this->session->set_flashdata("success_message",  "成功加入追蹤清單");
						redirect(base_url('shop/ShopItem/'.$product_sn));
					}
		    }else{
					if($ifback){
						echo '很抱歉，系統查無此商品，無法加入追蹤清單。';
						exit();
					}else{
						$this->session->set_flashdata("fail_message",  "很抱歉，系統查無此商品，無法加入追蹤清單。");
						redirect(base_url('shop'));
					}
		    }
		  }else{
					if($ifback){
						echo '該商品已經在您的追蹤清單嚕。';
						exit();
					}else{
						$this->session->set_flashdata("fail_message",  "該商品已經在您的追蹤清單嚕。");
						redirect(base_url('shop/ShopItem/'.$product_sn));
					}
		  }
		}else{
				if(isset($_SERVER['HTTP_REFERER'])) $this->session->set_flashdata("HTTP_REFERER",  $_SERVER['HTTP_REFERER']); //紀錄返回
				echo '<a href="member/memberSignIn">請先點我登入會員</a>';
				exit();
		}
	}

	/**
	 * 會員查詢購物金 Template nouse
	 *
	 */
	public function credit()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_credit'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_credit_js'; // 頁面主內容的 JS 由這個參數帶入, 必要
		$this->load->view('www/general/main', $page_data);
	}


	//nouse
	public function creditnew()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_credit_new'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_credit_js'; // 頁面主內容的 JS 由這個參數帶入, 必要
		$this->load->view('www/general/main', $page_data);
	}





	/**
	 * VIP 會員申請 Template
	 *
	 */
	public function affiliate($status='index')
	{
		$this->LoginCheck();
		//var_dump($this->session->userdata['web_member_data']);
		$page_data = array();
		$page_data['page_content'] = array('status' => $status); // 頁面主內容由這個參數帶入, 必要
		switch ($status) {
			case 'form':
				$page_data['page_content']['view_path'] = 'www/page_member/page_affiliate'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js']['view_path'] = 'www/page_member/page_affiliate_js'; // 頁面主內容的 JS view 位置, 必要
			break;
			case 'promo':
		  		$page_data['page_content']['CategoryItems']=$this->libraries_model->categoryParentChildTree(0,0,'','','');
        //echo '<pre>' . var_export($page_data['page_content']['CategoryItems'], true) . '</pre>';
				$page_data['page_content']['view_path'] = 'www/page_member/page_affiliate_promo'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js']['view_path'] = 'www/page_member/page_affiliate_promo_js'; // 頁面主內容的 JS view 位置, 必要
			break;
			case 'index':
				if(!$yearmonth=$this->input->post('yearmonth')){
					$yearmonth=date('Y-m');
				}
				//$cash=$this->Shop_model->get_gift_cash($this->session->userdata['web_member_data']['member_sn'],$yearmonth,2);
				//$page_data['page_content']['cash'] =$cash;
				$member_sn=$this->session->userdata['web_member_data']['member_sn'];
				$data_array=array('from'=>date('Y-m').'-01 00:00:00','to'=>date('Y-m-t').' 23:59:59','ij_order.associated_member_sn'=>$member_sn);
				$orders=$this->libraries_model->get_orders($data_array);
				$page_data['page_content']['cash'] = $orders['total'];
				$cash2=$this->Shop_model->get_gift_cash($this->session->userdata['web_member_data']['member_sn'],$yearmonth,3);
				$page_data['page_content']['cash2'] =$cash2;
				$page_data['page_content']['yearmonth'] =$yearmonth;
				$_where="date_format(order_create_date, '%Y-%m') = '$yearmonth' and ij_order.associated_member_sn=$member_sn and (checkstatus=1 or checkstatus=2)";
				$page_data['page_content']['total_ok_orders']=$this->libraries_model->get_order_statics('count(*) as t',$_where)['t'];
				$_where="date_format(order_create_date, '%Y-%m') = '$yearmonth' and ij_order.associated_member_sn=$member_sn and checkstatus<>1 and checkstatus<>2";
				$total_orders=$this->libraries_model->get_order_statics('count(*) as t',$_where,0)['t'];
				//echo $this->db->last_query();
				$page_data['page_content']['total_no_ok_orders']=$total_orders;

				$page_data['page_content']['total_members']=$this->libraries_model->get_member_by_partner_sn($this->session->userdata['web_member_data']['partner_sn'],'count(*) as t',$yearmonth)['t'];
				$page_data['page_content']['total_hits']=$this->libraries_model->get_hits_by_partner_sn($this->session->userdata['web_member_data']['partner_sn'],'count(*) as t',$yearmonth)['t'];

				//var_dump($total_members);
				//echo $cash;
				$page_data['page_content']['view_path'] = 'www/page_member/page_affiliate_index'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js']['view_path'] = 'www/page_member/page_affiliate_index_js'; // 頁面主內容的 JS view 位置, 必要
			break;
			default:
			break;
		}
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 * VIP 會員獎金查詢 Template
	 *
	 */
	public function benefit()
	{
		$this->LoginCheck();
		$page_data = array();
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['sub_order_status']=$this->libraries_model->_select('ij_sub_order_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array','sub_order_status,sub_order_status_name');
		$page_data['page_content']['payment_status']=$this->libraries_model->_select('ij_payment_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array','payment_status,payment_status_name');
		$page_data['page_content']['check_status']=$this->libraries_model->_select('ij_dealer_order_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array','dealer_order_status,dealer_order_status_name');
		$data_array=array('associated_member_sn'=>$this->session->userdata('web_member_data')['member_sn'],'from'=>date('Y-m').'-01','to'=>date('Y-m-t'));
		if($this->input->post('from')) $data_array['from']=date("Y-m-d", strtotime($this->input->post('from')));
		if($this->input->post('to')) $data_array['to']=date("Y-m-d", strtotime($this->input->post('to')));
		if($this->input->post('sub_order_status')) $data_array['sub_order_status']=(int)$this->input->post('sub_order_status');
		if($this->input->post('payment_status')) $data_array['payment_status']=(int)$this->input->post('payment_status');
		if($this->input->post('check_status')) $data_array['check_status']=$this->input->post('check_status');
		$orders = $this->libraries_model->get_orders($data_array);
		$page_data['page_content']['total'] = $orders['total'];
		//echo $orders['total'];
		unset($orders['total']);
		$page_data['page_content']['orders']=$orders;
		$this->load->model('member/partner_table');
		$page_data['page_content']['bonus']=$this->partner_table->get_newmember($this->session->userdata('web_member_data')['partner_sn'])['bonus']/100;
		$page_data['page_content']['data_array']=$data_array;
		//var_dump($page_data['page_content']['orders'][0]);
		$page_data['page_content']['view_path'] = 'www/page_member/page_benefit'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_benefit_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	public function cash($cash_type=0)
	{
		$this->LoginCheck();
		$page_data = array();
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['status_ct']=$this->libraries_model->_select('ij_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array','status,status_name',0,0,0);
		//var_dump($page_data['page_content']['status_ct']);
		$data_array=array('associated_member_sn'=>$this->session->userdata('web_member_data')['member_sn'],'from'=>date('Y-m').'-01','to'=>date('Y-m-t'));
		if($this->input->post('from')) $data_array['from']=date("Y-m-d", strtotime($this->input->post('from')));
		if($this->input->post('to')) $data_array['to']=date("Y-m-d", strtotime($this->input->post('to')));
		if(null!==$this->input->post('status')){
			if($this->input->post('status')!=0){
				$data_array['status']=(int)$this->input->post('status');
			}
		}else{
			$data_array['status']='1';
		}
		if($this->input->post('cash_type')){
			$data_array['cash_type']=(int)$this->input->post('cash_type');
		}elseif($cash_type){
			$data_array['cash_type']=$cash_type;
		}
		$orders = $this->libraries_model->get_gift_cash($data_array);
		//var_dump($data_array);
		//echo $this->db->last_query();
		//echo $orders['total'];
		$page_data['page_content']['orders']=$orders;
		$page_data['page_content']['cash_types'] = $this->config->item('cash_type');
		//$this->load->model('member/partner_table');
		//$page_data['page_content']['bonus']=$this->partner_table->get_newmember($this->session->userdata('web_member_data')['partner_sn'])['bonus']/100;
		$cash=$this->Shop_model->get_gift_cash($this->session->userdata['web_member_data']['member_sn']);

		$page_data['page_content']['cash'] =$cash;
		$page_data['page_content']['data_array']=$data_array;
		//var_dump($page_data['page_content']['orders'][0]);
		$page_data['page_content']['view_path'] = 'www/page_member/page_cash'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_cash_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 * 會員資料修改 Template
	 *
	 */
	public function memberAccount()
	{
		$this->LoginCheck();
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['city_ct'] = $this->Shop_model->_select('ij_city_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','city_code,city_name'); // city
		$page_data['page_content']['view_path'] = 'www/page_member/page_account'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_account_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 * 會員修改密碼 Template
	 *
	 */
	public function memberPassword()
	{
		$this->LoginCheck();
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		//$page_data['page_content']['city_ct'] = $this->Shop_model->_select('ij_city_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','city_code,city_name'); // city
		$page_data['page_content']['view_path'] = 'www/page_member/page_password'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_password_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 * 忘記密碼頁面 Template
	 *
	 */
	public function memberForgotPassword()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設

	  $page_data['_captcha_img']=$this->captcha_img();

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_forgot_password'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_forgot_password_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	public function memberForgotPasswordAction()
	{

        $_message='message';

        $_message_cap="";


       if ($this->input->get_post("rycode")!= $this->session->userdata('captcha'))
			{

					if ($this->input->get_post("rycode") != $this->session->userdata('captcha') && strlen($this->input->get_post("rycode")) > 0) {
                $_message_cap.="<font color=red size=\"3px\">驗證碼不符</font>";
            }
					 $this->session->set_flashdata($_message,$_message_cap);
            $data['post'] = $_POST;
           // $this->session->set_flashdata($data['post']);

            redirect(base_url('member/memberForgotPassword'));
            exit();
			}else{

			//

		  //$this->input->get_post("email")
		  	$_where=" where user_name='".$this->input->get_post("email")."' ";

		 	$_result01=$this->Sean_db_tools->db_get_max_record("*","ij_member",$_where);
				$_login_array=false;
				if(is_array($_result01))
				{
					foreach($_result01 as $key => $value)
			    {
			    	$_shopname="囍市集";
						$_username=$value->user_name;
						$_newpass=rand(111111, 999999);

						$_site_url="http://".$_SERVER['HTTP_HOST'];
						$_truename=$value->last_name.$value->first_name;
						$_to_email=$value->email;
						$_login_array=true;
					}
				}
				//var_dump($_result01[0]->member_sn);
				//exit();
		 	if($_login_array==false)
			{

				$this->session->set_flashdata("fail_message",  "<strong>驗證失敗!</strong> 系統找不到您的帳號或Email，請重新輸入或聯絡管理員．");

            	redirect(base_url('member/memberForgotPassword'));
           		exit();

			}else{
				//$this->session->set_userdata('web_login',true);
				//$this->session->set_userdata('customer_login',true);
				//$this->session->set_userdata('web_member_data',$_login_array);
				$this->session->set_flashdata("message","<strong>申請完成!</strong> 系統已經寄發一份密碼更改確認信件到您使用來註冊的信箱中，請前往收信並依照信件中指示完成密碼修改，囍市集感謝您的支持．");
				//$this->send_mail("忘記密碼",$this->input->get_post("email"));
			  $this->load->library("ijw");
			  $this->ijw->send_mail('忘記密碼',$this->input->get_post("email"),'','');
			  $this->Shop_model->add_member_message($_result01[0]->member_sn,"忘記密碼-<strong>申請完成!</strong>","系統已經寄發一份密碼更改確認信件到您使用來註冊的信箱中，請前往收信並依照信件中指示完成密碼修改，囍市集感謝您的支持．");

				redirect(base_url('member/memberForgotPassword'));
				exit();


			}
		}

  }

	/**
	 * 會員條款
	 *
	 */
	public function rule()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_rule'; // 頁面主內容 view 位置, 必要
		$page_data['page_content']['Item']=$this->Shop_model->_select('ij_system_file_config','system_file_name','會員服務條款',0,0,0,0,'row');
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_rule_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}


	/**
	 * privacy policy
	 *
	 */
	public function privacy()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_privacy_policy'; // 頁面主內容 view 位置, 必要

		$page_data['page_content']['Item']=$this->Shop_model->_select('ij_system_file_config','system_file_name','隱私權政策',0,0,0,0,'row');

		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_privacy_policy_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}


	/**
	 * TERMS & CONDITIONS
	 *
	 */
	public function terms()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_tc'; // 頁面主內容 view 位置, 必要
		$page_data['page_content']['Item']=$this->Shop_model->_select('ij_system_file_config','system_file_name','會員服務條款',0,0,0,0,'row');
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_tc_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}
	/**
	 * TERMS & CONDITIONS
	 *
	 */
	public function newslist()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/notification_list'; // 頁面主內容 view 位置, 必要
		//$page_data['page_content']['Item']=$this->Shop_model->_select('ij_system_file_config','system_file_name','使用條款',0,0,0,0,'row');
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/notification_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 *  客服中心 Template + 表單
	 *
	 */
	public function callcenter()
	{
		$this->LoginCheck();
		$page_data = array();
		$page=$this->input->get('page',true);

		// 內容設定

		//$this->Shop_model->get_temp_order_to_cart($this->session->userdata['web_member_data']['member_sn']);

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設

     //$_table_where=" left join ij_ccapply_detail_code_ct on ij_member_question.ccapply_detail_code=ij_ccapply_detail_code_ct.ccapply_detail_code  where member_sn='".$this->session->userdata['web_member_data']['member_sn']."' order by last_time_update desc";
     //left join ij_question_status_ct on ij_member_question_reply_log.question_status=ij_question_status_ct.question_status question_status_name
     //$_result01=$this->Sean_db_tools->db_get_max_record("ij_member_question.*,last_time_update,ccapply_detail_code_name","ij_member_question",$_table_where);
		//echo $this->db->last_query();
		 //$page_data['_result01']=$_result01;
			//var_dump($_result01);

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

    $this->load->library('pagination');
    $this->load->library('ijw');
		$page_data['page_content']['perpage'] = 5;
		$page_data['page_content']['total'] = count($this->Shop_model->_get_callcenter_list($this->session->userdata['web_member_data']['member_sn']));
    $config = $this->ijw->_pagination('member/callcenter', $page_data['page_content']['total'],$page_data['page_content']['perpage']);
    $this->pagination->initialize($config);

		$Items=$this->Shop_model->_get_callcenter_list($this->session->userdata['web_member_data']['member_sn'],@$page,@$page_data['page_content']['perpage']);
		$page_data['page_content']['_result01'] = $Items;
    $page_data['page_content']['page_links'] = $this->pagination->create_links();

		$page_data['page_content']['view_path'] = 'www/page_callcenter/page_ticket'; // 頁面主內容 view 位置, 必要
		$page_data['page_content']['ccapply'] = $this->Shop_model->_select('ij_ccapply_code_ct',array('display_flag'=>'1'),0,0,0,'ccapply_code','asc','result_array');
		foreach($page_data['page_content']['ccapply'] as $_key=>$_item){
			$page_data['page_content']['ccapply'][$_key]['detail']= $this->Shop_model->_select('ij_ccapply_detail_code_ct',array('display_flag'=>'1','ccapply_code'=>$_item['ccapply_code']),0,0,0,'ccapply_detail_code',0,'result_array','ccapply_detail_code,ccapply_detail_code_name,default_flag'); // town
		}
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_callcenter/page_ticket_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	public function kefu_list()
	{
		$this->LoginCheck();
		$page_data = array();
		$page=$this->input->get('page',true);

		// 內容設定

		//$this->Shop_model->get_temp_order_to_cart($this->session->userdata['web_member_data']['member_sn']);

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設

     //$_table_where=" left join ij_ccapply_detail_code_ct on ij_member_question.ccapply_detail_code=ij_ccapply_detail_code_ct.ccapply_detail_code  where member_sn='".$this->session->userdata['web_member_data']['member_sn']."' order by last_time_update desc";
     //left join ij_question_status_ct on ij_member_question_reply_log.question_status=ij_question_status_ct.question_status question_status_name
     //$_result01=$this->Sean_db_tools->db_get_max_record("ij_member_question.*,last_time_update,ccapply_detail_code_name","ij_member_question",$_table_where);
		//echo $this->db->last_query();
		 //$page_data['_result01']=$_result01;
			//var_dump($_result01);

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

    $this->load->library('pagination');
    $this->load->library('ijw');
		$page_data['page_content']['perpage'] = 5;
		$page_data['page_content']['total'] = count($this->Shop_model->_get_callcenter_list($this->session->userdata['web_member_data']['member_sn']));
    $config = $this->ijw->_pagination('member/kefu_list', $page_data['page_content']['total'],$page_data['page_content']['perpage']);
    $this->pagination->initialize($config);

		$Items=$this->Shop_model->_get_callcenter_list($this->session->userdata['web_member_data']['member_sn'],@$page,@$page_data['page_content']['perpage']);
		$page_data['page_content']['_result01'] = $Items;
    $page_data['page_content']['page_links'] = $this->pagination->create_links();

		$page_data['page_content']['view_path'] = 'www/page_callcenter/page_kefu_list'; // 頁面主內容 view 位置, 必要
		$page_data['page_content']['ccapply'] = $this->Shop_model->_select('ij_ccapply_code_ct',array('display_flag'=>'1'),0,0,0,'ccapply_code','asc','result_array');
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_callcenter/page_ticket_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}



  public function addCallcenter()
	{
		$this->LoginCheck();
		if($this->input->post('reply',true)){
		    $data = array(
		        'member_question_sn' => $this->input->post('member_question_sn',true),
		        'qna_content' => $this->input->post('reply',true),
		        'qna_date'    => date("Y-m-d H:i:s",time()),
		        'create_date'    => date("Y-m-d H:i:s",time()),
		        'create_member_sn'    => $this->session->userdata['web_member_data']['member_sn'],
		        'cc_reply_flag'        => '0'
		    );
		    //var_dump($data);exit;
			$this->Shop_model->_insert('ij_member_question_qna_log',$data);
		}else{
			$ExpectManDay=$this->Shop_model->_select('ij_ccapply_detail_code_ct',array('ccapply_detail_code'=>$this->input->post('ccapply_detail_code',true)),0,0,0,'ExpectManDay','asc','row','ExpectManDay')['ExpectManDay'];
			$this->load->model('membership/Callcenter');
		    $data = array(
		        'member_sn' => $this->session->userdata['web_member_data']['member_sn'],
		        'ccapply_detail_code' => $this->input->post('ccapply_detail_code',true),
		        'product_sn' => $this->input->post('product_sn',true),
		        'issue_date'          => date("Y-m-d H:i:s",time()),
		        'issue_expected_close_date'          => date("Y-m-d H:i:s",time()+86400*$ExpectManDay),
		        'question_subject' =>  $this->input->post('question_subject',true),
		        'associated_sub_order_sn' =>  $this->input->post('associated_sub_order_sn',true),
		        'question_description' =>  $this->input->post('question_description',true),
		        'question_status' => "1"
		    );

			$data = $this->Shop_model->CheckUpdate($data,1);
		    if($nonce = $this->Callcenter->create_callcenter($data)){
		    	if($supplier_sn=$this->input->post('supplier_sn',true)){
				    //管理員s
				    $_where=" where associated_supplier_sn=".$supplier_sn." and member_status=2 and (member_level_type=5 or member_level_type=6) order by last_time_update desc"; //有效經銷會員
					$_result03=$this->Sean_db_tools->db_get_max_record("member_sn,last_name,first_name,email,user_name","ij_member",$_where);
					foreach($_result03 as $_item){
		        		$this->ijw->send_mail('囍市集客服回覆','',$_item->member_sn,nl2br($data['question_description']),'',$this->input->post('supplier_name',true));
		        	}
		    	}
		        $this->session->set_flashdata('message'," 訊息送出成功，客服人員將在 ".$ExpectManDay." 個工作天內回應並處理您的問題！");
		    }else{
		        $this->session->set_flashdata('message',"訊息送出失敗！");
		    }
		}
			redirect(base_url('member/kefu_list'));
	}


	/**
	 *  客服中心回應內容
	 *	直接顯示在 model 中
	 *
	 */
	public function callcenterMsg($member_question_sn)
	{
		if($member_question_sn){
			$this->LoginCheck();
			$page_data = array();

			$_result = $this->Shop_model->_get_question_qna_log($member_question_sn,$this->session->userdata['web_member_data']['member_sn']);
			//var_dump($_result[0]['question_status']);
			// 內容設定
			if(@$_result[0]['question_status']=='5') //已結案
			{
				// 若為已解決， modal footer 不顯示
				$page_data['done'] = true;
			}
			else
			{
				$page_data['done'] = false;
			}
			$page_data['_result'] = $_result;
			$this->load->view('www/page_callcenter/modal_ticket_msg', $page_data);
		}
	}

	/**
	 * 訂單查詢 Template
	 *
	 */
	public function memberOrderList()
	{
		$this->libraries_model->auto_update_orders();//自動異動訂單
		$this->load->model('Member_model');
		$this->LoginCheck();
		$page_data = array();
		$page=$this->input->get('page',true);
		if($this->input->post()){
			if(!$this->input->post('order_item_sn','true')){
	        $this->session->set_flashdata('message',"請選擇欲退訂的商品！");
				//exit();
			}else{
				$order_item_sn_array=$this->input->post('order_item_sn','true');
				//var_dump($order_item_sn_array);exit;
				$return_amount=0;
				$sub_order_data=$this->libraries_model->get_orders(array('sub_order_sn'=>$this->input->post('sub_order_sn',true)))[0];
				if($sub_order_data['delivery_status']=='2'){
        			$this->session->set_flashdata('message',"備貨中無法退訂！");
					redirect(base_url('member/memberOrderList'));
				}
				foreach($order_item_sn_array as $_Item){
					if(!@$ifget && !$this->input->post('ifget_'.$_Item,'true')){
	        			$this->session->set_flashdata('message',"請選擇是否收到商品！");
						redirect(base_url('member/memberOrderList'));
					}
					if(!@$ccapply_code && !$this->input->post('ccapply_code_'.$_Item,'true')){
	        			$this->session->set_flashdata('message',"請選擇退訂原因！");
						redirect(base_url('member/memberOrderList'));
					}
					$return_amount=$return_amount+$this->input->post('actual_sales_amount_'.$_Item,'true');
					if($this->input->post('ifget_'.$_Item,'true')) $ifget=$this->input->post('ifget_'.$_Item,'true');
					if($this->input->post('ccapply_code_'.$_Item,'true')) $ccapply_code=$this->input->post('ccapply_code_'.$_Item,'true');
					//var_dump($ifget);exit;
		 			$data = array(
			        'order_item_sn' => $_Item,
			        'associated_order_sn' 			=> $this->input->post('order_sn',true),
			        'associated_sub_order_sn'    => $this->input->post('sub_order_sn',true),
			        'order_cancel_date'    => date("Y-m-d H:i:s",time()),
			        'create_date'          => date("Y-m-d H:i:s",time()),
			        'last_time_update'     => date("Y-m-d H:i:s",time()),
			        'order_cancel_reason_code'    => $ccapply_code,
			        'receive_goods_flag'          => $ifget,
			        'return_amount'          => $this->input->post('actual_sales_amount_'.$_Item,'true'),
			        'cacel_order_process_status'        => '1',
			        'receive_return_goods_flag'        => '0'
			    );
				    if($this->input->post('order_cancel_sn_'.$_Item,'true')){
				    	//var_dump($_Item);exit;
				    	$this->Shop_model->_update('ij_order_cancel_log',$data,'order_cancel_sn',$this->input->post('order_cancel_sn_'.$_Item,'true'));
				    }else{
						$this->Shop_model->_insert('ij_order_cancel_log',$data);
					}
				}
					//echo $return_amount;
					//exit();
				//$order_data=$this->Shop_model->_get_order($this->input->post('order_sn',true),'ij_order.order_sn,ij_order.payment_status');

				//var_dump($sub_order_data);exit;
				if(!$this->Member_model->if_all_return($this->input->post('sub_order_sn',true))){
		 			$data2 = array(
			        	'sub_order_status'        => '8' //部份退貨
			    	);
					//if($sub_order_data['delivery_status']=='1'){
					//	$data2['delivery_status']='5';//取消配送
					//}elseif($sub_order_data['delivery_status']=='3' || $sub_order_data['delivery_status']=='4'){
						$data2['delivery_status']='5';//退訂審核中
					//}
				}else{
					if($sub_order_data['delivery_method_name']=='線上開通'){
						if($sub_order_data['pay_status']=='1'){ //未付款
					 			$data2 = array(
						        	'sub_order_status'        => '2' //訂單取消
						    	);
						}else{
				 			$data2 = array(
					        	'sub_order_status'        => '3'//申請取消
					    	);
				 		}
					}else{
						if($sub_order_data['pay_status']=='1'){ //未付款
					 			$data2 = array(
						        	'sub_order_status'      => '2', //訂單取消
						    	);
						}else{
							//if($this->input->post('sub_sum',true)==$return_amount && $ifget=='2'){
					 		//	$data2 = array(
						    //    	'sub_order_status'        => '2'
						    //	);
							//}else{
					 			$data2 = array(
						        	'sub_order_status'        => '3'
						    	);
						    //}
						}
					}

					if($sub_order_data['delivery_status']=='1'){
						$data2['delivery_status']='8';//取消配送
					}elseif($sub_order_data['delivery_status']=='3' || $sub_order_data['delivery_status']=='4'){
						$data2['delivery_status']='5';//退訂審核中
					}
				}
				$data2 = $this->Shop_model->CheckUpdate($data2,0);
				$this->Shop_model->_update('ij_sub_order',$data2,'sub_order_sn',$this->input->post('sub_order_sn',true));
		        $this->ijw->send_mail('訂單取消','',$this->session->userdata['web_member_data']['member_sn'],'',$sub_order_data['sub_order_num']);
    			$this->session->set_flashdata('message',"申請(更新)退訂成功，請靜候管理人員核對無誤並與您聯絡！");
				redirect(base_url('member/memberOrderList'));

				//exit();
			}
		}
		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'www/page_member/page_order'; // 頁面主內容 view 位置, 必要

		$this->load->model('libraries_model');
	    $this->load->library('pagination');
	    $this->load->library('ijw');
		$page_data['page_content']['perpage'] = 5;
		//var_dump($this->Member_model->if_all_return(122));
		$page_data['page_content']['total'] = $this->Member_model->get_orders($this->session->userdata['web_member_data']['member_sn'],0,0);
	    $config = $this->ijw->_pagination('member/memberOrderList', $page_data['page_content']['total'],$page_data['page_content']['perpage']);
	    $this->pagination->initialize($config);
	    $page_data['page_content']['page_links'] = $this->pagination->create_links();

		$page_data['page_content']['orders']=$this->Member_model->get_orders($this->session->userdata['web_member_data']['member_sn'],@$page,@$page_data['page_content']['perpage']);
        //echo '<pre>' . var_export($page_data['page_content']['orders'], true) . '</pre>';
		//var_dump($page_data['page_content']['orders']);
		$page_data['page_content']['cancel_reason'] = $this->Shop_model->_select('ij_order_cancel_reason_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array');
		$page_data['page_content']['ccapply'] = $this->Shop_model->_select('ij_ccapply_code_ct',array('display_flag'=>'1'),0,0,0,'ccapply_code','asc','result_array');
		//var_dump($page_data['page_content']['order']);
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'www/page_member/page_order_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('www/general/main', $page_data);
	}

	/**
	 * 訂單明細查詢 Template
	 *
	 */
	public function memberOrderDetail($order_sn=0,$sub_order_sn=0)
	{
		if($order_sn){
			$this->LoginCheck();
			$page_data = array();

			// 內容設定

			// page level setting
			// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


			// general view setting
			$page_data['general_header'] = array(); // header 可以透過這個參數帶入
			$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
			$page_data['page_content']['view_path'] = 'www/page_member/page_order_list_detail'; // 頁面主內容 view 位置, 必要

			$page_data['page_content']['order'] = $this->Shop_model->_get_order($order_sn,0,0,$sub_order_sn);
        //echo '<pre>' . var_export($page_data['page_content']['order'], true) . '</pre>';
			$page_data['page_content']['sub_order_sn'] = $sub_order_sn;

			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'www/page_member/page_order_detail_js'; // 頁面主內容的 JS view 位置, 必要
			$this->load->view('www/general/main', $page_data);
		}else{
      	$this->session->set_flashdata('message',"很抱歉！查無此訂單詳細資料！");
				redirect(base_url('member/memberOrderList'));
		}
	}



	/**
	 * ==============   PARTNERS TEMPLATE AREA     ==============
	 */

	/**
	 * 合作夥伴專頁 Template
	 *
	 */
	public function partners()
	{
		$this->LoginCheck();
		$page_data = array();

		// 內容設定

		// page level setting

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview_partners/page_home'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview_partners/page_home_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview_partners/main', $page_data);
	}

	/**
	 * 登入頁面 Template
	 *
	 */
	public function partnersSignIn()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$this->load->view('preview_partners/page_sign_in', $page_data);
	}

	/**
	 * 註冊頁面 Template
	 *
	 */
	public function partnersSignUp()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$this->load->view('preview_partners/page_sign_up', $page_data);
	}

	/**
	 * 全站通知 Template
	 *
	 */
	public function notification($type='',$member_message_center_sn=0)
	{
		$this->LoginCheck();
		if($this->input->post()){
			if($this->input->post('member_message_center_sn')){
				if($this->input->post('flg')=='read'){ //判斷已讀或刪除
					$data_array['read_flag'] = '1';
					$this->session->set_flashdata("fail_message","通知標為已讀成功!");
				}elseif($this->input->post('flg')=='del'){
					$data_array['message_status'] = '2';
					$this->session->set_flashdata("fail_message","通知刪除成功!");
				}
				$data_array = $this->Shop_model->CheckUpdate($data_array,0);
				foreach($this->input->post('member_message_center_sn') as $_Item){
				//var_dump($_Item);
				//exit();
					$this->Shop_model->_update('ij_member_message_center_log',$data_array,'member_message_center_sn',$_Item);
				}
				redirect(base_url('member/notification'));
			}else{
				$this->session->set_flashdata("fail_message","請先選擇通知!");
				redirect(base_url('member/notification'));
			}
		}
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		switch($type){
			case "detail":
				if($member_message_center_sn){
					$data_array['read_flag'] = '1';
					$data_array = $this->Shop_model->CheckUpdate($data_array,0);
				  $this->Shop_model->_update('ij_member_message_center_log',$data_array,'member_message_center_sn',$member_message_center_sn);

				  $this->page_data['unreadmessage']= $this->Shop_model->get_unread_message_count();

					$page_data['general_header'] = array(); // header 可以透過這個參數帶入
					$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
					$page_data['page_content']['view_path'] = 'www/page_member/notification_item'; // 頁面主內容 view 位置, 必要
					$page_data['page_content']['message'] = $this->Shop_model->_select('ij_member_message_center_log',array('message_status'=>'1','ij_member_message_center_log.member_sn'=>$this->session->userdata['web_member_data']['member_sn'],'member_message_center_sn'=>$member_message_center_sn),0,0,0,'member_message_center_sn',0,'row');
					if($page_data['page_content']['message']['message_link_url']){
						redirect($page_data['page_content']['message']['message_link_url']);
					}
					if(!$page_data['page_content']['message']){
						$this->session->set_flashdata("fail_message","很抱歉！查無該筆通知的詳細資料");
						redirect(base_url('member/notification'));
					}
					$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
					$page_data['page_level_js']['view_path'] = 'www/page_member/notification_item_js'; // 頁面主內容的 JS view 位置, 必要
					$this->load->view('www/general/main', $page_data);
				}else{
						$this->session->set_flashdata("fail_message","很抱歉！查無該筆通知的詳細資料");
						redirect(base_url('member/notification'));
				}
			break;
			default:
				// general view setting
				$page=($this->input->get('page',true))? $this->input->get('page',true):'1';
				$this->load->library('pagination');
				$this->load->library('ijw');
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['total'] = count($this->Shop_model->_select('ij_member_message_center_log',array('default_display_time <='=>date('Y-m-d H:i:s'),'message_status'=>'1','ij_member_message_center_log.member_sn'=>$this->session->userdata['web_member_data']['member_sn']),0,0,0,'ij_member_message_center_log.last_time_update','desc','result'));
				$page_data['page_content']['perpage'] = 10;
				if($page_data['page_content']['total']==0) $page=0;
				$page_data['page_content']['start'] = ($page > 1)? ($page-1)*$page_data['page_content']['perpage']:'0';
				$page_data['page_content']['end'] = ($page)? $page*$page_data['page_content']['perpage']:'0';
				$config = $this->ijw->_pagination('member/notification', $page_data['page_content']['total'],$page_data['page_content']['perpage']);
				$this->pagination->initialize($config);
				//var_dump($page_data['page_content']['perpage']);
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
        		$page_data['page_content']['page_links'] = $this->pagination->create_links();
				$page_data['page_content']['view_path'] = 'www/page_member/notification'; // 頁面主內容 view 位置, 必要
				$page_data['page_content']['messages'] = $this->Shop_model->_select('ij_member_message_center_log',array('default_display_time <='=>date('Y-m-d H:i:s'),'message_status'=>'1','ij_member_message_center_log.member_sn'=>$this->session->userdata['web_member_data']['member_sn']),0,$page_data['page_content']['start'],$page_data['page_content']['perpage'],'ij_member_message_center_log.last_time_update','desc','result_array');
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'www/page_member/notification_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('www/general/main', $page_data);
			break;
		}
	}






	/**
	 * 搜尋結果 Template
	 *
	 */
	/*public function notification_list($type='',$member_message_center_sn=0)
	{
		$this->LoginCheck();
		if($this->input->post()){
			if($this->input->post('flg')=='read'){ //判斷已讀或刪除
				$data_array['read_flag'] = '1';
				$this->session->set_flashdata("fail_message","通知標為已讀成功!");
			}elseif($this->input->post('flg')=='del'){
				$data_array['message_status'] = '2';
				$this->session->set_flashdata("fail_message","通知刪除成功!");
			}
			$data_array = $this->Shop_model->CheckUpdate($data_array,0);
			foreach($this->input->post('member_message_center_sn') as $_Item){
			//var_dump($_Item);
			//exit();

				$this->Shop_model->_update('ij_member_message_center_log',$data_array,'member_message_center_sn',$_Item);
			}
			redirect(base_url('member/notification'));
		}
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		switch($type){
			case "detail":
				if($member_message_center_sn){
					$data_array['read_flag'] = '1';
					$data_array = $this->Shop_model->CheckUpdate($data_array,0);
				  $this->Shop_model->_update('ij_member_message_center_log',$data_array,'member_message_center_sn',$member_message_center_sn);

				  $this->page_data['unreadmessage']= $this->Shop_model->get_unread_message_count();

					$page_data['general_header'] = array(); // header 可以透過這個參數帶入
					$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
					$page_data['page_content']['view_path'] = 'www/page_member/notification_item'; // 頁面主內容 view 位置, 必要
					$page_data['page_content']['message'] = $this->Shop_model->_select('ij_member_message_center_log',array('message_status'=>'1','ij_member_message_center_log.member_sn'=>$this->session->userdata['web_member_data']['member_sn'],'member_message_center_sn'=>$member_message_center_sn),0,0,0,'member_message_center_sn',0,'row');
					if($page_data['page_content']['message']['message_link_url']){
						redirect($page_data['page_content']['message']['message_link_url']);
					}
					if(!$page_data['page_content']['message']){
						$this->session->set_flashdata("fail_message","很抱歉！查無該筆通知的詳細資料");
						redirect(base_url('member/notification'));
					}
					$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
					$page_data['page_level_js']['view_path'] = 'www/page_member/notification_item_js'; // 頁面主內容的 JS view 位置, 必要
					$this->load->view('www/general/main', $page_data);
				}else{
						$this->session->set_flashdata("fail_message","很抱歉！查無該筆通知的詳細資料");
						redirect(base_url('member/notification'));
				}
			break;
			default:
				// general view setting
				$page=($this->input->get('page',true))? $this->input->get('page',true):'1';
				$this->load->library('pagination');
				$this->load->library('ijw');
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['total'] = count($this->Shop_model->_select('ij_member_message_center_log',array('default_display_time <='=>date('Y-m-d H:i:s'),'message_status'=>'1','ij_member_message_center_log.member_sn'=>$this->session->userdata['web_member_data']['member_sn']),0,0,0,'ij_member_message_center_log.last_time_update','desc','result'));
				$page_data['page_content']['perpage'] = 10;
				if($page_data['page_content']['total']==0) $page=0;
				$page_data['page_content']['start'] = ($page > 1)? ($page-1)*$page_data['page_content']['perpage']:'0';
				$page_data['page_content']['end'] = ($page)? $page*$page_data['page_content']['perpage']:'0';
				$config = $this->ijw->_pagination('member/notification', $page_data['page_content']['total'],$page_data['page_content']['perpage']);
				$this->pagination->initialize($config);
				//var_dump($page_data['page_content']['perpage']);
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
        $page_data['page_content']['page_links'] = $this->pagination->create_links();
				$page_data['page_content']['view_path'] = 'www/page_member/notification_list'; // 頁面主內容 view 位置, 必要
				$page_data['page_content']['messages'] = $this->Shop_model->_select('ij_member_message_center_log',array('default_display_time <='=>date('Y-m-d H:i:s'),'message_status'=>'1','ij_member_message_center_log.member_sn'=>$this->session->userdata['web_member_data']['member_sn']),0,$page_data['page_content']['start'],$page_data['page_content']['perpage'],'ij_member_message_center_log.last_time_update','desc','result_array');
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'www/page_member/notification_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('www/general/main', $page_data);
			break;
		}
	}*/

	/*
	* 會員中心中個人婚禮網站與婚宴管理列表
	*/
    public function my($page='ewedding',$addnew=0,$product_sn=0)
    {
    	//$array['price']=$this->Shop_model->_get_package_price_by_psn(50,1,0,2);
			//var_dump($array['price']);
    	if($data_array=$this->input->post()){
    		if(is_array($wedding_serial=$this->Shop_model->_get_wedding_serials($data_array['serialnumber']))){
    			if(@$data_array['order_sn']){
						$wedding_website_title=$this->Shop_model->_select('ij_wedding_website','associated_order_sn',$data_array['order_sn'],0,0,'wedding_website_title',0,'row','wedding_website_title',0,0,0)['wedding_website_title'];
					}
					//var_dump($wedding_serial);
					if($wedding_serial['associated_product_sn'] && $_product_db=$this->Shop_model->_get_product($wedding_serial['associated_product_sn'])){
						//var_dump($_product_db);
						//exit();
		        $order = array(
		            'orginal_total_order_amount'  => $wedding_serial['price'],
		            'payment_status'  => '2', //已付款
		            'payment_method'  => '10', //序號券折抵
		            'payment_done_date'  => date("Y-m-d H:i:s",time()),
		            'total_order_amount'  => $wedding_serial['price'],
		            'original_order_sn'  => @$data_array['order_sn'], //母訂單
		            'member_sn'  => $this->session->userdata['web_member_data']['member_sn']
		        );
   					$order_item['product_name']=$_product_db['product_name'];
						$order_item['product_package_name']=$wedding_serial['product_package_name'];
						//$this->Shop_model->_select('ij_product_package_config','product_package_config_sn',$_Item['associated_product_template_sn'],0,0,0,0,'row','product_package_name',0,0,0)['product_package_name'];
						$order_item['product_sn']=$wedding_serial['associated_product_sn'];
						$order_item['mutispec_stock_sn']=$wedding_serial['associated_product_package_config_sn'];
						$order_item['buy_amount']=1;
						$order_item['virtual_prod_addon_update_status']='1';
						$order_item['sales_price']=$wedding_serial['price'];
						$order_item['actual_sales_amount']=$wedding_serial['price'];
						$order_item['cost']=$_product_db['cost'];
						//新增訂單
						$order_sn=$this->libraries_model->_save_order($order,$order_item);
						if(!$order_sn){
						  $this->session->set_flashdata("fail_message","新增失敗，錯誤訊息：訂單新增失敗，敬請回報客服處理");
					    redirect(base_url('member/my/ewedding'));
							exit();
						}else{
							//異動序號券狀態
    					if(@$data_array['order_sn']){ //加規
								$upt_data['associated_wedding_website_sn']=$this->Shop_model->_select('ij_wedding_website','associated_order_sn',$data_array['order_sn'],0,0,'wedding_website_sn',0,'row','wedding_website_sn',0,0,0)['wedding_website_sn'];
    					}else{ //新增
								$upt_data['associated_wedding_website_sn']=$this->Shop_model->_select('ij_wedding_website','associated_order_sn',$order_sn,0,0,'wedding_website_sn',0,'row','wedding_website_sn',0,0,0)['wedding_website_sn'];
    					}
							$upt_data['serial_card_status']='3';//開通
							$upt_data['used_member_sn']=$this->session->userdata['web_member_data']['member_sn'];
							$upt_data['used_date']=date("Y-m-d H:i:s",time());
							$upt_data['associated_order_sn']=$order_sn;
							//var_dump($data_array['order_sn']);
							//var_dump($upt_data);
							//exit();
							$this->Shop_model->_update('ij_serial_card',$upt_data,'serial_card_sn',$wedding_serial['serial_card_sn']);
						}
						//線上開通
						$order=$this->Shop_model->chk_order_online($order_sn);
						//exit();
						if($order){
    					if(@$data_array['order_sn']){
								$message='恭喜您! 序號已驗証有效。</br>
													'.$wedding_serial['product_package_name'].'規格: </br>
													'.$order.'
													已加規至您的 '.$wedding_website_title.' 結婚網站，請點編輯進入婚禮網站編輯總覽確認';
			//echo $this->db->last_query();
							}else{
								$message='恭喜您! 序號已驗証有效。</br>
													'.$wedding_serial['product_package_name'].'規格: </br>
													結婚網站已新增成功，請點編輯進入婚禮網站編輯總覽確認';
							}
					  	$this->session->set_flashdata("success_message",$message);
						}else{
					  	$this->session->set_flashdata("fail_message",'很抱歉！線上自動啟用失敗，可能是網路傳輸問題或是資料有誤，敬請回報客服處理。');
						}
				    redirect(base_url('member/my/ewedding'));
						exit();
					}
    		}else{
				  $this->session->set_flashdata("fail_message",$wedding_serial);
    		}
				//var_dump($data_array);
    	}
    	//免費試用
			if(base64_decode(urldecode($addnew))=='add_free_site' && $product_sn){
				if(!$this->session->userdata['web_member_data']['member_sn']){
					$this->session->set_flashdata("HTTP_REFERER",  base_url('member/my/ewedding/'.$addnew.'/'.$product_sn)); //紀錄返回
				  $this->session->set_flashdata("fail_message","請先登入會員");
					$this->LoginCheck();
				}
        $order = array(
            'orginal_total_order_amount'  => 0,
            'total_order_amount'  => 0,
            'payment_status'  => '2', //已付款
            'payment_done_date'  => date("Y-m-d H:i:s",time()),
            'member_sn'  => $this->session->userdata['web_member_data']['member_sn']
        );

				if($product_sn && $_product_db=$this->Shop_model->_get_product($product_sn)){
					//var_dump($_product_db);
					//exit();
					$order_item['product_name']=$_product_db['product_name'];
					$order_item['product_package_name']='試用版';
					$order_item['product_sn']=$product_sn;
					$order_item['mutispec_stock_sn']=$this->Shop_model->_select('ij_product_package_config','product_package_name','試用版',0,0,'product_package_name',0,'row','product_package_config_sn',0,0,0)['product_package_config_sn']; //試用版編號
					$order_item['virtual_prod_addon_update_status']='1';
					$order_item['buy_amount']=1;
					$order_item['sales_price']=0;
					$order_item['actual_sales_amount']=0;
					$order_item['cost']=0;
					//新增訂單
					$order_sn=$this->libraries_model->_save_order($order,$order_item);
					if(!$order_sn){
					  $this->session->set_flashdata("fail_message","新增失敗，錯誤訊息：訂單新增失敗");
				    redirect(base_url('member/my/ewedding'));
						exit();
					}

					//線上開通
					$order=$this->Shop_model->chk_order_online($order_sn);
					//var_dump($order);
					//exit();
					if($order){
					  $this->session->set_flashdata("success_message","試用網站已新增成功!");
					}
				}else{
					$this->session->set_flashdata("fail_message","查無版型資料");
				}
				redirect(base_url('member/my/ewedding'));
			}

			$this->LoginCheck();

			//取得婚禮網站列表（先要token）
			$this->load->library("ijw");
			$data_array2['username']=$this->session->userdata['web_member_data']['user_name'];
			$data_array2['pass']=$this->session->userdata['web_member_data']['password'];
			//$data_array2['username']='2222@gmail.com';
			//$data_array2['pass']='2222';
			$data_array2['key']='202cb962ac59075b964b07152d234b70';
			$data_array2['secret']='250cf8b51c773f3f8dc8b4be867a9a02';
			//var_dump($data_array2);
			$url=API_WEBSITE.'api/member/userLogin';
			$login_token=$this->ijw->get_array_from_curl($url,$data_array2);
			//var_dump($login_token);
			//$url=API_WEBSITE.'api/member/member_data';
			//$data_array3['login_token']=$login_token['login_token'];
			//$member_data=$this->ijw->get_array_from_curl($url,$login_token);
			//var_dump($member_data);
			//var_dump($this->session->userdata['web_member_data']['password']);
			//exit();
			if(@$login_token['response_code']=='OK'){
				//$data_array['member_sn']=$this->session->userdata['web_member_data']['member_sn'];
				$data_array=array();
				$data_array['member_sn']=$login_token['member_sn'];;
				$data_array['app_token']=$login_token['login_token'];
			//var_dump($data_array);
				$url=API_INVSITE.'api/wedding_website_list';
				$response=$this->ijw->get_array_from_curl($url,$data_array);
			  //var_dump($response);
			}else{
				$response='';
			}
      //var_dump($_SERVER['HTTP_HOST']);
	    $page_data = array();
			$page_data['general_header'] = array(); // header 可以透過這個參數帶入
			$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
			if(@$response['wedding_website_list']){
				foreach($response['wedding_website_list'] as $key=>$_Item ){
					$product_package_config_sn=$this->Shop_model->_get_order_product_package_config_sn($_Item['associated_order_sn'],$_Item['associated_product_sn']);
					$response['wedding_website_list'][$key]['product_package_name']=$this->Shop_model->_select('ij_product_package_config','product_package_config_sn',$product_package_config_sn,0,0,0,0,'row','product_package_name',0,0,0)['product_package_name'];
					$response['wedding_website_list'][$key]['product_name']=$this->Shop_model->_select('ij_product','product_sn',$_Item['associated_product_sn'],0,0,0,0,'row','product_name',0,0,0)['product_name'];
					$upg_products=@$this->Shop_model->get_upg_products($_Item['associated_order_sn'],$_Item['associated_product_sn'])['upg_products'];
			//var_dump($upg_products[0]).'<br>';
					if($upg_products){
						$upg_product_template_sn=$upg_products[0]['product_package_config_sn'];
					}else{
						$upg_product_template_sn='';
					}

					$response['wedding_website_list'][$key]['product_spec']=$upg_product_template_sn;
					$response['wedding_website_list'][$key]['sub_order_sn']=$this->Shop_model->_select('ij_sub_order',array('order_sn'=>$_Item['associated_order_sn'],'channel_name'=>'婚禮網站'),0,0,0,'sub_order_sn',0,'row','sub_order_sn',0,0,0)['sub_order_sn'];
					/*if($_Item['associated_order_sn']){
						//echo $_Item['associated_order_sn'];
						$order=$this->Shop_model->_get_order($_Item['associated_order_sn']);
						//var_dump($order);
						foreach($order['sub_order'] as $key2=>$_Item2 ){
							foreach($_Item2['order_item'] as $key3=>$_Item3 ){
								//echo $_Item3['product_package_name'];
								if($_Item3['product_sn']==$_Item['associated_product_sn']){
									$response['wedding_website_list'][$key]['product_package_name']=$_Item3['product_package_name'];
									$response['wedding_website_list'][$key]['product_name']=$_Item3['product_name'];
								}
							}
						}
					}*/
				}
				$page_data['page_content']['wedding_website_list'] = $response['wedding_website_list'];
			}else{
				$this->session->set_flashdata("fail_message","尚無婚禮網站");
				$page_data['page_content']['wedding_website_list'] = array();
			}
			$page_data['page_content']['view_path'] = 'www/page_member/page_'.$page; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js']['view_path'] = 'www/page_member/page_'.$page.'_js'; // 頁面主內容的 JS 由這個參數帶入, 必要
			$this->load->view('www/general/main', $page_data);
    }
	/**
	 * 婚禮網站加購
	 *
	 */
	public function ewedding_addon($wedding_website_sn = false)
	{
		if($wedding_website_sn){
			$this->LoginCheck();
			if($this->input->post('wedding_website',true) && $this->input->post('wedding_website_sn',true)){
					$data_array=$this->input->post('wedding_website',true);

					$this->session->set_userdata(array('Mes'=>'更新成功!'));
					redirect(base_url('member/my/ewedding'));
					exit();
			}
			$page_data = array();
			$this->load->model('libraries_model');
			$wedding_website=$this->libraries_model->_get_wedding_websites($wedding_website_sn);
			$page_data['wedding_website'] = $wedding_website;
			$page_data['wedding_website_sn'] = $wedding_website_sn;
			//$page_data['addons'] = $this->Shop_model->get_addon_products($wedding_website[0]['associated_order_sn'],$wedding_website[0]['associated_product_sn']);
			$page_data['product'] = $this->Shop_model->_get_product($wedding_website[0]['associated_product_sn']);
			$this->load->view('www/page_member/modal_website_addons', $page_data);
		}else{
			$this->session->set_userdata(array('Mes'=>'查無婚禮網站加購項目資料!'));
			redirect(base_url('member/my/ewedding'));
		}
	}

    //製作驗證碼
	  private function captcha_img()
    {
        $this->load->helper('captcha');
				$pool = '0123456789';
        $word = '';
        for ($i = 0; $i < 4; $i++){
            $word .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
        }

        $this->session->set_userdata('captcha', $word);

        $vals = array(
            'word'  => $word,
            'img_path'  => FCPATH.'public/uploads/captcha/',
            'img_url'  => base_url().'public/uploads/captcha/',
            'font_path' => FCPATH.'public/fonts/Ostrich-black-webfont.ttf',
						'img_width' => '150',
						'img_height' => 40,
						'font_size'  => 22,
						'expiration' => 7200
            );
        $cap = create_captcha($vals);
        return $cap['image'];
    }

	public function ajaxdel(){
		if(!$this->LoginCheck()){
				echo false;
				exit();
		}
		$table=base64_decode($this->input->post('dtable'));
		$field=base64_decode($this->input->post('dfield'));
		$id=$this->input->post('did');
		if($table && $field && $id){
			$this->load->model('libraries_model');
			if($row=$this->libraries_model->_select($table,$field,$id,0,0,$field,0,'row')){ //檢查是否有值
				switch ($table) {
					case 'ij_track_list_log':
						if($row['track_list_status']=='2'){ //狀態=2 直接刪除
		    				echo $this->libraries_model->_delete($table,$field,$id);
	    			}else{
	    				$upt_data=array('track_list_status'=>'2');//改變狀態->2
	    			}
	        break;
					default:
						if($row['status']=='2'){ //狀態=2 直接刪除
    					echo $this->libraries_model->_delete($table,$field,$id);
						}else{
	    				$upt_data=array('status'=>'2');//改變狀態->2
						}
	        break;
	      }

	        //紀錄log
					$_trans_log["_table"]=$table;
					$_trans_log["_before"]=json_encode($row);
					$_trans_log["_after"]=json_encode($upt_data);
					$_trans_log["_type"]="2";
					$_trans_log["_key_id"]=$id;
					$this->libraries_model->trans_log($_trans_log);

				echo $this->libraries_model->_update($table,$upt_data,$field,$id); //改變狀態2=刪除
			}else{
				echo false;
			}
		}else{
				echo false;
		}
	}
  function get_town_zip($ifvalue=0,$receiver_addr_state="")
  {
    $data = trim($this->input->get_post("data"));
    if(urldecode($receiver_addr_state)=="外島地區"){
		$towns = $this->Shop_model->_select('ij_town_ct',array('display_flag'=>'1','city_code'=>$data,'sort_order>='=>'800'),0,0,0,'zipcode',0,'result_array','town_code,town_name,zipcode'); // town
    }elseif(urldecode($receiver_addr_state)=="台灣本島"){
		$towns = $this->Shop_model->_select('ij_town_ct',array('display_flag'=>'1','city_code'=>$data,'sort_order<'=>'800'),0,0,0,'zipcode',0,'result_array','town_code,town_name,zipcode'); // town
    }else{
		$towns = $this->Shop_model->_select('ij_town_ct',array('display_flag'=>'1','city_code'=>$data),0,0,0,'zipcode',0,'result_array','town_code,town_name,zipcode'); // town
    }
		$_town='<option value="" selected>請選擇</option>';
		foreach($towns as $Item){
			if($ifvalue){
				$_town.=  ' <option value="'.$Item['town_name'].'" zipcode="'.$Item['zipcode'].'">'.$Item['town_name'].'</option>';
			}else{
				$_town.=  ' <option value="'.$Item['town_code'].'" zipcode="'.$Item['zipcode'].'">'.$Item['town_name'].'</option>';
			}
		}
     echo json_encode($_town);
  }
	public function CheckEmail(){
		$email=$this->input->post('dval');
		$field=base64_decode($this->input->post('dfield'));
		if($field='email' && $email){
			//if(filter_var($email, FILTER_VALIDATE_EMAIL)){
				$_where=" where user_name='".$email."'";
				$_check_dup_member=$this->Sean_db_tools->db_get_one_record("user_name","ij_member",$_where);
			  if(isset($_check_dup_member) && $_check_dup_member["user_name"]==$email){
			  	$_result='很抱歉，您填寫的Email已經被註冊！';
			  }else{
			  	$_result='';//未重複
			  }

			//}else{
			//  	$_result='很抱歉，您填寫的Email格式有誤！';
			//}
		}else{
			  	$_result='很抱歉，您沒有填寫 Email?';
		}
   echo $_result;
	}
  function get_ccapply_detail()
  {
    $data = trim($this->input->get_post("data"));
		$towns = $this->Shop_model->_select('ij_ccapply_detail_code_ct',array('display_flag'=>'1','ccapply_code'=>$data),0,0,0,'ccapply_detail_code',0,'result_array','ccapply_detail_code,ccapply_detail_code_name,default_flag'); // town
		$_result='';
		foreach($towns as $Item){
			if($Item['default_flag']){
				$_result.=  ' <option selected value="'.$Item['ccapply_detail_code'].'">'.$Item['ccapply_detail_code_name'].'</option>';
			}else{
				$_result.=  ' <option value="'.$Item['ccapply_detail_code'].'">'.$Item['ccapply_detail_code_name'].'</option>';
			}
		}
     echo json_encode($_result);
  }
  function get_member_name()
  {
	if($this->input->is_ajax_request()) {
	    $data = trim($this->input->get_post("data"));
	    $this->db->select('last_name,first_name');
	    $this->db->from('ij_member');
	    $this->db->where('member_sn',$data);
	    $_result=$this->db->get()->row_array();
	    if($_result) {
	    	echo $_result['last_name'].$_result['first_name'];
	    }else{
	    	echo '查無會員姓名資料';
	    }
	}
  }
   function get_product_package_config(){
    $product_sn = intval($this->input->post("product_sn",true));
    if($product_sn){
			$product = $this->Shop_model->_get_product($product_sn);
			$_result='<option selected value="">請選擇</option>';
			if(@$product['Packages']){
				foreach($product['Packages'] as $Item){
					//if($Item['default_flag']){
					//	$_result.=  ' <option selected value="'.$Item['ccapply_detail_code'].'">'.$Item['ccapply_detail_code_name'].'</option>';
					//}else{
					$tempAtt=' promo_price="'.$Item['promo_price'].'"';
						//var_dump($Item);
					foreach($Item['specs'] as $Item2){
							if($Item2['spec_option_name']=='婚紗照片'){
								$tempAtt.=' upload_pic_total_amount="'.$Item2['spec_option_limitation'].'"';
							}elseif($Item2['spec_option_name']=='精彩影音-相片分享'){
								$tempAtt.=' album_pic_total_amount="'.$Item2['spec_option_limitation'].'"';
							}elseif($Item2['spec_option_name']=='精彩影音-影片分享'){
								$tempAtt.=' album_video_total_amount="'.$Item2['spec_option_limitation'].'"';
							}elseif($Item2['spec_option_name']=='密碼保護'){
								$tempAtt.=' password_flag="'.$Item2['spec_option_limitation'].'"';
							}elseif($Item2['spec_option_name']=='簡訊提醒'){
								$tempAtt.=' sms_total_amount="'.$Item2['spec_option_limitation'].'"';
							}elseif($Item2['spec_option_name']=='使用期限'){
								$tempAtt.=' website_usable_duration="'.$Item2['spec_option_limitation'].'"';
							}
					}
						$_result.=  ' <option '.$tempAtt.' value="'.$Item['product_package_config_sn'].'">'.$Item['product_package_name'].'</option>';
					//}
				}
			}elseif(@$product['mutispec']){
					//$where = array('product_sn' => $data,'qty_in_stock >' => '0');
					//$mutispec=$this->Shop_model->_select('ij_mutispec_stock_log',$where,0,0,0,0,0,'result_array','mutispec_stock_sn,color_name,qty_in_stock');
				foreach($product['mutispec'] as $Item){
					//if($Item['default_flag']){
					//	$_result.=  ' <option selected value="'.$Item['ccapply_detail_code'].'">'.$Item['ccapply_detail_code_name'].'</option>';
					//}else{
					//$tempAtt=' promo_price="'.$Item['promo_price'].'"';
						//var_dump($Item);

						$_result.=  ' <option '.@$tempAtt.' value="'.$Item['mutispec_stock_sn'].'">'.$Item['color_name'].'</option>';
					//}
				}
			}else{
				$_result='<option selected value="">唯一規格</option>';
			}
			/*if($_result){
				 $_result=$_result1.$_result;
			}else{
				$_result='<option selected value="">唯一規格</option>';
			}*/
	     echo json_encode($_result);
	  }else{
	     echo json_encode(array());
	  }
  }
	public function shopItem($product_sn=0)
	{
		if($product_sn){
			$this->session->set_flashdata("HTTP_REFERER",base_url("shop/shopItem/".$product_sn));
			$this->LoginCheck();
		}else{
      		$this->session->set_flashdata('message',"很抱歉！連結有誤！");
			redirect(base_url('member'));
		}
	}
 }
