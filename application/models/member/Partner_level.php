<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Partner_level extends CI_Model {

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('sean_models/Sean_db_tools');


		/* Begin 修改  */
		$this->_ct_name="聯盟會員分級";// 功能名稱
		$this->_url="Member/partnerLevel/";//列表功能url
		$this->_url_item="Member/partnerLevel_item/";//處理動作 url
		$this->_table="ij_partner_level_config";//table name
		$this->_id="partner_level_sn";//table key
		$this->_table_field=array(
					 "partner_level_name" => "名稱",
					 "bonus" => "獎金比例",
					 "newmember" => "招募新會員獎金",
					 "sort_order" => "排序",
					 "status" => "是否啟用",
					 "description" => "說明",
				);


		/* End 修改  */

    }



	public function action_edit( $_where = "") {

		$_result="";
		$_result=$this->Sean_db_tools->db_get_max_record("*",$this->_table,$_where);

		return $_result;

	}

			public function action_insert() {

			$_result="";

			foreach($this->_table_field as $key => $value)
			{
					if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
					$_data[$key]=$this->input->get_post($key);
			}


			//basic insert
			$_data["create_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			$_data["create_date"]=date("Y-m-d H:i:s");
			//basic update and insert
			$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			$_data["last_time_update"]=date("Y-m-d H:i:s");



		$_result=$this->Sean_db_tools->db_insert_record($_data,$this->_table);

		return $_result;

	}

	public function action_update($_key_id,$_key_data) {

			$_result="";

			foreach($this->_table_field as $key => $value)
			{
					if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
					$_data[$key]=$this->input->get_post($key);

			}

	    $_where =" where ".$_key_id." = '".$_key_data."'";
			$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
			$_data["last_time_update"]=date("Y-m-d H:i:s");


		$_result=$this->Sean_db_tools->db_update_record($_data,$this->_table,$_where,$_key_id,$_key_data);

		return $_result;

	}

	public function action_delete( $_where) {

		$_result="";

		$_result=$this->Sean_db_tools->db_delete_record($this->_table,$_where);

		return $_result;

	}

	//取得新排序的值
	public function get_next_sort($_id=0 ) {

		$_result="0";

		$_where=" where ".$this->_id." > 0 order by sort_order desc ";
		$_result=$this->Sean_db_tools->db_get_one_record("sort_order",$this->_table,$_where);

		if(is_array($_result["sort_order"]) )
		$_result=1;
		else
		$_result=$_result["sort_order"]+1;

		return $_result;

	}

	//取得定義參數值
	public function get_define_value($_id)
	{
		if(isset($this->$_id))
		return $this->$_id;
		else
		return "";
	}

}
