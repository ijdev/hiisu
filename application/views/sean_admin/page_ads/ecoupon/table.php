<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>行銷管理 <small>虛擬折扣卷活動</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin-ijwedding/Ads">首頁</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        優惠活動
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin-ijwedding/Ads/ecoupon">虛擬折扣卷</a>
      </li>
    </ul>
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">虛擬折扣卷活動</span>
            </div>
            <div class="actions btn-set">
              <a href="admin-ijwedding/Ads/ecouponItem" class="btn green"><i class="fa fa-plus"></i> 新增虛擬折扣卷活動</a>
            </div>
          </div>
          <div class="table-actions-wrapper pull-right">
          </div>
          <div class="portlet-body">
            <div class="note note-warning">
              <div class="table-actions-wrapper pull-right">
                <span>
                </span>
                <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="8" style="margin: 0 5px;" placeholder="關鍵字">
                <select class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;">
                  <option value="">產業屬性</option>
                  <option value="">飯店</option>
                  <option value="">婚紗</option>
                </select>
                <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button>
              </div>
              <div style="clear:both;"></div>
            </div>
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>活動名稱</th>
                  <th>活動起始日期</th>
                  <th>活動結束日期</th>
                  <th>憑證數量</th>
                  <th>使用數量</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=0;$i<8;$i++):?>
                  <tr class="odd gradeX">
                    <td>
                      優惠活動<?php echo $i;?>
                    </td>
                    <td>2015-05-01</td>
                    <td>2016-05-01</td>
                    <td class="center">100</td>
                    <td class="center">20</td>
                    <td class="center">
                      <a href="admin-ijwedding/Ads/ecouponSend"><i class="fa fa-send-o"></i>發送</a> &nbsp;&nbsp;
                      <a href="admin-ijwedding/Ads/ecouponReport"><i class="fa fa-bar-chart-o"></i>統計</a> &nbsp;&nbsp;
                      <a href="admin-ijwedding/Ads/ecouponItem"><i class="fa fa-edit"></i>設定</a> &nbsp;&nbsp;
                      <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                    </td>
                  </tr>
                <?php endfor;?>
              </tbody>
            </table>

            <div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                  <ul class="pagination" style="visibility: visible;">
                    <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                    <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li class="active"><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                    <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->