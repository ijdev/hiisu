<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>系統設定 <small>付款方式設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="preview">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        系統設定
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="preview/location">婚宴地點管理</a>
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         地點編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-map-marker font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">地點編輯</span>
            </div>
            <div class="tools">
              <a href="preview/location"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">地點名稱 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">住址 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <div class="form-group">
                      <div class="col-md-4">
                        <select class="form-control">
                          <option>台北市</option>
                          <option>新北市</option>
                        </select>
                      </div>
                      <div class="col-md-4">
                        <select class="form-control">
                          <option>中山區</option>
                          <option>中正區</option>
                        </select>
                      </div>
                    </div>
                    <input type="text" class="form-control" id="address" value="汀州路三段">
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">是否啟用 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <select class="form-control">
                      <option>是</option>
                      <option>否</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-lg-3 control-label">更新者</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="administrator" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-3 control-label">更新時間</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="2015-05-25 10:00" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="button" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->