<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容
*/
?>
<div class="container">

<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/pages/css/inbox.css" rel="stylesheet" type="text/css"/>
<!-- Page level plugin styles END -->
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <!--<li>
      <a href="/member/notification">全站通知</a>
    </li>
    <li class="active">
      訊息內容
    </li>-->
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar col-md-2 col-sm-3">
    <div class="desktop_view_sidebar">
		<ul class="list-group margin-bottom-25 sidebar-menu">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
	<div class="mobile_view_sidebar">
		<div class="side-nav-head border_title_bottom">
			<button class="fa fa-bars collapsed" data-toggle="collapse" data-target="#mob_sidebar_drop_down"></button>
			<h4 class="bfont">會員中心<!--會員中心--></h4>
		</div>
		<ul class="list-group margin-bottom-25 sidebar-menu collapse" id="mob_sidebar_drop_down">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="col-md-10 col-sm-9">
    <div class="product-page">
      <h1 class="row">
        <div class="col-md-6 col-sm-6">
          <h4><i class="fa fa-comment-o"></i> 全站通知</h4>
        </div>
        <form method="post" id="form1">
        <div class="col-md-6 col-sm-6">
          <div class="actions btn-set pull-right">
            <a class="btn btn-sm btn-primary" onclick="$('#form1').submit();"><span class="glyphicon glyphicon-trash" style="font-size:16px;"></span> Delete </a>
            <a href="member/notification" class="btn btn-sm btn-primary"><i class="fa fa-list"></i> 回列表</a>
          </div>
            <input type='hidden' name='flg' id='flg' value='del'>
            <input type='hidden' name='member_message_center_sn[]' id='member_message_center_sn' value='<?php echo $message['member_message_center_sn'];?>'>
        </form>
        </div>
      </h1>
      <div class="portlet light">
        <div class="portlet-body">
          <div class="inbox-content">
            <div class="inbox-header inbox-view-header">
              <h3><?php echo $message['message_subject'];?></h3>
            </div>
            <div class="inbox-view-info">
              <div class="row">
                <div class="col-md-7">
                  <span class="bold"> <?php echo $message['update_member_sn'];?> </span>
                  on <?php echo $message['transmit_date'];?>
                </div>
              </div>
              <div class="inbox-view">
                <?php echo $message['message_content'];?>
              </div>
            </div>
          </div>
          <!-- /.inbox-content -->
        </div>
      </div>
      <!-- END EXAMPLE TABLE PORTLET-->
    </div>
  </div>
  <!-- END CONTENT -->
</div>
</div>
<!-- END SIDEBAR & CONTENT -->
<style>
.display_none{
  display:none;
}
.chnge_pass{
  float:left;
  width:40%;
}
.btn-black {
  margin-top:20px;
    background-color: #111;
    color: #FFF;
}
</style>