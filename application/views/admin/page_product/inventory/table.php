<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 庫存管理
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品管理 <small>庫存管理</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/admin">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
        庫存管理
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">庫存管理</span>
            </div>
            <div class="actions btn-set">
              <a data-toggle="modal" href="#searchPro" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
      <form class="form-horizontal" role="form" method="get">
          <div class="portlet-body">
            <div class="note note-warning">
              <div class="table-actions-wrapper pull-right">
                <span>
                </span>
                <input type="text" name="product_sn" value="<?php echo @$product_sn;?>" class="pagination-panel-input form-control input-inline input-sm" size="8" style="margin: 0 5px;" placeholder="商品編號">
                <select name="stock_type" class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;">
                  <option value="0">動作</option>
                  <option value="1" <?php echo (@$stock_type=='1')?'selected':'';?> >入庫</option>
                  <option value="2" <?php echo (@$stock_type=='2')?'selected':'';?> >報廢</option>
                </select>
                <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button> &nbsp; 
                <button id="reset_form" onclick="this.form.flg.value='reset';" class="btn btn-sm purple"><i class="fa"></i> 清除</button>
                <input type="hidden" name="flg" value="">
              </div>
              <div style="clear:both;"></div>
            </div>
</form>
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>主分類</th>
                  <th>商品編號</th>
                  <th>名稱</th>
                  <th>成本價</th>
                  <th>動作</th>
                  <th>數量</th>
                  <th>描述</th>
                  <th>異動時間</th>
                  <th>異動人員</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
<?php	if(@$Items){foreach(@$Items as $Item){?>
                  <tr class="odd gradeX">
                    <td><?php echo $Item['category_name'];?></td>
                    <td><?php echo $Item['product_sn'];?></td>
                    <td><?php echo $Item['product_name'];?><?php echo ($Item['color_name'])?'-'.$Item['color_name']:'';?><a href="shop/shopItem/<?php echo $Item['product_sn'];?>" target="_blank">&nbsp;<i class="fa fa-external-link"></i></a></td>
                    <td align="right"><?php echo $Item['cost'];?>&nbsp;</td>
                    <td class="center">
                      <?php if($Item['purchase_sales_stock_type']=='1'):?>
                        <span class="label label-success">入庫</span>
                      <?php elseif($Item['purchase_sales_stock_type']=='2'):?>
                        <span class="label label-danger">報廢</span>
                      <?php endif;?>
                    </td>
                    <td align="right"><?php echo abs($Item['actua_spec_option_amount']);?>&nbsp;</td>
                    <td><?php echo $Item['description'];?></td>
                    <td><small><?php echo $Item['last_time_update'];?></small></td>
                    <td><?php echo $Item['update_member_sn'];?></td>
                    <td class="center">
                      <a href="admin/product/inventory/item/<?php echo $Item['purchase_sales_stock_sn'];?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
    <?if(stripos($this->page_data['permission'],'del')!==false){?>
                      <a href="javascript:void(0);" class="deleteItem" ItemId="<?php echo $Item['purchase_sales_stock_sn'];?>"><i class="fa fa-trash-o"></i>刪除</a>
                      <?}?>
                    </td>
                  </tr>
<?php }}?>
              </tbody>
            </table>

            <div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">筆數：<?php echo $total;?></div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
									<?php echo $page_links;?>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
<div class="modal fade" id="searchPro" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form class="form-horizontal" role="form" action="admin/product/inventory/item" method="get">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">輸入商品編號開始調整庫存</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="name" class="col-md-2 control-label">商品編號</label>
            <div class="col-md-10">
              <input type="text" class="form-control" id="product_sn" name="product_sn" value="">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn default" data-dismiss="modal">Close</button>
          <input type="submit" class="btn blue" value="確定送出">
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->