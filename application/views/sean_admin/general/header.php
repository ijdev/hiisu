<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Page Header, include by general/main.php，變數由之傳入
*/
?>
<!-- BEGIN HEADER -->
<div class="page-header">
  <!-- BEGIN HEADER TOP -->
  <div class="page-header-top">
    <div class="container">
      <!-- BEGIN LOGO -->
      <div class="page-logo">
        <a href="admin/main"><img src="public/metronic/frontend/layout/img/logos/logo-corp-red.png" alt="logo" class="logo-default"></a>
      </div>
      <!-- END LOGO -->
      <!-- BEGIN RESPONSIVE MENU TOGGLER -->
      <a href="javascript:;" class="menu-toggler"></a>
      <!-- END RESPONSIVE MENU TOGGLER -->
      <!-- BEGIN TOP NAVIGATION MENU -->
      <div class="top-menu">
        <ul class="nav navbar-nav pull-right">
          <!-- BEGIN USER LOGIN DROPDOWN -->
          <li class="dropdown dropdown-user dropdown-dark">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <img alt="" class="img-circle" src="public/metronic/admin/layout3/img/avatar9.jpg">
            <span class="username username-hide-mobile"><?php echo $this->session->userdata['member_data']['member_sn'];?></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-default">
              <li>
                <a href="notification">
                <i class="icon-envelope-open"></i> 全站通知 <span class="badge badge-danger"> 3 </span>
                </a>
              </li>
              <li class="divider"></li>
             <?php 
             if($this->session->userdata('member_login')== true)
             {
             	 echo  '
             	 <li>
                <a href="admin/main/managerSignOut">
                <i class="icon-logout"></i> Log Out</a>
              </li>
             	 ';
             }else{
             		echo '
             		 <li>
                <a href="admin/login">
                <i class="icon-key"></i> Log In</a>
              </li>
             		';
             }
             ?>
             
              
            </ul>
          </li>
          <!-- END USER LOGIN DROPDOWN -->
        </ul>
      </div>
      <!-- END TOP NAVIGATION MENU -->
    </div>
  </div>
  <!-- END HEADER TOP -->
  <!-- BEGIN HEADER MENU -->
  <div class="page-header-menu">
    <div class="container">
      <div class="hor-menu ">
        <ul class="nav navbar-nav">
          <li class="active">
            <a href="admin/main">Dashboard</a>
          </li>
          <li class="menu-dropdown classic-menu-dropdown">
            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
              商品管理 <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
              <li class="dropdown-submenu">
                <a href="javascript:void();" class="iconify">
                <i class="icon-handbag"></i> 商品列表 </a>
                <ul class="dropdown-menu">
                  <li class="">
                    <a href="admin/product/productTable/婚禮網站">婚禮網站</a>
                  </li>
                  <li class="">
                    <a href="admin/product/productTable/婚宴管理">婚宴管理</a>
                  </li>
                  <li class="">
                    <a href="admin/product/productTable/結婚商店">結婚商店</a>
                  </li>
                </ul>
              </li>
              <li class="dropdown-submenu">
                <a href="javascript:void();" class="iconify">
                <i class="icon-direction"></i> 商品規格 </a>
                <ul class="dropdown-menu">
                  <li class="">
                    <a href="admin/product/productModule">規格單位</a>
                  </li>
                  <li class="">
                    <a href="admin/product/productPackage">規格組合</a>
                  </li>
                  <li class="">
                    <a href="admin/product/productColor">版型顏色</a>
                  </li>
                  <li class="">
                    <a href="admin/product/productTemplate">版型設定</a>
                  </li>
                </ul>
              </li>
              <li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-tag"></i> 商品分類 </a>
                <ul class="dropdown-menu">
                  <li class="">
                    <a href="admin/product/productCategory">分類管理</a>
                  </li>
                  <li class="">
                    <a href="admin/product/productAttribute">分類屬性</a>
                  </li>
                </ul>
              </li>
              <li>
                <a href="admin/product/inventory" class="iconify">
                <i class="fa fa-dashboard"></i> 庫存管理 </a>
              </li>
              <li>
                <a href="admin/product/productAssess" class="iconify">
                <i class="icon-star"></i> 商品評價 </a>
              </li>
              <li>
                <a href="admin/product/wishList" class="iconify">
                <i class="icon-heart"></i> 追蹤清單 </a>
              </li>
              <li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-tag"></i> 結婚商店首頁 </a>
                <ul class="dropdown-menu">
                  <li class="">
                    <a href="javascript:;">推薦商品編輯</a>
                  </li>
                  <li class="">
                    <a href="javascript:;">相關商品編輯</a>
                  </li>
                  <li class="">
                    <a href="javascript:;">圖廣編輯</a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li class="menu-dropdown classic-menu-dropdown">
            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
              訂單管理 <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
              <li>
                <a href="orderTable" class="iconify">
                <i class="icon-basket"></i> 訂單列表 </a>
              </li>
              <li>
                <a href="orderDealer" class="iconify">
                <i class="fa fa-bank"></i> 經銷商訂單 </a>
              </li>
              <li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="fa fa-credit-card"></i> 電子禮金管理 </a>
                <ul class="dropdown-menu">
                  <li class="">
                    <a href="credit">禮金列表</a>
                  </li>
                  <li class="">
                    <a href="credit/report">使用統計</a>
                  </li>
                </ul>
              </li>
              <li>
                <a href="accounting" class="iconify">
                  <i class="fa fa-dollar"></i> 分潤結帳 
                </a>
              </li>
            </ul>
          </li>
          <li class="menu-dropdown classic-menu-dropdown">
            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
              內容管理 <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
              <li>
                <a href="Content/contentBlog" class="iconify">
                <i class="icon-book-open"></i> 部落格管理 </a>
              </li>
              <li>
                <a href="Content/contentFaq" class="iconify">
                <i class="icon-question"></i> FAQ管理 </a>
              </li>
              <li>
                <a href="Content/contentCategory" class="iconify">
                <i class="icon-tag"></i> 內容分類 </a>
              </li>
            </ul>
          </li>
          <li class="menu-dropdown classic-menu-dropdown">
            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
              行銷管理 <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
              <li>
                <a href="Ads/adsMaterial" class="iconify">
                <i class="icon-bulb"></i> 廣告素材 </a>
              </li>
              <li>
                <a href="Ads/adsSlot" class="iconify">
                <i class="icon-grid"></i> 廣告版位 </a>
              </li>
              <li>
                <a href="Ads/adsMaterialReport" class="iconify">
                <i class="fa fa-bar-chart"></i> 廣告報表 </a>
              </li>
              <li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-diamond"></i> 優惠活動 </a>
                <ul class="dropdown-menu">
                  <li class="">
                    <a href="Ads/certificate">實體券列印</a>
                  </li>
                  <li class="">
                    <a href="Ads/certificateApply">兌換券管理</a>
                  </li>
                  <li class="">
                    <a href="Ads/couponApply">實體折扣卷管理</a>
                  </li>
                  <li class="">
                    <a href="Ads/ecoupon">虛擬折扣卷管理</a>
                  </li>
                </ul>
              </li>
              <li>
                <a href="Ads/edm" class="iconify">
                <i class="icon-envelope-open"></i> EDM 管理 </a>
              </li>
              <li>
                <a href="Ads/event" class="iconify">
                <i class="fa fa-puzzle-piece"></i> 活動查詢 </a>
              </li>
            </ul>
          </li>
          <li class="menu-dropdown classic-menu-dropdown">
            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
              會員管理 <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
              <li>
                <a href="admin/member/memberTable" class="iconify">
                <i class="icon-users"></i> 一般會員 </a>
              </li>
              <li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-diamond"></i> 經銷商 </a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="admin/member/dealerTable">經銷商管理 </a>
                  </li>
                  <li>
                    <a href="admin/member/dealerAD">原生廣告報表 </a>
                  </li>
                  <li>
                    <a href="admin/member/dealerLevel">經銷商分級設定 </a>
                  </li>
                </ul>
              </li>
              <li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-trophy"></i> 聯盟會員 </a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="admin/member/partnerTable">聯盟會員管理 </a>
                  </li>
                 <!-- <li>
                    <a href="admin/member/partnerTable/apply">聯盟會員申請審理 </a>
                  </li>-->
                  <li>
                    <a href="admin/member/partnerLevel">聯盟會員分級設定 </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li class="menu-dropdown classic-menu-dropdown">
            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
              訊息管理 <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
              <li>
                <a href="ticketTable" class="iconify">
                <i class="icon-star"></i> 使用者問題列表 </a>
              </li>
              <li>
                <a href="admin/Sysconfig/TicketCategory" class="iconify">
                <i class="icon-tag"></i> 問題分類管理 </a>
              </li>
              <li>
                <a href="admin/ticketStatus" class="iconify">
                <i class="fa fa-flag"></i></i> 問題狀況管理 </a>
              </li>
              <li>
                <a href="notification/sender" class="iconify">
                <i class="fa fa-comment-o"></i></i> 系統訊息發送 </a>
              </li>
            </ul>
          </li>
          <li class="menu-dropdown classic-menu-dropdown">
            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
              系統設定 <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
              
              
              
          
							<li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-trophy"></i> 會員代碼 </a>
                <ul class="dropdown-menu">
                             	                              
		              <li>
		                <a href="admin/Sysconfig/Member_level_ct" class="iconify">
		                <i class="icon-users"></i>使用者等級代碼  </a>
		              </li> 
		                       	              
		              <li>
		                <a href="admin/Sysconfig/Member_status_ct" class="iconify">
		                <i class="icon-users"></i>一般使用者狀態代碼   </a>
		              </li>  
		              <li>
		                <a href="admin/Sysconfig/Member_level_type_ct" class="iconify">
		                <i class="icon-users"></i>一般使用者身分別代碼  </a>
		              </li> 
		              
		              <li>
		                <a href="admin/Sysconfig/Dealer_status_ct" class="iconify">
		                <i class="icon-users"></i>經銷商使用者狀態代碼  </a>
		              </li>   
		           		 <li>
		                <a href="admin/Sysconfig/Dealer_level_type_ct" class="iconify">
		                <i class="icon-users"></i>經銷商使用者身分別代碼  </a>
		              </li> 
		              <li>
		                <a href="admin/Sysconfig/Supplier_status_ct" class="iconify">
		                <i class="icon-users"></i>供應商使用者狀態代碼  </a>
		              </li>           	              
		               <li>
		                <a href="admin/Sysconfig/Supplier_level_type_ct" class="iconify">
		                <i class="icon-users"></i>供應商使用者身分別代碼  </a>
		              </li> 
		               <li>
		                <a href="admin/Sysconfig/System_status_ct" class="iconify">
		                <i class="icon-users"></i>系統使用者狀態代碼  </a>
		              </li>           	              
		               <li>
		                <a href="admin/Sysconfig/System_level_type_ct" class="iconify">
		                <i class="icon-users"></i>系統使用者身分別代碼  </a>
		              </li> 
		              
		              
		              
		              
		              <li>
		                <a href="admin/Sysconfig/Login_status_ct" class="iconify">
		                <i class="icon-users"></i> 登入狀態代碼 </a>
		              </li>   
		                 	              
		              <li>
		                <a href="admin/Sysconfig/Join_path_ct" class="iconify">
		                <i class="icon-users"></i>加入方式代碼  </a>
		              </li>   
		               <li>
		                <a href="admin/Sysconfig/Cc_faq_close_ct" class="iconify">
		                <i class="icon-users"></i>客服問與答結案代碼  </a>
		              </li>         	              
		              <li>
		                <a href="admin/Sysconfig/Approval_status_ct" class="iconify">
		                <i class="icon-users"></i>批准狀態代碼  </a>
		              </li>            	              
		            
		               <li>
		                <a href="admin/Sysconfig/Send_member_level_type_ct" class="iconify">
		                <i class="icon-users"></i>發送使用者身份別代碼  </a>
		              </li>            	              
		         	
		              <li>
		                <a href="admin/Sysconfig/Wedding_website_status_ct" class="iconify">
		                <i class="icon-users"></i>婚訊平台使用狀態代碼  </a>
		              </li>            	                            
		              <li>
		                <a href="admin/Sysconfig/Question_status_ct" class="iconify">
		                <i class="icon-users"></i>客服問題單處理狀態代碼  </a>
		              </li>
                </ul>
              </li>
              <li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-trophy"></i> 產品代碼 </a>
                <ul class="dropdown-menu">
                   <li>
		                <a href="admin/Sysconfig/Currency_ct" class="iconify">
		                <i class="icon-users"></i>幣別代碼  </a>
		              </li>  
		               <li>
		                <a href="admin/Sysconfig/Product_status_ct" class="iconify">
		                <i class="icon-users"></i>產品狀態代碼  </a>
		              </li>            	                 	      
		              <li>
		                <a href="admin/Sysconfig/Delivery_status_ct" class="iconify">
		                <i class="icon-users"></i>配送狀態代碼  </a>
		              </li>            	                      
		              <li>
		                <a href="admin/Sysconfig/Product_type_ct" class="iconify">
		                <i class="icon-users"></i>產品類別代碼  </a>
		              </li>    
		               <li>
		                <a href="admin/Sysconfig/Product_package_type_ct" class="iconify">
		                <i class="icon-users"></i>產品組合類別代碼  </a>
		              </li>            	              
		              <li>
		                <a href="admin/Sysconfig/Spec_option_type_ct" class="iconify">
		                <i class="icon-users"></i>規格選項類別代碼  </a>
		              </li>            	              
		              <li>
		                <a href="admin/Sysconfig/Brand_status_ct" class="iconify">
		                <i class="icon-users"></i>品牌狀態代碼  </a>
		              </li>            	              
		              <li>
		                <a href="admin/Sysconfig/Category_status_ct" class="iconify">
		                <i class="icon-users"></i>產品分類狀態代碼  </a>
		              </li>   
                </ul>
              </li>
              <li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-trophy"></i> 購物車代碼 </a>
                <ul class="dropdown-menu">
                   <li>
		                <a href="admin/Sysconfig/Order_cancel_reason_ct" class="iconify">
		                <i class="icon-users"></i>訂單取消原因代碼  </a>
		              </li>            	              
		              <li>
		                <a href="admin/Sysconfig/Sub_order_status_ct" class="iconify">
		                <i class="icon-users"></i>子訂單狀態代碼  </a>
		              </li> 
		              <li>
		                <a href="admin/Sysconfig/Cacel_order_process_status_ct" class="iconify">
		                <i class="icon-users"></i>取消訂單處理狀態代碼  </a>
		              </li> 
		              <li>
		                <a href="admin/Sysconfig/Open_invoice_type_ct" class="iconify">
		                <i class="icon-users"></i>發票開立方式代碼  </a>
		              </li>            	              
		              <li>
		                <a href="admin/Sysconfig/Invoice_donor_ct" class="iconify">
		                <i class="icon-users"></i>發票捐贈對象代碼  </a>
		              </li>   
		               <li>
		                <a href="admin/Sysconfig/Invoice_carrie_type_ct" class="iconify">
		                <i class="icon-users"></i>發票載具方式代碼  </a>
		              </li>           	              
		              <li>
		                <a href="admin/Sysconfig/Pricing_method_ct" class="iconify">
		                <i class="icon-users"></i>定價方式代碼  </a>
		              </li> 
		              <li>
		                <a href="admin/Sysconfig/Ta_deliver_time_interval_ct" class="iconify">
		                <i class="icon-users"></i>指定送達時段代碼  </a>
		              </li>  
		               <li>
		                <a href="admin/Sysconfig/Dealer_order_status_ct" class="iconify">
		                <i class="icon-users"></i>經銷商訂單狀態代碼  </a>
		              </li>  
		              <li>
		                <a href="admin/Sysconfig/Payment_method_ct" class="iconify">
		                <i class="icon-users"></i>付款方式代碼  </a>
		              </li>            	                 	      
		              <li>
		                <a href="admin/Sysconfig/Delivery_method_ct" class="iconify">
		                <i class="icon-users"></i>配送方式代碼  </a>
		              </li>            	                      
		              <li>
		                <a href="admin/Sysconfig/Logistics_company_ct" class="iconify">
		                <i class="icon-users"></i>配合物流公司代碼  </a>
		              </li>            	              
		              <li>
		                <a href="admin/Sysconfig/Credit_card_type_ct" class="iconify">
		                <i class="icon-users"></i>信用卡類型代碼  </a>
		              </li>    
                </ul>
              </li>
               <li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-trophy"></i> 行銷代碼 </a>
                <ul class="dropdown-menu">
	                 <li>
		                <a href="admin/Sysconfig/Event_type_ct" class="iconify">
		                <i class="icon-users"></i>活動類別代碼  </a>
		              </li>   
		               <li>
		                <a href="admin/Sysconfig/Serial_card_status_ct" class="iconify">
		                <i class="icon-users"></i>序號卡使用狀態代碼  </a>
		              </li>            	              
		              <li>
		                <a href="admin/Sysconfig/Coupon_code_status_ct" class="iconify">
		                <i class="icon-users"></i>活動優惠代碼使用狀態代碼  </a>
		              </li>  
		               <li>
		                <a href="admin/Sysconfig/Discount_type_ct" class="iconify">
		                <i class="icon-users"></i>優惠折扣類型代碼  </a>
		              </li>            	              
		               <li>
		                <a href="admin/Sysconfig/Serial_card_type_ct" class="iconify">
		                <i class="icon-users"></i>序號卡卡別代碼  </a>
		              </li>           
		                       	               	                
		              <li>
		                <a href="admin/Sysconfig/Discount_code_type_ct" class="iconify">
		                <i class="icon-users"></i>優惠代碼類型代碼  </a>
		              </li>  
		              <li>
		                <a href="admin/Sysconfig/Banner_type_ct" class="iconify">
		                <i class="icon-users"></i>廣告類別代碼  </a>
		              </li>  
                </ul>
              </li>
               <li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-trophy"></i> 其他代碼 </a>
                <ul class="dropdown-menu">
                  <li>
		                <a href="admin/Sysconfig/Status_ct" class="iconify">
		                <i class="icon-users"></i> 使用狀態代碼 </a>
		              </li>
                  <li>
		                <a href="admin/Sysconfig/Country_ct" class="iconify">
		                <i class="icon-users"></i>國家代碼  </a>
		              </li>            	
		              <li>
		                <a href="admin/Sysconfig/State_ct" class="iconify">
		                <i class="icon-users"></i>省(州)代碼  </a>
		              </li> 
		              <li>
		                <a href="admin/Sysconfig/City_ct" class="iconify">
		                <i class="icon-users"></i>城市代碼  </a>
		              </li>            	                    
		              <li>
		                <a href="admin/Sysconfig/Town_ct" class="iconify">
		                <i class="icon-users"></i>行政區代碼  </a>
		              </li>        	              
		              <li>
		                <a href="admin/Sysconfig/Resource_rule_ct" class="iconify">
		                <i class="icon-users"></i>資源規則代碼  </a>
		              </li>            	                        
		              <li>
		                <a href="admin/Sysconfig/Attribute_type_ct" class="iconify">
		                <i class="icon-users"></i>屬性類別代碼  </a>
		              </li> 
		              <li>
		                <a href="admin/Sysconfig/Purchase_sales_stock_status_ct" class="iconify">
		                <i class="icon-users"></i>進銷存貨狀態代碼  </a>
		              </li>            	              
		                         	              
		              <li>
		                <a href="admin/Sysconfig/Purchase_sales_stock_type_ct" class="iconify">
		                <i class="icon-users"></i>進銷存貨類別代碼  </a>
		              </li> 
		              <li>
		                <a href="admin/Sysconfig/Rewrite_config_file" class="iconify">
		                <i class="icon-users"></i>代碼產生設定檔  </a>
		              </li>            	              
		                 
                </ul>
              </li>  	              
          		
          		
          		
          		
          		 <li>
                <a href="admin/main/basic" class="iconify">
                <i class="fa fa-cogs"></i> 一般設定 </a>
              </li>
              <li>
                <a href="admin/main/domain" class="iconify">
                <i class="icon-tag"></i> 產業屬性 </a>
              </li>
              <li>
                <a href="admin/main/payment" class="iconify">
                <i class="fa fa-money"></i> 付款方式 </a>
              </li>
              <li>
                <a href="admin/Sysconfig/Shipping" class="iconify">
                <i class="fa fa-truck"></i> 運費設定 </a>
              </li>
              <li>
                <a href="admin/Sysconfig/Mail" class="iconify">
                <i class="icon-envelope"></i> Mail 內容設定 </a>
              </li>
              <li>
                <a href="admin/Sysconfig/Doc" class="iconify">
                <i class="icon-envelope"></i> 系統文件內容設定 </a>
              </li>
              <li>
                <a href="admin/main/Location" class="iconify">
                <i class="fa fa-map-marker"></i> 婚宴地點管理 </a>
              </li>
              
          
              
              
              
            </ul>
          </li>
           <li class="menu-dropdown classic-menu-dropdown">
            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
              Admin <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
             
              
              <li>
                <a href="admin/main/Sys_member" class="iconify">
                <i class="fa fa-cogs"></i>系統管理員</a>
              </li>
              <li>
                <a href="admin/main/Sys_program" class="iconify">
                <i class="fa fa-cogs"></i>系統程式</a>
              </li>
              <li>
                <a href="admin/main/Sys_resource" class="iconify">
                <i class="fa fa-cogs"></i>系統資源</a>
              </li>
              <li>
                <a href="admin/main/Sys_role" class="iconify">
                <i class="icon-tag"></i>系統角色</a>
              </li>
             
            </ul>
          </li>
          
          
        </ul>
      </div>
      <!-- END MEGA MENU -->
    </div>
  </div>
  <!-- END HEADER MENU -->
</div>
<!-- END HEADER -->