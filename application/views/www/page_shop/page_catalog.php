
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 線上商店分類
*/
 							 $_2_name="";
 							 $_3_name="";
 							 $_1_name="";
 							 $_2_name_id="";
 							 $_3_name_id="";
 							 $_1_name_id="";

 					if(is_array($_category_array_left))
			        {
			        	foreach($_category_array_left as $key => $value)
			        	{
			        		if($value->category_sn == $_key_id && $value->category_status==1)
			        		{
			        			if($value->upper_category_sn){
						         $_3_name_id=$value->category_sn;
			        			 $_3_name=$value->category_name;
			        			 $_3_name_parent=$value->upper_category_sn;
						        	foreach($_category_array as $key => $value)
						        	{
						        		if($value->category_sn == $_3_name_parent && $value->category_status==1)
						        		{
						        			 $_2_name=$value->category_name;
						        			 $_2_name_id=$value->category_sn;
			        			       		$_2_name_parent=$value->upper_category_sn;
			        			     }
						        	}
			        			 }else{
						        			 $_2_name=$value->category_name;
						        			 $_2_name_id=$value->category_sn;
			        			}
			        		}
			        	}
			        	if(@$_2_name_parent){
					        	foreach($_category_array as $key => $value)
					        	{
					        		if($value->category_sn == $_2_name_parent && $value->category_status==1)
					        		{
					        			 $_1_name=$value->category_name;
					        			 $_1_name_id=$value->category_sn;
					        		}
					        	}
					        }
        			}
if($_1_name_id){
	$top_id=$_1_name_id;
}elseif($_2_name_id){
	$top_id=$_2_name_id;
}elseif($_3_name_id){
	$top_id=$_3_name_id;
}
//var_dump($top_id);
?>
<style>
  /*.pi-img-wrapper img{border:1px solid #EFEFEF;}*/
  .sale-product{background-color: #EFEFEF;padding: 12px;}
  .blog_txt p{    line-height: normal;letter-spacing: 0px;}
  h5{letter-spacing: 0px;}
  #shopcetlog .product-list {margin: 0 -15px; overflow: hidden;display: flex; flex-wrap: wrap;}
  .share{float: right;margin-top: 12px;font-size: 14px;}
  .categories_list li ul li:last-of-type {border-bottom: none;}
</style>
<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<link href="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="public/metronic/global/plugins/slider-layer-slider/css/layerslider.css" rel="stylesheet">
<link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css"><!-- for slider-range -->
<!-- Page level plugin styles END -->

<ul class="breadcrumb">
    <li><a href="/<?=$this->router->fetch_class()?>">首頁</a></li>
    <?php if($_1_name){?><li><a href="<?=$this->router->fetch_class()?>/shopCatalog/<?php echo $_1_name_id;?>"><?php echo $_1_name;?></a></li><?php }?>
    <?php if($_2_name){?><li><a href="<?=$this->router->fetch_class()?>/shopCatalog/<?php echo $_2_name_id;?>"><?php echo $_2_name;?></a></li><?php }?>
    <?php if($_3_name){?><li class="active"><a href="<?=$this->router->fetch_class()?>/shopCatalog/<?php echo $_3_name_id;?>"><?php echo $_3_name;?></a></li><?php }?>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->

<form id="form1" class="form-horizontal" role="form" method="get">
<div class=" margin-bottom-40">
  <!-- BEGIN CONTENT -->
  <div class=" margin-bottom-40 ">
    <!-- BEGIN SIDEBAR -->

 <div id="shopcetlog"  class=" padding_left0 sidebar col-md-3 col-sm-4">
<?php if(@$_category_array){?>
     <div class="sidebar-filter categories-menu">
		<h3>本館商品分類
        <i id="icon-filter" data-toggle="collapse" data-target="#goods-s"  class="fa fa-minus-square" ></i>
		<i id="icon-filter3" data-toggle="collapse" data-target="#goods-s"  class="fa fa-plus-square"  ></i>
        </h3>
		<ul class="padding_left0 categories_list collapse in" id="goods-s">
<?foreach($_category_array as $Item){ if($Item->upper_category_sn==$top_id && $Item->category_status==1){
	$counter=0;
	foreach($_category_array as $Item3){ //判斷是否有下層
		if($Item3->upper_category_sn==$Item->category_sn && $Item3->category_status==1){
			$counter++;
		}
	}
	if($counter==0){?>
		<li><a href="/<?=$this->router->fetch_class()?>/shopCatalog/<?=$Item->category_sn?>"><?=$Item->category_name?></a></li>
	<?}else{?>
			<li class="categories_list_sub_li collapsed" data-toggle="collapse" aria-expanded="true" data-target="#side_menu<?=$Item->category_sn?>">
			<?=$Item->category_name?>
				<ul id="side_menu<?=$Item->category_sn?>" class="collapse">
					<?foreach($_category_array as $Item2){
						if($Item2->upper_category_sn==$Item->category_sn && $Item->category_status==1){?>
										<li><a href="/<?=$this->router->fetch_class()?>/shopCatalog/<?=$Item2->category_sn?>"><?=$Item2->category_name?></a></li>
					<?}}?>
				</ul>
			</li>
	<?}?>
<?}}?>
		</ul>
	 </div>
<?}?>

	 <div class="sidebar-filter filter2">
		<h3 class="filter-txt " >商品篩選
		<i id="icon-filter" data-toggle="collapse" data-target="#side_menu3x"  class="fa fa-minus-square" ></i>
		<i id="icon-filter3" data-toggle="collapse" data-target="#side_menu3x"  class="fa fa-plus-square"  ></i>
		</h3>

		<div class="inner_filter2 collapse in" id="side_menu3x">
				<div id="filterschk" class="menu2_chk_box">
		<?php if($page_content['category_attributes']){
      		foreach($page_content['category_attributes'] as $ca){?>
					<h4><?php echo $ca['attribute_name'];?></h4>
					<div class="padding_left0 chk_box_lbl">
			      	<?php if($ca['attribute_value']){
			      		foreach($ca['attribute_value'] as $av){?>
			            <input id="tab<?php echo $av['attribute_value_sn'];?>_check" class="fdcheck" name="attribute_value_sn[]" value="<?php echo $av['attribute_value_name'];?>" type="checkbox" <?php echo (@$av['status'])?'checked':'';?> >
						<label class="sphov" for="tab<?php echo $av['attribute_value_sn'];?>_check"><span><?php echo $av['attribute_value_name'];?></span></label>
			        <?php }}?>
			        </div>
        <?php }}?>
			    </div>
			<p>
				<label class="ranglabal" for="amount">價格範圍</label><br>
			</p>
				<div id="slider-range" class="margin-bottom-10 ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" style="    max-width: 200px;"><div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 100%;"></span></div>
			<p>
			<input type="text" id="amount" name="pricerange" style="border:0;">
			</p>
			<!--p align="center" style="padding-top: 32px;">
				<button class="btn btn-default">進階搜尋</button>
			</p-->
		</div>
      </div>

      <?php /*<div class="sidebar-products margin-bottom-40 clearfix">
        <h2>優惠商品</h2>
<?php if(@$promo_product3){ foreach($promo_product3 as $Item){?>
        <div class="item">
          <a href="<?=$this->router->fetch_class()?>/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['image_path'];?>" alt="<?php echo $Item['image_alt'];?>"></a>
          <h3><a href="<?=$this->router->fetch_class()?>/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><?php echo $Item['product_name'];?></a></h3>
          <div class="price">$<?php echo $Item['price1'];?>&nbsp;<font style="font-size:12px;color:#333333;"><?php echo $Item['price2'];?></font></div>
        </div>
<?php }}?>
      </div>

      <div class="sidebar-products margin-bottom-40 clearfix">
        <h2>推薦商品</h2>
<?php if(@$promo_product2){ foreach($promo_product2 as $Item){?>
        <div class="item">
          <a href="<?=$this->router->fetch_class()?>/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['image_path'];?>" alt="<?php echo $Item['image_alt'];?>"></a>
          <h3><a href="<?=$this->router->fetch_class()?>/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><?php echo $Item['product_name'];?></a></h3>
          <div class="price">$<?php echo $Item['price1'];?>&nbsp;<font style="font-size:12px;color:#333333;"><?php echo $Item['price2'];?></font></div>
        </div>
<?php }}?>
      </div>*/?>

<?php if(@$promo_product5){?>
      <div class="padding_left0 sidebar-products clearfix">
        <div class="sidebar-products_inner">
			<h2 class="pull-left">熱賣商品</h2>
			<?php foreach($promo_product5 as $Item){?>
				<div class="item">
				  <a class="sidepro" href="<?=$this->router->fetch_class()?>/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['image_path'];?>" alt="<?php echo $Item['image_alt'];?>"></a>
				  <h3><a href="<?=$this->router->fetch_class()?>/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><?php echo $Item['product_name'];?></a></h3>
				  <div class="price">$<?php echo $Item['price1'];?>&nbsp;<font style="font-size:12px;color:#333333;"><?php echo $Item['price2'];?></font></div>
				</div>
			<?php }?>
		</div>
		 <div class="sidebar-products_inner2">
			<h2 class="mob_blog_title collapsed" data-toggle="collapse" data-target="#side_menu5">熱賣商品<i class="fa fa-plus" aria-hidden="true"></i></h2>
			<div class="outer_blog_box collapse" id="side_menu5">
				<?php if(@$promo_product5){ foreach($promo_product5 as $Item){?>
					<div class="item">
					  <a class="sidepro" href="<?=$this->router->fetch_class()?>/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['image_path'];?>" alt="<?php echo $Item['image_alt'];?>"></a>
					  <h3><a href="<?=$this->router->fetch_class()?>/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><?php echo $Item['product_name'];?></a></h3>
					  <div class="price">$<?php echo $Item['price1'];?>&nbsp;<font style="font-size:12px;color:#333333;"><?php echo $Item['price2'];?></font></div>
					</div>
				<?php }}?>
			</div>
		</div>
      </div>
<?php }?>
<?php if($blogs){?>
		<div class="padding_left0 sidebar-products clearfix" style="padding: 20px 15px 10px 0;">
			<h2 class="dsk_blog_title">本館文章</h2>
			<div class="outer_blog_box1">
<?php foreach($blogs as $_Item){?>
			<div class="blog_box">
				<div class="blog_img">
		          <a class="img-hover" href="<?=($this->router->fetch_class()=='shop')?'home':'dealer'?>/blogItem/<?php echo $_Item['blog_article_sn'];?>">
		          <?php if($_Item['blog_article_image_save_dir']):?><img class="img-responsive" alt="<?php echo $_Item['blog_article_titile'];?>-圖檔" src="/public/uploads/<?php echo @$_Item['blog_article_image_save_dir'];?>" ><?php else:?><img alt="無圖檔或尚未上傳" src="public/uploads/notyet.gif"><?php endif;?></a>
		        </div>
				<div class="blog_txt">
					<div class="blog-cnt-hrd">
	              		<h5><a style="white-space: normal;" href="<?=($this->router->fetch_class()=='shop')?'home':'dealer'?>/blogItem/<?php echo $_Item['blog_article_sn'];?>"><?php echo $_Item['blog_article_titile'];?></a></h5>
	              	</div>
	              <p><?php echo $this->ijw->truncate(strip_tags($_Item['blog_article_content']),20);?></p>
	        	</div>
        	</div>
<?php }?>
			</div>
			<h2 class="mob_blog_title collapsed" data-toggle="collapse" data-target="#side_menu_blog" style="padding:10px 0px 8px;">本館文章
			<i class="fa fa-plus-square" aria-hidden="true" style="float:right;"></i></h2>
			<div class="outer_blog_box collapse" id="side_menu_blog">
<?php foreach($blogs as $_Item){?>
			<div class="blog_box">
				<div class="blog_img">
		          <a class="img-hover" href="home/blogItem/<?php echo $_Item['blog_article_sn'];?>">
		          <?php if($_Item['blog_article_image_save_dir']):?><img class="img-responsive" alt="<?php echo $_Item['blog_article_titile'];?>-圖檔" src="/public/uploads/<?php echo @$_Item['blog_article_image_save_dir'];?>" ><?php else:?><img alt="無圖檔或尚未上傳" src="public/uploads/notyet.gif"><?php endif;?></a>
		        </div>
				<div class="blog_txt">
					<div class="blog-cnt-hrd">
	              		<h5><a style="white-space: normal;" href="home/blogItem/<?php echo $_Item['blog_article_sn'];?>"><?php echo $_Item['blog_article_titile'];?></a></h5>
	              	</div>
	              <p><?php echo $this->ijw->truncate(strip_tags($_Item['blog_article_content']),20);?></p>
	        	</div>
        	</div>
<?php }?>
			</div>
		</div>
<?php }?>
    </div>
    <!-- END SIDEBAR -->

    <!-- BEGIN CONTENT -->

    <div id="shop_catepg" class="shop_ctnew col-md-9 col-sm-7 catlog-rgt-content padding0 W80">
<?if($banner_save_dir){?>
    <div class="ad_product_image">
		<img alt="<?=$banner_desc?>" src="<?=ADMINUPLOADPATH.$banner_save_dir?>">
	</div>
<?}?>
		<div class=" list-view-sorting clearfix">
			<div class="col-md-2 col-sm-2 list-view">
			  <a href="#"><i class="fa fa-th-large"></i></a>
			  <a href="#"><i class="fa fa-th-list"></i></a>
			</div>
			<div class="col-md-12 col-sm-12 filter-bar">
			  <div class="pull-right">
				<label class="control-label">排序依據</label>
				<select class="form-control input-sm" name="sort" onchange="$('#form1').submit();">
				  <option value="sort_order" <?php if($sort=='sort_order'){?>selected="selected"<?php }?>>最新商品</option>
				  <option value="name_ASC" <?php if($sort=='name_ASC'){?>selected="selected"<?php }?>>名稱 (A - Z)</option>
				  <option value="name_DESC" <?php if($sort=='name_DESC'){?>selected="selected"<?php }?>>名稱 (Z - A)</option>
				  <option value="price_ASC" <?php if($sort=='price_ASC'){?>selected="selected"<?php }?>>價格 (Low &gt; High)</option>
				  <option value="price_DESC" <?php if($sort=='price_DESC'){?>selected="selected"<?php }?>>價格 (High &gt; Low)</option>
				  <option value="rating_DESC" <?php if($sort=='rating_DESC'){?>selected="selected"<?php }?>>訂購數量 (Highest)</option>
				  <option value="rating_ASC" <?php if($sort=='rating_ASC'){?>selected="selected"<?php }?>>訂購數量 (Lowest)</option>
				</select>
			  </div>
			</div>
		</div>
	</form>

		<!-- js effect filter items START -->
<div class="product-list"><!-- clearfix-->
<!-- PRODUCT ITEM-2 START -->
<?php /*if(@$promo_product1){ foreach($promo_product1 as $Item){?>
				<div class="shop-item_outer col-md-3 col-sm-4 col-xs-6 filterpro" money="<?=$Item['price1']?>" data-tag="<?=$Item['attribute_values']?>">
					<div class="product-item">
						  <div class="pi-img-wrapper">
                <a href="<?=$this->router->fetch_class()?>/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['image_path'];?>" class="img-responsive" alt="<?php echo $Item['image_alt'];?>"><!-- style="height:250px;width:250px;"--></a>
						  </div>
						<div class="product-item-btn-box">
						  <h3 class="item-name"><?php echo $Item['product_name'];?></h3>
					<div class="note note-success note-bordered heart<?php echo $Item['product_sn'];?>" style="display:none;"></div>
						  <div class="pi-textsp" style="height: 51px !important;overflow: hidden;">
                      		<a href="<?=$this->router->fetch_class()?>/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><?=$this->ijw->truncate($Item['special_item'],45)?></a>
						 </div>
							<a class="product_icon" title="加入追蹤" href="javascript:void(0);"><i product_sn="<?php echo $Item['product_sn'];?>" class="fa fa-heart"></i></a>
						  <div class="pi-price"><span>$</span><?php echo number_format($Item['price1']);?>&nbsp;<font style="font-size:12px;color:#b52b28;"><?php echo $Item['price2'];?></font>
<?php if($this->session->userdata('web_member_data')['bonus']){?>
								  <div class="share">
								  	分潤％數：<?=($Item['share_percent']*$this->session->userdata('web_member_data')['bonus']/100)*100;?>％
								  </div>
<?php }?>
						  </div>
						 <div class="prd_btn_box">
              <?php if(($Item['unispec_flag'] && ($Item['unispec_qty_in_stock'] || $Item['open_preorder_flag']=='1'))||($Item['unispec_flag']=='0' && count($Item['mutispec'])>=1)){?>
							<a class="btn btn-default prd_btn_lft shopadd" product_sn="<?php echo $Item['product_sn'];?>">立即結帳</a>
							<a class="btn btn-default prd_btn_rgt addcart" product_sn="<?php echo $Item['product_sn'];?>">放入購物車</a>
							<?php }else{?>
              					<a class="btn btn-default" style="cursor:default;" href="javascript:void(0);">補貨中</a>
							<?php }?>
						 </div>
						</div>
						<p class="h_item2"><?php echo ($promotion_label_name=@$Item['promotion'][0]['promotion_label_name'])? $promotion_label_name:'SALE';?></p>
					</div>
				</div>
<?php }}*/?>
          </div>
			<div class="product-list" ><!--clearfix -->
				<?php if(@$products){ foreach($products as $Item){?>
				<div class="shop-item_outer col-md-3 col-sm-4 col-xs-6 filterpro" money="<?=$Item['price1']?>" data-tag="<?=$Item['attribute_values']?>">
					<div class="product-item">
						  <div class="pi-img-wrapper">
							<a href="<?=$this->router->fetch_class()?>/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['image_path'];?>" class="img-responsive" alt="<?php echo $Item['image_alt'];?>"></a>
						  </div>
						<div class="product-item-btn-box">
						  <h3 class="item-name"><?php echo $Item['product_name'];?></h3>
						  <div class="note note-success note-bordered heart<?php echo $Item['product_sn'];?>" style="display:none;"></div>
						  <div class="pi-textsp" style="height: 51px !important;overflow: hidden;">
						  	<a href="<?=$this->router->fetch_class()?>/shopItem/<?php echo $Item['product_sn'];?>/<?php echo $_key_id?>"><?=$this->ijw->truncate($Item['special_item'],45)?></a>
						 </div>
						  <a class="product_icon" title="加入追蹤" href="javascript:void(0);"><i product_sn="<?php echo $Item['product_sn'];?>" class="fa fa-heart"></i></a></h3>

						  <div class="pi-price"><span>$</span><?php echo $Item['price1'];?>&nbsp;<font style="font-size:12px;color:#b52b28;"><?php echo $Item['price2'];?></font>
<?php if(@$this->session->userdata('web_member_data')['bonus']){?>
								  <div class="share">
								  	分潤％數：<?=($Item['share_percent']*$this->session->userdata('web_member_data')['bonus']/100)*100;?>％
								  </div>
<?php }?>
						  </div>
						 <div class="prd_btn_box">
              <?php if(($Item['unispec_flag'] && ($Item['unispec_qty_in_stock'] || $Item['open_preorder_flag']=='1'))||($Item['unispec_flag']=='0' &&  (count($Item['mutispec'])>=1 || $Item['open_preorder_flag']=='1'))){?>
							<a class="btn btn-default prd_btn_lft shopadd" product_sn="<?php echo $Item['product_sn'];?>">立即結帳</a>
							<a class="btn btn-default prd_btn_rgt addcart" product_sn="<?php echo $Item['product_sn'];?>">放入購物車</a>
							<?php }else{?>
              					<a class="btn btn-default" style="cursor:default;" href="javascript:void(0);">補貨中</a>
							<?php }?>
						 </div>
						</div>
<?php if($promotion_label_name=@$Item['promotion'][0]['promotion_label_name']){?>
						<p class="h_item2"><?=$promotion_label_name;?></p>
<?php }?>
<?php /*if($Item['promotion_label_name']=='推薦'){?>
						<p class="h_item2">嚴選</p>
<?php }*/?>
					</div>
				</div>
				<?php }}?>
			</div>
	<!-- END PRODUCT LIST-2 -->

	<!-- BEGIN PAGINATOR -->
				<div class="text-center col-md-12 pull-left">
						<?php echo $page_links;?>
				</div>
      <!-- END PAGINATOR -->
    </div>
    <!-- END CONTENT -->
  </div>
  <!-- END SALE PRODUCT & NEW ARRIVALS -->
  <!-- END CONTENT -->
</div>
<!-- END SIDEBAR & CONTENT -->
