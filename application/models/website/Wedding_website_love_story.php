<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wedding_website_love_story extends CI_Model {

    public $wedding_website_sn;
    public $limit = 10; // query limit setting
    public $until = 0; // last query sn

    public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
    }
    /*
    * Get Wedding Website 愛的故事列表
    * 全數拉出管理
    * 限制的部分 toto
    */
    public function get_result( $wedding_website_sn = 0 )
    {
        $this->db->select('*');
        $this->db->from('ij_wedding_website_love_story');
        $this->db->where('wedding_website_sn', $wedding_website_sn);
        $this->db->where('status', 1);
        $this->db->order_by('publish_date', 'asc');
        $this->db->order_by('wedding_website_love_story_sn', 'desc');
        $query = $this->db->get();
        return $result = $query->result_array();
    }
    /*
    * Update 愛的故事列表單筆資料
    */
    public function update_row( $wedding_website_love_story_sn = 0, $member_sn=0, $data_update = array() )
    {
        $wedding_website_love_story_sn = (int)$wedding_website_love_story_sn;
        return  $this->db->update('ij_wedding_website_love_story', $data_update, 'wedding_website_love_story_sn = '.$wedding_website_love_story_sn.' AND create_member_sn = '. $member_sn);
    }
    /*
    * insert 愛的故事單筆資料
    */
    public function insert_row( $data_insert = array() )
    {
        return  $this->db->insert('ij_wedding_website_love_story', $data_insert);
    }
}