<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 客服中心往返的 msg 列表
*/
?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
  <h4 class="modal-title">婚禮網站序號設定</h4>
</div>
<form role="form" name="reply_form" method="post" action="admin/admin/weddingWebsite_serial_item">
<div class="modal-body">
    <div class="row">
		  <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	          <label class="col-lg-3 control-label">序號</label>
	      			<div class="col-lg-4 col-md-4 col-sm-4">
		        			<?php echo $_result[0]['serial_card_check_code']?>
		      		</div>
	      			<div class="col-lg-3 col-md-3 col-sm-3">
	      				&nbsp;
		      		</div>
	      			<div class="col-lg-2 col-md-2 col-sm-2">
	      				&nbsp;
		      		</div>
		      </div>
		  </div>
		  <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	          <label class="col-lg-3 control-label">經銷商</label>
	      			<div class="col-lg-4 col-md-4 col-sm-4">
                  <select required name="wedding_website[stock_out_dealer_sn]" id="stock_out_dealer_sn" required class="form-control">
                    <option value="">請選擇</option>
                    <?php foreach(@$_member_dealer as $_Item){?>
                    <option <?php echo (@$_result[0]['stock_out_dealer_sn']==$_Item['dealer_sn'])?'selected':'';?> value="<?php echo @$_Item['dealer_sn']?>"><?php echo @$_Item['dealer_short_name'];?></option>
                    <?php }?>
                  </select>
		      		</div>
		      </div>
	    </div> 
      <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
        <label for="note" class="col-lg-3 control-label">館別</label>
	      	<div class="col-lg-4 col-md-4 col-sm-4">
          <!-- 選項在 table ij_config 中的 type = shop_menu -->
          <?php echo $Store;?>
        </div>
      	</div>
      </div>
      <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
        <label class="col-lg-3 control-label">分類 </label>
	      	<div class="col-lg-9 col-md-9 col-sm-9">
          <div class="table-scrollable table-scrollable-borderless">
        		<?php echo $categorys;?>
            <!--span class="help-block"> 按住 control 進行複選 </span-->
          </div>
        </div>
        </div>
      </div>
		  <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	          <label class="col-lg-3 control-label">商品名稱</label>
	      			<div class="col-lg-4 col-md-4 col-sm-4">
                  <select required name="wedding_website[associated_product_sn]" id="associated_product_sn" required class="form-control">
                    <option value="">請選擇</option>
                    <?php foreach(@$templates as $_Item){?>
                    <option <?php echo (@$_result[0]['associated_product_sn']==$_Item['product_sn'])?'selected':'';?> value="<?php echo @$_Item['product_sn']?>"><?php echo @$_Item['product_name']?></option>
                    <?php }?>
                  </select>
		      		</div>
		      </div>
	    </div> 
		  <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	          <label class="col-lg-3 control-label">規格款式</label>
	      			<div class="col-lg-4 col-md-4 col-sm-4">
                  <select required name="wedding_website[associated_product_package_config_sn]" id="product_package_config_sn" required class="form-control">
                    <option value="">請先選擇商品</option>
                    <?php if(@$product['Packages']){foreach(@$product['Packages'] as $_Item){?>
                    <option <?php echo (@$_result[0]['associated_product_package_config_sn']==$_Item['product_package_config_sn'])?'selected':'';?>  value="<?php echo @$_Item['product_package_config_sn']?>"><?php echo @$_Item['product_package_name']?></option>
                    <?php }}?>
                    <?php if(@$product['mutispec']){foreach(@$product['mutispec'] as $_Item){?>
                    <option <?php echo (@$_result[0]['associated_product_package_config_sn']==$_Item['mutispec_stock_sn'])?'selected':'';?>  value="<?php echo @$_Item['mutispec_stock_sn']?>"><?php echo @$_Item['color_name']?></option>
                    <?php }}?>                    
                 </select>
		      		</div>
		      </div>
	    </div>
      <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	        <label for="updater" class="col-lg-3 control-label">設定者</label>
	      	<div class="col-lg-4 col-md-4 col-sm-4">
	          <input type="text" class="form-control" placeholder="<?php echo (@$_result[0]['update_member_sn'])?@$_result[0]['update_member_sn']:@$_result[0]['create_member_sn']; ?>" Disabled>
	        </div>
	       </div>
      </div>
      <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	        <label for="time-update" class="col-lg-3 control-label">設定時間</label>
	      	<div class="col-lg-4 col-md-4 col-sm-4">
	          <input type="text" class="form-control" placeholder="<?php echo (@$_result[0]['last_time_update']!='0000-00-00 00:00:00')?@$_result[0]['last_time_update']:@$_result[0]['create_date']; ?>" Disabled>
	        </div>
      	</div>
      </div>
    </div>
</div>

  <div class="modal-footer">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6">
        <button data-dismiss="modal" style="margin-top: 20px;margin-right: 100px;" type="button" id="cancelbutton" class="btn yellow">取消</button> &nbsp; 
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6">
        <button style="margin-top: 20px;margin-right: 100px;" type="submit" id="replybutton" class="btn blue">送出</button>
        <input type="hidden" name="serial_card_sn" value="<?php echo $_result[0]['serial_card_sn']?>">
        <input type="hidden" name="wedding_website[stock_out_date]" value="<?php echo $_result[0]['stock_out_date']?>">
        <input type="hidden" name="wedding_website[serial_card_status]" value="<?php echo $_result[0]['serial_card_status']?>">
      </div>
    </div>
  </div>
</form>
<script>
jQuery(document).ready(function() {       
   	//Metronic.init(); // init metronic core components
	  //Layout.init(); // init current layout
    //ComponentsPickers.init();
    <?php if($default_channel_name){?>
    	ChgChannelName('<?php echo $default_channel_name?>','<?php echo $default_root_category_sn?>');
  	<?php }?>
});
function ChgChannelName(C,D){ //變動館別、帶入值
	$('#category_id option').remove();$('#category_id').append('<option>Loading...</option>');
	$.get('/admin/product/ChgChannelName3/'+C+'/'+D,function(data){$('#category_id option').remove();$('#category_id').append(data);});
}
	$('#category_id').change(function(event) {
		//console.log(C);
		//event.preventDefault();
		$('#associated_product_sn option').remove();$('#associated_product_sn').append('<option>Loading...</option>');
		var C=$(this).val();
		var D=$('#StoreName').val();
		if(C!=0 && C !=''){
	  $.post('admin/product/getP_by_cat', {
	          categorys: C,
	          channel: D
	      }, function(data) {
					$('#associated_product_sn option').remove();$('#associated_product_sn').append(data);
	      }, 'text');
	  }
	});
	$("#associated_product_sn").on("change", function(e) {
	    //$('#associated_product_template_sn').val($('option:selected', this).attr('associated_product_template_sn'));
	    //$('#product_name').val($('option:selected', this).text());
	    //console.log($('#product_name').val());
	    $.ajax({
				  type: 'POST',
	        url: 'Member/get_product_package_config',
	        data: 'product_sn=' + $(this).val(),
	        dataType: 'json',
	        success: function(result) {
	         	$('#product_package_config_sn').empty();
						$(result).appendTo("#product_package_config_sn");
	        }
	    });
	});	
</script>