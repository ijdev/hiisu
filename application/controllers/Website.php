<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Website extends CI_Controller {
    public $wedding_website_sn = 0;

    public function __construct()
    {
            parent::__construct();
            $this->load->library('io_website');
            $this->load->model([
                'website/wedding_website','website/wedding_thanksgiving_log','website/wedding_website_love_story',
                'website/gift_cash_amount_config','website/wedding_event_config','website/wedding_album',
                'website/wedding_guest_message_log','website/media_warehouse','website/wedding_questionnaire','website/wedding_questionnaire_title'
                ]);

            if($sn=$this->input->get('sn'))
            {
                $this->wedding_website_sn = $this->io_website->wedding_website_sn_decrypt($sn);
            }
            else
            {
                $host = explode('.', $_SERVER['HTTP_HOST']);
                $str_pass = array('www', 'ijwedding', 'test-www', 'info');
                if(COUNT($host) > 1 && in_array($host[0], $str_pass) === FALSE)
                {
                    if($info = $this->wedding_website->wedding_website_row_by_name($host[0]))
                    {
                        $this->wedding_website_sn = $info['wedding_website_sn'];
                    }
                    else
                    {
                        show_404('找不到使用者的網頁');
                    }
                }

            }

            $this->config->load('ij_template');
    }
	/**
	 * index ================
	 * ** Home for Wedding Website
	 * ** 若身份驗證通過並且擁有此 website, 未開放或無需密碼即可通行
	 * 
	 */
	public function index($sn=0)
	{
        $this->load->helper('url');
        $this->load->library('user_agent');

        if($this->wedding_website_sn == 0 && $sn < 100)
        {
            $this->wedding_website_sn = (int)$sn;
        }
        $page_data = array();

        // Raw data :  if check cache not exsit，get data from db
        $website_page_data_cache_name = 'website_page_data_'.$this->wedding_website_sn;
        $this->load->driver('cache', array('adapter' => 'file'));
        //if ( ! $str_website_page_data = $this->cache->get($website_page_data_cache_name))
        if ( true )
        {
            if(!$page_data['website_info'] = $this->wedding_website->wedding_website_row($this->wedding_website_sn))
            {
                show_404('結婚網站資料無法存取');
            }
            $password_pass = (int)$this->session->userdata('password_pass');
            if($page_data['website_info']['password'] && $password_pass!== (int)$page_data['website_info']['wedding_website_sn'])
            {
                //var_dump($page_data);
                $this->load->view('template/general/lock', $page_data);
                return false;
            }
            //var_dump($page_data['website_info']['associated_product_template_sn']);exit();
            $template_list = $this->wedding_website->getTemplateList();
            $page_data['website_info']['associated_product_template_sn']=10;
            foreach($template_list as $template)
            {
                if($template['product_template_sn']==$page_data['website_info']['associated_product_template_sn'])
                {
                    $page_data['website_info']['template'] = $template;
                    break;
                }
            }
            //var_dump($page_data['website_info']['template']['website_color_set'][0]['color_code']);exit();

            // show or hide by module status
            $page_data['module_not_show'] = json_decode($page_data['website_info']['wedding_website_subtitle'], true);
            $page_data['module_status'] = json_decode($page_data['website_info']['module_status'], true);
            $page_data['module_status_last'] = false;
            if($page_data['module_status'])
            {
                $_module_status = array_reverse($page_data['module_status']);
                foreach($_module_status as $index => $row)
                {
                    if($index!='gift')
                    {
                        $page_data['module_status_last'] = $index;
                        break;
                    }
                }
                //end($page_data['module_status']);
                //$page_data['module_status_last'] = key($page_data['module_status']);
            }
            $page_data['wedding_website_divider'] = $this->wedding_website->wedding_website_divider($this->wedding_website_sn);
            //var_dump($page_data['module_status']);

            $page_data['thanksgiving'] = $this->wedding_thanksgiving_log->get_result($this->wedding_website_sn);
            $page_data['stories'] = $this->wedding_website_love_story->get_result($this->wedding_website_sn);
            $page_data['gift_cashs'] = $this->gift_cash_amount_config->get_result($this->wedding_website_sn);
            $page_data['rsvp_show'] = false;
            if($page_data['events'] = $this->wedding_event_config->get_result($this->wedding_website_sn))
            {
                foreach($page_data['events'] as $index => $event)
                {
                    if($page_data['events'][$index]['wedding_questionnnaire'] = $this->wedding_questionnaire->get_row_by_event($event['wedding_event_sn']))
                    {
                        $page_data['events'][$index]['wedding_questionnnaire_title_set']  = $this->wedding_questionnaire_title->get_result_by_questionnaire($page_data['events'][$index]['wedding_questionnnaire']['wedding_questionnaire_sn']);
                    }
                    else
                    {
                        $page_data['events'][$index]['wedding_questionnnaire_title_set'] = false;
                    }
                }
            }
            if($page_data['gallery_photos'] = $this->wedding_album->get_result($this->wedding_website_sn))
            {
                foreach($page_data['gallery_photos'] as $index => $photo)
                {
                    $page_data['gallery_photos'][$index]['square_url'] = $this->io_website->get_photo_square('gallery', $photo['picture_save_dir']);
                    $page_data['gallery_photos'][$index]['thumb_url'] = $this->io_website->get_thumb_url('gallery', $photo['picture_save_dir'], $page_data['website_info']['member_sn'], 'm' );
                    $page_data['gallery_photos'][$index]['gallery_dealer_name'] = ($gallery_dealer_name=$this->io_website->gallery_dealer('name', $page_data['website_info']['wedding_website_sn']))? ('photo by '.$gallery_dealer_name):'';
                }
            }
            $page_data['media_warehouses'] = $this->media_warehouse->get_wedding_website_media_warehouse($page_data['website_info']['wedding_website_sn']);
            foreach($page_data['media_warehouses'] as $index => $w)
            {
                if($page_data['media_warehouses'][$index]['items'] = $this->media_warehouse->get_member_media_warehouse_items($page_data['website_info']['member_sn'], $w['media_warehouse']))
                {
                    if(!empty($page_data['media_warehouses'][$index]['items'][COUNT($page_data['media_warehouses'][$index]['items'])-1]['media_save_dir']))
                    {
                        //$page_data['media_warehouses'][$index]['cover'] = $page_data['media_warehouses'][$index]['items'][COUNT($page_data['media_warehouses'][$index]['items'])-1]['media_save_dir'];
                        if($this->agent->is_mobile())
                        {
                            //$page_data['media_warehouses'][$index]['cover'] = str_replace('source', 'thumb', $page_data['media_warehouses'][$index]['items'][COUNT($page_data['media_warehouses'][$index]['items'])-1]['media_save_dir']);
                            if($page_data['media_warehouses'][$index]['cover_photo_save_dir'])
                            {
                                $page_data['media_warehouses'][$index]['cover'] = $page_data['media_warehouses'][$index]['cover_photo_save_dir'];
                            }
                            else
                            {
                                $page_data['media_warehouses'][$index]['cover'] = $page_data['media_warehouses'][$index]['items'][COUNT($page_data['media_warehouses'][$index]['items'])-1]['media_save_dir'];
                            }
                            
                            /*
                            $path_new = str_replace('library/source', 'website', $page_data['media_warehouses'][$index]['cover']);
                            $tmp = explode('/', $path_new);
                            $tmp[COUNT($tmp)]=$tmp[COUNT($tmp)-1];
                            $tmp[COUNT($tmp)-2] = 'm';
                            $path_new = implode('/', $tmp);
                            $page_data['media_warehouses'][$index]['cover'] = $this->config->item('ij_frontend_cdn_path').$path_new;
                            */
                        }
                        else
                        {
                            if($page_data['media_warehouses'][$index]['cover_photo_save_dir'])
                            {
                                $page_data['media_warehouses'][$index]['cover'] = $page_data['media_warehouses'][$index]['cover_photo_save_dir'];
                            }
                            else
                            {
                                $page_data['media_warehouses'][$index]['cover'] = $page_data['media_warehouses'][$index]['items'][COUNT($page_data['media_warehouses'][$index]['items'])-1]['media_save_dir'];
                            }
                            /*
                            $path_new1 = str_replace('library/source', 'website', $page_data['media_warehouses'][$index]['cover']);
                            $tmp1 = explode('/', $path_new1);
                            $tmp1[COUNT($tmp1)]=$tmp1[COUNT($tmp1)-1];
                            $tmp1[COUNT($tmp1)-2] = 'l';
                            $path_new1 = implode('/', $tmp1);
                            $page_data['media_warehouses'][$index]['cover'] = $path_new1;
                            */
                        }
                    }
                    else
                    {
                        $page_data['media_warehouses'][$index]['cover'] = str_replace('source', 'thumb', $page_data['media_warehouses'][$index]['items'][0]['media_save_dir']);
                    }
                    $photos = array();
                    foreach ($page_data['media_warehouses'][$index]['items'] as $key => $item)
                    {
                        //$photo = array();
                        //$photo['src'] = $this->config->item('base_url').$item['media_save_dir'];
                        $path = array();
                        $path = explode('/', $item['media_save_dir']);
                        if($this->agent->is_mobile())
                        {
                            $path_new = $this->config->item('ij_frontend_cdn_path'). str_replace('library/source', 'website', implode('/', $path));
                            $tmp = explode('/', $path_new);
                            $tmp[COUNT($tmp)]=$tmp[COUNT($tmp)-1];
                            $tmp[COUNT($tmp)-2] = 'm';
                            $path_new = implode('/', $tmp);
                            $photos[] = "{src: '".$path_new."'}";
                        }
                        else
                        {
                            $path_new = $this->config->item('ij_frontend_cdn_path'). str_replace('library/source', 'website', implode('/', $path));
                            $tmp = explode('/', $path_new);
                            $tmp[COUNT($tmp)]=$tmp[COUNT($tmp)-1];
                            $tmp[COUNT($tmp)-2] = 'l';
                            $path_new = implode('/', $tmp);
                            $photos[] = "{src: '".$path_new."'}";
                        }
                        
                    }
                    $page_data['media_warehouses'][$index]['photos'] = $photos;
                }
            }

            // Save into the cache for 1 second
            $this->cache->save($website_page_data_cache_name , json_encode($page_data), 1);
        }
        else
        {
            $page_data = json_decode($str_website_page_data, true);
        }

        // parameter init

        // template use
        $demo_site = array(2,4,6,7,9,11,14,19);
        if(isset($page_data['website_info']['template_dir'])&&in_array($page_data['website_info']['wedding_website_sn'], $demo_site) === false)
        {
            $template_dir = $page_data['website_info']['template_dir'];
            if(!$template_dir)
            {
                $template_dir = 'forever';
            }
        }
        else
        {
            $template_dir = 'forever_old';
        }


        // logo decode
        $page_data['website_logo_text'] = array('I', 'U');
        if(isset($page_data['website_info']['wedding_logo_text']) && $wedding_logo_text = json_decode($page_data['website_info']['wedding_logo_text'], true))
        {
            $page_data['website_logo_text'] = $wedding_logo_text;
        }

        // couple name
        $page_data['wedding_couple_name'] = array('William', 'Kate');
        if(isset($page_data['website_info']['wedding_couple_name']) && $wedding_couple_name = json_decode($page_data['website_info']['wedding_couple_name'], true))
        {
            $page_data['wedding_couple_name'] = $wedding_couple_name;
        }

        // get message, cache todo
        $page_data['guest_messages'] = $this->wedding_guest_message_log->get_result($this->wedding_website_sn);
        $page_data['guest_messages_j'] = array();
        foreach($page_data['guest_messages'] as $m){
            $item = array();
            $item['guest_logo_save_dir'] = $m['guest_logo_save_dir'];
            $item['create_date'] = date('d M', strtotime($m['create_date']));
            $item['message'] = nl2br($m['message']);
            $item['guest_name'] = $m['guest_name'];
            $page_data['guest_messages_j'][] = $item;
        }
        $this->load->view('template/'.$template_dir.'/main', $page_data);
	}

    
	public function devicedemo()
	{
		$page_data = array();
		$this->load->view('template/yes/res_index', $page_data);
	}
	
	
	
	
	
    /**
     * Guest ================
     *** Guest function for Wedding Website
     *** 處理訪客留言相關程式
     *** guest/avatar : 大頭照上傳
     *** authFB / authGoogle : 登入
     *** message : 留言儲存
     */
    public function guest($ac='')
    {
        $wedding_website_sn = (int)$this->input->post('wedding_website_sn');
        switch($ac)
        {
            case "avatar":
                // security check, todo

                $this->load->model(['website/img_manage']);
                $this->img_manage->wedding_website_sn = $wedding_website_sn;
                $this->img_manage->default_wid = 100;
                $this->img_manage->default_hei = 100;
                $feedback = $this->img_manage->add_img();
                $this->ajax_feedback($feedback);
            break;
            case "message":
                $feedback = array('success' => 'Y', 'msg' => '');
                $this->load->model(['website/Wedding_guest_message_log']);
                // security check, todo

                $data_message = array();
                $data_message['wedding_website_sn'] = $wedding_website_sn;
                $data_message['guest_name'] = strip_tags((string)$this->input->post('guest_name'));
                $data_message['guest_logo_save_dir'] = strip_tags((string)$this->input->post('guest_logo_save_dir'));
                $data_message['message'] = strip_tags((string)$this->input->post('message'));
                // 檢查 message 長度不能超過 84 字，超過截斷
                if(mb_strlen($data_message['message'],'utf-8') > 84){
                    $data_message['message'] = mb_substr($data_message['message'],0,84,'utf8');
                }

                foreach($data_message as $v)
                {
                    if(empty($v))
                    {
                        $feedback['success'] = 'N';
                        break;
                    }
                }
                if($feedback['success'] == 'Y')
                {
                    $data_message['status'] = 1;
                    // insert message
                    $this->Wedding_guest_message_log->add_new($data_message);
                }
                $this->ajax_feedback($feedback);

            break;
        }
    }

    
    /**
     * RSVP ================
     */
    public function rsvp()
    {
        if($this->input->is_ajax_request())
        {
            $feedback = array('success' => 'N', 'msg' => '');
            $data_insert = array();
            if($data_insert['wedding_questionnaire_sn'] = (int)$this->input->post('wedding_questionnaire_sn'))
            {
                $guest_feedback = array();
                foreach($_POST as $index => $value)
                {
                    if(substr($index, 0, 9) == 'feedback_')
                    {
                        $key = substr($index, 9);
                        switch($key)
                        {
                            case "name":
                                $data_insert['guest_first_name'] = $value;
                            break;
                            case "cell":
                                $data_insert['cell'] = $value;
                            break;
                            case "email":
                                $data_insert['email'] = $value;
                            break;
                            case "participate_flag":
                                if($value == 0)
                                {
                                    // 標示出席，取得人數
                                    $data_insert['participate_flag'] = (int)$this->input->post('feedback_participate_flag_extend');
                                }
                                else
                                {
                                    $data_insert['participate_flag'] = 0;
                                }
                            break;
                            default:
                                $guest_feedback[$key] = $value;
                            break;
                        }
                    }
                }
                $data_insert['guest_feedback'] = json_encode($guest_feedback);
                $data_insert['status'] = 1;
                $data_insert['create_date'] = date('Y-m-d H:i:s');
                //echo($this->db->insert_string('ij_wedding_guest_feedback_log', $data_insert));exit();
                if($this->db->insert('ij_wedding_guest_feedback_log', $data_insert))
                {
                    $feedback['success'] = 'Y';
                }
                else
                {
                    $feedback['msg'] = '無法儲存，請稍候再試';
                }
            }
            $this->ajax_feedback($feedback);
        }
        /*
        * feedback_1:1
        * feedback_answer_sn_2_extend_0:同學
        * feedback_answer_sn_2_extend_1:同學
        * feedback_answer_sn_3_extend_0:1
        * feedback_answer_sn_4_extend_0:1
        * feedback_6:1
        * feedback_7:1
        * wedding_event_sn:1
        */

    }

    public function validate()
    {
        $feedback = array('success' => false, 'msg' => '');
        $wedding_website_sn = $this->input->post('wedding_website_sn');
        $password = $this->input->post('password');

        if(!$website_info = $this->wedding_website->wedding_website_row($wedding_website_sn))
        {
            $feedback['msg'] = '結婚網站資料無法存取';
        }
        else
        {
            if($website_info['password']===$password)
            {
                // setsession
                $this->session->set_userdata('password_pass', $website_info['wedding_website_sn']);
                $feedback['success'] = true;
            }
            else
            {
                $feedback['msg'] = '密碼不正確喔';
            }
        }
        $this->ajax_feedback($feedback);
    }

    /*
    * ajax 吐回結果使用
    */
    private function ajax_feedback($result){
        header('Content-Type: text/plain; charset=utf-8');
        echo json_encode($result);
    }
}
