<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/typeahead/typeahead.css">
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/pages/css/portfolio.css" rel="stylesheet" type="text/css"/>



<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>FAQ內容 <small>文章編輯</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">FAQ編輯</span>
            </div>
            <div class="tools">
              <a href="admin-ijwedding/Content/contentFaq"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" method="post" role="form" action="admin-ijwedding/Content/contentFaqItem/add/1">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-md-2 control-label">是否發佈</label>
                  <div class="col-md-10">
                    <div class="radio-list">
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status1" value="1" checked>  發佈  
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status2" value="0" >  不發佈
                        </label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-md-2 control-label">Question</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control" name="faq_titile" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-md-2 control-label">Answer</label>
                  <div class="col-md-10">
                    <textarea name="faq_description" class="form-control" rows="6"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">分類: <span class="required"> * </span></label>
                  <div class="col-md-8">
                    <div class="checkbox-list">
                    	<?php 
												foreach($_result02 as $key => $value)
													{														
														print '<label class="checkbox-inline"><input type="checkbox" name="associated_content_typel_sn_set[]" value="'.$value->content_typel_sn.'">'.$value->content_title.'</label>';														
													}                    	
                    	?>                                            
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-md-2 control-label">排序</label>
                  <div class="col-md-10">
                    <input type="text" name="sort_order" class="form-control" value="<?php echo ($_sort+1);?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-md-2 control-label">建立者</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control"  placeholder="<?php echo $this->session->userdata['member_data']['user_name'];?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-md-2 control-label">建立時間</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control"  placeholder="<?php echo date("Y-m-d H:i:s");?>" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                    <a href="admin-ijwedding/Content/contentFaq"><button type="button" class="btn default">Cancel</button></a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->

<!-- Modal -->
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">上傳圖片</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-without-legend" role="form">
          <fieldset>
            <div class="form-group">
              <label for="email" class="col-lg-4 control-label">圖片標題 <span class="require">*</span></label>
              <div class="col-md-10">
                <input type="text" class="form-control" id="email">
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-4 control-label">顏色</label>
              <div class="col-md-10">
                <select class="form-control">
                  <option>粉紅色</option>
                  <option>黑色</option>
                  <option>金色</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-4 control-label">選擇檔案</label>
              <div class="col-md-10">
                <input class="form-control" name="photo" type="file" id="photo" onchange="">
              </div>
            </div>
          </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">上傳</button>
      </div>
    </div>
  </div>
</div>
