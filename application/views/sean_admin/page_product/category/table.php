<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<link rel="stylesheet" href="public/metronic/global/plugins/jquery-treegrid/css/jquery.treegrid.css">

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品分類 <small>分類列表</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="<?php echo $init_control;?>">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品分類
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         分類管理
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">分類列表</span>
            </div>
            <div class="actions btn-set">
              <a href="<?php echo $_action_link;?>" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <div class="portlet-body">
            <table class="tree-category table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th>名稱</th>
                  <th>排序</th>
                  <th>是否啟用</th>
                  <th>功能</th>
                </tr>
              </thead>
               <?php
               
              	 if(is_array($_result01))
                	{                                  
                 			foreach($_result01 as $key => $value)
                    		{                    			                    			                    			
                    			if($value->category_status == 1)
                    				{
                    					$_active = '<i class="fa fa-check"></i>';
                    				}
                    			else
                    				{
                    					$_active = '<i class="fa fa-times"></i>';
                    				}
               
               ?>
                
                  <tr class="treegrid-<?php echo $value->category_sn;if($value->upper_category_sn > 0 ) echo " treegrid-parent-".$value->upper_category_sn;?>">
                    <td><?php echo $value->category_name;?></td>
                    <td><?php echo $value->sort_order;?></td>
                    <td class="center"><?php echo $_active;?></td>
                    <td class="center">
                      <a href="<?php echo $_edit_link.$value->$_table_key;?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                      <a href="<?php echo $_delete_link.$value->$_table_key;?>" onclick="return deletechecked();"><i class="fa fa-trash-o"></i>刪除</a>&nbsp;&nbsp;
                      
                    
                    
                    </td>
                  </tr>
                  
                  
               <?php
               
			             }
			           }
           			?>
           			
              <!--
              <tr class="treegrid-1">
                <td>結婚商店</td>
                <td>1</td>
                <td><i class="fa fa-check"></i></td>
                <td>
                  <a href="preview/productCategoryItem"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                  <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                </td>
              </tr>
              
              <tr class="treegrid-2 treegrid-parent-1">
                <td>創意卡片</td>
                <td>1</td>
                <td><i class="fa fa-check"></i></td>
                <td>
                  <a href="preview/productCategoryItem"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                  <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                </td>
              </tr>
              <tr class="treegrid-3 treegrid-parent-1">
                <td>喜帖 邀請卡</td>
                <td>1</td>
                <td><i class="fa fa-check"></i></td>
                <td>
                  <a href="preview/productCategoryItem"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                  <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                </td>
              </tr>
              <tr class="treegrid-4 treegrid-parent-3">
                <td>中式精選喜帖</td>
                <td>1</td>
                <td><i class="fa fa-check"></i></td>
                <td>
                  <a href="preview/productCategoryItem"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                  <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                </td>
              </tr>
              
              <tr class="treegrid-6">
                <td>雲端婚訊</td>
                <td>1</td>
                <td><i class="fa fa-check"></i></td>
                <td>
                  <a href="preview/productCategoryItem"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                  <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                </td>
              </tr>
              
              <tr class="treegrid-7">
                <td>現場報到系統</td>
                <td>1</td>
                <td><i class="fa fa-check"></i></td>
                <td>
                  <a href="preview/productCategoryItem"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                  <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                </td>
              </tr>
              -->
            </table>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->