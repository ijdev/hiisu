<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wedding_website extends CI_Model {

    public $title;

    public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
    }
    /*
    * Get Wedding Website Basic Info
    * 限制的部分 toto
    */
    public function wedding_website_row( $wedding_website_sn = 0 )
    {
    	$this->db->select('*');
    	$this->db->from('ij_wedding_website');
    	$this->db->where('wedding_website_sn', $wedding_website_sn);
        $this->db->where('wedding_website_status', 1);
    	/*
    	$this->db->where('website_publish_flag', 1);
    	$this->db->where('website_start_date >= ', time());
    	$this->db->where('website_end_date <= ', time());
    	*/
    	$query = $this->db->get();
        return $row = $query->row_array();
    }
    public function wedding_website_row_by_name( $name = '' )
    {
        $this->db->select('*');
        $this->db->from('ij_wedding_website');
        $this->db->where('customize_website_url', $name);
        /*
        $this->db->where('website_publish_flag', 1);
        $this->db->where('website_start_date >= ', time());
        $this->db->where('website_end_date <= ', time());
        */
        $query = $this->db->get();
        return $row = $query->row_array();
    }
    public function wedding_website_divider( $wedding_website_sn = 0 )
    {
        $this->db->select('*');
        $this->db->from('ij_wedding_divider_info');
        $this->db->where('wedding_website_sn', $wedding_website_sn);
        $this->db->where('status', 1);
        $query = $this->db->get();
        if($result = $query->result_array())
        {
            $modules = array();
            foreach($result as $row)
            {
                $modules[$row['associated_area']] = $row;
            }
            return $modules;
        }
        return array();
    }

    public function getTemplateList()
    {
        // check cache
        $this->load->driver('cache', array('adapter' => 'file'));

        if ( ! $product_template_list = $this->cache->get('product_template_list'))
        {
            if(ENVIRONMENT=='production')
            {
                $api = 'http://www.ijwedding.com/shop/Get_Template';
            }
            else
            {
                $api = 'http://test-www.ijwedding.com/shop/Get_Template';
            }
            $product_template_list = $this->io_website->curl_post($api);

            $this->cache->save('product_template_list', $product_template_list, 600); // 每10分鐘 renew 一次
        }
        return json_decode($product_template_list, true);

    }
}