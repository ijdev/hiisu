<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品規格 <small>規格單位設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/admin">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品規格
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
        規格單位
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">規格單位</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/product/productModuleItem" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>規格單位名稱</th>
                  <th>代號</th>
                  <th>適用分類</th>
                  <th>備註</th>
                  <th>排序</th>
                  <th>啟用</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
<?php	if(@$Items){foreach(@$Items as $Item){?>
                  <tr class="odd gradeX">
                    <td><?php echo $Item['spec_option_name'];?></td>
                    <td><?php echo $Item['spec_option_config_sn'];?></td>
                    <td><?php echo @$Item['category_names'];?></td>
                    <td><?php echo $Item['description'];?></td>
                    <td><?php echo $Item['sort_order'];?></td>
                    <td class="center">
                      <?php if($Item['status']):?>
                        <i class="fa fa-check"></i>
                      <?php else:?>
                        <i class="fa fa-times"></i>
                      <?php endif;?>
                    </td>
                    <td class="center">
                      <a href="admin/product/productModuleItem/<?php echo $Item['spec_option_config_sn'];?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
    <?if(stripos($this->page_data['permission'],'del')!==false){?>
                      <a href="javascript:void(0);" class="deleteItem" ItemId="<?php echo $Item['spec_option_config_sn'];?>"><i class="fa fa-trash-o"></i>刪除</a>
                      <?}?>
                    </td>
                  </tr>
<?php }}?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->