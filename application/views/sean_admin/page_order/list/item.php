<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/typeahead/typeahead.css">
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/pages/css/portfolio.css" rel="stylesheet" type="text/css"/>



<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>訂單列表 <small>訂單編輯</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="icon-basket font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">
              訂單 #12313232 </span>
              <span class="caption-helper">2015-05-22 07:16:25</span>
            </div>
            <div class="actions btn-set">
              <a href="javascript:;" class="btn green"><i class="fa fa-file-pdf-o"></i> 列印</a>
              <a href="preview/orderTable" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body">
           <div class="tabbable">
            <ul class="nav nav-tabs nav-tabs-lg">
              <li class="active">
                <a href="#tab_detail" data-toggle="tab"> Details </a>
              </li>
              <li>
                <a href="#tab_history" data-toggle="tab"> History </a>
              </li>
              <li>
                <a href="#tab_message" data-toggle="tab"> 訊息紀錄 </a>
              </li>
            </ul>            
            <div class="tab-content">
              <div class="tab-pane active" id="tab_detail">
                <div class="row">
                  <div class="col-md-6 col-sm-12">
                    <div class="portlet yellow-crusta box">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs"></i>訂單資訊
                        </div>
                        <div class="actions">
                          <a href="javascript:;" class="btn btn-default btn-sm" data-toggle="modal" data-target="#statusModal">
                            <i class="fa fa-pencil"></i> 編輯 
                          </a>
                        </div>
                      </div>
                      <div class="portlet-body">
                        <div class="row static-info">
                          <div class="col-md-3 name">訂單編號 #:</div>
                          <div class="col-md-9 value">12313232</span></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">成立時間:</div>
                          <div class="col-md-9 value">2015-05-21 7:16:25 AM</div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">訂單狀態:</div>
                          <div class="col-md-9 value"><span class="label label-info">處理中</span></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">配送狀態:</div>
                          <div class="col-md-9 value"><span class="label label-warning">未發貨</span></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">訂單客服:</div>
                          <div class="col-md-9 value">Administrator</div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">差額設定:</div>
                          <div class="col-md-9 value">100 (正負都可以輸入)</div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">應收總價:</div>
                          <div class="col-md-9 value">$1,600</div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">發票格式:</div>
                          <div class="col-md-9 value">二聯式</div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">發票抬頭:</div>
                          <div class="col-md-9 value">無</div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">統一編號:</div>
                          <div class="col-md-9 value">無</div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">付款方式:</div>
                          <div class="col-md-9 value">信用卡</div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">付款狀態:</div>
                          <div class="col-md-9 value"><span class="label label-success">已付款</span></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-3 name">備註:</div>
                          <div class="col-md-9 value">備註內容備註內容備註內容備註內容</div>
                        </div>
                      </div>
                      <!-- End of portlet-body -->
                    </div>
                    <!-- End of portlet -->
                  </div>
                  <div class="col-md-6 col-sm-12">
                    <div class="portlet blue-hoki box">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs"></i>會員資料
                        </div>
                      </div>
                      <div class="portlet-body">
                        <div class="row static-info">
                          <div class="col-md-5 name">
                             Customer Name:
                          </div>
                          <div class="col-md-7 value">
                             <a href="javascript:;" onclick="alert('前往個人檔案');">DSDS&nbsp;<i class="fa fa-external-link"></i></a>
                          </div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-5 name">
                             Email:
                          </div>
                          <div class="col-md-7 value">
                             jhon@doe.com
                          </div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-5 name">
                             住址 :
                          </div>
                          <div class="col-md-7 value">
                             台北市中山區民權東路一段45 號 9 樓之1
                          </div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-5 name">
                             電話:
                          </div>
                          <div class="col-md-7 value">
                             12234389
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="portlet green-meadow box">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs"></i>收件人資料
                        </div>
                        <div class="actions">
                          <a href="javascript:;" class="btn btn-default btn-sm" data-toggle="modal" data-target="#userModal">
                          <i class="fa fa-pencil"></i> Edit </a>
                        </div>
                      </div>
                      <div class="portlet-body">
                        <div class="row static-info">
                          <div class="col-md-5 name">收件人:</div>
                          <div class="col-md-7 value">吳曉瑋</div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-5 name">住址:</div>
                          <div class="col-md-7 value">台北市中山區民權東路一段45 號 9 樓之1</div>
                        </div>
                        <div class="row static-info">
                          <div class="col-md-5 name">聯絡電話:</div>
                          <div class="col-md-7 value">123123232</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- portlet 購物清單 - 實體商品 -->
                <div class="row">
                  <div class="col-md-12 col-sm-12">
                    <div class="portlet red-sunglo box">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs"></i>購物清單 - 結婚商店(訂單內容結婚商店, 現場報到系統, 雲端婚訊只會出現一種)
                        </div>
                        <div class="actions">
                          <!--
                          <a href="javascript:;" class="btn btn-default btn-sm" data-toggle="modal" data-target="#pproductModal">
                            <i class="fa fa-plus"></i> 新增 
                          </a>
                          -->
                        </div>
                      </div>
                      <div class="portlet-body">
                        <div class="table-responsive">
                          <table class="table table-hover table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>商品名稱</th>
                                <th>商品分類</th>
                                <th>售價</th>
                                <th>數量</th>
                                <th>加購商品</th>
                                <th>折扣</th>
                                <th>小計</th>
                                <th>功能</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php for($i=0;$i<6;$i++):?>
                                <tr>
                                  <td><a href="javascript:;" onclick="alert(前往商品頁面);">開心肥皂泡</a></td>
                                  <td>婚禮小物</td>
                                  <td>45</td>
                                  <td>20</td>
                                  <td><?php echo $i==5? '<i class="fa fa-check-square-o"></i>':'';?></td>
                                  <td><?php echo $i==3? '100':'0';?></td>
                                  <td>800</td>
                                  <td>
                                    <a href="javascript:;" data-toggle="modal" data-target="#pproductModal">
                                      <i class="fa fa-pencil-square"></i>編輯
                                    </a>&nbsp;&nbsp;
                                    <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                                  </td>
                                </tr>
                              <?php endfor;?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <!-- End of portlet-body -->
                    </div>
                    <!-- End of portlet -->
                  </div>
                </div>
                <!-- portlet 購物清單 - 虛擬商品 -->
                <!-- portlet 購物清單 - 虛擬商品 -->
                <div class="row">
                  <div class="col-md-12 col-sm-12">
                    <div class="portlet grey-cascade box">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs"></i>購物清單 - 雲端婚訊
                        </div>
                        <div class="actions">
                          <!--
                          <a href="javascript:;" class="btn btn-default btn-sm" data-toggle="modal" data-target="#vproductModal">
                            <i class="fa fa-plus"></i> 新增 
                          </a>
                          -->
                        </div>
                      </div>
                      <div class="portlet-body">
                        <div class="table-responsive">
                          <table class="table table-hover table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>商品名稱</th>
                                <th>商品分類</th>
                                <th>款型</th>
                                <th>顏色</th>
                                <th>售價</th>
                                <th>數量</th>
                                <th>加購商品</th>
                                <th>折扣</th>
                                <th>小計</th>
                                <th>功能</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php for($i=0;$i<6;$i++):?>
                                <tr>
                                  <td><a href="javascript:;" onclick="alert(前往商品頁面);">旗艦型 Yes 版 </a></td>
                                  <td>線上喜帖</td>
                                  <td>旗艦款</td>
                                  <td>黑色</td>
                                  <td>345</td>
                                  <td><?php echo $i==3? '1':'2';?></td>
                                  <td><?php echo $i==5? '<i class="fa fa-check-square-o"></i>':'';?></td>
                                  <td><?php echo $i==3? '100':'0';?></td>
                                  <td>890</td>
                                  <td>
                                    <a href="javascript:;" data-toggle="modal" data-target="#vproductModal">
                                      <i class="fa fa-pencil-square"></i>編輯
                                    </a>&nbsp;&nbsp;
                                    <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                                  </td>
                                </tr>
                              <?php endfor;?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <!-- End of portlet-body -->
                    </div>
                    <!-- End of portlet -->
                  </div>
                </div>
                <!-- portlet 購物清單 - 虛擬商品 -->
                <div class="row">
                  <div class="col-md-6">
                  </div>
                  <div class="col-md-6">
                    <div class="well">
                      <div class="row static-info align-reverse">
                        <div class="col-md-8 name">總價:</div>
                        <div class="col-md-3 value">$1,800</div>
                      </div>
                      <div class="row static-info align-reverse">
                        <div class="col-md-8 name">運費:</div>
                        <div class="col-md-3 value">$70</div>
                      </div>
                      <div class="row static-info align-reverse">
                        <div class="col-md-8 name">折扣:</div>
                        <div class="col-md-3 value">$200 (單品折扣 - 差額設定)</div>
                      </div>
                      <div class="row static-info align-reverse">
                        <div class="col-md-8 name">小計:</div>
                        <div class="col-md-3 value">$1,600</div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              <!-- End of tab_detail -->
              <div class="tab-pane" id="tab_history">
                <div class="table-responsive">
                  <table class="table table-hover table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>更新時間</th>
                        <th>更新者</th>
                        <th>紀錄</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>2015-05-22 12:00</td>
                        <td>Administrator</td>
                        <td>訂單狀態(已結束)</td>
                      </tr>
                      <tr>
                        <td>2015-05-22 08:00</td>
                        <td>Administrator</td>
                        <td>訂單狀態(處理中)</td>
                      </tr>
                      <tr>
                        <td>2015-05-22 08:00</td>
                        <td>System (系統自動)</td>
                        <td>訂單狀態(未處理)</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- End of tab_history -->
              <div class="tab-pane" id="tab_message">
                <div class="portlet box">
                  <div class="portlet-title">
                    <div class="row">
                      <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9">
                        <textarea class="form-control" rows="2"></textarea>
                        <input type="file" id="exampleInputFile1">
                      </div>
                      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3">
                        <button type="button" class="btn blue">送出</button>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="portlet-body" id="chats">
                    <ul class="chats">
                      <li class="in">
                        <img class="avatar" alt="" src="public/img/icon-supportfemale-48.png">
                        <div class="message">
                          <span class="arrow">
                          </span>
                          <a href="javascript:;" class="name">
                          客服人員 </a>
                          <span class="datetime">
                          at May 04 2015 20:09 </span>
                          <span class="body">
                          Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
                        </div>
                      </li>
                      <li class="out">
                        <img class="avatar" alt="" src="public/img/icon-diamond-48.png">
                        <div class="message">
                          <span class="arrow">
                          </span>
                          <a href="javascript:;" class="name"> Luciel Lo </a>
                          <span class="datetime">
                          at May 04 2015 20:00 </span>
                          <span class="body">
                          Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
                        </div>
                      </li>
                      <li class="in">
                        <img class="avatar" alt="" src="public/img/icon-supportfemale-48.png">
                        <div class="message">
                          <span class="arrow">
                          </span>
                          <a href="javascript:;" class="name">
                          客服人員 </a>
                          <span class="datetime">
                          at May 04 2015 19:50 </span>
                          <span class="body">
                          Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
                        </div>
                      </li>
                      <li class="out">
                        <img class="avatar" alt="" src="public/img/icon-diamond-48.png">
                        <div class="message">
                          <span class="arrow">
                          </span>
                          <a href="javascript:;" class="name"> Luciel Lo </a>
                          <span class="datetime">
                          at May 04 2015 19:30 </span>
                          <span class="body">
                          Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <!-- End of tab_message -->
            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->

<!-- Modal for 訂單狀態變更 -->
<div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">訂單狀態編輯</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-without-legend" role="form">
          <fieldset>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">訂單客服</label>
              <div class="col-md-9">
                <select class="form-control">
                  <option>Administrator</option>
                  <option>Lucile</option>
                  <option>Jenny</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">付款狀態</label>
              <div class="col-md-9">
                <select class="form-control">
                  <option value="0">未付款</option>
                  <option value="1">已付款</option>
                  <option value="2">付款失敗</option>
                  <option value="3">等待付款</option>
                  <option value="4">退款中</option>
                  <option value="5">完成退款</option>
                  <option value="6">部份付款</option>
                  <option value="7">部份退款</option>
                  <option value="8">部份退款中</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">訂單狀態</label>
              <div class="col-md-9">
                <select class="form-control">
                  <option value="0">未確認</option>
                  <option value="1">已確認</option>
                  <option value="2">申請取消</option>
                  <option value="3">取消訂單</option>
                  <option value="4">完成交易</option>
                  <option value="5">訂單失效</option>
                  <option value="6">異常</option>
                  <option value="7">結案</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">配送狀態</label>
              <div class="col-md-9">
                <select class="form-control">
                  <option value="0">未發貨</option>
                  <option value="1">已出貨</option>
                  <option value="2">已到貨</option>
                  <option value="3">申請換貨</option>
                  <option value="4">換貨中</option>
                  <option value="5">申請退貨</option>
                  <option value="6">退貨中</option>
                  <option value="7">商品取回</option>
                  <option value="8">新品寄出</option>
                  <option value="9">部份出貨</option>
                  <option value="10">部份換貨</option>
                  <option value="11">部份退貨</option>
                  <option value="12">備貨中</option>
                  <option value="13">退貨異常</option>
                  <option value="14">換貨異常</option>
                  <option value="15">配送異常</option>
                  <option value="16">貨已到店</option>
                  <option value="17">逾期未取店退</option>
                  <option value="18">已取貨</option>
                  <option value="19">完成換貨</option>
                  <option value="20">完成退貨</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">差額設定</label>
              <div class="col-md-9">
                <input type="text" class="form-control" value="100">
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">備註</label>
              <div class="col-md-9">
                <textarea class="form-control" rows="3"></textarea>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">變更</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal for 收件人資料變更 -->
<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="userModalLabel">收件人資料編輯</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-without-legend" role="form">
          <fieldset>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">姓名</label>
              <div class="col-md-9">
                <input type="text" class="form-control" value="">
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">手機號碼</label>
              <div class="col-md-9">
                <input type="text" class="form-control" value="">
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">配送區域</label>
              <div class="col-md-9">
                <select class="form-control input-sm" id="region-state">
                  <option value="" disable> 台灣 </option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">郵遞區號</label>
              <div class="col-md-9">
                <input type="text" class="form-control" value="">
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">縣市</label>
              <div class="col-md-9">
                <input type="text" class="form-control" value="">
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">地址</label>
              <div class="col-md-9">
                <input type="text" class="form-control" value="">
              </div>
            </div>
          </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">變更</button>
      </div>
    </div>
  </div>
</div>



<!-- Modal for 虛擬商品資訊變更 -->
<div class="modal fade" id="vproductModal" tabindex="-1" role="dialog" aria-labelledby="vproductModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="vproductModalLabel">虛擬商品資訊編輯</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-without-legend" role="form">
          <fieldset>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">商品名稱</label>
              <div class="col-md-9">
                <span class="form-control-static">旗艦型 Yes 版</span>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">數量</label>
              <div class="col-md-9">
                <input type="text" class="form-control" value="2">
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">折扣</label>
              <div class="col-md-9">
                <input type="text" class="form-control" value="100">
              </div>
            </div>
          </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">變更</button>
      </div>
    </div>
  </div>
</div>



<!-- Modal for 實體商品資訊變更 -->
<div class="modal fade" id="pproductModal" tabindex="-1" role="dialog" aria-labelledby="pproductModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="pproductModalLabel">實體商品資訊編輯</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-without-legend" role="form">
          <fieldset>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">商品名稱</label>
              <div class="col-md-9">
                <span class="form-control-static">開心肥皂泡</span>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">數量</label>
              <div class="col-md-9">
                <input type="text" class="form-control" value="1">
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-3 control-label">折扣</label>
              <div class="col-md-9">
                <input type="text" class="form-control" value="100">
              </div>
            </div>
          </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">變更</button>
      </div>
    </div>
  </div>
</div>
