<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>經銷商分級 <small>分級設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="preview">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        會員管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="preview/dealerTable">經銷商</a>
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         <a href="preview/dealerLevel">經銷商分級管理</a>
         <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         經銷商分級編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">經銷商分級設定</span>
            </div>
            <div class="tools">
              <a href="preview/dealerLevel"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">名稱 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label for="seq" class="col-lg-3 control-label">排序 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" value="0">
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-3 control-label">說明</label>
                  <div class="col-lg-8">
                    <textarea class="form-control" rows="3"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-3 control-label">經銷商專頁功能</label>
                  <div class="col-lg-8">
                    <div class="checkbox-list">
                      <label class="checkbox-inline"><input type="checkbox" name="" value="">專頁大 Banner</label>
                      <label class="checkbox-inline"><input type="checkbox" name="" value="">登入頁背景圖</label>
                      <label class="checkbox-inline"><input type="checkbox" name="" value="">客製化子域名</label>
                      <label class="checkbox-inline"><input type="checkbox" name="" value="">廣告隱藏</label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-3 control-label">經銷商後台功能</label>
                  <div class="col-lg-8">
                    <div class="checkbox-list">
                      <label class="checkbox-inline"><input type="checkbox" name="" value="">成交訂單</label>
                      <label class="checkbox-inline"><input type="checkbox" name="" value="">廣告管理</label>
                      <label class="checkbox-inline"><input type="checkbox" name="" value="">會員列表</label>
                      <label class="checkbox-inline"><input type="checkbox" name="" value="">作品集編輯</label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">是否啟用 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <select class="form-control">
                      <option>是</option>
                      <option>否</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-lg-3 control-label">更新者</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="administrator" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-3 control-label">更新時間</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="2015-05-25 10:00" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="button" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->