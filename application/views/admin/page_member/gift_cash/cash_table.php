<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>會員管理 <small>囍市錢包管理</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/admin">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        會員管理
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
        囍市錢包管理
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">囍市錢包管理</span>
          <?php $this->load->view('www/general/flash_error');?>
              <!--div style="font-size:15px;display:inline;margin-left: 100px;">
                <span class="label label-success">有效</span>
                總發放金額：<span class="number-redb"><?=number_format(@$cash_sum[0]['cash_amount'],2)?></span> &nbsp;
                總使用金額：<span class="number-redb"><?=number_format(@$cash_sum[0]['used_amount'],2)?></span> &nbsp;
                總金額：<span class="number-redb"><?=number_format(@$cash_sum[0]['total'],2)?></span> &nbsp;
              </div-->
            </div>
            <div class="actions btn-set">
              <!--a data-toggle="modal" href="#searchPro" class="btn green"><i class="fa fa-plus"></i> 新增</a-->
              <a href="admin/member/gift_cash/add" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
      <form class="form-horizontal" role="form" method="get">
          <div class="portlet-body">
            <div class="note note-warning">
              <div class="table-actions-wrapper">
                  <div class="col-md-4">
                    <div class="input-group input-large date-picker input-daterange" data-date="2015-05-07" data-date-format="yyyy-mm-dd">
                      <span class="input-group-addon"> 日期 </span>
                      <input type="text" class="form-control input-sm date-picker" name="from" value="<?=@$from?>">
                      <span class="input-group-addon"> 到 </span>
                      <input type="text" class="form-control input-sm date-picker" name="to" value="<?=@$to?>">
                    </div>
                  </div>
                <div class="col-md-8">
                   <input type="text" name="search" value="<?php echo @$search;?>" class="pagination-panel-input form-control input-inline input-sm" style="margin: 0 15px;" placeholder="姓名、內容">
              <select name="cash_type" required class="table-group-action-input form-control input-inline input-sm">
                <option value="">類別</option>
                 <?php foreach($cash_types as $_key=>$_value){
                      echo '<option value="'.$_key.'"';
                      if(@$cash_type==$_key)
                      echo ' selected="selected"';
                      echo '>'.$_value.'</option>';
                   }?>
              </select>
              <select name="status" class="table-group-action-input form-control input-inline input-sm" style="font-size:11px;">
                <option value="">狀態</option>
              <?php foreach($status_ct as $_Item){ ?>
                <option <?php echo (@$status==$_Item['status'])?'selected':'';?> value="<?php echo $_Item['status']?>"><?php echo $_Item['status_name']?></option>
              <?php }?>
              </select>
                  <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button> &nbsp;&nbsp;&nbsp;
                  <button id="reset_form" onclick="this.form.flg.value='reset';" class="btn btn-sm purple"><i class="fa"></i> 清除</button>
                  <input type="hidden" name="flg" value="">
                </div>
              </div>
              <div style="clear:both;"></div>
            </div>
</form>
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>流水號</th>
                  <th>類別</th>
                  <th>內容備註</th>
                  <th>金額</th>
                  <!--th>使用(扣除)金額</th-->
                  <th>所屬會員(編號)</th>
                  <th>狀態</th>
                  <th>異動時間</th>
                  <!--th>異動人員</th-->
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
<?php	if(@$Items){foreach(@$Items as $Item){?>
                  <tr class="odd gradeX">
                    <td><?php echo $Item['gift_cash_sn'];?></td>
                    <td><?php echo @$cash_types[$Item['cash_type']];?></td>
                    <td><?php echo $Item['memo'];?></td>
                    <td align="right"><?php echo number_format($Item['cash_amount']);?>&nbsp;</td>
                    <!--td align="right"><?php echo number_format($Item['used_amount'],2);?>&nbsp;</td-->
                    <td><a title="只看該會員囍市錢包流水帳資料" href="admin/member/gift_cash?search=<?php echo $Item['partner_name'];?>"><?php echo $Item['partner_name'];?>（<?php echo $Item['associated_member_sn'];?>）</a></td>
                    <td class="center">
                      <span class="label label-<?=($Item['status']==1)?'success':'warning'?>"><?=$Item['status_name']?></span>
                    </td>
                    <td><small><?php echo ($Item['last_time_update']!=0)?$Item['last_time_update']:$Item['create_date'];?></small></td>
                    <!--td><?php echo @$Item['last_name'];?></td-->
                    <td class="center">
                      <a href="admin/member/gift_cash/item/<?php echo $Item['gift_cash_sn'];?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                      <a href="javascript:void(0);" class="deleteItem" ItemId="<?php echo $Item['gift_cash_sn'];?>"><i class="fa fa-trash-o"></i>刪除</a>
                    </td>
                  </tr>
<?php }}?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
<div class="modal fade" id="searchPro" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form class="form-horizontal" role="form" action="admin/product/inventory/item" method="get">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">輸入商品編號開始調整庫存</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="name" class="col-md-2 control-label">商品編號</label>
            <div class="col-md-10">
              <input type="text" class="form-control" id="product_sn" name="product_sn" value="">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn default" data-dismiss="modal">Close</button>
          <input type="submit" class="btn blue" value="確定送出">
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->