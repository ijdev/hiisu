<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script>

function ChgChannelName(C,D){ //變動館別、帶入值
	$('#Categorys option').remove();$('#Categorys').append('<option>Loading...</option>');
	$.get('/admin/product/ChgChannelName2/'+C+'/'+D,function(data){$('#Categorys option').remove();$('#Categorys').append(data);getSpec();});
}
/*function ChgSpec(C){ //變動分類
	$('#Categorys option').remove();$('#Categorys').append('<option>Loading...</option>');
	$.get('/admin/product/ChgSpec/'+C,function(data){$('#Categorys option').remove();$('#Categorys').append(data);});;
}*/
$('#Categorys').change(function(event) {
	//event.preventDefault();
	var C=$('#Categorys').val();
	var D=$('#product_package_config_sn').val();
  var SpecList = $('#SpecList');
	if(C!=0 && C !='' && D!=0 && D !=''){
  $.post('admin/product/AddSpec', {
          category_sn: C,
          product_package_config_sn: D
      }, function(data) {
				showSpec(data);
      }, 'json');
  }
});
function getSpec(){
	var C=$('#Categorys').val();
	//console.log(C);
  $.post('admin/product/AddSpec', {
          category_sn: C,
          product_package_config_sn: <?php echo @$Item['product_package_config_sn']; ?>
      }, function(data) {
				showSpec(data);
      }, 'json');
}
jQuery(document).ready(function() {       
  Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
<?php if(@$Item['channel_name']){?>
	ChgChannelName('<?php echo @$Item['channel_name'];?>','<?php echo @$Item['product_package_config_sn'];?>');
<?php }?>
});
function showSpec(data){
  	var SpecList = $('#SpecList');
    var html = '';
    for (var x = 0; x < data.length; x++) {
    	var spec_option_limitation=(data[x].spec_option_limitation)?data[x].spec_option_limitation:'';
    	var product_package_spec_option_relation_sn=data[x].product_package_spec_option_relation_sn;
    	var ifchecked=(data[x].status=='1')?'checked':'';
        html += '<label>';
        html += '<input type="hidden" name="spec['+x+'][product_package_spec_option_relation_sn]" value="'+ product_package_spec_option_relation_sn +'" >';
        html += '<input class="checkbox-list" type="checkbox" name="spec['+x+'][status]" value="1" '+ifchecked+' >'+data[x].spec_option_name+' : '+data[x].usage_limitation;
        html += '<input type="text" size="10" name="spec['+x+'][spec_option_limitation]" value="'+ spec_option_limitation +'" style="vertical-align: middle;height:18px;font-size: 12px;" >';
        html += '<span style="color:#999;">(-1 代表無限制)</span>';
        html += '</label>';
    }
    if(!html){ 
    	html='<span class="help-block"> (請先選取館別與分類) </span>';
    }else{
        html += '<span class="help-block"> (若需新增規格請選擇規格所屬館別與分類) </span>';
    }
    SpecList.html(html);
}
</script>