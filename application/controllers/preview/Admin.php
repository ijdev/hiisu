<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
* for admin console template preview
*/

class Admin extends CI_Controller {

	/**
	 * 總控首頁 Template
	 *
	 */
	public function index()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_home/page_content'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_home/page_content_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 總控登入頁面 Template
	 *
	 */
	public function managerSignIn()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$this->load->view('preview/admin/page_manager/sign_in', $page_data);
	}

	/**
	 * 會員列表 Template
	 *
	 */
	public function memberTable()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_member/page_table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_member/page_table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 會員編輯 Template
	 *
	 */
	public function memberItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_member/page_item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_member/page_item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 會員列表 Template
	 *
	 */
	public function ticketTable()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_callcenter/ticket_content'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_callcenter/ticket_content_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 *  客服中心回應內容
	 *	直接顯示在 model 中
	 *
	 */
	public function callcenterMsg($done = false)
	{
		$page_data = array();

		// 內容設定
		if($done)
		{
			// 若為已解決， modal footer 不顯示
			$page_data['done'] = true;
		}
		else
		{
			$page_data['done'] = false;
		}

		$this->load->view('preview/admin/page_callcenter/modal_ticket_msg', $page_data);
	}

	/**
	 * 客服問題分類管理 Template
	 *
	 */
	public function ticketCategory()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_callcenter/ticket_category_table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_callcenter/ticket_category_table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 客服問題狀態管理 Template
	 *
	 */
	public function ticketStatus()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_callcenter/ticket_status_table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_callcenter/ticket_status_table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 商品顏色 Template
	 *
	 */
	public function productColor()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_product/color/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/color/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 商品顏色 Template
	 *
	 */
	public function productColorItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_product/color/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/color/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 商品模組 Template
	 *
	 */
	public function productModule()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_product/module/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/module/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 商品模組 Template
	 *
	 */
	public function productModuleItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_product/module/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/module/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 模組打包 Template
	 *
	 */
	public function productPackage()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_product/package/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/package/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 模組打包編輯 Template
	 *
	 */
	public function productPackageItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_product/package/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/package/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 分類列表 Template
	 *
	 */
	public function productCategory()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_product/category/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/category/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 分類編輯 Template
	 *
	 */
	public function productCategoryItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_product/category/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/category/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 評價列表 Template
	 *
	 */
	public function productAssess()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_product/assess/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/assess/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 評價編輯 Template
	 *
	 */
	public function productAssessItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_product/assess/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/assess/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 商品列表 Template
	 *
	 */
	public function productTable($type='ewedding')
	{
		$page_data = array();

		// 內容設定

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		if($type=="ewedding"){
			$page_data['page_content']['view_path'] = 'preview/admin/page_product/list/ewedding'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/list/ewedding_js'; // 頁面主內容的 JS view 位置, 必要
		}elseif($type=="checkIn"){
			$page_data['page_content']['view_path'] = 'preview/admin/page_product/list/checkIn'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/list/checkIn_js'; // 頁面主內容的 JS view 位置, 必要
		}else{
			$page_data['page_content']['view_path'] = 'preview/admin/page_product/list/table'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/list/table_js'; // 頁面主內容的 JS view 位置, 必要
		}
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 商品編輯 Template
	 *
	 */
	public function productItem($type='ewedding')
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		if($type=="ewedding"){
			$page_data['page_content']['view_path'] = 'preview/admin/page_product/list/ewedding_item'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/list/ewedding_item_js'; // 頁面主內容的 JS view 位置, 必要
		}elseif($type=="checkIn"){
			$page_data['page_content']['view_path'] = 'preview/admin/page_product/list/checkIn_item'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/list/checkIn_item_js'; // 頁面主內容的 JS view 位置, 必要
		}else{
			$page_data['page_content']['view_path'] = 'preview/admin/page_product/list/item'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/list/item_js'; // 頁面主內容的 JS view 位置, 必要
		}
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 商品分類屬性列表 Template
	 *
	 */
	public function productAttribute()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_product/attribute/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/attribute/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 商品分類屬性編輯 Template
	 *
	 */
	public function productAttributeItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_product/attribute/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/attribute/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 商品分類屬性值列表 Template
	 *
	 */
	public function productAttributeValue()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_product/attribute/value'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/attribute/value_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 商品分類屬性值編輯 Template
	 *
	 */
	public function productAttributeValueItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_product/attribute/value_item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/attribute/value_item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 庫存管理 Template
	 *
	 */
	public function inventory($type='table')
	{
		$page_data = array();

		// 內容設定

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		if($type=="table"){
			$page_data['page_content']['view_path'] = 'preview/admin/page_product/inventory/table'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/inventory/table_js'; // 頁面主內容的 JS view 位置, 必要
		}elseif($type=="log"){
			$page_data['page_content']['view_path'] = 'preview/admin/page_product/inventory/log'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/inventory/log_js'; // 頁面主內容的 JS view 位置, 必要
		}else{
			$page_data['page_content']['view_path'] = 'preview/admin/page_product/inventory/item'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/inventory/item_js'; // 頁面主內容的 JS view 位置, 必要
		}
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 訂單列表 Template
	 *
	 */
	public function orderTable()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_order/list/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_order/list/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 訂單編輯 Template
	 *
	 */
	public function orderItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_order/list/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_order/list/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 企業訂單列表 Template
	 *
	 */
	public function orderDealer()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_order/list/dealer'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_order/list/dealer_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 內容分類列表 Template
	 *
	 */
	public function contentCategory()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_content/category/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_content/category/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 內容分類編輯 Template
	 *
	 */
	public function contentCategoryItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_content/category/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_content/category/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 部落格列表 Template
	 *
	 */
	public function contentBlog()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_content/blog/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_content/blog/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 部落格編輯 Template
	 *
	 */
	public function contentBlogItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_content/blog/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_content/blog/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * FAQ列表 Template
	 *
	 */
	public function contentFaq()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_content/faq/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_content/faq/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 部落格編輯 Template
	 *
	 */
	public function contentFaqItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_content/faq/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_content/faq/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 廣告版位列表 Template
	 *
	 */
	public function adsSlot()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/slot/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/slot/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 廣告版位編輯 Template
	 *
	 */
	public function adsSlotItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/slot/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/slot/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 廣告素材列表 Template
	 *
	 */
	public function adsMaterial()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/material/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/material/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 廣告素材編輯 Template
	 *
	 */
	public function adsMaterialItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/material/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/material/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 廣告報表 Template
	 * 查詢單一廣告狀態
	 *
	 */
	public function adsMaterialReport()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/material/report'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/material/report_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 經銷商列表 Template
	 *
	 */
	public function dealerTable()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_dealer/list/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_dealer/list/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 經銷商編輯 Template
	 *
	 */
	public function dealerItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_dealer/list/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['status'] = array(
														array('label' => '未發送', 'num' => 200),
														array('label' => '已發送', 'num' => 100),
														array('label' => '已審核', 'num' => 300),
														array('label' => '已使用', 'num' => 300),
														array('label' => '失效', 'num' => 100)
														);
		$page_data['page_level_js']['enable'] = array(
														array('label' => '尚未使用', 'num' => 30000),
														array('label' => '已使用', 'num' => 70000)
														);
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_dealer/list/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 經銷商編輯 Template
	 *
	 */
	public function dealerPage()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_dealer/list/page'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_dealer/list/page_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 經銷商類別編輯 Template
	 *
	 */
	public function dealerLevel()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_dealer/level/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_dealer/level/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 經銷商類別項目編輯 Template
	 *
	 */
	public function dealerLevelItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_dealer/level/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_dealer/level/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 經銷商原生廣告 AD Template
	 *
	 */
	public function dealerAD()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/dealer/report'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/dealer/report_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * VIP 會員列表 Template
	 *
	 */
	public function partnerTable()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_partner/list/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_partner/list/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * VIP 會員編輯 Template
	 *
	 */
	public function partnerItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_partner/list/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_partner/list/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * VIP 會員類別編輯 Template
	 *
	 */
	public function partnerLevel()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_partner/level/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_partner/level/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * VIP 會員類別項目編輯 Template
	 *
	 */
	public function partnerLevelItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_partner/level/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_partner/level/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 兌換券列表 Template
	 *
	 */
	public function certificate()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/certificate/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/certificate/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 兌換券編輯 Template
	 *
	 */
	public function certificateItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/certificate/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/certificate/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 兌換券統計 Template
	 *
	 */
	public function certificateReport()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/certificate/report'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['status'] = array(
														array('label' => '未發送', 'num' => 200),
														array('label' => '已發送', 'num' => 100),
														array('label' => '已審核', 'num' => 300),
														array('label' => '已使用', 'num' => 300),
														array('label' => '失效', 'num' => 100)
														);
		$page_data['page_level_js']['enable'] = array(
														array('label' => '尚未使用', 'num' => 30000),
														array('label' => '已使用', 'num' => 70000)
														);
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/certificate/report_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 兌換券申請列表 Template
	 *
	 */
	public function certificateApply()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/certificate/table_apply'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/certificate/table_apply_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 兌換券申請編輯 Template
	 *
	 */
	public function certificateApplyItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/certificate/item_apply'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/certificate/item_apply_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 兌換券登錄 Template
	 *
	 */
	public function certificateActive()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/certificate/item_active'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/certificate/item_active_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 虛擬折扣卷活動列表 Template
	 *
	 */
	public function ecoupon()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/ecoupon/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/ecoupon/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 虛擬折扣卷編輯 Template
	 *
	 */
	public function ecouponItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/ecoupon/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/ecoupon/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 虛擬折扣卷統計 Template
	 *
	 */
	public function ecouponReport()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/ecoupon/report'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['status'] = array(
														array('label' => '未發送', 'num' => 200),
														array('label' => '已發送', 'num' => 100),
														array('label' => '已使用', 'num' => 300),
														array('label' => '失效', 'num' => 100)
														);
		$page_data['page_level_js']['enable'] = array(
														array('label' => '尚未使用', 'num' => 300),
														array('label' => '已使用', 'num' => 400)
														);
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/ecoupon/report_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 虛擬折扣卷發送 Template
	 *
	 */
	public function ecouponSend()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/ecoupon/send'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/ecoupon/send_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 虛擬折扣卷發送名單 Template
	 *
	 */
	public function ecouponSendList()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/ecoupon/send_list'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/ecoupon/send_list_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 實體折扣卷編輯 Template
	 *
	 */
	public function couponItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/coupon/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/coupon/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 實體折扣卷統計 Template
	 *
	 */
	public function couponReport()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/coupon/report'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['status'] = array(
														array('label' => '未發送', 'num' => 200),
														array('label' => '已發送', 'num' => 100),
														array('label' => '已審核', 'num' => 300),
														array('label' => '已使用', 'num' => 300),
														array('label' => '失效', 'num' => 100)
														);
		$page_data['page_level_js']['enable'] = array(
														array('label' => '尚未使用', 'num' => 30000),
														array('label' => '已使用', 'num' => 70000)
														);
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/coupon/report_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}


	/**
	 * EDM列表 Template
	 *
	 */
	public function edm()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/edm/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/edm/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * EDM編輯 Template
	 *
	 */
	public function edmItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/edm/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/edm/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * EDM發送 Template
	 *
	 */
	public function edmSend()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/edm/send'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/edm/send_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * EDM發送名單 Template
	 *
	 */
	public function edmSendList()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/edm/send_list'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/edm/send_list_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 實體折扣卷申請列表 Template
	 *
	 */
	public function couponApply()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/coupon/table_apply'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/coupon/table_apply_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 實體折扣卷申請編輯 Template
	 *
	 */
	public function couponApplyItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/coupon/item_apply'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/coupon/item_apply_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 實體折扣卷登錄 Template
	 *
	 */
	public function couponActive()
	{
		$page_data = array();
		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/coupon/item_active'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/coupon/item_active_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}


	/**
	 * 系統一般設定 Template
	 *
	 */
	public function basic()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_system/basic/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_system/basic/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}


	/**
	 * 產業屬性列表 Template
	 *
	 */
	public function domain()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_system/domain/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_system/domain/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 產業屬性編輯 Template
	 *
	 */
	public function domainItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_system/domain/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_system/domain/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}


	/**
	 * 付款方式列表 Template
	 *
	 */
	public function payment()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_system/payment/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_system/payment/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 付款方式編輯 Template
	 *
	 */
	public function paymentItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_system/payment/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_system/payment/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}


	/**
	 * 運費列表 Template
	 *
	 */
	public function shipping()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_system/shipping/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_system/shipping/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 運費編輯 Template
	 *
	 */
	public function shippingItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_system/shipping/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_system/shipping/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}


	/**
	 * mail內容設定列表 Template
	 *
	 */
	public function mail()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_system/mail/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_system/mail/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * mail內容設定編輯 Template
	 *
	 */
	public function mailItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_system/mail/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_system/mail/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}


	/**
	 * doc內容設定列表 Template
	 *
	 */
	public function doc()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_system/doc/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_system/doc/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * doc內容設定編輯 Template
	 *
	 */
	public function docItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_system/doc/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_system/doc/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 全站通知 Template
	 *
	 */
	public function notification($type="")
	{
		$page_data = array();

		// 內容設定

		switch($type){
			case "form":
				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'preview/admin/page_callcenter/notification/item'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'preview/admin/page_callcenter/notification/item_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('preview/admin/general/main', $page_data);

			break;
			case "sender":
				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'preview/admin/page_callcenter/notification/table'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'preview/admin/page_callcenter/notification/table_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('preview/admin/general/main', $page_data);

			break;
			case "item":
				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'preview/admin/page_member/notification_item'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'preview/admin/page_member/notification_item_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('preview/admin/general/main', $page_data);

			break;
			default:
				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'preview/admin/page_member/notification'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'preview/admin/page_member/notification_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('preview/admin/general/main', $page_data);

			break;
		}
	}

	/**
	 * 分潤結帳 Template
	 *
	 */
	public function accounting()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_order/list/accounting'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_order/list/accounting_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 婚宴地點管理 Template
	 *
	 */
	public function location($type="")
	{
		$page_data = array();

		// 內容設定

		switch($type){
			case "item":
				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'preview/admin/page_system/location/item'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'preview/admin/page_system/location/item_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('preview/admin/general/main', $page_data);

			break;
			default:
				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'preview/admin/page_system/location/table'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'preview/admin/page_system/location/table_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('preview/admin/general/main', $page_data);

			break;
		}
	}

	/**
	 * 活動查詢 Template
	 *
	 */
	public function event()
	{
		$page_data = array();

		// 內容設定
				// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_ads/event/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_ads/event/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 商品追蹤清單 Template
	 *
	 */
	public function wishList()
	{
		$page_data = array();

		// 內容設定
				// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/admin/page_product/wish_list/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/admin/page_product/wish_list/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/admin/general/main', $page_data);
	}

	/**
	 * 電子禮金管理 Template
	 *
	 */
	public function credit($type="")
	{
		$page_data = array();

		// 內容設定

		switch($type){
			case "report":
				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'preview/admin/page_order/credit/report'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['enable'] = array(
																array('label' => '尚未使用', 'num' => 30000),
																array('label' => '已使用', 'num' => 70000)
																);
				$page_data['page_level_js']['view_path'] = 'preview/admin/page_order/credit/report_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('preview/admin/general/main', $page_data);

			break;
			default:
				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'preview/admin/page_order/credit/table'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'preview/admin/page_order/credit/table_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('preview/admin/general/main', $page_data);

			break;
		}
	}
}
