<?php

 /*共用語言*/
  $lang["val_open"]="請輸入";
	$lang["val_close"]="!";
	
	$lang["create_date"]="建立日期";
	$lang["create_member_sn"]="建立使用者";
	$lang["last_time_update"]="最近更新日期";
	$lang["update_member_sn"]="最近更新使用者";
	
	
	
	
	$lang["hide_flag"]="是否隱藏";
	
	$lang["sort_order"]="排序順次";
  $lang["val_sort_order"]=$lang["val_open"]."排序順次".$lang["val_close"];
	$lang["status"]="使用狀態";
	
	
	$lang["back_list"]="回列表";
	$lang["cencel_list"]="取消";
	
	$lang["general_status"]="使用狀態";
	$lang["general_status_0"]="未啟用";
	$lang["general_status_1"]="啟用中";
	$lang["general_status_2"]="失效";
	
	$lang["general_hide_flag_0"]="開放";
	$lang["general_hide_flag_1"]="隱藏";
	
	$lang["general_default_flag_0"]="否";
	$lang["general_default_flag_1"]="是";
	
	$lang["general_display_flag_1"]="開放";
	$lang["general_display_flag_0"]="隱藏";
	 
	
	
	/*共用語言*/
	
	
	
  /*admin 系統程式*/
  $lang["sys_program_name"]="功能中文名稱";
	$lang["val_sys_program_name"]=$lang["val_open"].$lang["sys_program_name"].$lang["val_close"];
	
	$lang["sys_program_eng_name"]="功能英文名稱";
	$lang["val_sys_program_eng_name"]=$lang["val_open"].$lang["sys_program_eng_name"].$lang["val_close"];
	
	$lang["sys_program_url"]="檔案名稱";
	$lang["val_sys_program_url"]=$lang["val_open"].$lang["sys_program_url"].$lang["val_close"];
	
	/*admin 系統程式*/
	
	 /*admin 系統角色*/
  $lang["role_name"]="角色中文名稱";
	$lang["val_role_name"]=$lang["val_open"].$lang["role_name"].$lang["val_close"];
	
	$lang["role_eng_name"]="角色英文名稱";
	$lang["val_role_eng_name"]=$lang["val_open"].$lang["role_eng_name"].$lang["val_close"];
	
	$lang["role_descrption"]="角色描述";
	$lang["val_role_descrption"]=$lang["val_open"].$lang["role_descrption"].$lang["val_close"];
	
	/*admin 系統角色*/
	
	/* 會員管理*/
	 
	 $lang["member_status"]="帳號狀態";
	 $lang["email"]="帳號";
	 $lang["user_name"]="帳號";
	 $lang["password"]="密碼";
	 $lang["password2"]="重複輸入密碼";
	 $lang["registration_date"]="使用者註冊日期";
	 $lang["last_name"]="姓";
	 $lang["first_name"]="名";
	 $lang["tel"]="市話";
	 $lang["cell"]="手機";
	 $lang["wedding_date"]="結婚日期";
	 $lang["birthday"]="生日";
	 $lang["address"]="住址";
	 $lang["owner_dealer"]="關聯廠商";
	 $lang["memo"]="備註";
	 
	 
	
	
?>