<?php defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('sean_admin/general/_seo.php');
$this->load->view('sean_admin/general/_top.php');
?>

<!-- BEGIN BODY -->
<body>
<!-- BEGIN HEADER -->
<?php switch($this->session->userdata('member_role_system_rules')[0]['role_name']){
	case '經銷商管理員':
		$_header='header_dealer';
	break;
	case '供應商管理員':
		$_header='header_supplier';
	break;
	case 9:
	default:
		$_header='header';
	break;
}?>
<?php $this->load->view('admin/general/'.$_header);?>
<!-- Header END -->



<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
  <?php $this->load->view($page_content['view_path'], $page_content);?>
</div>
<!-- END PAGE CONTAINER -->



<!-- BEGIN PRE-FOOTER -->
<?php //$this->load->view('sean_admin/general/_prefooter.php');?>
<!-- END PRE-FOOTER -->


<!-- BEGIN FOOTER -->
<?php $this->load->view('sean_admin/general/_footer.php');?>
<!-- END FOOTER -->

<!-- BEGIN JAVASCRIPTS -->
<?php $this->load->view($page_level_js['view_path'], $page_level_js);?>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>