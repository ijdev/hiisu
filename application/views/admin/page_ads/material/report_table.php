<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>廣告報表查詢</h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
        廣告報表查詢
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">廣告報表查詢</span>
            </div>
          </div>
      		<form class="form-horizontal" role="form" method="get">
          <div class="portlet-body">
            <div class="note note-warning">
              <div class="input-group input-large date-picker input-daterange pull-left" data-date="2015-05-07" data-date-format="yyyy-mm-dd">
                <span class="input-group-addon"> 期間 </span>
                <input type="text" class="form-control input-sm daterange" name="from" value="<?php echo $from;?>">
                <span class="input-group-addon"> 到 </span>
                <input type="text" class="form-control input-sm daterange" name="to" value="<?php echo $to;?>">
          <input name="flg" type="hidden" value="">
              </div>
              <div class="table-actions-wrapper pull-right">
                <span>
                </span>
                <!--input type="text" class="pagination-panel-input form-control input-inline input-sm" size="12" style="margin: 0 5px;" placeholder="素材名稱">
                <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="12" style="margin: 0 5px;" placeholder="客戶名稱"-->
                <select id="published_locaiotn_type" name="published_locaiotn_type" class="table-group-action-input form-control input-inline input-sm" style="font-size:11px;">
                  <option value="">頻道</option>
                    <?php if($published_locaiotn_types){foreach($published_locaiotn_types as $key=>$_Item){ ?>
                      <option value="<?php echo $_Item['published_locaiotn_type'];?>" <?php echo ($published_locaiotn_type==$_Item['published_locaiotn_type'])?'selected':'';?>><?php echo $_Item['published_locaiotn_type_name'];?></option>
                    <?php }}?>
                </select>
                <select id="banner_location_sn" name="banner_location_sn" class="table-group-action-input form-control input-inline input-sm" style="font-size:11px;">
                  <option value="">版位</option>
                    <?php if($banner_locations){foreach($banner_locations as $key=>$_Item){ ?>
                      <option value="<?php echo $_Item['banner_location_sn'];?>" <?php echo (@$banner_location_sn==$_Item['banner_location_sn'])?'selected':'';?>><?php echo $_Item['banner_location_name'];?></option>
                    <?php }}?>
                </select>
                <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button> &nbsp; 
           		  <button id="reset_form" onclick="this.form.flg.value='reset';" class="btn btn-sm purple"><i class="fa"></i> 清除</button>
              </div>
              <div style="clear:both;"></div>
            </div>
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>客戶名稱</th>
                  <th>廣告素材名稱</th>
                  <th>廣告版位名稱</th>
                  <th>頻道</th>
                  <th>檔期</th>
                  <th>View</th>
                  <th>Click</th>
                </tr>
              </thead>
              <tbody>
								<?php if($Items){ foreach($Items as $key=>$_Item){?>
                  <tr class="odd gradeX">
										<td><?php echo $_Item['banner_customer_name'];?></td>
                    <td><?php echo $_Item['banner_content_name'];?></td>
                    <td><?php echo $_Item['published_locaiotn_type_name'];?></td>
                    <td><?php echo $_Item['banner_location_name'];?></td>
                    <td><small><?php echo $_Item['banner_list_start_date'];?>~<br><?php echo $_Item['banner_list_end_date'];?></small></td>
                    <td><?php echo $_Item['view_amount'];?></td>
                    <td><?php echo $_Item['click_amount'];?></td>
                  </tr>
                <?php }}?>
              </tbody>
            </table>
          </div>
        </form>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->


      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->