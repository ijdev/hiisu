<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/

foreach($_result01 as $key => $value)
	{
		$_data_info=$value;
	}		

?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>廣告版位設定 <small>版位設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin-ijwedding/Ads">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin-ijwedding/Ads/adsSlot">廣告版位</a><i class="fa fa-circle"></i>
      </li>
      <li class="active">
        廣告版位設定
      </li>
    </ul>
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">廣告版位設定</span>
            </div>
            <div class="tools">
              <a href="admin-ijwedding/Ads/adsSlot"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" method="post" role="form" action="admin-ijwedding/Ads/adsSlotItem/update/<?php echo $_data_info->banner_location_sn;?>">
              <div class="form-body">
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">頻道</label>
                  <div class="col-lg-8">
                    <select class="form-control">
                    	<?php 
                    		$_adsSlot_type =  $this->config->item("adsSlot_type");
												foreach($_adsSlot_type as $key => $value)
													{																																		
														if (preg_match("/$key/i", $_data_info->associated_category_sn))
															{																
																print '<option value="'.$key.'" selected >'.$value.'</option>';														
															}
														else
															{																
																print '<option value="'.$key.'" >'.$value.'</option>';														
															}																																						
													}                    	
                    	?>                      	         
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">版位名稱</label>
                  <div class="col-lg-8">
                    <input type="text" name="banner_location_name" class="form-control" value="<?php echo $_data_info->banner_location_name;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">寬度</label>
                  <div class="col-lg-8">
                    <input type="text" name="width" class="form-control" value="<?php echo $_data_info->width;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">高度</label>
                  <div class="col-lg-8">
                    <input type="text" name="hight" class="form-control" value="<?php echo $_data_info->hight;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-3 control-label">敘述</label>
                  <div class="col-lg-8">
                    <textarea name="banner_location_description" class="form-control" rows="3"><?php echo $_data_info->banner_location_description;?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">型態</label>
                  <div class="col-lg-8">
                    <select class="form-control" name="banner_tyype">
                    	<?php 
                    		$_banner_type =  $this->config->item("banner_type");
												foreach($_banner_type as $key => $value)
													{																																		
														if (preg_match("/$key/i", $_data_info->banner_tyype))
															{																
																print '<option value="'.$key.'" selected >'.$value.'</option>';														
															}
														else
															{																
																print '<option value="'.$key.'" >'.$value.'</option>';														
															}																																						
													}                    	
                    	?>                      	
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">預設內容</label>
                  <div class="col-lg-8">
                    <div class="adType imgAD">
                      <img src="public/metronic/frontend/pages/img/works/img3.jpg" height="160" class="feature-img">
                      <i class="fa fa-trash-o"></i>
                    </div>
                    <div class="adType textAD">
                      <input type="text" class="form-control" placeholder="廣告顯示文字" value="預設文字預設文字">
                    </div>
                    <div class="adType htmlAD">
                      <textarea class="form-control" rows="3" placeholder="請貼入 HTML 代碼">預設內容<br>
預設內容第二行</textarea>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">預設連結</label>
                  <div class="col-lg-8">
                    <input type="text" name="default_banner_link" class="form-control" value="<?php echo $_data_info->default_banner_link;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-lg-3 control-label">更新者</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="<?php echo $this->session->userdata['member_data']['user_name'];?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-3 control-label">更新時間</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="<?php echo date("Y-m-d H:i:s");?>" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                    <a href="admin-ijwedding/Ads/adsSlot"><button type="button" class="btn default">Cancel</button></a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->