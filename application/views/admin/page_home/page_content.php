<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<style>
  body,h1 {font-family: '微軟正黑體' !important;}
</style>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>控制台 <small>統計</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
<?php $this->load->view('sean_admin/general/flash_error');?>
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-light blue-madison" href="javascript:;">
        <div class="visual">
          <i class="fa fa-briefcase fa-icon-medium"></i>
        </div>
        <div class="details">
          <div class="number">
             <?=number_format($total_amount)?>
          </div>
          <div class="desc">
             總銷售額
          </div>
        </div>
        </a>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-light red-intense" href="javascript:;">
        <div class="visual">
          <i class="fa fa-shopping-cart"></i>
        </div>
        <div class="details">
          <div class="number">
             <?=number_format($total_orders)?>
          </div>
          <div class="desc">
             總訂單數
          </div>
        </div>
        </a>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-light green-haze" href="javascript:;">
        <div class="visual">
          <i class="fa fa-group fa-icon-medium"></i>
        </div>
        <div class="details">
          <div class="number">
             <?=number_format($average_amount)?>
          </div>
          <div class="desc">
             平均訂單銷售額
          </div>
        </div>
        </a>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-10">
        <a class="dashboard-stat dashboard-stat-light blue-madison" href="javascript:;">
        <div class="visual">
          <i class="fa fa-briefcase fa-icon-medium"></i>
        </div>
        <div class="details">
          <div class="number">
             <?=number_format($current_month_total_amount)?>
          </div>
          <div class="desc">
             本月銷售額
          </div>
        </div>
        </a>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-light red-intense" href="javascript:;">
        <div class="visual">
          <i class="fa fa-shopping-cart"></i>
        </div>
        <div class="details">
          <div class="number">
             <?=number_format($current_month_total_orders)?>
          </div>
          <div class="desc">
             本月訂單數
          </div>
        </div>
        </a>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-light green-haze" href="javascript:;">
        <div class="visual">
          <i class="fa fa-group fa-icon-medium"></i>
        </div>
        <div class="details">
          <div class="number">
             <?=number_format($current_month_average_amount)?>
          </div>
          <div class="desc">
             本月平均訂單銷售額
          </div>
        </div>
        </a>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <!-- Begin: life time stats -->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="icon-bar-chart font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">銷售總覽</span>
              <span class="caption-helper">本月統計</span>
            </div>
            <div class="tools">
              <!--a href="javascript:;" class="collapse">
              </a>
              <a href="#portlet-config" data-toggle="modal" class="config">
              </a>
              <a href="javascript:;" class="remove">
              </a-->
              <a href="javascript:;" class="reload">
              </a>
            </div>
          </div>
          <div class="portlet-body" id="overview_area">
            <div class="tabbable-line">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#overview_1" data-toggle="tab">
                  熱銷排名 </a>
                </li>
                <li>
                  <a href="#overview_2" data-toggle="tab">
                  熱門瀏覽 </a>
                </li>
                <li>
                  <a href="#overview_3" data-toggle="tab">
                  客戶 </a>
                </li>
                <li>
                  <a href="#overview_4" data-toggle="tab">
                  訂單 </a>
                </li>
                <!--li class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                  Orders <i class="fa fa-angle-down"></i>
                  </a>
                  <ul class="dropdown-menu" role="menu">
                    <li>
                      <a href="#overview_4" tabindex="-1" data-toggle="tab">
                      Latest 10 Orders </a>
                    </li>
                    <li>
                      <a href="#overview_4" tabindex="-1" data-toggle="tab">
                      Pending Orders </a>
                    </li>
                    <li>
                      <a href="#overview_4" tabindex="-1" data-toggle="tab">
                      Completed Orders </a>
                    </li>
                    <li>
                      <a href="#overview_4" tabindex="-1" data-toggle="tab">
                      Rejected Orders </a>
                    </li>
                  </ul>
                </li-->
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="overview_1">
                  <div class="table-responsive">
                    <table class="table table-hover table-light">
                    <thead>
                    <tr class="uppercase">
                      <th>
                         商品名稱
                      </th>
                      <th>
                         平均價格
                      </th>
                      <th>
                         數量
                      </th>
                      <th>
                      </th>
                    </tr>
                    </thead>
                    <tbody>
<?if($hot_sales){ foreach($hot_sales as $_item){?>
                    <tr>
                      <td>
                        <a href="/shop/shopItem/<?=$_item['product_sn']?>" target="_blank">
                        <?=$_item['product_name']?> </a>
                      </td>
                      <td>
                         $<?=number_format($_item['actual_sales_amount']/$_item['buy_amount'])?>
                      </td>
                      <td>
                         <?=number_format($_item['buy_amount'])?>
                      </td>
                      <td>
                        <a href="/shop/shopItem/<?=$_item['product_sn']?>" class="btn default btn-xs green-stripe" target="_blank">
                        View </a>
                      </td>
                    </tr>
<?}}?>
                    </tbody>
                    </table>
                  </div>
                </div>
                <div class="tab-pane" id="overview_2">
                  <div class="table-responsive">
                    <table class="table table-hover table-light">
                    <thead>
                    <tr class="uppercase">
                      <th>
                         商品名稱
                      </th>
                      <th>
                         平均價格
                      </th>
                      <th>
                         瀏覽次數
                      </th>
                      <th>
                      </th>
                    </tr>
                    </thead>
                    <tbody>
<?if($click_sales){ foreach($click_sales as $_item){?>
                    <tr>
                      <td>
                        <a href="/shop/shopItem/<?=$_item['product_sn']?>" target="_blank">
                        <?=$_item['product_name']?> </a>
                      </td>
                      <td>
                         $<?=number_format($_item['actual_sales_amount']/$_item['buy_amount'])?>
                      </td>
                      <td>
                         <?=number_format($_item['click_number'])?>
                      </td>
                      <td>
                        <a href="/shop/shopItem/<?=$_item['product_sn']?>" class="btn default btn-xs green-stripe" target="_blank">
                        View </a>
                      </td>
                    </tr>
<?}}?>
                    </tbody>
                    </table>
                  </div>
                </div>
                <div class="tab-pane" id="overview_3">
                  <div class="table-responsive">
                    <table class="table table-hover table-light">
                    <thead>
                    <tr>
                      <th>
                         客戶姓名
                      </th>
                      <th>
                         總訂單數
                      </th>
                      <th>
                         總金額
                      </th>
                      <th>
                      </th>
                    </tr>
                    </thead>
                    <tbody>
<?if($members){ foreach($members as $_item){?>
                    <tr>
                      <td>
                        <a href="/admin/Member/memberTable_item/edit/<?=$_item['member_sn']?>" target="_blank">
                        <?=$_item['last_name']?> </a>
                      </td>
                      <td>
                         <?=number_format($_item['times'])?>
                      </td>
                      <td>
                         $<?=number_format($_item['total_order_amount'])?>
                      </td>
                      <td>
                        <a href="/admin/Member/memberTable_item/edit/<?=$_item['member_sn']?>" class="btn default btn-xs green-stripe" target="_blank">
                        View </a>
                      </td>
                    </tr>
<?}}?>
                    </tbody>
                    </table>
                  </div>
                </div>
                <div class="tab-pane" id="overview_4">
                  <div class="table-responsive">
                    <table class="table table-hover table-light">
                    <thead>
                    <tr class="uppercase">
                      <th>
                         客戶姓名
                      </th>
                      <th>
                         日期
                      </th>
                      <th>
                         金額
                      </th>
                      <th>
                         狀態
                      </th>
                      <th>
                      </th>
                    </tr>
                    </thead>
                    <tbody>
<?if($sub_orders){ foreach($sub_orders as $_item){?>
                    <tr>
                      <td>
                        <a href="/admin/Member/memberTable_item/edit/<?=$_item['associated_member_sn']?>" target="_blank">
                        <?=$_item['buyer_last_name']?> </a>
                      </td>
                      <td>
                         <?=substr($_item['order_create_date'],0,10)?>
                      </td>
                      <td>
                         $<?=number_format($_item['sub_sum'])?>
                      </td>
                      <td>
                         <span class="label label-sm label-<?=$this->config->item('label_status')[$_item['sub_order_status']]?>"><?=$_item['sub_order_status_name']?></span>
                      </td>
                      <td>
                        <a href="/admin/admin/orderItem/<?=$_item['sub_order_sn']?>" class="btn default btn-xs green-stripe" target="_blank">
                        View </a>
                      </td>
                    </tr>
<?}}?>
                    </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- End: life time stats -->
      </div>
      <div class="col-md-6">
        <!-- Begin: life time stats -->
        <div class="portlet light">
          <div class="portlet-title tabbable-line">
            <div class="caption">
              <i class="icon-share font-red-sunglo"></i>
              <span class="caption-subject font-red-sunglo bold uppercase">銷售圖</span>
              <span class="caption-helper">(年.週)</span>
            </div>
            <ul class="nav nav-tabs">
              <li>
                <a href="#portlet_tab2" data-toggle="tab" id="statistics_amounts_tab">
                銷售額 </a>
              </li>
              <li class="active">
                <a href="#portlet_tab1" data-toggle="tab">
                訂單 </a>
              </li>
            </ul>
          </div>
          <div class="portlet-body">
            <div class="tab-content">
              <div class="tab-pane active" id="portlet_tab1">
                <div id="statistics_1" class="chart">
                </div>
              </div>
              <div class="tab-pane" id="portlet_tab2">
                <div id="statistics_2" class="chart">
                </div>
              </div>
            </div>
            <!--div class="margin-top-20 no-margin no-border">
              <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                  <span class="label label-success uppercase">
                  銷售額： </span>
                  <h3>$0</h3>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                  <span class="label label-info uppercase">
                  Tax: </span>
                  <h3>$0</h3>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                  <span class="label label-danger uppercase">
                  Shipment: </span>
                  <h3>$0</h3>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                  <span class="label label-warning uppercase">
                  訂單： </span>
                  <h3>0</h3>
                </div>
              </div>
            </div-->
          </div>
        </div>
        <!-- End: life time stats -->
      </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
  </div>
</div>
<!-- END PAGE CONTENT -->
