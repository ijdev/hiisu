<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/typeahead/typeahead.css">
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/pages/css/portfolio.css" rel="stylesheet" type="text/css"/>



<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>現場報到系統 <small>商品編輯</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="preview">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品列表
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="preview/productTable/checkIn">現場報到系統</a>
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
        商品編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">商品編輯</span>
            </div>
            <div class="actions btn-set">
              <a href="preview/productTable/checkIn" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form">
              <div class="tabbable">
                <ul class="nav nav-tabs">
                  <li class="active">
                    <a href="#tab_general" data-toggle="tab"> 一般設定 </a>
                  </li>
                  <li>
                    <a href="#tab_price" data-toggle="tab"> 銷售設定 </a>
                  </li>
                  <li>
                    <a href="#tab_meta" data-toggle="tab"> Meta </a>
                  </li>
                  <li id="tabThumbs">
                    <a href="#tab_thumbs" data-toggle="tab"> 商品圖設定 </a>
                  </li>
                  <li>
                    <a href="#tab_shipping" data-toggle="tab"> 配送設置 </a>
                  </li>
                  <li>
                    <a href="#tab_stock" data-toggle="tab"> 庫存管理 </a>
                  </li>
                  <li id="tabAdditionalPurchase">
                    <a href="#tab_additional_purchase" data-toggle="tab"> 加購商品設定 </a>
                  </li>
                </ul>
                <div class="tab-content no-space">
                  <div class="tab-pane active" id="tab_general">
                    <div class="form-body">
                      <div class="form-group">
                        <label for="name" class="col-md-2 control-label">是否發佈</label>
                        <div class="col-md-10">
                          <div class="radio-list">
                              <label class="radio-inline">
                                <input type="radio" name="optionsRadios" id="optionsRadios4" value="option1" checked>  發佈  
                              </label>
                              <label class="radio-inline">
                                <input type="radio" name="optionsRadios" id="optionsRadios4" value="option1" checked>  不發佈 
                                <a href="http://dev.ijwedding.com/preview_jerry/shopItem" target="_blank" onclick="if(!confirm('前往預覽畫面')){return false;}"><i class="fa fa-eye"></i></a>
                              </label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-md-2 control-label">排序</label>
                        <div class="col-md-2">
                          <input type="text" class="form-control" value="" placeholder="預設 9999">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-md-2 control-label">產品名稱</label>
                        <div class="col-md-10">
                          <input type="text" class="form-control" value="商品一二三">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-md-2 control-label">庫號</label>
                        <div class="col-md-10">
                          <input type="text" class="form-control" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-md-2 control-label">銷售對象</label>
                        <div class="col-md-10">
                          <div class="checkbox-list">
                            <label class="checkbox-inline">
                              <input class="checkbox-list" type="checkbox" class="form-control" value="" checked="">&nbsp;一般使用者
                            </label>
                            <label class="checkbox-inline">
                              <input class="checkbox-list" type="checkbox" class="form-control" value="">&nbsp;經銷商
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">主分類</label>
                        <div class="col-md-10">
                          <select class="form-control">
                            <option value="2001">├─現場報到系統</option>
                            <option value="1001">├─Voucher</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">擴展分類</label>
                        <div class="col-md-10">
                          <select multiple class="form-control">
                            <option value="2001">├─現場報到系統</option>
                            <option value="1001">├─Voucher</option>
                          </select>
                          <span class="help-block"> 按住 control 進行複選 </span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-md-2 control-label">商品分類屬性</label>
                        <div class="col-md-10">
                          <div class="form-control height-auto">
                            <div class="table-scrollable table-scrollable-borderless">
                              <table class="table table-hover">
                                <thead>
                                  <tr>
                                    <th>分類屬性</th>
                                    <th>屬性值</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td><label class="control-label">分類屬性1</label></td>
                                    <td>
                                      <div class="checkbox-list">
                                        <label class="checkbox-inline" style="float:right;">
                                          <a data-toggle="modal" href="#addAttrValue" class="btn green btn-xs">新增屬性值</a>
                                        </label>
                                        <label class="checkbox-inline">
                                          <input class="checkbox-list" type="checkbox" class="form-control" value="">&nbsp;分類屬性1值1
                                        </label>
                                        <label class="checkbox-inline">
                                          <input class="checkbox-list" type="checkbox" class="form-control" value="">&nbsp;分類屬性1值2
                                        </label>
                                        <label class="checkbox-inline">
                                          <input class="checkbox-list" type="checkbox" class="form-control" value="">&nbsp;分類屬性1值3
                                        </label>
                                      </div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td><label class="control-label">分類屬性2</label></td>
                                    <td>
                                      <div class="checkbox-list">
                                        <label class="checkbox-inline" style="float:right;">
                                          <a data-toggle="modal" href="#addAttrValue" class="btn green btn-xs">新增屬性值</a>
                                        </label>
                                        <label class="checkbox-inline">
                                          <input class="checkbox-list" type="checkbox" class="form-control" value="">&nbsp;分類屬性2值1
                                        </label>
                                        <label class="checkbox-inline">
                                          <input class="checkbox-list" type="checkbox" class="form-control" value="">&nbsp;分類屬性2值2
                                        </label>
                                      </div>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                          <span class="help-block">依照不同的分類顯示不同的分類屬性讓管理員選擇</span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="note" class="col-md-2 control-label">付款方式</label>
                        <div class="col-md-10">
                          <div class="checkbox-list">
                            <label class="checkbox-inline">
                              <input class="checkbox-list" type="checkbox" class="form-control" value="">&nbsp;信用卡付款
                            </label>
                            <label class="checkbox-inline">
                              <input class="checkbox-list" type="checkbox" class="form-control" value="" disabled="">&nbsp;ATM 轉帳
                            </label>
                            <label class="checkbox-inline">
                              <input class="checkbox-list" type="checkbox" class="form-control" value="" disabled="">&nbsp;超商取貨
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="note" class="col-md-2 control-label">商品資訊</label>
                        <div class="col-md-10">
                          <textarea class="ckeditor form-control" name="editor1" rows="6"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- End of tab_general -->
                  <div class="tab-pane" id="tab_price">
                    <h4>特殊規格選擇</h4>
                    <div class="form-group">
                      <label for="name" class="col-md-2 control-label">款型</label>
                      <div class="col-md-10">
                        <div class="checkbox-list">
                          <label class="checkbox-inline">
                          <input type="checkbox" name="template-Package[]"  value="基本款"> 基本款 </label>
                          <label class="checkbox-inline">
                          <input type="checkbox" name="template-Package[]"  value="豪華款" checked="checked"> 豪華款 </label>
                          <label class="checkbox-inline">
                          <input type="checkbox" name="template-Package[]"  value="旗艦款"> 旗艦款 </label>
                        </div>
                      </div>
                      <p class="form-control-static">依照一般設定中的分類設定顯示不同的規格組合選擇，設定後價格設定也會顯示相對應的價格設定項目</p>
                    </div>
                    <hr>
                    <h4>購買限制與定價方式</h4>
                    <div class="form-group">
                      <label for="name" class="col-md-2 control-label">最少採購量</label>
                      <div class="col-md-10">
                        <input type="text" class="form-control" placeholder="" value="1">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="name" class="col-md-2 control-label">遞增數量</label>
                      <div class="col-md-10">
                        <input type="text" class="form-control" placeholder="" value="1">
                        <span class="help-block">規範選單出現的數量選擇遞增數量，填入 10，則只會出現 110,120,130.... 以此類推</span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="name" class="col-md-2 control-label">定價方式</label>
                      <div class="col-md-10">
                        <div class="radio-list">
                          <label class="radio-inline"><input type="radio" name="setting_price" value="original" checked="">單一售價</label>
                          <label class="radio-inline"><input type="radio" name="setting_price" value="area" disabled="">區間計價</label>
                        </div>
                      </div>
                    </div>
                    <!-- /購買限制 -->
                    <div id="setting_price_area" style="display:none;">
                      <hr>
                      <h4>
                        區間計價設定
                        <div style="float:right;">
                          <a href="javascript:void(0);" class="btn green btn-xs" id="btn_area_add"><i class="fa fa-plus"></i> 新增區間</a>
                        </div>
                        <div style="clear:both;"></div>
                      </h4>
                      <div id="sample_price_area" style="display:none;">
                        <div class="form-group">
                          <label for="name" class="col-md-4 control-label">
                            <label for="name" class="col-md-4 control-label">起始數量</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" placeholder="此區間起始數量" value="">
                            </div>
                          </label>
                          <label for="name" class="col-md-4 control-label">
                            <label for="name" class="col-md-4 control-label">結束數量</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" placeholder="此區間結束數量" value="">
                            </div>
                          </label>
                          <label for="name" class="col-md-3 control-label">
                            <label for="name" class="col-md-4 control-label">單價</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" placeholder="此區間單價" value="">
                            </div>
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-md-4 control-label">
                          <label for="name" class="col-md-4 control-label">起始數量</label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" placeholder="此區間起始數量" value="">
                          </div>
                        </label>
                        <label for="name" class="col-md-4 control-label">
                          <label for="name" class="col-md-4 control-label">結束數量</label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" placeholder="此區間結束數量" value="">
                          </div>
                        </label>
                        <label for="name" class="col-md-3 control-label">
                          <label for="name" class="col-md-4 control-label">單價</label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" placeholder="此區間單價" value="">
                          </div>
                        </label>
                      </div>
                    </div>
                    <div id="setting_price_original">
                      <hr>
                      <h4>單一售價設定</h4>
                      <div class="form-group">
                        <label for="name" class="col-md-2 control-label">規格</label>
                        <label for="name" class="col-md-3 control-label">定價</label>
                        <label for="name" class="col-md-3 control-label">售價</label>
                      </div>
                      <div class="form-group trPackage" style="display:none;" rel="原始">
                        <label for="name" class="col-md-2 control-label">無</label>
                        <div class="col-md-3">
                          <input type="text" class="form-control" placeholder="">
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control" placeholder="">
                        </div>
                      </div>
                      <div class="form-group trPackage" style="display:none;" rel="基本款">
                        <label for="name" class="col-md-2 control-label">基本款</label>
                        <div class="col-md-3">
                          <input type="text" class="form-control" placeholder="">
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control" placeholder="">
                        </div>
                      </div>
                      <div class="form-group trPackage" style="display:none;" rel="豪華款">
                        <label for="name" class="col-md-2 control-label">豪華款</label>
                        <div class="col-md-3">
                          <input type="text" class="form-control" placeholder="">
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control" placeholder="">
                        </div>
                      </div>
                      <div class="form-group trPackage" style="display:none;" rel="旗艦款">
                        <label for="name" class="col-md-2 control-label">旗艦款</label>
                        <div class="col-md-3">
                          <input type="text" class="form-control" placeholder="">
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control" placeholder="">
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- End of tab_price -->
                  <div class="tab-pane" id="tab_meta">
                    <div class="form-group">
                      <label class="col-md-2 control-label">Meta Title</label>
                      <div class="col-md-10">
                        <input type="text" class="form-control maxlength-handler" name="product[meta_title]" maxlength="100" placeholder="">
                        <span class="help-block">
                        max 100 chars </span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Meta Keywords</label>
                      <div class="col-md-10">
                        <textarea class="form-control maxlength-handler" rows="8" name="product[meta_keywords]" maxlength="1000"></textarea>
                        <span class="help-block">
                        max 1000 chars </span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Meta Description</label>
                      <div class="col-md-10">
                        <textarea class="form-control maxlength-handler" rows="8" name="product[meta_description]" maxlength="255"></textarea>
                        <span class="help-block">
                        max 255 chars </span>
                      </div>
                    </div>
                  </div>
                  <!-- End of tab_meta -->
                  <div class="tab-pane" id="tab_thumbs">
                    <h4>商品圖設定</h4>
                    <div class="form-group">
                      <label class="col-md-2 control-label">商品主圖</label>
                      <div class="col-md-3">
                        <img src="http://dev.ijwedding.com/public/img/cover_sample.png" width="120" style="vertical-align: bottom;" ><i class="fa fa-trash-o"></i>
                        <span class="help-block">最佳大小 720px X 720px</span>
                      </div>
                      <label class="col-md-3 control-label">圖片描述</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control" placeholder="預設為商品名稱">
                      </div>
                    </div>
                    <hr>
                    <div class="form-group">
                      <label class="col-md-2 control-label">商品圖一</label>
                      <div class="col-md-3">
                        <input class="form-control" name="thumb2" type="file" id="photo" onchange="">
                        <span class="help-block">最佳大小 720px X 720px</span>
                      </div>
                      <label class="col-md-3 control-label">圖片描述</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control" placeholder="預設為商品名稱">
                      </div>
                    </div>
                    <hr>
                    <div class="form-group">
                      <label class="col-md-2 control-label">商品圖二</label>
                      <div class="col-md-3">
                        <input class="form-control" name="thumb3" type="file" id="photo" onchange="">
                        <span class="help-block">最佳大小 720px X 720px</span>
                      </div>
                      <label class="col-md-3 control-label">圖片描述</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control" placeholder="預設為商品名稱">
                      </div>
                    </div>
                    <hr>
                    <div class="form-group">
                      <label class="col-md-2 control-label">商品圖三</label>
                      <div class="col-md-3">
                        <input class="form-control" name="thumb4" type="file" id="photo" onchange="">
                        <span class="help-block">最佳大小 720px X 720px</span>
                      </div>
                      <label class="col-md-3 control-label">圖片描述</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control" placeholder="預設為商品名稱">
                      </div>
                    </div>
                    <hr>
                    <div class="form-group">
                      <label class="col-md-2 control-label">商品圖四</label>
                      <div class="col-md-3">
                        <input class="form-control" name="thumb5" type="file" id="photo" onchange="">
                        <span class="help-block">最佳大小 720px X 720px</span>
                      </div>
                      <label class="col-md-3 control-label">圖片描述</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control" placeholder="預設為商品名稱">
                      </div>
                    </div>
                  </div>
                  <!-- End of tab_thumbs -->
                  <div class="tab-pane" id="tab_shipping">
                    <div class="form-group">
                      <label class="col-md-2 control-label">貨到付款</label>
                      <div class="col-md-10">
                        <div class="radio-list">
                          <label class="radio-inline"><input type="radio" name="arr" value="1" disabled="">是</label>
                          <label class="radio-inline"><input type="radio" name="arr" value="0" checked="">否</label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">滿額免運費</label>
                      <div class="col-md-10">
                        <div class="radio-list">
                          <label class="radio-inline"><input type="radio" name="arr1" value="1" disabled="">是</label>
                          <label class="radio-inline"><input type="radio" name="arr1" value="0" checked="">否</label>
                        </div>
                      </div>
                    </div>

                  </div>
                  <!-- End of tab_shipping -->
                  <div class="tab-pane" id="tab_stock">
                    <div class="form-group">
                      <label class="col-md-2 control-label">庫存</label>
                      <div class="col-md-10">
                        <div class="radio-list">
                          <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="12" placeholder="0" value="-100">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">開放預購</label>
                      <div class="col-md-10">
                        <div class="radio-list">
                          <label class="radio-inline"><input type="radio" name="book" value="Y" checked="">是</label>
                          <label class="radio-inline"><input type="radio" name="book" value="N" disabled="">否</label>
                        </div>
                        <span class="help-block">開放預購，庫存不足仍可購買，所以虛擬類的商品都可直接設為開放</span>
                      </div>
                    </div>

                  </div>
                  <!-- End of tab_stock -->
                  <div class="tab-pane" id="tab_additional_purchase">
                    <div class="portlet light">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs font-green-sharp"></i>
                          <span class="caption-subject font-green-sharp bold uppercase">加購列表</span>
                        </div>
                        <div class="actions btn-set">
                          <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="12" style="margin: 0 5px;" placeholder="請輸入系統序號">
                          <a data-toggle="modal" href="#basic" class="btn green"><i class="fa fa-plus"></i> 新增</a>
                        </div>
                      </div>
                      <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="table_member">
                          <thead>
                            <tr>
                              <th>商品名稱</th>
                              <th>定價</th>
                              <th>加購價</th>
                              <th>功能</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="odd gradeX">
                              <td>相簿1</td>
                              <td class="center">100</td>
                              <td class="center">50</td>
                              <td class="center">
                                <a data-toggle="modal" href="#basic"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                                <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                              </td>
                            </tr>
                            <tr class="odd gradeX">
                              <td>相簿2</td>
                              <td>200</td>
                              <td class="center">200</td>
                              <td class="center">
                                <a data-toggle="modal" href="#basic"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                                <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                  </div>
                </div>
                <!-- End of tab-content -->
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="button" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->

<!-- Modal -->
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">上傳圖片</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-without-legend" role="form">
          <fieldset>
            <div class="form-group">
              <label for="email" class="col-lg-4 control-label">圖片標題 <span class="require">*</span></label>
              <div class="col-md-8">
                <input type="text" class="form-control" id="email">
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-4 control-label">顏色</label>
              <div class="col-md-8">
                <select class="form-control" name="imgColorSelect">
                  <option value="粉紅色" disabled="">粉紅色</option>
                  <option value="黑色" disabled="">黑色</option>
                  <option value="金色" disabled="">金色</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-4 control-label">選擇檔案</label>
              <div class="col-md-8">
                <input class="form-control" name="photo" type="file" id="photo" onchange="">
              </div>
            </div>
          </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">上傳</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">設定加購價</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form">
          <div class="form-group">
            <label for="name" class="col-md-3 control-label">商品名稱</label>
            <div class="col-md-9">
              <p class="form-control-static">相簿(商品名稱)</p>
            </div>
          </div>
          <div class="form-group">
            <label for="name" class="col-md-3 control-label">定價</label>
            <div class="col-md-9">
              <p class="form-control-static">100</p>
            </div>
          </div>
          <div class="form-group">
            <label for="name" class="col-md-3 control-label">加購價</label>
            <div class="col-md-9">
              <input type="text" class="form-control" id="" value="50">
            </div>
          </div>
          <div class="form-group">
            <label for="name" class="col-md-3 control-label">加購數量限制</label>
            <div class="col-md-2">
              <input type="text" class="form-control" id="" value="1">
            </div>
            <div class="col-md-7">
              <span class="help-block">數量限制隨著主商品數量倍增，所以填入 3 ，買兩個主商品就最多就能買 6 個加購商品</span>
            </div>
          </div>
          
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
        <button type="button" class="btn blue">儲存</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" id="addAttrValue" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">新增屬性值</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form">
          <div class="form-group">
            <label for="name" class="col-md-2 control-label">屬性</label>
            <div class="col-md-10">
              <p class="form-control-static">分類屬性1</p>
            </div>
          </div>
          <div class="form-group">
            <label for="name" class="col-md-2 control-label">屬性值</label>
            <div class="col-md-10">
              <input type="text" class="form-control" id="" value="">
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
        <button type="button" class="btn blue">確定新增</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

