<!-- BEGIN RIGHT SIDEBAR -->            
<div class="col-md-3 col-sm-3 blog-sidebar">
  <!-- CATEGORIES START -->
  <h2 class="no-top-space">最新問答</h2>
  <ul class="nav sidebar-categories margin-bottom-40">
    <li><a href="javascript:;">question1</a></li>
    <li><a href="javascript:;">Moscow</a></li>
    <li class="active"><a href="javascript:;">Paris</a></li>
  </ul>
  <!-- CATEGORIES END -->

  <!-- BEGIN BLOG TAGS -->
  <div class="blog-tags margin-bottom-20">
    <h2>關鍵字</h2>
    <ul>
      <li><a href="javascript:;"><i class="fa fa-tags"></i>OS</a></li>
      <li><a href="javascript:;"><i class="fa fa-tags"></i>Metronic</a></li>
      <li><a href="javascript:;"><i class="fa fa-tags"></i>Dell</a></li>
      <li><a href="javascript:;"><i class="fa fa-tags"></i>Conquer</a></li>
      <li><a href="javascript:;"><i class="fa fa-tags"></i>MS</a></li>
      <li><a href="javascript:;"><i class="fa fa-tags"></i>Google</a></li>
      <li><a href="javascript:;"><i class="fa fa-tags"></i>Keenthemes</a></li>
      <li><a href="javascript:;"><i class="fa fa-tags"></i>Twitter</a></li>
    </ul>
  </div>
  <!-- END BLOG TAGS -->
</div>
<!-- END RIGHT SIDEBAR -->