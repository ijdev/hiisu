<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class supplier_table extends CI_Model {

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('sean_models/Sean_db_tools');
		$this->load->model('sean_models/Sean_db_toolss');


		/* Begin 修改  */
		$this->_ct_name="經銷商";// 功能名稱
		$this->_url="supplier/Supplier_list/";//列表功能url
		$this->_url_item="Member/supplierTable_item/";//處理動作 url
		$this->_table="ij_supplier";//table name
		$this->_id="supplier_sn";//table key
		$this->_table_field=array(
					 "supplier_status" => "帳號狀態",
					 "supplier_name" => "公司中文名稱",
					 "supplier_eng_name" => "公司英文名稱",
					 //"supplier_brand_name" => "品牌名稱",
					 "main_domain_sn" => "主要產業類別",
					 "cooperate_mode_type" => "合作模式",
					 "partnership_start_date" => "有效日期",
					 "partnership_end_date" => "有效日期",
					 "supplier_chairman" => "負責人",
					 "office_tel" => "電話",
					 "office_fax" => "傳真",
					 "addr_city_code" => "city",
					 "addr_town_code" => "town",
					 "addr1" => "住址",
					 "zipcode" => "zipcode",
					 "invoice_addr_city_code" => "city",
					 "invoice_addr_town_code" => "town",
					 "invoice_send_addr1" => "住址",
					 "invoice_send_zipcode" => "zipcode",
					 "supplier_chairman" => "公司負責人",
					 "supplier_company_title" => "公司抬頭",
					 "supplier_tax_id" => "統一編號",
					 "min_amount_for_noshipping" => "滿額免運金額",
					 "shipping_fee" => "運費",
					 "shipping_description" => "宅配說明",
					 "share_percent" => "分潤％數",
					 "brand" => "品牌名稱",
					 "precautions" => "注意事項",
				);

				/*

_REQUEST["domain_sn"]

Array
(
    [0] => 3
)


_REQUEST["confirm-password"]	123456

_REQUEST["account_name"]	戶名
_REQUEST["bank_name"]	銀行
_REQUEST["branch_name"]	分行
_REQUEST["bank_account"]	帳號


_REQUEST["status_1"]	1
_REQUEST["contact_name_1"]	聯絡人姓名1
_REQUEST["office_tel_1"]	電話1
_REQUEST["office_tel_ext_1"]	分機1
_REQUEST["cell_1"]	手機1
_REQUEST["email_1"]	Email 1
_REQUEST["memo_1"]	備註1


_REQUEST["status_2"]	1
_REQUEST["contact_name_2"]	聯絡人姓名2
_REQUEST["office_tel_2"]	電話 2
_REQUEST["office_tel_ext_2"]	分機2
_REQUEST["cell_2"]	手機2
_REQUEST["email_2"]	Email2
_REQUEST["memo_2"]	備註

_REQUEST["status_3"]	1
_REQUEST["contact_name_3"]	聯絡人姓名 3
_REQUEST["office_tel_3"]	電話 3
_REQUEST["office_tel_ext_3"]	分機3
_REQUEST["cell_3"]	手機3
_REQUEST["email_3"]	Email 3
_REQUEST["memo_3"]	備註


_REQUEST[""]	2


*/

		/* End 修改  */

    }

    //取得列表內容
    public function table_value_list( $_where,$_id = 0)
	{
		$_table_key=$this->_id;
	    $page_data['_body_result']="";
		//判斷是否有帶入特殊查詢
		if(strlen($_where)==0)
		{
			$_where="  order by apply_date asc";

		}
		// 取得列表
		$_v=" ".$this->_table.".*, concat(first_name,'',last_name) as ijtmp_name ";
		$_result01=$this->Sean_db_tools->db_get_max_record($_v,$this->_table,$_where);

		/* 列表標頭名稱 */
		$page_data['_table_thead_name']=array("姓名","Email","縣市","鄉鎮市區","住址","電話","結婚日期","關聯廠商","帳號狀態","功能");
		$page_data['_table_thead_field']=array("ijtmp_name","email","addr_state","addr_city","addr1","tel","wedding_date","member_level_type","member_status");


		/* 類表內容 */
		if(is_array($_result01))
		{
                 		$_i=0;
                 		foreach($_result01 as $key => $value)
                    	{


			                    $_body_result='';
						/* Begin 修改  */
			                    $i_key=0;
								foreach(	$page_data['_table_thead_field'] as $key => $_value)
								{
									$page_data['_body_result'][$_i][$i_key]=array("_value"=>$value->$_value,"_class"=>"");
			                    	$i_key++;
								}


						/* End 修改  */
									$page_data['_body_result'][$_i][$i_key]=array("_value"=>'
			                    		 		<a href="'.$this->init_control.$this->_url_item.'edit/'.$value->$_table_key.'"><i class="fa fa-edit"></i>編輯</a>&nbsp;&nbsp;
						                      <a href="'.$this->init_control.$this->_url_item.'delete/'.$value->$_table_key.'"><i class="fa fa-circle-o-notch"></i>重發驗證信</a>&nbsp;&nbsp;
											   <a href="'.$this->init_control.$this->_url_item.'delete/'.$value->$_table_key.'"><i class="fa fa-lightbulb-o"></i>忘記密碼&nbsp;&nbsp;
						                  	',"_class"=>'class="center"');
                				$_i++;


                			}

    				}
			return $page_data;

    }

	public function action_edit( $_where = "") {

		$_result=array();
		$_result=$this->Sean_db_tools->db_get_max_record("*",$this->_table,$_where);


		return $_result;

	}

	public function action_insert() {
			$_result=array();
			foreach($this->_table_field as $key => $value)
			{
					if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
					$_data[$key]=$this->input->get_post($key);
			}
			//var_dump($_data);
			//exit();
			//$_data["email"]=$_data["user_name"];
			//$_data["member_level_type"]=1;
			//$_data["member_level"]=1;
			//$_data["system_user_flag"]=0;
			if($_data['share_percent']){
				$_data['share_percent']=$_data['share_percent']/100;
			}
			$_data["addr_country_code"] ="886";
			$_data["add_state_code"] ="886";
			$_data["addr_country"] ="中華民國";
			$_data["addr_state"] ="台灣";

			$_data["invoice_addr_country_code"] ="886";
			$_data["invoice_add_state_code"] ="886";
			$_data["invoice_send_addr_country"] ="中華民國";
			$_data["invoice_send_addr_state"] ="台灣";
			//付款方式
			if($this->input->post('payment[]')){
				$_data['associated_payment_method']=implode(':',$this->input->post('payment[]'));
			}
			//basic insert
			$_data["create_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"0";
			$_data["create_date"]=date("Y-m-d H:i:s");
			//basic update and insert
			$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"0";
			$_data["last_time_update"]=date("Y-m-d H:i:s");

		 $page_data["_table"]=$this->_table;

		  //$_result=$this->Sean_db_tools->db_insert_record($_data,$this->_table);

			$_result=$this->Sean_db_toolss->db_insert_record($_data,$page_data);
			$_data=array();
			if($_result > 0 )
		  {
				  //init_bank_info_relation

					if(strlen($this->input->get_post("account_name")) > 0 )
					$_data["account_name"]=$this->input->get_post("account_name");

					if(strlen($this->input->get_post("bank_name")) > 0 )
					$_data["bank_name"]=$this->input->get_post("bank_name");
					if(strlen($this->input->get_post("bank_code")) > 0 )
					$_data["bank_name"]=$this->input->get_post("bank_code");

					if(strlen($this->input->get_post("branch_name")) > 0 )
					$_data["branch_name"]=$this->input->get_post("branch_name");
					if(strlen($this->input->get_post("branch_code")) > 0 )
					$_data["bank_name"]=$this->input->get_post("branch_code");

					if(strlen($this->input->get_post("bank_account")) > 0 )
					$_data["bank_account"]=$this->input->get_post("bank_account");

				  $_data["associated_supplier_sn"]=$_result;
				  $_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";

				  $_data["create_date"]=date("Y-m-d H:i:s");
				  $_data["status"]=1;

				  $this->insert_bank_info_relation($_data);

				  }
				  //insert supplier member
				  /*	 	$_data=array();
				  		if(strlen($this->input->get_post("password")) > 0 )
							$_data["password"]=md5($this->input->get_post("password"));

							if(strlen($this->input->get_post("user_name")) > 0 )
							$_data["user_name"]=$this->input->get_post("user_name");

							if(strlen($this->input->get_post("supplier_name")) > 0 )
							$_data["first_name"]=$this->input->get_post("supplier_name");


							$_data["member_level_type"]=3;
							$_data["system_user_flag"]=0;
							$_data["member_status"]=1;
							$_data["associated_supplier_sn"]=$_result;
							$_data["email"]=$_data["user_name"];
							$_data["last_name"]="";

									//basic update and insert
								$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
								$_data["last_time_update"]=date("Y-m-d H:i:s");


							$this->insert_member_relation($_data);
						*/



		return $_result;

	}

	function insert_member_relation($_data)
	{
					$page_data["_table"]="ij_member";
				  $_result=$this->Sean_db_toolss->db_insert_record($_data,$page_data);
	}
	function update_member_relation($_data,$_where)
	{
					$page_data["_table"]="ij_member";
					$_result=$this->Sean_db_toolss->db_update_record($_data,$_where,$page_data);
	}

	function init_contact_info_relation($_id)
	{
		$page_data["_table"]="ij_contact_info";
		$_where=" where associated_supplier_sn='".$_id."'";
		$_data["status"]=0;
		$_data["last_time_update"]=date("Y-m-d H:i:s");
		$_result=$this->Sean_db_toolss->db_update_record($_data,$_where,$page_data);

	}
	function insert_contact_info_relation($_data)
	{
					$page_data["_table"]="ij_contact_info";
				  return $this->Sean_db_toolss->db_insert_record($_data,$page_data);
	}

	function update_contact_info_relation($_data,$_where)
	{
					$page_data["_table"]="ij_contact_info";
					return $this->Sean_db_toolss->db_update_record($_data,$_where,$page_data);
	}




	function init_bank_info_relation($_id)
	{
		$page_data["_table"]="ij_bank_info";
		$_where=" where associated_supplier_sn='".$_id."'";
		$_data["status"]=0;
		$_data["last_time_update"]=date("Y-m-d H:i:s");
		$_result=$this->Sean_db_toolss->db_update_record($_data,$_where,$page_data);

	}

	function insert_bank_info_relation($_data)
	{
					$page_data["_table"]="ij_bank_info";
				  $_result=$this->Sean_db_toolss->db_insert_record($_data,$page_data);
	}

	function update_bank_info_relation($_data,$_where)
	{
					$page_data["_table"]="ij_bank_info";
					$_result=$this->Sean_db_toolss->db_update_record($_data,$_where,$page_data);
	}
	public function delete_sub($_key_id) {
				  $_data=array();
					if(strlen($this->input->get_post("contact_sn")) > 0 ){
				  		$_data["status"]=2;
						$page_data["_table"]="ij_contact_info";
						$_where=" where contact_sn='".$this->input->get_post("contact_sn")."'";
					}elseif(strlen($this->input->get_post("member_sn")) > 0 ){
				  	//$_data["status"]=2;
						//$page_data["_table"]="ij_contact_info";
						//$_where=" where member_sn='".$this->input->get_post("member_sn")."'";
					}
					$_result=$this->Sean_db_toolss->db_update_record($_data,$_where,$page_data);
					if($_result)
						$_return='刪除成功!';
					//else
						//$_return='刪除失敗!';
					return $_return;
	}

	public function bank_update($_key_id) {
				  $_data=array();
					if(strlen($this->input->get_post("account_name")) > 0 )
					$_data["account_name"]=$this->input->get_post("account_name");

					if(strlen($this->input->get_post("bank_name")) > 0 )
					$_data["bank_name"]=$this->input->get_post("bank_name");
					if(strlen($this->input->get_post("bank_code")) > 0 )
					$_data["bank_code"]=$this->input->get_post("bank_code");

					if(strlen($this->input->get_post("branch_name")) > 0 )
					$_data["branch_name"]=$this->input->get_post("branch_name");
					if(strlen($this->input->get_post("branch_code")) > 0 )
					$_data["branch_code"]=$this->input->get_post("branch_code");

					if(strlen($this->input->get_post("bank_account")) > 0 )
					$_data["bank_account"]=$this->input->get_post("bank_account");

				  $_data["last_time_update"]=date("Y-m-d H:i:s");
				  $_data["status"]=$this->input->get_post("bank_status");
			    $_data["update_member_sn"]=$this->session->userdata['member_data']['member_sn'];

					$_where=" where associated_supplier_sn='".$_key_id."' and status='1'";
					$_result_bank=$this->Sean_db_tools->db_get_one_record("associated_supplier_sn",'ij_bank_info',$_where);

				 if($_result_bank){ //判斷是否輸入過銀行資料且有效
					  $_where=" where associated_supplier_sn='".$_key_id."'";
				    return $this->update_bank_info_relation($_data,$_where);
				 }else{
				  $_data["associated_supplier_sn"]=$_key_id;
				  $_data["create_date"]=date("Y-m-d H:i:s");
				  $_data["update_member_sn"]=$this->session->userdata['member_data']['member_sn'];
				  $_data["status"]=1;
				  return $this->insert_bank_info_relation($_data);
				 }

	}
	public function contact_update($_key_id) {
				  $_data=array();
				  		if(strlen($this->input->get_post("contact_name")) > 0 )
							$_data["contact_name"]=$this->input->get_post("contact_name");

						  if(strlen($this->input->get_post("office_tel")) > 0 )
							$_data["office_tel"]=$this->input->get_post("office_tel");

							if(strlen($this->input->get_post("office_tel_ext")) > 0 )
							$_data["office_tel_ext"]=$this->input->get_post("office_tel_ext");

							if(strlen($this->input->get_post("cell")) > 0 )
							$_data["cell"]=$this->input->get_post("cell");

							if(strlen($this->input->get_post("email")) > 0 )
							$_data["email"]=$this->input->get_post("email");

							if(strlen($this->input->get_post("contact_type")) > 0 )
							$_data["contact_type"]=$this->input->get_post("contact_type");

						 if(strlen($this->input->get_post("status")) > 0 )
							$_data["status"]=$this->input->get_post("status");

						if(strlen($this->input->get_post("contact_sn")) > 0 )
							$_data["contact_sn"]=$this->input->get_post("contact_sn");

						  $_data["associated_supplier_sn"]=$_key_id;
						  $_data["create_member_sn"]=$this->session->userdata['member_data']['member_sn'];

						  $_data["create_date"]=date("Y-m-d H:i:s");

							if(isset($_data["contact_sn"])  && $_data["contact_sn"] > 0 )
							{
								$_where = " where contact_sn='".$_data["contact_sn"]."'";
								$_result=$this->update_contact_info_relation($_data,$_where);
								$_return='更新';
							}else{
				  			//$_data["status"]=1;
						  	$_result=$this->insert_contact_info_relation($_data);
								$_return='新增';
							}
					if($_result)
						$_return.='成功!';
					else
						$_return.='失敗!';
					return $_return;
	}
	public function action_update($_where,$_key_id,$_key) {
			$_result=array();
			$_data=array();
			//付款方式
			if($this->input->post('payment[]')){
				$_data['associated_payment_method']=implode(':',$this->input->post('payment[]'));
				//$_where=" where ".$_key."='".$_key_id."'";
				$_result=$this->action_edit($_where);
				//var_dump($_result[0]->associated_payment_method);
				if($_result[0]->associated_payment_method!=$_data['associated_payment_method']){
					$this->Sean_db_tools->db_update_record($_data,'ij_product'," where supplier_sn='$_key_id'");
				}
				//exit;
			}
			foreach($this->_table_field as $key => $value)
			{
				if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
				$_data[$key]=$this->input->get_post($key);
			}
			if(@$_data['share_percent']){
				$_data['share_percent']=$_data['share_percent']/100;
			}
			$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"0";
			$_data["last_time_update"]=date("Y-m-d H:i:s");

			$this->session->set_flashdata("message",$_data);
			//var_dump($_key_id);
			//var_dump($_key);
		$_result=$this->Sean_db_tools->db_update_record($_data,$this->_table,$_where,$_key_id,$_key);
		//echo $this->db->last_query().'<br>';
		$_data=array();
		if($_result > 0 )
		  {
				  //insert /update supplier member
				  /*$_data=array();
		  		if(strlen($this->input->get_post("password")) > 0 )
					$_data["password"]=md5($this->input->get_post("password"));
					if(strlen($this->input->get_post("user_name")) > 0 )
					$_data["user_name"]=$this->input->get_post("user_name");
					$_data["email"]=$_data["user_name"];
					if(strlen($this->input->get_post("supplier_name")) > 0 )
					$_data["first_name"]=$this->input->get_post("supplier_name");
					//basic update and insert
					$_data["update_member_sn"]=($this->session->userdata['member_data']['member_sn']) ? $this->session->userdata['member_data']['member_sn'] :"1";
					$_data["last_time_update"]=date("Y-m-d H:i:s");

					$_where=" where associated_supplier_sn='".$_result."'";
					$_result_member=$this->Sean_db_tools->db_get_one_record("associated_supplier_sn",'ij_member',$_where);
					if($_result_member){ //判斷是否輸入過對應會員帳號資料
					  $_where=" where associated_supplier_sn='".$_key_id."'";
					//var_dump($_data);
					//exit();
				    $this->update_member_relation($_data,$_where);
				 }else{
				 	//新增
							$_data["member_level_type"]=3;
							$_data["system_user_flag"]=0;
							$_data["member_status"]=1;
							$_data["associated_supplier_sn"]=$_result;
							$_data["last_name"]="";
				  $this->insert_member_relation($_data);
				 }*/


				  //init_bank_info_relation
					//$this->init_bank_info_relation($_result);




		/*多選才需要
		$domain_sn=$this->input->get_post('domain_sn');
		if($domain_sn){
		  $_where=" where associated_supplier_sn='".$_key_id."'"; //先把原本資料庫的status改成 2
			$_data_domain['status']='2';
			$page_data["_table"]="ij_member_domain_relation";
	    $this->Sean_db_toolss->db_update_record($_data_domain,$_where,$page_data);
			foreach($domain_sn as $key=>$Item){
				$_data1['associated_supplier_sn']=$_key_id;
				//$_data1['associated_member_sn']=$Item;
				$_data1['domain_sn']=$Item;
				$_data1['status']='1';
				$_data1=$this->Sean_db_tools->CheckUpdate($_data1,0); //因為沒有create_member_sn

				$_where=" where associated_supplier_sn='".$_key_id."' and domain_sn='".$Item."'";
				$_result_domain=$this->Sean_db_tools->db_get_one_record("associated_supplier_sn",'ij_member_domain_relation',$_where);
				if($_result_domain){ //判斷是否輸入過產業屬性資料
				  $_where=" where associated_supplier_sn='".$_key_id."' and domain_sn='".$Item."'";
			    $this->Sean_db_toolss->db_update_record($_data1,$_where,$page_data);
			 }else{
			  $_result1=$this->Sean_db_toolss->db_insert_record($_data1,$page_data);
			 }
			}
		}*/
					$this->session->set_flashdata("message",$_data);
			}
		return $_result;
	}

	public function action_delete($_where) {

		$_result=array();
		$page_data["_table"]=$this->_table;
		$_data["supplier_status"]=0;
		$_result=$this->Sean_db_toolss->db_update_record($_data,$_where,$page_data);
		return $_result;

	}

	//取得新排序的值
	public function get_next_sort($_id=0 ) {

		$_result="0";

		$_where=" where ".$this->_id." > 0 order by sort_order desc ";
		$_result=$this->Sean_db_tools->db_get_one_record("sort_order",$this->_table,$_where);

		if(is_array($_result["sort_order"]) )
		$_result=1;
		else
		$_result=$_result["sort_order"]+1;

		return $_result;

	}

	//取得定義參數值
	public function get_define_value($_id)
	{
		if(isset($this->$_id))
		return $this->$_id;
		else
		return "";
	}

	public function get_supplier_list($_table_where){
			//$_result=$this->Sean_db_tools->db_get_max_record("ij_supplier.*,email,user_name,ij_city_ct.city_name,ij_town_ct.town_name","ij_supplier left join ij_member on ij_supplier.supplier_sn=ij_member.associated_supplier_sn left join ij_city_ct on ij_city_ct.city_code=ij_supplier.addr_city_code left join ij_town_ct on ij_town_ct.town_code=ij_supplier.addr_town_code",$_table_where);
			$_result=$this->Sean_db_tools->db_get_max_record("ij_supplier.*,domain_name as domaintype,cooperate_mode_type_name,ij_city_ct.city_name,ij_town_ct.town_name","ij_supplier"," left join ij_city_ct on ij_city_ct.city_code=ij_supplier.addr_city_code left join ij_town_ct on ij_town_ct.town_code=ij_supplier.addr_town_code left join ij_biz_domain on ij_biz_domain.domain_sn=ij_supplier.main_domain_sn left join ij_cooperate_mode_type_ct on ij_cooperate_mode_type_ct.cooperate_mode_type=ij_supplier.cooperate_mode_type ".$_table_where);
			foreach($_result as $key=>$Item){
				$_where=" where supplier_sn='".$Item->supplier_sn."' and product_status !='9' ";
				$_result[$key]->product_count=$this->Sean_db_tools->db_get_one_record("count(*) as product_count",'ij_product',$_where)['product_count'];
				//窗口
				$_where=" where associated_supplier_sn='".$Item->supplier_sn."' and status='1' order by contact_sn ";
				$contact=$this->Sean_db_tools->db_get_one_record("*",'ij_contact_info',$_where);
				if($contact){
					$_result[$key]->contact_name=$contact['contact_name'];
					$_result[$key]->cell=$contact['cell'];
					$_result[$key]->email=$contact['email'];
				}else{
					$_result[$key]->contact_name='';
					$_result[$key]->cell='';
					$_result[$key]->email='';
				}

				//改單選
				/*$_where=" where associated_supplier_sn='".$Item->supplier_sn."' and ij_member_domain_relation.status =1 ";
				$domaintypes=$this->Sean_db_tools->db_get_max_record("domain_name","ij_member_domain_relation left join ij_biz_domain on ij_biz_domain.domain_sn=ij_member_domain_relation.domain_sn",$_where);
				if($domaintypes){
					$domain_name='';
	        foreach($domaintypes as $row){
	        	$domain_name[]=$row->domain_name;
	        }
					$_result[$key]->domaintype=implode('、',$domain_name);
				}else{
					$_result[$key]->domaintype='';
				}*/
			}
		return $_result;
	}
}


function add_img(){
		$this->sourceFolder = FCPATH."public/uploads/supplier/";
		$this->sourceUrl = "public/uploads/supplier/";
		$result = array('success' => 'N', 'msg' => '', 'img' => '');

		$files = $_FILES;
		$originalFile = empty($files['Filedata']['tmp_name'])? $files['Filedata']['name']:$files['Filedata']['tmp_name'];
		$tmp = explode(".", $files['Filedata']['name']);
		$ext = ".".$tmp[COUNT($tmp)-1];
		$thumb = uniqid();

		// Validate the file type
		$fileTypes = array('jpg','jpeg','gif','png'); // File extensions
		$fileParts = pathinfo(empty($files['Filedata']['name'])? $files['upload']['name']:$files['Filedata']['name']);
		$destFile=$this->sourceFolder.$this->supplier_sn.'/'.$thumb.$ext;
		if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
			if(move_uploaded_file($originalFile,$destFile)){

					$result['success'] = 'Y';
					$result['img'] = $this->config->item('base_url').'/'.$this->sourceUrl.'/'.$this->_id.'/'.$thumb.$ext;

			}else{
				$result['msg'] = 'move_uploaded_file faile';
			}
		} else {
			$result['msg'] = 'Invalid file type.';
		}
		return $result;
	}