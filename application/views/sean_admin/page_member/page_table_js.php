<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>


<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script>
jQuery(document).ready(function() {
   	Metronic.init(); // init metronic core components
		Layout.init(); // init current layout
    $(".select2_dealer").select2({
        tags: ["婚紗1", "婚紗2", "婚紗3", "婚紗4", "婚紗5"]
    });
    $(".select3_dealer").select2({
        tags: ["典華1", "典華2", "典華3", "典華4", "典華5"]
    });
     $('.date-picker').datepicker({
        rtl: Metronic.isRTL(),
        format: "yyyy-mm-dd",
        orientation: "left",
        autoclose: true
    });


    /* Action On Select Box Change */
    $('select[id="addr_city_code"]').change(function()
    {
        var data = $(this).val(); // Get Selected Value
		    $.ajax({
		        url: 'admin/member/package_addr_more',
		        data: 'data=' + data,
		        dataType: 'json',
		        success: function(data) {
		         	$('#addr_town_code').empty();
					$(data).appendTo("#addr_town_code");
		        }
		    });
		});
    /* Action On Select Box Change */

	var initTableMember = function () {
        var table = $('#table_member');
        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": 依此欄位做順向排序",
                    "sortDescending": ": 依此欄位做逆向排序"
                },
                "emptyTable": "沒有資料可以顯示",
                "info": "顯示 _TOTAL_ 中的第 _START_ 到 _END_ 筆",
                "infoEmpty": "查無結果",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "顯示 _MENU_ 項目",
                "search": "搜尋:",
                "zeroRecords": "查無結果"
            },

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "columns": [{
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": false
            }],
            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,
            "pagingType": "bootstrap_full_number",
            "language": {
                "search": "快速搜尋: ",
                "lengthMenu": "  _MENU_ 顯示",
                "paginate": {
                    "previous":"上一頁",
                    "next": "下一頁",
                    "last": "最後",
                    "first": "最前"
                }
            },
            "columnDefs": [{  // set default column settings
                'orderable': false,
                'targets': [0]
            }, {
                "searchable": false,
                "targets": [0]
            }],
            "order": [
                //[1, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = jQuery('#table_member_wrapper');

        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('tr').addClass("active");
                } else {
                    $(this).attr("checked", false);
                    $(this).parents('tr').removeClass("active");
                }
            });
            jQuery.uniform.update(set);
        });

        table.on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });

        tableWrapper.find('.dataTables_length select').addClass("form-control input-xsmall input-inline"); // modify table per page dropdown
    }
    initTableMember();
});
</script>

<script>

  // When the browser is ready...
  $(function() {
	jQuery.validator.addMethod("mobileTaiwan", function( value, element ) {
	var str = value;
	var result = false;
	if(str.length > 0){
		//是否只有數字;
		var patt_mobile = /^[\d]{1,}$/;
		result = patt_mobile.test(str);

		if(result){
			//檢查前兩個字是否為 09
			//檢查前四個字是否為 8869
			var firstTwo = str.substr(0,2);
			var firstFour = str.substr(0,4);
			var afterFour = str.substr(4,str.length-1);
			if(firstFour == '8869'){
				$(element).val('09'+afterFour);
				if(afterFour.length == 8){
					result = true;
				} else {
					result = false;
				}
			} else if(firstTwo == '09'){
				if(str.length == 10){
					result = true;
				} else {
					result = false;
				}
			} else {
				result = false;
			}
		}
	} else {
		result = true;
	}
	return result;
}, "手機號碼不符合格式，僅允許09開頭的10碼數字");

    // Setup form validation on the #register-form element
    $("#register-form").validate({

        // Specify the validation rules
        rules: {
            last_name: "required",
            lastname: "required",
            cell:{
						required:true,
						mobileTaiwan:true
						},
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6
            },
             confirm_password: {
                required: true,
                equalTo: "#password",
                minlength: 6
            },

            member_agree: "required"
        },

        // Specify the validation error messages
        messages: {
            last_name: "請輸入姓！",
            first_name: "請輸入名！",
            cell:{
						required: "請輸入您的聯繫手機",
						mobileTaiwan: "請輸入一個有效的聯繫電話"
						},
            password: {
                required: "請輸入密碼",
                minlength: "密碼最小長度為6"
            },
            confirm_password: {
                required: "請輸入密碼",
                minlength: "密碼最小長度為6"
            },
            email: "請輸入正確的email格式！",
            member_agree: "請確認會員條款已閱讀和打勾"
        },

        submitHandler: function(form) {
            form.submit();
        }


    });

  });

  </script>
