<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* for admin console template preview
*/
class Content extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('libraries_model');
		$this->page_data['unreadmessage']= $this->libraries_model->get_unread_message_count();
		$this->load->library("ijw");
		$data['Mes'] = $this->session->userdata('Mes');
		if($data['Mes']){$this->load->view('Message',$data);$this->session->unset_userdata('Mes');}
		$this->LoginCheck();
		}

	/**
	 * 內容分類列表 Template
	 *
	 */
	public function contentCategory()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['Items']=$this->libraries_model->_select('ij_content_type_config','status','1',0,0,0,0,'result_array');
		$page_data['page_content']['view_path'] = 'admin/page_content/category/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_content/category/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 內容分類編輯 Template
	 *
	 */
	public function contentCategoryItem($content_type_sn=0,$blog_faq_flag=0)
	{
		if(!$content_type_sn){
			$this->Check_page_permission('add','新增');
		}else{
			$this->Check_page_permission('search','查詢');
		}
		if($data_array=$this->input->post('ctype')){
			//var_dump($data_array);exit;
			if(!$content_type_sn=$this->input->post('content_type_sn')){ //新增
				$this->Check_page_permission('add','新增');
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);//方便秀最後更新
				$data_array['status']='1';
				if(@$data_array['post_channel_name_set']){
					$data_array['post_channel_name_set']=implode('、',$data_array['post_channel_name_set']);
				}
				$content_type_sn=$this->libraries_model->_insert('ij_content_type_config',$data_array);
				$this->session->set_userdata(array('Mes'=>'新增成功!'));
				redirect(base_url('admin/content/contentCategoryItem/0/'.$data_array['blog_faq_flag']));
			}else{
				$this->Check_page_permission('edit','修改');
				//有改變名稱
				if($this->input->post('old_content_title')!=$data_array['content_title']){
					$where=array('content_title'=>$data_array['content_title'],'blog_faq_flag'=>$data_array['blog_faq_flag'],'content_type_sn !='=>$content_type_sn);
					if($this->libraries_model->_select('ij_content_type_config',$where,0,0,0,0,0,'num_rows')){
						$this->session->set_userdata(array('Mes'=>'很抱歉！已有相同名稱，修改失敗！'));
						redirect(base_url('admin/content/contentCategoryItem/'.$content_type_sn));
					}else{
					//異動faq type
						$up_data_array['associated_content_type_sn_set']=$data_array['content_title'];
				    $up_data_array = $this->libraries_model->CheckUpdate($up_data_array,0);
						$this->libraries_model->_update('ij_faq',$up_data_array,'associated_content_type_sn_set',$this->input->post('old_content_title'));
					}
				}
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				if(@$data_array['post_channel_name_set']){
					$data_array['post_channel_name_set']=implode('、',$data_array['post_channel_name_set']);
				}
				$this->libraries_model->_update('ij_content_type_config',$data_array,'content_type_sn',$content_type_sn);
				$this->session->set_userdata(array('Mes'=>'修改成功!'));
				redirect(base_url('admin/content/contentCategory/'));
			}
		}
		$page_data = array();

		// 內容設定

		$Store=$this->libraries_model->get_channels2();

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['Store'] = $Store;
		$page_data['page_content']['Item']=$this->libraries_model->_select('ij_content_type_config','content_type_sn',$content_type_sn,0,0,0,0,'row');
		$page_data['page_content']['blog_faq_flag'] = $blog_faq_flag;

		$page_data['page_content']['view_path'] = 'admin/page_content/category/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_content/category/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 部落格列表 Template
	 *
	 */
	public function contentBlog()
	{
		$flg=$this->input->get('flg',true);
		if($flg=='reset'){
			redirect(base_url('admin/content/contentBlog'));
			exit();
		}else{
			$content_type_sn=$this->input->get('content_type_sn',true);
			$post_flag=$this->input->get('post_flag',true);
		}
		$page_data = array();
		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		$where=array('blog_faq_flag'=>'1','post_flag'=>'1'); //1:blog
		$page_data['page_content']['content_types']=$this->libraries_model->_select('ij_content_type_config',$where,0,0,0,0,0,'result_array');

		//$where=array('blog_faq_flag'=>'1'); //1:blog
		if($post_flag=='9') {
			$where2['status']='2';
		}elseif($post_flag){
			$where2['status']='1';
			$where2['post_flag']=$post_flag;
		}else{
			$where2['status']='1';
		}
		if($content_type_sn) {
			//$like_field='associated_content_type_sn';
			$where2['ij_blog_type_relation.content_type_sn']=$content_type_sn;
			$where2['ij_blog_type_relation.relation_status']='1';
		}
		$like_field='0';

		$page_data['page_content']['view_path'] = 'admin/page_content/blog/table'; // 頁面主內容 view 位置, 必要
		//$page_data['page_content']['Items']=$this->libraries_model->_select('ij_blog_article',$where2,0,0,0,0,0,'result_array',0,@$content_type_sn);
		$page_data['page_content']['Items']=$this->libraries_model->_get_blog_faq('ij_blog_article',$where2,$like_field,$content_type_sn);
		$page_data['page_content']['content_type_sn']=$content_type_sn;
		$page_data['page_content']['post_flag']=$post_flag;
		//echo $this->db->last_query();
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_content/blog/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 部落格編輯 Template
	 *
	 */
	public function contentBlogItem($blog_article_sn=0)
	{
		if($data_array=$this->input->post('Item')){
			//var_dump($this->input->post());exit;
			if(!$blog_article_sn=$this->input->post('blog_article_sn')){ //新增
				$this->Check_page_permission('add','新增');
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);//方便秀最後更新
				$data_array['status']='1';
				//if(@$data_array['associated_content_type_sn']){
					//$data_array['associated_content_type_sn']=implode('、',$data_array['associated_content_type_sn']);
				//}
				if(@$data_array['post_category_sn_set']) $data_array['post_category_sn_set']=implode('、',$data_array['post_category_sn_set']);
				$data_array['post_channel_name_set']='囍市集';
		        if(@$this->session->userdata('member_role_system_rules')[0]['role_name']){
		            switch(@$this->session->userdata('member_role_system_rules')[0]['role_name']){
		            case '經銷商管理員':
        				$data_array['associated_dealer_sn']=@$this->session->userdata['member_data']['associated_dealer_sn'];
		            break;
		            case '供應商管理員':
        				$data_array['associated_supplier_sn']=@$this->session->userdata['member_data']['associated_supplier_sn'];
		            break;
		            default:
		            break;
		            }
		        }
				$blog_article_sn=$this->libraries_model->_insert('ij_blog_article',$data_array);
				$blog_type=$this->input->post('blog_type');
        //echo '<pre>' . var_export($blog_type, true) . '</pre>';exit;
				if(is_array($blog_type)){
    				foreach($blog_type as $_item){
    					$relation_status=(isset($_item['relation_status']))?'1':'0';
						if(isset($_item['blog_type_sn']) && $_item['blog_type_sn']){
							$this->libraries_model->_update('ij_blog_type_relation',array('relation_status'=>$relation_status),'blog_type_sn',$_item['blog_type_sn']);
						}else{
							$blog_type_array=array('blog_article_sn'=>$blog_article_sn,'content_type_sn'=>$_item['content_type_sn'],'relation_status'=>$relation_status);
							$this->libraries_model->_insert('ij_blog_type_relation',$blog_type_array);
						}
    				}
				}
				if(@$this->session->userdata['member_data']['associated_dealer_sn']){
                	$this->libraries_model->_insert('ij_blog_relation',array('blog_article_sn'=>$blog_article_sn,'dealer_sn'=>@$this->session->userdata['member_data']['associated_dealer_sn'],'relation_status'=>'1'));
                }
				$this->session->set_userdata(array('Mes'=>'新增成功!'));
			}else{
				$this->Check_page_permission('edit','修改');
	            switch(@$this->session->userdata('member_role_system_rules')[0]['role_name']){
	            case '經銷商管理員':
    				//unset($data_array['post_flag']);
	            	$_data['relation_status']=$data_array['post_flag'];
					$this->libraries_model->_update('ij_blog_relation',$_data,array('blog_article_sn'=>$blog_article_sn,'dealer_sn'=>$this->session->userdata['member_data']['associated_dealer_sn']));
	            break;
	            case '供應商管理員':
    				//$data_array['associated_supplier_sn']=@$this->session->userdata['member_data']['associated_supplier_sn'];
	            break;
	            default:
					$data_array = $this->libraries_model->CheckUpdate($data_array,0);
					$blog_type=$this->input->post('blog_type');
					if(is_array($blog_type)){
						//$data_array['associated_content_type_sn']=implode('、',$data_array['associated_content_type_sn']);
						//$this->libraries_model->_update('ij_blog_type_relation',array('relation_status'=>'0'),'blog_article_sn',$blog_article_sn);
        				foreach($blog_type as $_item){
        					$relation_status=(isset($_item['relation_status']))?'1':'0';
    						if(isset($_item['blog_type_sn']) && $_item['blog_type_sn']){
								$this->libraries_model->_update('ij_blog_type_relation',array('relation_status'=>$relation_status),'blog_type_sn',$_item['blog_type_sn']);
    						}else{
    							$blog_type_array=array('blog_article_sn'=>$blog_article_sn,'content_type_sn'=>$_item['content_type_sn'],'relation_status'=>$relation_status);
								$this->libraries_model->_insert('ij_blog_type_relation',$blog_type_array);
    						}
        				}
					}
					if(@$data_array['post_category_sn_set']) $data_array['post_category_sn_set']=implode('、',$data_array['post_category_sn_set']);
					//var_dump($data_array);
					$this->libraries_model->_update('ij_blog_article',$data_array,'blog_article_sn',$blog_article_sn);
	            break;
	            }
				$this->session->set_userdata(array('Mes'=>'修改成功!'));
				//redirect(base_url('admin/content/contentBlog/'));
				$blog_article_image_save_dir=$this->input->post('blog_article_image_save_dir');
			}
      if (!empty($_FILES['banner']['name'])) {
					//$this->load->library('upload');
          $config['upload_path']  = ADMINUPLOADPATH.'blog/' ;
          $config['allowed_types']= 'gif|jpg|png|jpeg';
          $config['max_size']     = '4196';
          $config['max_width']    = '1920';
          $config['max_height']   = '1920';
          $config['encrypt_name']   = true;

          $this->load->library('upload', $config);

					//$this->upload->initialize($config);
          if (!$this->upload->do_upload('banner')) {
              //$error = array('error' => $this->upload->display_errors());
							$this->session->set_userdata(array('Mes'=>$this->session->userdata('Mes').' '.strip_tags($this->upload->display_errors())));
          		//var_dump($error);
					}else{
              $img	 = $this->upload->data();
							$this->libraries_model->_update('ij_blog_article',array('blog_article_image_save_dir'=>'blog/'.$img['file_name']),'blog_article_sn',$blog_article_sn);
							//var_dump($img);
              if(is_file(ADMINUPLOADPATH.$blog_article_image_save_dir)) unlink(ADMINUPLOADPATH.$blog_article_image_save_dir);
          }
          //exit();
      }
			redirect(base_url('admin/content/contentBlogItem/'.$blog_article_sn));
		}
		$page_data = array();

		// 內容設定
		$where=array('status'=>'1','blog_faq_flag'=>'1','post_flag'=>'1'); //1:blog
		if(!$blog_article_sn){
			$this->Check_page_permission('add','新增');
			$content_types=$this->libraries_model->get_content_type($where);
		}else{
			$this->Check_page_permission('search','查詢');
			$content_types=$this->libraries_model->get_content_type($where,$blog_article_sn);
		}
		//echo $this->libraries_model->get_content_type_string($blog_article_sn);
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		//$page_data['page_content']['content_types']=$this->libraries_model->_select('ij_content_type_config',$where,0,0,0,0,0,'result_array');
		$page_data['page_content']['content_types']=$content_types;
		//var_dump($page_data['page_content']['content_types']);

		$page_data['page_content']['Item']=$this->libraries_model->_select('ij_blog_article','blog_article_sn',$blog_article_sn,0,0,0,0,'row');
        if(@$this->session->userdata('member_role_system_rules')[0]['role_name']){
            switch(@$this->session->userdata('member_role_system_rules')[0]['role_name']){
            case '經銷商管理員': //取代是否發布
				$page_data['page_content']['Item']['post_flag']=$this->libraries_model->_select('ij_blog_relation',array('blog_article_sn'=>$blog_article_sn,'dealer_sn'=>@$this->session->userdata['member_data']['associated_dealer_sn']),0,0,0,'blog_article_sn',0,'row')['relation_status'];
            break;
            case '供應商管理員':
				//$data_array['associated_supplier_sn']=@$this->session->userdata['member_data']['associated_supplier_sn'];
            break;
            default:
            break;
            }
        }
		//var_dump($page_data['page_content']['Item']);exit;
		if($blog_article_sn && !$page_data['page_content']['Item']){
			$this->session->set_userdata(array('Mes'=>'很抱歉！查無該文章或該文章不是您發表的!'));
			redirect(base_url('admin/content/contentBlog/'));
		}
		$post_categorys = explode('、',@$page_data['page_content']['Item']['post_category_sn_set']);
		foreach($post_categorys as $_Item){
			$post_category[$_Item]=$_Item;
		}
		$page_data['page_content']['categorys']=form_dropdown('post_category_sn_set',array(''=>'請先選擇館別'),'','id="Categorys" class="form-control" size="10"');
		$Store=$this->libraries_model->get_channels();

		$page_data['page_content']['Store']=form_dropdown('Item[post_channel_name_set]',$Store,@$page_data['page_content']['Item']['post_channel_name_set'],'id="StoreName" class="form-control" onChange="ChgStoreName(this.value);"');
		//已選分類
		$page_data['page_content']['categorys2']=form_multiselect('Item[post_category_sn_set][]',$post_category,$post_category,'id="Categorys2" class="form-control" multiple size="5"');

		$page_data['page_content']['view_path'] = 'admin/page_content/blog/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_content/blog/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * FAQ列表 Template
	 *
	 */
	public function contentFaq()
	{
		$flg=$this->input->get('flg',true);
		if($flg=='reset'){
			redirect(base_url('admin/content/contentFaq'));
			exit();
		}else{
			$content_type_sn=$this->input->get('content_type_sn',true);
			$post_flag=$this->input->get('post_flag',true);
			$post_channel_name=$this->input->get('post_channel_name',true);
		}
		$page_data = array();
		// 內容設定

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		//$where=array('blog_faq_flag'=>'1'); //1:blog
		if($post_flag=='9') {
			$where2['ij_faq.status']='2';
		}elseif($post_flag){
			$where2['ij_faq.status']='1';
			$where2['ij_faq.post_flag']=$post_flag;
		}else{
			$where2['ij_faq.status']='1';
		}
		if($post_channel_name) {
			$where2['ij_faq.post_channel_name_set']=$post_channel_name;
		}
		if($content_type_sn) {
			$where2['ij_faq.associated_content_type_sn_set']=$content_type_sn;
			//$like_field='associated_content_type_sn_set';
		}else{
			$like_field='0';
		}
		$like_field='0';
		//var_dump($where2);
		$page_data['page_content']['Items']=$this->libraries_model->_get_blog_faq('ij_faq',$where2,$like_field,$content_type_sn);
		//$Store=$this->libraries_model->get_channels('首頁');
		if($this->session->userdata('member_role_system_rules')[0]['role_name']=='經銷商管理員' || $this->session->userdata('member_role_system_rules')[0]['role_name']=='供應商管理員'){
			$page_data['page_content']['Store']='';
			$where=array('status'=>'1','blog_faq_flag'=>'2','post_flag'=>'1'); //1:blog
			if($this->session->userdata('member_role_system_rules')[0]['role_name']=='經銷商管理員'){
				$post_channel_name='經銷商';
			}else{
				$post_channel_name='供應商';
			}
			$where['post_channel_name_set']=$post_channel_name;
			$page_data['page_content']['content_types']=$this->libraries_model->_select('ij_content_type_config',$where,0,0,0,0,0,'result_array');
			//echo $this->db->last_query();
		}else{
			$where=array('status'=>'1','blog_faq_flag'=>'2','post_flag'=>'1'); //1:blog
			$page_data['page_content']['content_types']=$this->libraries_model->_select('ij_content_type_config',$where,0,0,0,0,0,'result_array');
			//var_dump($page_data['page_content']['content_types']);
			$Store=$this->libraries_model->get_channels2();
			$page_data['page_content']['Store']=form_dropdown('post_channel_name',$Store,$post_channel_name,'id="StoreName" class="table-group-action-input form-control input-inline input-small input-sm" onChange="ChgChanelName(this.value);"');
		}
		$page_data['page_content']['content_type_sn']=$content_type_sn;
		$page_data['page_content']['post_flag']=$post_flag;
		$page_data['page_content']['post_channel_name']=$post_channel_name;
		$page_data['page_content']['view_path'] = 'admin/page_content/faq/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_content/faq/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 部落格編輯 Template
	 *
	 */
	public function contentFaqItem($faq_sn=0)
	{
		if(!$faq_sn){
			$this->Check_page_permission('add','新增');
		}else{
			$this->Check_page_permission('search','查詢');
		}
		if($data_array=$this->input->post('Item')){
			//var_dump($data_array);
			if(!$faq_sn=$this->input->post('faq_sn')){ //新增
				$this->Check_page_permission('add','新增');
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);//方便秀最後更新
				$data_array['status']='1';
				//$data_array['associated_content_type_sn_set']=implode('、',$data_array['associated_content_type_sn_set']);
				if($data_array['post_category_sn_set']){
					$data_array['post_category_sn_set']=implode('、',$data_array['post_category_sn_set']);
				}
				$faq_sn=$this->libraries_model->_insert('ij_faq',$data_array);
				$this->session->set_userdata(array('Mes'=>'新增成功!'));
			}else{
				$this->Check_page_permission('edit','修改');
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				//$data_array['associated_content_type_sn_set']=implode('、',$data_array['associated_content_type_sn_set']);
				if(@$data_array['post_category_sn_set']){
					$data_array['post_category_sn_set']=implode('、',$data_array['post_category_sn_set']);
				}else{
					$data_array['post_category_sn_set']='';
				}
				//var_dump(@$data_array['post_category_sn_set']);exit;
				$this->libraries_model->_update('ij_faq',$data_array,'faq_sn',$faq_sn);
				$this->session->set_userdata(array('Mes'=>'修改成功!'));
				//redirect(base_url('admin/content/contentBlog/'));
			}
			redirect(base_url('admin/content/contentFaqItem/'.$faq_sn));
		}

		$page_data = array();

		// 內容設定

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		$page_data['page_content']['Item']=$this->libraries_model->_select('ij_faq','faq_sn',$faq_sn,0,0,0,0,'row');

		if(!$page_data['page_content']['Item']){
			$page_data['page_content']['Item']['post_channel_name_set']='囍市集';
		}
		$where=array('status'=>'1','blog_faq_flag'=>'2','post_flag'=>'1'); //2:faq
		$where['post_channel_name_set']=@$page_data['page_content']['Item']['post_channel_name_set'];
		$content_types=$this->libraries_model->_select('ij_content_type_config',$where,0,0,0,'post_channel_name_set,sort_order',0,'list','content_type_sn,content_title');

		$page_data['page_content']['categorys']=form_dropdown('Item[post_category_sn_set][]',array('0'=>'請先選擇館別'),'','id="Categorys" class="form-control" multiple size="10"');
		$Store=$this->libraries_model->get_channels2();
		$page_data['page_content']['Store']=form_dropdown('Item[post_channel_name_set]',$Store,$page_data['page_content']['Item']['post_channel_name_set'],'id="StoreName" class="form-control" onChange="ChgChanelName(this.value);"');
		$content_types[0]='請先選擇頻道';
		$page_data['page_content']['content_types_array']=form_dropdown('Item[associated_content_type_sn_set]',$content_types,@$page_data['page_content']['Item']['associated_content_type_sn_set'],'id="content_types_sn" class="form-control"');
		$page_data['page_content']['view_path'] = 'admin/page_content/faq/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_content/faq/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}
}