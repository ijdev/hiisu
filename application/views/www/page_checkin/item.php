<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Home 主內容
*/
?>

<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<link href="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="public/metronic/global/plugins/slider-layer-slider/css/layerslider.css" rel="stylesheet">
<!-- Page level plugin styles END -->

<div class="container">
  <div class="row service-box margin-bottom-40" id="section-service">
    <h1 style="margin-bottom:160px;">功能介紹</h1>
  </div>
  <div class="row service-box margin-bottom-40" id="section-price">
    <h1>價格說明</h1>
    <div class="content-page">
      <div class="row margin-bottom-40">
        <!-- Pricing -->
        <div class="col-md-3">
          <div class="pricing hover-effect">
            <div class="pricing-head">
              <h3>簡易款
              <span>
                 Officia deserunt mollitia
              </span>
              </h3>
              <h4><i>$</i>5<i>.49</i>
              <span>
                 Per Month
              </span>
              </h4>
            </div>
            <ul class="pricing-content list-unstyled">
              <li>
                <i class="fa fa-tags"></i> At vero eos
              </li>
              <li>
                <i class="fa fa-asterisk"></i> No Support
              </li>
              <li>
                <i class="fa fa-heart"></i> Fusce condimentum
              </li>
              <li>
                <i class="fa fa-star"></i> Ut non libero
              </li>
              <li>
                <i class="fa fa-shopping-cart"></i> Consecte adiping elit
              </li>
            </ul>
            <div class="pricing-footer">
              <p>
                 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
              </p>
              <a href="javascript:;" class="btn btn-primary">
                 Sign Up <i class="m-icon-swapright m-icon-white"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="pricing hover-effect">
            <div class="pricing-head">
              <h3>基本款
              <span>
                 Officia deserunt mollitia
              </span>
              </h3>
              <h4><i>$</i>8<i>.69</i>
              <span>
                 Per Month
              </span>
              </h4>
            </div>
            <ul class="pricing-content list-unstyled">
              <li>
                <i class="fa fa-tags"></i> At vero eos
              </li>
              <li>
                <i class="fa fa-asterisk"></i> No Support
              </li>
              <li>
                <i class="fa fa-heart"></i> Fusce condimentum
              </li>
              <li>
                <i class="fa fa-star"></i> Ut non libero
              </li>
              <li>
                <i class="fa fa-shopping-cart"></i> Consecte adiping elit
              </li>
            </ul>
            <div class="pricing-footer">
              <p>
                 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
              </p>
              <a href="javascript:;" class="btn btn-primary">
                 Sign Up <i class="m-icon-swapright m-icon-white"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="pricing hover-effect">
            <div class="pricing-head">
              <h3>豪華款
              <span>
                 Officia deserunt mollitia
              </span>
              </h3>
              <h4><i>$</i>13<i>.99</i>
              <span>
                 Per Month
              </span>
              </h4>
            </div>
            <ul class="pricing-content list-unstyled">
              <li>
                <i class="fa fa-tags"></i> At vero eos
              </li>
              <li>
                <i class="fa fa-asterisk"></i> No Support
              </li>
              <li>
                <i class="fa fa-heart"></i> Fusce condimentum
              </li>
              <li>
                <i class="fa fa-star"></i> Ut non libero
              </li>
              <li>
                <i class="fa fa-shopping-cart"></i> Consecte adiping elit
              </li>
            </ul>
            <div class="pricing-footer">
              <p>
                 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
              </p>
              <a href="javascript:;" class="btn btn-primary">
                 Sign Up <i class="m-icon-swapright m-icon-white"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="pricing hover-effect">
            <div class="pricing-head">
              <h3>旗艦款
              <span>
                 Officia deserunt mollitia
              </span>
              </h3>
              <h4><i>$</i>99<i>.00</i>
              <span>
                 Per Month
              </span>
              </h4>
            </div>
            <ul class="pricing-content list-unstyled">
              <li>
                <i class="fa fa-tags"></i> At vero eos
              </li>
              <li>
                <i class="fa fa-asterisk"></i> No Support
              </li>
              <li>
                <i class="fa fa-heart"></i> Fusce condimentum
              </li>
              <li>
                <i class="fa fa-star"></i> Ut non libero
              </li>
              <li>
                <i class="fa fa-shopping-cart"></i> Consecte adiping elit
              </li>
            </ul>
            <div class="pricing-footer">
              <p>
                 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
              </p>
              <a href="javascript:;" class="btn btn-primary">
                 Sign Up <i class="m-icon-swapright m-icon-white"></i>
              </a>
            </div>
          </div>
        </div>
        <!--//End Pricing -->
      </div>
    </div>
  </div>
  <div class="row service-box margin-bottom-40" id="section-demo">
    <h1 style="margin-bottom:160px;">畫面展示</h1>
  </div>
  <div class="row service-box" id="section-related">
    <h1>周邊商品</h1>
    <div class="owl-carousel owl-carousel5">
      <?php for($i=0;$i<6;$i++):?>
        <div>
          <div class="product-item">
            <div class="pi-img-wrapper">
              <img src="public/metronic/frontend/pages/img/products/k4.jpg" class="img-responsive" alt="Berry Lace Dress">
            </div>
            <h3><a href="shop-item.html">Berry Lace Dress</a></h3>
            <a href="#" class="btn btn-default add2cart">前往選購</a>
          </div>
        </div>
      <?php endfor;?>
    </div>
  </div>
</div>