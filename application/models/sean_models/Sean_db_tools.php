<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
     *
     * Database_tools_model: contains generic functions, used in several controllers
     *
     */

class Sean_db_tools extends CI_Model
{

    public function __construct() {
        parent::__construct();
      /* load 資料庫 */
      $this->load->database();
    }


		public function db_message_handle($_data)
		{

				switch($_data)
				{
					case (preg_match("/\foreign key\b/i", $_data)):
						return  "資料已在其他功能引用,請先刪除其他引用的資料。";
						break;
					case '2string':
						return  "this is a string";
						break;
				}
				return $_data;
		}
		public function db_error_debug($error)
		{

				if($this->config->item("sys_debug")=="true")
				$this->session->set_flashdata("message",$error);

		}
		public function db_insert_record($_data,$_table)
		{
				if ( ! $this->db->insert($_table, $_data))
				{
							$error = $this->db->error(); // Has keys 'code' and 'message'
							array_push($error,$_data);
							if($this->config->item("sys_debug")=="true")
							$this->session->set_flashdata("message",$error);
							$this->db_error_debug($error);
							return $error;
				}else{
					return $this->db->insert_id();
				}

		}

		public function db_update_record($_data,$_table,$_where,$_key_id='',$_key='')
		{
				$_set="";
				$_next_add=0;
				$_set_after="";
				$_next_add=0;
				foreach($_data as $key => $value)
				{
					if(strlen($value) > 0 && strlen($key) > 0  )
					{
						if(strlen($_set) > 0  && $_next_add==1)
						{
							$_set.=",";
							$_set_after.=",";
						}
						$_set.=" ".$key."='".$value."'";
						$_set_after.="[".$key."='".$value."']";
						$_next_add=1;
					}

				}
				//trans log
				//$_where = " where ".$_key." ='".$_key_id."'";
				//$_trans_log["_table"]=$_table;
				//$_trans_log["_before"]=$this->db_get_move_record($_data,$_table,$_where);
				//$_trans_log["_after"]=$_set_after;//$this->after_trans_column_set(1,1);
				//$_trans_log["_type"]="1";
				//$_trans_log["_key_id"]=$_key_id;
				//$this->trans_log($_trans_log);

					$action_one="update $_table set $_set $_where";
					//echo $action_one;
					$this->session->set_flashdata("one_message",$action_one);

					if ( ! $this->db->simple_query($action_one))
					{
						$error = $this->db->error(); // Has keys 'code' and 'message'
						array_push($error,$action_one);
						$this->db_error_debug($error);
						if($this->config->item("sys_debug")=="true")
						$this->session->set_flashdata("message",$error);
						return $error;
					}else{
						$this->session->set_flashdata("one_message",$action_one);
						return true;
					}

		}

		public function db_delete_record($_table,$_where,$_key_id='',$_key='')
		{
					$action_one="delete from $_table $_where";

					$_trans_log["_table"]=$_table;
					$_trans_log["_before"]=$_where;//$this->beforetrans_column_log(1,1);
					$_trans_log["_after"]="";//$this->after_trans_column_set(1,1);
					$_trans_log["_type"]="2";
					$_trans_log["_key_id"]=$_key_id;
					$this->trans_log($_trans_log);


					if ( ! $this->db->simple_query($action_one))
					{
							$error = $this->db->error(); // Has keys 'code' and 'message'
							array_push($error,$action_one);
							$this->db_error_debug($error);
							return $error;
					}else{

						return true;
					}



		}

		public function db_get_array($_field1,$_field2,$_table,$_where)
		{

					$_one=array();
					$action_one="SELECT $_field1,$_field2 from $_table $_where";
					$query = $this->db->query($action_one);

				if ( ! $query )
				{
						$error = $this->db->error(); // Has keys 'code' and 'message'
						array_push($error,$action_one);
						$this->db_error_debug($error);
						return $error;
				}else{

					if ($query->num_rows() > 0)
					{
					   foreach ($query->result() as $row)
					   {
						   $_one1= $row->$_field1;
						   $_one2= $row->$_field2;
						   $_one[$_one1]=$_one2;
					   }
					}

					$query->free_result();
					return $_one;
				}
		}

		public function db_get_move_record($_data,$_table,$_where)
		{

				$_one="";
				$_field="";
				 foreach($_data as $key => $value)
				 {
					if(strlen($_field) > 0 )
					$_field .="," ;
					$_field	.= $key;

				 }

				 $action_one="SELECT $_field from $_table $_where limit 1";
				$query = $this->db->query($action_one);


				if ( ! $query )
				{
						$error = $this->db->error(); // Has keys 'code' and 'message'
						array_push($error,$action_one);
						$this->db_error_debug($error);
						return $error;
				}else{

					if ($query->num_fields() > 0)
					{
					   	foreach ($query->result() as $row)
   						{

							foreach($_data as $key => $value)
							{
								if(strlen($_one) > 0 )
								$_one .="," ;
								$_one.= "[".$key."='".$row->$key.'"]';
							}
						}
					}
					$query->free_result();
					return $_one;
				}
		}


		public function db_get_one_record($_field,$_table,$_where)
		{

				$_one="";
				$action_one="SELECT $_field from $_table $_where limit 1";
				$query = $this->db->query($action_one);

				if ( ! $query )
				{
						$error = $this->db->error(); // Has keys 'code' and 'message'
						array_push($error,$action_one);
						$this->db_error_debug($error);
						return $error;
				}else{

					if ($query->num_fields() > 0)
					{
					   $_one = $query->row_array(0);
					}
					$query->free_result();
					return $_one;
				}
		}

		public function db_get_max_record($_field,$_table,$_where)
		{
				//echo $_table.'<br>';
				$_one="";
				//if($_table!='ij_member'){
				//	$action_one="SELECT $_field,aaa.last_name,$_table.last_time_update,$_table.create_date from $_table left join ij_member as aaa on aaa.member_sn=$_table.update_member_sn $_where";
				//}else{
					$action_one="SELECT $_field from $_table $_where";
				//}
				$this->session->set_flashdata("sql_message",$action_one);
				$query = $this->db->query($action_one);
		//echo $this->db->last_query().'<br>';
				if ( ! $query )
				{
						$error = $this->db->error(); // Has keys 'code' and 'message'
						array_push($error,$action_one);
						$this->db_error_debug($error);
						return $error;
				}else{
					if ($query->num_fields() > 0)
					{
						$_one=$query->result();
						//var_dump($_one);
						$last_member_sn=0;
						$last_member_name='';
						if($_table!='ij_member'){
							foreach($_one as $_key=>$_item){
								if(isset($_item->update_member_sn)){
									$_member_sn=$_item->update_member_sn;
								}elseif(isset($_item->create_member_sn)){
									$_member_sn=$_item->create_member_sn;
								}
								if(isset($_member_sn) && $last_member_sn!=$_member_sn){
									//var_dump($this->db_get_one_record('last_name','ij_member',"where member_sn=$_member_sn")['last_name']);
									//var_dump($_member_sn);
									//exit();
									$_one{$_key}->update_last_name=$this->db_get_one_record('last_name','ij_member',"where member_sn=$_member_sn")['last_name'];
									$last_member_sn=$_member_sn;
									$last_member_name=$_one{$_key}->update_last_name;
								}else{
									$_one{$_key}->update_last_name=$last_member_name;
								}
							}
						}
					}
					$query->free_result();
					return $_one;
				}
		}

		public function db_get_max_array($_field,$_table,$_where)
		{

				$_one="";
				$action_one="SELECT $_field from $_table $_where";
				$this->session->set_flashdata("sql_message",$action_one);
				$query = $this->db->query($action_one);
				if ( ! $query )
				{
						$error = $this->db->error(); // Has keys 'code' and 'message'
						array_push($error,$action_one);
						$this->db_error_debug($error);
						return $error;
				}else{

					if ($query->num_fields() > 0)
					{
						$_one=$query->result_array();
					}
					$query->free_result();
					return $_one;
				}
		}

		public function db_get_max_num($_field,$_table,$_where)
		{

				$_max=0;
				$action_one="SELECT $_field from $_table $_where";
				$query = $this->db->query($action_one);
				if ( ! $query )
				{
						$error = $this->db->error(); // Has keys 'code' and 'message'
						array_push($error,$action_one);
						$this->db_error_debug($error);
						return $error;
				}else{

					$_max=$query->num_rows();
					$query->free_result();
					return $_max;
				}

		}


		public function db_create_data($_table,$_data) {

			$this->db->insert($_table, $_data);

			if ($this->db->affected_rows() == 1) {
				return true;
			}
			return false;
		}

		public function trans_log(array $_trans_log)
		{
				$_table_log="ij_trans_log";

				$_data["trans_date"]=date("Y-m-d H:i:s");
				$_data["trans_table"]=$_trans_log["_table"];
				$_data["trans_type"]=$_trans_log["_type"];
				$_data["trans_column_set"]=$_trans_log["_key_id"];
				$_data["beforetrans_column_set"]=$_trans_log["_before"];
				$_data["after_trans_column_set"]=$_trans_log["_after"];
				$_data["trans_member_sn"]=( isset($this->session->userdata['member_data']['member_sn'] ) ) ? $this->session->userdata['member_data']['member_sn'] : "1";
				$_data["trans_member_name"]=( isset($this->session->userdata['member_data']['member_name'] ) ) ? $this->session->userdata['member_data']['member_name'] : "1";
				$_data["trans_program_sn"]=$_SERVER['REQUEST_URI'];

			 	$this->db_insert_record($_data,$_table_log);
		}
		public function beforetrans_column_log($_table,$_key)
		{
			return "be";
		}
		public function after_trans_column_set($_table,$_key)
		{
			return "after";
		}

  public function CheckUpdate($array,$ifnew=0)
  {
  	if(!$ifnew){
      $array['last_time_update']=date("Y-m-d H:i:s",time());
      $array['update_member_sn']=( isset($this->session->userdata['member_data']['member_sn'] ) ) ? $this->session->userdata['member_data']['member_sn'] : "1";
    }else{
      $array['create_date']=date("Y-m-d H:i:s",time());
      $array['create_member_sn']=( isset($this->session->userdata['member_data']['member_sn'] ) ) ? $this->session->userdata['member_data']['member_sn'] : "2";
    }
      return $array;
  }
}
/* End of file database_tools_model.php */
/* Location: ./application/models/database_tools_model.php */
