<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/


if(isset($_result01) && is_array($_result01))
{
 	foreach($_result01 as $key => $value)
  {
  	
  	$_result_item=$value;
  	
  }   
}

?>
<link href="public/metronic/global/plugins/jquery-minicolors/jquery.minicolors.css" rel="stylesheet" type="text/css"/>


<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>版型顏色 <small>編輯版型顏色</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="<?php echo $init_control;?>">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品規格
        <i class="fa fa-circle"></i>
      </li>
      <li>
         <a href="product/productColor">版型顏色</a>
         <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         版型顏色編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">編輯版型顏色</span>
            </div>
            <div class="actions btn-set">
              <a href="product/productColor" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
          	 <?php $this->load->view('admin-ijwedding/general/flash_error');?>
            <form class="form-horizontal" role="form" action="<?php echo $_action_link;?>"  method="POST">
              <div class="form-group">
                <label for="firstname" class="col-lg-3 control-label">名稱 <span class="require">*</span></label>
                <div class="col-lg-8">
                  <input type="text" name="color_name" id="color_name" maxlength="64" class="form-control" value="<?php if(isset($_result_item->color_name)) echo $_result_item->color_name;?>">
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-lg-3 control-label">顯示顏色 <span class="require">*</span></label>
                <div class="col-lg-8">
                  <input type="text" id="color_code" name="color_code"  class="form-control demo" data-control="hue" value="<?php if(isset($_result_item->color_code)) echo $_result_item->color_code;?>">
                </div>
              </div>
              <div class="form-group">
                <label for="password" class="col-lg-3 control-label">備註</label>
                <div class="col-lg-8">
                 <textarea class="form-control maxlength-handler" rows="8" id="description" name="description" maxlength="255"><?php if(isset($_result_item->description)) echo $_result_item->description;?></textarea>
                 <span class="help-block"> max 255 chars </span>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-lg-3 control-label">排序</label>
                <div class="col-md-8">
                    <input type="text" id="sort_order" name="sort_order" class="form-control" value="<?php if(isset($_result_item->sort_order)) echo $_result_item->sort_order;?>">
                  </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-lg-3 control-label">是否啟用</label>
                <div class="col-lg-8">
                  <label class="radio-inline">
                        <input type="radio" name="status" id="status" value="1" <?php if(isset($_result_item->status) && $_result_item->status==1 || !isset($_result_item->status)) echo "checked";?>>  啟用  
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="status" id="status" value="0" <?php if(isset($_result_item->status) && $_result_item->status==0) echo "checked";?>>  不啟用 
                  </label>
                </div>
              </div>
              <!--
              <div class="form-group">
                <label for="email" class="col-lg-3 control-label">更新者</label>
                <div class="col-lg-8">
                  <input type="text" class="form-control"  placeholder="administrator" Disabled>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-lg-3 control-label">更新時間</label>
                <div class="col-lg-8">
                  <input type="text" class="form-control"  placeholder="2015-05-25 10:00" Disabled>
                </div>
              </div>
              -->
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                    <a href="<?php echo $_father_link;?>"><button type="button" class="btn default">Cancel</button></a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->