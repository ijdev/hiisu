<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Page 主架構
*/
$dealer=$this->page_data['dealer'];
$content_types=$this->page_data['content_types'];
$_web_member_data=$this->session->userdata('web_member_data');
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html>
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <meta charset="utf-8">
  <title><?php echo $dealer['dealer_name']?></title>
  <base href="<?php echo base_url('/'); ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <meta content="<?php echo $dealer['dealer_description']?>" name="description">
  <meta content="<?php echo $dealer['page_keywords']?>" name="keywords">
  <meta property="og:site_name" content="<?php echo $dealer['dealer_name']?>">
  <meta property="og:title" content="<?php echo $dealer['dealer_name']?>">
  <meta property="og:description" content="<?php echo $dealer['dealer_description']?>">
  <meta property="og:type" content="website">
  <meta property="og:image" content="/public/uploads/<?php echo $dealer['page_profile_pic_save_dir']?>"><!-- link to image for socio -->
  <meta property="og:url" content="<?php echo base_url('dealer/'.$this->router->fetch_method()); ?>">

  <link rel="icon" href="/public/uploads/<?php echo $profile[0]?>" />

  <!-- Begin Stylesheets -->
  <link type="text/css" rel="stylesheet" href="/public/uno/css/reset.css">
  <link type="text/css" rel="stylesheet" href="/public/uno/includes/entypo/style.css">
  <link type="text/css" rel="stylesheet" href="/public/uno/includes/icomoon/style.css">
  <link type="text/css" rel="stylesheet" href="/public/uno/includes/font_awesome/font-awesome.css">
  <link type="text/css" rel="stylesheet" href="/public/uno/includes/cosy/style.css">
  <link type="text/css" rel="stylesheet" href="/public/uno/js/jquery-ui/jquery-ui-1.10.3.custom.min.css">
  <link type="text/css" rel="stylesheet" href="/public/uno/js/flexslider/style.css">
  <link type="text/css" rel="stylesheet" href="/public/uno/js/custom-scroll/mCustomScrollbar.css">
  <link type="text/css" rel="stylesheet" href="/public/uno/js/Magnific-Popup/magnific-popup.css">
  <link type="text/css" rel="stylesheet" href="/public/uno/js/mb.YTPlayer/css/YTPlayer.css">
  <link type="text/css" rel="stylesheet" href="/public/uno/css/animate.min.css">
  <?php if($this->input->get('css')=='dark'):?>
    <link type="text/css" rel="stylesheet" href="/public/uno/css/dark.css">
  <?php else:?>
    <link type="text/css" rel="stylesheet" href="/public/uno/css/light.css">
  <?php endif;?>
  <link href="/public/metronic/global/css/header.css" rel="stylesheet">  <!-- Theme styles END -->
  <link href="/public/metronic/global/css/components.css" rel="stylesheet">

  <link href="/public/metronic/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="/public/metronic/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="/public/metronic/frontend/layout/css/themes/red.css" rel="stylesheet" id="style-color">
  <link href="/public/metronic/frontend/layout/css/style.css" rel="stylesheet">
  <link href="/public/metronic/frontend/pages/css/style-shop.css" rel="stylesheet" type="text/css">
  <link href="/public/css/dealer.css" rel="stylesheet">

</head>
<!-- Head END -->

<!-- Begin Body -->
<body>
<?include('header.php')?>

    <!-- Begin Content -->
    <?php echo $mainContain;?>
    <!-- End Content -->

    <!-- Begin Footer -->
    <footer>
        <p class="copyrights">2017 ©  HiiSU . ALL RIGHTS RESERVED.</p>
        <div class="social-networks clearfix">
           <a href="<?php echo $dealer['page_facebook_url_set'];?>" target="_blank">
                <span>FACEBOOK</span>
           </a>
          <?php if(@$soical_media){ foreach($soical_media as $key=>$sm){?>
            <a href="<?php echo @$sm['soical_media_hyperlink'];?>" target="_blank">
                <span><?php echo @$sm['soical_media_name'];?></span>
            </a>
          <?php }}?>
        </div>
    </footer>
    <!-- End Footer -->

    <!-- Begin JavaScript -->
    <script type="text/javascript" src="/public/uno/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="/public/uno/js/migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="/public/uno/js/modernizr-respond.js"></script>
    <script type="text/javascript" src="/public/uno/js/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/public/js/device.min.js"></script>
    <script type="text/javascript" src="/public/uno/js/scrollTo-min.js"></script>
    <script type="text/javascript" src="/public/uno/js/easing.1.3.js"></script>
    <script type="text/javascript" src="/public/uno/js/appear.js"></script>
    <script type="text/javascript" src="/public/uno/js/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="/public/uno/js/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="/public/uno/js/flexslider/flexslider.min.js"></script>
    <script type="text/javascript" src="/public/uno/js/isotope.min.js"></script>
    <script type="text/javascript" src="/public/uno/js/queryloader2.min.js"></script>
    <script type="text/javascript" src="/public/uno/js/gmap.min.js"></script>
    <script type="text/javascript" src="/public/uno/js/custom-scroll/mCustomScrollbar.min.js"></script>
    <script type="text/javascript" src="/public/uno/js/fitvids.js"></script>
    <script type="text/javascript" src="/public/uno/js/Magnific-Popup/magnific-popup.min.js"></script>
    <script type="text/javascript" src="/public/uno/js/mb.YTPlayer/inc/mb.YTPlayer.js"></script>
    <script type="text/javascript" src="/public/uno/js/mousewheel.min.js"></script>
    <script type="text/javascript" src="/public/uno/js/scripts.js"></script>
    <!-- End JavaScript -->
    <script src="/public/metronic/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/public/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>

    <script src="https://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script type="text/javascript" src="/public/js/jquery.tinyMap.min.js"></script>
    <?php if(@$page_level_js['view_path']) $this->load->view(@$page_level_js['view_path'], @$page_level_js);?>
    <script>
    function IEVersion() {
      if (!!navigator.userAgent.match(/Trident\/7\./)) {
        return 11;
      }
    }

      jQuery(document).ready(function() {
        if($("#mapView").length > 0) {
      jQuery.fn.tinyMapConfigure({
          // Google Maps API URL
          'api': '//maps.googleapis.com/maps/api/js',
          // Google Maps API Version
          'v': '3.20',
          // GPS Sensor，預設 false
          'sensor': true|false,
          // Google Maps API Key，預設 null
          'key': '<?=GOOGLE_MAPS_API_KEY?>',
          // 使用的地圖語言
          'language': 'zh‐TW',
          // 載入的函式庫名稱，預設 null
          'libraries': 'adsense,drawing,geometry',
          // 使用個人化的地圖，預設 false
          'signed_in': false,
          // MarkerClustererPlus.js 路徑
          // 預設 '//google‐maps‐utility‐library‐v3.googlecode.com/svn/trunk/markerclustererplus/src/markerclusterer_packed.js'
          // 建議下載至自有主機，避免讀取延遲造成無法使用。
          'clusterer': '//google‐maps‐utility‐library‐v3.googlecode.com/svn/trunk/markerclustererplus/src/markerclusterer_packed.js',
          // MarkerWithLabel.js 路徑
          // 預設 '//google‐maps‐utility‐library‐v3.googlecode.com/svn/trunk/markerwithlabel/src/markerwithlabel_packed.js'
          // 建議下載至自有主機，避免讀取延遲造成無法使用。
          'withLabel': '//google‐maps‐utility‐library‐v3.googlecode.com/svn/trunk/markerwithlabel/src/markerwithlabel_packed.js'
      });
          $('#mapView').tinyMap({
            // Map center
            'center': '<?php echo $dealer['addr1']?>',
            'zoom': 16,
            'marker': [
              {
                'addr': '<?php echo $dealer['addr1']?>',
                'icon': 'public/img/spotlight-poi.png',
                'animation': 'DROP'
              }
            ]
          });
        }
        $('.btn-love').click(function(){
          location.href = '<?php echo base_url('dealer/contactus')?>';
        });
        if($(".articles.masonry").length > 0) {
            $('.articles').masonry({
              // options
              itemSelector: '.article',
              columnWidth: ".size-regular"
            });
        }

    // Handles scrollable contents using jQuery SlimScroll plugin.
    /*var isRTL = false;
    if ($('body').css('direction') === 'rtl') {
          isRTL = true;
    }
    var handleScrollers = function () {
        $('.scroller').each(function () {
            var height;
            if ($(this).attr("data-height")) {
                height = $(this).attr("data-height");
            } else {
                height = $(this).css('height');
            }
            //console.log(height);
            $(this).slimScroll({
                allowPageScroll: true, // allow page scroll when the element scroll is ended
                size: '7px',
                color: ($(this).attr("data-handle-color")  ? $(this).attr("data-handle-color") : '#bbb'),
                railColor: ($(this).attr("data-rail-color")  ? $(this).attr("data-rail-color") : '#eaeaea'),
                position: isRTL ? 'left' : 'right',
                height: height,
                alwaysVisible: ($(this).attr("data-always-visible") == "1" ? true : false),
                railVisible: ($(this).attr("data-rail-visible") == "1" ? true : false),
                disableFadeOut: true
            });
        });
    }
    handleScrollers();*/
      $(document).on('click', '.top-del-goods', function (event) {
            var button = $(this);
            var rowid = button.attr('rowid')
            var addon_from_rowid = button.attr('addon_from_rowid');
            if(!addon_from_rowid){ //如果是加購商品才抓主商品
              addon_from_rowid=rowid;
            }
            //console.log(rowid);
              var r = confirm('是否確認刪除該購物車商品?（若有加購將一併刪除）');
                if(r)
                {
                    //console.log(button.attr('ItemId'));
                    $.post('shop/cartdel/', {dtable: '<?php echo base64_encode('cart');?>',dfield:'<?php echo base64_encode('rowid');?>',did:button.attr('rowid')}, function(data){
                      if (!data.error){
                        //更新購物車popup
                        $('#top-cart-content').load('shop/top_cart');
                        console.log($('#top-cart-content').html());

                        //$('.top-cart-info-count').text(data.total_items+' 件');
                        //$('.top-cart-info-value').text('$ '+data.total);
                        //if(data.gift_cash){
                        //  $('#show_gift_cash').text(data.gift_cash);
                        //  $('#gift_cash').val(data.gift_cash);
                        //}
                        //if($(".tr_"+addon_from_rowid+' .checkboxrowid').is(":checked")){ //if有勾選
                          //checkoutupdate(false,rowid);
                        //}
                        //button.parents().filter('li').remove();
                        $('.addon_from_rowid'+button.attr('rowid')).remove();
                        $(".tr_"+rowid).remove();
                      }
                    },'json');
                }
        });
  });
 	$(document).ready(function() {
		$("li.dropdown > a > .arrow-down").removeClass("up");
		$("li.dropdown > a > .arrow-down").click(function(e) {
            $(this).toggleClass("up");
			$("ul.dropdown-menu > li > a > .arrow-down").removeClass("up");
        });
		$("ul.dropdown-menu > li > a > .arrow-down").click(function(e) {
			$(this).toggleClass("up");
		});
//		 $(".dropdown > ul.dropdown-menu").css("display","block");
	});
	  $(document).ready(function(){
		var shopW = $(".dealernav").width();
    //console.log(shopW);
		$("ul li.dropdown:nth-child(2) ul.dropdown-menu").css("width",function(){
			return shopW - 178.09;
		});
	  });
	  $(window).resize(function(){
		var shopW = $(".dealernav").width();
		$("ul li.dropdown:nth-child(2) ul.dropdown-menu").css("width",function(){
			return shopW - 178.09;
		});
	  });
    </script>

</body>
<!-- End Body -->

</html>