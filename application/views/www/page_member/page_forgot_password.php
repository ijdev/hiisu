<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容
*/
?>
<div class="container">

<!-- Page level plugin styles START -->
<!-- Page level plugin styles END -->
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <li class="active">忘記密碼</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar col-md-2 col-sm-3">
    <ul class="list-group margin-bottom-25 sidebar-menu">
      <?php //$this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
    </ul>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="col-md-10 col-sm-9">
    <div class="product-page">
     <!--
      <div class="alert alert-success">
        <strong>申請完成!</strong> 系統已經寄發一份密碼更改確認信件到您使用來註冊的信箱中，請前往收信並依照信件中指示完成密碼修改，HiiSU 感謝您的支持．
      </div>
     
      <div class="alert alert-danger">
        <strong>驗證失敗!</strong> 系統找不到您的帳號或Email，請重新輸入或聯絡管理員．
      </div>
      -->
      <?php $this->load->view('www/general/flash_error');?>
      <h1>
        <i class="fa fa-question-circle"></i> 忘記密碼了嗎？
      </h1>
      <div class="row">
        <div class="col-md-7 col-sm-7">
          <form class="form-horizontal form-without-legend" role="form" method="post" action="member/memberForgotPasswordAction">
            <div class="form-group">
              <label for="email" class="col-lg-4 control-label">Email <span class="require">*</span></label>
              <div class="col-lg-8">
                <input type="text" class="form-control" name="email" id="email" required>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-lg-4 control-label">驗證碼 <span class="require">*</span></label>
              <div class="col-lg-8">
                <input type="text" class="form-control" name="rycode" id="rycode" size="6" required><br>
               <?php echo $_captcha_img;?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-4 control-label"></label>
              <div class="col-lg-8 padding-top-5">
                <button type="submit" class="btn btn-primary">重設密碼</button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-4 col-sm-4 pull-right">
          <div class="form-info">
            <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/page_forgot_password_info');?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END CONTENT -->
</div>
</div>
<!-- END SIDEBAR & CONTENT -->
<style>
.row.chnge_pass label {
    font-weight: 600;
}

</style>