<?php defined('BASEPATH') OR exit('No direct script access allowed');
$_web_member_data=$this->session->userdata('web_member_data');
?>

<script src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
<script src="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->
<script src="public/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script><!-- Quantity -->
<script src="public/metronic/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/rateit/src/jquery.rateit.js" type="text/javascript"></script>
<script src="//code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script><!-- for slider-range -->
<script src="public/metronic/frontend/layout/scripts/layout.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
		$("#receiver_addr_state").attr("disabled","disabled");
		$("#ifinvoice1,#ifinvoice0").click(function () {
		    $(".greenon").css("color","#3c763d");

		});

		$(".fbtnsgrn").click(function () {
		    $(".greenon4").css("color","#3c763d");
			 $(".rdbtnicls").show();
		});

        Layout.init();
        Layout.initOWL();
        //Layout.initImageZoom();
        Layout.initTouchspin();
        Layout.initFixHeaderWithPreHeader();
        Layout.initNavScrolling();
        //Layout.initUniform();
        Layout.initSliderRange();

		$( "#datepicker" ).datepicker({
			minDate: +3,
			maxDate: "+1M",
			dateFormat: "yy-mm-dd",
			onSelect: function(dateText, inst) {
				//alert(dateText);
				$.ajax({
						url: 'checkholiday.php',
						data: 'date=' + dateText,
						type:'get',
						dataType:"html",
						cache: false,
						success: function(msg){
							if(msg==0){
								alert("您不可以選擇節假日作為配送日期");
								$('#datepicker').val("");
							}
						}
				});
			}
		});
        jQuery('#checkbox_data_sync_1').change(function(){
        	if($(this).is(":checked")){
						$('#name1').val($('#last_name').val());
						$('#tel_area_code1').val($('#tel_area_code').val());
						$('#tel1').val($('#tel').val());
						$('#tel_ext1').val($('#tel_ext').val());
						$('#zipcode1').val($('#zipcode').val());
						$('#addr1').val($('#addr').val());
						$('#cell1').val($('#cell').val());
						$('#addr_city_code1').val($("#addr_city_code").find(":selected").val());
						get_town1($("#addr_city_code").find(":selected").val(),$("#addr_town_code").find(":selected").val());
						//$('#addr_town_code1').val($("#addr_town_code").find(":selected").val());
					  //$('select[id="addr_town_code1"]').val($("#addr_town_code").find(":selected").val());
						//console.log($("#addr_town_code").find(":selected").val());
						//console.log($("#addr_town_code1").html());
						//$.uniform.update('#addr_town_code1');
					}
        });
				var addr_city_code='<?php echo (@$order["buyer_addr_city_code"])? $order["buyer_addr_city_code"]:@$_web_member_data["addr_city_code"];?>';
				var addr_town_code='<?php echo (@$order["buyer_addr_town_code"])? $order["buyer_addr_town_code"]:@$_web_member_data["addr_town_code"];?>';
				if(addr_city_code){
					get_town(addr_city_code);
					$('select[id="addr_city_code"]').val(addr_city_code);
				}
			     /* Action On Select Box Change */
					  $('select[id="addr_city_code"]').change(function() {
					        var data = $(this).val(); // Get Selected Value
									get_town(data);

			      });

						function get_town(data){
						    $.ajax({
						        url: 'Member/get_town_zip',
						        data: 'data=' + data,
						        dataType: 'json',
						        success: function(data) {
						         		 $('#addr_town_code').empty();
												 $(data).appendTo("#addr_town_code");
		      							 //$.uniform.update('#addr_town_code');
													if(addr_town_code){
														$('select[id="addr_town_code"]').val(addr_town_code);
		      							 		//$.uniform.update('#addr_town_code');
		      							 	}else{
		      							 		//$.uniform.update('#addr_town_code');
													}
						        }
						    });
				    }
					  $('select[id="addr_town_code"]').change(function() {
								var zipcode = $('option:selected', this).attr('zipcode');
								$('#zipcode').val(zipcode);
			      });

					  $('#addr_city_code1').change(function() {
					        var data1 = $(this).val(); // Get Selected Value
									get_town1(data1);

			      });

				var addr_city_code1='<?php echo (@$order["receiver_addr_city_code"])? $order["receiver_addr_city_code"]:@$receiver_data["addr_city_code"];?>';
				var addr_town_code1='<?php echo (@$order["receiver_addr_town_code"])? $order["receiver_addr_town_code"]:@$receiver_data["addr_town_code"];?>';
				if(addr_city_code1){
					get_town1(addr_city_code1,addr_town_code1);
					$('select[id="addr_city_code1"]').val(addr_city_code1);
				}
			     /* Action On Select Box Change */
						//console.log(data1);
					  $('select[id="addr_city_code1"]').change(function() {
					        var data1 = $(this).val(); // Get Selected Value
									get_town1(data1);

			      });

						function get_town1(data1,addr_town_code1){
						    $.ajax({
						        url: 'Member/get_town_zip/0/'+$("#receiver_addr_state").val(),
						        data: 'data=' + data1,
						        dataType: 'json',
						        success: function(data1) {
						         		 $('#addr_town_code1').empty();
												 $(data1).appendTo("#addr_town_code1");
													if(addr_town_code1){
														$('select[id="addr_town_code1"]').val(addr_town_code1);
		      							 		//$.uniform.update('#addr_town_code');
													}
						        }
						    });
				    }
					  $('select[id="addr_town_code1"]').change(function() {
								var zipcode = $('option:selected', this).attr('zipcode');
								$('#zipcode1').val(zipcode);
			      });

			$('#submit1').click(function(e) {
				if(!$("input[name='order[payment_method]']:checked").val()){
					$("input[name='order[payment_method]']").focus();
					alert('請選擇付款方式');
					return false;
				}else if(!$("input[name='ifinvoice']:checked").val()){
					$("input[name='ifinvoice']").focus();
					alert('請選擇發票格式');
					return false;
				}else if(!$("input[name='order[receiver_last_name]']").val()){
					$("input[name='order[receiver_last_name]']").focus();
					alert('請填寫收貨人姓名');
					return false;
				}else if(!$("input[name='order[receiver_tel]']").val()){
					$("input[name='order[receiver_tel]']").focus();
					alert('請填寫聯絡電話');
					return false;
				}else if(!$("input[name='order[receiver_cell]']").val()){
					$("input[name='order[receiver_cell]']").focus();
					alert('請填寫聯絡手機');
					return false;
				}else if(!$("select[name='order[receiver_addr_city]']").val()){
					$("select[name='order[receiver_addr_city]']").focus();
					alert('請選擇縣市');
					return false;
				}else if(!$("select[name='order[receiver_addr_town]']").val()){
					$("select[name='order[receiver_addr_town]']").focus();
					alert('請選擇鄉鎮區');
					return false;
				}else if(!$("input[name='order[receiver_addr1]']").val()){
					$("input[name='order[receiver_addr1]']").focus();
					alert('請填寫地址');
					return false;
				}else if($("#ifinvoice_f1").is(':visible')){
					if(!$('#invoice_title').val()){
						alert('您選擇三聯式統一發票，發票抬頭必須填寫');
						$('#invoice_title').focus();
						return false;
					}
					if(!$('#company_tax_id').val()){
						alert('您選擇三聯式統一發票，統一編號必須填寫');
						$('#company_tax_id').focus();
						return false;
					}
				}
				$("#receiver_addr_state").attr("disabled",false);
				//console.log($("input[name='order[payment_method]']:checked").val());
				if($("input[name='order[payment_method]']:checked").val()=='11'){
					//$('#shoppingform').attr('target','_blank');
				}
	        });
		<?php if(@$order["invoice_title"] && @$order["company_tax_id"]){?>
			$('#tab2s').trigger('click');
		<?php }?>
    });
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->