<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Supplier extends MY_Controller {
	 	public function __construct()
		{
			 parent::__construct();

				$this->load->library('session');
				$this->load->helper('form');
				$this->load->helper('url');
				$this->load->library('form_validation');
				$this->load->library('sean_lib/Sean_form_general');
				$this->load->model('general/My_general_ct');
				$this->load->config('ij_config');
				$this->load->config('ij_sys_config');
			    $this->load->model('sean_models/Sean_general');
				$this->load->model('sean_models/Sean_db_tools');
				$this->load->model('libraries_model');
				$this->page_data = array();
				$this->page_data['unreadmessage']= $this->libraries_model->get_unread_message_count();
				$this->page_data['init_control']="admin/";
				$this->page_data['view_control']="admin/";
				$this->init_control=$this->page_data['init_control'];
				$this->page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
				$this->page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$this->page_data['page_content']['view_path'] = 'admin/page_home/page_content'; // 頁面主內容 view 位置, 必要
				$this->page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$this->page_data['page_level_js']['view_path'] = 'admin/page_home/page_content_js'; // 頁面主內容的 JS view 位置, 必要
				$this->lang->load('general', 'zh-TW');
				$this->page_data["_per_page"]=10;
				$this->LoginCheck();
				$data['Mes'] = $this->session->userdata('Mes');
				if($data['Mes']){$this->load->view('Message',$data);$this->session->unset_userdata('Mes');}
		}

 /*
	*
  * 檢查是否有登入
	*/

	//$this->LoginCheck();
	public function LoginCheck()
	{

			if($this->session->userdata('member_login')== true)
			return true;
			else
			redirect(base_url("/admin/login"));
			exit();
	}

 /**
	 * 供應商列表 Template
	 *
	 */
	public function supplierTable($_action="",$_id="",$_sub_id="")
	{
		/* Begin 修改 */
		//載入 model
    	if($this->session->userdata('member_role_system_rules')[0]['role_name']=='供應商管理員' && $this->session->userdata['member_data']['associated_supplier_sn']!=$_id){
			$this->session->set_userdata(array('Mes'=>'很抱歉！您僅能觀看跟修改自己的資料! '));
			redirect(base_url('admin/supplier/supplierTable/edit/'.$this->session->userdata['member_data']['associated_supplier_sn']));
		}
		$_model_name="supplier_table";
		$this->page_data['_id']=$_id;// 功能名稱
		$this->page_data['_father_name']="會員管理";// 功能名稱
		$this->page_data['_ct_name']="供應商列表";// 功能名稱
		$this->page_data['_ct_name_item']="供應商列表編輯";// 功能子名稱
		$this->page_data['_url']="supplier/supplierTable/";//列表功能url
		$this->page_data['_table']="ij_supplier";//table name
		$this->page_data['_table_key']="supplier_sn";//table key
		$this->page_data['_member_data']=$this->session->userdata('member_data');
		$this->page_data['_table_field']=array(
					"supplier_sn",
					"supplier_name",
					"supplier_status",
				);

		//產業屬性
		$_where=" where status='1' ";
		$_biz_domain_list=$this->Sean_db_tools->db_get_max_record("*","ij_biz_domain",$_where);
		$this->page_data['_biz_domain_list']=$_biz_domain_list;

		//合作模式
		$_where=" where display_flag='1' ";
		$_cooperate_mode_list=$this->Sean_db_tools->db_get_max_record("*","ij_cooperate_mode_type_ct",$_where);
		$this->page_data['_cooperate_mode_list']=$_cooperate_mode_list;

		//聯絡人類別
		$_where=" where display_flag='1' ";
		$_contact_type_list=$this->Sean_db_tools->db_get_max_record("*","ij_contact_type_ct",$_where);
		$this->page_data['_contact_type_list']=$_contact_type_list;

		$_where=" where member_status='2' and system_user_flag=1 ";
		$_associated_sales_membe_list=$this->Sean_db_tools->db_get_max_record("*","ij_member",$_where);
		$this->page_data['_associated_sales_membe_list']=$_associated_sales_membe_list;

		$_table_where=" ";//table list where
		$_page_content="page_supplier/table";
		$_page_level_js="page_supplier/table_js";
		$_page_content_item="page_supplier/item";
		$_page_level_js_item="page_supplier/item_js";

		$this->page_data['system_words']=$this->libraries_model->_select('ij_system_file_config','system_file_name','退換貨須知',0,0,0,0,'row')['system_file_ontent'];

		/* END 修改 */
		$this->page_data['_father_link']=base_url($this->page_data['init_control'].$this->page_data['_url']);
		$this->load->model('member/'.$_model_name);

	  $payments = $this->libraries_model->_select('ij_payment_method_ct','display_flag','1',0,0,0,0,'result_array');
	  $this->page_data['payments'] = $payments;

		switch($_action){
			case "add" :
			case "item" :
			case "edit" :
			case "adding" :
			case "update" :
			case "bank_update" :
			case "contact_update" :
			case "delete_contact" :
			case "delete_member" :

				// 內容設定
				 $this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."adding";

				//產業屬性
				$_where=" where status='1' ";
				$_biz_domain_list=$this->Sean_db_tools->db_get_max_record("*","ij_biz_domain",$_where);
				$this->page_data['_biz_domain_list']=$_biz_domain_list;




				 //編輯
		    if($_action=="edit")
				{
					$this->Check_page_permission('search','查詢');
					//新增管理員
					if($this->input->post('associated_member_sn') && $_id){
						$associated_member_sn=$this->input->post('associated_member_sn');
		    			$_data["associated_supplier_sn"]=$_id;
		    			$_data["system_user_flag"]='1'; //2017-5-25 add 因為跟權限整合並加入系統角色-供應商管理
		    			$_where="where member_sn=$associated_member_sn";
		 				$update_result=$this->Sean_db_tools->db_update_record($_data,'ij_member',$_where);
		 				//新增角色設定
		 				$roles=$this->libraries_model->get_member_roles($associated_member_sn);
		 				//var_dump($roles);exit;
		 				foreach($roles as $_item){
		 					if($_item['role_name']=='供應商管理員'){
								$_data2=array('status'=>'1');
								$_data2 = $this->libraries_model->CheckUpdate($_data2,0);
								$this->libraries_model->_update('ij_member_role_relation',$_data2,'member_role_relation_sn',$_item['member_role_relation_sn']);
		 					}
		 				}

				//echo $this->db->last_query().'<br>';
						if($update_result)
							$this->session->set_flashdata("message","新增完成！");
						else
							$this->session->set_flashdata("fail_message","新增失敗！");
						redirect($this->page_data['_father_link'].'/edit/'.$_id.'#tab_manager');
						//var_dump($update_result);
						exit();
					}
					//刪除管理員
					if($this->input->post('member_sn') && $_id){
						$this->Check_page_permission('del','刪除');
						$del_member_sn=$this->input->post('member_sn');
						//var_dump($del_member_sn);
	    			$_data["associated_supplier_sn"]='null';
	    			$_where="where member_sn=$del_member_sn";
		 				$update_result=$this->Sean_db_tools->db_update_record($_data,'ij_member',$_where,'member_sn',$del_member_sn);
						if($update_result)
							echo true;
							//$this->session->set_flashdata("message","刪除完成！");
						else
							echo false;
							//$this->session->set_flashdata("fail_message","刪除失敗！");
						//redirect($this->page_data['_father_link'].'edit/'.$_id.'#tab_manager');
						//var_dump($update_result);
						exit();
					}
				  //關聯會員s
					//$_where=" left join ij_member_supplier_relation on ij_member.member_sn=ij_member_supplier_relation.member_sn where supplier_sn='".$_id."' order by last_time_update desc";
					//$_result02=$this->Sean_db_tools->db_get_max_record("ij_member.*,ij_city_ct.city_name,ij_town_ct.town_name","ij_member left join ij_city_ct on ij_city_ct.city_code=ij_member.addr_city_code left join ij_town_ct on ij_town_ct.town_code=ij_member.addr_town_code",$_where);
					//$this->page_data['_result02']=$_result02;
					//var_dump($_result02);

			    //管理員s
			    $_where=" where associated_supplier_sn=".$_id." and member_status=2 and (member_level_type=5 or member_level_type=6) order by last_time_update desc"; //有效經銷會員
					$_result03=$this->Sean_db_tools->db_get_max_record("member_sn,last_name,first_name,email,user_name","ij_member",$_where);
					$this->page_data['_result03']=$_result03;

			    //供應商管理員s
        	if($this->session->userdata['member_data']['member_level_type']=='9'){
				    $_where=" where (associated_supplier_sn is null or associated_supplier_sn=0) and member_status=2 and (member_level_type=5 or member_level_type=6) order by first_name,last_name"; //有效會員 associated_supplier_sn=0
						$_result04=$this->Sean_db_tools->db_get_max_record("member_sn,last_name,first_name,email","ij_member",$_where);
					}else{
						$_result04=array();
					}
					$this->page_data['_result04']=$_result04;

					$_where=" where status='1' and associated_supplier_sn=$_id ";
					$_bank_info=$this->Sean_db_tools->db_get_max_record("*","ij_bank_info",$_where);
					$this->page_data['_bank_info']=$_bank_info;

					$_where=" left join ij_contact_type_ct on ij_contact_type_ct.contact_type=ij_contact_info.contact_type where associated_supplier_sn=$_id and status <='1' order by last_time_update desc";
					$_contact_info=$this->Sean_db_tools->db_get_max_record("*","ij_contact_info",$_where);
					$this->page_data['_contact_info']=$_contact_info;

					//產業屬性 內容

					//$_where=" where associated_member_sn='".$_id."' and status =1 "; ?associated_member_sn
					$_where=" where associated_supplier_sn='".$_id."' and status =1 ";
					$_member_domain_relation=$this->Sean_db_tools->db_get_max_record("*","ij_member_domain_relation",$_where);
					$this->page_data['_member_domain_relation']=$_member_domain_relation;



					$this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."update/".$_id."";

					$_result="";
					$_where=" where ".$this->page_data['_table_key']."='".$_id."'";
					$_result=$this->Sean_db_tools->db_get_max_record("*",$this->page_data['_table'],$_where);
					//付款方式
					if($_result[0]->associated_payment_method){
						$associated_payment_array=explode(':',$_result[0]->associated_payment_method);
					}else{
						$associated_payment_array=array();
					}
					//var_dump($_result[0]->associated_payment_method);
				  foreach($payments as $key=>$payment){
						if($associated_payment_array){
				  		foreach($associated_payment_array as $key2=>$payment2){
				  			if($payment2==$payment['payment_method']){
									$payments[$key]['checked']='1';
								}else{
								}
							}
						}else{
							if($payment['default_flag']=='1'){
								$payments[$key]['checked']='1';
							}else{
								$payments[$key]['checked']='0';
							}
						}
				  }

					//$_result=$this->Sean_db_tools->db_get_max_record("ij_supplier.*,email,user_name","ij_supplier left join ij_member on ij_supplier.supplier_sn=ij_member.associated_supplier_sn",$_where);
					$this->page_data['_aCtion_link']=$this->page_data['init_control'].$this->page_data['_url']."update/$_id";
					$this->page_data['_result01']=$_result;
	  				$this->page_data['payments'] = $payments;
				}

				//更新
				if($_action=="update")
				{
					$this->Check_page_permission('edit','修改');
					$_where=" where supplier_sn='".$_id."'";
					$_result=$this->$_model_name->action_update($_where,$_id,"supplier_sn");
					//var_dump($_result);
					//exit();
					if($_result)
						$this->session->set_flashdata("success_message","更新完成！");
					else
						$this->session->set_flashdata("fail_message","更新失敗！");
					$tab_id=$this->input->post('tab_id');

					//var_dump($_result['bank_status']);
					//exit();
					//if($_result['bank_status']=='0'){
						redirect($this->page_data['_father_link'].'edit/'.$_id.'#'.$tab_id);
					//}else{
						//redirect($this->page_data['_father_link']);
					//}
					exit();
				}

				//更新
				if($_action=="bank_update")
				{
					$this->Check_page_permission('edit','修改');
					$_result=$this->$_model_name->bank_update($_id);
					//var_dump($_result);
					//exit();
					//if($_result)
						$this->session->set_flashdata("success_message","更新完成！");
					//else
					//	$this->session->set_flashdata("fail_message","更新失敗！");

					//var_dump($_result['bank_status']);
					//exit();
					//if($_result=='0'){
						redirect($this->page_data['_father_link'].'/edit/'.$_id.'#tab_money');
					//}else{
					//	redirect($this->page_data['_father_link']);
					//}
					exit();
				}

				//add
				if($_action=="contact_update")
				{
					$this->Check_page_permission('edit','修改');
					$_result=$this->$_model_name->contact_update($_id);
					//var_dump($_result);
					//exit();
					$this->session->set_flashdata("success_message",$_result);
					redirect($this->page_data['_father_link'].'/edit/'.$_id.'#tab_member');
					exit();
				}
				//刪除子項
				if($_action=="delete_contact")
				{
					$this->Check_page_permission('del','刪除');
					$_result=$this->$_model_name->delete_sub($_id);
					echo $_result;
					//$this->session->set_flashdata("success_message",$_result);
					//redirect($this->page_data['_father_link'].'edit/'.$_id.'#tab_member');
					exit();
				}
				//新增
				if($_action == "adding")
				{
					$this->Check_page_permission('add','新增');
					$_result=$this->$_model_name->action_insert($this->page_data);
					if($_result)
						$this->session->set_flashdata("success_message","新增完成！");
					else
						$this->session->set_flashdata("fail_message","新增失敗！");
				}
				if($_action == "add")
				{
					$this->Check_page_permission('add','新增');
				}
				/*$this->page_data['page_level_js']['status'] = array(
														array('label' => '未發送', 'num' => 200),
														array('label' => '已發送', 'num' => 100),
														array('label' => '已審核', 'num' => 300),
														array('label' => '已使用', 'num' => 300),
														array('label' => '失效', 'num' => 100)
														);
				$this->page_data['page_level_js']['enable'] = array(
														array('label' => '尚未使用', 'num' => 30000),
														array('label' => '已使用', 'num' => 70000)
														);
					*/

				// 頁面主內容 view 位置, 必要
				$this->page_data['page_content']['view_path'] = $this->page_data['view_control'].$_page_content_item;
				// 頁面主內容的 JS view 位置, 必要
				$this->page_data['page_level_js']['view_path'] = $this->page_data['view_control'].$_page_level_js_item;

				$this->page_data['_city_ct'] = $this->libraries_model->_select('ij_city_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','city_code,city_name'); // city
				//var_dump($this->page_data['_city_ct']);

				$this->load->view($this->page_data['view_control'].'general/main', $this->page_data);

			break;

			case "edit_contact" :
					$this->Check_page_permission('edit','修改');
				//聯絡人類別
				$_where=" where display_flag='1' ";
				$_contact_type_list=$this->Sean_db_tools->db_get_max_record("*","ij_contact_type_ct",$_where);
				$this->page_data['_contact_type_list']=$_contact_type_list;
				$_where=" left join ij_contact_type_ct on ij_contact_type_ct.contact_type=ij_contact_info.contact_type where contact_sn=$_sub_id and status <='1'";
				$_contact_info=$this->Sean_db_tools->db_get_max_record("*","ij_contact_info",$_where)[0];
				//var_dump($_where);
				$this->page_data['_contact_info_item']=$_contact_info;
				$this->page_data['_supplier_sn']=$_id;

				$this->load->view('admin/page_supplier/modal_contact', $this->page_data);
			break;

			default:
				//刪除
				if($_action=="delete")
				{
					$this->Check_page_permission('del','停用');

					$_where=" where ".$this->page_data['_table_key']."='".$_id."'";
					$_result=$this->$_model_name->action_delete($_where,$this->page_data);


					if($_result)
						$this->session->set_flashdata("success_message","停用完成！");
					else
						$this->session->set_flashdata("fail_message","停用失敗！");
				}
				if($_action=="search")
				{


							$_table_where="";
							if($this->input->get_post("status") || strlen($this->input->get_post("status")) > 0)
							{
								if(strlen($_table_where) < 2 )
								$_table_where .=" where";
								else
								$_table_where .= " and ";

								$_data["status"]=$this->input->get_post("status");
								$_table_where .="  supplier_status = '".$_data["status"]."'";
								$this->session->set_userdata('search_location_status',$_data["status"]);
							}

							if($this->input->get_post("wedding_location_name") || strlen($this->input->get_post("wedding_location_name")) > 0)
							{
								if(strlen($_table_where) < 2 )
								$_table_where .=" where";
								else
								$_table_where .= " and ";

								$_data["wedding_location_name"]=$this->input->get_post("wedding_location_name");
								$_table_where .="  wedding_location_name  like '%".$_data["wedding_location_name"]."%'";
								$this->session->set_userdata('search_location_wedding_location_name',$_data["wedding_location_name"]);
							}

							$search_term = ''; // default when no term in session or POST


				}else{
					$this->Check_page_permission('search','查詢');
					$this->session->set_flashdata("message",$this->session->userdata('search_location_status'));
					if ( $this->session->userdata('search_location_status') or  strlen($this->session->userdata('search_location_status')) > 0  )
					{

								if(strlen($_table_where) < 2 )
									$_table_where .=" where";
								else
									$_table_where .= " and ";

								$search_term = $this->session->userdata('search_location_status');
								$_table_where .="  supplier_status = '".$search_term."'";
					}

					if ($this->session->userdata('search_location_wedding_location_name') )
					{
								if(strlen($_table_where) < 2 )
									$_table_where .=" where";
								else
									$_table_where .= " and ";

								$search_term = $this->session->userdata('search_location_wedding_location_name');
								$_table_where .="  wedding_location_name  like '%".$search_term."%'";
					}

				}

				// 返回連結
		    $this->page_data['_action_link']=$this->page_data['init_control'].$this->page_data['_url']."add";
		    $this->page_data['_edit_link']=$this->page_data['init_control'].$this->page_data['_url']."edit/";
		    $this->page_data['_delete_link']=$this->page_data['init_control'].$this->page_data['_url']."delete/";
		    $this->page_data['_search_link']=$this->page_data['init_control'].$this->page_data['_url']."search/";

		    /*$config = array();
		    $config['total_rows'] = $this->Sean_db_tools->db_get_max_num($this->page_data['_table_key'],$this->page_data['_table'],$_table_where);

		    if(is_numeric($_action))
		    {
		    	$_start_num=  $_action + 1;
		    	if($_start_num <  1 )
		    	$_start_num=1;

		    	$_end_num=$_start_num + $this->page_data["_per_page"] - 1;

		    	if($_end_num  > $config['total_rows'])
		    	$_end_num=$config['total_rows'];
		    	$_table_where .= " limit $_start_num , ".$this->page_data["_per_page"];
		    }else{
		    	$_start_num=1;
		    	$_end_num=$this->page_data["_per_page"];

		    	if($_end_num  > $config['total_rows'])
		    	$_end_num=$config['total_rows'];

		    	$_table_where .=" limit  ".$this->page_data["_per_page"];
		    }
				//$_result01=$this->Sean_db_tools->db_get_max_record("*",$this->page_data['_table'],$_table_where);
				//$_result01=$this->Sean_db_tools->db_get_max_record("*","ij_supplier left join ij_member on ij_supplier.supplier_sn=ij_member.associated_supplier_sn",$_table_where);
				$_result01=$this->$_model_name->get_supplier_list($_table_where);
				$this->page_data['_result01']=$_result01;

				$this->load->library("pagination");

				$config['base_url'] = $this->page_data['_father_link'];
				$config['full_tag_open'] = '<div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing '.$_start_num.' to '.$_end_num.' of '.$config['total_rows'].' entries</div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                 <ul class="pagination" style="visibility: visible;"> ';
				$config['full_tag_close'] = '</ul></div>
              </div>
            </div>';

        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class="previous">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><span>';
        $config['cur_tag_close'] = '</span></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

				$config['per_page'] = $this->page_data["_per_page"];
				$this->pagination->initialize($config);
				$this->page_data["_pagination"]=$this->pagination->create_links();
		*/
				$_result01=$this->$_model_name->get_supplier_list('');
				$this->page_data['_result01']=$_result01;

				$this->page_data['_city_ct'] = $this->libraries_model->_select('ij_city_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','city_code,city_name'); // city
				//var_dump($this->page_data['_city_ct']);
				// 頁面主內容 view 位置, 必要
				$this->page_data['page_content']['view_path'] = $this->page_data['view_control'].$_page_content;
				// 頁面主內容的 JS view 位置, 必要
				$this->page_data['page_level_js']['view_path'] = $this->page_data['view_control'].$_page_level_js;
				$this->load->view($this->page_data['view_control'].'general/main', $this->page_data);
			break;
		}
	}

	public function supplierAccounting(){
		if($sub_order_sn=$this->input->post('sub_order_sn')){
			$checkstatus=$this->input->post('checkstatus');
			$data_array=array('checkstatus'=>$checkstatus);
			$data_array = $this->libraries_model->CheckUpdate($data_array,0);
			$this->libraries_model->_update('ij_sub_order',$data_array,'sub_order_sn',$sub_order_sn);
			$this->session->set_flashdata("success_message","修改成功");
			redirect(base_url('admin/supplier/supplierAccounting'));
			exit;
		}
		$page_data = array();
		// 內容設定
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['sub_order_status']=$this->libraries_model->_select('ij_sub_order_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array','sub_order_status,sub_order_status_name');
		$page_data['page_content']['payment_status']=$this->libraries_model->_select('ij_payment_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array','payment_status,payment_status_name');
		$page_data['page_content']['check_status']=$this->libraries_model->_select('ij_dealer_order_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array','dealer_order_status,dealer_order_status_name');
		$page_data['page_content']['checkstatus']=$this->libraries_model->_select('ij_dealer_order_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'list','dealer_order_status,dealer_order_status_name');

		$data_array=array('from'=>date('Y-m').'-01','to'=>date('Y-m-t'));
		if($this->input->get_post('from')) $data_array['from']=date("Y-m-d", strtotime($this->input->get_post('from')));
		if($this->input->get_post('to')) $data_array['to']=date("Y-m-d", strtotime($this->input->get_post('to')));
		if($this->input->get_post('sub_order_status')) $data_array['sub_order_status']=(int)$this->input->get_post('sub_order_status');
		if($this->input->get_post('payment_status')) $data_array['payment_status']=(int)$this->input->get_post('payment_status');
		if(strlen($this->input->get_post('checkstatus'))!=0) $data_array['checkstatus']=$this->input->get_post('checkstatus');
		if($this->input->get_post('supplier_name')) $data_array['supplier_name']=$this->input->get_post('supplier_name');
		$orders=$this->libraries_model->get_orders($data_array,4);
		//$this->load->model('member/partner_table');
		$page_data['page_content']['total'] = $orders['total'];
		unset($orders['total']);
		if($this->input->get_post('flg')=='export'){
	        $this->load->library('io_excel');
	        $this->io_excel->supplierAccounting_export($orders,$page_data['page_content']['checkstatus']);
	        exit;
		}
		$page_data['page_content']['orders'] = $orders;
		$page_data['page_content']['data_array']=$data_array;
		$page_data['page_content']['view_path'] = 'admin/page_supplier/accounting'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_supplier/accounting_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}


			//---------------------------------
			// EMAIL EXISTS (true or false)
			//---------------------------------
			private function Email_exists($email)
			{

				$_result=0;
				$_where= " where user_name='".$email."' ";
				$_result=$this->Sean_db_tools->db_get_max_num("member_sn","ij_member",$_where);

				return $_result;
			}

			//---------------------------------
			// AJAX REQUEST, IF EMAIL EXISTS
			//---------------------------------
			function Register_email_exists()
			{
				if (array_key_exists('user_name',$_POST))
				{


					if( $this->Email_exists($this->input->post('user_name')) > 0 )
					{
							echo '<span style="color:#f00">此Email已註冊。 </span>';
					} else {
						 echo '<span style="color:#0c0">此Email可註冊。</span>';
					}
				}
				die();// or exit
			}

		 function do_upload($files,$_id,$field)
		{
				$_where=" where supplier_sn='".$_id."'";
				//var_dump($this->Sean_db_tools->db_get_one_record($field,'ij_supplier',$_where));
				$origin=explode(':',$this->Sean_db_tools->db_get_one_record($field,'ij_supplier',$_where)[$field]);

				$_tmp['filenames']='';
				$_tmp['Mes']='';
		    $this->load->library('upload');
		    //$files = $_FILES;
		    $cpt = count($files['name']);
		    //var_dump($files['name']);

		    for($i=0; $i<$cpt; $i++)
		    {
		    	//var_dump($files['name'][$i]);
		    	if($files['name'][$i]){
		        $_FILES['userfile']['name']= $files['name'][$i];
		        $_FILES['userfile']['type']= $files['type'][$i];
		        $_FILES['userfile']['tmp_name']= $files['tmp_name'][$i];
		        $_FILES['userfile']['error']= $files['error'][$i];
		        $_FILES['userfile']['size']= $files['size'][$i];

		        $this->upload->initialize($this->set_upload_options($_id));
		        if($this->upload->do_upload()){
                $img	 = $this->upload->data();
                $_tmp['filenames'] .=  'dealer/'.$_id.'/'.$img['file_name'];

		        }else{
								$_tmp['Mes'].= strip_tags($this->upload->display_errors());
		        }
		      }else{
             $_tmp['filenames'] .= (@$origin[$i])? $origin[$i]:':'; //原本檔名
		      }
			          if($i< $cpt){
			          	$_tmp['filenames'] .=':';
			          }
		    }
				return $_tmp;
		}
		private function set_upload_options($_id)
		{
		    //upload an image options
		    $config = array();
        $config['upload_path']  = ADMINUPLOADPATH.'dealer/'.$_id.'/' ;
        $config['allowed_types']= 'gif|jpg|png|jpeg';
        $config['max_size']     = '4196';
        //$config['max_width']    = '1000';
        //$config['max_height']   = '900';
        $config['encrypt_name']   = true;

		    return $config;
		}
	public function Shipping($_default_supplier='')
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Shipping";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱



		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>列表</small></h1>";
	    $this->page_data['_caption_subject']= $_ct_name."列表";

	    // 返回連結
	    $this->page_data['_father_link']=$_url;
	    $this->page_data['_default_supplier']=urldecode($_default_supplier);
	    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."add";

    /*
    	Begin  model 查詢結果
      回傳  _table_thead_name _body_result
    */
    //特殊條件  $_where="  order by sort_order asc";
    $_where="";

    $_result=$this->$_model_name->table_value_list($_where);

    /* End  model 查詢結果，回傳 */

    // 列表標頭 backend_system/Status_ct
    $this->page_data['_table_thead_name']= $_result["_table_thead_name"];
    if($this->session->userdata('member_role_system_rules')[0]['role_name']=='供應商管理員'){
    	//var_dump($this->page_data['_table_thead_name']);
    	unset($this->page_data['_table_thead_name']['supplier_sn']);
    }
	    // 列表內容
	    $this->page_data['_body_result']= $_result["_body_result"];

		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'admin/page_system/shipping/table';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'admin/page_system/shipping/table_js';
		$this->load->view('sean_admin/general/main', $this->page_data);
	}

	public function Shipping_Item($_action,$_id = 0)
	{

		/* Begin 修改 */
		//載入 model
		$_model_name="Shipping";
		/* END 修改 */


		$this->load->model('backend_system/'.$_model_name);
		$_url=$this->$_model_name->get_define_value("_url"); //列表功能url
		$_url_item=$this->$_model_name->get_define_value("_url_item"); //處理動作 url
		$_ct_name=$this->$_model_name->get_define_value("_ct_name"); // 功能名稱
		$_key=$this->$_model_name->get_define_value("_id");
		$_table_field=$this->$_model_name->get_define_value("_table_field");
($_id > 0 ) ? $this->$_model_name->set_define_value("_key_id",$_id) : "";
(strlen($_action) > 0 ) ? $this->$_model_name->set_define_value("_action",$_action) : "";


    // 返回連結
    $this->page_data['_father_link']=$_url;
    $this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."adding/$_id";
	$this->page_data['supplier_list'] = $this->libraries_model->_select('ij_supplier',array('supplier_status'=>'1'),0,0,0,'supplier_name',0,'list','supplier_sn,supplier_name');
	$this->page_data['supplier_list'][0]='請選擇';


    //取得下一個排序值
    $_next_sort=1;
    $_next_sort=$this->$_model_name->get_next_sort();
    //編輯
    if($_action=="edit" or $_action=="view")
	{
    	$this->page_data['_caption_subject']= $_ct_name."修改";
		//標題
		$this->page_data['_page_title']="<h1>".$_ct_name." <small>修改</small></h1>";

		$_result="";
		$_where=" where ".$_key."='".$_id."'";
		$_result=$this->$_model_name->action_edit($_where,$_id);

		$this->page_data['_action_link']=$this->page_data['init_control'].$_url_item."update/$_id";
		$this->page_data['_result01']=$_result;
    	//echo '<pre>' . var_export($_result, true) . '</pre>';
	}

		//更新
		if($_action=="update")
		{

			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_update($_where,$_id);

			//$this->session->set_flashdata("success_message","更新完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();

		}

		// 刪除
    if($_action=="delete")
		{
			$_where=" where ".$_key."='".$_id."'";
			$_result=$this->$_model_name->action_delete($_where,$_id);

			$this->session->set_flashdata("success_message","刪除完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}

    //新增
		if($_action == "adding")
		{
			$_result=$this->$_model_name->action_insert();
			$this->session->set_flashdata("success_message","新增完成！");
			redirect($this->page_data['init_control'].$this->page_data['_father_link']);
			exit();
		}
		if($_action == "add"){
			//標題
			$this->page_data['_page_title']="<h1>".$_ct_name." <small>新增</small></h1>";
			$this->page_data['_caption_subject']= $_ct_name."新增";
		}



	  /* Begin 修改 */

    /*		產生表單 */

    $this->page_data["em_columns"] = array(
			'supplier_sn'=>array('header'=>$_table_field["supplier_sn"],'type'=>($_action=="view") ? 'label':'enum', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>@$_result[0]->supplier_sn, 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50','source'=>$this->page_data['supplier_list']),

			'delivery_method_name'=>array('header'=>$_table_field["delivery_method_name"],'type'=>($_action=="view") ? 'label':'textbox', 'required'=>'required', 'required_message'=>'','form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'delivery_amount'=>array('header'=>$_table_field["delivery_amount"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'outland_amount'=>array('header'=>$_table_field["outland_amount"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'description'=>array('header'=>$_table_field["description"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'參考：訂單滿 $[free_shop_money] 即可享有台灣本島免運費優惠(*部分特殊規格商品除外)','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'255', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			//'status'=>array('header'=>$_table_field["status"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'sort_order'=>array('header'=>$_table_field["sort_order"], 'type'=>($_action=="view") ? 'label':'number', 'required'=>'required','required_message'=>'','form_control'=>'','maxlength'=>'3', 'default'=>$_next_sort,  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			//'status'=>array('header'=>$_table_field["status"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			//'create_date'=>array('header'=>$_table_field["create_date"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			//'create_member_sn'=>array('header'=>$_table_field["create_member_sn"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			//'update_member_sn'=>array('header'=>$_table_field["update_member_sn"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			//'last_time_update'=>array('header'=>$_table_field["last_time_update"], 'type'=>($_action=="view") ? 'label':'textbox','required'=>'','required_message'=>'', 'form_control'=>'date-picker', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'maxlength'=>'100', 'default'=>'', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
			'default_flag'=>array('header'=>$_table_field["default_flag"], 'type'=>($_action=="view") ? 'label':'default_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),
	 	 	'display_flag'=>array('header'=>$_table_field["display_flag"], 'type'=>($_action=="view") ? 'label':'display_flag', 'required_message'=>'','required'=>'','form_control'=>'','maxlength'=>'3', 'default'=>'',  'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'align'=>'left', 'width'=>'500px', 'title'=>'', 'readonly'=>'false', 'unique'=>'false', 'unique_condition'=>'', 'visible'=>'true', 'on_js_event'=>'', 'edit_type'=>'simple', 'resizable'=>'both', 'upload_images'=>'false', 'rows'=>'15', 'cols'=>'50'),

	 );
	if($this->session->userdata['member_data']['associated_supplier_sn']){
		unset($this->page_data["em_columns"]['supplier_sn']);
	}
		// 頁面主內容 view 位置, 必要
		$this->page_data['page_content']['view_path'] = 'admin/page_system/shipping/item';
		// 頁面主內容的 JS view 位置, 必要
		$this->page_data['page_level_js']['view_path'] = 'admin/page_system/shipping/item_js';
		$this->load->view('admin/general/main', $this->page_data);
	}
}
