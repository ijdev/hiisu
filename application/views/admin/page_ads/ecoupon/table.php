<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>行銷管理 <small>優惠代碼設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">首頁</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        優惠活動
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/marketing/ecoupon">優惠代碼</a>
      </li>
    </ul>
    <!-- BEGIN PAGE CONTENT INNER -->
      		<form class="form-inline" role="form" method="get">
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">優惠代碼</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/marketing/ecouponItem" class="btn green"><i class="fa fa-plus"></i> 新增優惠代碼</a>
            </div>
          </div>
          <div class="table-actions-wrapper pull-right">
          </div>
          <div class="portlet-body">
            <div class="note note-warning">
              	<div class="pull-left radio-list">
	                代碼類型：
									<label class="radio-inline" style="padding-left:0px;"><input type="radio" name="search[coupon_code_type]" <?php echo(@$search['coupon_code_type']=='1')?'checked':'';?> value="1">type1: 一個優惠代碼，可以讓N個會員使用</label><br>
									<label class="radio-inline" style="padding-left:68px;"><input type="radio" name="search[coupon_code_type]" <?php echo(@$search['coupon_code_type']=='2')?'checked':'';?> value="2">type2: 每個優惠代碼只能用一次，不能重覆使用</label><br>
									<label class="radio-inline" style="padding-left:68px;"><input type="radio" name="search[coupon_code_type]" <?php echo(@$search['coupon_code_type']=='3')?'checked':'';?> value="3">type3: 一個會員只有能有一組優惠代碼，重覆使用</label><br>
								</div>
								<div class="table-actions-wrapper pull-right radio-list">
                折扣類型：
								<label class="radio-inline" style="padding-left:0px;"><input type="radio" name="search[discount_type]" <?php echo(@$search['discount_type']=='2')?'checked':'';?> id="discount_type2" value="2">百分比</label>
								<label class="radio-inline" style="padding-left:0px;"><input type="radio" name="search[discount_type]" <?php echo(@$search['discount_type']=='1')?'checked':'';?> id="discount_type1" value="1">金額</label> &nbsp; 
                適用產品分類：<input name="search[apply_category]" type="text" class="pagination-panel-input form-control input-inline input-sm" size="8" style="margin: 0 5px;" placeholder="請用;區隔" value="<?php echo $search['apply_category'];?>" > &nbsp; 
                適用產品：<input name="search[apply_product]" type="text" class="pagination-panel-input form-control input-inline input-sm" size="8" style="margin: 0 5px;" placeholder="請用;區隔" value="<?php echo $search['apply_product'];?>" > &nbsp; <br>
                <br>適用經銷商代號：<input name="search[apply_dealer]" type="text" class="pagination-panel-input form-control input-inline input-sm" size="8" style="margin: 0 5px;" placeholder="請用;區隔" value="<?php echo $search['apply_dealer'];?>" > &nbsp; 
                <select name="search[apply_TA]" class="table-group-action-input form-control input-inline input-sm" style="font-size:11px;">
                  <option value="">適用對象</option>
                  <option <?php echo($search['apply_TA']=='1;1;1')?'selected':'';?> value="1;1;1">全部會員適用</option>
                  <option <?php echo($search['apply_TA']=='1;1;0')?'selected':'';?> value="1;1;0">經銷商,聯盟適用</option>
                  <option <?php echo($search['apply_TA']=='1;0;0')?'selected':'';?> value="1;0;0">僅經銷商</option>
                  <option <?php echo($search['apply_TA']=='0;1;0')?'selected':'';?> value="0;1;0">僅聯盟</option>
                  <option <?php echo($search['apply_TA']=='0;0;1')?'selected':'';?> value="0;0;1">僅一般</option>
                  <option <?php echo($search['apply_TA']=='1;0;1')?'selected':'';?> value="1;0;1">經銷商,一般適用</option>
                  <option <?php echo($search['apply_TA']=='0;1;1')?'selected':'';?> value="0;1;1">聯盟,一般適用</option>
                </select> &nbsp; 
                <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button> &nbsp; 
                <button id="reset_form" onclick="this.form.flg.value='reset';" class="btn btn-sm purple"><i class="fa"></i> 清除</button>
          			<input name="flg" type="hidden" value="">
              </div>
              <div style="clear:both;"></div>
            </div>
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <!--th>設定代號</th-->
                  <th>活動名稱</th>
                  <th>起始日期</th>
                  <th>結束日期</th>
                  <th>折扣類型</th>
                  <th>代碼類型</th>
                  <th>備註</th>
                  <th width="160">功能</th>
                </tr>
              </thead>
              <tbody>
								<?php if($Items){ foreach($Items as $_Item){?>
                  <tr class="odd gradeX">
                    <!--td><?php echo $_Item['coupon_code_config_sn'];?></td-->
                    <td><?php echo $_Item['coupon_code_event_name'];?></td>
                    <td><small><?php echo date_format(date_create($_Item['event_start_date']),"Y/m/d");?></small></td>
                    <td><small><?php echo date_format(date_create($_Item['event_end_date']),"Y/m/d");?></small></td>
                    <td class="center"><?php echo ($_Item['discount_type']=='1')?'金額':'百分比';?></td>
                    <td class="center">type<?php echo $_Item['coupon_code_type'];?></td>
                    <td class="center"><?php echo $_Item['memo'];?></td>
                    <td class="center">
                      <a href="admin/marketing/ecouponItem/<?php echo $_Item['coupon_code_config_sn'];?>"><i class="fa fa-edit"></i>設定</a> &nbsp;&nbsp;
                      <a href="admin/marketing/ecouponReport/<?php echo $_Item['coupon_code_config_sn'];?>"><i class="fa fa-bar-chart-o"></i>明細</a> &nbsp;&nbsp;
                      <?php if(!$_Item['ifused']){?>
<?php if(stripos($this->ijw->check_permission(1,$this->router->fetch_class(),$this->router->fetch_method()),'del')){?>
                      	<a href="javascript:void(0);" class="deleteItem" ItemId="<?php echo $_Item['coupon_code_config_sn'];?>"><i class="fa fa-trash-o"></i>刪除</a>
<?php }?>
                      <?php }?>
                    </td>
                  </tr>
                <?php }}?>
              </tbody>
            </table>

          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
                  	</form>

      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->