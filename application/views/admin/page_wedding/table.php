<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>婚禮網站列表</h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">婚禮網站列表</span>
            </div>
             <div class="actions btn-set">
              <a href="javascript:void(0);" data-remote="false" data-target="#addnew" data-toggle="modal" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
<!--form id="form1" class="form-horizontal" role="form" method="get">
          <div class="note note-warning">
            <div class="input-group input-large date-picker input-daterange pull-left" data-date="2015-05-07" data-date-format="yyyy-mm-dd">
              <span class="input-group-addon"> 期間 </span>
              <input type="text" class="form-control input-sm" name="orderTable[from]" value="<?php echo @$data_array['from'];?>">
              <span class="input-group-addon"> 到 </span>
              <input type="text" class="form-control input-sm" name="orderTable[to]" value="<?php echo @$data_array['to'];?>">
            </div>
            <div class="table-actions-wrapper pull-right">
              <span>
              </span>
              <input name="orderTable[user_name]" type="text" value="<?php echo @$data_array['user_name'];?>" class="pagination-panel-input form-control input-inline input-sm" size="8" style="margin: 0 5px;" placeholder="會員帳號">
              <?php echo $Store;?>
              <select name="orderTable[sub_order_status]" class="table-group-action-input form-control input-inline  input-sm" style="font-size:11px;">
                <option value="">訂單狀態</option>
              <?php foreach($sub_order_status as $_Item){ ?>
                <option <?php echo ($data_array['sub_order_status']==$_Item['sub_order_status'])?'selected':'';?> value="<?php echo $_Item['sub_order_status']?>"><?php echo $_Item['sub_order_status_name']?></option>
              <?php }?>
              </select>
              <select name="orderTable[payment_status]" class="table-group-action-input form-control input-inline  input-sm" style="font-size:11px;">
                <option value="">付款狀態</option>
              <?php foreach($payment_status as $_Item){?>
                <option <?php echo ($data_array['payment_status']==$_Item['payment_status'])?'selected':'';?> value="<?php echo $_Item['payment_status']?>"><?php echo $_Item['payment_status_name']?></option>
              <?php }?>
              </select>
              <select name="orderTable[admin_user]" class="table-group-action-input form-control input-inline  input-sm" style="font-size:11px;">
                <option value="">客服人員</option>
              <?php foreach($admin_users as $_Item){?>
                <option <?php echo ($data_array['admin_user']==$_Item['member_sn'])?'selected':'';?> value="<?php echo $_Item['member_sn']?>"><?php echo $_Item['last_name'];?></option>
              <?php }?>
              </select>
              <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button>
            </div>
            <div style="clear:both;"></div>
          </div>
</form-->
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>網站編號</th>
                  <th>會員姓名</th>
                  <th>訂單代號</th>
                  <th>版型代號</th>
                  <th>產品代號</th>
                  <th>網站標題</th>
                  <th>新人名字</th>
                  <th>婚禮日期</th>
                  <th>自訂網址</th>
                  <th>關聯廠商</th>
                  <th class="text-center" width="100">功能</th>
                </tr>
              </thead>
              <tbody>
                <?php if($orders){foreach($orders as $key=>$_Item){
                	$couple_name=json_decode($_Item['wedding_couple_name']);?>
                  <tr class="odd gradeX">
                    <td><small><?php echo $_Item['wedding_website_sn'];?></small></td>
                    <td><a target="_blank" href="/admin/Member/memberTable_item/edit/<?php echo $_Item['member_sn'];?>"><?php echo $_Item['last_name'].$_Item['first_name'];?><i class="fa fa-external-link"></i></a></td>
                    <td><?php echo $_Item['associated_order_sn'];?></td>
                    <td><?php echo $_Item['associated_product_template_sn'];?></td>
                    <td><?php echo $_Item['associated_product_sn'];?></td>
                    <td><?php echo $_Item['wedding_website_title'];?></td>
                    <td><?php echo ($couple_name[0])? @$couple_name[0].'＆'.@$couple_name[1]:'';?></td>
                    <td><?php echo $_Item['wedding_date'];?></td>
                    <td><?php echo $_Item['customize_website_url'];?></td>
                    <td><?php echo $_Item['member_dealer'];?></td>
                    <td align="center" class="text-center" style="display:flex;">
                      <a class="btn btn-xs btn-info" href="admin/admin/weddingWebsite_item/<?php echo $_Item['wedding_website_sn'];?>" member_sn="<?php echo $_Item['member_sn'];?>" wedding_website_sn="<?php echo $_Item['wedding_website_sn'];?>" data-remote="false" data-target="#ajax_msg" data-toggle="modal">設定</a>&nbsp;
                      <a class="btn btn-xs btn-warning edit" member_sn="<?php echo $_Item['member_sn'];?>" associated_product_template_sn="<?php echo $_Item['associated_product_template_sn'];?>" href="javascript:void(0);">編輯</a> &nbsp;&nbsp;
                      <!--a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a-->
                    </td>
                  </tr>
                <?php }}?>
              </tbody>
            </table>

          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->

<div class="modal fade" id="ajax_msg" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">網站參數設定</h4>
      </div>
      <div class="modal-body">
        <img src="public/metronic/global/img/loading-spinner-grey.gif" alt="" class="loading">
        <span>
        &nbsp;&nbsp;內容載入中... </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn blue">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addnew" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
  <h4 class="modal-title">新增婚禮網站</h4>
</div>
<form role="form" name="reply_form" method="post" action="admin/admin/weddingWebsite_addnew">
<div class="modal-body">
    <div class="row">
		  <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	          <label class="col-lg-3 control-label">所屬會員代號</label>
	      			<div class="col-lg-4 col-md-4 col-sm-4">
		        			<input id="member_sn" required type="text" class="form-control" name="wedding_website[member_sn]" value="">
		      		</div>
	      			<div class="col-lg-5 col-md-5 col-sm-5">
		        			<label class="control-label">會員姓名：</label><span id="member_name"></span>
		      		</div>
		      </div>
		  </div>
		  <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	          <label class="col-lg-3 control-label">商品名稱</label>
	      			<div class="col-lg-4 col-md-4 col-sm-4">
                  <select required name="wedding_website[associated_product_sn]" id="associated_product_sn" required class="form-control">
                    <option value="">請選擇</option>
                    <?php foreach(@$templates as $_Item){?>
                    <option associated_product_template_sn="<?php echo $_Item['product_template_sn']?>" <?php echo (@$_Item['product_name']=='')?'selected':'';?> value="<?php echo @$_Item['product_sn']?>"><?php echo @$_Item['product_name']?></option>
                    <?php }?>
                  </select>
		      		</div>
		      </div>
	    </div> 
		  <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	          <label class="col-lg-3 control-label">規格款式</label>
	      			<div class="col-lg-4 col-md-4 col-sm-4">
                  <select required name="wedding_website[product_package_config_sn]" id="product_package_config_sn" required class="form-control">
                    <option value="">請先選擇商品</option>
                  </select>
		      		</div>
		      </div>
	    </div>
     		<div class="col-lg-12 col-md-12 col-sm-12">
		  <div class="form-group">
	          <label class="col-lg-3 control-label">密碼保護</label>
	      			<div class="col-lg-4 col-md-4 col-sm-4">
                <div class="radio-list">
                  <label class="radio-inline">
                    <input class="radio-list" required name="wedding_website[password_flag]" id="password_flag1" type="radio" class="form-control" value="1" <?php echo (@$Item['free_delivery']=='1') ?'checked':''; ?> >&nbsp;是
                  </label>
                  <label class="radio-inline">
                    <input class="radio-list" required name="wedding_website[password_flag]" id="password_flag2" type="radio" class="form-control" value="0" <?php echo (@$Item['free_delivery']=='0') ?'checked':''; ?> >&nbsp;否
                  </label>
                </div>
		      		</div>
		      </div>
	    </div>	    
		  <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	          <label class="col-lg-3 control-label">可使用天數</label>
	      			<div class="col-lg-4 col-md-4 col-sm-4">
		        			<input type="text" required class="form-control" name="wedding_website[website_usable_duration]" id="website_usable_duration" value="">
		      		</div>
		      </div>
	    </div> 
		  <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	          <label class="col-lg-3 control-label">可上傳相片數量</label>
	      			<div class="col-lg-4 col-md-4 col-sm-4">
		        			<input type="text" required class="form-control" name="wedding_website[upload_pic_total_amount]" id="upload_pic_total_amount" value="">
		      		</div>
		      </div>
	    </div> 
		  <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	          <label class="col-lg-3 control-label">可分享相片數量</label>
	      			<div class="col-lg-4 col-md-4 col-sm-4">
		        			<input type="text" required class="form-control" name="wedding_website[album_pic_total_amount]" id="album_pic_total_amount" value="">
		      		</div>
	      			
		      </div>
	    </div>
		  <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	          <label class="col-lg-3 control-label">可使用影片數量</label>
	      			<div class="col-lg-4 col-md-4 col-sm-4">
		        			<input type="text" required class="form-control" name="wedding_website[album_video_total_amount]" id="album_video_total_amount" value="">
		      		</div>
		      </div>
	    </div>
		  <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
	          <label class="col-lg-3 control-label">可使用簡訊數</label>
	      			<div class="col-lg-4 col-md-4 col-sm-4">
		        			<input type="text" required class="form-control" name="wedding_website[sms_total_amount]" id="sms_total_amount" value="">
		      		</div>
		      </div>
	    </div>	
    </div>
</div>

  <div class="modal-footer">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6">
        <button data-dismiss="modal" style="margin-top: 20px;margin-right: 100px;" type="button" id="cancelbutton" class="btn yellow">取消</button> &nbsp; 
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6">
        <button style="margin-top: 20px;margin-right: 100px;" type="submit" id="replybutton" class="btn blue">送出</button>
		    <input type="hidden" name="wedding_website[associated_product_template_sn]" id="associated_product_template_sn" value="">
		    <input type="hidden" name="promo_price" id="promo_price" value="">
		    <input type="hidden" name="order_item[product_name]" id="product_name" value="">
		    <input type="hidden" name="order_item[product_package_name]" id="product_package_name" value="">
      </div>
    </div>
  </div>
</form>
</div></div></div>