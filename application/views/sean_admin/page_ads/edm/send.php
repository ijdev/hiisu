<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>行銷管理 <small>電子報發送</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
<div class="page-content">
  <div class="container">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin-ijwedding/Ads">首頁</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        優惠活動
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin-ijwedding/Ads/edm">電子報管理</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        電子報發送
      </li>
    </ul>
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">發送電子報</span>
            </div>
            <div class="actions btn-set">
            </div>
          </div>
          <div class="table-actions-wrapper pull-right">
          </div>
          <div class="portlet-body">
            <div class="note note-info">
              <div class="table-actions-wrapper form">
                <form class="form-horizontal" role="form">
                  <div class="form-body">
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label class="radio-inline">
                          <input type="radio" name="sendfilter"> 按會員發送
                        </label>
                      </div>
                      <div class="col-md-9">
                        <textarea class="form-control" rows="3" placeholder="輸入會員 Email, 多個請以逗號隔開"></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label class="radio-inline">
                          <input type="radio" name="sendfilter"> 按商品發送
                        </label>
                      </div>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon">由</span>
                          <input type="text" class="form-control form-control-inline input-small date-picker" value="2015-05-08">
                          <span class="input-group-addon">到</span>
                          <input type="text" class="form-control form-control-inline input-small date-picker" value="2015-05-09">
                          <span class="input-group-addon"> 購買過商品編號 </span>
                          <input type="text" class="form-control form-control-inline input-small" value="" placeholder="商品編號">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label class="radio-inline">
                          <input type="radio" name="sendfilter"> 按訂單金額發送
                        </label>
                      </div>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon">由</span>
                          <input type="text" class="form-control form-control-inline input-small date-picker" value="2015-05-08">
                          <span class="input-group-addon">到</span>
                          <input type="text" class="form-control form-control-inline input-small date-picker" value="2015-05-09">
                          <span class="input-group-addon"> 訂單金額超過 </span>
                          <input type="text" class="form-control form-control-inline input-small" value="" placeholder="金額下限">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label class="radio-inline">
                          <input type="radio" name="sendfilter"> 按會員產業屬性發送
                        </label>
                      </div>
                      <div class="col-md-9">
                        <input type="hidden" class="form-control select2_category" value="飯店1, 婚紗2">
                        <!-- select option 在 send_js 中 -->
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label class="radio-inline">
                          <input type="radio" name="sendfilter"> 按聯盟會員發送
                        </label>
                      </div>
                      <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="輸入經銷商編號">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label class="radio-inline">
                          <input type="radio" name="sendfilter"> 按聯盟會員產業屬性發送
                        </label>
                      </div>
                      <div class="col-md-9">
                        <input type="hidden" class="form-control select2_category" value="飯店1, 婚紗2">
                        <!-- select option 在 send_js 中 -->
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label class="radio-inline">
                          <input type="radio" name="sendfilter"> 按經銷商發送
                        </label>
                      </div>
                      <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="輸入經銷商編號">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label class="radio-inline">
                          <input type="radio" name="sendfilter"> 按經銷商產業屬性發送
                        </label>
                      </div>
                      <div class="col-md-9">
                        <input type="hidden" class="form-control select2_category" value="飯店1">
                        <!-- select option 在 send_js 中 -->
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label class="radio-inline">
                          <input type="radio" name="sendfilter"> 按結婚月份
                        </label>
                      </div>
                      <div class="col-md-9">
                        <input type="text" class="form-control form-control-inline input-small date-picker1" data-date="2015-05" value="2015-05">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="radio-list col-md-3">
                        <label class="radio-inline">
                          <input type="radio" name="sendfilter"> 按結婚日期
                        </label>
                      </div>
                      <div class="col-md-9">
                        <div class="input-group input-large date-picker input-daterange  pull-left" data-date="2015-05-01" data-date-format="yyyy-mm-dd">
                          <input type="text" class="form-control input-sm" name="from" value="2015-05-01">
                          <span class="input-group-addon"> 到 </span>
                          <input type="text" class="form-control input-sm" name="to" value="2015-06-01">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-actions right1">
                    <button class="btn btn-sm yellow"><i class="fa fa-check"></i> 加入</button>
                  </div>
                </div>
              </form>
              <div style="clear:both;"></div>
            </div>
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>發送條件</th>
                  <th>發送數量</th>
                  <th>使用狀況</th>
                  <th>寄送日期</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=0;$i<8;$i++):?>
                  <tr class="odd gradeX">
                    <td>
                      產業屬性含飯店1, 飯店2
                    </td>
                    <td class="center">200</td>
                    <td class="center">100</td>
                    <td>2015-06-01</td>
                    <td class="center">
                      <a href="admin-ijwedding/Ads/edmSendList"><i class="fa fa-users"></i>瀏覽</a>&nbsp;&nbsp;&nbsp;
                      <a href="javascript:;" onclick="alert('開始寄發');"><i class="fa fa-envelope-o"></i>寄送</a>
                    </td>
                  </tr>
                <?php endfor;?>
              </tbody>
            </table>

            <div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                  <ul class="pagination" style="visibility: visible;">
                    <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                    <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li class="active"><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                    <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->