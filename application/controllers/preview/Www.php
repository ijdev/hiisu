<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
* for template preview, develope area of jerry
*/

class Www extends CI_Controller {

	/**
	 * 首頁 Template
	 *
	 */
	public function index()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		$page_data['slider'] = 'preview/www/page_home/slider'; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，非必要


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_home/page_content'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_home/page_content_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	/**
	 * 登入頁面 Template
	 *
	 */
	public function memberSignIn()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_member/page_sign_in'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_member/page_sign_in_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	/**
	 * 註冊頁面 Template
	 *
	 */
	public function memberSignUp()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_member/page_sign_up'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_member/page_sign_up_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	/**
	 * 註冊成功 Template，失敗回到原畫面並顯示錯誤訊息
	 *
	 */
	public function memberSignUpOK()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_member/page_sign_up_result'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_member/page_sign_up_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	
	/**
	 * Wish List Template
	 *
	 */
	public function wishList()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_member/page_wish_list'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_member/page_wish_list_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	
	/**
	 * 會員查詢購物金 Template
	 *
	 */
	public function credit()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_member/page_credit'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_member/page_credit_js'; // 頁面主內容的 JS 由這個參數帶入, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}


	/**
	 * VIP 會員申請 Template
	 *
	 */
	public function affiliate($status='form')
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array('status' => $status); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_member/page_affiliate'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	/**
	 * VIP 會員獎金查詢 Template
	 *
	 */
	public function benefit()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_member/page_benefit'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	/**
	 * 會員資料修改 Template
	 *
	 */
	public function memberAccount()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_member/page_account'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_member/page_account_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	/**
	 * 忘記密碼頁面 Template
	 *
	 */
	public function memberForgotPassword()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_member/page_forgot_password'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_member/page_forgot_password_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	
	/**
	 * 會員條款
	 *
	 */
	public function rule()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_member/page_rule'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_member/page_rule_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	
	/**
	 * privacy policy
	 *
	 */
	public function privacy()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_member/page_privacy_policy'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_member/page_privacy_policy_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	
	/**
	 * TERMS & CONDITIONS
	 *
	 */
	public function trems()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_member/page_tc'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_member/page_tc_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	/**
	 *  客服中心 Template + 表單
	 *
	 */
	public function callcenter()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_callcenter/page_ticket'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_callcenter/page_ticket_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	/**
	 *  客服中心回應內容
	 *	直接顯示在 model 中
	 *
	 */
	public function callcenterMsg($done = false)
	{
		$page_data = array();

		// 內容設定
		if($done)
		{
			// 若為已解決， modal footer 不顯示
			$page_data['done'] = true;
		}
		else
		{
			$page_data['done'] = false;
		}

		$this->load->view('preview/www/page_callcenter/modal_ticket_msg', $page_data);
	}

	/**
	 * 訂單查詢 Template
	 *
	 */
	public function memberOrderList()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_member/page_order'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_member/page_order_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	/**
	 * 訂單明細查詢 Template
	 *
	 */
	public function memberOrderDetail()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_member/page_order_detail'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_member/page_order_detail_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}


	/**
	 * ==============   SHOP TEMPLATE AREA     ==============
	 */

	/**
	 * 線上商店首頁 Template
	 *
	 */
	public function shop()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		$page_data['slider'] = 'preview/www/page_shop/slider'; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，非必要


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_shop/page_home'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_shop/page_home_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	/**
	 * 線上商店分類頁 Template
	 *
	 */
	public function shopCatalog()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設
		$page_data['title_wrapper'] = 'preview/www/page_shop/title_wrapper'; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，非必要


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_shop/page_catalog'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_shop/page_catalog_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	/**
	 * 線上商店商品頁 Template
	 *
	 */
	public function shopItem()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_shop/page_item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_shop/page_item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	/**
	 * 線上商店商品清單 Template
	 *
	 */
	public function shopView()
	{
		$page_data = array();

		// 內容設定

		// page level setting


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_shop/page_cart_view'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_shop/page_cart_view_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	/**
	 * 線上商店付款流程 Template
	 *
	 */
	public function shopCheckout()
	{
		$page_data = array();

		// 內容設定

		// page level setting


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_shop/page_checkout'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_shop/page_checkout_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}


	/**
	 * ==============   PARTNERS TEMPLATE AREA     ==============
	 */

	/**
	 * 合作夥伴專頁 Template
	 *
	 */
	public function partners()
	{
		$page_data = array();

		// 內容設定

		// page level setting

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview_partners/page_home'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview_partners/page_home_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview_partners/main', $page_data);
	}

	/**
	 * 登入頁面 Template
	 *
	 */
	public function partnersSignIn()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$this->load->view('preview_partners/page_sign_in', $page_data);
	}

	/**
	 * 註冊頁面 Template
	 *
	 */
	public function partnersSignUp()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$this->load->view('preview_partners/page_sign_up', $page_data);
	}

	/**
	 * FAQ列表頁面 Template
	 *
	 */
	public function faq()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_blog/faq'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_blog/faq_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	/**
	 * 部落格列表頁面 Template
	 *
	 */
	public function blogList()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_blog/list'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_blog/list_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	/**
	 * 部落格文章頁面 Template
	 *
	 */
	public function blogItem()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_blog/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_blog/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	/**
	 * 雲端婚訊 Template
	 *
	 */
	public function ewedding($type='home', $segment = '')
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設

		switch($type){
			case "faq":
				$this->faq($segment);
			break;
			case "template":
				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'preview/www/page_ewedding/list'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'preview/www/page_ewedding/list_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('preview/www/general/main', $page_data);
			break;
			default:
			case "home":
				$page_data['slider'] = 'preview/www/page_ewedding/slider'; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，非必要

				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'preview/www/page_ewedding/home'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'preview/www/page_ewedding/home_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('preview/www/general/main', $page_data);
			break;
		}
	}

	/**
	 * 產品 Detail 光箱 Template
	 *
	 */
	public function templatePreview($templateID='ID')
	{
		$page_data = array();

		// 內容設定
		$page_data['templateID'] = $templateID;

		// page level setting


		// general view setting
		$this->load->view('preview/www/page_ewedding/modal_template', $page_data);
	}

	/**
	 * 現場報到系統頁面 Template
	 *
	 */
	public function checkIn($type='home',$segment='')
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		switch($type){
			case "faq":
				$this->faq($segment);
			break;
			default:
			case "home":
				$page_data['slider'] = 'preview/www/page_checkin/slider'; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，非必要

				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'preview/www/page_checkin/item'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'preview/www/page_checkin/item_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('preview/www/general/main', $page_data);
			break;
		}
	}

	/**
	 * 全站通知 Template
	 *
	 */
	public function notification($type='')
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		switch($type){
			case "detail":
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'preview/www/page_member/notification_item'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'preview/www/page_member/notification_item_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('preview/www/general/main', $page_data);
			break;
			default:
				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'preview/www/page_member/notification'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'preview/www/page_member/notification_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('preview/www/general/main', $page_data);
			break;
		}
	}

	/**
	 * 搜尋結果 Template
	 *
	 */
	public function shopSearch()
	{
		$page_data = array();

		// 內容設定

		// page level setting
		// $page_data['slider'] = ''; // 有 slide 的話透過這個參數($page_data['slider'])設定帶入 view 的位置，無可以不用設


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_shop/page_search'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_shop/page_search_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('preview/www/general/main', $page_data);
	}

	/*
	* 會員中心中個人雲端婚訊與報到系統列表
	*/
    public function my($page='ewedding')
    {
        $page_data = array();
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'preview/www/page_member/page_'.$page; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js']['view_path'] = 'preview/www/page_member/page_'.$page.'_js'; // 頁面主內容的 JS 由這個參數帶入, 必要
		$this->load->view('preview/www/general/main', $page_data);
    }

	/**
	 * 經銷商頁面 Template
	 *
	 */
	public function dealer($page='home')
	{
		$page_data = array();
		if($page!='main'){
			$page_data['mainContain'] = $this->load->view('preview/www/page_dealer/'.$page, $page_data, true);
		}
		$this->load->view('preview/www/page_dealer/template', $page_data);
	}
}
