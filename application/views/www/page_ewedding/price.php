<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Home 主內容
*/
?>

<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<link href="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="public/metronic/global/plugins/slider-layer-slider/css/layerslider.css" rel="stylesheet">
<!-- Page level plugin styles END -->
<style type="text/css">
  .table-price{
    width:100%;
  }
    .table-price .center{
      text-align: center;
    }
  .table-price tr{
    line-height: 2;
    border:0px;
    margin: 0px;
    padding: 0px;
  }
    .table-price tr.head{
      background-color: #CEBEA2;
      color:#FFF;
      font-size: 20px;
      line-height: 3;
      font-weight: normal;
    }
    .table-price tr td{
      text-align: center;
      border:0px;
      margin: 0px;
      padding: 0px;
    }
    .table-price tr.data{
      background-color: #FFF;
      color:#000;
      font-size: 18px;
      line-height: 3;
      font-weight: normal;
      border-right: 1px solid #CEBEA2;
    }
    .table-price tr.data td.tag{
      background-color: #CEBEA2;
      color:#FFF;
      border-top:1px solid #FFF;
      font-weight: bold;
      text-align: left;
      padding-left:30px;
    }
    .table-price tr.data td.tag.subtag{
      font-size:18px;
      text-align: right;
      padding-right:30px;
      font-weight: normal;
    }
    .table-price tr.data td.value{
      border-top:1px solid #CEBEA2;
      text-align: center;
    }
    .table-price tr.data.end td.value{
      border-bottom:1px solid #CEBEA2;
    }
    .table-price tr.data td.no-border{
      border:0px;
    }
    .table-price tr.outside{
      text-align: center;
      border:0px;
      font-size: 18px;
    }
    .btn-use{
      background-color: #F2B800;
      color:#FFF;
    }
</style>
<div class="container">
  <div class="row service-box margin-bottom-60" id="section-price">
    <h1>價格說明</h1>
    <div style="width:100%;" cellpadding="0" cellspacing="0">
      <table class="table-price">
        <tr class="head">
          <th width="22%"></th>
          <th width="26%" class="center">試用版</th>
          <th width="26%" class="center">標準版</th>
          <th width="26%" class="center">進階版</th>
        </tr>
        <tr class="data">
          <td class="tag">網站首頁</td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
        </tr>
        <tr class="data">
          <td class="tag">婚紗照片</td>
          <td class="value">６張</td>
          <td class="value">18張</td>
          <td class="value">30張</td>
        </tr>
        <tr class="data">
          <td class="tag">精彩影音</td>
          <td class="value"></td>
          <td class="value"></td>
          <td class="value"></td>
        </tr>
        <tr class="data">
          <td class="tag no-border subtag">相片分享</td>
          <td class="value no-border">10張</td>
          <td class="value no-border">100張</td>
          <td class="value no-border">200張</td>
        </tr>
        <tr class="data">
          <td class="tag no-border subtag">影片分享</td>
          <td class="value no-border">1部</td>
          <td class="value no-border">不限</td>
          <td class="value no-border">不限</td>
        </tr>
        <tr class="data">
          <td class="tag">婚禮訊息</td>
          <td class="value"></td>
          <td class="value"></td>
          <td class="value"></td>
        </tr>
        <tr class="data">
          <td class="tag no-border subtag">電子喜帖</td>
          <td class="value no-border">1場</td>
          <td class="value no-border">1場</td>
          <td class="value no-border">不限</td>
        </tr>
        <tr class="data">
          <td class="tag no-border subtag">出席回覆</td>
          <td class="value no-border">1場</td>
          <td class="value no-border">1場</td>
          <td class="value no-border">不限</td>
        </tr>
        <tr class="data">
          <td class="tag no-border subtag">簡訊提醒</td>
          <td class="value no-border"></td>
          <td class="value no-border">100則</td>
          <td class="value no-border">200則</td>
        </tr>
        <tr class="data">
          <td class="tag">愛的故事</td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
        </tr>
        <tr class="data">
          <td class="tag">留言祝福</td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
        </tr>
        <tr class="data">
          <td class="tag">線上禮金</td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
        </tr>
        <tr class="data">
          <td class="tag">感謝有你</td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
        </tr>
        <tr class="data">
          <td class="tag">婚宴日倒數</td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
        </tr>
        <tr class="data">
          <td class="tag">來訪客計數</td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
        </tr>
        <tr class="data">
          <td class="tag">社群分享 (FB G+)</td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
        </tr>
        <tr class="data">
          <td class="tag">密碼保護</td>
          <td class="value"></td>
          <td class="value"></td>
          <td class="value"><img src="public/img/icon-price-checked.png"></td>
        </tr>
        <tr class="data end">
          <td class="tag">使用期限</td>
          <td class="value">七天</td>
          <td class="value">6 個月</td>
          <td class="value">12 個月</td>
        </tr>
        <tr class="outside">
          <td class=""></td>
          <td class="">$ 0</td>
          <td class="">$ 3980</td>
          <td class="">$ 5980</td>
        </tr>
        <tr class="outside">
          <td class=""></td>
          <td class=""><a class="btn btn-use btn-lg" href="javascript:;">免費試用</a></td>
          <td class=""><a class="btn btn-use btn-lg" href="javascript:;">立即購買</a></td>
          <td class=""><a class="btn btn-use btn-lg" href="javascript:;">立即購買</a></td>
        </tr>
      </table>
    </div>
  </div>
  <div class="row margin-bottom-60 padding-top-40">
    <h1>可加購項目</h1>
    <div style="width:100%;" cellpadding="0" cellspacing="0">
      <table class="table-price">
        <tr class="head">
          <th colspan="4"><div style="text-align:center;">加購規格</div></th>
        </tr>
        <tr class="data">
          <td class="tag" width="22%"></td>
          <td class="value" style="background-color:#F9EDDC;color:#CEBEA2;" width="26%">數量</td>
          <td class="value" style="background-color:#F9EDDC;color:#CEBEA2;" width="26%">單位</td>
          <td class="value" style="background-color:#F9EDDC;color:#CEBEA2;" width="26%">價格</td>
        </tr>
        <tr class="data">
          <td class="tag">獨立網址</td>
          <td class="value"> </td>
          <td class="value"> </td>
          <td class="value"> NT$300</td>
        </tr>
        <tr class="data">
          <td class="tag">使用期限</td>
          <td class="value"> 90</td>
          <td class="value"> 天</td>
          <td class="value"> NT$300</td>
        </tr>
        <tr class="data">
          <td class="tag">婚紗照片 (張數)</td>
          <td class="value"> 20</td>
          <td class="value"> 張</td>
          <td class="value"> NT$200</td>
        </tr>
        <tr class="data">
          <td class="tag">精彩照片 (張數)</td>
          <td class="value"> 20</td>
          <td class="value"> 張</td>
          <td class="value"> NT$200</td>
        </tr>
        <tr class="data end">
          <td class="tag">喜宴提醒 (SMS)</td>
          <td class="value"> 50</td>
          <td class="value"> 則</td>
          <td class="value"> NT$100</td>
        </tr>
      </table>
      <table class="table-price">
        <tr class="head">
          <th colspan="4"><div style="text-align:center;">加購商品/服務</div></th>
        </tr>
        <tr class="data">
          <td class="tag" width="22%">代製作</td>
          <td class="value" width="26%"> 1</td>
          <td class="value" width="26%"> 次</td>
          <td class="value" width="26%"> NT$1,200</td>
        </tr>
        <tr class="data end">
          <td class="tag" width="22%">DVD 儲存版</td>
          <td class="value" width="26%"> 1</td>
          <td class="value" width="26%"> 次</td>
          <td class="value" width="26%"> NT$2,000</td>
        </tr>
      </table>
    </div>
  </div>
</div>



















