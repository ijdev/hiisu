<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>行銷管理 <small>兌換券出貨管理</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="preview">首頁</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        優惠活動
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="preview/certificateApply">兌換券管理</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        活動編輯
      </li>
    </ul>
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">兌換券出貨申請編輯</span>
            </div>
            <div class="tools">
              <a href="preview/certificateApply"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">訂單編號 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" placeholder="請輸入業務訂單編號">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">廠商名稱</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" value="廠商名稱由輸入名單帶入" disabled="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">申請數量 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" placeholder="100">
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">適用分類</label>
                  <div class="col-lg-8">
                    <input type="hidden" class="form-control select2_category" value="分類1, 分類2">
                    <!-- select option 在 item_js 中 -->
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-3 control-label">適用產品<span class="require">*</span></label>
                  <div class="col-lg-8">
                    <textarea class="form-control" rows="3"></textarea>
                    <span class="help-block">一行一個產品編號</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">起始時間</label>
                  <div class="col-lg-4">
                    <div class="input-group date form_datetime">
                      <input type="text" class="form-control date-picker" value="2015-05-08">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">結束時間</label>
                  <div class="col-lg-4">
                    <div class="input-group date form_datetime">
                      <input type="text" class="form-control date-picker" value="2015-05-08">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-3 control-label">價值估算</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" value="1000" disabled="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">合約影本</label>
                  <div class="col-lg-8">
                    <input type="file" id="exampleInputFile1">
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">審核完成</label>
                  <div class="col-lg-8">
                    <select class="form-control">
                      <option>是</option>
                      <option>否</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-lg-3 control-label">申請者</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="administrator" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-3 control-label">更新時間</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="2015-05-25 10:00" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">送出</button>
                    <button type="button" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->