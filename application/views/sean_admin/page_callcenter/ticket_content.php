<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>客服管理 <small>問題列表</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">問題列表</span>
            </div>
            <div class="tools">
              <a href="javascript:;" class="collapse">
              </a>
              <a href="javascript:;" class="reload">
              </a>
            </div>
          </div>
          <div class="portlet-body">
            <div class="note note-warning">
              <div class="input-group input-large date-picker input-daterange" data-date="2015-05-07" data-date-format="yyyy-mm-dd">
                <span class="input-group-addon"> 日期 </span>
                <input type="text" class="form-control" name="from" value="2015-05-07">
                <span class="input-group-addon"> 到 </span>
                <input type="text" class="form-control" name="to" value="2015-05-13">
              </div>
            </div>
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>表單時間</th>
                  <th>編號</th>
                  <th>Email</th>
                  <th>電話</th>
                  <th>問題類型</th>
                  <th>狀態</th>
                  <th>更新時間</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=0;$i<32;$i++):?>
                  <tr>
                    <td>2015-04-20</td>
                    <td><?php echo $i;?></td>
                    <td>cwei.wu@gmail.com</td>
                    <td>09220022555</td>
                    <td>帳號 / 密碼</td>
                    <td>
                      <a href="preview/callcenterMsg" data-target="#ajax_msg" data-toggle="modal" class="label label-sm label-info">處理中</a>
                      <!-- label-success(綠), info(藍), danger(紅), warning(黃), table : ij_callcenter_status -->
                    </td>
                    <td>2015-04-21</td>
                  </tr>
                <?php endfor;?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
<!-- Modal -->
<div class="modal fade" id="memberModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">編輯會員</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-without-legend" role="form">
          <fieldset>
            <div class="form-group">
              <label for="lastname" class="col-lg-4 control-label">姓 <span class="require">*</span></label>
              <div class="col-lg-8">
                <input type="text" class="form-control" id="lastname">
              </div>
            </div>
            <div class="form-group">
              <label for="firstname" class="col-lg-4 control-label">名 <span class="require">*</span></label>
              <div class="col-lg-8">
                <input type="text" class="form-control" id="firstname">
              </div>
            </div>
            <div class="form-group">
              <label for="email" class="col-lg-4 control-label">Email <span class="require">*</span></label>
              <div class="col-lg-8">
                <input type="text" class="form-control" id="email">
              </div>
            </div>
            <hr>
            <div class="form-group">
              <label for="password" class="col-lg-4 control-label">密碼 <span class="require">*</span></label>
              <div class="col-lg-8">
                <input type="text" class="form-control" id="password">
              </div>
            </div>
            <div class="form-group">
              <label for="confirm-password" class="col-lg-4 control-label">重複輸入密碼 <span class="require">*</span></label>
              <div class="col-lg-8">
                <input type="text" class="form-control" id="confirm-password">
              </div>
            </div>
          </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">儲存</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal 顯示與客服的訊息列表 -->
<div class="modal fade" id="ajax_msg" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">訊息記錄</h4>
      </div>
      <div class="modal-body">
        <img src="public/metronic/global/img/loading-spinner-grey.gif" alt="" class="loading">
        <span>
        &nbsp;&nbsp;內容載入中... </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn blue">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- /Modal 顯示與客服的訊息列表 -->
