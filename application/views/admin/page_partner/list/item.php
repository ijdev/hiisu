<?php defined('BASEPATH') OR exit('No direct script access allowed');
$_result_item=@$_result01[0];
$_result_item2=@$_result02[0];
$_partner_list2=@$_partner_list[0];
//var_dump($_partner_list);
?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>聯盟會員 <small>廠商設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        會員管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/member/partnerTable">聯盟會員</a>
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         聯盟會員設定
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">聯盟會員設定</span>
            </div>
            <div class="tools">
              <a href="admin/member/partnerTable"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
            <div class="tabbable">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_general" data-toggle="tab"> 一般設定 </a>
                </li>
                <?php if($_action=='edit'){?>
                <li>
                  <a href="#tab_dashboard" data-toggle="tab"> 績效總覽 </a>
                </li>
                <li>
                  <a href="#tab_member" data-toggle="tab"> 關聯會員 </a>
                </li>
              <?php }?>
              </ul>
              <div class="tab-content no-space">
                <!-- Tab 資料設定 -->
                <div class="tab-pane active" id="tab_general">
                    <form class="form-horizontal" method="post" role="form" action="<?php echo $_action_link;?>">
                    <div class="form-body">
                      <h4>帳號設定</h4>
                      <div class="form-group">
                        <label for="user_name" class="col-lg-3 control-label">帳號 <span class="require">*</span></label>
                        <div class="col-lg-3">
                          <input type="email" class="form-control" name="user_name" id="user_name" required value="<?php if(isset($_result_item->user_name)) echo $_result_item->user_name;?>">
                          <!--p class="form-control-static">
                            <?php if(isset($_result_item->user_name)) echo $_result_item->user_name;?>
                          </p-->
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="lastname" class="col-lg-3 control-label">會員名稱 <span class="require">*</span></label>
                        <div class="col-lg-3">
                          <input type="text" class="form-control" maxlength="32" name="last_name" required value="<?php if(isset($_result_item->last_name)) echo $_result_item->last_name;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="firstname" class="col-lg-3 control-label">簡稱 </label>
                        <div class="col-lg-3">
                          <input type="text" class="form-control" maxlength="32" name="first_name" value="<?php if(isset($_result_item->first_name)) echo $_result_item->first_name;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="email" class="col-lg-3 control-label">Email <span class="require">*</span></label>
                        <div class="col-lg-3">
                          <input type="email" class="form-control" id="email" name="email" required value="<?php if(isset($_result_item->email)) echo $_result_item->email;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">市話</label>
                        <div class="col-lg-3">
                          <input type="text" class="form-control" maxlength="16"  name="tel" id="tel" value="<?php if(isset($_result_item->tel)) echo $_result_item->tel;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="cell" class="col-lg-3 control-label">手機 <span>*</span></label>
                        <div class="col-lg-3">
                          <input type="text" class="form-control" required maxlength="10" name="cell" id="cell" value="<?php if(isset($_result_item->cell)) echo $_result_item->cell;?>">
                        </div>
                      </div>
                      <!--div class="form-group">
                        <label for="name" class="col-lg-3 control-label">傳真</label>
                        <div class="col-lg-3">
                          <input type="text" class="form-control" maxlength="16"  name="fax" id="fax" value="<?php if(isset($_result_item->fax)) echo $_result_item->fax;?>">
                        </div>
                      </div-->
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">住址</label>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <div class="col-md-4">
                          <select id="addr_city_code" name="addr_city_code"  class="table-group-action-input form-control " >
                              <option value="">縣市</option>
                            <?php
                              foreach($_city_ct as $keys=>$values){
                              $_selected="";
                              if(@$_result_item->addr_city_code == $keys)
                              $_selected=" selected";
                              echo  ' <option value="'.$keys.'" '.$_selected.'>'.$values.'</option>';
                            }?>
                            </select>
                           </div>
                           <div class="col-md-4">
                            <select  id="addr_town_code" name="addr_town_code" class="table-group-action-input form-control  " >
                              <option value="">鄉鎮市區</option>
                            </select>
                            </div>
                           <div class="col-md-4">
                              <input type="text" class="form-control" maxlength="5"  name="zipcode" id="zipcode" value="<?php if(isset($_result_item->zipcode)) echo $_result_item->zipcode;?>">
                           </div>
                          </div>
                         <input type="text"id="addr1" name="addr1"  class="pagination-panel-input form-control" maxlength="64"  size="12" style="margin: 0 5px;" placeholder="住址" value="<?php if(isset($_result_item->addr1)) echo $_result_item->addr1;?>">

                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">聯盟會員分級 <span> * </span></label>
                        <div class="col-lg-3">
                          <select class="form-control" id="partner_level_sn" required name="partner_level_sn">
                            <option value="0">請選擇</option>
                           <?php
                            if(isset($_result03) && is_array($_result03))
                            {
                                foreach($_result03 as $key => $value)
                                {
                                  $_selected="";
                                  if(@$_partner_list2->partner_level_sn==$value->partner_level_sn)
                                  $_selected=" selected";
                                  echo  ' <option value="'.$value->partner_level_sn.'" '.$_selected.'>'.$value->partner_level_name.'</option>';
                                }
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <!--div class="form-group">
                        <label for="name" class="col-lg-3 control-label">生日</label>
                        <div class="col-lg-4">
                          <div class="input-group date form_datetime">
                            <input type="text" class="form-control date-picker" id="birthday" name="birthday" value="<?php if(isset($_result_item->birthday)) echo $_result_item->birthday;?>">
                          </div>
                        </div>
                      </div-->
                      <!--div class="form-group">
                        <label for="name" class="col-lg-3 control-label">結婚日期</label>
                        <div class="col-lg-4">
                          <div class="input-group date form_datetime">
                            <input type="text" class="form-control date-picker" id="wedding_date" name="wedding_date" value="<?php if(isset($_re_wedding_date2->wedding_date) && $_re_wedding_date2->wedding_date!="0000-00-00") echo $_re_wedding_date2->wedding_date;?>">
                          </div>
                        </div>
                      </div-->

                      <div class="form-group">
                        <label class="col-lg-3 control-label">產業屬性: <span> * </span></label>
                        <div class="col-lg-8">
                          <div class="radio-list">
                            <?php
							                foreach($_biz_domain_list as $keys=>$values)
							                {
							    							$_checked="";
							    							//foreach($_member_domain_relation as $ckey => $cvalue){
							    									if(@$_partner_list2->domain_sn==$values->domain_sn)
							    									$_checked=" checked";
							    							//}
							    							echo  '  <label class="radio-inline"><input type="radio" required name="domain_sn" value="'.$values->domain_sn.'" '.$_checked.'>'.$values->domain_name.'</label>';
															}
                						?>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">備註</label>
                        <div class="col-lg-8">
                          <textarea class="form-control" rows="3" id="approval_memo" name="approval_memo"><?php if(isset($_result_item->approval_memo)) echo $_result_item->approval_memo;?></textarea>
                        </div>
                      </div>
                      <!--div class="form-group">
                        <label for="name" class="col-lg-3 control-label">是否成為 VIP</label>
                        <div class="col-lg-3">
                          <select name="approval_status" id="approval_status" class="form-control">
                            <option value="0"  <?php if(isset($_result_item->approval_status) && $_result_item->approval_status==0 || !isset($_result_item->approval_status)) echo "selected";?>>審核中</option>
                            <option value="1" <?php if(isset($_result_item->approval_status) && $_result_item->approval_status==1 || !isset($_result_item->approval_status)) echo "selected";?>>審核通過</option>
                            <option value="2" <?php if(isset($_result_item->approval_status) && $_result_item->approval_status==2 || !isset($_result_item->approval_status)) echo "selected";?>>審核不通過</option>
                          </select>
                        </div>
                      </div-->
                      <!--
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">獎金比例</label>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <div class="col-lg-2">
                              <input type="text" maxlength="2" class="form-control form-inline" id="affiliate_bonus" name="affiliate_bonus" value="<?php if(isset($_result_item->affiliate_bonus)) echo $_result_item->affiliate_bonus;?>">
                            </div>
                            <div class="col-lg-1">
                              <span class="form-control-static form-inline">%</span>
                            </div>
                          </div>
                        </div>
                      </div>
                      -->

                      <!--div class="form-group">
                        <label for="name" class="col-lg-3 control-label">有效日期</label>
                        <div class="col-lg-9">
                          <div class="input-group input-large date-picker input-daterange  pull-left" data-date="2015-05-07" data-date-format="yyyy-mm-dd">
                            <input type="text" class="form-control input-sm" name="partnership_start_date" value="<?php if(isset($_partner_list2->partnership_start_date) && $_partner_list2->partnership_start_date!="0000-00-00") echo substr($_partner_list2->partnership_start_date,0,10);?>">
                            <span class="input-group-addon"> 到 </span>
                            <input type="text" class="form-control input-sm" name="partnership_end_date" value="<?php if(isset($_partner_list2->partnership_end_date) && $_partner_list2->partnership_end_date!="0000-00-00") echo substr($_partner_list2->partnership_end_date,0,10);?>">
                          </div>
                        </div>
                      </div-->
                      <!--div class="form-group">
                        <label for="name" class="col-lg-3 control-label">Promote Code</label>
                        <div class="col-lg-4">
                          <div class="input-group date form_datetime">
                            <span class="form-control-static form-inline"><?php  if(isset($_partner_list2->partner_promote_code)) echo $_partner_list2->partner_promote_code;?></span>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">Promote URL</label>
                        <div class="col-lg-4">
                          <div class="input-group date form_datetime">
                            <span class="form-control-static form-inline"><?php if(isset($_partner_list2->partner_promote_url)) echo $_partner_list2->partner_promote_url;?></span>
                          </div>
                        </div>
                      </div-->
                      <div class="form-group">
                        <label for="active" class="col-lg-3 control-label">帳號狀態 <span class="require">*</span></label>
                        <div class="col-lg-3">
                          <select name="member_status" id="member_status" class="form-control" require>
                            <option value="1"  <?php if(isset($_result_item->member_status) && $_result_item->member_status==1 ) echo "selected";?>>審核中</option>
                            <option value="2"  <?php if(isset($_result_item->member_status) && $_result_item->member_status==2 ) echo "selected";?>>有效</option>
                            <option value="3"  <?php if(isset($_result_item->member_status) && $_result_item->member_status==3 ) echo "selected";?>>停用</option>
                         </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="password" class="col-lg-3 control-label">密碼</label>
                        <div class="col-lg-3">
                          <input type="text" class="form-control" name="password" placeholder="不修改請空白">
                        </div>
                      </div>
                      <!--div class="form-group">
                        <label for="confirm-password" class="col-lg-3 control-label">重複輸入密碼 <span class="require">*</span></label>
                        <div class="col-lg-3">
                          <input type="text" class="form-control" name="confirm-password">
                        </div>
                      </div-->
                      <hr>
                      <h4>帳務資訊</h4>
                      <!--div class="form-group">
                        <label for="name" class="col-lg-3 control-label">官方網站</label>
                        <div class="col-lg-3">
                          <input type="url" class="form-control" maxlength="32" name="partner_website_name" id="partner_website_name" value="<?php if(isset($_partner_list2->partner_website_name)) echo $_partner_list2->partner_website_name;?>" placeholder="請填入網址含http">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">LINE@</label>
                        <div class="col-lg-3">
                          <input type="text" class="form-control"  maxlength="32" name="partner_website_url" id="partner_website_url" value="<?php if(isset($_partner_list2->partner_website_url)) echo $_partner_list2->partner_website_url;?>">
                        </div>
                      </div-->
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">負責人</label>
                        <div class="col-lg-3">
                          <input type="text" class="form-control" maxlength="16"  name="partner_chairman" id="partner_chairman" value="<?php if(isset($_partner_list2->partner_chairman)) echo $_partner_list2->partner_chairman;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">公司抬頭</label>
                        <div class="col-lg-3">
                          <input type="text" class="form-control" maxlength="32"  name="partner_company_name" id="partner_company_name" value="<?php if(isset($_partner_list2->partner_company_name)) echo $_partner_list2->partner_company_name;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">統一編號/身份證號碼</label>
                        <div class="col-lg-3">
                          <input type="text" class="form-control" maxlength="10"  name="dealer_tax_id" id="dealer_tax_id" value="<?php if(isset($_partner_list2->dealer_tax_id)) echo $_partner_list2->dealer_tax_id;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">戶名</label>
                        <div class="col-lg-3">
                          <input type="text" class="form-control" maxlength="32"  name="account_name" id="account_name" value="<?php if(isset($_result_item2->account_name)) echo $_result_item2->account_name;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">銀行</label>
                        <div class="col-lg-3">
                          <input type="text" class="form-control" maxlength="32" name="bank_name" id="bank_name" value="<?php if(isset($_result_item2->bank_name)) echo $_result_item2->bank_name;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">分行</label>
                        <div class="col-lg-3">
                          <input type="text" class="form-control" maxlength="32" name="branch_name" id="branch_name" value="<?php if(isset($_result_item2->branch_name)) echo $_result_item2->branch_name;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">帳號</label>
                        <div class="col-lg-3">
                          <input type="text" class="form-control" maxlength="32" name="bank_account" id="bank_account" value="<?php if(isset($_result_item2->bank_account)) echo $_result_item2->bank_account;?>">
                        </div>
                      </div>
                      <hr>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">客服代表</label>
                        <div class="col-lg-3">
                          <select class="form-control" name="create_member_sn">
                            <option value="">請選擇</option>
                            <?php
                              foreach($_associated_sales_membe_list as $keys=>$values){
                              $_selected="";
                              if(@$_partner_list2->create_member_sn == $keys)
                              $_selected=" selected";
                              echo  ' <option value="'.$keys.'" '.$_selected.'>'.$values.'</option>';
                            }?>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                          <button type="submit" class="btn green">儲存</button>
                          <a href="admin/member/partnerTable"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- End of Tab 資料設定 -->
                <!-- Tab 所屬會員 -->
                <div class="tab-pane" id="tab_member">
                  <table class="table table-striped table-bordered table-hover" id="table_member">
                    <thead>
                      <tr>
                        <th>姓名</th>
                        <th>Email</th>
                        <th>縣市</th>
                        <th>鄉鎮市區</th>
                        <th>住址</th>
                        <th>電話</th>
                        <th>加入時間</th>
                        <th>帳號狀態</th>
                        <th>編輯</th>
                      </tr>
                    </thead>
                    <tbody>
               <?php
                 $_member_status_ct=$this->config->item("member_status_ct");
                 $_member_level_type_ct=$this->config->item("member_level_type_ct");
                 //$_city_ct=$this->config->item("city_ct");


                if(is_array(@$_result04))
                  {
                      foreach($_result04 as $key => $value)
                        {
                          $_town_ct_init="town_ct_".$value->addr_city_code;
                          $_town_ct=$this->config->item($_town_ct_init);
                  ?>


                  <tr class="odd gradeX">
                    <td><?php echo $value->last_name.$value->first_name;?></td>
                    <td><?php echo $value->email;?></td>
                    <td><?php echo $value->city_name;?></td>
                    <td><?php echo $value->town_name;?></td>
                    <td><?php echo $value->addr1;?></td>
                    <td><?php echo $value->cell;?></td>
                    <td><?php echo $value->registration_date;?></td>
                    <td><?php echo $_member_status_ct[$value->member_status];?> </td>
                    <td>
                      <a target="_blank" href="<?php echo $init_control;?>Member/memberTable_item/edit/<?php echo $value->member_sn;?>"><i class="fa fa-edit"></i></a>&nbsp;
                    </td>
                  </tr>
                <?php } } ?>
                    </tbody>
                  </table>
                </div>
                <!-- End of Tab 所屬會員 -->
                <!-- Tab 訂單紀錄 -->
                <div class="tab-pane" id="tab_dashboard">
                  <div class="col-lg-12">
                      <form method="post" id="form1">
                    <h2 style="display: inline-block;">
                      <i class="icon-trophy"></i> 績效總覽 &nbsp;
                    </h2>
                        <input type="text" name="yearmonth" id="yearmonth" value="<?=@$yearmonth?>" class="date-picker">
                      </form>
                        <div class="col-md-6">
                          <div class="alert alert-info text-center">
                            <h3>分潤獎金：<?=number_format(@$cash)?></h3>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="alert alert-info text-center">
                            <h3>招募獎金：<?=number_format(@$cash2)?></h3>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="alert alert-warning text-center">
                            <h3>分潤訂單：<?=@$total_ok_orders?></h3>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="alert alert-warning text-center">
                            <h3>相關訂單：<?=@$total_no_ok_orders?></h3>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="alert alert-warning text-center">
                            <h3>招募會員：<?=@$total_members?></h3>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="alert alert-warning text-center">
                            <h3>點擊數：<?=@$total_hits?></h3>
                          </div>
                        </div>
                  </div>
                </div>
                <!-- End of Tab 訂單紀錄 -->
                <!-- Tab 獎金紀錄 -->
                <div class="tab-pane" id="tab_benefit">
                  <table class="table table-striped table-bordered table-hover" id="table_member">
                    <thead>
                      <tr>
                        <th>月份</th>
                        <th>預估獎金</th>
                        <th>是否入帳</th>
                        <th>功能</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php for($i=6;$i>0;$i--):?>
                        <tr class="odd gradeX">
                          <td>2015-0<?php echo $i;?></td>
                          <td>1,000</td>
                          <td>
                            <?php if($i%2==0):?>
                              <i class="fa fa-check"></i>
                            <?php else:?>
                              <i class="fa fa-times"></i>
                            <?php endif;?>
                          </td>
                          <td class="center">
                            <?php if($i%3==0):?>
                              <a href="javascript:;" class="btn default disabled"> 已入帳 </a>
                            <?php elseif($i%3==1):?>
                              <a href="javascript:;" class="btn default"> 入帳 </a>
                            <?php else:?>
                              未達轉帳門檻
                            <?php endif;?>
                          </td>
                        </tr>
                      <?php endfor;?>
                    </tbody>
                  </table>
                  <!-- pager -->
                  <div class="row">
                    <div class="col-md-5 col-sm-12">
                      <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
                    </div>
                    <div class="col-md-7 col-sm-12">
                      <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                        <ul class="pagination" style="visibility: visible;">
                          <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                          <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                          <li><a href="#">3</a></li>
                          <li><a href="#">4</a></li>
                          <li class="active"><a href="#">5</a></li>
                          <li><a href="#">6</a></li>
                          <li><a href="#">7</a></li>
                          <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                          <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!-- End of pager -->
                </div>
                <!-- End of Tab 獎金紀錄 -->
              </div>
            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->