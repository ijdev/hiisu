<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script src="public/metronic/admin/pages/scripts/components-pickers.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<?php //var_dump($_result01);?>
<script>
jQuery(document).ready(function() {
   	Metronic.init(); // init metronic core components
		Layout.init(); // init current layout
		$(".select2_dealer").select2({
		    tags: ["典華1", "典華2", "典華3", "典華4", "典華5"]
		});
    $('.date-picker').datepicker({
        rtl: Metronic.isRTL(),
        format: "yyyy-mm-dd",
        orientation: "left",
        autoclose: true
    });

	var addr_city_code='<?php echo @$_result01[0]->addr_city_code;?>';
	var addr_town_code='<?php echo @$_result01[0]->addr_town_code;?>';
	if(addr_city_code){
		get_town(addr_city_code);
		$('select[id="addr_city_code"]').val(addr_city_code);
	}
 /* Action On Select Box Change */
  $('select[id="addr_city_code"]').change(function() {
        var data = $(this).val(); // Get Selected Value
		get_town(data);
  });

	function get_town(data){
	    $.ajax({
	        url: 'Member/get_town_zip',
	        data: 'data=' + data,
	        dataType: 'json',
	        success: function(data) {
	         		 $('#addr_town_code').empty();
							 $(data).appendTo("#addr_town_code");
							 //$.uniform.update('#addr_town_code');
								if(addr_town_code){
									$('select[id="addr_town_code"]').val(addr_town_code);
							 		//$.uniform.update('#addr_town_code');
							 	}else{
							 		//$.uniform.update('#addr_town_code');
								}
	        }
	    });
  }
  $('select[id="addr_town_code"]').change(function() {
			var zipcode = parseInt($('option:selected', this).attr('zipcode'));
			//console.log(zipcode);
			$('#zipcode').val(zipcode);
  });
  //發票
	var invoice_addr_city_code='<?php echo @$_result01[0]->invoice_addr_city_code;?>';
	var invoice_addr_town_code='<?php echo @$_result01[0]->invoice_addr_town_code;?>';
	if(invoice_addr_city_code){
		get_invoice_town(invoice_addr_city_code);
		$('select[id="invoice_addr_city_code"]').val(invoice_addr_city_code);
	}
 /* Action On Select Box Change */
  $('select[id="invoice_addr_city_code"]').change(function() {
        var data = $(this).val(); // Get Selected Value
		get_invoice_town(data);
  });

	function get_invoice_town(data){
	    $.ajax({
	        url: 'Member/get_town_zip',
	        data: 'data=' + data,
	        dataType: 'json',
	        success: function(data) {
	         		 $('#invoice_addr_town_code').empty();
							 $(data).appendTo("#invoice_addr_town_code");
							 //$.uniform.update('#addr_town_code');
								if(invoice_addr_town_code){
									$('select[id="invoice_addr_town_code"]').val(invoice_addr_town_code);
							 		//$.uniform.update('#addr_town_code');
							 	}else{
							 		//$.uniform.update('#addr_town_code');
								}
	        }
	    });
  }
  $('select[id="invoice_addr_town_code"]').change(function() {
			var invoice_send_zipcode = parseInt($('option:selected', this).attr('zipcode'));
			//console.log(zipcode);
			$('#invoice_send_zipcode').val(invoice_send_zipcode);
  });
		  $('#sameas_supplier_name').click(function() {
		  	if($(this).attr('checked')){
					$('#supplier_company_title').val($('#supplier_name').val());
				}
      });
		  $('#sameasadd').click(function() {
		  	if($(this).attr('checked')){
					$('#invoice_addr_city_code').val($('#addr_city_code').val());
					$($("#addr_town_code > option").clone()).appendTo("#invoice_addr_town_code");
					$('#invoice_send_zipcode').val($('#zipcode').val());
					$('#invoice_send_addr1').val($('#addr1').val());
					$('#invoice_addr_town_code').val($('#addr_town_code').val());
				}
      });
});
</script>


<script>

  // When the browser is ready...
  $(function() {
	jQuery.extend(jQuery.validator.defaults,
	{
	    errorPlacement: function (error, element) {
	        if (element.is(':radio') || element.is(':checkbox')) {
	            var eid = element.attr('name');
	            $('input[name="' + eid + '"]:last').parent().after(error);
	        }
	        else {
	            error.insertAfter(element);
	        }
	    }
	});
jQuery.validator.addMethod("mobileTaiwan", function( value, element ) {
	var str = value;
	var result = false;
	if(str.length > 0){
		//是否只有數字;
		var patt_mobile = /^[\d]{1,}$/;
		result = patt_mobile.test(str);

		if(result){
			//檢查前兩個字是否為 09
			//檢查前四個字是否為 8869
			var firstTwo = str.substr(0,2);
			var firstFour = str.substr(0,4);
			var afterFour = str.substr(4,str.length-1);
			if(firstFour == '8869'){
				$(element).val('09'+afterFour);
				if(afterFour.length == 8){
					result = true;
				} else {
					result = false;
				}
			} else if(firstTwo == '09'){
				if(str.length == 10){
					result = true;
				} else {
					result = false;
				}
			} else {
				result = false;
			}
		}
	} else {
		result = true;
	}
	return result;
}, "手機號碼不符合格式，僅允許09開頭的10碼數字");

    // Setup form validation on the #register-form element
    $("#register-form").validate({
        // Specify the validation rules
        rules: {
            supplier_name: "required",
            /*user_name: {
                required: true,
            },
            password: {
                required: false,
                minlength: 6
            },
             confirm_password: {
                required: false,
                equalTo: "#password",
                minlength: 6
            },*/
        },
        // Specify the validation error messages
        messages: {
            supplier_name: "請輸入公司中文名稱!",
            /*password: {
                required: "請輸入密碼",
                minlength: "密碼最小長度為6"
            },
            confirm_password: {
                required: "請輸入密碼",
                minlength: "密碼最小長度為6"
            },
            user_name: "請輸入正確的email格式！"*/
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

    $("#register-form2").validate({
        // Specify the validation rules
        rules: {
            min_amount_for_noshipping: "required",
            shipping_fee: "required",
        },
        // Specify the validation error messages
        messages: {
            min_amount_for_noshipping: "請輸入滿額免運金額!",
            shipping_fee: "請輸入運費!",
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    $(document).on('click', '.deleteContact', function() {
        var button = $(this);
        	var r = confirm('是否刪除此聯絡窗口?');
            if(r)
            {
				$.post('/admin/supplier/supplierTable/delete_contact/'+button.attr('supplier_sn'), {contact_sn:button.attr('ItemId')}, function(data){
                	if (data){
                		button.parents().parents().filter('tr').remove();
                	}else{
                		alert('刪除失敗');
                	}
				});
            }
    });
    $(document).on('click', '.deleteAddon', function() {
        var button = $(this);
        	var r = confirm('是否刪除此管理人員?');
            if(r)
            {
				$.post('/admin/supplier/supplierTable/edit/'+button.attr('supplier_sn'), {member_sn:button.attr('ItemId')}, function(data){
                	if (data){
                		button.parents().parents().filter('tr').remove();
                	}else{
                		alert('刪除失敗');
                	}
				});
            }
    });
	var initTableMember2 = function () {
        var table = $('#table_member_admin');
        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": 依此欄位做順向排序",
                    "sortDescending": ": 依此欄位做逆向排序"
                },
                "emptyTable": "沒有資料可以顯示",
                "info": "顯示 _TOTAL_ 中的第 _START_ 到 _END_ 筆",
                "infoEmpty": "查無結果",
                "infoFiltered": "(從總筆數 _MAX_ 筆過濾出)",
                "lengthMenu": "顯示 _MENU_ 項目",
                "search": "搜尋:",
                "zeroRecords": "查無結果",
                "paginate": {
                    "previous":"上一頁",
                    "next": "下一頁",
                    "last": "最後",
                    "first": "最前"
                }
            },

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "columns": [{
                "orderable": true
            },{
               "orderable": true
            },{
                "orderable": true
            }, {
                "orderable": false
            }],
            "lengthMenu": [
                [5, 10, 20, -1],
                [5, 10, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [{  // set default column settings
                'orderable': false,
                'targets': [0]
            }, {
                "searchable": true,
                "targets": [0]
            }],
            "order": [
                //[0, "asc"]
            ] // set first column as a default sort by asc
        });
    }
    initTableMember2();
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('.alert').hide();
        $('.note-success').hide();
    });
  });
		$("#contact_edit").on("show.bs.modal", function(e) {
		    var link = $(e.relatedTarget);
			//console.log(link.attr("href"));
		    if(link.attr("href")){
		    	$(this).find(".modal-content").load(link.attr("href"));
		  	}
		});
  </script>