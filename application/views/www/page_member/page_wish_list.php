<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容
*/
?>
<div class="container mob_padding">

<!-- Page level plugin styles START -->
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<style type="text/css">
  .select2_dealer{
    padding: 0px;
  }
</style>
<div class="container mob_padding">
<!-- Page level plugin styles END -->
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <li class="active">追蹤清單</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar col-md-2 col-sm-3">
    <div class="desktop_view_sidebar">
		<ul class="list-group margin-bottom-25 sidebar-menu">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
	<div class="mobile_view_sidebar">
		<div class="side-nav-head border_title_bottom">
			<button class="fa fa-bars collapsed" data-toggle="collapse" data-target="#mob_sidebar_drop_down"></button>
			<h4 class="bfont">會員中心<!--會員中心--></h4>
		</div>
		<ul class="list-group margin-bottom-25 sidebar-menu collapse" id="mob_sidebar_drop_down">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
<div class="col-md-9 col-sm-9 margin-bottom-30 product_right border_left minheight">

   <h4 class="col-md-12 col-sm-12 pull-left padding0">
   <i product_sn="10" class="fa fa-heart"></i> 追蹤清單
	 <h4>
       <!--我的追蹤清單開始 -->
	    <div class="col-md-12 col-sm-12 pull-left wishhead">
					<li class="col-md-2 col-xs-4 pull-left wishtittle">縮圖</li>
					<li class="col-md-7 col-xs-5 pull-left wishtittle">商品資訊</li>
					<li class="col-md-1 col-xs-1 pull-left wishtittle">庫存</li>
					<li class="col-md-1 col-xs-1 pull-left wishtittle">價格</li>
					<li class="col-md-1 col-xs-1 pull-left wishtittle">操作</li>
		</div>

<?php if(@$Items){foreach(@$Items as $Item){?>
    <div class="blog-post-item pull-left">
		 <div id="wish_list_<?php echo $Item['track_list_sn'];?>" class="form-group product_detls">
			<div class="col-md-2 col-xs-4 mob_title" data-title="縮圖">
             <figure class="margin-bottom-20">
        <a href="shop/ShopItem/<?php echo $Item['product_sn'];?>" target="_blank"><img class="img-responsive" onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['product']['image_path'];?>" alt="<?php echo $Item['product']['image_alt'];?>"></a>
             </figure>
			 </div>
			 <div class="col-md-7 col-xs-5 mob_title" data-title="商品資訊">
				 <div class="blog-item-small-content">
          <h2><a href="shop/ShopItem/<?php echo $Item['product_sn'];?>" rel="external"><?php echo $Item['product_name'];?></a></h2>
				  <p><?php echo $Item['product']['special_item'];?></p>
				 </div>
			 </div>
			 <div class="col-md-1 col-xs-1 pull-left wishtittle mob_title" data-title="庫存">
            <?php if($Item['product']['unispec_flag']){
                  $qty_in_stock=intval($Item['product']['unispec_qty_in_stock']);
              }else{
                if(@$mutispec=$Item['product']['mutispec']){ foreach($mutispec as $mu){
                  $qty_in_stock = @$qty_in_stock+intval($mu['qty_in_stock']);
                }}
              }?>
            <?php echo (@$qty_in_stock > 0)?'有':'無'?>
       </div>
			  <div class="col-md-1 col-xs-1 pull-left wishtittle goldfont mob_title" data-title="價格">$<?php echo $Item['product']['price1'];?><?php echo $Item['product']['price2'];?></div>
			   <div class="col-md-1 col-xs-1 pull-left wishtittle mob_title" data-title="操作">
           <?php if(@$qty_in_stock > 0){?>
           <a class="plussg prd_btn_rgt add-goods" style="margin-top:0px;" product_sn="<?php echo $Item['product_sn'];?>" href="javascript:;"><span style="margin-top: 3px;" class="glyphicon glyphicon-plus "></span></a>
           <?php }?>
           <a class="del-goods remover" style="margin-top:0px;" ItemId="<?php echo $Item['track_list_sn'];?>" href="javascript:;"><span style="margin-top: 3px;" class="glyphicon glyphicon-remove"></span></a>
					</div>
			 <div style="clear:both;"></div>
        </div>
		</div>
<?php }}
$total_page=ceil($total/$perpage);?>
  <table width="150" border="0" align="center" cellpadding="0" cellspacing="0">
		<tbody><tr><td colspan="2" height="5"></td></tr>
    <tr>
      <td height="29" valign="middle">
<?if($page>1){?>
        <a href="/member/wishList?page=<?=($page-1)?>"><i class="icon-caret-left size-30"></i></a>
<?}?>
      </td>
      <td height="29" valign="middle"><div class="size-20 text-center margin-right-10"><?=$page?> / <?=$total_page?></div></td>
      <td height="29" valign="middle">
<?if($page<$total_page){?>
        <a href="/member/wishList?page=<?=($page+1)?>"><i class="icon-caret-right size-30"></i></a>
<?}?>
      </td>
    </tr>
  </tbody></table>
			</div>
</div>
</div><!-- container -->
</div>

<!-- Modal for 表單填寫 -->
