<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Page 主架構
*/
?>
<div class="main-container scrollbar-space" style="background-image:url(/public/uploads/<?php echo $banner[0]?>)">
  <div class="med-style">
      <div class="content-inner clearfix">
          <div class="row">
              <div class="col full">
                  <h4>ABOUT US</h4>
                  <p><?php echo $dealer['dealer_description']?></p>
              </div>
          </div>
          <div class="divider" style="height:20px;"></div>

          <!-- Begin Team -->
          <div class="row">
              <div class="team clearfix">
<?php if($aboutus){ foreach ($aboutus as $key=>$_item) {?>
                  <div class="member col one-third">
                      <div class="member-cont">
                          <img src="/public/uploads/<?php echo $_item;?>" alt="<?php echo $aboutus_description[$key];?>">
                          <div class="overlay">
                              <h5><?php echo $aboutus_description[$key];?></h5>
                          </div>
                      </div>
                  </div>
<?php	}} ?>
              </div>
          </div>
          <!-- End Team -->

          <div class="divider" style="height:60px;"></div>

          <!--div class="row">
              <div class="col full">
                  <h4>FACEBOOK POSTS</h4>
              </div>
          </div-->

          <!-- Blog List -->
          <div class="blog style-2 fullwidth">
            <div class="container">
              <div class="articles masonry clearfix cols-3">
								<?php if($articles){ foreach ($articles as $key=>$_item) {
									date_default_timezone_set('Asia/Taipei');
									$created_time = date('F jS,Y', strtotime($_item['created_time']));
									?>
                  <div class="article size-regular">
                    <div class="article-cont">
                      <div class="header clearfix">
                          <p class="date">
                            <img src="/public/uploads/<?php echo $profile[0]?>" class="logo-page">
                            <a href="https://www.facebook.com/<?php echo $_item['id'];?>" target="_blank"><?php echo $created_time;?></a>
                          </p>
                      </div>
                      <div class="content">
                        <?php if(@$_item['attachments']['data'][0]['media']):?>
                        	<?php if(@$_item['attachments']['data'][0]['media']['image']['src']):?>
	                          <a href="https://www.facebook.com/<?php echo $_item['id'];?>" target="_blank">
	                            <img height="234" src="<?php echo $_item['attachments']['data'][0]['media']['image']['src'];?>" alt="">
	                          </a>
                        	<?php endif;?>
                        <?php endif;?>
                          <p>
                              <?php echo $this->ijw->make_links_clickable($_item['message']);?>
                          </p>
                      </div>

                    </div>

                  </div>
                <?php }}?>
              </div>
            </div>
          </div>
          <!-- /.Blog List -->
        </div>
      </div>
  </div>
</div>