<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Libraries_model extends MY_Model{
    public function _select($table,$where = 0,$where_val = 0,$offset = 0,$per_page = 0,$order = 0,$desc = 0,$result = 'result',$select = 0,$like = 0,$distinct=0,$status=1,$_ifupdate_member_sn=1){
        if (!$select) $select = $table.'.*';

        $this->db->from($table);
        if($this->db->field_exists('update_member_sn', $table)){ //有更新者欄位抓其user_name代替update_member_sn
            if($_ifupdate_member_sn=='1'){
    	        $this->db->select($select.',aaa.last_name as update_member_sn');
    	        $this->db->join('ij_member as aaa','aaa.member_sn='.$table.'.update_member_sn','left');
            }else{
               $this->db->select($select);
            }
	    }else{
        	   $this->db->select($select);
      	}


        if($where && $where_val && !$like){
            $this->db->where($where,$where_val);
        }elseif($where && !$where_val && !$like){
            $this->db->where($where);
        }elseif($where && $like && !$where_val){
            $this->db->like($where, $like);
        }else{
        	if($this->db->field_exists('status', $table)){
            $this->db->where('status','1');
          }
        }
        //加入供應商、經銷商機制
        if(@$this->session->userdata('member_role_system_rules')[0]['role_name']){
            switch(@$this->session->userdata('member_role_system_rules')[0]['role_name']){
            case '經銷商管理員':
                if($table=='ij_product'){
                    //$this->db->where('ij_product.supplier_sn',$this->session->userdata['member_data']['associated_supplier_sn']);
                }elseif($table=='ij_blog_article'){
                    //$this->db->where('ij_blog_article.associated_dealer_sn',$this->session->userdata['member_data']['associated_dealer_sn']);
                }
            break;
            case '供應商管理員':
                if($table=='ij_product'){
                    $this->db->where('ij_product.supplier_sn',$this->session->userdata['member_data']['associated_supplier_sn']);
                }elseif($table=='ij_blog_article'){
                    $this->db->where('ij_blog_article.associated_supplier_sn',$this->session->userdata['member_data']['associated_supplier_sn']);
                }elseif($table=='ij_delivery_method_ct'){
                    $this->db->where('ij_delivery_method_ct.supplier_sn',$this->session->userdata['member_data']['associated_supplier_sn']);
                }
            break;
            default:
            break;
            }
        }
      	if($status){
        	if($this->db->field_exists('status', $table)){  //test除了指定以外都只抓1的
                $this->db->where('status','1');
            }
		}
        if($order && $desc){
            $this->db->order_by($order,'desc');
        }elseif($order && !$desc){
            $this->db->order_by($order,'asc');
        //}else{
            //$this->db->order_by('sort_order','asc'); 因為有的表沒有 sort_order
        }

        if($offset && $per_page){
            $this->db->limit($per_page,$offset);
        }elseif(!$offset && $per_page){
            $this->db->limit($per_page);
        }
        if($distinct){
            $this->db->distinct($distinct);
        }

        $query = $this->db->get();
		//echo $this->db->last_query().'<Br>';
        if($result == 'row'){
            return $query->row_array();
        }elseif($result == 'rows'){
        	foreach($query->result_array() as $row){
        		$rows[]=$row[$select];
        	}
          return @$rows;
        }elseif($result == 'result'){
        	$return_array=$query->result();
        	$query->free_result();
          return $return_array;
        }elseif($result == 'result_array'){
        	$return_array=$query->result_array();
        	$query->free_result();
          return $return_array;
        }elseif($result == 'list'){
		//echo $this->db->last_query();
		//var_dump($query->result());
			if($result=$query->result()){
	        	$_fields=explode(',',$select);
					   foreach ($result as $row)
					   {
                            //var_dump($_fields[0]);
						   $_one1= $row->{$_fields[0]};
						   $_one2= $row->{$_fields[1]};
						   $_one[$_one1]=$_one2;
					   }
	            return $_one;
	        }else{
	        	return array();
	        }
        }elseif($result == 'num_rows'){
            return $query->num_rows();
        }elseif($result == 'strings'){
					if ($select){
            return implode('、',$query->row_array());
          }
        }

    }


    public function _insert($table,$data){

        $this->db->insert($table,$data);
        return $this->db->insert_id();

    }

    public function _delete($table,$where,$where_val = 0){

        if($where && $where_val){
            $this->db->where($where,$where_val);
        }elseif($where && !$where_val){
            $this->db->where($where);
        }

        $this->db->delete($table);

        return ($this->db->affected_rows() > 0);

    }

    public function _count_all($table){
        return $this->db->count_all($table);
    }
    public function _selectifsame($table,$where = 0,$where_val = 0,$wherenot = 0,$wherenot_val = 0){

        $this->db->select($where);
        $this->db->from($table);

        if($where && $where_val){
            $this->db->where($where,$where_val);
				}

        if($wherenot && $wherenot_val){
            $this->db->where($wherenot.' !=',$wherenot_val);
				}

        $query = $this->db->get();
        if ($query->num_rows()>0){
        	return true;
        }else{
        	return false;
        }

    }

    public function _check_ip($ip){

      if($ip){
        $this->db->select('IP,ipActive,ValidDate');
        $this->db->from('IP_Restrict');

        $this->db->where('IP',$ip);

        $query = $this->db->get();
        if($query->num_rows()>0){
        	$row=$query->row();
        	if($row->ipActive=='1'){
        		return true;
        	}elseif($row->ipActive=='2'){
        		if($row->ValidDate && $row->ValidDate >=date("Y-m-d")){
        			return true;
        		}else{
        			return false;
        		}
        	}else{
        		return false;
        	}
        }else{
        	return false;
        }

			}
   }
    public function CheckUpdate($array,$ifnew=0)
    {
    	if(!$ifnew){
        $array['last_time_update']=date("Y-m-d H:i:s",time());
        $array['update_member_sn']=( isset($this->session->userdata['member_data']['member_sn'] ) ) ? $this->session->userdata['member_data']['member_sn'] : "1";
      }else{
        $array['create_date']=date("Y-m-d H:i:s",time());
        $array['create_member_sn']=( isset($this->session->userdata['member_data']['member_sn'] ) ) ? $this->session->userdata['member_data']['member_sn'] : "1";
      }
        return $array;
    }
    public function _AddAmount($array,$table=0)
    {
    	if($array && $table){
				switch($table){
					case "ij_attribute_config":
						foreach($array as $key=>$arr){
								$array[$key]['amount']=$this->libraries_model->_select('ij_attribute_value_config','attribute_sn',$arr['attribute_sn'],0,0,0,0,'num_rows');
						}
						return $array;
					break;
					case "ij_attribute_value_config":
						foreach($array as $key=>$arr){
								$array[$key]['amount']=$this->libraries_model->_select('ij_product_attribute_log','attribute_value_sn',$arr['attribute_value_sn'],0,0,'status',0,'num_rows');
						}
						return $array;
					break;
	      }
    	}
    }
    public function category_attributes($category_sn=0,$ifall=0){
        $this->db->select('attribute_sn,attribute_name');
        $this->db->from('ij_attribute_config');
        $this->db->order_by('attribute_sn');
        $this->db->where('status',1);
        $array2 = $this->db->get()->result_array();
				//var_dump($array2);

        $this->db->select('attribute_sn,status');
        $this->db->from('ij_category_attribute_log');
        $this->db->order_by('attribute_sn');
        $this->db->where('category_sn',$category_sn);
        $array1 = $this->db->get()->result_array();
		if($array1){
		  foreach($array2 as $key2=>$value2) {
		  	foreach($array1 as $key1=>$value1) {
		      if($value1['attribute_sn']==$value2['attribute_sn']){
		      	 //$array2[$key2]['status']='1';
		      	 $array1[$key1]['attribute_name']=$value2['attribute_name'];
		      	 $array1[$key1]['status']='1';
                 $array2[$key2]['attribute_name']=$value2['attribute_name'];
                 $array2[$key2]['status']='1';

		      	 //break;
		      }
		    }
		  }
		}else{
			return $array2;
		}
		//var_dump($array1);
		//exit();
		if($ifall){
            return $array2;
        }else{
            return $array1;
        }
    }
    public function categoryParentChildTree($channel_name=0,$category_sn=0,$spacing = '├─', $category_tree_array = '',$spacing2='│&nbsp;',$_level=0)
    {
		//NodeId為Parent的點
		    if (!is_array($category_tree_array))
		        $category_tree_array = array();

	    //取得該點的下一層
        $this->db->select('category_sn,category_name,upper_category_sn,sort_order,channel_name,category_status');
        $this->db->from("ij_category");
        $this->db->where('upper_category_sn',$category_sn);
        $this->db->where('category_status','1');
        if($channel_name){
        	$this->db->where('channel_name',$channel_name);
      	}
		$query =$this->db->get()->result_array();
		//echo $this->db->last_query().'<br>';
	    if($query) {
			//如果有下一層資料，逐一取出
        	$_level++;
        foreach($query as $key => $intro){
        	//echo $spacing . $intro['category_name'].'<br>';
          $category_tree_array[] = array("category_sn" => $intro['category_sn'], "category_name" => $spacing . $intro['category_name'], "upper_category_sn" => $intro['upper_category_sn'], "sort_order" => $intro['sort_order'], "channel_name" => $intro['channel_name'], "category_status" => $intro['category_status'], "pure_category_name" => $intro['category_name'], "level" => $_level);
          $category_tree_array = $this->categoryParentChildTree($channel_name,$intro['category_sn'], $spacing2.$spacing , $category_tree_array,$spacing2,$_level);
        	//var_dump($category_tree_array);
				}
	    }
		    return $category_tree_array;
    }

    public function get_channels2($more=0){ //for FAQ
        if($more){
                //return array(''=>'請選擇',$more=>$more,'婚禮網站'=>'婚禮網站','婚宴管理'=>'婚宴管理','結婚商店'=>'結婚商店');
                return array(''=>'頻道',$more=>$more,'結婚商店'=>'結婚商店');
        }else{
                //return array(''=>'請選擇','婚禮網站'=>'婚禮網站','婚宴管理'=>'婚宴管理','結婚商店'=>'結婚商店');
                return array(''=>'頻道','囍市集'=>'囍市集','供應商'=>'供應商','經銷商'=>'經銷商');
        }
    }
    public function _get_categorys($spec_option_config_sn=0){ //規格單位抓分類名稱
    	if($spec_option_config_sn){
            $this->db->select('category_name');
            $this->db->from('ij_category_spec_option_relation');
            $this->db->join('ij_category','ij_category.category_sn=ij_category_spec_option_relation.category_sn');
            $this->db->where('spec_option_config_sn',$spec_option_config_sn);
            $this->db->where('status',1);
            $this->db->order_by('sort_order');
            $array=$this->db->get()->result_array();
            $category_names=array();
            foreach($array as $row){
            	$category_names[]=$row['category_name'];
            }
            return implode('、',$category_names);
	    }else{
	    	return false;
	    }
  	}
    public function _get_spec_by_cat($category_sn=0,$product_package_config_sn=0){ //分類抓規格單位寫入關聯表再抓出
    	if($category_sn && $product_package_config_sn){
        foreach($category_sn as $key=>$categorysn){
	        $this->db->select('ij_category_spec_option_relation.spec_option_config_sn');
	        $this->db->from('ij_category_spec_option_relation');
	        $this->db->join('ij_spec_option_config','ij_spec_option_config.spec_option_config_sn=ij_category_spec_option_relation.spec_option_config_sn');
	        $this->db->where('category_sn',$categorysn);
	        $this->db->where('ij_spec_option_config.status',1);
	        $this->db->order_by('sort_order');
	        $array=$this->db->get()->result_array();
	        if($array){
	        	foreach($array as $key=>$row){
							$where = array('product_package_config_sn' => $product_package_config_sn,'spec_option_config_sn' => $row['spec_option_config_sn']);
							//echo $this->libraries_model->_select('ij_product_package_spec_option_relation',$where,0,0,0,'product_package_config_sn',0,'num_rows',0,0,0,0).'<br>';
							//echo $this->db->last_query().'<br>';
		        	if($this->libraries_model->_select('ij_product_package_spec_option_relation',$where,0,0,0,'product_package_config_sn',0,'num_rows',0,0,0,0)==0){ //沒資料才新增
								$data[$key]['product_package_config_sn']=$product_package_config_sn;
								$data[$key]['spec_option_config_sn']=$row['spec_option_config_sn'];
								$data[$key]['status']='0';
							  $data[$key] = $this->libraries_model->CheckUpdate($data[$key],1);
								$this->libraries_model->_insert('ij_product_package_spec_option_relation',$data[$key]);
							}
		        }
	      	}
	      }

        $this->db->select('product_package_spec_option_relation_sn,ij_product_package_spec_option_relation.spec_option_config_sn,spec_option_name,usage_limitation,description,spec_option_limitation,ij_product_package_spec_option_relation.status');
        $this->db->from('ij_product_package_spec_option_relation');
        $this->db->join('ij_spec_option_config','ij_spec_option_config.spec_option_config_sn=ij_product_package_spec_option_relation.spec_option_config_sn','left');
        $this->db->where('product_package_config_sn',$product_package_config_sn);
        $this->db->where('ij_spec_option_config.status',1);
        $this->db->order_by('sort_order');
        return $this->db->get()->result_array();
	    }else{
	    	return false;
	    }
  	}
    public function _get_color_by_template($product_template_sn=0,$website_color_sn=0){ //版型抓顏色
    	if($product_template_sn){
    		if($website_color_sn){
						$where = array('product_template_sn' => $product_template_sn,'website_color_sn' => $website_color_sn);
	        	if(!$template_color_relation=$this->libraries_model->_select('ij_template_color_relation',$where,0,0,0,0,0,'row','template_color_relation_sn,status',0,0,0)){ //沒資料才新增
							$data['product_template_sn']=$product_template_sn;
							$data['website_color_sn']=$website_color_sn;
							$data['status']='1';
						  $data = $this->libraries_model->CheckUpdate($data,1);
							$this->libraries_model->_insert('ij_template_color_relation',$data);
						}else{
							$template_color_relation_sn = $template_color_relation['template_color_relation_sn'];
							if($template_color_relation['status']){
								$data['status']='0';
							}else{
								$data['status']='1';
							}
						  $data = $this->libraries_model->CheckUpdate($data,0);
							//$this->libraries_model->_delete('ij_template_color_relation','template_color_relation_sn',$template_color_relation_sn); //有資料則刪除（取消選取）改為更新status
							$this->libraries_model->_update('ij_template_color_relation',$data,'template_color_relation_sn',$template_color_relation_sn); //有資料則刪除（取消選取）
						}
		    }
        $this->db->select('color_name,website_color_sn,color_code');
        $this->db->from('ij_website_color');
        $this->db->where('status',1);
        $this->db->order_by('sort_order');
        $array1=$this->db->get()->result_array();
	      $this->db->select('template_color_relation_sn,website_color_sn,status');
        $this->db->from('ij_template_color_relation');
        $this->db->where('product_template_sn',$product_template_sn);
        $this->db->order_by('website_color_sn');
        $array2=$this->db->get()->result_array();
      	foreach($array1 as $key1=>$row1){
      		foreach($array2 as $key2=>$row2){
      			if($row1['website_color_sn']==$row2['website_color_sn']){
      				//echo $row2['status'].'<br>';
      				$array1[$key1]['status']=$row2['status']; //抓出有選擇的
      				$array1[$key1]['template_color_relation_sn']=$row2['template_color_relation_sn']; //抓出有選擇的
						  $where = array('product_template_sn' => $product_template_sn,'associated_template_color_relation_sn' => $row2['template_color_relation_sn']);
							$array1[$key1]['amount']=$this->libraries_model->_select('ij_template_picture',$where,'',0,0,0,0,'num_rows');
      			}
      		}
      	}
        return $array1;
	    }else{
	    	return false;
	    }
  	}
    public function _get_products($type,$search=0)
    {
    	if($type){
            $this->db->select('ij_product.*,category_name,supplier_name');
            $this->db->from("ij_product");
            $this->db->join("ij_category","ij_category.category_sn=ij_product.default_root_category_sn","left");
            $this->db->join("ij_supplier","ij_supplier.supplier_sn=ij_product.supplier_sn","left");
            $this->db->where('ij_product.default_channel_name',$type);
            //加入供應商、經銷商機制
            if($this->session->userdata('member_role_system_rules')[0]['role_name']=='供應商管理員'){
                $this->db->where('ij_product.supplier_sn',$this->session->userdata['member_data']['associated_supplier_sn']);
            }else if($this->session->userdata('member_role_system_rules')[0]['role_name']=='經銷商管理員'){
            }
            if(isset($search['product_name']) && $search['product_name']){
                $this->db->like('ij_product.product_name',$search['product_name']);
            }else{
                $this->db->where('ij_product.product_name <>','');
            }
            if(@$search['product_sn']){
                $this->db->where('ij_product.product_sn',$search['product_sn']);
            }
            if(@$search['promotion_label']){
                $this->db->join("ij_product_promotion_label_relation","ij_product_promotion_label_relation.product_sn=ij_product.product_sn","left");
                $this->db->where('ij_product_promotion_label_relation.promotion_label_sn',$search['promotion_label']);
                $this->db->where('ij_product_promotion_label_relation.status','1');
                $this->db->where('start_date <=',date("Y-m-d H:i:s",time()));
                $this->db->where('end_date >=',date("Y-m-d H:i:s",time()));
            }
            if(@$search['default_root_category_sn']){
                $this->db->where('ij_product.default_root_category_sn',$search['default_root_category_sn']);
            }
            if(@$search['supplier_sn']){
                $this->db->where('ij_product.supplier_sn',$search['supplier_sn']);
            }
            if(isset($search['product_status']) && $search['product_status']!=''){
                $this->db->where('ij_product.product_status',$search['product_status']);
            }else{
                $this->db->where('ij_product.product_status <','9');
            }
            $this->db->group_by('product_sn');
            $this->db->order_by('ij_product.last_time_update','desc');
			$query =$this->db->get()->result_array();
		    //echo $this->db->last_query().'<br>';
		    if($query) {
	            foreach($query as $key => $p){
	            $query[$key]['image_path'] = $this->_select('ij_product_gallery',array("product_sn" => $p['product_sn']),0,0,1,0,0,'row','image_path')['image_path'];
                //$product_gallery_sn = $this->_select('ij_product_gallery',array("product_sn" => $p['product_sn']),0,0,1,0,0,'row','product_gallery_sn')['product_gallery_sn'];
						//$where = array('product_sn' => $p['product_sn'],'start_date <=' => date("Y-m-d H:i:s",time()),'end_date >=' => date("Y-m-d H:i:s",time()));
	            $query[$key]['promotion'] = $this->_get_promotion_label_by_psn($p['product_sn']);
	            if($p['pricing_method']=='1'){  //單一
							$price_tier=$this->libraries_model->_select('ij_product_fix_price_log','product_sn',$p['product_sn'],1,0,'price',0,'row');
							if($price_tier){
								$query[$key]['price1']=($price_tier['fixed_price'])? $price_tier['fixed_price']:'尚未';
								$query[$key]['price2']=($price_tier['price'])? $price_tier['price']:'尚未';
							}else{
								$query[$key]['price1']='尚未';
								$query[$key]['price2']='尚未';
							}
	            }else{
		  				$query[$key]['price1'] = ($price1=$this->libraries_model->_select('ij_product_price_tier_log','product_sn',$p['product_sn'],1,0,'price','desc','row')['price'])? $price1:'尚未';
		  				$query[$key]['price2'] = ($price2=$this->libraries_model->_select('ij_product_price_tier_log','product_sn',$p['product_sn'],1,0,'price',0,'row')['price'])? $price2:'尚未';
                        /*if($query[$key]['price1']=='尚未'){
                            $this->_update('ij_product',array('pricing_method'=>'1'),'product_sn',$p['product_sn']);
                            $ntssi_goods=$this->_select('ntssi_goods','gid',$p['product_sn'],1,0,'price',0,'row','pricedesc,price');
                            if($ntssi_goods){
                                $fix_price['fixed_price']=$ntssi_goods['price'];
                                $fix_price['price']=$ntssi_goods['pricedesc'];
                                $fix_price['product_sn']=$p['product_sn'];
                                $fix_price['status']='1';
                                $fix_price = $this->libraries_model->CheckUpdate($fix_price,1);
                        //var_dump($fix_price);
                                $this->libraries_model->_insert('ij_product_fix_price_log',$fix_price);
                            }
                    //exit();
                        }*/
	            }
                /*if($this->_select('ij_product_price_tier_log','product_sn',$p['product_sn'],1,0,'price','desc','num_rows')>1){
                    $this->_update('ij_product',array('pricing_method'=>'2'),'product_sn',$p['product_sn']);
                }else{
                    $this->_update('ij_product',array('pricing_method'=>'1'),'product_sn',$p['product_sn']);
                }*/

                //add img url
                //if(strpos($query[$key]['image_path'],'product')===false){
                //    $this->_update('ij_product_gallery ',array('image_path'=>'product/'.$query[$key]['image_path']),'product_gallery_sn',$product_gallery_sn);
                //}
                /*$_associated_dealer_sn=@$this->session->userdata['member_data']['associated_dealer_sn'];
                if(!isset($p['relation_status']) && $_associated_dealer_sn){
                    $this->_insert('ij_product_relation',array('product_sn'=>$p['product_sn'],'dealer_sn'=>$_associated_dealer_sn,'relation_status'=>'1'));
                    $query[$key]['relation_status']='1';
                }else{
                    if(@$this->session->userdata('member_role_system_rules')[0]['role_name']){
                        switch(@$this->session->userdata('member_role_system_rules')[0]['role_name']){
                        case '經銷商管理員':
                            $query[$key]['product_status']=$this->libraries_model->_select('ij_product_relation',array('product_sn'=>$p['product_sn'],'dealer_sn'=>$_associated_dealer_sn),0,0,0,'product_sn',0,'row')['relation_status'];
                        break;
                        case '供應商管理員':
                        break;
                        default:
                        break;
                        }
                    }
                }*/

        		//echo $this->db->last_query().'<br>';
        		//var_dump($query[$key]['promotion']);
				}
		    }
		//var_dump($query);
		    return $query;
    	}
  	}
    public function _get_product($product_sn=0,$select=0)
    {
    	if($product_sn){
	    	if($select){
	        $this->db->select($select);
	    	}else{
	        $this->db->select('ij_product.*,category_name,image_path,product_fix_price_sn,fixed_price,price,ij_member.last_name as update_member_name');
	      }
        $this->db->from("ij_product");
        $this->db->join("ij_category","ij_category.category_sn=ij_product.default_root_category_sn","left");
        $this->db->join("ij_product_gallery","ij_product_gallery.product_sn=ij_product.product_sn","left");
        $this->db->join("ij_product_fix_price_log","ij_product_fix_price_log.product_sn=ij_product.product_sn","left");
        $this->db->join('ij_member','ij_member.member_sn=ij_product.update_member_sn','left');
        $this->db->where('ij_product.product_sn',$product_sn);
        //加入供應商、經銷商機制
        if($this->session->userdata('member_role_system_rules')[0]['role_name']=='供應商管理員'){
            $this->db->where('ij_product.supplier_sn',$this->session->userdata['member_data']['associated_supplier_sn']);
        }else if($this->session->userdata('member_role_system_rules')[0]['role_name']=='經銷商管理員'){
        }
        $this->db->group_by('ij_product.product_sn');
		$query =$this->db->get()->row_array();
		//echo $this->db->last_query().'<br>';
				//var_dump($query);
		    return $query;
    	}
  	}
    public function _add_product($type=0)
    {
    	if($type){
		  	if(!$product_sn=$this->_select('ij_product',array("product_name" => '','default_channel_name'=>$type),0,0,0,'create_date','desc','row','product_sn')['product_sn']){ //找出之前新增過但名稱空白＝沒送出的編號
					$data_array = array('default_channel_name'=>$type);
				  $data_array = $this->libraries_model->CheckUpdate($data_array,0);
					$product_sn=$this->libraries_model->_insert('ij_product',$data_array);
		  	}
		    return $product_sn;
		  }else{
		  	return false;
		  }
  	}
    public function _get_Attribute_by_cat($categorys=0,$product_sn=0){ //分類抓屬性寫入關聯表再抓出
    	if($categorys && $product_sn){
				//var_dump($categorys);
				//$this->_update('ij_product_attribute_log',array('status'=>'999'),array('product_sn' => $product_sn,'status !='=>'1')); //更新status999
				$this->libraries_model->_delete('ij_product_attribute_log',array('product_sn' => $product_sn,'status !='=>'1'));
		//echo $this->db->last_query();
				foreach($categorys as $ca){
				//var_dump($ca);
					foreach($this->category_attributes($ca) as $ca_attr){
				//var_dump($ca_attr);
						foreach($this->_select('ij_attribute_value_config',array("attribute_sn" => $ca_attr['attribute_sn']),0,0,0,'create_date',0,'result','attribute_value_sn') as $key=>$ca_attr_value){
							//var_dump($ca_attr_value);
							//var_dump($ca_attr_value->attribute_value_sn);
							$where = array('product_sn' => $product_sn,'attribute_sn' => $ca_attr['attribute_sn'],'attribute_value_sn' => $ca_attr_value->attribute_value_sn);
		        	if($this->_select('ij_product_attribute_log',$where,0,0,0,'attribute_value_sn',0,'num_rows',0,0,0,0)==0){ //沒資料才新增
								$data[$key]['product_sn']=$product_sn;
								$data[$key]['attribute_sn']=$ca_attr['attribute_sn'];
								$data[$key]['attribute_value_sn']=$ca_attr_value->attribute_value_sn;
								$data[$key]['status']='0';
							  $data[$key] = $this->CheckUpdate($data[$key],1);
							//var_dump($data[$key]);
								$this->_insert('ij_product_attribute_log',$data[$key]);
							/*}else{
							  $status=$this->_select('ij_product_attribute_log',$where,0,0,0,'attribute_value_sn',0,'row','status',0,0,0)['status'];
								if($status='999')$status='0';
								$updatedata['status']= $status;
							  $updatedata = $this->CheckUpdate($updatedata,0);
								$this->_update('ij_product_attribute_log',$updatedata,$where);
								*/
							}
						}
					}
				}

				//var_dump($this->_get_product_attribute($product_sn));
				//exit();
        return $this->_get_product_attribute($product_sn);
	    }else{
	    	return array();
	    }
  	}
    public function _get_Package_by_cat($categorys=0,$product_sn=0){ //分類抓規格組合
    	if($categorys && $product_sn){
				//var_dump($categorys);
				//exit();
				//var_dump($product_sn);
				//foreach($categorys as $ca){
						//var_dump($ca_attr['attribute_sn']);
						//var_dump($ca_attr_value->attribute_value_sn);
						//$where = array('category_sn' => $ca);
		        $this->db->select('ij_category_product_package_relation.product_package_config_sn,product_package_name');
		        $this->db->from('ij_category_product_package_relation');
		        $this->db->join('ij_product_package_config','ij_product_package_config.product_package_config_sn=ij_category_product_package_relation.product_package_config_sn');
		        $this->db->where_in('ij_category_product_package_relation.category_sn',$categorys);
		        $this->db->where('ij_product_package_config.status','1');
		        $this->db->order_by('ij_product_package_config.sort_order');
            $this->db->group_by('ij_category_product_package_relation.product_package_config_sn');
		        $array1=$this->db->get()->result_array();
				//}
				//var_dump($array1);
	        $this->db->select('ij_product_fix_price_log.product_package_config_sn,fixed_price,price,status,product_fix_price_sn');
	        $this->db->from('ij_product_fix_price_log');
	        $this->db->where('ij_product_fix_price_log.product_sn',$product_sn);
	        $this->db->order_by('product_package_config_sn');
	        $array2=$this->db->get()->result_array();
				foreach($array1 as $key1=>$row1){
					foreach($array2 as $key2=>$row2){
						if($row1['product_package_config_sn']==$row2['product_package_config_sn']){
							$array1[$key1]['product_fix_price_sn']=$row2['product_fix_price_sn'];
							$array1[$key1]['fixed_price']=$row2['fixed_price'];
							$array1[$key1]['price']=$row2['price'];
							$array1[$key1]['status']=$row2['status'];
						}
					}
				}
				//var_dump($array1);
				//exit();
	    	return $array1;
	    }else{
	    	return array();
	    }
  	}
    public function _get_product_attribute($product_sn=0,$ifstatus=0,$ifstatus2=0,$ifstatus3=0){ //抓產品屬性記錄檔
    	if($product_sn){
    		$attribute_value_name='';
            $this->db->select('ij_product_attribute_log.product_sn,ij_product_attribute_log.attribute_sn,attribute_name');
            $this->db->from('ij_product_attribute_log');
            $this->db->join('ij_attribute_config','ij_attribute_config.attribute_sn=ij_product_attribute_log.attribute_sn','left');
            $this->db->where('ij_product_attribute_log.product_sn',$product_sn);
            $this->db->where('ij_attribute_config.status',"1"); //2020-1-3 add
            $this->db->group_by('ij_product_attribute_log.attribute_sn');
            $this->db->order_by('ij_attribute_config.sort_order');
            $array=$this->db->get()->result_array();
            $attribute_values='';
            foreach($array as $key=>$row){
    	        $this->db->select('ij_product_attribute_log.attribute_value_sn,attribute_value_name,ij_product_attribute_log.status');
    	        $this->db->from('ij_product_attribute_log');
    	        $this->db->join('ij_attribute_value_config','ij_attribute_value_config.attribute_value_sn=ij_product_attribute_log.attribute_value_sn','left');
    	        $this->db->where('ij_product_attribute_log.product_sn',$product_sn);
    	        $this->db->where('ij_product_attribute_log.attribute_sn',$row['attribute_sn']);
    	        $this->db->order_by('ij_attribute_value_config.sort_order');
            	if($ifstatus){ //找出status=1的屬性名稱（應該只會有一個）
    	        	$this->db->where('ij_product_attribute_log.status','1');
    	        	$temprow=$this->db->get()->row_array();
    	        	if($temprow){
    	        		$attribute_value_name=$temprow['attribute_value_name'];
    	        	}
                }elseif($ifstatus3){
                    //去除完全沒勾選的屬性
                    $array[$key]['attribute_values']=$this->db->get()->result_array();
                    $_ifchecked=false;
                    foreach($array[$key]['attribute_values'] as $_key=>$_item){
                        if($_item['status']=='1'){
                            $_ifchecked=true;
                        }
                    }
                    if(!$_ifchecked){
                        unset($array[$key]);
                    }
    	        }else{
                    if($ifstatus2){
                        $this->db->where('ij_product_attribute_log.status','1');
                        $attribute_value_array=$this->db->get()->result_array();
                        if($attribute_value_array){
                        //var_dump($attribute_value_array);
                            foreach($attribute_value_array as $key2=>$row2){
                                $attribute_values.=trim($row2['attribute_value_name']).' ';
                            }
                        }
                        $array['attribute_values']=$attribute_values;
                    }else{
                        $array[$key]['attribute_values']=$this->db->get()->result_array();
                    }
    	        }
            }
        if($ifstatus){
        	return intval($attribute_value_name);
        }else{
        	return $array;
        }
        //依據分類抓規格組合
	      //$array['packages']=_select('ij_product_attribute_log',$where,0,0,0,'attribute_value_sn',0,'num_rows';
	    }else{
	    	return array();
	    }
  	}
    public function _get_addon_list($apply_product_sn=0)
    {
    	if($apply_product_sn){
            $this->db->select('addon_log_sn,addon_price,product_orginal_price,product_name,product_eng_name,addon_limitation,associated_product_sn,unispec_flag,unispec_qty_in_stock,product_type,open_preorder_flag');
            $this->db->from("ij_addon_log");
            $this->db->join("ij_addon_config","ij_addon_config.addon_config_sn=ij_addon_log.addon_config_sn","left");
            $this->db->join("ij_product","ij_product.product_sn=ij_addon_log.associated_product_sn","left");
            $this->db->where('apply_product_sn',$apply_product_sn);
            $this->db->where('ij_addon_log.status','1');
            //$this->db->where('unispec_qty_in_stock >','0'); 不管庫存
            $this->db->where('product_status!=','9');//刪除不抓
            $this->db->order_by('ij_addon_log.create_date','desc');
    		$query =$this->db->get()->result_array();
			//$query_count=count($query);
			//echo $this->db->last_query().'<br>';
			/*$this->load->model('Shop_model');
			$query2=$this->Shop_model->_get_products($query[0]['default_channel_name'],0,105); //加購分類id:105
			foreach($query as $_key=>$_Item){
				if($_Item['product_type']==100){ //虛擬商品庫存預設100
					$query[$_key]['unispec_qty_in_stock']=100;
				}
			}
            */
		    return $query;
    	}
  	}
     public function _chk_addon($associated_product_sn=0,$apply_product_sn=0)
    {
    	if($associated_product_sn && $apply_product_sn){
		    if($query=$this->_get_addon_list($apply_product_sn)) {
	        foreach($query as $key => $p){
	        	if (@$p['associated_product_sn']==$associated_product_sn){
		    			return true; // 已有加價購
		    			exit();
	        	}
					}
    		return false;
		    }
	    }else{
	    	return false;
	  	}
  	}
    public function _get_Track_list($from=0,$to=0,$user_name=0,$product_sn=0,$product_name=0,$page=0,$perpage=0,$ifpic=0)
    {
        $this->db->select('track_list_sn,create_date,product_sn,product_name,user_name,ij_track_list_log.member_sn');
        $this->db->from("ij_track_list_log");
        $this->db->join("ij_member","ij_member.member_sn=ij_track_list_log.member_sn","left");
    		if($from){
        	$this->db->where('create_date >=',$from.'-01');
        }
    		if($to){
        	$this->db->where('create_date <=',$to.'-31');
        }
    		if($user_name){
        	$this->db->where('user_name',$user_name);
        }
    		if($product_sn){
        	$this->db->where('product_sn',$product_sn);
        }
    		if($product_name){
        	$this->db->where('product_name',$product_name);
        }
        $this->db->where('track_list_status','1');
        $this->db->order_by('ij_track_list_log.create_date','asc');
        $this->db->order_by('ij_track_list_log.product_sn','asc');
        $this->db->order_by('user_name','asc');
        if($page >= 1)$page=$page-1;
        if($page && $perpage){
            $this->db->limit($perpage,$page*$perpage);
        }elseif(!$page && $perpage){
            $this->db->limit($perpage);
        }
        $array = $this->db->get()->result_array();
		//echo $ifpic;
		    if($ifpic) {
			    if($array) {
						$this->load->model('Shop_model');
		        foreach($array as $key => $p){
		          $array[$key]['product'] = $this->Shop_model->_get_product($p['product_sn']);
		          //$array[$key]['image_alt'] = $product['image_alt'];
		          //$array[$key]['price1'] = $product['price1'];
		          //$array[$key]['price2'] = $product['price2'];
		        }
		      }
		    }

				return $array;
  	}
    public function _get_promotion_label_by_psn($product_sn=0){ //產品跟促銷標籤
    	if($product_sn){
	        $this->db->select('promotion_label_sn,promotion_label_name,promotion_label_content_save_dir');
	        $this->db->from('ij_promotion_label_config');
	        //$this->db->join('ij_product_promotion_label_relation','ij_product_promotion_label_relation.promotion_label_sn=ij_promotion_label_config.promotion_label_sn','left');
	        $this->db->where('ij_promotion_label_config.status','1');
	        $this->db->order_by('ij_promotion_label_config.promotion_label_sn');
          //$this->db->group_by('ij_category_product_package_relation.product_package_config_sn');
	        $array1=$this->db->get()->result_array();

	        $this->db->select('product_promotion_label_relation_sn,promotion_label_sn,status,start_date,end_date,discount_percentage,discount_amount');
	        $this->db->from('ij_product_promotion_label_relation');
					$where = array('product_sn' => $product_sn,'start_date <=' => date("Y-m-d H:i:s",time()),'end_date >=' => date("Y-m-d H:i:s",time()));
	        $this->db->where($where);
	        $this->db->order_by('promotion_label_sn');
	        $array2=$this->db->get()->result_array();
//var_dump($array2);
				foreach($array1 as $key1=>$row1){
					foreach($array2 as $key2=>$row2){
						if($row1['promotion_label_sn']==@$row2['promotion_label_sn']){
                            $array1[$key1]['product_promotion_label_relation_sn']=$row2['product_promotion_label_relation_sn'];
							$array1[$key1]['start_date']=$row2['start_date'];
							$array1[$key1]['end_date']=$row2['end_date'];
							$array1[$key1]['status']=$row2['status'];
							$array1[$key1]['discount_amount']=$row2['discount_amount'];
							$array1[$key1]['discount_percentage']=$row2['discount_percentage'];
						}
					}
				}
				//var_dump($array1);
				//exit();
	    	return $array1;
	    }else{
	    	return array();
	    }
  	}
    public function _get_Inventory_list($product_sn=0,$purchase_sales_stock_type=0,$page=0,$perpage=0)
    {
        $this->db->select('purchase_sales_stock_sn,category_name,ij_purchase_sales_stock_log.product_sn,product_name,ij_purchase_sales_stock_log.cost,purchase_sales_stock_type,actua_spec_option_amount,ij_purchase_sales_stock_log.last_time_update,ij_purchase_sales_stock_log.update_member_sn,color_name,ij_purchase_sales_stock_log.description');
        $this->db->from("ij_purchase_sales_stock_log");
        $this->db->join("ij_product","ij_product.product_sn=ij_purchase_sales_stock_log.product_sn","left");
        $this->db->join("ij_category","ij_product.default_root_category_sn=ij_category.category_sn","left");
        $this->db->join("ij_mutispec_stock_log","ij_mutispec_stock_log.mutispec_stock_sn=ij_purchase_sales_stock_log.associated_mutispec_stock_sn","left");
    		if($product_sn){
        	$this->db->where('ij_purchase_sales_stock_log.product_sn',$product_sn);
        }
    		if($purchase_sales_stock_type){
        	$this->db->where('purchase_sales_stock_type',$purchase_sales_stock_type);
        }
        //加入供應商、經銷商機制
        if($this->session->userdata('member_role_system_rules')[0]['role_name']=='供應商管理員'){
            $this->db->where('ij_product.supplier_sn',$this->session->userdata['member_data']['associated_supplier_sn']);
        }else if($this->session->userdata('member_role_system_rules')[0]['role_name']=='經銷商管理員'){
        }

        $this->db->where('purchase_sales_stock_status','1');
        //$this->db->order_by('ij_purchase_sales_stock_log.product_sn','asc');
        $this->db->order_by('last_time_update','desc');
        if($page >= 1)$page=$page-1;
        if($page && $perpage){
            $this->db->limit($perpage,$page*$perpage);
        }elseif(!$page && $perpage){
            $this->db->limit($perpage);
        }
        $array = $this->db->get()->result_array();
		//echo $this->db->last_query();

				return $array;
  	}
    public function _get_Inventory_item_by_product_sn($product_sn=0)
    {
    	if($array=$this->libraries_model->_select('ij_product','product_sn',$product_sn,1,0,0,0,'row','product_name,unispec_flag,unispec_qty_in_stock')){
				$array['product_sn']=$product_sn;
				if($array['unispec_flag']){ //單一規格
					$array['current_amount']=$array['unispec_qty_in_stock'];
				//}else{
					//多重規格庫存量點了規格才秀
					//$array['current_amount']=$this->libraries_model->_select('ij_mutispec_stock_log','mutispec_stock_sn',$product_sn,0,0,'actua_spec_option_amount',0,'row','sum(actua_spec_option_amount)as current_amount')['current_amount'];
				}
				return $array;
			}else{
				return false;
			}
  	}
    public function _get_Inventory_item($purchase_sales_stock_sn=0)
    {
    	$array=$this->libraries_model->_select('ij_purchase_sales_stock_log','purchase_sales_stock_sn',$purchase_sales_stock_sn,1,0,'purchase_sales_stock_sn',0,'row');
    	//var_dump($array);
			//$array['product_name']=$this->libraries_model->_select('ij_product','product_sn',$array['product_sn'],1,0,0,0,'row','product_name')['product_name'];
    	if($array2=$this->libraries_model->_select('ij_product','product_sn',$array['product_sn'],1,0,0,0,'row','product_name,unispec_flag,unispec_qty_in_stock')){
				$array['unispec_flag']=$array2['unispec_flag'];
				$array['product_name']=$array2['product_name'];
				if($array2['unispec_flag']){ //單一規格
			    //$array['current_amount']=$this->libraries_model->_select('ij_purchase_sales_stock_log','product_sn',$array['product_sn'],0,0,'actua_spec_option_amount',0,'row','sum(actua_spec_option_amount)as current_amount')['current_amount'];
			    $array['current_amount']=$array2['unispec_qty_in_stock'];
				}else{
					//多重規格
					//echo $array['associated_mutispec_stock_sn'];
			    $array['current_amount']=($qty=$this->_select('ij_mutispec_stock_log','mutispec_stock_sn',$array['associated_mutispec_stock_sn'],0,0,'qty_in_stock',0,'row','qty_in_stock')['qty_in_stock'])? $qty:'0';
//$qty=$this->_select('ij_mutispec_stock_log','mutispec_stock_sn',$array['associated_mutispec_stock_sn'],0,0,'qty_in_stock',0,'row','qty_in_stock')['qty_in_stock'];
			    //echo var_dump($array['current_amount']);
				}
			}
		//echo $this->db->last_query();
			return $array;
  	}
	public function _Get_Template_Color($product_template_sn,$associated_template_color_relation_sn=0){
		if($product_template_sn){
			$picture=array();
			if($associated_template_color_relation_sn){ //單一顏色
				$where = array('product_template_sn' => $product_template_sn,'associated_template_color_relation_sn'=>$associated_template_color_relation_sn);
		  	$picture['photos']=$this->libraries_model->_select('ij_template_picture',$where,'',0,5,0,0,'result_array');
			  $website_color_sn=$this->libraries_model->_select('ij_template_color_relation','template_color_relation_sn',$associated_template_color_relation_sn,1,0,0,0,'row')['website_color_sn'];
			  $picture['color_name']=$this->libraries_model->_select('ij_website_color','website_color_sn',$website_color_sn,1,0,0,0,'row')['color_name'];
				$picture['associated_template_color_relation_sn']=$associated_template_color_relation_sn;
			//exit();
			}else{ //所有顏色
				//$where = array('product_template_sn' => $product_template_sn);
				//$pictures=$this->libraries_model->_select('ij_template_picture',$where,'',5,0,'associated_template_color_relation_sn',0,'result_array',0,0,'associated_template_color_relation_sn');
				$pictures=$this->_Get_distinct_associated_template_color_relation_sn($product_template_sn);
			//echo $this->db->last_query();
			  foreach($pictures as $key=>$Item){
					$where = array('product_template_sn' => $product_template_sn,'associated_template_color_relation_sn'=>$Item['associated_template_color_relation_sn']);
			  	$picture[$key]['photos']=$this->libraries_model->_select('ij_template_picture',$where,'',0,5,0,0,'result_array');
				  $website_color_sn=$this->libraries_model->_select('ij_template_color_relation','template_color_relation_sn',$Item['associated_template_color_relation_sn'],1,0,0,0,'row','website_color_sn')['website_color_sn'];
			  //var_dump($website_color_sn);
			  	if($website_color_sn){
					  $picture[$key]['website_color_sn']=$website_color_sn;
					  $website_color=$this->libraries_model->_select('ij_website_color','website_color_sn',$website_color_sn,1,0,0,0,'row','color_name,sort_order');
					  $picture[$key]['sort_order']=$website_color['sort_order'];
					  $picture[$key]['color_name']=$website_color['color_name'];
					}else{
						unset($picture[$key]); //沒選取顏色
					}
				  //$picture[$key]['associated_template_color_relation_sn']=$Item['associated_template_color_relation_sn'];
				}
				$picture=array_values($picture); //ReIndex
			}

			return $picture;

			//echo $this->db->last_query();
			//exit();
		}
	}

	public function _Get_Template_divider($product_template_sn){
		if($product_template_sn){
				$where = array('product_template_sn' => $product_template_sn,'status'=>'1');
		  	$dividers=$this->_select('ij_template_divider_config',$where,'',0,0,'associated_area',0,'result_array');
			//echo $this->db->last_query();
        $divider_area = array(
            'album'=> '精彩影音',
            'story'=> '愛的故事',
            'invitation'=> '婚禮訊息',
            'rsvp'=> '出席回覆',
            'guestbook'=> '留言祝福',
            'thanks'=> '感謝有您',
            'cowndown'=> '婚禮倒數',
            'gift'=> '線上禮金',
        );
				foreach($dividers as $key=>$row){
					$dividers[$key]['area_name']=$divider_area[$row['associated_area']];
				}
				//var_dump($dividers);
				return $dividers;
			//echo $this->db->last_query();
			//exit();
		}
	}

	private function _Get_distinct_associated_template_color_relation_sn($product_template_sn){
    $this->db->select('associated_template_color_relation_sn');
    $this->db->distinct();
    $this->db->from('ij_template_picture');
    $this->db->join('ij_template_color_relation','ij_template_color_relation.template_color_relation_sn=ij_template_picture.associated_template_color_relation_sn','left');
    $this->db->join('ij_website_color','ij_website_color.website_color_sn=ij_template_color_relation.website_color_sn','left');
    $this->db->where('ij_template_picture.product_template_sn',$product_template_sn);
    $this->db->order_by('ij_template_picture.sort_order','asc'); //小的優先
    return $this->db->get()->result_array();
	}
  public function _add_product_template()
  {
	  	if(!$product_template_sn=$this->_select('ij_product_template_config',array("product_template_name" => ''),0,0,0,'create_date','desc','row','product_template_sn')['product_template_sn']){ //找出之前新增過但名稱空白＝沒送出的編號
				$data_array = array('status'=>'0','product_template_name'=>'');
			  $data_array = $this->libraries_model->CheckUpdate($data_array,0);
				$product_template_sn=$this->libraries_model->_insert('ij_product_template_config',$data_array);
	  	}
	    return $product_template_sn;
	}
	public function _get_templates_colors($product_sn){
		if($product_sn){
			  //所有版型
        $this->db->select('product_template_sn,product_template_name,template_demo_website');
        $this->db->from('ij_product_template_config');
        $this->db->where('ij_product_template_config.status','1');
        $this->db->order_by('ij_product_template_config.sort_order');
        $array1=$this->db->get()->result_array();
        //已選版型顏色
        $this->db->select('promotion_label_sn,status,start_date,end_date');
        $this->db->from('ij_product_promotion_label_relation');
				$where = array('product_sn' => $product_sn,'start_date <=' => date("Y-m-d H:i:s",time()),'end_date >=' => date("Y-m-d H:i:s",time()));
        $this->db->where($where);
        $this->db->order_by('promotion_label_sn');
        $array2=$this->db->get()->result_array();
				//var_dump($array2);
				foreach($array1 as $key1=>$row1){
					foreach($array2 as $key2=>$row2){
						if($row1['promotion_label_sn']==@$row2['promotion_label_sn']){
							$array1[$key1]['start_date']=$row2['start_date'];
							$array1[$key1]['end_date']=$row2['end_date'];
							$array1[$key1]['status']=$row2['status'];
						}
					}
				}
				//var_dump($array1);
				//exit();
	    	return $array1;
	    }else{
	    	return array();
	    }
	}
	public function _update_field_amount($table,$where,$where_val,$field,$value){
    //$sql="update FROM $table set $field=$field $value where $field='".$user['id']."' and status=107 ";
    $sql="update $table set $field=$field $value where $where='$where_val'";
    return $this->db->query($sql);
	}

    public function _get_min_price($product_sn=0,$pricing_method=0,$chktime=0){ //產品編號、定價方式、時間，找最低售價
       if($product_sn && $pricing_method){
            if($pricing_method=='2'){ //區間定價找出最低金額
							$where = array('product_sn' => $product_sn);
							if(!$price=$this->_select('ij_product_price_tier_log',$where,0,1,0,'price',0,'row','price')['price']){
	    					return false;
							}
          	}elseif($pricing_method=='1'){ //單一定價
							$where = array('product_sn' => $product_sn,'price >'=>0);
								if(!$price=$this->_select('ij_product_fix_price_log',$where,'',1,0,'price',0,'row','price')['price']){
		    					return false;
								}
            }
	        if(!$chktime){
	        	$chktime=date("Y-m-d H:i:s",time());
	        }
					$where = array('product_sn' => $product_sn,'start_date <=' => $chktime,'end_date >=' => $chktime,'promotion_label_sn'=>'3','status'=>'1'); //3:特價
					$promo_sale=$this->_select('ij_product_promotion_label_relation',$where,0,1,0,'discount_percentage',0,'row','discount_percentage,discount_amount');
					if($promo_sale){
						if($promo_sale['discount_percentage'] > 0 && $promo_sale['discount_percentage'] < 1){ //有折扣
							$price = round($price - ($price * $promo_sale['discount_percentage']));
						}elseif($promo_sale['discount_amount'] > 0){
							$price = ($price=$price - intval($promo_sale['discount_amount']) > 0)? $price:'1';
						}
					}
	    	return $price;
	    }else{
	    	return false;
	    }
  	}
	  public function get_unread_message_count(){
	  	if(@$this->session->userdata['member_data']['member_sn']){
				return $this->_select('ij_member_message_center_log',array('default_display_time <='=>date('Y-m-d H:i:s'),'message_status'=>'1','ij_member_message_center_log.member_sn'=>@$this->session->userdata['member_data']['member_sn'],'read_flag'=>null),0,0,0,'read_flag',0,'num_rows');
	  	}else{
	  		return '0';
	  	}
	  }
    public function _get_callcenter_list($where=0,$process_ccagent_member_sn=0)
    {
        $this->db->select('*,ij_member_question.last_time_update as last_time_update1,ij_member_question.member_sn as q_member_sn');
        $this->db->from("ij_member_question");
        $this->db->join("ij_ccapply_detail_code_ct","ij_member_question.ccapply_detail_code=ij_ccapply_detail_code_ct.ccapply_detail_code","left");
        $this->db->join("ij_member","ij_member.member_sn=ij_member_question.member_sn","left");
        $this->db->join('ij_sub_order','ij_sub_order.sub_order_sn=ij_member_question.associated_sub_order_sn','left');
        $this->db->join("ij_member_question_status_ct","ij_member_question.question_status=ij_member_question_status_ct.member_question_status","left");
        $this->db->join('ij_product','ij_product.product_sn=ij_member_question.product_sn','left');
    	if($where){
        	$this->db->where($where);
        }
        //加入供應商、經銷商機制
        if($this->session->userdata('member_role_system_rules')[0]['role_name']=='供應商管理員'){
            $this->db->group_start();
            $this->db->where('ij_sub_order.associated_supplier_sn',$this->session->userdata['member_data']['associated_supplier_sn']);
            $this->db->or_where('ij_product.supplier_sn',$this->session->userdata['member_data']['associated_supplier_sn']);
            $this->db->group_end();
        }else if($this->session->userdata('member_role_system_rules')[0]['role_name']=='經銷商管理員'){
            $this->db->join('ij_sub_order','ij_sub_order.sub_order_sn=ij_member_question.associated_sub_order_sn','left');
            $this->db->where('ij_sub_order.associated_dealer_sn',$this->session->userdata['member_data']['associated_dealer_sn']);
        }

    	if($process_ccagent_member_sn){
        	$this->db->where('process_ccagent_member_sn',$process_ccagent_member_sn);
	      	$this->db->join('ij_member_question_qna_log','ij_member_question_qna_log.member_question_sn=ij_member_question.member_question_sn','left');
	      	$this->db->group_by('ij_member_question.member_question_sn');
        }
        //$this->db->where('question_status','1');
        $this->db->order_by('ij_member_question.question_status','asc');
        $this->db->order_by('ij_member_question.issue_date','asc');
        /*if($page >= 1)$page=$page-1;
        if($page && $perpage){
            $this->db->limit($perpage,$page*$perpage);
        }elseif(!$page && $perpage){
            $this->db->limit($perpage);
        }*/
        $array = $this->db->get()->result_array();
        foreach($array as $key => $p){
        	//var_dump($p);
          $array[$key]['member_name'] = $this->_select('ij_member','ij_member.member_sn',$p['create_member_sn'],0,0,'ij_member.member_sn',0,'row','ij_member.last_name as last_name1')['last_name1'];
          $array[$key]['reply_count'] = $this->_get_question_qna_log($p['member_question_sn'],$p['member_sn'],1);
          if($p['supplier_sn']){
            $array[$key]['supplier_name'] = $this->_select('ij_supplier','ij_supplier.supplier_sn',$p['supplier_sn'],0,0,'ij_supplier.supplier_sn',0,'row','ij_supplier.supplier_name')['supplier_name'];

          }
        }
				return $array;
  	}
    public function _get_question_qna_log($member_question_sn=0,$member_sn=0,$count=0)
    {
    	if($member_question_sn){
        $this->db->select('*,ij_member.last_name as process_ccagent_member_name');
        $this->db->from("ij_member_question_qna_log");
        $this->db->join("ij_member_question","ij_member_question.member_question_sn=ij_member_question_qna_log.member_question_sn","left");
	    $this->db->join('ij_member','ij_member.member_sn=ij_member_question_qna_log.process_ccagent_member_sn','left');
        $this->db->where('ij_member_question_qna_log.member_question_sn',$member_question_sn);
        //加入供應商、經銷商機制
        if($this->session->userdata('member_role_system_rules')[0]['role_name']=='供應商管理員'){
            $this->db->join('ij_sub_order','ij_sub_order.sub_order_sn=ij_member_question.associated_sub_order_sn','left');
            $this->db->where('ij_sub_order.associated_supplier_sn',$this->session->userdata['member_data']['associated_supplier_sn']);
        }else if($this->session->userdata('member_role_system_rules')[0]['role_name']=='經銷商管理員'){
            $this->db->join('ij_sub_order','ij_sub_order.sub_order_sn=ij_member_question.associated_sub_order_sn','left');
            $this->db->where('ij_sub_order.associated_dealer_sn',$this->session->userdata['member_data']['associated_dealer_sn']);
        }
        //$this->db->where('member_sn',$member_sn);
        //$this->db->where('question_status','1');
        $this->db->order_by('ij_member_question_qna_log.qna_date','asc');
        if($count){
        	$array = $this->db->get()->num_rows();
        }else{
        	$array['qna_log'] = $this->db->get()->result_array();
        	$where['member_question_sn']=$member_question_sn;
        	$array['member_question']=$this->_get_callcenter_list($where);
        	//var_dump($array['member_question']);
        }
				return $array;
			}
  	}

//總控抓取訂單s
public function get_orders($data_array,$_if_order_items=0){
	      $this->db->select('*,ij_sub_order.memo as memo,share_percent,partner_member.last_name as partner_name,ij_member.last_name asorder_process_member_name,ij_order.member_sn as member_sn,ij_sub_order.associated_dealer_sn as associated_dealer_sn,dealer_order_status_name,ij_payment_status_ct.payment_status_name as payment_status_name,pay2.payment_status_name as pay_status_name,ij_order.payment_status as payment_status');
	      $this->db->from("ij_sub_order");
	      $this->db->join("ij_delivery_method_ct","ij_delivery_method_ct.delivery_method=ij_sub_order.delivery_method","left");
	      $this->db->join("ij_delivery_status_ct","ij_delivery_status_ct.delivery_status=ij_sub_order.delivery_status","left");
	      $this->db->join("ij_sub_order_status_ct","ij_sub_order_status_ct.sub_order_status=ij_sub_order.sub_order_status","left");
	      $this->db->join("ij_order","ij_order.order_sn=ij_sub_order.order_sn","left");
	      $this->db->join("ij_payment_method_ct","ij_payment_method_ct.payment_method=ij_order.payment_method","left");
          $this->db->join("ij_payment_status_ct","ij_payment_status_ct.payment_status=ij_order.payment_status","left");
          $this->db->join("ij_payment_status_ct as pay2","pay2.payment_status=ij_sub_order.pay_status","left");
	      $this->db->join("ij_member","ij_member.member_sn=ij_sub_order.order_process_member_sn","left");
          $this->db->join("ij_supplier","ij_supplier.supplier_sn=ij_sub_order.associated_supplier_sn","left");
          $this->db->join("ij_dealer","ij_dealer.dealer_sn=ij_sub_order.associated_dealer_sn","left");
          $this->db->join("ij_dealer_order_status_ct","ij_dealer_order_status_ct.dealer_order_status=ij_order.original_order_sn","left");
          $this->db->join("ij_member as partner_member","partner_member.member_sn=ij_order.associated_member_sn","left");
        //加入供應商機制
        if($this->session->userdata('member_role_system_rules')[0]['role_name']=='供應商管理員'){
        	$this->db->where('ij_sub_order.associated_supplier_sn',$this->session->userdata['member_data']['associated_supplier_sn']);
        }else if($this->session->userdata('member_role_system_rules')[0]['role_name']=='經銷商管理員'){
            $this->db->where('ij_sub_order.associated_dealer_sn',$this->session->userdata['member_data']['associated_dealer_sn']);
        }
	      //$this->db->join("ij_member","ij_member.member_sn=ij_order.member_sn","left");
				if(is_array($data_array)){
					foreach($data_array as $key=>$Item){
						//echo $key.' xxx '.$Item.'<br>';
						if(null!==$Item){
							if($key=='from' && $Item) $this->db->where('order_create_date >=',$Item.' 00:00:00');
							if($key=='to' && $Item) $this->db->where('order_create_date <=',$Item.' 23:59:59');
							if($key=='user_name' && $Item){
								 $this->db->like('bbb.user_name',$Item);
								 $this->db->join("ij_member as bbb","bbb.member_sn=ij_sub_order.associated_member_sn","left");
							}
							if($key=='member_sn'){
								 $this->db->where('bbb.member_sn',$Item);
								 $this->db->join("ij_member as bbb","bbb.member_sn=ij_sub_order.associated_member_sn","left");
							}
                            if($key=='channel_name') $this->db->where('channel_name',$Item);
                            if($key=='supplier_sn' && $Item) $this->db->where('ij_sub_order.associated_supplier_sn',$Item);
                            if($key=='dealer_sn') $this->db->where('ij_sub_order.associated_dealer_sn',$Item);
							if($key=='sub_order_status' && $Item) $this->db->where('ij_sub_order.sub_order_status',$Item);
                            if($key=='payment_status' && $Item) $this->db->where('ij_order.payment_status',$Item);
                            if($key=='pay_status' && $Item) $this->db->where('ij_sub_order.pay_status',$Item);
							if($key=='admin_user'  && $Item) $this->db->where('order_process_member_sn',$Item);
							if($key=='sub_order_sn' && $Item) $this->db->where('ij_sub_order.sub_order_sn',$Item);
                            if($key=='checkstatus') $this->db->where('checkstatus',$Item);
                            if($key=='associated_member_sn') $this->db->where('ij_order.associated_member_sn',$Item);
                            if($key=='original_order_sn') $this->db->where('original_order_sn',$Item);
                            if($key=='partner_name') $this->db->where('partner_member.last_name',$Item);
                            if($key=='supplier_name') $this->db->where('supplier_name',$Item);
                            if($key=='dealer_checkstatus') $this->db->where('dealer_checkstatus',$Item);
                            if($key=='order_num') $this->db->where('order_num',$Item);
                            if($key=='ij_order.associated_member_sn>') $this->db->where('ij_order.associated_member_sn>','0');
						}
					}
				}elseif($data_array){
                    $this->db->where($data_array);
                    $this->db->limit(5);
                }
	        //$this->db->where('order_sn',$Item['order_sn']);
            if($_if_order_items==2){ //partner
                $this->db->where('ij_order.associated_member_sn!=','');
            }elseif($_if_order_items==3){ //dealer
                $this->db->where('ij_sub_order.associated_dealer_sn!=','');
            }elseif($_if_order_items==4){ //supplier
                $this->db->where('ij_sub_order.associated_supplier_sn!=','');
            }
            $this->db->order_by('ij_order.order_create_date','desc');
	        $result=$this->db->get()->result_array();
            //echo $this->db->last_query();
            $total=0;
                $this->load->model('Shop_model');
                $this->load->library('ijw');
				foreach($result as $key=>$Item){
					//是否重計算總重量
					/*if($Item['total_weight']=='0.0000'){
						$result[$key]['total_weight']=$this->sum_weight_by_suborder($Item['sub_order_sn']);
					}*/
					//$result[$key]['sub_order'][$key2]['order_item']=$this->_select('ij_order_item','sub_order_sn',$Item2['sub_order_sn'],0,0,'order_item_sn',0,'result_array');
                    if($member_question=$this->_get_callcenter_list(array('associated_sub_order_sn'=>$Item['sub_order_sn']))){
                         $result[$key]['member_question_sn']=$member_question[0]['member_question_sn'];
                    }
                    if($_if_order_items==1){
    			      $this->db->select('*,ij_order_item.order_item_sn as order_item_sn,ij_order_item.product_sn as product_sn,ij_order_item.product_name as product_name,ij_order_item.unispec_warehouse_num as unispec_warehouse_num,receive_goods_flag');
    			      $this->db->from("ij_order_item");
                      $this->db->join("ij_product","ij_product.product_sn=ij_order_item.product_sn","left");
    			      $this->db->join("ij_order_cancel_log","ij_order_cancel_log.order_item_sn=ij_order_item.order_item_sn","left");
                      $this->db->join("ij_order_cancel_reason_ct","ij_order_cancel_reason_ct.order_cancel_reason_code=ij_order_cancel_log.order_cancel_reason_code","left");
    			      $this->db->where('sub_order_sn',$Item['sub_order_sn']);
    			      $result[$key]['order_item']=$this->db->get()->result_array();
                      //var_dump($result[$key]['order_item']);
                        //echo $this->db->last_query();
                    }
                    $result[$key]['sub_sum']=$this->_select('ij_order_item','sub_order_sn',$Item['sub_order_sn'],0,0,'order_item_sn',0,'row','sum(actual_sales_amount) as sub_sum')['sub_sum'];
                    $cancel_sum=$this->_select('ij_order_cancel_log','associated_sub_order_sn',$Item['sub_order_sn'],0,0,'order_item_sn',0,'row','sum(return_amount) as sub_sum')['sub_sum'];
                    if($cancel_sum>0) $result[$key]['sub_sum']=$result[$key]['sub_sum']-$cancel_sum;
                    if($Item['checkstatus']=='1' || $Item['checkstatus']=='2'){ //待出帳或已出帳
                        if($_if_order_items==4){
                            $result[$key]['share_total']=round($result[$key]['sub_sum']*(1-$Item['share_percent']));
                        }elseif($_if_order_items==3){
                            $this->load->model('member/Dealer_table');
                            //var_dump($Item['associated_dealer_sn']);
                            $result[$key]['dealer_bonus']=$this->Dealer_table->get_newmember($Item['associated_dealer_sn'])['dealer_bonus']/100;
                            $result[$key]['share_total']=round($result[$key]['sub_sum']*$Item['share_percent']*$result[$key]['dealer_bonus']);
                        }else{
                            $this->load->model('member/partner_table');
                            $result[$key]['bonus']=$this->partner_table->get_newmember(0,$Item['associated_member_sn'])['bonus']/100;
                            $result[$key]['share_total']=round($result[$key]['sub_sum']*$Item['share_percent']*$result[$key]['bonus']);
                        }
                        //var_dump($result[$key]['share_total']);
                    }else{
                         $result[$key]['share_total']=0;
                    }
                    $total+=$result[$key]['share_total'];
                    if(!$Item['pay_status']){
                        $result[$key]['pay_status']=$Item['payment_status'];
                        $_sub_order['pay_status']=$Item['payment_status'];
                        $this->_update('ij_sub_order',$_sub_order,'sub_order_sn',$Item['sub_order_sn']);
                    }
					//foreach($result[$key]['sub_order'][$key2]['order_item'] as $key3=>$Item3){
					//	$result[$key]['sub_order'][$key2]['order_item'][$key3]
					//}
					$result[$key]['sub_count']=$this->_select('ij_order_item','sub_order_sn',$Item['sub_order_sn'],0,0,'order_item_sn',0,'num_rows');
                    //echo $this->Shop_model->get_order_num($Item['order_create_date']).'<br>';
                    /*if(!$Item['order_num']){
                        $_order=array();
                        $_order['order_num']= $this->Shop_model->get_order_num($Item['order_create_date']);
                        $this->_update('ij_order',$_order,'order_sn',$Item['order_sn']);
                        $Item['order_num']=$_order['order_num'];
                    }
                    if(!$Item['sub_order_num']){
                        $_sub_order=array();
                        $_sub_order['sub_order_num']=$Item['order_num'].str_pad( $Item['supplier_sn'] , 4 , '0' , STR_PAD_LEFT );
                        $this->_update('ij_sub_order',$_sub_order,'sub_order_sn',$Item['sub_order_sn']);
                    }*/
				}
                $result['total']=$total;
				//$result[$key]['sub_count']=$this->_select('ij_sub_order','order_sn',$Item['order_sn'],0,0,'order_sn',0,'num_rows');
	//var_dump($result);
	//exit();
	return $result;
}
    public function _get_admin_users($system_user_flag='1',$member_status='2',$select=0){ //列出管理者
    	if($select){
        $this->db->select($select);
      }
        $this->db->from('ij_member');
        //$this->db->join('ij_category','ij_category.category_sn=ij_category_spec_option_relation.category_sn');
        $this->db->where('system_user_flag',$system_user_flag);
        $this->db->where('member_status',$member_status);
        $this->db->order_by('member_sn');
        $array=$this->db->get()->result_array();
        return $array;
  	}
    public function _get_trans_log($trans_program_sn=0)
    {
    	if($trans_program_sn){
        $this->db->select('*');
        $this->db->from("ij_trans_log");
        $this->db->where('trans_program_sn',$trans_program_sn);
        $this->db->join('ij_member','ij_member.member_sn=ij_trans_log.trans_member_sn');
        $this->db->order_by('trans_date','asc');
        $array = $this->db->get()->result_array();
        //foreach($array as $key=>$_Item){
        //}
				return $array;
			}
  	}
    public function _get_wedding_websites($wedding_website_sn=0,$associated_order_sn=0,$if_ok_days=0)
    {
    	if($wedding_website_sn){
        $this->db->where('wedding_website_sn',$wedding_website_sn);
    	}
    	if($associated_order_sn){
        $this->db->where('associated_order_sn',$associated_order_sn);
    	}
        $this->db->select('*');
        $this->db->from("ij_wedding_website");
        $this->db->join('ij_member','ij_member.member_sn=ij_wedding_website.member_sn');
        $this->db->where('wedding_website_status','1');
        $this->db->order_by('ij_wedding_website.last_time_update','desc');
        $array = $this->db->get()->result_array();
				foreach($array as $key=>$_Item){
					//關聯經銷商短名
					$_where=" left join ij_dealer on ij_dealer.dealer_sn=ij_serial_card.stock_out_dealer_sn where used_member_sn='".$_Item['member_sn']."' ";
					$this->load->model('sean_models/Sean_db_tools');
					$_member_dealers=$this->Sean_db_tools->db_get_max_record("distinct dealer_short_name","ij_serial_card",$_where);
					$_member_dealer='';
					foreach($_member_dealers as $Item){
						$_member_dealer.=$Item->dealer_short_name.'、';
					}
					$array[$key]['member_dealer']=substr($_member_dealer, 0, -3);
					$array[$key]['sms_used_amount']=0; //簡訊已使用數量
				}
				if($if_ok_days){
					$this->load->library("ijw");
					return $this->ijw->date_diff_days($array[0]['website_end_date']);
				}else{
					return $array;
				}
  	}

    public function _get_blog_faq($table='ij_blog_article',$where=0,$like_field=0,$like_value=0,$_search_array=0,$offset = 0,$per_page = 0,$if_numrows=0)
    {
    	//$array=array();
    	if($where){
         $this->db->where($where);
    	}
    	if($like_field && $like_value){
            $this->db->like($like_field,$like_value);
    	}
        //var_dump($_search_array);
        if($_search_array){
            $this->db->group_start();
            $this->db->or_like($_search_array);
            $this->db->group_end();
        }
        $this->db->select('*');
        $this->db->from($table);
        $this->db->select($table.'.*,aaa.last_name');
        $this->db->join('ij_member as aaa','aaa.member_sn='.$table.'.update_member_sn','left');
        if($table=='ij_blog_article'){
            $this->db->join('ij_blog_relation','ij_blog_relation.blog_article_sn='.$table.'.blog_article_sn','left');
            $this->db->join("ij_blog_type_relation","ij_blog_type_relation.blog_article_sn=ij_blog_article.blog_article_sn","left");
            $this->db->group_by('ij_blog_article.blog_article_sn');
        }
        if($table=='ij_faq')
        $this->db->join('ij_content_type_config','ij_content_type_config.content_type_sn='.$table.'.associated_content_type_sn_set','left');
	    //$this->db->where('wedding_website_status','1');
        //加入供應商、經銷商機制
        if(@$this->session->userdata('member_role_system_rules')[0]['role_name']){
            $_associated_dealer_sn=@$this->session->userdata['member_data']['associated_dealer_sn'];
            $_associated_supplier_sn=@$this->session->userdata['member_data']['associated_supplier_sn'];
            switch(@$this->session->userdata('member_role_system_rules')[0]['role_name']){
            case '經銷商管理員':
                if($table=='ij_blog_article'){
                    $this->db->where($table.'.status','1');
                    $this->db->where(" ( $table.associated_dealer_sn = $_associated_dealer_sn OR ( $table.associated_supplier_sn = 0 AND $table.associated_dealer_sn = 0 ) )", NULL, FALSE);
                    //$this->db->not_like('associated_content_type_sn','網站最新消息');
                }else{
                    $this->db->like($table.'.post_channel_name_set','供應商');
                }
            break;
            case '供應商管理員':
                if($table=='ij_blog_article'){
                    $this->db->where($table.'.status','1');
                    $this->db->where(" ( $table.associated_supplier_sn = $_associated_supplier_sn OR ( $table.associated_supplier_sn = 0 AND $table.associated_dealer_sn = 0 ) )", NULL, FALSE);
                }else{
                    $this->db->like($table.'.post_channel_name_set','供應商');
                }
                //$this->db->not_like('associated_content_type_sn','網站最新消息');
            break;
            default:
                //$this->db->where('ij_blog_article.associated_supplier_sn',$this->session->userdata['member_data']['associated_supplier_sn']);
            break;
            }
        }
        $this->db->order_by($table.'.sort_order','asc');
        $this->db->order_by($table.'.last_time_update','desc');
        if($offset && $per_page){
            $this->db->limit($per_page,$offset);
        }elseif(!$offset && $per_page){
            $this->db->limit($per_page);
        }
        if($if_numrows){
        	return  $this->db->get()->num_rows();
        }else{
	        $array = $this->db->get()->result_array();
            //echo $this->db->last_query();
	        if($table=='ij_blog_article'){
		        $keyword_array=array();
				foreach($array as $_key=>$_Item){
                    $array[$_key]['type_names']=$this->get_content_type_string($_Item['blog_article_sn']);
					$keyword_array = array_merge($keyword_array,explode(chr(13).chr(10),trim($_Item['keyword'])));
                    if(!isset($_Item['relation_status']) && @$_associated_dealer_sn && ($_Item['associated_dealer_sn']=='0' || $_Item['associated_dealer_sn']==@$_associated_dealer_sn)){//新增不是該經銷商發表的文章預設開放
                        $this->_insert('ij_blog_relation',array('blog_article_sn'=>$_Item['blog_article_sn'],'dealer_sn'=>$_associated_dealer_sn,'relation_status'=>'1'));
                        $array[$_key]['relation_status']='1';
                    }else{
                        if(@$this->session->userdata('member_role_system_rules')[0]['role_name']){
                            switch(@$this->session->userdata('member_role_system_rules')[0]['role_name']){
                            case '經銷商管理員':
                                $array[$_key]['post_flag']=$this->libraries_model->_select('ij_blog_relation',array('blog_article_sn'=>$_Item['blog_article_sn'],'dealer_sn'=>@$this->session->userdata['member_data']['associated_dealer_sn']),0,0,0,'blog_article_sn',0,'row')['relation_status'];
                            break;
                            case '供應商管理員':
                            break;
                            default:
                            break;
                            }
                        }
                    }
                    if(strpos($_Item['blog_article_image_save_dir'],'blog')===false){
                        //var_dump($_Item['blog_article_image_save_dir']);
                        //$this->_update('ij_blog_article ',array('blog_article_image_save_dir'=>'blog/'.$_Item['blog_article_image_save_dir']),'blog_article_sn',$_Item['blog_article_sn']);
                    }
				}
				//var_dump(count($keyword_array));
				$keyword_array=array_unique($keyword_array);
				//var_dump(count($keyword_array));
				natcasesort($keyword_array);
				if($keyword_array){
					$array[0]['keywords']=$keyword_array;
				}
			}
			//var_dump($array);
			return $array;
		}
  	}

    public function _get_blog_faq_categorys($blog_faq_flag='1',$post_channel_name_set='囍市集',$if_dealer=0)
    {
		$where=array('status'=>'1','post_flag'=>'1');
        $this->db->select('*');
        $this->db->from('ij_content_type_config');
	    $this->db->where($where);
	    $this->db->where('blog_faq_flag',$blog_faq_flag);
	    $this->db->like('post_channel_name_set',$post_channel_name_set);
        $this->db->order_by('sort_order','asc');
        $array = $this->db->get()->result_array();
        if($blog_faq_flag=='1'){
	        if($blog_faq_flag=='1') $talbe='ij_blog_article';
	        if($blog_faq_flag=='2') $talbe='ij_faq';
            $where=array($talbe.'.status'=>'1',$talbe.'.post_flag'=>'1');
            if($if_dealer) $where['relation_status']='1';
			foreach($array as $key=>$_Item){
                //$array[$key]['numrows']=$this->_get_blog_faq($talbe,$where,'associated_content_type_sn',$_Item['content_title'],'0',0,0,1);
                $array[$key]['numrows']=$this->get_content_type_string(0,1,$_Item['content_type_sn']);
			}
		}else{
			foreach($array as $key=>$_Item){
				$array[$key]['faqs']=$this->_get_blog_faq('ij_faq',array('ij_faq.post_flag'=>'1','associated_content_type_sn_set'=>$_Item['content_type_sn']));
			}
		}
		return $array;
  	}

    public function _get_wedding_serials($serial_card_sn=0)
    {
        $this->db->select('ij_serial_card.*,ij_dealer.dealer_short_name,ij_member.last_name,ij_member.first_name,serial_card_status_name,aaa.last_name as update_member_sn,product_name,product_package_name,color_name');
        $this->db->from("ij_serial_card");
        $this->db->join('ij_member','ij_member.member_sn=ij_serial_card.used_member_sn','left');
        $this->db->join('ij_dealer','ij_dealer.dealer_sn=ij_serial_card.stock_out_dealer_sn','left');
        $this->db->join('ij_serial_card_status_ct','ij_serial_card_status_ct.serial_card_status=ij_serial_card.serial_card_status','left');
        $this->db->join('ij_member as aaa','aaa.member_sn=ij_serial_card.update_member_sn','left');
        $this->db->join('ij_product','ij_product.product_sn=ij_serial_card.associated_product_sn','left');
        $this->db->join('ij_product_package_config','ij_product_package_config.product_package_config_sn=ij_serial_card.associated_product_package_config_sn','left');
        $this->db->join('ij_mutispec_stock_log','ij_mutispec_stock_log.mutispec_stock_sn=ij_serial_card.associated_product_package_config_sn','left');
	    	if($serial_card_sn){
	        $this->db->where('serial_card_sn',$serial_card_sn);
	    	}
        //$this->db->where('associated_order_sn !=','');
        $this->db->order_by('serial_card_check_code','asc');
        $array = $this->db->get()->result_array();
				//foreach($array as $key=>$_Item){
					//關聯經銷商短名
					//$_where=" left join ij_dealer on ij_dealer.dealer_sn=ij_member_dealer_relation.dealer_sn where member_sn='".$_Item['member_sn']."' ";
					//$this->load->model('sean_models/Sean_db_tools');
					//$_member_dealers=$this->Sean_db_tools->db_get_max_record("dealer_short_name","ij_member_dealer_relation",$_where);
					//$_member_dealer='';
					//foreach($_member_dealers as $Item){
					//	$_member_dealer.=$Item->dealer_short_name.'、';
					//}
					//$array[$key]['member_dealer']=substr($_member_dealer, 0, -3);
					//$array[$key]['sms_used_amount']=0; //簡訊已使用數量
				//}
				return $array;
  	}
  public function _save_order($order=0,$order_item=0,$coupon_code_cdiscount_amount=0){
	    if($order){
	    	//主訂單
	    	$member=$this->get_member($order['member_sn']);
				if($member){
					$order['total_shipping_charge_amount']=0;
					$order['gift_cash_total_discount_amount']=0;
					$order['buyer_addr_country']='中華民國';
					$order['buyer_addr_state']='台灣';
					$order['receiver_addr_country']='中華民國';
					$order['receiver_addr_state']='台灣';
					$order['coupon_code_total_cdiscount_amount']=$coupon_code_cdiscount_amount; //折扣金額（升級
					//縣市鄉鎮區改直接抓值寫入訂單
					$order['buyer_addr_city']=$this->_select('ij_city_ct','city_code',$member['addr_city_code'],0,0,0,0,'row','city_name')['city_name'];
					$order['buyer_addr_town']=$this->_select('ij_town_ct','town_code',$member['addr_town_code'],0,0,0,0,'row','town_name')['town_name'];
					$order['receiver_addr_city']=$this->_select('ij_city_ct','city_code',$member['addr_city_code'],0,0,0,0,'row','city_name')['city_name'];
					$order['receiver_addr_town']=$this->_select('ij_town_ct','town_code',$member['addr_town_code'],0,0,0,0,'row','town_name')['town_name'];
					$order['buyer_last_name']=$member['last_name'];
					$order['buyer_first_name']=$member['first_name'];
					$order['buyer_cell']=$member['cell'];
					$order['buyer_email']=$member['email'];
					$order['buyer_tel_area_code']=$member['tel_area_code'];
					$order['buyer_tel']=$member['tel'];
					$order['buyer_tel_ext']=$member['tel_ext'];
					$order['buyer_zipcode']=$member['zipcode'];
					$order['buyer_addr1']=$member['addr1'];
					$order['receiver_last_name']=$member['last_name'].$member['first_name'];
					$order['receiver_cell']=$member['cell'];
					$order['receiver_tel_area_code']=$member['tel_area_code'];
					$order['receiver_tel']=$member['tel'];
					$order['receiver_tel_ext']=$member['tel_ext'];
					$order['receiver_zipcode']=$member['zipcode'];
					$order['receiver_addr1']=$member['addr1'];
					if(!$order['payment_status']){
						$order['payment_status']='1';//未付款
					}
					$order = $this->libraries_model->CheckUpdate($order);
				  $order['order_create_date']=date("Y-m-d H:i:s",time());
					$order_sn=$this->_insert('ij_order',$order);
				}else{
					return false;
				}
				//子訂單
				$sub_order['order_sn']=$order_sn;
				$sub_order['order_process_member_sn']=$this->session->userdata['member_data']['member_sn'];
				$sub_order['delivery_status']='1';
				$sub_order['sub_order_status']='1';
				$sub_order['free_shipping_flag']='1';
				$sub_order['channel_name']='婚禮網站';
				$sub_order['associated_member_sn']=$order['member_sn'];
				$delivery_method=$this->_select('ij_delivery_method_ct','delivery_method_name','線上開通',0,0,0,0,'row','delivery_method')['delivery_method'];
				$sub_order['product_type']='2';
				$sub_order['delivery_method']=$delivery_method;
				$sub_order = $this->libraries_model->CheckUpdate($sub_order);
				$sub_order_sn=$this->_insert('ij_sub_order',$sub_order);
				//訂單細項
				$order_item['coupon_code_cdiscount_amount']=$coupon_code_cdiscount_amount;
				$order_item['gift_cash_discount_amount']=0;
				$order_item['addon_flag']=0;
				$order_item['associated_order_item_sn']=0;
				$order_item['currency_code']=1;
				$order_item['sub_order_sn']=$sub_order_sn;
				$this->load->model('Shop_model');
				if($sharing_percentage=$this->Shop_model->_get_profit_sharing_rate($order_item['product_sn'])){
					$order_item['sharing_percentage']=$sharing_percentage;
					$order_item['sharing_amount']=round($sharing_percentage*$order_item['sales_price']);
				}
				$order_item = $this->libraries_model->CheckUpdate($order_item);
				$order_item_sn=$this->_insert('ij_order_item',$order_item);
				return $order_sn;
	    }else{
				return false;
	    }
  }
	  public function get_member($member_sn,$select='*')
	  {
	    $this->db->select($select);
	    $this->db->from('ij_member');
	    $this->db->where('member_sn',$member_sn);
	    $_result=$this->db->get()->row_array();
	    if($_result) {
	    	return $_result;
	    }else{
	    	return array();;
	    }
	  }
	  public function get_member_by_email($email,$select=0)
	  {
	    if($select) $this->db->select($select);
	    $this->db->from('ij_member');
	    $this->db->where('email',$email);
	    $_result=$this->db->get()->row_array();
	    if($_result) {
	    	return $_result;
	    }else{
	    	return array();
	    }
	  }
      public function get_member_by_username($user_name,$select=0)
      {
        if($select) $this->db->select($select);
        $this->db->from('ij_member');
        $this->db->where('user_name',$user_name);
        $_result=$this->db->get()->row_array();
        if($_result) {
            return $_result;
        }else{
            return array();
        }
      }
      public function get_member_by_partner_sn($partner_sn,$select=0,$yearmonth=0)
      {
        if($select) $this->db->select($select);
        if($yearmonth) $this->db->like('registration_date',$yearmonth);
        $this->db->from('ij_member');
        $this->db->where('associated_partner_sn',$partner_sn);
        $this->db->where('member_status',2);
        $_result=$this->db->get()->row_array();
        if($_result) {
            return $_result;
        }else{
            return array();
        }
      }
      public function get_hits_by_partner_sn($partner_sn,$select=0,$yearmonth=0)
      {
        if($select) $this->db->select($select);
        if($yearmonth) $this->db->like('view_date',$yearmonth);
        $this->db->from('ij_partner_hit_log');
        $this->db->where('partner_sn',$partner_sn);
        $_result=$this->db->get()->row_array();
        if($_result) {
            return $_result;
        }else{
            return array();
        }
      }
	  public function get_member_by_where_in($_field,$_where_in_array,$_select=0,$_where=0)
	  {
	    if($_select) $this->db->select($_select);
	    $this->db->from('ij_member');
	    $this->db->where_in($_field,$_where_in_array);
	    if($_where){
	    	$this->db->where($_where);
	    }
	    $_result=$this->db->get()->result_array();
	    if($_result) {
	    	return $_result;
	    }else{
	    	return array();
	    }
	  }
	  public function get_dealer_members($where_in_array,$select='*',$like_field=0,$like_value=0,$_where=0)
	  {
	    $this->db->select($select);
	    $this->db->from('ij_member');
      $this->db->join('ij_member_dealer_relation','ij_member_dealer_relation.member_sn=ij_member.member_sn','left');
	    if($where_in_array && $like_field){
	    	//$this->db->where($where);
	    	$this->db->where_in($like_field,$where_in_array);
	    }
	    if($_where){
	    	$this->db->where($_where);
	    }
    	if($like_field && $like_value){
         $this->db->like($like_field,$like_value);
    	}
      $this->db->group_by('member_sn');
	    $_result=$this->db->get()->result_array();
	    if($_result) {
	    	return $_result;
	    }else{
	    	return array();
	    }
	  }
 	  public function get_Package($Packages,$product_package_config_sn){
			if($Packages){
				foreach($Packages as $Item){
					$tempAtt=' promo_price="'.$Item['promo_price'].'"';
						//var_dump($Item);
					foreach($Item['specs'] as $Item2){
							if($Item2['spec_option_name']=='婚紗照片'){
								$tempAtt.=' upload_pic_total_amount="'.$Item2['spec_option_limitation'].'"';
							}elseif($Item2['spec_option_name']=='精彩影音-相片分享'){
								$tempAtt.=' album_pic_total_amount="'.$Item2['spec_option_limitation'].'"';
							}elseif($Item2['spec_option_name']=='精彩影音-影片分享'){
								$tempAtt.=' album_video_total_amount="'.$Item2['spec_option_limitation'].'"';
							}elseif($Item2['spec_option_name']=='密碼保護'){
								$tempAtt.=' password_flag="'.$Item2['spec_option_limitation'].'"';
							}elseif($Item2['spec_option_name']=='簡訊提醒'){
								$tempAtt.=' sms_total_amount="'.$Item2['spec_option_limitation'].'"';
							}elseif($Item2['spec_option_name']=='使用期限'){
								$tempAtt.=' website_usable_duration="'.$Item2['spec_option_limitation'].'"';
							}
					}
						$_result.=  ' <option '.$tempAtt.' value="'.$Item['product_package_config_sn'].'">'.$Item['product_package_name'].'</option>';
					//}
				}
			}
 	  }
    public function _get_adsSlots($banner_location_sn=0,$published_locaiotn_type=0)
    {
        $this->db->select('ij_banner_location.*,published_locaiotn_type_name,banner_type_name,banner_type_eng_name');
        $this->db->from("ij_banner_location");
        $this->db->join('ij_banner_type_ct','ij_banner_location.banner_type=ij_banner_type_ct.banner_type','left');
        //$this->db->join('ij_banner_link_type_ct','ij_banner_link_type_ct.banner_link_type=ij_banner_location.banner_link_type','left');
        $this->db->join('ij_published_locaiotn_type_ct','ij_published_locaiotn_type_ct.published_locaiotn_type=ij_banner_location.published_locaiotn_type','left');
	      $this->db->where('status','1');
	    	if($banner_location_sn){
	        $this->db->where('banner_location_sn',$banner_location_sn);
	        return $this->db->get()->row_array();
	    	}
	    	if($published_locaiotn_type){
	        $this->db->where('ij_banner_location.published_locaiotn_type',$published_locaiotn_type);
	    	}
        //$this->db->where('associated_order_sn !=','');
        $this->db->order_by('last_time_update','desc');
        $array = $this->db->get()->result_array();
				//foreach($array as $key=>$_Item){
				//}
				return $array;
  	}
    public function _get_adsMaterials($banner_content_sn=0,$where=array())
    {
		$this->db->select('ij_banner_content.*,banner_location_name,published_locaiotn_type_name,banner_type_name,banner_type_eng_name,banner_customer_name');
        $this->db->from("ij_banner_content");
		$this->db->join('ij_banner_location','ij_banner_location.banner_location_sn=ij_banner_content.banner_location_sn','left');
		$this->db->join('ij_banner_type_ct','ij_banner_content.banner_type=ij_banner_type_ct.banner_type','left');
		$this->db->join('ij_published_locaiotn_type_ct','ij_published_locaiotn_type_ct.published_locaiotn_type=ij_banner_location.published_locaiotn_type','left');
        $this->db->join('ij_banner_customer','ij_banner_customer.banner_customer_sn=ij_banner_content.banner_customer_sn','left');
			//$this->db->where('ij_banner_content.status <=','1');
    	if($where){
           $this->db->where($where);
    	}
    	if($banner_content_sn){
           $this->db->where('banner_content_sn',$banner_content_sn);
           return $this->db->get()->row_array();
    	}
        $this->db->order_by('sort_order','asc');
        $this->db->order_by('last_time_update','desc');
        $array = $this->db->get()->result_array();
		return $array;
  	}
    public function _get_adsCustomers($banner_customer_sn=0,$where=array())
    {
		$this->db->select("ij_banner_customer.*,dealer_short_name,supplier_name,concat(member1.last_name,member1.first_name) as sale_name,concat(member2.last_name,member2.first_name) as agent_name",false);
            $this->db->from("ij_banner_customer");
				$this->db->join('ij_dealer','ij_banner_customer.associated_dealer_sn=ij_dealer.dealer_sn','left');
				$this->db->join('ij_supplier','ij_banner_customer.associated_supplier_sn=ij_supplier.supplier_sn','left');
				$this->db->join('ij_member as member1','ij_banner_customer.associated_sales_member_sn=member1.member_sn','left');
        $this->db->join('ij_member as member2','ij_banner_customer.associated_ccagent_member_sn=member2.member_sn','left');
				$this->db->where('ij_banner_customer.status <=','1');
	    	if($where){
	        $this->db->where($where);
	        //$this->db->or_where("ij_banner_customer.partnership_start_date ='' or ij_banner_customer.partnership_end_date =''");
	    	}
	    	if($banner_customer_sn){
	        $this->db->where('banner_customer_sn',$banner_customer_sn);
	        return $this->db->get()->row_array();
	    	}
        $this->db->order_by('last_time_update','desc');
        $array = $this->db->get()->result_array();
		//echo $this->db->last_query();
				//foreach($array as $key=>$_Item){
				//}
				return $array;
  	}
    public function _get_adsSchedules($banner_schedule_sn=0,$_where=array())
    {
        $this->db->select('ij_banner_schedule_log.*,banner_content_name,banner_link_type_eng_name,banner_link_type_name');
        $this->db->from("ij_banner_schedule_log");
        $this->db->join('ij_banner_content','ij_banner_schedule_log.banner_content_sn=ij_banner_content.banner_content_sn','left');
        $this->db->join('ij_banner_link_type_ct','ij_banner_schedule_log.banner_link_type=ij_banner_link_type_ct.banner_link_type','left');
	      $this->db->where('ij_banner_schedule_log.status <=','1');
	    	if($_where){
	        $this->db->where($_where);
	    	}
	    	if($banner_schedule_sn){
	        $this->db->where('banner_schedule_sn',$banner_schedule_sn);
	        return $this->db->get()->row_array();
	    	}
        $this->db->order_by('sort_order','asc');
        $this->db->order_by('last_time_update','desc');
        $array = $this->db->get()->result_array();
		//echo $this->db->last_query();
				//foreach($array as $key=>$_Item){
				//}
				return $array;
  	}
    public function _get_ads_content_Schedules($_where=array())
    {
        $this->db->select('ij_banner_schedule_log.*,banner_content_name');
        $this->db->from("ij_banner_schedule_log");
        $this->db->join('ij_banner_content','ij_banner_schedule_log.banner_content_sn=ij_banner_content.banner_content_sn','left');
        //$this->db->join('ij_banner_link_type_ct','ij_banner_schedule_log.banner_link_type=ij_banner_link_type_ct.banner_link_type','left');
	      $this->db->where('ij_banner_schedule_log.status <=','1');
	    	if($_where){
	        $this->db->where($_where);
	    	}

        $this->db->order_by('sort_order','asc');
        $this->db->order_by('last_time_update','desc');
        $array = $this->db->get()->result_array();
		//echo $this->db->last_query();
		//var_dump($array);
		//exit();
				if($array){
					foreach($array as $key=>$_Item){
						$adsMaterials=$this->_get_adsMaterials($_Item['banner_content_sn']);
						$array[$key]['banner_customer_name']=$adsMaterials['banner_customer_name'];
						$array[$key]['published_locaiotn_type_name']=$adsMaterials['published_locaiotn_type_name'];
						$array[$key]['banner_location_name']=$adsMaterials['banner_location_name'];
					}
				}
				return $array;
  	}
    public function get_edm_send_config($edm_id=0,$edm_send_sn=0)
    {
        //$this->db->select('ij_edm_send_list_log.*');
        $this->db->from("ij_edm_send_config");
        //$this->db->join('ij_edm_send_config','ij_edm_send_config.edm_send_sn=ij_edm_send_list_log.edm_send_id','left');
	    	if($edm_send_sn){
	        $this->db->where('edm_send_sn',$edm_send_sn);
	    	}
	    	if($edm_id){
	        $this->db->where('edm_id',$edm_id);
	    	}
 	      $this->db->where('status','1');
        $this->db->order_by('last_time_update','desc');
        $array = $this->db->get()->result_array();
				//echo $this->db->last_query();
				//var_dump($array);
				//exit();
				if($array){
					foreach($array as $_key=>$_Item){
						if($_Item['send_member_level_type']=='9999'){
							$array[$_key]['send_config']='會員Email';
						}elseif($_Item['send_product_set']){
							$array[$_key]['send_config']='商品編號：'.$_Item['send_product_set'].' '.$_Item['order_start_date'].'～'.$_Item['order_end_date'];
						}elseif($_Item['over_order_amount']){
							$array[$_key]['send_config']='訂單金額超過：'.$_Item['over_order_amount'].' '.$_Item['order_start_date'].'～'.$_Item['order_end_date'];
						}elseif($_Item['send_dealer_set']){
							$array[$_key]['send_config']='經銷商編號：'.$_Item['send_dealer_set'];
						}elseif($_Item['send_biz_domain_set']){
							$array[$_key]['send_config']='產業屬性：'.$_Item['send_biz_domain_set'];
						}elseif($_Item['wedding_year_month']){
							$array[$_key]['send_config']='結婚月份：'.$_Item['wedding_year_month'];
						}elseif($_Item['wedding_start_date']!='0000-00-00' || $_Item['wedding_end_date']!='0000-00-00'){
							$array[$_key]['send_config']='結婚日期： '.$_Item['wedding_start_date'].'～'.$_Item['wedding_end_date'];
						}
						//已開信數
						$array[$_key]['edm_open_amount']=$this->get_edm_send_list($_Item['edm_send_sn'],$_Item['edm_id'],1);
					}
					return $array;
				}else{
					return array();
				}
  	}
    public function get_edm_send_list($edm_send_id=0,$edm_id=0,$if_edm_open_flag=0,$edm_send_list_sn=0,$receiver_email=0,$if_member_array=0)
    {
        //$this->db->select('ij_edm_send_list_log.*');
        $this->db->from("ij_edm_send_list_log");
        $this->db->join('ij_edm_send_config','ij_edm_send_config.edm_send_sn=ij_edm_send_list_log.edm_send_id','left');
	    	if($edm_send_id){
	        $this->db->where('edm_send_id',$edm_send_id);
	    	}
	    	if($edm_id){
	        $this->db->where('edm_id',$edm_id);
	    	}
        $this->db->order_by('ij_edm_send_list_log.send_time','desc');
        $this->db->order_by('edm_open_flag','asc');
        if($if_edm_open_flag){
	        $this->db->where('edm_open_flag','1');
			return $this->db->get()->num_rows();
        }elseif($edm_send_list_sn){
	        $this->db->where('edm_send_list_sn',$edm_send_list_sn);
			return $this->db->get()->row_array();
        }elseif($receiver_email){
	        $this->db->where('receiver_email',$receiver_email);
	        $this->db->where('ij_edm_send_list_log.send_time !=','0000-00-00 00:00:00');
			return $this->db->get()->num_rows();
        }elseif($if_member_array){
        	//配合其他條件輸出方式
        	$array = $this->db->get()->result_array();
					if($array){
						foreach($array as $_key=>$_Item){
							$_member_array[$_key]['member_sn']=$_Item['associated_member_sn'];
							$_member_array[$_key]['last_name']=$_Item['receiver_last_name'];
							$_member_array[$_key]['first_name']=$_Item['receiver_first_name'];
							$_member_array[$_key]['accept_edm_flag']='1';
							$_member_array[$_key]['email']=$_Item['receiver_email'];
						}
						return $_member_array;
					}else{
						return array();
					}
      	}else{
        	$array = $this->db->get()->result_array();
            //echo $this->db->last_query();
            //var_dump($array);
			return $array;
      	}
				//var_dump($array);
				//exit();
				/*if($array){
					foreach($array as $key=>$_Item){
						$adsMaterials=$this->_get_adsMaterials($_Item['banner_content_sn']);
						$array[$key]['banner_customer_name']=$adsMaterials['banner_customer_name'];
						$array[$key]['published_locaiotn_type_name']=$adsMaterials['published_locaiotn_type_name'];
						$array[$key]['banner_location_name']=$adsMaterials['banner_location_name'];
					}
				}*/
  	}
    public function get_edm_send_config_list_send($_array,$ifsend=0){
    	//var_dump($_array);
    	if(is_array($_array)){
				if($_array['send_member_level_type']=='9999'){
					$_member_array=$this->get_edm_send_list($_array['edm_send_sn'],0,0,0,0,1);
				}elseif($_array['send_product_set']){
			    $_where['product_sn']=$_array['send_product_set'];
			    $_where['create_date <=']=$_array['order_end_date'];
					$_where['create_date >=']=$_array['order_start_date'];
					$_member_array=$this->_get_order_member($_where);
					//$array[$_key]['send_config']='商品：'.$_array['send_product_set'].' '.$_array['order_start_date'].'～'.$_array['order_end_date'];
				}elseif($_array['over_order_amount']){
			    $_where['total_order_amount >']=$_array['over_order_amount'];
			    $_where['create_date <=']=$_array['order_end_date'];
					$_where['create_date >=']=$_array['order_start_date'];
					$_member_array=$this->_get_order_member($_where);
					//$array[$_key]['send_config']='訂單金額超過：'.$_array['over_order_amount'].' '.$_array['order_start_date'].'～'.$_array['order_end_date'];
				}elseif($_array['send_dealer_set']){
					$dealer_array=explode(',',$_array['send_dealer_set']);
					$_member_array=$this->get_dealer_members($dealer_array,'ij_member.member_sn,last_name,first_name,accept_edm_flag,email','dealer_sn');
		//echo $this->db->last_query();
					//var_dump($dealer_array);
					//$array[$_key]['send_config']='經銷商：'.$_array['send_dealer_set'];
				}elseif($_array['send_biz_domain_set']){
			    $_where=explode(",",$_array['send_biz_domain_set']);
					$_member_array=$this->_get_dealer_member_by_domain($_where);
					//$array[$_key]['send_config']='產業屬性：'.$_array['send_biz_domain_set'];
				}elseif($_array['wedding_year_month']){
					$_member_array=$this->get_dealer_members(0,'ij_member.member_sn,last_name,first_name,accept_edm_flag,email','wedding_date',$_array['wedding_year_month']);
					//$array[$_key]['send_config']='結婚月份：'.$_array['wedding_year_month'];
				}elseif($_array['wedding_start_date']!='0000-00-00' || $_array['wedding_end_date']!='0000-00-00'){
			    $_where['wedding_date <=']=$_array['wedding_end_date'];
					$_where['wedding_date >=']=$_array['wedding_start_date'];
					$_member_array=$this->get_dealer_members(0,'ij_member.member_sn,last_name,first_name,accept_edm_flag,email',0,0,$_where);
					//$array[$_key]['send_config']='結婚日期： '.$_array['wedding_start_date'].'～'.$_array['wedding_end_date'];
				}
				if($_member_array){
					$_i=0;
					$edm=$this->_select('ij_edm',array('edm_sn'=>$_array['edm_id']),0,0,0,'edm_sn',0,'row','edm_titile,edm_content');
					if($ifsend){
    					//寄送先清空該條件的寄送清單（尚未寄送的）? why....
    				    $this->_delete('ij_edm_send_list_log',array('edm_send_id' => $_array['edm_send_sn'],'send_time'=>'0000-00-00 00:00:00'));
					}
					$edm_send_list=$this->get_edm_send_list($_array['edm_send_sn'],$_array['edm_id']);
					$this->load->library("ijw");
					foreach($_member_array as $_key=>$_Item){
					  if($_Item['accept_edm_flag'] || $_array['skip_cancel_edm_flag']){
    		        	$send_list_log = array(
        		            'edm_send_id'=> $_array['edm_send_sn'],
        		            'associated_member_sn'=> $_Item['member_sn'],
        		            'receiver_email'=> $_Item['email'],
        		            'receiver_last_name'=> $_Item['last_name'],
        		            'receiver_first_name'=> $_Item['first_name']
    		        	);
    					//同edm_sn、email 不再寄送
    					//var_dump($this->get_edm_send_list(0,$_array['edm_id'],0,0,$_Item['email']));
                        //var_dump($_member_array);
                        //exit;
    					if($this->get_edm_send_list(0,$_array['edm_id'],0,0,$_Item['email']) <= 0){

    		        		//沒有清單或寄送才新增
    			        	if(!$edm_send_list){
    							$edm_send_list_sn=$this->libraries_model->_insert('ij_edm_send_list_log ',$send_list_log);
    						}

    						if($ifsend){
    							$_header_array=array('edm_send_list_id'=>base64_encode($edm_send_list_sn),'edm_id'=>base64_encode($_array['edm_id']));
    							if($this->ijw->send_edm($_Item['first_name'],$_Item['email'],$edm['edm_titile'],$edm['edm_content'],$_header_array)){
    								$this->_update('ij_edm_send_list_log ',array('send_time'=>date('Y-m-d H:i:s')),'edm_send_list_sn',$edm_send_list_sn);
    								$_i++;
    							}
    						}else{
    								$_i++;
    						}
    					}else{
                            //echo $this->db->last_query();exit;
    						//var_dump($this->get_edm_send_list(0,$_array['edm_id'],0,0,$_Item['email']));
    					}
		              }
		            }

		        if($_i){
						if($ifsend){
							$this->libraries_model->_update('ij_edm_send_config ',array('send_time'=>date('Y-m-d H:i:s')),'edm_send_sn',$_array['edm_send_sn']);
							$this->libraries_model->_update('ij_edm ',array('last_time_send'=>date('Y-m-d H:i:s')),'edm_sn',$_array['edm_id']);
						}
						//$this->_update_field_amount('ij_edm_send_config','edm_send_sn',$_array['edm_send_sn'],'send_amount','+'.$_i);
						$this->libraries_model->_update('ij_edm_send_config ',array('send_amount'=>$_i),'edm_send_sn',$_array['edm_send_sn']);
						$this->_update_field_amount('ij_edm','edm_sn',$_array['edm_id'],'send_total_amount','+'.$_i);
    				return $_i;
    			}else{
    				return false;
    			}
    		}else{
    			return false;
				}
    	}else{
    		return false;
    	}
    }
	private function _get_order_member($_where){
      $this->db->select('ij_member.member_sn,last_name,first_name,accept_edm_flag,email');
      $this->db->from("ij_order_item");
      $this->db->join("ij_sub_order","ij_sub_order.sub_order_sn=ij_order_item.sub_order_sn","left");
      $this->db->join("ij_order","ij_order.order_sn=ij_sub_order.order_sn","left");
      $this->db->join("ij_member","ij_sub_order.associated_member_sn=ij_member.member_sn","left");
      $this->db->where($_where);
      $this->db->group_by('member_sn');
		  return $this->db->get()->result_array();
	}
	private function _get_dealer_member_by_domain($_where){
      $this->db->select('ij_member.member_sn,last_name,first_name,accept_edm_flag,email');
      $this->db->from("ij_member");
      //$this->db->join("ij_member","ij_dealer.dealer_sn=ij_member.associated_dealer_sn","left");
      $this->db->join('ij_member_dealer_relation','ij_member_dealer_relation.member_sn=ij_member.member_sn','left');
      $this->db->join("ij_member_domain_relation","ij_member_domain_relation.associated_dealer_sn=ij_member_dealer_relation.dealer_sn","left");
      $this->db->join("ij_biz_domain","ij_member_domain_relation.domain_sn=ij_biz_domain.domain_sn","left");
      $this->db->where_in('domain_name',$_where);
      $this->db->group_by('member_sn');
		  return $this->db->get()->result_array();
		}
	public function get_coupon_codes($_where){
      $this->db->select('ij_coupon_code.*,ij_member.last_name,ij_member.first_name,dealer_short_name,aaa.last_name as update_member_sn');
      $this->db->from("ij_coupon_code");
      $this->db->join("ij_coupon_code_status_ct","ij_coupon_code_status_ct.coupon_code_status=ij_coupon_code.coupon_code_status","left");
      $this->db->join("ij_member","ij_member.member_sn=ij_coupon_code.used_member_sn","left");
      $this->db->join("ij_dealer","ij_dealer.dealer_sn=ij_coupon_code.used_dealer_sn","left");
	    $this->db->join('ij_member as aaa','aaa.member_sn=ij_coupon_code.update_member_sn','left');

      $this->db->where($_where);

		  return $this->db->get()->result_array();
		}
	public function sum_weight_by_suborder($sub_order_sn){
      $this->db->select('sum(weight*buy_amount) as total_weight');
      $this->db->from("ij_order_item");
      $this->db->where('sub_order_sn',$sub_order_sn);
      $total_weight= $this->db->get()->row_array()['total_weight'];
			$this->libraries_model->_update('ij_sub_order',array('total_weight'=>$total_weight),'sub_order_sn',$sub_order_sn);
			return $total_weight;
	}

    public function programTree($sys_program_config_sn=0,$spacing = '├─', $program_tree_array = '',$spacing2='│&nbsp;',$_level=0,$if_status=0,$just_second=0)
    {
		//NodeId為Parent的點
		    if (!is_array($program_tree_array))
		        $program_tree_array = array();
	    //取得該點的下一層
        $this->db->select('sys_program_config_sn,sys_program_name,upper_sys_program_config_sn,sort_order,sys_program_eng_name,status,system_rule_set');
        $this->db->from("ij_sys_program_config");
				if($just_second){
        	$this->db->where('upper_sys_program_config_sn !=',$sys_program_config_sn);
				}else{
        	$this->db->where('upper_sys_program_config_sn',$sys_program_config_sn);
        }
        if($if_status){
        	$this->db->where('status','1');
        }else{
        	$this->db->where('status <=','1');
        }
        //$this->db->order_by('upper_sys_program_config_sn');
        $this->db->order_by('sys_program_eng_name');
				$query =$this->db->get()->result_array();
		//echo $this->db->last_query().'<br>';
	    if($query) {
			//如果有下一層資料，逐一取出
        	$_level++;
        foreach($query as $key => $intro){
        	//echo $spacing . $intro['category_name'].'<br>';
        	$system_rule_set=implode('、',$this->get_system_rule($intro['system_rule_set'],1));
        	$down_counter=$this->libraries_model->_select('ij_sys_program_config','upper_sys_program_config_sn',$intro['sys_program_config_sn'],0,0,0,0,'num_rows');
          $program_tree_array[] = array("sys_program_config_sn" => $intro['sys_program_config_sn'], "sys_program_name" => $spacing . $intro['sys_program_name'], "upper_sys_program_config_sn" => $intro['upper_sys_program_config_sn'], "sort_order" => $intro['system_rule_set'], "sys_program_eng_name" => $intro['sys_program_eng_name'], "status" => $intro['status'], "pure_sys_program_name" => $intro['sys_program_name'], "system_rule_set" => $system_rule_set, "level" => $_level, "down_counter" => $down_counter);
          $program_tree_array = $this->programTree($intro['sys_program_config_sn'], $spacing2.$spacing , $program_tree_array,$spacing2,$_level,$if_status);
        	//var_dump($category_tree_array);
				}
	    }
		    return $program_tree_array;
    }
    public function get_system_rule($system_rule_set=0,$just_select=0,$array4=0,$if_english=0){
        $this->db->select('system_rule_type,system_rule_name,system_rule_eng_name');
        $this->db->from('ij_system_rule_ct');
        $this->db->order_by('system_rule_type');
        //$this->db->where('status',1);
        $array2 = $this->db->get()->result_array();
	    	if(is_array($array4)){
	    		$array2=$array4;
					//var_dump($array2);
	      }
				$array3=array();
				if($system_rule_set){
					$array1=$this->bindecValues($system_rule_set);
				  foreach($array2 as $key2=>$value2) {
					  	foreach($array1 as $value1) {
						   if($value1 == $value2['system_rule_type']){
					      	 $array2[$key2]['status']='1';
					      	 if($if_english){
					      	 		$array3[]=$value2['system_rule_eng_name'];
					      	 }else{
					      	 		$array3[]=$value2['system_rule_name'];
					      	 }
						    }
						  }
				  }
				}
				//var_dump($array1);
				//exit();
				if($just_select=='1'){
					return $array3;
				//}elseif($just_select=='2'){
				//	return $array4;
				}else{
					return $array2;
				}
    }

		function bindecValues($decimal,$return_array=1, $reverse=false, $inverse=false) {
		/*
		1. This function takes a decimal, converts it to binary and returns the
		     decimal values of each individual binary value (a 1) in the binary string.
		     You can use larger decimal values if you pass them to the function as a string!
		2. The second optional parameter reverses the output.
		3. The third optional parameter inverses the binary string, eg 101 becomes 010.
		-- darkshad3 at yahoo dot com
		*/

		    $bin = decbin($decimal);
		    if ($inverse) {
		        $bin = str_replace("0", "x", $bin);
		        $bin = str_replace("1", "0", $bin);
		        $bin = str_replace("x", "1", $bin);
		    }
		    $total = strlen($bin);

		    $stock = array();

		    for ($i = 0; $i < $total; $i++) {
		        if ($bin{$i} != 0) {
		            $bin_2 = str_pad($bin{$i}, $total - $i, 0);
		            array_push($stock, bindec($bin_2));
		        }
		    }

		    $reverse ? rsort($stock):sort($stock);
		    if($return_array){
		    	return $stock;
		    }else{
		    	return implode(", ", $stock);
		    }
		}
		function get_clean_Array($_Array,$_count=3){
	    	if(is_array($_Array)){
				  foreach($_Array as $_key=>$_value) {
				  	if(count($_value)<$_count){
				  		unset($_Array[$_key]);
				  	}else{
				  		$_Array[$_key]['status']=0;
				  	}
				  }
				  return $_Array;
	    	}else{
	    		return array();
	      }
		}
		function get_status1_Array($_Array,$_status=1){
	    	if(is_array($_Array)){
				  foreach($_Array as $_key=>$_value) {
				  	if(@$_value['status']!=$_status){
				  		unset($_Array[$_key]);
				  	}
				  }
				  return $_Array;
	    	}else{
	    		return array();
	      }
		}
		public function get_system_roles($sys_program_config_sn,$array=array(),$if_no_super=1){
      $this->db->select('role_sys_program_relation_sn,ij_role.role_sn,role_name,ij_role_sys_program_relation.sort_order as system_rule_set');
      $this->db->from("ij_role_sys_program_relation");
      $this->db->join("ij_role","ij_role_sys_program_relation.role_sn=ij_role.role_sn","left");
      $this->db->where('sys_program_config_sn',$sys_program_config_sn);
      $this->db->where('ij_role.status','1');
      if($if_no_super){
      	$this->db->where('ij_role.role_sn >','1');
      }
		  $array2= $this->db->get()->result_array();
		//echo $this->db->last_query();
			if($array2){
			  foreach($array2 as $key2=>$value2) {
					//$array1=$this->bindecValues($value2['system_rule_set']);
        	$array2[$key2]['system_rule']=$this->get_system_rule($value2['system_rule_set'],0,$this->get_clean_Array($array));
			  }
			}
			return  $array2;
		}
		public function get_system_programs($role_sn){
      /*$this->db->select('role_sys_program_relation_sn,ij_sys_program_config.sys_program_config_sn,sys_program_name,ij_role_sys_program_relation.sort_order as system_rule_set');
      $this->db->from("ij_role_sys_program_relation");
      $this->db->join("ij_sys_program_config","ij_role_sys_program_relation.sys_program_config_sn=ij_sys_program_config.sys_program_config_sn","left");
      $this->db->group_start()->or_where('role_sn',$role_sn);
      $this->db->or_where('role_sn',null)->group_end();
      $this->db->where('ij_sys_program_config.status','1');
		  $array2= $this->db->get()->result_array();*/
		  $array2=$this->programTree(0,'','','',0,1); //1只抓有效
		  //var_dump($array2);
		  //exit();
		//echo $this->db->last_query();
			if($array2){
			  foreach($array2 as $key2=>$value2) {
					//$array1=$this->bindecValues($value2['system_rule_set']);
					$_where=array('role_sn'=>$role_sn,'sys_program_config_sn'=>$value2['sys_program_config_sn']);
					$role_sys_program_relation=$this->_select('ij_role_sys_program_relation',$_where,0,0,0,0,0,'row');
        	$array2[$key2]['system_rule']=$this->get_system_rule($role_sys_program_relation['sort_order'],0,$this->get_clean_Array($this->get_system_rule($value2['sort_order'],0,0)));
        	$array2[$key2]['role_sys_program_relation_sn']=$role_sys_program_relation['role_sys_program_relation_sn'];
			  }
			}
		  //exit();

			return  $array2;
		}
	public function get_member_roles($member_sn,$if_status=0){
      $this->db->select('role_sn,role_name');
      $this->db->from("ij_role");
      $this->db->where('status','1');
	  $array2= $this->db->get()->result_array();
    //echo $this->db->last_query();
      $this->db->select('member_role_relation_sn,status,role_sn');
      $this->db->from("ij_member_role_relation");
      $this->db->where('member_sn',$member_sn);
      if($if_status){
      	$this->db->where('status','1');
      }
		  $array1= $this->db->get()->result_array();
    //echo $this->db->last_query();exit;
		  $array3=array();
			if($array2){
			  foreach($array2 as $key2=>$value2) {
					//var_dump($this->chk_array($array1,'role_sn',$value2['role_sn']));
					//檢查角色是否有已有關連檔資料，沒有就新增
			  	if($array1 && $this->chk_array($array1,'role_sn',$value2['role_sn'])){
				  	foreach($array1 as $key1=>$value1) {
				  		if($value1['role_sn']==$value2['role_sn']){
	        			$array2[$key2]['status']=$value1['status'];
	        			$array2[$key2]['member_role_relation_sn']=$value1['member_role_relation_sn'];
	        			$array3[$key1]['role_sn']=$value2['role_sn'];
	        			$array3[$key1]['role_name']=$value2['role_name'];
	        		}
	        	}
	        }else{
      			if(!$if_status){
					$_data['member_sn']=$member_sn;
					$_data['role_sn']=$value2['role_sn'];
					$_data['status']='0';
				    $_data = $this->libraries_model->CheckUpdate($_data,1);
				    $_data = $this->libraries_model->CheckUpdate($_data,0);
					$array2[$key2]['member_role_relation_sn']=$this->libraries_model->_insert('ij_member_role_relation',$_data);
		            $array2[$key2]['status']='0';//預設不選取
		            //$array2['empty_role']=true;
		        }else{
		          //$array3['empty_role']=true;
		        }
	        }
			  }
			}
     	if(!$if_status){
				return  $array2;
			}else{
				return  $array3;
			}
		}
		public function chk_array($_array,$_field,$_chk_value){
			foreach($_array as $_key=>$_value) {
				if($_value[$_field]===$_chk_value){
					return true;
					exit();
				}
			}
			return false;
		}
		public function get_role_programs($role_sn,$if_english=0){

 		  $array2=$this->programTree(0,'','','',0,1,0); //1只抓有效 第二層
		  //var_dump($array2);
		  //exit();
		//echo $this->db->last_query();
			if($array2){
			  foreach($array2 as $key2=>$value2) {
					//$array1=$this->bindecValues($value2['system_rule_set']);
					$_where=array('role_sn'=>$role_sn,'sys_program_config_sn'=>$value2['sys_program_config_sn']);
					$role_sys_program_relation=$this->_select('ij_role_sys_program_relation',$_where,0,0,0,0,0,'row');
        	$array3[$key2]['sys_program_name']=$value2['sys_program_name'];
        	$array3[$key2]['sys_program_eng_name']=$value2['sys_program_eng_name'];
        	$array3[$key2]['sort_order']=$role_sys_program_relation['sort_order'];
        	$array3[$key2]['system_rule']=implode('、',$this->get_system_rule($role_sys_program_relation['sort_order'],1,0,$if_english));
		  		//var_dump($array3[$key2]['system_rule']);
		  		//echo '<br>';
			  }
			}
			return  $array3;
		}
        public function add_update_hot_search($_search=0){
            if($_search=trim($_search)){
                $_where=array('banner_content_name'=>$_search);
                if(!$banner_content_sn=@$this->_get_adsMaterials(0,$_where)[0]['banner_content_sn']){
                    $_data_array=array(
                        'banner_content_name'=>$_search,
                        'status'=>'2',
                        'click_total_amount'=>'0',
                        'view_total_amount'=>'1',
                        'banner_content_save_dir'=>$_search,
                        'banner_eff_start_date'=>date("Y-m-d H:i:s",time()),
                        'published_locaiotn_type'=>'1',
                        'banner_location_sn'=>'11',
                        'sort_order'=>'9999',
                        'banner_type'=>'4',
                    );
                    $_data_array = $this->CheckUpdate($_data_array,1);
                    $_data_array = $this->CheckUpdate($_data_array,0);//方便秀最後更新
                    $banner_content_sn=$this->_insert('ij_banner_content',$_data_array);
                }else{
                    $_upt_array=array(
                        'banner_eff_end_date'=>date("Y-m-d H:i:s",time()),
                    );
                    $this->_update('ij_banner_content',$_upt_array,'banner_content_sn',$banner_content_sn);
                    $this->_update_field_amount('ij_banner_content','banner_content_sn',$banner_content_sn,'view_total_amount','+1');
                }
            }
        }
   public function get_dealer_categorys($_dealer_sn=0,$_product_relation_sn=0){
        $this->db->select('ij_category.*,relation_status,ij_category_relation.sort_order as sort_order,product_relation_sn');
        $this->db->from('ij_category');
        $this->db->join('ij_category_relation','ij_category.category_sn=ij_category_relation.category_sn','left');
        //$this->db->where('upper_category_sn','0');
        $this->db->where('category_status','1');
        if($_dealer_sn){
            $this->db->where('dealer_sn',$_dealer_sn);
        }
        if($_product_relation_sn){
            $this->db->where('product_relation_sn',$_product_relation_sn);
        }
        $this->db->order_by('ij_category_relation.sort_order');
        $array=$this->db->get()->result_array();
        //echo $this->db->last_query();
        //var_dump($array);exit;
        if($array){
            foreach($array as $row){
                //$category_names[]=$row['category_name'];
            }
        }else{
            $array=$this->get_dealer_categorys();
            foreach($array as $_key=>$_Item){
        //var_dump($_Item);
                $this->_insert('ij_category_relation',array('category_sn'=>$_Item['category_sn'],'dealer_sn'=>$_dealer_sn,'relation_status'=>'1','sort_order'=>'999'));
                $array[$_key]['relation_status']='1';
            }
            //$this->get_dealer_categorys($_dealer_sn);
        }
        return $array;
       //var_dump($array);exit;
    }
    public function dealer_categoryParentChildTree($_dealer_sn=0,$category_sn=0,$spacing = '├─', $category_tree_array = '',$spacing2='│&nbsp;',$_level=0)
    {
        //NodeId為Parent的點
            if (!is_array($category_tree_array))
                $category_tree_array = array();

        //取得該點的下一層
        $this->db->select('ij_category_relation.category_sn,category_name,upper_category_sn,ij_category_relation.sort_order,category_status,relation_status,product_relation_sn');
        $this->db->from("ij_category");
        $this->db->join('ij_category_relation','ij_category.category_sn=ij_category_relation.category_sn','left');
        $this->db->where('upper_category_sn',$category_sn);
        $this->db->where('category_status','1');
        if($_dealer_sn){
            $this->db->where('dealer_sn',$_dealer_sn);
        }
        $query =$this->db->get()->result_array();
        if($query) {
            //如果有下一層資料，逐一取出
            $_level++;
        foreach($query as $key => $intro){
            //echo $spacing . $intro['category_name'].'<br>';
          $category_tree_array[] = array("category_sn" => $intro['category_sn'], "category_name" => $spacing . $intro['category_name'], "upper_category_sn" => $intro['upper_category_sn'], "sort_order" => $intro['sort_order'], "relation_status" => $intro['relation_status'], "pure_category_name" => $intro['category_name'], "level" => $_level, "product_relation_sn" => $intro['product_relation_sn']);
          $category_tree_array = $this->dealer_categoryParentChildTree($_dealer_sn,$intro['category_sn'], $spacing2.$spacing , $category_tree_array,$spacing2,$_level);
            //var_dump($category_tree_array);
                }
        }
            return $category_tree_array;
    }
    public function get_order_statics($_select=0,$_where=0,$_ifok=1){
        if($_select) $this->db->select($_select);
        $this->db->from("ij_sub_order");
        $this->db->join("ij_order","ij_order.order_sn=ij_sub_order.order_sn","left");
        if($_where) $this->db->where($_where,'',false);
        if($_ifok){
            $this->db->where('payment_status','2'); //已付款
            //$this->db->where('sub_order_status','5'); //完成
        }
        //加入供應商、經銷商機制
        if($this->session->userdata('member_role_system_rules')[0]['role_name']=='供應商管理員'){
            $this->db->where('ij_sub_order.associated_supplier_sn',$this->session->userdata['member_data']['associated_supplier_sn']);
        }else if($this->session->userdata('member_role_system_rules')[0]['role_name']=='經銷商管理員'){
            $this->db->where('ij_sub_order.associated_dealer_sn',$this->session->userdata['member_data']['associated_dealer_sn']);
        }
        //$this->db->group_by('member_sn');
        return $this->db->get()->row_array();
    }
    public function get_order_item_statics($_where=0,$_oreder='buy_amount desc'){
        $this->db->select('ij_order_item.product_sn,ij_order_item.product_name,sum(actual_sales_amount) as actual_sales_amount,sum(buy_amount) as buy_amount,click_number');
        $this->db->from("ij_order_item");
        $this->db->join("ij_sub_order","ij_sub_order.sub_order_sn=ij_order_item.sub_order_sn","left");
        $this->db->join("ij_order","ij_order.order_sn=ij_sub_order.order_sn","left");
        $this->db->join("ij_product","ij_product.product_sn=ij_order_item.product_sn","left");
        if($_where) $this->db->where($_where);
        $this->db->where('payment_status','2'); //已付款
        $this->db->where('sub_order_status','1'); //有效
        $this->db->group_by('product_sn');
        $this->db->order_by($_oreder);
        //加入供應商、經銷商機制
        if($this->session->userdata('member_role_system_rules')[0]['role_name']=='供應商管理員'){
            $this->db->where('ij_sub_order.associated_supplier_sn',$this->session->userdata['member_data']['associated_supplier_sn']);
        }else if($this->session->userdata('member_role_system_rules')[0]['role_name']=='經銷商管理員'){
            $this->db->where('ij_sub_order.associated_dealer_sn',$this->session->userdata['member_data']['associated_dealer_sn']);
        }
        $this->db->limit(5);
        //$this->db->group_by('member_sn');
        return $this->db->get()->result_array();
    }
    public function get_order_members($_where,$_oreder='times desc'){
        $this->db->select('ij_member.member_sn,last_name,sum(total_order_amount) as total_order_amount,count(*) as times');
        $this->db->from("ij_sub_order");
        $this->db->join("ij_order","ij_order.order_sn=ij_sub_order.order_sn","left");
        $this->db->join("ij_member","ij_sub_order.associated_member_sn=ij_member.member_sn","left");
        $this->db->where($_where);
        $this->db->order_by($_oreder);
        $this->db->group_by('member_sn');
        //加入供應商、經銷商機制
        if($this->session->userdata('member_role_system_rules')[0]['role_name']=='供應商管理員'){
            $this->db->where('ij_sub_order.associated_supplier_sn',$this->session->userdata['member_data']['associated_supplier_sn']);
        }else if($this->session->userdata('member_role_system_rules')[0]['role_name']=='經銷商管理員'){
            $this->db->where('ij_sub_order.associated_dealer_sn',$this->session->userdata['member_data']['associated_dealer_sn']);
        }
        $this->db->limit(5);
        return $this->db->get()->result_array();
    }
    public function get_order_statics_period($_kind='sum(actual_sales_amount)'){
        $this->db->select("date_format(`order_create_date`,'%x.%v') as sdate ,$_kind as actual_sales_amount");
        $this->db->from("ij_order_item");
        $this->db->join("ij_sub_order","ij_sub_order.sub_order_sn=ij_order_item.sub_order_sn","left");
        $this->db->join("ij_order","ij_order.order_sn=ij_sub_order.order_sn","left");
        //if($_where) $this->db->where($_where);
        $this->db->where('payment_status','2'); //已付款
        $this->db->where('sub_order_status','1'); //有效
        $this->db->group_by('sdate');
        $this->db->order_by('sdate desc');
        //加入供應商、經銷商機制
        if($this->session->userdata('member_role_system_rules')[0]['role_name']=='供應商管理員'){
            $this->db->where('ij_sub_order.associated_supplier_sn',$this->session->userdata['member_data']['associated_supplier_sn']);
        }else if($this->session->userdata('member_role_system_rules')[0]['role_name']=='經銷商管理員'){
            $this->db->where('ij_sub_order.associated_dealer_sn',$this->session->userdata['member_data']['associated_dealer_sn']);
        }
        $this->db->limit(8);
        $result=$this->db->get()->result();
        //echo $this->db->last_query().'<br>';
        $_one = Array();
        foreach (array_reverse($result) as $row)
        {
            //$_one[] = Array($row->sdate, number_format($row->actual_sales_amount));
            $_one[] = Array($row->sdate, $row->actual_sales_amount);
        }
        return $_one;
    }
    //購物金發放或使用
    public function sav_gift_cash($member_sn,$gift_cash_amount,$donate_member_sn=0,$associated_order_sn=0,$memo=0){
        if($member_sn && $gift_cash_amount){
            $where['associated_member_sn']=$member_sn;
            $cash=array(
                'associated_member_sn'=>$member_sn,
                'cash_type'=>'3',
                'status'=>'1',
            );

            if($memo){
                $cash['memo']=$memo;
            }
            //if($gift_cash_amount>0){
                 $cash['cash_amount']=$gift_cash_amount;
            //}else{
                 //$cash['used_amount']=$gift_cash_amount;
            //}
            if($donate_member_sn>0){
                 $cash['donate_member_sn']=$donate_member_sn;
                 $where['donate_member_sn']=$donate_member_sn;
            }
            if($associated_order_sn>0){
                 $cash['associated_order_sn']=$associated_order_sn;
                 $where['associated_order_sn']=$associated_order_sn;
            }
            if($gift_cash_sn=$this->libraries_model->_select('ij_gift_cash',$where,0,0,0,'gift_cash_sn',0,'row',0,0,0,0)['gift_cash_sn']){ //沒資料才新增
                //$cash = $this->libraries_model->CheckUpdate($cash,0); //create
                //$this->_update('ij_gift_cash',$cash,'gift_cash_sn',$gift_cash_sn);
            }else{
                $cash = $this->libraries_model->CheckUpdate($cash,1); //create
                //var_dump($cash);exit;
                $this->_insert('ij_gift_cash',$cash);
            }
            return true;
        }else{
            return false;
        }
    }
public function get_gift_cash($data_array=0,$gift_cash_sn=0){
          $this->db->select('ij_gift_cash.*,partner_member.last_name as partner_name,ij_member.last_name as donate_member_name,status_name,update_member.last_name as update_member_name,sub_order_num');
          $this->db->from("ij_gift_cash");
          $this->db->join("ij_member","ij_member.member_sn=ij_gift_cash.donate_member_sn","left");
          $this->db->join("ij_member as partner_member","partner_member.member_sn=ij_gift_cash.associated_member_sn","left");
          $this->db->join("ij_status_ct","ij_status_ct.status=ij_gift_cash.status","left");
          $this->db->join("ij_member as update_member","update_member.member_sn=ij_gift_cash.update_member_sn","left");
          $this->db->join("ij_sub_order","ij_sub_order.sub_order_sn=ij_gift_cash.wedding_website_sn","left");
        if(is_array($data_array)){
            foreach($data_array as $key=>$Item){
                //echo $key.' xxx '.$Item.'<br>';
                if(null!==$Item){
                    if($key=='from') $this->db->where('create_date >=',$Item);
                    if($key=='to') $this->db->where('create_date <=',$Item);
                    if($key=='partner_sn'){
                         $this->db->where('ij_partner.partner_sn',$Item);
                         $this->db->join("ij_partner","ij_partner.member_sn=ij_gift_cash.associated_member_sn","left");
                    }
                    if($key=='member_sn' || $key=='associated_member_sn'){
                         $this->db->where('ij_gift_cash.associated_member_sn',$Item);
                    }
                    if($key=='status') $this->db->where('ij_gift_cash.status',$Item);
                    if($key=='cash_type') $this->db->where('ij_gift_cash.cash_type',$Item);
                    if($key=='order_sn'){
                        $this->db->join("ij_order","ij_order.order_sn=ij_gift_cash.associated_order_sn","left");
                        $this->db->where('ij_gift_cash.associated_order_sn',$Item);
                    }
                    if($key=='search'){
                        $this->db->group_start();
                        $this->db->like('ij_gift_cash.memo',$Item);
                        $this->db->or_like('partner_member.last_name',$Item);
                        $this->db->group_end();
                    }
                }
            }
        }
        //$this->db->where('order_sn',$Item['order_sn']);
        if($gift_cash_sn){
            $this->db->where('gift_cash_sn',$gift_cash_sn);
            return $this->db->get()->row_array();
        }else{
            $result=$this->db->get()->result_array();
            //echo $this->db->last_query();
            return $result;
        }
    }
    public function get_up_categorys($_category_sn,$up_categorys=0){
        if(!$up_categorys) $up_categorys=array();
        if($up_category_sn=@$this->libraries_model->_select('ij_category',array('category_sn'=>$_category_sn),0,0,0,'category_sn',0,'row','upper_category_sn',0,0,0)['upper_category_sn']){
            $up_categorys[]=$up_category_sn;
            return $this->get_up_categorys($up_category_sn,$up_categorys);
        }else{
            return $up_categorys;
        }
    }
    public function del_up_categorys($product_sn,$_category_sn){
            //var_dump($product_sn);
            //var_dump($_category_sn);
        if($_array=$this->get_up_categorys($_category_sn,0)){
            foreach($_array as $_value){
                $_where=array('product_sn'=>$product_sn,'category_sn'=>$_value);
                //var_dump($_where);
                $this->_delete('ij_product_category_relation',$_where);
            }
        }
    }
    public function get_content_type($_where,$blog_article_sn=0){
        $this->db->select('ij_content_type_config.content_type_sn,content_title');
        $this->db->from("ij_content_type_config");
        //$this->db->join("ij_blog_type_relation","ij_blog_type_relation.content_type_sn=ij_content_type_config.content_type_sn","left");
        $this->db->where($_where);
        $result=$this->db->get()->result_array();
        $result1=array();
        if($blog_article_sn){
            $this->db->select('content_type_sn,blog_type_sn,relation_status');
            $this->db->from("ij_blog_type_relation");
            $this->db->where('blog_article_sn',$blog_article_sn);
            $result1=$this->db->get()->result_array();
        }
        foreach($result as $_key=>$_item){
            $result[$_key]['blog_type_sn']=0;
            $result[$_key]['relation_status']=0;
            if($blog_article_sn && $result1){
                foreach($result1 as $_key1=>$_item1){
                    if($_item['content_type_sn']==$_item1['content_type_sn']){
                        $result[$_key]['blog_type_sn']=$_item1['blog_type_sn'];
                        $result[$_key]['relation_status']=$_item1['relation_status'];
                    }
                }
            }
        }
        //echo $this->db->last_query();
        return $result;
    }
    public function get_content_type_string($blog_article_sn,$_if_numrows=0,$content_type_sn=0){
        $this->db->select('content_title');
        $this->db->from("ij_content_type_config");
        $this->db->join("ij_blog_type_relation","ij_blog_type_relation.content_type_sn=ij_content_type_config.content_type_sn","left");
        if($blog_article_sn){
            $this->db->where('blog_article_sn',$blog_article_sn);
        }
        if($content_type_sn){
            $this->db->where('ij_blog_type_relation.content_type_sn',$content_type_sn);
        }
        $this->db->where('relation_status','1');
        if($_if_numrows){
            return $this->db->get()->num_rows();
        }else{
            $result=$this->db->get()->result_array();
            //var_dump(array_column($result,'content_title'));
            return implode('、',array_column($result,'content_title'));
        }
    }
    //寄出訂單通知信一併更改配送狀態
    public function send_order_mail($order_sn){
        $sub_order_array=$this->_select('ij_sub_order','order_sn',$order_sn,0,0,'order_sn',0,'result_array','ij_sub_order.sub_order_sn,ij_sub_order.associated_supplier_sn,delivery_status,sub_order_status');
        $this->load->library("ijw");
        //var_dump($sub_order_array);exit;
        $data_array=array();
        $data_array['delivery_status']='1';
        $this->load->model('sean_models/Sean_db_tools');
        foreach($sub_order_array as $_key=>$_item){
            //$data_array['designated_delivery_date']=date('Y-m-d H:i:s');
            $_result=$this->_update('ij_sub_order',$data_array,'sub_order_sn',$_item['sub_order_sn']);
            //var_dump($_result);exit;
            //echo $this->db->last_query();exit;
            if(!$_item['delivery_status'] && $_item['sub_order_status']=='1'){
                $_where=" where associated_supplier_sn=".$_item['associated_supplier_sn']." and member_status=2 and (member_level_type=5 or member_level_type=6) order by last_time_update desc"; //有效經銷會員
                $_result03=$this->Sean_db_tools->db_get_max_record("member_sn,last_name,first_name,email,user_name","ij_member",$_where);
                foreach($_result03 as $_key=>$_item){
                    @$this->ijw->send_mail('訂單通知','',$_item->member_sn,'');
                }
            }
        }
        return true;
    }
    public function auto_update_orders(){
        //$five_date=$this->ijw->date_add_days(date('Y-m-d'),-5).' '.date('H:i:s');
        $today=date('Y-m-d H:i:s');
        //var_dump($five_date);exit;
        //訂單成立未付款超過 7日付款狀態自動變為訂單失效並寄信
        $query=$this->db->query("select ij_sub_order.associated_member_sn,sub_order_num from `ij_sub_order` left join ij_order on ij_order.order_sn=ij_sub_order.order_sn where DATEDIFF('$today',order_create_date)>=7 and sub_order_status=1 and payment_status=1");
        if($result=$query->result_array()){
            foreach($result as $_item){
                $this->ijw->send_mail('訂單失效','',$_item['associated_member_sn'],'',$_item['sub_order_num']);
                //var_dump($_item);
            }
        }
        $this->db->query("update `ij_sub_order` left join ij_order on ij_order.order_sn=ij_sub_order.order_sn set sub_order_status=4 where DATEDIFF('$today',order_create_date)>=5 and sub_order_status=1 and payment_status=1");

        //商品已送達後7天後訂單狀態自動變更為訂單完成出帳狀態自動變更為待結帳
        $this->db->query("update `ij_sub_order` left join ij_order on ij_order.order_sn=ij_sub_order.order_sn set sub_order_status=5,checkstatus=1,original_order_sn=1 where DATEDIFF('$today',order_create_date)>=7 and sub_order_status=1 and payment_status=2 and delivery_status=4");
        //商品已出貨後10天後訂單狀態自動變更為訂單完成出帳狀態自動變更為待結帳
        $this->db->query("update `ij_sub_order` left join ij_order on ij_order.order_sn=ij_sub_order.order_sn set sub_order_status=5,checkstatus=1,original_order_sn=1 where DATEDIFF('$today',order_create_date)>=10 and sub_order_status=1 and payment_status=2 and delivery_status=3");
    }
    public function if_card($payment_method){
        $this->db->select('payment_method');
        $this->db->from("ij_payment_method_ct");
        $this->db->where('payment_method',$payment_method);
        $this->db->like('payment_method_eng_name','card');
        return ($this->db->get()->num_rows())?true:false;
    }
}
?>