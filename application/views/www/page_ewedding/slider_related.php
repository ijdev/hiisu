<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 首頁 slider
*/
?>
<!-- BEGIN SLIDER -->
<div class="page-slider margin-bottom-35">
	  <!-- LayerSlider start -->
	  <a href="javascript:;">
		  <div id="layerslider" style="width: 100%; height: 494px; margin: 0 auto;">

		    <!-- slide one start -->
		    <div class="ls-slide">
		      <img src="public/img/slide-relative.png" class="ls-bg" alt="Slide background">
		    </div>
		    <!-- slide one end -->

		  </div>
	</a>
  	<!-- LayerSlider end -->
</div>
<!-- END SLIDER -->