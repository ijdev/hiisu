<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Page Header, include by general/main.php，變數由之傳入
*/
?>
<!-- BEGIN HEADER -->
<div class="page-header">
  <!-- BEGIN HEADER TOP -->
  <div class="page-header-top">
    <div class="container">
      <!-- BEGIN LOGO -->
      <div class="page-logo">
        <a href="admin/dashboard"><img src="public/img/ij-logo.png" alt="logo" class="logo-default"></a>
      </div>
      <!-- END LOGO -->
      <!-- BEGIN RESPONSIVE MENU TOGGLER -->
      <a href="javascript:;" class="menu-toggler"></a>
      <!-- END RESPONSIVE MENU TOGGLER -->
      <!-- BEGIN TOP NAVIGATION MENU -->
      <div class="top-menu">
        <ul class="nav navbar-nav pull-right">
          <!-- BEGIN USER LOGIN DROPDOWN -->
          <li class="dropdown dropdown-user dropdown-dark">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <img alt="" class="img-circle" style="border-radius: unset !important;" src="/public/metronic/admin/layout3/img/avatarb.jpg">
            <span class="username username-hide-mobile"><?php echo $this->session->userdata['member_role_system_rules'][0]['role_name'];?>：</span>
            <span class="username username-hide-mobile"><?php echo $this->session->userdata['member_data']['last_name'];?></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-default">
              <li>
                <a href="admin/CRM/notification">
                <i class="icon-envelope-open"></i> 全站通知 <span class="badge badge-danger"> <?php echo $this->page_data['unreadmessage'];?> </span>
                </a>
              </li>
              <li>
                <a href="admin/main/sys_member_item/edit/<?php echo $this->session->userdata['member_data']['member_sn'];?>">
                <i class="icon-envelope-open"></i> 個人資料 </span>
                </a>
              </li>
<?if($this->session->userdata['member_data']['role_count'] >1){?>
              <li>
                <a href="admin/main/role_select/">
                <i class="icon-envelope-open"></i> 權限角色 </span>
                </a>
              </li>
<?}?>
              <li class="divider"></li>
             <?php
             if($this->session->userdata('member_login')== true)
             {
             	 echo  '
             	 <li>
                <a href="admin/main/managerSignOut">
                <i class="icon-logout"></i> Log Out</a>
              </li>
             	 ';
             }else{
             		echo '
             		 <li>
                <a href="admin/login">
                <i class="icon-key"></i> Log In</a>
              </li>
             		';
             }
             ?>


            </ul>
          </li>
          <!-- END USER LOGIN DROPDOWN -->
        </ul>
      </div>
      <!-- END TOP NAVIGATION MENU -->
    </div>
  </div>
  <!-- END HEADER TOP -->
  <!-- BEGIN HEADER MENU -->
  <div class="page-header-menu">
    <div class="container">
      <div class="hor-menu">
        <ul class="nav navbar-nav" id="list">
          <li class="active">
            <a href="admin/main">控制台</a>
          </li>
          <li class="menu-dropdown classic-menu-dropdown">
            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
              系統設定 <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
                <li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-trophy"></i> 系統管理 </a>
			            <ul class="dropdown-menu pull-left">
			              <li>
			                <a href="admin/main/Sys_member" class="iconify">
			                管理員列表</a>
			              </li>
			              <li>
			                <a href="admin/main/Sys_role" class="iconify">
			                管理員分類</a>
			              </li>
			              <li>
			                <a href="admin/main/sysProgram" class="iconify">
			                系統程式</a>
			              </li>
			            </ul>
			    </li>
              <li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-trophy"></i> 訂單相關 </a>
                <ul class="dropdown-menu">
                   <li>
		                <a href="admin/Sysconfig/Order_cancel_reason_ct" class="iconify">
		                訂單取消原因代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Sub_order_status_ct" class="iconify">
		                訂單狀態代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Cacel_order_process_status_ct" class="iconify">
		                取消訂單處理狀態代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Open_invoice_type_ct" class="iconify">
		                發票開立方式代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Invoice_donor_ct" class="iconify">
		                發票捐贈對象代碼  </a>
		              </li>
		               <li>
		                <a href="admin/Sysconfig/Invoice_carrie_type_ct" class="iconify">
		                發票載具方式代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Pricing_method_ct" class="iconify">
		                定價方式代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Ta_deliver_time_interval_ct" class="iconify">
		                指定送達時段代碼  </a>
		              </li>
		               <li>
		                <a href="admin/Sysconfig/Dealer_order_status_ct" class="iconify">
		                出帳狀態代碼  </a>
		              </li>
		              <!--li>
		                <a href="admin/Sysconfig/Delivery_method_ct" class="iconify">
		                配送方式代碼  </a>
		              </li-->
		              <li>
		                <a href="admin/Sysconfig/Logistics_company_ct" class="iconify">
		                配合物流公司代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Credit_card_type_ct" class="iconify">
		                信用卡類型代碼  </a>
		              </li>
                </ul>
              </li>

				<li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-trophy"></i> 會員相關 </a>
                <ul class="dropdown-menu">
		              <li>
		                <a href="admin/Sysconfig/Member_level_ct" class="iconify">
		                使用者等級代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Member_status_ct" class="iconify">
		                一般使用者狀態代碼   </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Member_level_type_ct" class="iconify">
		                一般使用者身分別代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Dealer_status_ct" class="iconify">
		                經銷商使用者狀態代碼  </a>
		              </li>
		           	  <!--li>
		                <a href="admin/Sysconfig/Dealer_level_type_ct" class="iconify">
		                經銷商使用者身分別代碼  </a>
		              </li-->
		              <li>
		                <a href="admin/Sysconfig/Supplier_status_ct" class="iconify">
		                供應商使用者狀態代碼  </a>
		              </li>
		               <li>
		                <a href="admin/Sysconfig/Supplier_level_type_ct" class="iconify">
		                供應商使用者身分別代碼  </a>
		              </li>
		               <!--li>
		                <a href="admin/Sysconfig/System_status_ct" class="iconify">
		                系統使用者狀態代碼  </a>
		              </li-->
		               <li>
		                <a href="admin/Sysconfig/System_level_type_ct" class="iconify">
		                系統使用者身分別代碼  </a>
		              </li>

		              <li>
		                <a href="admin/Sysconfig/Login_status_ct" class="iconify">
		                 登入狀態代碼 </a>
		              </li>

		              <li>
		                <a href="admin/Sysconfig/Join_path_ct" class="iconify">
		                加入方式代碼  </a>
		              </li>
		               <li>
		                <a href="admin/Sysconfig/Cc_faq_close_ct" class="iconify">
		                客服問與答結案代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Approval_status_ct" class="iconify">
		                批准狀態代碼  </a>
		              </li>
		              <!--li>
		                <a href="admin/Sysconfig/Send_member_level_type_ct" class="iconify">
		                發送使用者身份別代碼  </a>
		              </li-->
		              <!--li>
		                <a href="admin/Sysconfig/Wedding_website_status_ct" class="iconify">
		                婚訊平台使用狀態代碼  </a>
		              </li-->
		              <li>
		                <a href="admin/Sysconfig/Question_status_ct" class="iconify">
		                客服問題單處理狀態代碼  </a>
		              </li>
                </ul>
              </li>
              <li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-trophy"></i> 商品相關 </a>
                <ul class="dropdown-menu">
                   <li>
		                <a href="admin/Sysconfig/Currency_ct" class="iconify">
		                幣別代碼  </a>
		              </li>
		               <li>
		                <a href="admin/Sysconfig/Product_status_ct" class="iconify">
		                產品狀態代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Delivery_status_ct" class="iconify">
		                配送狀態代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Product_type_ct" class="iconify">
		                產品類別代碼  </a>
		              </li>
		               <?/*<li>
		                <a href="admin/Sysconfig/Product_package_type_ct" class="iconify">
		                產品組合類別代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Spec_option_type_ct" class="iconify">
		                規格選項類別代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Brand_status_ct" class="iconify">
		                品牌狀態代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Category_status_ct" class="iconify">
		                產品分類狀態代碼  </a>
		              </li>*/?>
                </ul>
              </li>
               <li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-trophy"></i> 行銷代碼 </a>
                <ul class="dropdown-menu">
	                 <li>
		                <a href="admin/Sysconfig/Event_type_ct" class="iconify">
		                活動類別代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Coupon_code_status_ct" class="iconify">
		                活動優惠代碼使用狀態代碼  </a>
		              </li>
		               <li>
		                <a href="admin/Sysconfig/Discount_type_ct" class="iconify">
		                優惠折扣類型代碼  </a>
		              </li>
		               <?/*<li>
		                <a href="admin/Sysconfig/Serial_card_status_ct" class="iconify">
		                序號卡使用狀態代碼  </a>
		              </li>
		               <li>
		                <a href="admin/Sysconfig/Serial_card_type_ct" class="iconify">
		                序號卡卡別代碼  </a>
		              </li>*/?>

		              <li>
		                <a href="admin/Sysconfig/Discount_code_type_ct" class="iconify">
		                優惠代碼類型代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Banner_type_ct" class="iconify">
		                廣告類別代碼  </a>
		              </li>
                </ul>
              </li>
               <li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-trophy"></i> 地區管理 </a>
                <ul class="dropdown-menu">
                  <li>
		                <a href="admin/Sysconfig/Country_ct" class="iconify">
		                國家代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/State_ct" class="iconify">
		                省(州)代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/City_ct" class="iconify">
		                城市代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Town_ct" class="iconify">
		                行政區代碼  </a>
		              </li>
                </ul>
              </li>
              <li>
                <a href="admin/Sysconfig/Payment_method_ct" class="iconify">
                <i class="fa fa-money"></i> 付款方式 </a>
              </li>
              <li>
                <a href="admin/main/domain" class="iconify">
                <i class="icon-tag"></i> 產業屬性 </a>
              </li>
               <?/*<li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-trophy"></i> 其他代碼 </a>
                <ul class="dropdown-menu">
                  <li>
		                <a href="admin/Sysconfig/Status_ct" class="iconify">
		                 購物金使用狀態代碼 </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Resource_rule_ct" class="iconify">
		                資源規則代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Attribute_type_ct" class="iconify">
		                屬性類別代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Purchase_sales_stock_status_ct" class="iconify">
		                進銷存貨狀態代碼  </a>
		              </li>

		              <li>
		                <a href="admin/Sysconfig/Purchase_sales_stock_type_ct" class="iconify">
		                進銷存貨類別代碼  </a>
		              </li>
		              <li>
		                <a href="admin/Sysconfig/Rewrite_config_file" class="iconify">
		                代碼產生設定檔  </a>
		              </li>
                </ul>
              </li>*/?>
          	  <!--li>
                <a href="admin/admin/basic" class="iconify">
                <i class="fa fa-cogs"></i> 一般設定 </a>
              </li-->

              <!--li>
                <a href="admin/main/Location" class="iconify">
                <i class="fa fa-map-marker"></i> 婚宴地點管理 </a>
              </li-->
            </ul>
          </li>
          <li class="menu-dropdown classic-menu-dropdown">
            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
              商品管理 <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
              <li>
                <a href="admin/product/productTable" class="iconify">
                <i class="icon-handbag"></i> 商品列表 </a>
              </li>
              <li class="dropdown-submenu">
                <a href="javascript:void();" class="iconify">
                <i class="icon-direction"></i> 商品規格 </a>
                <ul class="dropdown-menu">
                  <li class="">
                    <a href="admin/product/productModule">規格單位</a>
                  </li>
                  <li class="">
                    <a href="admin/product/productPackage">規格組合</a>
                  </li>
                  <!--li class="">
                    <a href="admin/product/productColor">版型顏色</a>
                  </li>
                  <li class="">
                    <a href="admin/product/productTemplate">版型設定</a>
                  </li-->
                </ul>
              </li>
              <li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-tag"></i> 商品分類 </a>
                <ul class="dropdown-menu">
                  <li class="">
                    <a href="admin/product/productCategory">分類管理</a>
                  </li>
                  <li class="">
                    <a href="admin/product/productAttribute">分類屬性</a>
                  </li>
                </ul>
              </li>
              <li>
                <a href="admin/product/inventory" class="iconify">
                <i class="fa fa-dashboard"></i> 庫存管理 </a>
              </li>
              <li>
                <a href="admin/product/comment" class="iconify">
                <i class="icon-star"></i> 商品評價 </a>
              </li>
              <li>
                <a href="admin/product/wishList" class="iconify">
                <i class="icon-heart"></i> 追蹤清單 </a>
              </li>
            </ul>
          </li>
          <li class="menu-dropdown classic-menu-dropdown">
            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
              訂單管理 <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
              <li>
                <a href="admin/order/orderTable" class="iconify">
                <i class="icon-basket"></i> 訂單列表 </a>
              </li>
              <!--li>
                <a href="admin/admin/orderDealer" class="iconify">
                <i class="fa fa-bank"></i> 經銷商訂單 </a>
              </li-->
              <!--li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="fa fa-credit-card"></i> 電子禮金管理 </a>
                <ul class="dropdown-menu">
                  <li class="">
                    <a href="admin/admin/credit">禮金列表</a>
                  </li>
                  <li class="">
                    <a href="admin/admin/credit/report">使用統計</a>
                  </li>
                </ul>
              </li-->
              <li>
                <a href="admin/admin/accounting" class="iconify">
                  <i class="fa fa-dollar"></i> 分潤結帳
                </a>
              </li>
            </ul>
          </li>
          <li class="menu-dropdown classic-menu-dropdown">
            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
              內容管理 <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
              <li>
                <a href="admin/content/contentBlog" class="iconify">
                <i class="icon-note"></i> 部落格管理 </a>
              </li>
              <li>
                <a href="admin/content/contentFaq" class="iconify">
                <i class="icon-question"></i> FAQ管理 </a>
              </li>
              <li>
                <a href="admin/content/contentCategory" class="iconify">
                <i class="icon-tag"></i> 內容分類 </a>
              </li>
            </ul>
          </li>
          <li class="menu-dropdown classic-menu-dropdown">
            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
              客戶關係 <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
              <li>
                <a href="admin/member/gift_cash" class="iconify">
                <i class="fa fa-money"></i> 囍市錢包管理 </a>
              </li>
              <li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-bubbles"></i> 客服中心 </a>
                <ul class="dropdown-menu">
		              <li>
		                <a href="admin/CRM/ticketTable" class="iconify">
		                客服單列表 </a>
		              </li>
		              <li>
		                <a href="admin/CRM/ticketCategory" class="iconify">
		                客服單主分類 </a>
		              </li>
		              <li>
		                <a href="admin/CRM/TicketCategory2" class="iconify">
		                客服單次分類 </a>
		              </li>
 		              <li>
		                <a href="admin/CRM/ticketStatus" class="iconify">
		                客服單狀態 </a>
		              </li>
		              <li>
		                <a href="admin/CRM/notification" class="iconify">
		                系統通知管理 </a>
		              </li>
                </ul>
              </li>
              <li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-users"></i> 會員管理 </a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="admin/member/memberTable">一般會員列表 </a>
                  </li>
                  <li>
                    <a href="admin/member/partnerTable">聯盟會員列表 </a>
                  </li>
                  <li>
                    <a href="admin/member/dealerTable">經銷會員列表 </a>
                  </li>
									<!-- <li>
                    <a href="admin/member/partnerTable/apply">聯盟會員申請審理 </a>
                  </li>-->
                </ul>
              </li>
              <li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-book-open"></i> 會員帳務管理 </a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="admin/member/partnerAccounting">聯盟會員 </a>
                  </li>
                  <li>
                    <a href="admin/member/dealerAccounting">經銷會員 </a>
                  </li>
                </ul>
              </li>
              <li class="dropdown-submenu">
                <a href="javascript:;" class="iconify">
                <i class="icon-diamond"></i> 會員分級設定 </a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="admin/member/partnerLevel">聯盟會員 </a>
                  </li>
                  <li>
                    <a href="admin/member/dealerLevel">經銷會員 </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li class="menu-dropdown classic-menu-dropdown">
            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
              行銷工具 <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
              <li>
                <a href="admin/marketing/adsMaterial" class="iconify">
                <i class="icon-list"></i> 廣告列表 </a>
              </li>
              <li>
                <a href="admin/marketing/adsCustomer" class="iconify">
                <i class="icon-users"></i> 廣告客戶 </a>
              </li>
              <li>
                <a href="admin/marketing/keyword" class="iconify">
                <i class="icon-key"></i> 熱門關鍵字 </a>
              </li>
              <li>
                <a href="admin/marketing/adsSlot" class="iconify">
                <i class="icon-grid"></i> 廣告版位 </a>
              </li>
              <li>
                <a href="admin/marketing/adsMaterialReport" class="iconify">
                <i class="fa fa-bar-chart"></i> 廣告報表 </a>
              </li>
              <li>
                <a href="admin/marketing/ecoupon" class="iconify">
                <i class="icon-diamond"></i> 優惠代碼管理 </a>
                <!--ul class="dropdown-menu">
                  <li class="">
                    <a href="admin/marketing/certificate">實體券列印</a>
                  </li>
                  <li class="">
                    <a href="admin/marketing/certificateApply">兌換券管理</a>
                  </li>
                  <li class="">
                    <a href="admin/marketing/couponApply">實體折扣卷管理</a>
                  </li>
                  <li class="">
                    <a href="admin/marketing/ecoupon">虛擬優惠代碼管理</a>
                  </li>
                </ul-->
              </li>
              <li>
                <a href="admin/marketing/edm" class="iconify">
                <i class="icon-envelope-open"></i> EDM 管理 </a>
              </li>
              <!--li>
                <a href="admin/admin/event" class="iconify">
                <i class="fa fa-puzzle-piece"></i> 活動查詢 </a>
              </li-->
            </ul>
          </li>
          <!--li class="menu-dropdown classic-menu-dropdown">
            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
              婚禮網站 <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
              <li>
                <a href="admin/admin/weddingWebsite" class="iconify">
                <i class="icon-book-open"></i> 網站管理 </a>
              </li>
              <li>
                <a href="admin/admin/serialNumber" class="iconify">
                <i class="icon-tag"></i> 序號管理 </a>
              </li>
            </ul>
          </li-->
          <li class="menu-dropdown classic-menu-dropdown">
            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
              供應商管理 <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
              <li>
                <a href="admin/supplier/supplierTable" class="iconify">
                <i class="icon-badge"></i> 供應商列表 </a>
              </li>
              <li>
                <a href="admin/supplier/supplierAccounting" class="iconify">
                <i class="icon-book-open"></i> 供應商帳務管理 </a>
              </li>
              <li>
                <a href="admin/supplier/Shipping" class="iconify">
                <i class="fa fa-truck"></i> 運費設定 </a>
              </li>
            </ul>
          </li>
          <li class="menu-dropdown classic-menu-dropdown">
            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
              系統資訊 <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
              <li>
                <a href="admin/main/" class="iconify">
                <i class="icon-book-open"></i> 網站SEO管理 </a>
              </li>
              <li>
                <a href="admin/admin/Doc" class="iconify">
                <i class="icon-tag"></i> 網站文件管理 </a>
              </li>
              <li>
                <a href="admin/Sysconfig/Mail" class="iconify">
                <i class="icon-envelope"></i> 系統郵件管理 </a>
              </li>
              <li>
                <a href="admin/main" class="iconify">
                <i class="icon-tag"></i> 系統簡訊管理 </a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
      <!-- END MEGA MENU -->
    </div>
  </div>
  <!-- END HEADER MENU -->
</div>
<!-- END HEADER -->