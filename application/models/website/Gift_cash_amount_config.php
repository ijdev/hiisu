<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gift_cash_amount_config extends CI_Model {

    public $wedding_website_sn;
    public $limit = 10; // query limit setting
    public $until = 0; // last query sn

    public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
    }
    /*
    * 禮金選項列表
    * 全數拉出管理
    * 限制的部分 toto
    */
    public function get_result( $wedding_website_sn = 0 )
    {
        $this->db->select('*');
        $this->db->from('ij_gift_cash_amount_config');
        $this->db->where('wedding_website_sn', $wedding_website_sn);
        $this->db->where('status', 1);
        $this->db->order_by('amount', 'asc');
        $this->db->order_by('gift_cash_amount_config_sn', 'desc');
        $query = $this->db->get();
        return $result = $query->result_array();
    }
}