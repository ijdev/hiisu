<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容
*/
?>
<?php $_web_member_data=$this->session->userdata('web_member_data'); //var_dump($_web_member_data);?>
<!-- Page level plugin styles START -->
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<style type="text/css">
  .select2_dealer{
    padding: 0px;
  }
</style>
<div class="container mob_padding">
<!-- Page level plugin styles END -->
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <li class="active">會員資料修改</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
  <div id="memberacside" class="sidebar col-md-2 col-sm-3">
    <div class="desktop_view_sidebar">
		<ul class="list-group margin-bottom-25 sidebar-menu">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
	<div class="mobile_view_sidebar">
		<div class="side-nav-head border_title_bottom">
			<button class="fa fa-bars collapsed" data-toggle="collapse" data-target="#mob_sidebar_drop_down"></button>
			<h4 class="bfont">會員中心<!--會員中心--></h4>
		</div>
		<ul class="list-group margin-bottom-25 sidebar-menu collapse" id="mob_sidebar_drop_down">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
<form name="register-form" id="register-form" method="post" action="member/memberModify">
  <div class="col-md-10 col-sm-9">
<?php $this->load->view('www/general/flash_error');?>
    <div id="productp" class="product-page memb_accnt">
      <h1 class="display_none">
        <i class="fa fa-user"></i> 會員資料修改
      </h1>
	  <div class="row">
						<div class="col-xs-12 col-sm-6 padding-0">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
								<label class="col-xs-12 col-sm-3 control-label">帳號</label>
									<div class="col-xs-12 col-sm-9">
										<p class="form-control-static"><?php echo $_web_member_data["user_name"];?></p>
									</div>
								</div>
							</div>
							<!--div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label class="col-xs-12 col-sm-3 control-label">會員編號</label>
									<div class="col-xs-12 col-sm-9">
										<p class="form-control-static"><?php echo $this->ijw->get_member_number($_web_member_data["member_sn"]);?></p>
									</div>
								</div>
							</div-->
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label class="col-xs-12 col-sm-3 control-label">Email <span class="require">*</span></label>
									<div class="col-xs-12 col-sm-9">
										<input name="email" id="email" value="<?php echo $_web_member_data["email"];?>" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label class="control-label col-xs-12 col-sm-3">姓名 <span class="require">*</span></label>
									<div class="col-xs-12 col-sm-9">
										<input name="last_name" id="last_name" value="<?php echo $_web_member_data["last_name"];?>" class="form-control">
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12">
								<div class="form-group has-feedback">
									<label class="control-label col-xs-12 col-sm-3">性別</label>
									<div class="col-xs-12 col-sm-9">
										<label class="radio c_radio" style="padding-top:0">
											<input type="radio" value="1" <?php if(@$_web_member_data["gender"]=='1') echo 'checked';?> class="st_radio" name="gender" style="display:none" data-bv-field="sex"> <i></i> 男
										</label>
										<label class="radio " style="padding-top:0; margin-top: 0;">
											<input type="radio" class="st_radio" value="0" <?php if(@$_web_member_data["gender"]=='0') echo 'checked';?> name="gender" style="display:none" data-bv-field="sex"> <i></i> 女
										</label></div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12">
								<div class="form-group has-feedback">
									<label class="control-label col-xs-12 col-sm-3">出生日期</label>
									<div class="col-xs-12 col-sm-9 margin-bottom-10">
                  <input type="text" class="form-control date-picker" id="birthday" name="birthday" value="<?php if(isset($_web_member_data["birthday"])) echo $_web_member_data["birthday"];?>">
								</div>
							</div>
            </div>
              <div class="col-xs-12 col-sm-12">
                <div class="form-group has-feedback">
                  <label class="control-label col-xs-12 col-sm-3">結婚日期</label>
                    <div class="col-xs-12 col-sm-9 margin-bottom-10">
                      <input type="text" class="form-control date-picker" name="wedding_date" id="wedding_date" value="<?php if(isset($_web_member_data["wedding_date"]) && $_web_member_data["wedding_date"]!='0000-00-00') echo $_web_member_data["wedding_date"];?>">
                    </div>
                </div>
              </div>
          </div>
						<div class="col-xs-12 col-sm-6 padding-0">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group has-feedback">
									<label class="control-label col-xs-12 col-sm-3">地區</label>
									<div class="col-xs-12 col-sm-9 margin-bottom-10">
										<select id="addr_state" name="addr_state" class="form-control select" data-bv-field="county">
                    <option value="">請選擇</option>
                    <option value="台灣本島" <?=(@$_web_member_data['addr_state']=='台灣本島')?'selected':'';?>>台灣本島</option>
                    <option value="外島地區" <?=(@$_web_member_data['addr_state']=='外島地區')?'selected':'';?>>外島地區</option>
                    </select>
									</div>
									<div class="col-xs-12 col-sm-9 col-sm-offset-3 margin-bottom-10">
										<select id="addr_city_code" name="addr_city_code" class="form-control select" data-bv-field="province">
                    <option value="">請選擇</option>
                          <?php
                             while (list($keys, $values) = each ($city_ct)) {
                              echo  ' <option value="'.$keys.'">'.$values.'</option>';
                          }?>
                    </select></div>
									<div class="col-xs-12 col-sm-9 col-sm-offset-3">
										<select id="addr_town_code" name="addr_town_code" class="form-control select" data-bv-field="city">
                      <option>鄉鎮區</option>
                    </select></div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12">
								<div class="form-group has-feedback">
									<label class="control-label col-xs-12 col-sm-3">地址</label>
									<div class="col-xs-12 col-sm-9 margin-bottom-10">
                      <input type="text" name="zipcode" id="zipcode" class="form-control" id="exampleInputEmail2" size="8" maxlength="5" placeholder="郵遞區號" value="<?php echo @$_web_member_data["zipcode"];?>"> 
									</div>
									<div class="col-xs-12 col-sm-9 col-sm-offset-3">
                    <input type="text" class="form-control" name="addr1" value="<?php echo @$_web_member_data["addr1"];?>">
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12">
								<div class="form-group has-feedback">
									<label class="control-label col-xs-12 col-sm-3">手機 <span class="require">*</span></label>
									<div class="col-xs-12 col-sm-9">
										<input type="text" name="cell" id="cell" value="<?php if(isset($_web_member_data["cell"])) echo $_web_member_data["cell"];?>" class="form-control" data-bv-field="mobile"></div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12">
								<div class="form-group has-feedback">
									<label class="control-label col-xs-12 col-sm-3">市話</label>
									<div class="col-xs-12 col-sm-9">
										<input type="text" name="tel" id="tel" value="<?php if(isset($_web_member_data["tel"])) echo $_web_member_data["tel"];?>" class="form-control" data-bv-field="phone"></div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label class="control-label col-xs-12 col-sm-3">電子報</label>
									<div class="col-xs-12 col-sm-9">
									<label class="radio" style="padding-top:0"><input type="radio" <?php if(@$_web_member_data["accept_edm_flag"]=='1') echo 'checked';?> name="accept_edm_flag" class="st_radio" id="dianzibao1" value="1" style="display:none"> <i></i>訂閱</label>
									<label class="radio" style="padding-top:0;margin:0"><input type="radio" <?php if(@$_web_member_data["accept_edm_flag"]=='0') echo 'checked';?> class="st_radio" name="accept_edm_flag" id="dianzibao0" value="0" style="display:none"> <i></i>取消</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-2 col-sm-offset-5">
							<button type="submit" name="Submit" class="col-xs-12 col-sm-12 btn btn-black">確定</button>
						</div>
					</div>
	  <!-- copy of form -->
    </div>
  </div></form>
  <!-- END CONTENT -->
</div>
</div>
<!-- END SIDEBAR & CONTENT -->
<style>
.form-info.display_none {
    display: none;
}
.display_none{
	display:none;
}
.display_block{
	display:block;
}
.btn-black {
	margin-top:20px;
    background-color: #111;
    color: #FFF;
}
.row .form-group label.col-xs-12.col-sm-3.control-label {
    margin: 2px 0px;
    padding: 7px 10px;
    font-weight: 600;
    text-align: right;
}
.row .col-xs-12.col-sm-12 {
    margin-bottom: 10px;
}
.row .form-group label.radio {
    width: 20%;
    float: left;
    margin: 0px 20px;
}
label.radio.c_radio {
    width: 8% !important;
}
</style>