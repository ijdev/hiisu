<?php
Class MY_Cart extends CI_Cart
{
function __construct()
{
parent::__construct();
}
/**
* Insert
*
* @access private
* @param array
* @return bool
*/
function _insert($items = array())
{
// Was any cart data passed? No? Bah...
if ( ! is_array($items) OR count($items) == 0)
{
log_message('error', 'The insert method must be passed an array containing data.');
return FALSE;
}
// --------------------------------------------------------------------
// Does the $items array contain an id, quantity, price, and name? These are required
if ( ! isset($items['id']) OR ! isset($items['qty']) OR ! isset($items['price']) OR ! isset($items['name']))
{
log_message('error', 'The cart array must contain a product ID, quantity, price, and name.');
return FALSE;
}
// --------------------------------------------------------------------
// Prep the quantity. It can only be a number. Duh...
$items['qty'] = trim(preg_replace('/([^0-9])/i', '', $items['qty']));
// Trim any leading zeros
$items['qty'] = trim(preg_replace('/(^[0]+)/i', '', $items['qty']));
// If the quantity is zero or blank there's nothing for us to do
if ( ! is_numeric($items['qty']) OR $items['qty'] == 0)
{
return FALSE;
}
// --------------------------------------------------------------------
// Validate the product ID. It can only be alpha-numeric, dashes, underscores or periods
// Not totally sure we should impose this rule, but it seems prudent to standardize IDs.
// Note: These can be user-specified by setting the $this->product_id_rules variable.
if ( ! preg_match("/^[".$this->product_id_rules."]+$/i", $items['id']))
{
log_message('error', 'Invalid product ID. The product ID can only contain alpha-numeric characters, dashes, and underscores');
return FALSE;
}
// --------------------------------------------------------------------
// Validate the product name. It can only be alpha-numeric, dashes, underscores, colons or periods.
// Note: These can be user-specified by setting the $this->product_name_rules variable.
//以下修改CART的name支持中文
/* if ( ! preg_match("/^[".$this->product_name_rules."]+$/i", $items['name']))
{
log_message('error', 'An invalid name was submitted as the product name: '.$items['name'].' The name can only contain alpha-numeric characters, dashes, underscores, colons, and spaces');
return FALSE;
}*/
// --------------------------------------------------------------------
// Prep the price. Remove anything that isn't a number or decimal point.
$items['price'] = trim(preg_replace('/([^0-9\\.])/i', '', $items['price']));
// Trim any leading zeros
$items['price'] = trim(preg_replace('/(^[0]+)/i', '', $items['price']));
// Is the price a valid number?
if ( ! is_numeric($items['price']))
{
log_message('error', 'An invalid price was submitted for product ID: '.$items['id']);
return FALSE;
}

// Prep the gift_cash. Remove leading zeros and anything that isn't a number or decimal point.
$items['gift_cash'] = (float) $items['gift_cash'];

// Prep the coupon_money. Remove leading zeros and anything that isn't a number or decimal point.
$items['coupon_money'] = (float) $items['coupon_money'];

// --------------------------------------------------------------------
// We now need to create a unique identifier for the item being inserted into the cart.
// Every time something is added to the cart it is stored in the master cart array.
// Each row in the cart array, however, must have a unique index that identifies not only
// a particular product, but makes it possible to store identical products with different options.
// For example, what if someone buys two identical t-shirts (same product ID), but in
// different sizes? The product ID (and other attributes, like the name) will be identical for
// both sizes because it's the same shirt. The only difference will be the size.
// Internally, we need to treat identical submissions, but with different options, as a unique product.
// Our solution is to convert the options array to a string and MD5 it along with the product ID.
// This becomes the unique "row ID"
if (isset($items['options']) AND count($items['options']) > 0)
{
$rowid = md5($items['id'].implode('', $items['options']));
}
else
{
// No options were submitted so we simply MD5 the product ID.
// Technically, we don't need to MD5 the ID in this case, but it makes
// sense to standardize the format of array indexes for both conditions
$rowid = md5($items['id']);
}
// --------------------------------------------------------------------
// Now that we have our unique "row ID", we'll add our cart items to the master array
// let's unset this first, just to make sure our index contains only the data from this submission
unset($this->_cart_contents[$rowid]);
// Create a new index with our new row ID
$this->_cart_contents[$rowid]['rowid'] = $rowid;
// And add the new items to the cart array
foreach ($items as $key => $val)
{
$this->_cart_contents[$rowid][$key] = $val;
}
// Woot!
return $rowid;
}
//2016-6-6 add
	function _update($items = array())
	{
		// Without these array indexes there is nothing we can do
		if ( ! isset($items['rowid'], $this->_cart_contents[$items['rowid']]))
		{
			return FALSE;
		}

		// Prep the quantity
		if (isset($items['qty']))
		{
			$items['qty'] = (float) $items['qty'];
			// Is the quantity zero?  If so we will remove the item from the cart.
			// If the quantity is greater than zero we are updating
			if ($items['qty'] == 0)
			{
				unset($this->_cart_contents[$items['rowid']]);
				return TRUE;
			}
		}

		// find updatable keys
		$keys = array_intersect(array_keys($this->_cart_contents[$items['rowid']]), array_keys($items));
		// if a price was passed, make sure it contains valid data
		if (isset($items['price']))
		{
			$items['price'] = (float) $items['price'];
		}
		if (isset($items['gift_cash']))
		{
			$items['gift_cash'] = (float) $items['gift_cash'];
		}
		if (isset($items['coupon_money']))
		{
			$items['coupon_money'] = (float) $items['coupon_money'];
		}
		// product id & name shouldn't be changed
		foreach (array_diff($keys, array('id', 'name')) as $key)
		{
			$this->_cart_contents[$items['rowid']][$key] = $items[$key];
		}

		return TRUE;
	}
	function _save_cart()
	{
		// Let's add up the individual prices and set the cart sub-total
		$this->_cart_contents['total_items'] = $this->_cart_contents['cart_total'] = 0;
		$time=time();
		foreach ($this->_cart_contents as $key => $val)
		{
			// We make sure the array contains the proper indexes
			if ( ! is_array($val) OR ! isset($val['price'], $val['qty']))
			{
				continue;
			}
			if($val['coupon_money'] > 0 && $val['coupon_money'] < 1){ //優惠是折扣的話
				$val['coupon_money']=round(($val['price'] * $val['qty'])*(1-$val['coupon_money']));
			}
			$this->_cart_contents['cart_total'] += ($val['price'] * $val['qty'])-$val['gift_cash']-$val['coupon_money'];
			$this->_cart_contents['total_items'] += $val['qty'];
			$this->_cart_contents[$key]['subtotal'] = ($this->_cart_contents[$key]['price'] * $this->_cart_contents[$key]['qty'])-$val['gift_cash']-$val['coupon_money'];
			$this->_cart_contents[$key]['time'] =$time++;
		}

		// Is our cart empty? If so we delete it from the session
		if (count($this->_cart_contents) <= 2)
		{
			$this->CI->session->unset_userdata('cart_contents');

			// Nothing more to do... coffee time!
			return FALSE;
		}

		// If we made it this far it means that our cart has data.
		// Let's pass it to the Session class so it can be stored
		$this->CI->session->set_userdata(array('cart_contents' => $this->_cart_contents));

		// Woot!
		return TRUE;
	}
// --------------------------------------------------------------------
}
?>
