<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容
*/
?>
<div class="container">

<!-- Page level plugin styles START -->
<!-- Page level plugin styles END -->
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <li class="active">聯盟會員</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar col-md-2 col-sm-3">
    <div class="desktop_view_sidebar">
		<ul class="list-group margin-bottom-25 sidebar-menu">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
	<div class="mobile_view_sidebar">
		<div class="side-nav-head border_title_bottom">
			<button class="fa fa-bars collapsed" data-toggle="collapse" data-target="#mob_sidebar_drop_down"></button>
			<h4 class="bfont">會員中心<!--會員中心--></h4>
		</div>
		<ul class="list-group margin-bottom-25 sidebar-menu collapse" id="mob_sidebar_drop_down">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="col-md-10 col-sm-9">
    <div class="product-page">
      <h1>
        <i class="icon-trophy"></i> 聯盟會員
      </h1>
      <?php switch($status):
          case 'apply': ?>
          <div class="alert alert-warning">
            <strong>審核中!</strong> 您的 聯盟會員資格審核中，敬請耐心等待結果，完成審核後 IJWedding 將透過 Email 等多管道通知您是否通過審核．感謝您對 IJWedding 的支持．
          </div>
        <?php break;?>
        <?php case 'deny': ?>
          <div class="alert alert-danger">
            <strong>審核未通過!</strong> 您的聯盟會員資格審核未通過，原因如下：
          </div>
          <div class="well"><?php 
          	$_approval_memo="";
          	$_web_member_data=$this->session->userdata("web_member_data");
          	if(strlen($_web_member_data["approval_memo"]) > 0 )
  					$_approval_memo=$_web_member_data["approval_memo"];
          	echo $_approval_memo;
          	?></div>
        <?php break;?>
        <?php case 'approve': ?>
          <div class="alert alert-success">
            <strong>恭喜您!</strong> 您已成為 IJWedding 的 聯盟會員，您可透過以下的連結分享給您的讀者或好友，藉以獲取分潤獎金：
          </div>
          <div class="well">
           http://www.hiisu.shop/affiliate/fetch/
          </div>
        <?php break;?>
        <?php default: ?>
          <div class="alert alert-info">
            <strong>申請成為聯盟會員!</strong> 一旦審核通過聯盟會員，一旦透過您介紹進來的使用者完成訂購的動作，IJWedding 將提供使用一定比例的獎金給您，趕快填寫表格申請吧．
          </div>
          <div class="row">
            <div class="col-md-12">
            	<?php $this->load->view('www/general/flash_error');?>
              <form name="register-form" id="register-form" novalidate class="form-horizontal" method="post"  action="member/affiliate_Action">
                <h4>基本資料</h4>
                <div class="form-group">
                  <label for="email" class="col-md-4 control-label">網站名稱 <span class="require">*</span></label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" id="affiliate_website" name="affiliate_website" maxlength="30" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-md-4 control-label">網站位置 <span class="require">*</span></label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" id="affiliate_url" name="affiliate_url" maxlength="30" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-md-4 control-label">公司負責人</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" id="affiliate_user" name="affiliate_user" maxlength="30" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-md-4 control-label">公司抬頭</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" id="affiliate_name" name="affiliate_name" maxlength="30" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-md-4 control-label">統一編號</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" id="affiliate_vat"  name="affiliate_vat" maxlength="8" value="">
                  </div>
                </div>
                <h4>基本資料</h4>
                <div class="form-group">
                  <label for="email" class="col-md-4 control-label">戶名</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" id="account_name" name="account_name" maxlength="30" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-md-4 control-label">銀行</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" id="bank_name" name="bank_name" maxlength="30" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-md-4 control-label">分行</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" id="branch_name" name="branch_name" maxlength="30" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-md-4 control-label">帳號</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" id="bank_account" name="bank_account" maxlength="30" value="">
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">                        
                    <button type="submit" class="btn btn-primary">送出變更</button>
               	 	  <button type="button" class="btn btn-default">取消</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        <?php break;?>
      <?php endswitch;?>
    </div>
  </div>
  <!-- END CONTENT -->
</div>
</div>
<!-- END SIDEBAR & CONTENT -->