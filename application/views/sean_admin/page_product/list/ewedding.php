<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品列表 <small>雲端婚訊</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="preview">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品列表
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
        雲端婚訊
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">商品列表 - 雲端婚訊</span>
            </div>
            <div class="actions btn-set">
              <a href="preview/productItem/ewedding" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <div class="table-actions-wrapper pull-right">
            <span>
            </span>
            <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="10" maxlenght="10" style="margin: 0 5px;" placeholder="關鍵字">
            <select class="table-group-action-input form-control input-inline input-small input-sm">
              <option value="">是否發佈</option>
              <option value="publish">發佈</option>
              <option value="unpublished">未發佈</option>
              <option value="delete">已刪除</option>
            </select>
            <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>主分類</th>
                  <th>商品編號</th>
                  <th>名稱</th>
                  <th>定價</th>
                  <th>定價方式</th>
                  <th width="60">代表圖</th>
                  <th>推薦</th>
                  <th>特價</th>
                  <th>熱賣</th>
                  <th>獨家</th>
                  <th>排序</th>
                  <th>是否發佈</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=0;$i<8;$i++):?>
                  <tr class="odd gradeX">
                    <td>雲端婚訊</td>
                    <td>d234234</td>
                    <td>產品<?php echo $i;?><a href="http://dev.ijwedding.com/preview_jerry/shopItem" target="_blank">&nbsp;<i class="fa fa-external-link"></i></a></td>
                    <td>500</td>
                    <td class="center">
                      <?php if($i%2==0):?>
                        單一
                      <?php else:?>
                        區間
                      <?php endif;?>
                    </td>
                    <td><img src="http://dev.ijwedding.com/public/img/cover_sample.png" width="60" ></td>
                    <td class="center">
                      <?php if($i%2==0):?>
                        <i class="fa fa-check"></i>
                      <?php else:?>
                        <i class="fa fa-times"></i>
                      <?php endif;?>
                    </td>
                    <td class="center">
                      <?php if($i%2==0):?>
                        <i class="fa fa-check"></i>
                      <?php else:?>
                        <i class="fa fa-times"></i>
                      <?php endif;?>
                    </td>
                    <td class="center">
                      <?php if($i%2==0):?>
                        <i class="fa fa-check"></i>
                      <?php else:?>
                        <i class="fa fa-times"></i>
                      <?php endif;?>
                    </td>
                    <td class="center">
                      <?php if($i%2==0):?>
                        <i class="fa fa-check"></i>
                      <?php else:?>
                        <i class="fa fa-times"></i>
                      <?php endif;?>
                    </td>
                    <td><?php echo $i;?></td>
                    <td class="center">
                      <?php if($i%2==0):?>
                        <i class="fa fa-check"></i>
                      <?php else:?>
                        <i class="fa fa-times"></i>
                      <?php endif;?>
                    </td>
                    <td class="center">
                      <a href="preview/productItem/ewedding"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                      <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                    </td>
                  </tr>
                <?php endfor;?>
              </tbody>
            </table>

            <div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                  <ul class="pagination" style="visibility: visible;">
                    <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                    <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li class="active"><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                    <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->