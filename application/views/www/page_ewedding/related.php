<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Home 主內容
*/
?>

<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<link href="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="public/metronic/global/plugins/slider-layer-slider/css/layerslider.css" rel="stylesheet">
<!-- Page level plugin styles END -->
<style>
  .product-item{
    margin-top: 12px;
    box-shadow: 1px 3px 5px 1px #888888;
    padding: 0px;
  }
    .img-responsive{
      width: 100%;
    }
    .product-item h3{
      padding:12px 24px 6px 24px;
    }
    .product-item h3 a{
      color:#cebea2;
      font-size:16px;
    }
    .product-item p{
      padding:0 24px 32px 24px;
      margin:0px;
      line-height: 1.5;
      font-size: 14px;
      overflow:hidden;
    }
</style>
<div class="container">
<!--
  <div>
    <img src="public/img/slide-relative.png" style="width:100%;">
  </div>
  -->
  <div class="row padding-top-30 service-box" id="section-related">
    <h1>相關商品</h1>
    <div class="row margin-bottom-60">
      <?php for($i=0;$i<3;$i++):?>
        <div class="col-md-4 col-lg-4">
          <div class="product-item">
            <div class="pi-img-wrapper">
              <img src="http://www.kute.com.tw/UploadFile/GoodPic/142770355013875_middle.jpg" class="img-responsive" alt="Berry Lace Dress">
            </div>
            <h3><a href="javascript:;">新婚小熊喜糖</a></h3>
            <p>
              商品描述商品描述商品描述商品描述商品描述商品描述商品描述商品描述商品描述商品描述商品描述
            </p>
          </div>
        </div>
      <?php endfor;?>
    </div>
  </div>
</div>