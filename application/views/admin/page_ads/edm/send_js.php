<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/jquery.blockui.min.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script src="public/metronic/admin/pages/scripts/components-pickers.js"></script>
<script>
var UIBlockUI = function() {
    var l = function() {
            $("#blockui_sample_4_1").click(function() {
                App.blockUI({
                    target: "#blockui_sample_4_portlet_body",
                    boxed: !0,
                    message: "Processing..."
                }), window.setTimeout(function() {
                    App.unblockUI("#blockui_sample_4_portlet_body")
                }, 2e3)
            })
        };
    return {
        init: function() {
            l()
        }
    }
}();
jQuery(document).ready(function() {       
   	Metronic.init(); // init metronic core components
	  Layout.init(); // init current layout
	  UIBlockUI.init();
    //ComponentsPickers.init();
    $('.date-picker').datepicker({
        rtl: Metronic.isRTL(),
        format: "yyyy-mm-dd",
        orientation: "left",
        autoclose: true
    });
    $('.date-picker1').datepicker({
        rtl: Metronic.isRTL(),
        format: "yyyy-mm",
        orientation: "left",
        autoclose: true
    });
    $(".select2_category").select2({
        tags: [<?php echo $_biz_domain_list;?>]
    });

    $('.send_product_set').click(function(){
			$("input[name=sendfilter][value='send_product_set']").attr('checked',true);
			$.uniform.update();
		});
    $('.over_order_amount').click(function(){
			$("input[name=sendfilter][value='over_order_amount']").attr('checked',true);
			$.uniform.update();
		});
    $('.send_dealer_set').click(function(){
			$("input[name=sendfilter][value='send_dealer_set']").attr('checked',true);
			$.uniform.update();
		});
    $('.send_biz_domain_set').click(function(){
			$("input[name=sendfilter][value='send_biz_domain_set']").attr('checked',true);
			$.uniform.update();
		});
    $('.wedding_year_month').click(function(){
			$("input[name=sendfilter][value='wedding_year_month']").attr('checked',true);
			$.uniform.update();
		});
    $('.wedding_date').click(function(){
			$("input[name=sendfilter][value='wedding_date']").attr('checked',true);
			$.uniform.update();
		});
    $('.send_member_level_type').click(function(){
			$("input[name=sendfilter][value='send_member_level_type']").attr('checked',true);
			$.uniform.update();
		});
	var initTableMember = function () {
        var table = $('#table_member');
        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": 依此欄位做順向排序",
                    "sortDescending": ": 依此欄位做逆向排序"
                },
                "emptyTable": "沒有資料可以顯示",
                "info": "顯示 _TOTAL_ 中的第 _START_ 到 _END_ 筆",
                "infoEmpty": "查無結果",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "顯示 _MENU_ 項目",
                "search": "搜尋:",
                "zeroRecords": "查無結果",
                "paginate": {
                    "previous":"上一頁",
                    "next": "下一頁",
                    "last": "最後",
                    "first": "最前"
                }
            },

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "columns": [{
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": false
            }],
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,            
            "pagingType": "bootstrap_full_number",
            "columnDefs": [{  // set default column settings
                'orderable': true,
                'targets': [0]
            }, {
                "searchable": true,
                "targets": [0]
            }],
            "order": [
                [1, "desc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = jQuery('#table_member_wrapper');

        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('tr').addClass("active");
                } else {
                    $(this).attr("checked", false);
                    $(this).parents('tr').removeClass("active");
                }
            });
            jQuery.uniform.update(set);
        });

        table.on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });

        tableWrapper.find('.dataTables_length select').addClass("form-control input-xsmall input-inline"); // modify table per page dropdown
    }
    initTableMember();
		$(document).on('click', '.deleteItem', function(event) {
        var button = $(this);
        //console.log(button);
        	var r = confirm('是否刪除此筆發送條件?');
            if(r)
            {
				$.post('/admin/admin/ajaxdel/', {dtable: '<?php echo base64_encode('ij_edm_send_config');?>',dfield:'<?php echo base64_encode('edm_send_sn');?>',did:button.attr('ItemId')}, function(data){
                	if (data){
                		button.parents().parents().filter('tr').remove();
                	}else{
                		alert('很抱歉，已發送無法刪除。');
                	}
				});
            }
    });  
});
function SendEdm(edm_send_sn,edm_title){
        	var r = confirm('請確認是否發送？：'+edm_title+'(如果清單較多，敬請耐心等候)');
            if(edm_send_sn && r)
            {
				$.blockUI({
                    target: "#allpage",
                    boxed: !0,
                    message: "彙整名單寄送中，敬請稍後..."
                });
        				//console.log(button.attr('ItemId'));
				$.post('/admin/marketing/get_edm_send_list/', {dtable: '<?php echo base64_encode('ij_edm_send_config');?>',dfield:'<?php echo base64_encode('edm_send_sn');?>',did:edm_send_sn}, function(data){
                    $.unblockUI("#allpage");
                	if (data){
                		//button.parents().parents().filter('tr').remove();
                		//alert('已成功取得寄送名單，排程發送中');
                        //console.log(data);
                		alert(data);
                		location.reload();
                	}else{
                		alert('很抱歉，發送失敗，無法取得寄送名單，請確認條件是否正確。');
                	}
				});
            }
}
</script>

