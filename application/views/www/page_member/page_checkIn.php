<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容
*/
?>
<div class="container">

<!-- Page level plugin styles START -->
<!-- Page level plugin styles END -->
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <li class="active">婚宴管理列表</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar col-md-2 col-sm-3">
    <div class="desktop_view_sidebar">
		<ul class="list-group margin-bottom-25 sidebar-menu">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
	<div class="mobile_view_sidebar">
		<div class="side-nav-head border_title_bottom">
			<button class="fa fa-bars collapsed" data-toggle="collapse" data-target="#mob_sidebar_drop_down"></button>
			<h4 class="bfont">會員中心<!--會員中心--></h4>
		</div>
		<ul class="list-group margin-bottom-25 sidebar-menu collapse" id="mob_sidebar_drop_down">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="col-md-10 col-sm-9">
    <div class="product-page">
        <h1>
          <i class="icon-user-following"></i> 婚宴管理列表
        </h1>
        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                    <tr>
                        <th>婚宴活動名稱</th>
                        <th>活動日期</th>
                        <th>啟用時間</th>
                        <th>賓客上限</th>
                        <th>賓客數</th>
                        <th>功能</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <a href="javascript:void(0);">
                                訂婚婚宴
                            </a>
                        </td>
                        <td>
                            <small>2014/03/10 12:00</small>
                        </td>
                        <td>
                            2014/03/10
                        </td>
                        <td>
                            100
                        </td>
                        <td> 50 </td>
                        <td>
                            <a href="javascript:void(0);" class="btn default btn-xs yellow-stripe">編輯</a>&nbsp;
                            <a data-toggle="modal" href="#serialNo" class="btn default btn-xs green-stripe" disabled>啟用</a>&nbsp;
                            <a href="javascript:void(0);" class="btn default btn-xs green-stripe">升級</a>&nbsp;
                            <a href="javascript:void(0);" class="btn default btn-xs red-stripe">刪除</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="javascript:void(0);">
                                結婚婚宴
                            </a>
                        </td>
                        <td>
                            <small>2014/03/10 12:00</small>
                        </td>
                        <td>
                            -
                        </td>
                        <td> 50 </td>
                        <td> 5 </td>
                        <td>
                            <a href="javascript:void(0);" class="btn default btn-xs yellow-stripe">編輯</a>&nbsp;
                            <a data-toggle="modal" href="#serialNo" class="btn default btn-xs green-stripe">啟用</a>&nbsp;
                            <a href="javascript:void(0);" class="btn default btn-xs blue-stripe" disabled>升級</a>&nbsp;
                            <a href="javascript:void(0);" class="btn default btn-xs red-stripe">刪除</a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <a href="javascript:void(0);" class="btn blue btn-block">
                新增婚宴 <i class="fa fa-plus"></i>
            </a>
        </div>
    </div>
  </div>
  <!-- END CONTENT -->
</div>
<!-- END SIDEBAR & CONTENT -->
</div>