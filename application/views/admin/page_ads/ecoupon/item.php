<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<!-- END PAGE LEVEL STYLES -->



<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>行銷管理 <small>優惠代碼編輯</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">首頁</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        優惠活動
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/marketing/ecoupon">優惠代碼</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        設定
      </li>
    </ul>
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN SETTING TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">優惠代碼設定</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/marketing/ecoupon" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <form id="form1" class="form-horizontal" role="form" method="post" onsubmit="return checkForm();">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">活動名稱<span class="require">*</span></label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" required name="Item[coupon_code_event_name]" value="<?php echo @$Item['coupon_code_event_name'];?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">活動起始日期<span class="require">*</span></label>
                  <div class="col-lg-4">
                    <div class="input-group date form_datetime">
                      <input type="text" class="form-control form-control-inline required input-medium date-picker" name="Item[event_start_date]" value="<?php echo (@$Item['event_start_date']!='0000-00-00 00:00:00' && @$Item['event_start_date']!='')?date_format(date_create(@$Item['event_start_date']),"Y-m-d"):'';?>">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">活動結束日期<span class="require">*</span></label>
                  <div class="col-lg-4">
                    <div class="input-group date form_datetime">
                      <input type="text" class="form-control form-control-inline required input-medium date-picker" name="Item[event_end_date]" value="<?php echo (@$Item['event_end_date']!='0000-00-00 00:00:00' && @$Item['event_end_date']!='')?date_format(date_create(@$Item['event_end_date']),"Y-m-d"):'';?>">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-2 control-label">折扣類型<span class="require">*</span></label>
                  <div class="col-lg-10">
                    <div class="radio-list row">
                      <div class="col-md-3">
                        <label class="radio-inline" style="padding-left:0px;">
                          <input type="radio" name="Item[discount_type]" required id="discount_type1" <?php echo(@$Item['discount_type']=='1')?'checked':'';?> <?php echo(@$Item['ifused'])?'disabled':'';?> value="1">
                           金額抵用 (ex.90)
                        </label>
                      </div>
                      <div class="col-md-8">
                        <input type="number" size="3" name="Item[discount_amount]" id="discount_amount" value="<?php echo @$Item['discount_amount'];?>" min="0" <?php echo(@$Item['ifused'])?'disabled':'';?> class="form-control form-control-inline input-xsmall input-sm">
                      </div>
                    </div>
                    <div class="radio-list row">
                      <div class="col-md-3">
                        <label class="radio-inline" style="padding-left:0px;">
                          <input type="radio" name="Item[discount_type]" required id="discount_type2" <?php echo(@$Item['discount_type']=='2')?'checked':'';?> <?php echo(@$Item['ifused'])?'disabled':'';?> value="2">
                          百分比抵用 (ex.0.9)
                        </label>
                      </div>
                      <div class="col-md-8">
                        <input type="text" size="3" name="Item[discount_percent]" id="discount_percent" value="<?php echo @$Item['discount_percent'];?>" <?php echo(@$Item['ifused'])?'disabled':'';?> class="form-control form-control-inline input-xsmall input-sm">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-2 control-label">代碼類型<span class="require">*</span></label>
                  <div class="col-lg-10">
                    <div class="radio-list row">
                        <label class="radio-inline" style="padding-left:0px;">
                          <input type="radio" name="Item[coupon_code_type]" required id="coupon_code_type1" value="1" <?php echo(@$Item['coupon_code_type']=='1')?'checked':'';?> <?php echo(@$Item['ifused'])?'disabled':'';?> >
                           type1: 一個優惠代碼，可以讓N個會員使用
                        </label>
                    </div>
                    <div class="radio-list row">
                        <label class="radio-inline" style="padding-left:0px;">
                          <input type="radio" name="Item[coupon_code_type]" required id="coupon_code_type2" value="2" <?php echo(@$Item['coupon_code_type']=='2')?'checked':'';?> <?php echo(@$Item['ifused'])?'disabled':'';?> >
                          type2: 每個優惠代碼只能用一次，不能重覆使用
                        </label>
                    </div>
                    <div class="radio-list row">
                        <label class="radio-inline" style="padding-left:0px;">
                          <input type="radio" name="Item[coupon_code_type]" required id="coupon_code_type3" value="3" <?php echo(@$Item['coupon_code_type']=='3')?'checked':'';?> <?php echo(@$Item['ifused'])?'disabled':'';?> >
                          type3: 一個會員只有能有一組優惠代碼，重覆使用
                        </label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-2 control-label">整筆訂單折扣<span class="require">*</span></label>
                  <div class="col-lg-10">
                    <div class="radio-list row">
                        <label class="radio-inline" style="padding-left:0px;">
                          <input type="radio" name="Item[lumsum_discount_flag]" required id="lumsum_discount_flag1" value="1" <?php echo(@$Item['lumsum_discount_flag']=='1')?'checked':'';?> <?php echo(@$Item['ifused'])?'disabled':'';?> >是
                        </label>
                        <label class="radio-inline" style="padding-left:0px;">
                          <input type="radio" name="Item[lumsum_discount_flag]" required id="lumsum_discount_flag2" value="0" <?php echo(@$Item['lumsum_discount_flag']=='0')?'checked':'';?> <?php echo(@$Item['ifused'])?'disabled':'';?> >否
                        </label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">滿額金額</label>
                  <div class="col-lg-2">
                    <input type="number" size="5" min="0" name="Item[min_amount_for_discount]" value="<?php echo @$Item['min_amount_for_discount'];?>" class="form-control" <?php echo(@$Item['ifused'])?'disabled':'';?> placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-2 control-label">適用分類</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" name="Item[apply_category]" rows="3"><?php echo @$Item['apply_category'];?></textarea>
                    <span class="help-block">請用;區隔</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-2 control-label">適用產品</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" name="Item[apply_product]" rows="3"><?php echo @$Item['apply_product'];?></textarea>
                    <span class="help-block">請用;區隔</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">適用對象 <span class="require">*</span></label>
		                    <div class="checkbox-list">
		                    	<?php foreach($send_obj as $_item){?>
		                      <label class="col-xs-2 control-label" <?=($_item['member_level_type_name']=='同時具備供應商及經銷商管理員')?'style="width: 256px;"':''?> <?=($_item['member_level_type_name']=='聯盟會員')?'style="margin-left: 116px;"':''?>>
		                        <input class="checkbox-list checkboxs" type="checkbox" class="form-control" name="apply_TA[]" <?php echo(@$Item['ifused'])?'disabled':'';?> value="<?php echo $_item['member_level_type'];?>" <?php if(@$_item['checked']) echo 'checked';?> >&nbsp;<?php echo $_item['member_level_type_name'];?>
		                      </label>
		                    <?php }?>
		                    </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-2 control-label">適用經銷商</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" name="Item[apply_dealer]" rows="3"><?php echo @$Item['apply_dealer'];?></textarea>
                    <span class="help-block">請用;區隔</span>
                  </div>
                </div>
                <!--div class="form-group">
                  <label for="name" class="col-lg-2 control-label">數量</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control input-medium" value="100">
                  </div>
                </div-->
                <div class="form-group">
                  <label for="note" class="col-lg-2 control-label">備註</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" name="Item[memo]" rows="3"><?php echo @$Item['memo'];?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-md-2 control-label">更新者</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['update_member_sn'])?@$Item['update_member_sn']:@$Item['create_member_sn']; ?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-md-2 control-label">更新時間</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['last_time_update']!='0000-00-00 00:00:00')?@$Item['last_time_update']:@$Item['create_date']; ?>" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                    <a href='admin/marketing/ecoupon'><button type="button" class="btn default">Cancel</button></a>
                    <input type="hidden" name="coupon_code_config_sn" value="<?php echo @$Item['coupon_code_config_sn']; ?>">
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END SETTING TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
