<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<style>
  .select2_dealer,.select3_dealer{height:28px;}
</style>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <?php echo $_page_title;?>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase"><?php echo $_caption_subject;?></span>
            </div>
            <div class="actions btn-set">
              <a href="<?php echo $_action_link;?>" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            	<a href="<?php echo $init_control;?><?php echo $_father_link;?>" class="btn green"><i class="fa fa-list"></i> <?php echo $this->lang->line("back_list");?></a>
          
            </div>
          </div>
          <div class="portlet-body">
          
           <!-- End of search_filter -->
           <!--  <div class="note note-warning" id="search_filter">
              
             
            </div> -->
            <!-- End of search_filter -->
             <?php $this->load->view('sean_admin/general/flash_error');?>
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>＋</th>
                  <th>功能中文名稱</th>
                  <th>功能英文名稱</th>
                  <th>檔案名稱</th>
                  <th>排序順次</th>
                  <th>使用狀態</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
              	
              
                <?php
                
                	if(is_array($_result01))
                	{                                  
                 			foreach($_result01 as $key => $value)
                    		{                    			                    			                    			
                    			if($value->status == 1)
                    				{
                    					$_active = '<i class="fa fa-check"></i>';
                    				}
                    			else
                    				{
                    					$_active = '<i class="fa fa-times"></i>';
                    				}
                    				
                  		
                  ?>
                  <tr class="odd gradeX">
                    <td>
                    	<a href="<?php echo $init_control;?>sys_program_item/add/<?php echo $value->sys_program_config_sn;?>"><i class="fa fa-edit"></i>新增子類</a>&nbsp;&nbsp;
                    	<a href="<?php echo $init_control;?>sys_program/<?php echo $value->sys_program_config_sn;?>/<?php echo $value->sys_program_name;?>"><i class="fa fa-edit"></i>預覽子類</a>
                    </td>
                    <td><?php echo $value->sys_program_name;?></td>
                    <td><?php echo $value->sys_program_eng_name;?></td>
                    <td><?php echo $value->sys_program_url;?></td>
                    <td><?php echo $value->sort_order;?></td>
                    <td><?php echo $_active;?></td>
                    <td class="center">
                      <a href="<?php echo $init_control;?>sys_program_item/edit/<?php echo $value->sys_program_config_sn;?>"><i class="fa fa-edit"></i>編輯</a>&nbsp;&nbsp;
                      <a href="<?php echo $init_control;?>sys_program_item/delete/<?php echo $value->sys_program_config_sn;?>" onclick="return deletechecked();"><i class="fa fa-edit"></i>刪除</a>&nbsp;&nbsp;
                    </td>
                  </tr>
                <?php
                	}
                	
                  }
                ?>
                
              </tbody>
            </table>
           
         
            
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->

