<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容的 page level js
*/
$_web_member_data=$this->session->userdata('web_member_data');
?>

<script src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
<script src="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->
<script src='public/metronic/global/plugins/zoom/jquery.zoom.min.js' type="text/javascript"></script><!-- product zoom -->
<script src="public/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script><!-- Quantity -->
<script src="public/metronic/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>


<script src="public/metronic/frontend/layout/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/frontend/pages/scripts/checkout.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Layout.init();
        Layout.initOWL();
        Layout.initImageZoom();
        Layout.initTouchspin();
        Layout.initFixHeaderWithPreHeader();
        Layout.initNavScrolling();
        //Layout.initUniform();
        Checkout.init();

        jQuery('#checkbox_data_sync_1').change(function(){
        	if($(this).is(":checked")){
						$('#name1').val($('#last_name').val()+$('#first_name').val());
						$('#tel_area_code1').val($('#tel_area_code').val());
						$('#tel1').val($('#tel').val());
						$('#tel_ext1').val($('#tel_ext').val());
						$('#zipcode1').val($('#zipcode').val());
						$('#addr1').val($('#addr').val());
						$('#cell1').val($('#cell').val());
						$('#addr_city_code1').val($("#addr_city_code").find(":selected").val());
						get_town1($("#addr_city_code").find(":selected").val(),$("#addr_town_code").find(":selected").val());
						//$('#addr_town_code1').val($("#addr_town_code").find(":selected").val());
					  //$('select[id="addr_town_code1"]').val($("#addr_town_code").find(":selected").val());
						//console.log($("#addr_town_code").find(":selected").val());
						//console.log($("#addr_town_code1").html());
						//$.uniform.update('#addr_town_code1');
					}
        });
				var addr_city_code='<?php echo (@$order["buyer_addr_city_code"])? $order["buyer_addr_city_code"]:@$_web_member_data["addr_city_code"];?>';
				var addr_town_code='<?php echo (@$order["buyer_addr_town_code"])? $order["buyer_addr_town_code"]:@$_web_member_data["addr_town_code"];?>';
				if(addr_city_code){
					get_town(addr_city_code);
					$('select[id="addr_city_code"]').val(addr_city_code);
				}
			     /* Action On Select Box Change */
					  $('select[id="addr_city_code"]').change(function() {
					        var data = $(this).val(); // Get Selected Value
									get_town(data);
			
			      });
				      
						function get_town(data){
						    $.ajax({
						        url: 'Member/get_town_zip',
						        data: 'data=' + data,
						        dataType: 'json',
						        success: function(data) {
						         		 $('#addr_town_code').empty();
												 $(data).appendTo("#addr_town_code");
		      							 //$.uniform.update('#addr_town_code');
													if(addr_town_code){
														$('select[id="addr_town_code"]').val(addr_town_code);
		      							 		//$.uniform.update('#addr_town_code');
		      							 	}else{
		      							 		//$.uniform.update('#addr_town_code');
													}
						        }
						    });
				    }
					  $('select[id="addr_town_code"]').change(function() {
								var zipcode = $('option:selected', this).attr('zipcode');
								$('#zipcode').val(zipcode);
			      });
			      
					  $('#addr_city_code1').change(function() {
					        var data1 = $(this).val(); // Get Selected Value
									get_town1(data1);
			
			      });
			      
				var addr_city_code1='<?php echo (@$order["receiver_addr_city_code"])? $order["receiver_addr_city_code"]:@$receiver_data["addr_city_code"];?>';
				var addr_town_code1='<?php echo (@$order["receiver_addr_town_code"])? $order["receiver_addr_town_code"]:@$receiver_data["addr_town_code"];?>';
				if(addr_city_code1){
					get_town1(addr_city_code1,addr_town_code1);
					$('select[id="addr_city_code1"]').val(addr_city_code1);
				}
			     /* Action On Select Box Change */
						//console.log(data1);
					  $('select[id="addr_city_code1"]').change(function() {
					        var data1 = $(this).val(); // Get Selected Value
									get_town1(data1);
			
			      });
			      
						function get_town1(data1,addr_town_code1){
						    $.ajax({
						        url: 'Member/get_town_zip',
						        data: 'data=' + data1,
						        dataType: 'json',
						        success: function(data1) {
						         		 $('#addr_town_code1').empty();
												 $(data1).appendTo("#addr_town_code1");
													if(addr_town_code1){
														$('select[id="addr_town_code1"]').val(addr_town_code1);
		      							 		//$.uniform.update('#addr_town_code');
													}
						        }
						    });
				    }
					  $('select[id="addr_town_code1"]').change(function() {
								var zipcode = $('option:selected', this).attr('zipcode');
								//console.log(zipcode);
								$('#zipcode1').val(zipcode);
			      });
			      
					  $('#submit1').click(function(e) {
							if($("#tab2").is(':visible')){
								if(!$('#invoice_title').val()){
									alert('妳選擇公司用（三聯式）發票，發票抬頭必須填寫');
									$('#invoice_title').focus();
									return false;
								}
								if(!$('#company_tax_id').val()){
									alert('妳選擇公司用（三聯式）發票，統一編號必須填寫');
									$('#company_tax_id').focus();
									return false;
								}								
							}
			      });	
		<?php if(@$order["invoice_title"] && @$order["company_tax_id"]){?>
			$('#tab2s').trigger('click');
		<?php }?>
    });
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->