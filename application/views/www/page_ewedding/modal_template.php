<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Modal Template Display 主內容
*/
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <meta charset="utf-8">
  <title>IJWedding Template Screenshot</title>
  <base href="<?php echo $this->config->item('base_url'); ?>" />
  <link href="public/metronic/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="public/metronic/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="public/metronic/global/css/components.css" rel="stylesheet">
  <link href="public/metronic/frontend/layout/css/style.css" rel="stylesheet">
  <style>
    /* Reset */
    html, body, div, span, applet, object, iframe,
    h1, h2, h3, h4, h5, h6, p, blockquote, pre,
    a, abbr, acronym, address, big, cite, code,
    del, dfn, em, img, ins, kbd, q, s, samp,
    small, strike, strong, sub, sup, tt, var,
    b, u, i, center,
    dl, dt, dd, ol, ul, li,
    fieldset, form, label, legend,
    table, caption, tbody, tfoot, thead, tr, th, td,
    article, aside, canvas, details, embed, 
    figure, figcaption, footer, header, hgroup, 
    menu, nav, output, ruby, section, summary,
    time, mark, audio, video {
      margin: 0;
      padding: 0;
      border: 0;
      font-size: 100%;
      font: inherit;
      vertical-align: baseline;
    }
    /* HTML5 display-role reset for older browsers */
    article, aside, details, figcaption, figure, 
    footer, header, hgroup, menu, nav, section {
      display: block;
    }
    body {
      line-height: 1;
      font-family: BlinkMacSystemFont,"proxima-nova","Helvetica Neue",Arial,sans-serif;
    }
    ol, ul {
      list-style: none;
    }
    blockquote, q {
      quotes: none;
    }
    blockquote:before, blockquote:after,
    q:before, q:after {
      content: '';
      content: none;
    }
    table {
      border-collapse: collapse;
      border-spacing: 0;
    }

    body{
      background-color: #FFF;
    }
    .album-window{
      width: 100%;
      max-width: 980px;
      margin: 0 auto;
      padding: 50px 0 50px 0;
    }
    .album-window h2{
      color:#000;
      font-size: 24px;
      margin-left: 0px;
      margin-bottom: 12px;
    }
    .templateTitle{
      float: left;
    }
    .templateColor{
      margin-left: 0px;
      float: right;
    }
      .templateColor button{
        vertical-align: bottom;
      }
    .colorBox{
      width: 32px;
      height: 32px;
      display: inline-block;
      vertical-align: middle;
    }
      .colorBox span{
        width: 28px;
        height: 28px;
        display: inline-block;
        margin:1px;
        border:1px solid #000;
      }
    .colorBox.active{
      width: 32px;
      height: 32px;
      display: inline-block;
      vertical-align: middle;
      border:1px solid #FFF;
    }
      .colorBox.active span{
        width: 28px;
        height: 28px;
        display: inline-block;
        margin:1px;
      }
    .template-module{
      margin-top:24px;
      color:#666;
    }
    .template-module h3{
      border-bottom: 1px dashed #FFF;
      width: 75%;
      margin: 0px auto 12px auto;
      font-size: 20px;
      padding-bottom: 6px;

    }
    .template-module .list-unstyled{
      width: 75%;
      margin: 0px auto 12px auto;
      line-height: 1.5;
    }

    .carousel-control.left{
      background-image:none;
      left: -100px;
    }
    .carousel-control.right{
      background-image:none;
      right: -100px;
    }
    .btn-freetry{
      background-color: #CEBE9F;
      color: #FFF;
      border: 0px solid #000;
    }
    .btn-buy{
      background-color: #F6B93A;
      color: #FFF;
      border: 0px solid #000;
    }
    .btn-slide{
      display: inline-block;
      position: absolute;
      width: 72px;
      height: 72px;
      border-radius: 50%;
      background-position: center center;
      background-size: 100%;
      top: 40%;
      text-indent: -9999px;
    }

    .btn-slide-left{
      background-image: url("<?php echo $this->config->item('base_url'); ?>public/img/btn-slide-left.png") !important;
      opacity: initial !important;
    }

    .btn-slide-right{
      background-image: url("<?php echo $this->config->item('base_url'); ?>public/img/btn-slide-right.png") !important;
      opacity: initial !important;
    }

    /* BEGIN max width 992px */ 
    @media (max-width: 992px) {
      .shop-index-carousel{
        margin-top:30px;
      }
    }
  </style>
</head>
<!-- Body BEGIN -->
<body>
  <div class="album-window container">
    <div class="">
      <div class="col-md-8 col-md-offset-2">
        <h2 class="templateTitle"><?php echo $template['product_name'];?> - <?php echo @$template['color_name'];?></h2>
        <div class="templateColor">
<?php	if(@$template['colors']){foreach(@$template['colors'] as $color){?>
          <span class="colorBox <?php echo ($color['website_color_sn']==@$template['website_color_sn'])?'active':'';?>"><a href="home/templatePreview/<?php echo $template['product_sn'];?>/<?php echo $color['website_color_sn'];?>" title="<?php echo $color['color_name'];?>" ><span class="changecolor" rel="<?php echo $color['website_color_sn'];?>" style="background-color: <?php echo $color['color_code'];?>;"></span></a></span>
<?php }}?>
        </div>
      </div>
      <!-- BEGIN ALBUM -->
      <div class="col-md-8 col-md-offset-2 shop-index-carousel">
        <div class="content-slider">
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
<?php	if(@$template['images']){foreach(@$template['images'] as $key=>$image){?>
              <li data-target="#myCarousel" data-slide-to="<?php echo $key;?>" <?php echo ($key==0)?'class="active"':'';?> ></li>
<?php }}?>
            </ol>
            <div class="carousel-inner">
<?php	if(@$template['images']){foreach(@$template['images'] as $key=>$image){?>
              <div class="item <?php echo ($key==0)?'active':'';?>">
                <img src="public/uploads/<?php echo $image['image_path'];?>" class="img-responsive" style="max-height:500px;" alt="<?php echo $image['image_alt'];?>" title="<?php echo $image['image_alt'];?>">
              </div>
<?php }}?>
            </div>
            <!-- Controls -->
            <a class="left carousel-control btn-slide btn-slide-left" href="#myCarousel" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control btn-slide btn-slide-right" href="#myCarousel" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
      <!-- END ALBUM -->
      <div class="col-md-8 col-md-offset-2">
        <div class="templateColor" style="padding-top:20px;">
          <a class="btn blue btn-sm fancybox detailspopup" product_sn="<?php echo $template['product_sn'];?>" href="javascript:;">免費試用</a>
          <button type="button" class="btn btn-freetry btn-sm add2cart" product_sn="<?php echo $template['product_sn'];?>" href="javascript:;">加入購物車</button>
          <button type="button" class="btn btn-buy btn-sm shopaddcart" product_sn="<?php echo $template['product_sn'];?>" href="javascript:;">直接購買</button>
        </div>
      </div>
    </div>
  </div>
  <script src="public/metronic/global/plugins/jquery.min.js" type="text/javascript"></script>
  <script src="public/metronic/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
  <script src="public/metronic/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    
  <script>
    jQuery(document).ready(function() {
 
      jQuery("#myCarousel").carousel();
      
				$('.add2cart').click(function(e) {
					var product_sn=$(this).attr('product_sn');
					if(product_sn){
					 var url = "shop/add";
					    $.ajax({
					           type: "POST",
					           url: url,
										 dataType:'json',
					           data: { product_sn:product_sn,qty:'1' },
					           success: function(data)
					           {
					           	if (data.error){
					           		alert(data.error);
					           	}else{
					           		$('.top-cart-info-count').text(data.total_items+' 件');
					           		$('.top-cart-info-value').text('$ '+data.total);
					           		//更新購物車popup
					           		$('.top-cart-content').load('shop/top_cart');
					           		//console.log(data.total);
					           	}
					           }
					         });
					  }
					    e.preventDefault();
				});        
				$('.shopaddcart').click(function(e) {
					var product_sn=$(this).attr('product_sn');
					if(product_sn){
					 var url = "shop/add";
					    $.ajax({
					           type: "POST",
					           url: url,
										 dataType:'json',
					           data: { product_sn:product_sn,qty:'1' },
					           success: function(data)
					           {
					           	if (data.error){
					           		alert(data.error);
												parent.jQuery.fancybox.close();
					           		parent.location.href='/shop/shopView';
					           	}else{
					           		$('.top-cart-info-count').text(data.total_items+' 件');
					           		$('.top-cart-info-value').text('$ '+data.total);
												parent.jQuery.fancybox.close();
					           		location.href='/shop/shopView';
					           		//console.log(data.total);
					           	}
					           }
					         });
					  }
				});
				$('.detailspopup').click(function(e) {
					var product_sn=$(this).attr('product_sn');
					if(product_sn){
								parent.jQuery.fancybox.close();
					      parent.location.href='member/my/ewedding/<?php echo urlencode(base64_encode('add_free_site'));?>/'+product_sn;
					  }
				});
    });
  </script>
</body>
</html>