<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<style>
  .select2_dealer,.select3_dealer{height:28px;}
</style>

<!-- BEGIN PAGE HEAD 傳功能名稱 $_page_title  -->
<?php $this->load->view('sean_admin/general/_page_header.php');?>
<!-- END PAGE HEAD -->

<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">

	         	 <div class="portlet-title">

	            <div class="caption">
	              <i class="fa fa-cogs font-green-sharp"></i>
	              <span class="caption-subject font-green-sharp bold uppercase"><?php echo $_caption_subject;?></span>
	            </div>

	            <div class="actions btn-set">
	              <a href="/admin/CRM/Ticketcategory2_item/add" class="btn green"><i class="fa fa-plus"></i> 新增</a>
	            </div>

	          </div>

          <div class="portlet-body">

           <!-- End of search_filter -->
           <!--  <div class="note note-warning" id="search_filter">


            </div> -->
            <!-- End of search_filter -->
            <!-- Begin 系統帶入出訊息-->
             <?php $this->load->view('sean_admin/general/flash_error');?>
            <!-- End 系統帶入出訊息-->

            <table class="table table-striped table-bordered table-hover" id="table_member">
              <?php
              $_table_one= new sean_table_general();
              $_table_one->table_thead($_table_thead_name);
              ?>
              <tbody>

                <?php

                	if(is_array($_body_result))
                	{
                 			foreach($_body_result as $key => $value)
                    	{
			                    $_table_one->table_tbody($value);
                			}

                  }
                ?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->

