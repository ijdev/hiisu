<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/typeahead/typeahead.css">
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/pages/css/portfolio.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>


<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>系統訊息 <small>發送/編輯</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="preview">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="preview/notification/sender">系統訊息</a><i class="fa fa-circle"></i>
      </li>
      <li class="active">
        發送/編輯系統訊息
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">發送/編輯系統訊息</span>
            </div>
            <div class="tools">
              <a href="preview/notification/sender"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">通知對象 <span class="require">*</span></label>
                  <div class="col-lg-10">
                    <textarea class="form-control" name="" rows="3" placeholder="通知對象"></textarea>
                    <span class="help-block">請填寫 Email, 多筆請用逗號","隔開</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">系統訊息主題 <span class="require">*</span></label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="系統訊息主題">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">系統訊息內容</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" name="" rows="6" placeholder="通知內容"></textarea>
                    <span class="help-block">內容或連結擇一填寫，若存在連結內容將無法點入，使用者會直接進入連結網頁</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">系統訊息連結</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="通知連結">
                    <span class="help-block">內容或連結擇一填寫，若存在連結內容將無法點入，使用者會直接進入連結網頁</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">顯示時間</label>
                  <div class="col-lg-10">
                    <div class="input-group date form_datetime">
                      <input type="text" class="form-control" value="2015-05-08 12:00">
                      <span class="input-group-btn">
                        <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-lg-2 control-label">更新者</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control"  placeholder="administrator" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-2 control-label">更新時間</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control"  placeholder="2015-05-25 10:00" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-2 col-md-10">
                    <button type="submit" class="btn green">發送</button>
                    <button type="button" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->