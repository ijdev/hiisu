<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* for admin console template preview
*/
class Product extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('libraries_model');
		$this->page_data['unreadmessage']= $this->libraries_model->get_unread_message_count();
		$this->LoginCheck();
		$this->load->library("ijw");
		$data['Mes'] = $this->session->userdata('Mes');
		if($data['Mes']){$this->load->view('Message',$data);$this->session->unset_userdata('Mes');}
	}
	/**
	 * LoginCheck
	 *
	 */
	public function LoginCheck()
	{
			if($this->session->userdata('member_login')== true){
				return true;
			}else{
				redirect(base_url("admin/login"));
				exit();
			}
	}
	/**
	 * 更新商品最低價
	 *
	 */
	private function _update_min_price()
	{
		$products = $this->libraries_model->_select('ij_product','product_status','1',0,0,0,0,'result_array','product_sn,pricing_method,product_type');
		foreach($products as $p){
			$data = array();
			$price=$this->libraries_model->_get_min_price($p['product_sn'],$p['pricing_method']);
			if($price > 0){
				$data['min_price']=$price;
			}else{
				if($p['product_type']!='2'){ //虛擬允許價錢=0 所以不關閉
					$data['product_status']='0'; // 價錢=0 關閉該商品
				}
			}
			//var_dump($data).'<br>';
			$data = $this->libraries_model->CheckUpdate($data,0);
			$this->libraries_model->_update('ij_product',$data,'product_sn',$p['product_sn']);
		}
	}
	/**
	 * 商品模組 Template
	 *
	 */
	public function productModule()
	{
		$page_data = array();

		// 內容設定 ij_spec_option_config


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$Items=$this->libraries_model->_select('ij_spec_option_config',0,0,0,0,0,0,'result_array');
		foreach($Items as $key=>$Item){
			$Items[$key]['category_names']=$this->libraries_model->_get_categorys($Item['spec_option_config_sn']);
		}
		$page_data['page_content']['Items'] = $Items;
		$page_data['page_content']['view_path'] = 'admin/page_product/module/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_product/module/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 商品模組 Template
	 *
	 */
	public function productModuleItem($spec_option_config_sn=0)
	{
		if(!$spec_option_config_sn){
			$this->Check_page_permission('add','新增');
		}else{
			$this->Check_page_permission('search','查詢');
		}
		//寫入資料庫
		if($data_array=$this->input->post('module')){
			if(!$spec_option_config_sn=$this->input->post('spec_option_config_sn')){ //判斷新增或修改
				$this->Check_page_permission('add','新增');
				$data_array['sort_order'] = $this->ijw->nf_to_wf($data_array['sort_order']); //轉半形
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				$spec_option_config_sn=$this->libraries_model->_insert('ij_spec_option_config',$data_array);
				//規格與分類屬性對應
				$categorys=$this->input->post('categorys[]');
				foreach($categorys as $key=>$ca){
					$data[$key]['spec_option_config_sn']=$spec_option_config_sn;
					$data[$key]['category_sn']=$ca;
					$data[$key]['status']='1';
				  $data[$key] = $this->libraries_model->CheckUpdate($data[$key],1);
					$this->libraries_model->_insert('ij_category_spec_option_relation',$data[$key]);
				}
				$this->session->set_userdata(array('Mes'=>'規格單位新增成功!'));
			}else{
				$this->Check_page_permission('edit','修改');
				$data_array['sort_order'] = $this->ijw->nf_to_wf($data_array['sort_order']); //轉半形
				//exit();
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				$this->libraries_model->_update('ij_spec_option_config',$data_array,'spec_option_config_sn',$spec_option_config_sn);
				//規格與分類屬性對應-先刪除再新增
				$categorys=$this->input->post('categorys[]');
				$this->libraries_model->_delete('ij_category_spec_option_relation','spec_option_config_sn',$spec_option_config_sn);
				foreach($categorys as $key=>$ca){
					$data[$key]['spec_option_config_sn']=$spec_option_config_sn;
					$data[$key]['category_sn']=$ca;
					$data[$key]['status']='1';
				  $data[$key] = $this->libraries_model->CheckUpdate($data[$key],1);
					$this->libraries_model->_insert('ij_category_spec_option_relation',$data[$key]);
				}
				$this->session->set_userdata(array('Mes'=>'規格單位修改成功!'));
			}

			redirect(base_url('admin/product/productModule'));
		}

		$page_data = array();
		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		if($spec_option_config_sn){
			$Item=$this->libraries_model->_select('ij_spec_option_config','spec_option_config_sn',$spec_option_config_sn,0,0,0,0,'row');
			$page_data['page_content']['Item'] = $Item;
		  //$page_data['page_content']['category_attributes'] = $this->libraries_model->category_attributes($spec_option_config_sn);
			$ij_categorys=$this->libraries_model->categoryParentChildTree($Item['channel_name']);
			//foreach($ij_categorys as $crow){
			//		$first[$crow['category_sn']] = $crow['category_name'];
			//}
			//$page_data['page_content']['first']=form_dropdown('first',$first,@$Item['upper_category_sn'],'id="first" multiple size="10" class="form-control" style="option {height:16px};" required');

		  //var_dump($page_data['page_content']['category_attributes']);
		}else{ //新增預設
			$page_data['page_content']['Item']['status']='1'; //預設啟用
		  $page_data['page_content']['category_attributes'] = $this->libraries_model->category_attributes();
		  //$page_data['page_content']['category_attributes'] = $this->libraries_model->_select('ij_attribute_config','status','1',0,0,0,0,'result_array');
		}
		  $page_data['page_content']['categorys']=form_dropdown('categorys[]',array('0'=>'請先選擇館別'),'','id="Categorys" multiple class="form-control" size="10" required');

		$Store=$this->libraries_model->get_channels();
		$page_data['page_content']['Store']=form_dropdown('module[channel_name]',$Store,@$Item['channel_name'],'id="StoreName" class="form-control" required onChange="ChgChannelName(this.value);"');
		$uselimits=array(''=>'無','使用時間(天)'=>'使用時間(天)','使用數量(個)'=>'使用數量(個)');
		$page_data['page_content']['uselimit']=form_dropdown('module[usage_limitation]',$uselimits,@$Item['usage_limitation'],'id="Uselimit" class="form-control"');

		$page_data['page_content']['view_path'] = 'admin/page_product/module/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_product/module/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 模組打包 Template
	 *
	 */
	public function productPackage()
	{
		$page_data = array();

		// 內容設定 ij_product_package_config


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$Items=$this->libraries_model->_select('ij_product_package_config',0,0,0,0,0,0,'result_array');
		/*foreach($Items as $key=>$Item){
			$Items[$key]['category_names']=$this->libraries_model->_get_categorys($Item['spec_option_config_sn']);
		}*/
		$page_data['page_content']['Items'] = $Items;
		$page_data['page_content']['view_path'] = 'admin/page_product/package/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_product/package/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 模組打包編輯 Template
	 *
	 */
	public function productPackageItem($product_package_config_sn=0)
	{
		if(!$product_package_config_sn){
			$this->Check_page_permission('add','新增');
		}else{
			$this->Check_page_permission('search','查詢');
		}
		//寫入資料庫
		if($data_array=$this->input->post('package')){
			if(!$product_package_config_sn=$this->input->post('product_package_config_sn')){ //判斷新增或修改（這邊新增用不到
				$this->Check_page_permission('add','新增');
				$data_array['sort_order'] = $this->ijw->nf_to_wf($data_array['sort_order']); //轉半形
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				$product_package_config_sn=$this->libraries_model->_insert('ij_product_package_config',$data_array);
				//產品組合-規格選項對應
				$specs=$this->input->post('spec[]');
				foreach($specs as $key=>$spec){
					$data[$key]['status']='1';
					$data[$key]['spec_option_limitation']=$spec['spec_option_limitation'];
				  $data[$key] = $this->libraries_model->CheckUpdate($data[$key],1);
					$where = array('product_package_config_sn' => $product_package_config_sn,'spec_option_config_sn' => $spec['spec_option_config_sn']);
					$this->libraries_model->_update('ij_product_package_spec_option_relation',$data[$key],$where);
				}
				//分類與規格組合對應
				$categorys=$this->input->post('categorys[]');
				foreach($categorys as $key=>$category){
					$data2[$key]['product_package_config_sn']=$product_package_config_sn;
					$data2[$key]['category_sn']=$category;
					$data2[$key]['status']='1';
				  $data2[$key] = $this->libraries_model->CheckUpdate($data[$key],1);
					$this->libraries_model->_insert('ij_category_product_package_relation',$data[$key]);
				}
				$this->session->set_userdata(array('Mes'=>'規格組合新增成功!'));
			}else{
				$this->Check_page_permission('edit','修改');
				$data_array['sort_order'] = $this->ijw->nf_to_wf($data_array['sort_order']); //轉半形
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				$this->libraries_model->_update('ij_product_package_config',$data_array,'product_package_config_sn',$product_package_config_sn);
				//產品組合-規格選項對應
				$specs=$this->input->post('spec');
				//var_dump($specs);
				//exit();
				//$this->libraries_model->_delete('ij_product_package_spec_option_relation','product_package_config_sn',$product_package_config_sn);
				foreach($specs as $key=>$spec){
					if(!@$spec['status']){ //沒勾選則刪除對應
						$where = array('product_package_spec_option_relation_sn' => $spec['product_package_spec_option_relation_sn']);
						$this->libraries_model->_delete('ij_product_package_spec_option_relation',$where);
					}else{
						$data[$key]['status']='1';
						$data[$key]['spec_option_limitation']=$spec['spec_option_limitation'];
					  $data[$key] = $this->libraries_model->CheckUpdate($data[$key],1);
						$where = array('product_package_spec_option_relation_sn' => $spec['product_package_spec_option_relation_sn']);
						$this->libraries_model->_update('ij_product_package_spec_option_relation',$data[$key],$where);
					}
				}
				//分類與規格組合對應（先刪除再新增）
				$where = array('product_package_config_sn' => $product_package_config_sn);
				$this->libraries_model->_delete('ij_category_product_package_relation',$where);
				$categorys=$this->input->post('categorys[]');
				foreach($categorys as $key=>$category){
					$data2[$key]['product_package_config_sn']=$product_package_config_sn;
					$data2[$key]['category_sn']=$category;
					$data2[$key]['status']='1';
				  $data2[$key] = $this->libraries_model->CheckUpdate($data2[$key],1);
					$this->libraries_model->_insert('ij_category_product_package_relation',$data2[$key]);
				}
				$this->session->set_userdata(array('Mes'=>'規格組合修改成功!'));
			}

			redirect(base_url('admin/product/productPackage'));
		}

		$page_data = array();
		// 內容設定 ij_product_package_spec_option_relation
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		if($product_package_config_sn){
			$Item=$this->libraries_model->_select('ij_product_package_config','product_package_config_sn',$product_package_config_sn,0,0,0,0,'row');
			$page_data['page_content']['Item'] = $Item;
		  //$page_data['page_content']['category_attributes'] = $this->libraries_model->category_attributes($product_package_config_sn);
			/*$ij_categorys=$this->libraries_model->categoryParentChildTree($Item['channel_name']);
			foreach($ij_categorys as $crow){
					$first[$crow['category_sn']] = $crow['category_name'];
			}*/
			//$page_data['page_content']['first']=form_dropdown('first',$first,@$Item['upper_category_sn'],'id="first" multiple size="10" class="form-control" style="option {height:16px};" required');

		  //var_dump($page_data['page_content']['category_attributes']);
		}else{ //新增預設
			$data_array = array();
		  $data_array = $this->libraries_model->CheckUpdate($data_array,0);
			$page_data['page_content']['Item']['product_package_config_sn']=$this->libraries_model->_insert('ij_product_package_config',$data_array);
			$page_data['page_content']['Item']['status']='1'; //預設啟用
		  $page_data['page_content']['category_attributes'] = $this->libraries_model->category_attributes();
		  //$page_data['page_content']['category_attributes'] = $this->libraries_model->_select('ij_attribute_config','status','1',0,0,0,0,'result_array');
		}
		  $page_data['page_content']['categorys']=form_dropdown('categorys[]',array('0'=>'請先選擇館別'),'','id="Categorys" multiple class="form-control" size="10"');

		$Store=$this->libraries_model->get_channels();
		$page_data['page_content']['Store']=form_dropdown('package[channel_name]',$Store,@$Item['channel_name'],'id="StoreName" class="form-control" onChange="ChgChannelName(this.value);"');


		$page_data['page_content']['view_path'] = 'admin/page_product/package/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_product/package/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 分類列表 Template
	 *
	 */
	public function productCategory($category_sn=0)
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		if($category_sn){
			//$this->session->set_userdata(array('upper_category_sn'=>$category_sn));//記住上層分類
			$page_data['page_content']['current_category']=$this->libraries_model->_select('ij_category','category_sn',$category_sn,0,0,0,0,'row');//當前分類
			if($page_data['page_content']['current_category']['upper_category_sn']){
				//當前分類的上層
				$page_data['page_content']['upper_current_category1']=$this->libraries_model->_select('ij_category','category_sn',$page_data['page_content']['current_category']['upper_category_sn'],0,0,0,0,'row');
				if($page_data['page_content']['upper_current_category1']['upper_category_sn']){
					//當前分類的上上層
					$page_data['page_content']['upper_current_category2']=$this->libraries_model->_select('ij_category','category_sn',$page_data['page_content']['upper_current_category1']['upper_category_sn'],0,0,0,0,'row');
				}
			}
			//$where = array('upper_category_sn' => $category_sn,'category_status' => '1');
		  $CategoryItems=$this->libraries_model->categoryParentChildTree($page_data['page_content']['current_category']['channel_name'],$category_sn,'','','');
		}else{
			//$where = array('category_status' => '1');
		  $CategoryItems=$this->libraries_model->categoryParentChildTree(0,0,'','','');
		}
		//$CategoryItems=$this->libraries_model->_select('ij_category',$where,0,0,0,'upper_category_sn',0,'result_array');
		//var_dump($CategoryItems);

		$Store=$this->libraries_model->get_channels();
		$page_data['page_content']['Store']=form_dropdown('module[channel_name]',$Store,@$Item['channel_name'],'id="StoreName" class="form-control" ');

		$page_data['page_content']['CategoryItems'] = $CategoryItems;
		$page_data['page_content']['view_path'] = 'admin/page_product/category/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_product/category/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 分類編輯 Template
	 *
	 */
	public function productCategoryItem($category_sn=0,$upper_category_sn=0)
	{
		if(!$category_sn){
			$this->Check_page_permission('add','新增');
		}else{
			$this->Check_page_permission('search','查詢');
		}
		$page_data = array();
		//寫入資料庫
		if($data_array=$this->input->post('category')){
			if(!$category_sn=$this->input->post('category_sn')){ //判斷新增或修改
				/*if(intval($data_array['profit_sharing_rate'])){
					$data_array['profit_sharing_rate'] = intval($data_array['profit_sharing_rate'])/100;
				}*/
				/*if(!$upper_category_sn && !$data_array['upper_category_sn']){
					$data_array['upper_category_sn'] = '0';
				}*/
				if(!$data_array['banner_desc']){
					$data_array['banner_desc'] = $data_array['category_name'];
				}
				if(!$data_array['main_picture_desc']){
					$data_array['main_picture_desc'] = $data_array['category_name'];
				}
				$data_array['publish_flag']=($this->input->post('publish_flag'))?'1':'0';
				$data_array['sort_order'] = $this->ijw->nf_to_wf($data_array['sort_order']); //轉半形
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				$category_sn=$this->libraries_model->_insert('ij_category',$data_array);
				//分類屬性
				$category_attribute=$this->input->post('category_attribute[]');
				if($category_attribute){
					foreach($category_attribute as $key=>$ca){
						//echo $ca;
						$data[$key]['category_sn']=$category_sn;
						$data[$key]['attribute_sn']=$ca;
						$data[$key]['status']='1';
					    $data[$key] = $this->libraries_model->CheckUpdate($data[$key],1);
					  //var_dump($data[$key]);
						$this->libraries_model->_insert('ij_category_attribute_log',$data[$key]);
					}
				}
				//exit();
				$this->session->set_userdata(array('Mes'=>'商品分類新增成功!'));
			}else{
				/*if(intval(@$data_array['profit_sharing_rate'])){
					$data_array['profit_sharing_rate'] = intval($data_array['profit_sharing_rate'])/100;
				}*/
				if(!$data_array['banner_desc']){
					$data_array['banner_desc'] = $data_array['category_name'];
				}
				if(!$data_array['main_picture_desc']){
					$data_array['main_picture_desc'] = $data_array['category_name'];
				}
				$data_array['publish_flag']=($this->input->post('publish_flag'))?'1':'0';
				$this->load->library("ijw");
				$data_array['sort_order'] = $this->ijw->nf_to_wf($data_array['sort_order']); //轉半形
				//exit();
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				$this->libraries_model->_update('ij_category',$data_array,'category_sn',$category_sn);
				//分類屬性先刪除再新增
				$this->libraries_model->_delete('ij_category_attribute_log','category_sn',$category_sn);
				$category_attribute=$this->input->post('category_attribute[]');
				if($category_attribute){
					foreach($category_attribute as $key=>$ca){
						//echo $ca;
						$data[$key]['category_sn']=$category_sn;
						$data[$key]['attribute_sn']=$ca;
						$data[$key]['status']='1';
					  $data[$key] = $this->libraries_model->CheckUpdate($data[$key],1);
					  //var_dump($data[$key]);
						$this->libraries_model->_insert('ij_category_attribute_log',$data[$key]);
					}
				}
				$this->session->set_userdata(array('Mes'=>'商品分類修改成功!'));
			}

        $banner_save_dir = $this->db->where('category_sn',$category_sn)->get('ij_category')->row()->banner_save_dir;
        $main_picture_save_dir = $this->db->where('category_sn',$category_sn)->get('ij_category')->row()->main_picture_save_dir;
        $main_banner = $this->db->where('category_sn',$category_sn)->get('ij_category')->row()->main_banner;
				//var_dump($_FILES['banner']);
				//exit();
        if (!empty($_FILES['banner']['name'])) {
						//$this->load->library('upload');
            $config['upload_path']  = ADMINUPLOADPATH.'category/' ;
            $config['allowed_types']= 'gif|jpg|png|jpeg';
            $config['max_size']     = '4196';
            $config['max_width']    = '1920';
            $config['max_height']   = '300';
            $config['encrypt_name']   = true;

            $this->load->library('upload', $config);

						//$this->upload->initialize($config);
            if (!$this->upload->do_upload('banner')) {
                //$error = array('error' => $this->upload->display_errors());
								$this->session->set_userdata(array('Mes'=>$this->session->userdata('Mes').' '.strip_tags($this->upload->display_errors())));
            		//var_dump($error);
						}else{
                $img	 = $this->upload->data();
								$this->libraries_model->_update('ij_category',array('banner_save_dir'=>'category/'.$img['file_name']),'category_sn',$category_sn);
								//var_dump($config['upload_path']);
                if(is_file(ADMINUPLOADPATH.$banner_save_dir)) unlink(ADMINUPLOADPATH.$banner_save_dir);
            }
            //exit();
        }
        if (!empty($_FILES['main_picture']['name'])) {
						//$this->load->library('upload');
            $config['upload_path']  = ADMINUPLOADPATH.'category/' ;
            $config['allowed_types']= 'gif|jpg|png|jpeg';
            $config['max_size']     = '4196';
            $config['max_width']    = '1000';
            $config['max_height']   = '1000';
            $config['encrypt_name']   = true;

            $this->load->library('upload', $config);

						//$this->upload->initialize($config);
            if (!$this->upload->do_upload('main_picture')) {
                	//$error = array('error' => $this->upload->display_errors());
					$this->session->set_userdata(array('Mes'=>$this->session->userdata('Mes').' '.strip_tags($this->upload->display_errors())));
            		//var_dump($error);
				}else{
                	$img	 = $this->upload->data();
					$this->libraries_model->_update('ij_category',array('main_picture_save_dir'=>'category/'.$img['file_name']),'category_sn',$category_sn);
                if(is_file(ADMINUPLOADPATH.$main_picture_save_dir)) unlink(ADMINUPLOADPATH.$main_picture_save_dir);
            }
            //exit();
        }
        if (!empty($_FILES['main_banner']['name'])) {
						//$this->load->library('upload');
            $config['upload_path']  = ADMINUPLOADPATH.'category/' ;
            $config['allowed_types']= 'gif|jpg|png|jpeg';
            $config['max_size']     = '4196';
            $config['max_width']    = '1000';
            $config['max_height']   = '1000';
            $config['encrypt_name']   = true;

            $this->load->library('upload', $config);

			//$this->upload->initialize($config);
            if (!$this->upload->do_upload('main_banner')) {
                //$error = array('error' => $this->upload->display_errors());
				$this->session->set_userdata(array('Mes'=>$this->session->userdata('Mes').' '.strip_tags($this->upload->display_errors())));
			}else{
                $img	 = $this->upload->data();
				$this->libraries_model->_update('ij_category',array('main_banner'=>'category/'.$img['file_name']),'category_sn',$category_sn);
                if(is_file(ADMINUPLOADPATH.$main_banner)) unlink(ADMINUPLOADPATH.$main_banner);
            }
            //exit();
        }
        	$this->session->set_userdata('category_array','');
			redirect(base_url('admin/product/productCategory'));
		}


		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$channel_name='囍市集';
		if($upper_category_sn){ //預設上層分類
			$upper_CategoryItem=$this->libraries_model->_select('ij_category','category_sn',$upper_category_sn,0,0,0,0,'row');
			$channel_name=$upper_CategoryItem['channel_name'];
			$page_data['page_content']['upper_CategoryItem']=$upper_CategoryItem;
		}
		if($category_sn){
			$CategoryItem=$this->libraries_model->_select('ij_category','category_sn',$category_sn,0,0,0,0,'row');
			$page_data['page_content']['CategoryItem'] = $CategoryItem;
			$channel_name=$CategoryItem['channel_name'];
			$upper_category_sn=$CategoryItem['upper_category_sn'];
		    $page_data['page_content']['category_attributes'] = $this->libraries_model->category_attributes($category_sn,1);
		}else{ //新增預設
			$page_data['page_content']['CategoryItem']['category_status']='1'; //預設發佈
		  	$page_data['page_content']['category_attributes'] = $this->libraries_model->category_attributes();
		}
//var_dump($page_data['page_content']['category_attributes']);
		$page_data['page_content']['categorys']=form_dropdown('category[upper_category_sn]',array('0'=>'請先選擇館別'),'','id="Categorys" class="form-control" size="5"');

		$Store=$this->libraries_model->get_channels();
		$page_data['page_content']['Store']=form_dropdown('category[channel_name]',$Store,@$channel_name,'id="StoreName" class="form-control" required onChange="ChgStoreName(this.value);"');
		$page_data['page_content']['channel_name']=@$channel_name;
		$page_data['page_content']['upper_category_sn']=@$upper_category_sn;
		$page_data['page_content']['view_path'] = 'admin/page_product/category/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_product/category/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}
	/**
	 * AJAX取值
	 *
	 */
	public function ChgStoreName(){   //AJAX 根據館別抓第一層
		$channel_name = urldecode($this->uri->segment(4));
		$upper_category_sn = $this->uri->segment(5);
		if($channel_name){
			//$this->db->start_cache();//快取10.14
			$ij_categorys=$this->libraries_model->categoryParentChildTree($channel_name);
			//$where = array('channel_name'=>$channel_name,'upper_category_sn' => 0,'category_status' => 1);
			//var_dump($ij_categorys);
			//exit();
			if($ij_categorys){
				if(stripos($_SERVER['HTTP_REFERER'],"category")!==false){  //是分類才秀無表示第一層
					echo "<option value='0' selected>無</option>";
				}
				foreach($ij_categorys as $crow){
					if($upper_category_sn == $crow['category_sn']){
						$ifselect=' selected';
					}else{
						$ifselect='';
					}
					//if($crow['level'] == 2){
					//	$ifselect.=' disabled';
					//}
					if(strrpos($crow['category_name'], "│")===false){
						$style="style='height:16px;'".$ifselect;
					}else{
						$style="style='height:16px;margin-left: 1px;'".$ifselect;
					}
					echo "<option ".$style." pure_category_name='".$crow['pure_category_name']."' value='".$crow['category_sn']."'>".$crow['category_name']."</option>";
				}
			//$this->db->stop_cache();
			}else{
				echo "<option value='0' selected>無</option>";
			}
		}
	}

	public function ChgExtStoreName(){   //AJAX 根據館別-商品編號抓對應擴展分類
		$channel_name = urldecode($this->uri->segment(4));
		$product_sn = (int)$this->uri->segment(5);
		if($channel_name){
			//echo "<option value='0'>第一層</option>";
			//$this->db->start_cache();//快取10.14
			$ij_categorys=$this->libraries_model->categoryParentChildTree($channel_name);
			$product_category_relation=$this->libraries_model->_select('ij_product_category_relation',array('product_sn' => $product_sn,'status' => 1),0,0,0,'category_sn',0,'rows','category_sn');
			if(!$product_category_relation) $product_category_relation=array();
			//var_dump($product_sn);
			//var_dump(in_array('3', $product_category_relation));
			//exit();
			//if($product_category_relation){
				foreach($ij_categorys as $crow){
					if(in_array($crow['category_sn'], $product_category_relation)){
						$ifselect=' selected';
					}else{
						$ifselect='';
					}
					//if($crow['level'] < 2){
					//	$ifselect.=' disabled';
					//}
					if(strrpos($crow['category_name'], "│")===false){
						$style="style='height:16px;'".$ifselect;
					}else{
						$style="style='height:16px;margin-left: 1px;'".$ifselect;
					}
					echo "<option ".$style." value='".$crow['category_sn']."'>".$crow['category_name']."</option>";
				}
			//}
			//$this->db->stop_cache();
		}
	}
	public function ChgDown(){ //AJAX 根據第一層抓第二層x
		$category_sn = $this->uri->segment(4);
		if($category_sn){
			echo "<option value='0'>第三層</option>";
			$where = array('upper_category_sn' => $category_sn,'category_status' => 1);
			foreach($this->libraries_model->_select('ij_category',$where,0,0,0,0,0,'result','category_sn,category_name') as $crow){
				echo "<option value='$crow->category_sn'>$crow->category_name</option>";
			}
		}
	}
	public function ChgChannelName(){   //AJAX 根據館別抓全部分類 for Module
		$channel_name = urldecode($this->uri->segment(4));
		$spec_option_config_sn = $this->uri->segment(5);
		if($channel_name){
			//echo "<option value='0'>第一層</option>";
			//$this->db->start_cache();//快取10.14
			$ij_categorys=$this->libraries_model->categoryParentChildTree($channel_name);
			//$where = array('channel_name'=>$channel_name,'upper_category_sn' => 0,'category_status' => 1);
			foreach($ij_categorys as $crow){
				if($spec_option_config_sn && $this->libraries_model->_select('ij_category_spec_option_relation',array('spec_option_config_sn' => $spec_option_config_sn,'category_sn' => $crow['category_sn']),0,0,0,'category_sn',0,'row','category_sn')){
					$ifselect=' selected';
				}else{
					$ifselect='';
				}
				if(strrpos($crow['category_name'], "│")===false){
					$style="style='height:16px;'".$ifselect;
				}else{
					$style="style='height:16px;margin-left: 1px;'".$ifselect;
				}
				echo "<option ".$style." value='".$crow['category_sn']."'>".$crow['category_name']."</option>";

			}
			//$this->db->stop_cache();
		}
	}
	public function ChgChannelName2(){   //AJAX 根據館別抓全部分類for productPackageItem
		$channel_name = urldecode($this->uri->segment(4));
		$product_package_config_sn = $this->uri->segment(5);
		if($channel_name){
			//echo "<option value='0'>第一層</option>";
			//$this->db->start_cache();//快取10.14
			$ij_categorys=$this->libraries_model->categoryParentChildTree($channel_name);
			//$where = array('channel_name'=>$channel_name,'upper_category_sn' => 0,'category_status' => 1);
			foreach($ij_categorys as $crow){
				if($product_package_config_sn && $this->libraries_model->_select('ij_category_product_package_relation',array('product_package_config_sn' => $product_package_config_sn,'category_sn' => $crow['category_sn']),0,0,0,'category_sn',0,'row','category_sn')){
					$ifselect=' selected';
				}else{
					$ifselect='';
				}
				if(strrpos($crow['category_name'], "│")===false){
					$style="style='height:16px;'".$ifselect;
				}else{
					$style="style='height:16px;margin-left: 1px;'".$ifselect;
				}
				echo "<option ".$style." value='".$crow['category_sn']."'>".$crow['category_name']."</option>";

			}
			//$this->db->stop_cache();
		}
	}
	public function ChgChannelName3(){   //AJAX 根據館別抓全部分類for productPackageItem
		$channel_name = urldecode($this->uri->segment(4));
		$category_sn = $this->uri->segment(5);
		if($channel_name){
			//echo "<option value='0'>第一層</option>";
			//$this->db->start_cache();//快取10.14
			$ij_categorys=$this->libraries_model->categoryParentChildTree($channel_name);
			//$where = array('channel_name'=>$channel_name,'upper_category_sn' => 0,'category_status' => 1);
			foreach($ij_categorys as $crow){
				if($category_sn==$crow['category_sn']){
					$ifselect=' selected';
				}else{
					$ifselect='';
				}
				if(strrpos($crow['category_name'], "│")===false){
					$style="style='height:16px;'".$ifselect;
				}else{
					$style="style='height:16px;margin-left: 1px;'".$ifselect;
				}
				echo "<option ".$style." value='".$crow['category_sn']."'>".$crow['category_name']."</option>";

			}
			//$this->db->stop_cache();
		}
	}
	public function AddSpec(){   //AJAX 根據分類抓對應規格
		$category_sn = $this->input->post('category_sn');
		$product_package_config_sn = $this->input->post('product_package_config_sn');
		if($category_sn && $product_package_config_sn){
			$spec=$this->libraries_model->_get_spec_by_cat($category_sn,$product_package_config_sn);
			//var_dump($spec);
			echo json_encode($spec);
			//$this->db->stop_cache();
		}
	}
	public function AddColor(){   //AJAX 根據版型編號抓對應顏色
		$product_template_sn = $this->input->post('product_template_sn');
		$website_color_sn = $this->input->post('website_color_sn');
		if($product_template_sn){
			$color=$this->libraries_model->_get_color_by_template($product_template_sn,$website_color_sn);
			//var_dump($color);
			echo json_encode($color);
		}
	}
	public function getProductData($product_sn=0,$parent_product_sn=0){   //AJAX 抓商品名稱跟原價
		if($product_sn){
			$spec=$this->libraries_model->_get_product($product_sn,'product_name,min_price,supplier_sn');
			//var_dump($spec['supplier_sn']);
			if($spec){
				if($parent_product_sn){
					$parent_supplier_sn=$this->libraries_model->_get_product($parent_product_sn,'supplier_sn')['supplier_sn'];
					//var_dump($parent_supplier_sn);
					if($parent_supplier_sn!=$spec['supplier_sn']){
						$spec='1';
					}
				}
				//var_dump($spec);
				echo json_encode($spec);
			}else{
				echo json_encode(false);
			}
			//$this->db->stop_cache();
		}
	}

	public function AddAddon(){   //AJAX 新增加購
		$ifedit = $this->input->post('ifedit');
		$addon_price = $this->input->post('addon_price');
		$addon_limitation = $this->input->post('addon_limit');
		$product_orginal_price = $this->input->post('product_orginal_price');
		$product_sn = $this->input->post('product_sn');
		$associated_product_sn = $this->input->post('associated_product_sn');
		//var_dump($unispec_qty_in_stock);
		/*if(!$this->libraries_model->_select('ij_product','product_sn',$associated_product_sn,1,0,0,0,'row','product_sn')){ // 是否有該編號商品
				$data_array3['error'] = '很抱歉！查無該編號商品資料，請重新確認！';
				echo json_encode($data_array3);
				exit();
		}*/
		if($this->libraries_model->_select('ij_product','product_sn',$associated_product_sn,1,0,0,0,'row','unispec_flag,')['unispec_flag']!='1'){ // 是否多規格
		//echo $this->db->last_query();
				$data_array3['error'] = '很抱歉！該商品為多重規格，無法列為加購商品';
				echo json_encode($data_array3);
				exit();
		}
		//exit();
		$addon_log_sn = $this->input->post('addon_log_sn');
		$product_name = $this->input->post('product_name');
		if($associated_product_sn){
			$data_array['associated_product_sn'] = $associated_product_sn;
		}
		$data_array['addon_price'] = $addon_price;
		$data_array['product_orginal_price'] = $product_orginal_price;
		$data_array['addon_limitation'] = $addon_limitation;
		$data_array['status'] = '1';
		if($addon_price && $product_sn && null!==$addon_limitation){
			if($addon_log_sn){ //修改
					$data_array = $this->libraries_model->CheckUpdate($data_array,0);
					//var_dump($data_array);
					$this->libraries_model->_update('ij_addon_log',$data_array,'addon_log_sn',$addon_log_sn);
			    	$data_array['associated_product_sn'] = $this->libraries_model->_select('ij_addon_log',array('addon_log_sn' => $addon_log_sn),0,0,0,'associated_product_sn',0,'row','associated_product_sn')['associated_product_sn'];
			}else{
				if(!$this->libraries_model->_chk_addon($associated_product_sn,$product_sn)){
					$data_array2['addon_name'] = '一般';
					$data_array2['addon_consistent_price'] = $addon_price;
					$data_array2['apply_product_sn'] = $product_sn;
					$data_array2['status'] = '1';
					$data_array2['status'] = '1';
					$data_array2 = $this->libraries_model->CheckUpdate($data_array2,1);
					$addon_config_sn=$this->libraries_model->_insert('ij_addon_config',$data_array2);
					$data_array['addon_config_sn'] = $addon_config_sn;
		            $data_array['associated_product_sn'] = $associated_product_sn;
					$data_array = $this->libraries_model->CheckUpdate($data_array,1);
					$addon_log_sn=$this->libraries_model->_insert('ij_addon_log',$data_array);
				}else{
					//var_dump($this->libraries_model->_chk_addon($associated_product_sn,$product_sn));
					//echo $this->db->last_query();
					$data_array3['error'] = '該商品已經有設定加購了喔！';
					echo json_encode($data_array3);
					exit();
				}
			}
			$data_array['addon_log_sn'] = $addon_log_sn;
			$data_array['product_name'] = $product_name;
			$data_array['ifedit'] = $ifedit;
			$data_array['unispec_qty_in_stock'] = $this->libraries_model->_select('ij_product','product_sn',$associated_product_sn,1,0,0,0,'row','unispec_qty_in_stock,')['unispec_qty_in_stock'];
			echo json_encode($data_array);
		}else{
			echo '0';
		}
	}

	public function AddAttributeValue(){   //AJAX 新增屬性值
		$attribute_sn = $this->input->post('attribute_sn');
		$attribute_value_name = $this->input->post('attribute_value_name');
		$product_sn = $this->input->post('product_sn');
		if($attribute_sn && $attribute_value_name){
				$data_array['attribute_sn'] = $attribute_sn;
				$data_array['attribute_value_name'] = $attribute_value_name;
				$data_array['sort_order'] = '9999';
				$data_array['status'] = '1';
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				$attribute_value_sn=$this->libraries_model->_insert('ij_attribute_value_config',$data_array);
		}
		if(@$attribute_value_sn){ //成功新增後寫入對應分類屬性
				$data_array2['attribute_sn'] = $attribute_sn;
				$data_array2['product_sn'] = $product_sn;
				$data_array2['attribute_value_sn'] = $attribute_value_sn;
				$data_array2['status'] = '1';
				$data_array2 = $this->libraries_model->CheckUpdate($data_array2,1);
				$where = array('product_sn'=>$product_sn,'attribute_sn' => $attribute_sn,'attribute_value_sn' => $attribute_value_sn);
				//var_dump($data_array2);
				//沒重複才新增
				if($this->libraries_model->_select('ij_product_attribute_log',$where,0,0,0,'product_sn',0,'num_rows')==0){
					$this->libraries_model->_insert('ij_product_attribute_log',$data_array2);
				}
				echo '1';
		}else{
				echo '0';
		}
	}

	public function AddAttribute(){   //AJAX 根據分類抓對應分類屬性
		$categorys = $this->input->post('categorys');
		$product_sn = $this->input->post('product_sn');
		//var_dump($categorys);
		//exit();
		if($categorys && $product_sn){
			$Attribute=$this->libraries_model->_get_Attribute_by_cat($categorys,$product_sn);
			//var_dump($Attribute);
			echo json_encode($Attribute);
			//$this->db->stop_cache();
		}
	}
	public function GetPackage(){   //AJAX 根據分類抓對應規格組合
		$categorys = $this->input->post('categorys');
		$product_sn = $this->input->post('product_sn');
		//var_dump($categorys);
		//exit();
		if($categorys){
			$Package=$this->libraries_model->_get_Package_by_cat($categorys,$product_sn);
			//var_dump($Package);
			//exit();
			echo json_encode($Package);
			//$this->db->stop_cache();
		}
	}
	public function AddPackage(){   //AJAX 寫入選取的規格組合
		$categorys = $this->input->post('categorys');
		$product_package_config_sn = $this->input->post('product_package_config_sn');
		$product_sn = $this->input->post('product_sn');
		$fixed_price = $this->input->post('fixed_price');
		$price = $this->input->post('price');
		$status = $this->input->post('status');
		if($product_sn && $product_package_config_sn){
			if($fixed_price){
				$data['fixed_price']=$fixed_price;
			}
			if($price){
				$data['price']=$price;
			}
			$data['status']=$status;
		  $data = $this->libraries_model->CheckUpdate($data,0);
			$where = array('product_sn'=>$product_sn,'product_package_config_sn' => $product_package_config_sn);
			//var_dump($where);
			if($this->libraries_model->_select('ij_product_fix_price_log',$where,0,0,0,'product_sn',0,'num_rows')){
				$this->libraries_model->_update('ij_product_fix_price_log',$data,$where);
			}else{
				$this->libraries_model->_insert('ij_product_fix_price_log',$data+$where);
			}
			$Package=$this->libraries_model->_get_Package_by_cat($categorys,$product_sn);
			echo json_encode($Package);
			//$this->db->stop_cache();
		}
	}
	/**
	 * 評價列表 Template
	 *
	 */
	public function comment()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_product/assess/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_product/assess/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 評價編輯 Template
	 *
	 */
	public function commentItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_product/assess/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_product/assess/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 商品列表 Template
	 *
	 */
	public function productTable($type='囍市集')
	{
		//echo $this->db->last_query();exit;
		/*$images=$this->db->query("select product_gallery_sn,image_path from ij_product_gallery where image_path not like 'product%'")->result_array();
		foreach($images as $_item){
            $this->libraries_model->_update('ij_product_gallery ',array('image_path'=>'product/'.$_item['image_path']),'product_gallery_sn',$_item['product_gallery_sn']);
        }*/
        if($this->input->post()){
        	$product_sn=$this->input->post('item_id');
        	$mode=$this->input->post('mode');
        	$value=$this->input->post('new_value');
        	//var_dump($mode);
        	$_data=array($mode=>$value);
			$_data = $this->libraries_model->CheckUpdate($_data);
			//舊主分類
			$default_root_category_sn=$this->libraries_model->_get_product($product_sn,'default_root_category_sn')['default_root_category_sn'];
			//刪除舊主分類的上層擴展分類
			if($default_root_category_sn!=$value){
				$this->libraries_model->del_up_categorys($product_sn,$default_root_category_sn);
			}
			if($this->libraries_model->_update('ij_product',$_data,'product_sn',$product_sn)){
				/*if($category_array=$this->libraries_model->get_up_categorys($value)){
					foreach($category_array as $value){
						if($product_category_relation_sn=$this->libraries_model->_select('ij_product_category_relation',array('product_sn'=>$product_sn,'category_sn'=>$value),0,0,0,'product_category_relation_sn',0,'row','product_category_relation_sn',0,0,0)['product_category_relation_sn']){
							$data_ca['status']='1';
						  //$data = $this->libraries_model->CheckUpdate($data,0);
							$this->libraries_model->_update('ij_product_category_relation',$data_ca,'product_category_relation_sn',$product_category_relation_sn);
						}else{
							$data_ca['category_sn']=$value;
							$data_ca['product_sn']=$product_sn;
							$data_ca['status']='1';
						    $data_ca = $this->libraries_model->CheckUpdate($data_ca,1);
							$this->libraries_model->_insert('ij_product_category_relation',$data_ca);
						}
					}
				}*/
				echo json_encode(true);
			}else{
				echo json_encode(false);
			}
			exit;
        }
        $search=$this->input->get('search');
		$type = urldecode($type);
		$page_data = array();
		$this->Check_page_permission('search','查詢');
		//if(stripos($this->ijw->check_permission(1),'search')===false){
		//	var_dump(stripos($this->ijw->check_permission(1),'search'));
		//}
		// 內容設定 ij_product

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$Items=$this->libraries_model->_get_products($type,$search);
		$page_data['page_content']['Items'] = $Items;
		$page_data['page_content']['suppliers'] = $this->libraries_model->_select('ij_supplier','supplier_status','1',0,0,'supplier_name',0,'list','supplier_sn,supplier_name');

        if($this->session->userdata('member_role_system_rules')[0]['role_name']=='供應商管理員'){
        	$_where=array('display_flag'=>'1','sort_order<='=>'2');
        }else if($this->session->userdata('member_role_system_rules')[0]['role_name']=='經銷商管理員'){
        	$_where=array('display_flag'=>'1','sort_order>='=>'3');
        }else{
        	$_where=array('display_flag'=>'1');
        }
    	$page_data['page_content']['product_status'] = $this->libraries_model->_select('ij_product_status_ct',$_where,0,0,0,'sort_order',0,'list','product_status,product_status_name');
    	$page_data['page_content']['all_product_status'] = $this->libraries_model->_select('ij_product_status_ct',0,0,0,0,'sort_order',0,'list','product_status,product_status_name');
    	$page_data['page_content']['promotion_labels'] = $this->libraries_model->_select('ij_promotion_label_config',0,0,0,0,'sort_order',0,'list','promotion_label_sn,promotion_label_name');

		$page_data['page_content']['ij_categorys'] =$this->libraries_model->categoryParentChildTree($type);
		//var_dump($page_data['page_content']['ij_categorys']);
		$page_data['page_content']['search'] =$search;
		if($type=="婚禮網站"){
			$page_data['page_content']['view_path'] = 'admin/page_product/list/ewedding'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'admin/page_product/list/ewedding_js'; // 頁面主內容的 JS view 位置, 必要
		}elseif($type=="婚宴管理"){
			$page_data['page_content']['view_path'] = 'admin/page_product/list/checkIn'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'admin/page_product/list/checkIn_js'; // 頁面主內容的 JS view 位置, 必要
		}else{
			$page_data['page_content']['view_path'] = 'admin/page_product/list/store'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'admin/page_product/list/store_js'; // 頁面主內容的 JS view 位置, 必要
		}
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 商品編輯 Template
	 *
	 */
	public function productItem($product_sn=0,$type='囍市集')
	{
		$this->Check_page_permission('search','查詢');
		$type = urldecode($type);
		//寫入資料庫
		if($data_array=$this->input->post('Item')){
			$this->Check_page_permission('edit','修改');
			if(!$product_sn=$this->input->post('product_sn')){ //只會有修改
				//$data_array['sort_order'] = $this->ijw->nf_to_wf($data_array['sort_order']); //轉半形
				//$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				//$product_sn=$this->libraries_model->_insert('ij_product',$data_array);
				//$this->session->set_userdata(array('Mes'=>'查無商品編號，資料無異動!'));
			}else{
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				$data_array['sort_order'] = $this->ijw->nf_to_wf($data_array['sort_order']); //轉半形
				//echo count($this->input->post('target_buyer[]'));
				/*if(count($this->input->post('target_buyer[]'))==2){
					$data_array['target_buyer']='全部';
				}else{
					$data_array['target_buyer']=$this->input->post('target_buyer[]')[0];
				}*/
				$min_price=$this->libraries_model->_get_min_price($product_sn,$data_array['pricing_method']); //一併更新最低價欄位
				//var_dump($min_price);
				//exit();
				if($min_price > 0){
					$data_array['min_price']=$min_price;
				}
				//var_dump($data_array);
				//exit();
				//付款方式
				if($this->input->post('payment[]')){
					$data_array['associated_payment_method']=implode(':',$this->input->post('payment[]'));
				}
				//運送方式
				if($this->input->post('deliverys[]')){
					$data_array['associated_delivery_method']=implode(':',$this->input->post('deliverys[]'));
				}
				//舊主分類
				$default_root_category_sn=$this->libraries_model->_get_product($product_sn,'default_root_category_sn')['default_root_category_sn'];

        		//加入供應商、經銷商機制
	            switch(@$this->session->userdata('member_role_system_rules')[0]['role_name']){
		            case '經銷商管理員':
	    				//unset($data_array['post_flag']);
		            	$_data['relation_status']=$data_array['product_status'];
						$this->libraries_model->_update('ij_product_relation',$_data,array('product_sn'=>$product_sn,'dealer_sn'=>$this->session->userdata['member_data']['associated_dealer_sn']));
		            break;
		            case '供應商管理員':
						if(!$this->input->post('supplier_sn')){ //沒有才更新
							$data_array['supplier_sn']=$this->session->userdata['member_data']['associated_supplier_sn'];
						}
						$this->libraries_model->_update('ij_product',$data_array,'product_sn',$product_sn);
		            break;
		            default:
						$data_array['supplier_sn']=$this->input->post('supplier_sn');
						$this->libraries_model->_update('ij_product',$data_array,'product_sn',$product_sn);
		            break;
	            }
	        if(@$this->session->userdata('member_role_system_rules')[0]['role_name']!='經銷商管理員'){
				//擴展分類(先把原本狀態都改2、判斷是否已有資料，狀態1，再刪除狀態2)
				$this->libraries_model->_update('ij_product_category_relation',array('status'=>'2'),'product_sn',$product_sn);

				$categorys=$this->input->post('categorys[]');
				/*if($category_array=$this->libraries_model->get_up_categorys(@$data_array['default_root_category_sn'])){
					if(is_array($categorys)){
						foreach($category_array as $_value){
							if(!in_array($_value,$categorys)){
								$categorys[]=$_value;
							}
						}
					}else{
						$categorys=$category_array;
					}
				}*/
				//var_dump($categorys);
				//exit();
				if($categorys){
					foreach($categorys as $key=>$ca){
						$data_ca=array();
						if($product_category_relation_sn=$this->libraries_model->_select('ij_product_category_relation',array('product_sn'=>$product_sn,'category_sn'=>$ca),0,0,0,'product_category_relation_sn',0,'row','product_category_relation_sn',0,0,0)['product_category_relation_sn']){
							$data_ca['status']='1';
						  //$data = $this->libraries_model->CheckUpdate($data,0);
							$this->libraries_model->_update('ij_product_category_relation',$data_ca,'product_category_relation_sn',$product_category_relation_sn);
						}else{
							$data_ca[$key]['category_sn']=$ca;
							$data_ca[$key]['product_sn']=$product_sn;
							$data_ca[$key]['status']='1';
						    $data_ca[$key] = $this->libraries_model->CheckUpdate($data_ca[$key],1);
							$this->libraries_model->_insert('ij_product_category_relation',$data_ca[$key]);
						}

					}
				}
				//刪除狀態2
				$this->libraries_model->_delete('ij_product_category_relation',array('status'=>'2','product_sn'=>$product_sn));
				//刪除舊主分類的上層擴展分類
				if(@$data_array['default_root_category_sn'] && $default_root_category_sn!=@$data_array['default_root_category_sn']){
					$this->libraries_model->del_up_categorys($product_sn,$default_root_category_sn);
				}
				//分類屬性
				$attribute=$this->input->post('attribute');
				//echo count($attribute);
				if($attribute){
					foreach($attribute as $key=>$attr){
						//$data[$key]['attribute_sn']=$attr['attribute_sn'];
						//$data[$key]['attribute_sn']=$attr['attribute_sn'];
						//$data[$key]['product_sn']=$product_sn;
						if(@$attr['status']){
							$data2[$key]['status']=$attr['status'];
						}else{
							$data2[$key]['status']='0';
						}
					    $data2[$key] = $this->libraries_model->CheckUpdate($data2[$key],0);
						$where = array('product_sn' => $product_sn,'attribute_sn' => $attr['attribute_sn'],'attribute_value_sn' => $attr['attribute_value_sn']);
						$this->libraries_model->_update('ij_product_attribute_log',$data2[$key],$where);
					}
				}
				//商品圖
				$gallery=$this->input->post('gallery');
				if($gallery){
					foreach($gallery as $key=>$ga){
						if(!is_dir(ADMINUPLOADPATH.'product/'.$product_sn.'/')) mkdir(ADMINUPLOADPATH.'product/'.$product_sn,0777);
		          $config['upload_path']  = ADMINUPLOADPATH.'product/'.$product_sn.'/' ;
		          $config['allowed_types']= 'gif|jpg|png|jpeg';
		          $config['max_size']     = '4196';
		          //$config['max_width']    = '1000';
		          //$config['max_height']   = '900';
		          $config['encrypt_name']   = true;
		          $this->load->library('upload', $config);
	        	if (!empty($_FILES['image'.$key]['name'])) {
		            if (!$this->upload->do_upload('image'.$key)) {
										//$this->session->set_userdata(array('Mes'=>$this->session->userdata('Mes').' '.strip_tags($this->upload->display_errors())));
										$Mes = strip_tags($this->upload->display_errors());
										//echo $this->upload->display_errors();
								}else{
		                $img	 = $this->upload->data();
		        				if(!$ga['image_alt']) $ga['image_alt']= $data_array['product_name'];
		                $data=array(
		                	'product_sn'  =>  $product_sn,
		               	 	'image_path'  =>  'product/'.$product_sn.'/'.$img['file_name'],
		                	'image_alt'   =>  $ga['image_alt'],
		                	'status'      =>  '1',
		                	'sort_order'  =>  $ga['sort_order']
		                );
		                //var_dump($data);
					  				$data = $this->libraries_model->CheckUpdate($data,1);
										$this->libraries_model->_insert('ij_product_gallery',$data);
										//var_dump($config['upload_path']);
		                //if(is_file(ADMINUPLOADPATH.$banner_save_dir)) unlink(ADMINUPLOADPATH.$banner_save_dir);
		            }
		        }else{
		        	if(@$ga['product_gallery_sn']){
		        		if(!$ga['image_alt']) $ga['image_alt']= $data_array['product_name'];
		            $data=array(
		            	'image_alt'   =>  $ga['image_alt'],
		            	'sort_order'  =>  $ga['sort_order']
		            );
								$this->libraries_model->_update('ij_product_gallery',$data,'product_gallery_sn',$ga['product_gallery_sn']);
							}
		        }
		      }
	    	}
	      //促銷標籤
				$labels=$this->input->post('labels[]');
				if($labels){
					//先把status都改NULL
					$this->libraries_model->_update('ij_product_promotion_label_relation',array('status'=>NULL),'product_sn',$product_sn);
					foreach($labels as $key=>$label){
						$data3[$key]['promotion_label_sn']=$label['promotion_label_sn'];
						$data3[$key]['product_sn']=$product_sn;
						$data3[$key]['status']=@$label['status'];
						$data3[$key]['start_date']=$label['from'];
						$data3[$key]['end_date']=$label['to'];
						if(intval(@$label['discount_percentage'])){
							$data3[$key]['discount_percentage']=intval($label['discount_percentage'])/100;
						}
						if(@$label['discount_amount']){
							$data3[$key]['discount_amount']=$label['discount_amount'];
						}
						if($data3[$key]['status']){
							if(@$label['product_promotion_label_relation_sn']){  //update
							    $data3[$key] = $this->libraries_model->CheckUpdate($data3[$key],0);
								$this->libraries_model->_update('ij_product_promotion_label_relation',$data3[$key],'product_promotion_label_relation_sn',@$label['product_promotion_label_relation_sn']);
							}else{
							    $data3[$key] = $this->libraries_model->CheckUpdate($data3[$key],1);
								$this->libraries_model->_insert('ij_product_promotion_label_relation',$data3[$key]);
							}
						}
					}
					$this->libraries_model->_delete('ij_product_promotion_label_relation','status',NULL);
					//echo $this->db->last_query();exit;
				}

	      //特殊規格-單一售價
				if($data_array['pricing_method']=='1'){ //單一
					$Packages=$this->input->post('Package[]');
					if($Packages){
						foreach($Packages as $key=>$Package){
							//$data[$key]['product_package_config_sn']=$Package['product_package_config_sn'];
							//$data[$key]['product_sn']=$product_sn;
							$fix_price[$key]['status']=$Package['status'];
							$fix_price[$key]['fixed_price']=$Package['fixed_price'];
							$fix_price[$key]['price']=$Package['price'];
						    $fix_price[$key] = $this->libraries_model->CheckUpdate($fix_price[$key],0);
							$this->libraries_model->_update('ij_product_fix_price_log',$fix_price[$key],'product_fix_price_sn',@$Package['product_fix_price_sn']);
						}
					}
					$fix_price=$this->input->post('fix_price[]');
					if($fix_price){
						//如果單一售價有一筆以上先刪除再新增避免價格出錯
						if($this->libraries_model->_select('ij_product_fix_price_log','product_sn',$product_sn,0,0,'product_sn',0,'num_rows') > 1){
							$this->libraries_model->_delete('ij_product_fix_price_log','product_sn',$product_sn);
							$fix_price['product_fix_price_sn']=''; //清空$fix_price['product_fix_price_sn']變新增
						}
						$fix_price['product_sn']=$product_sn;
						$fix_price['status']='1';
						if($fix_price['product_fix_price_sn']){ //修改
							  $fix_price = $this->libraries_model->CheckUpdate($fix_price,0);
								$this->libraries_model->_update('ij_product_fix_price_log',$fix_price,'product_fix_price_sn',@$fix_price['product_fix_price_sn']);
						}else{ //新增
							$fix_price = $this->libraries_model->CheckUpdate($fix_price,1);
					//var_dump($fix_price);
							$this->libraries_model->_insert('ij_product_fix_price_log',$fix_price);
					//exit();
						}
					}
				}else{
					//區間
					$PriceTiers=$this->input->post('PriceTier[]');
					$interval_value = $this->input->post('interval_value');
					$flexible_interval_flag = $this->input->post('flexible_interval_flag');
					//echo count($PriceTiers);
					//var_dump($PriceTiers);

					//先刪除再新增（避免數量錯亂
				  $this->libraries_model->_delete('ij_product_price_tier_log','product_sn',$product_sn);
					foreach($PriceTiers as $key=>$PriceTier){
						$Price_Tier[$key]['status']='1';
						$Price_Tier[$key]['product_sn']=$product_sn;
						$Price_Tier[$key]['interval_value']=$interval_value;
						$Price_Tier[$key]['flexible_interval_flag']=$flexible_interval_flag;
						$Price_Tier[$key]['start_qty']=$PriceTier['start_qty'];
						$Price_Tier[$key]['end_qty']=$PriceTier['end_qty'];
						$Price_Tier[$key]['price']=$PriceTier['price'];
						$Price_Tier[$key]['sort_order']=$PriceTier['start_qty'];
					    $Price_Tier[$key] = $this->libraries_model->CheckUpdate($Price_Tier[$key],1);
					    if(@$PriceTier['price']){ //有單價才新增
					  		//echo $PriceTier['price'].'<br>';
							$this->libraries_model->_insert('ij_product_price_tier_log',$Price_Tier[$key]);
						}
					}
				}
				if($data_array['unispec_flag']=='0'){ //多重
					//多重規格庫存設定
					$Mutispecs=$this->input->post('Mutispec[]');
					//var_dump($PriceTiers);

					foreach($Mutispecs as $key=>$Mutispec){
						$Mutispecs[$key]['status']='1';
						$Mutispecs[$key]['product_sn']=$product_sn;
						$Mutispecs[$key]['color_name']=$Mutispec['color_name'];
						$Mutispecs[$key]['warehouse_num']=$Mutispec['warehouse_num'];
						$Mutispecs[$key]['safe_qty_in_stock']=$Mutispec['safe_qty_in_stock'];
						$sort_order = ($key==0)?'10':$key+1*10;
						$Mutispecs[$key]['sort_order']=$sort_order;
						if(@$Mutispec['mutispec_stock_sn']){  //判斷新增或更新
						  if($Mutispec['color_name']){ //有名稱才更新否則刪除
						  	$Mutispecs[$key] = $this->libraries_model->CheckUpdate($Mutispecs[$key],1);
								$this->libraries_model->_update('ij_mutispec_stock_log',$Mutispecs[$key],'mutispec_stock_sn',$Mutispec['mutispec_stock_sn']);
							}else{
				  			$this->libraries_model->_delete('ij_mutispec_stock_log','mutispec_stock_sn',$Mutispec['mutispec_stock_sn']);
							}
						}else{
						  if($Mutispec['color_name']){ //有名稱才新增
								$Mutispecs[$key]['qty_in_stock']='0'; //新增預設0
						  		$Mutispecs[$key] = $this->libraries_model->CheckUpdate($Mutispecs[$key],1);
								$this->libraries_model->_insert('ij_mutispec_stock_log',$Mutispecs[$key]);
							}
						}
					}
				}

					//版型-顏色
					$template_colors=$this->input->post('template_colors[]');

				if($template_colors){
					//先刪除再新增
				    $this->libraries_model->_delete('ij_product_color_relation','product_sn',$product_sn);
					foreach($template_colors as $key=>$template_color){
						$template_color[$key]['status']='1';
						$template_color[$key]['product_sn']=$product_sn;
						$template_color[$key]['website_color_sn']=@$template_color['website_color_sn'];
						$template_color[$key]['sort_order']=$template_color['sort_order'];
					    $template_color[$key] = $this->libraries_model->CheckUpdate($template_color[$key],1);
						$this->libraries_model->_insert('ij_product_color_relation',$template_color[$key]);
					}
				}
			}
				//exit();
				if(@$Mes){
					$this->session->set_userdata(array('Mes'=>'商品修改成功! '.$Mes));
				}else{
					$this->session->set_flashdata("success_message","商品修改成功！");
					//$this->session->set_userdata(array('Mes'=>'商品修改成功!'));
				}
			}
			//redirect(base_url('admin/product/productTable/'.$type));
			//redirect(base_url('admin/product/productItem/'.$type.'/'.$product_sn));
			redirect(base_url('admin/product/productItem/'.$product_sn));
		}

		$page_data = array();
		// 內容設定 ij_product
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		$payments = $this->libraries_model->_select('ij_payment_method_ct','display_flag','1',0,0,0,0,'result_array');

		if($product_sn){
			$this->Check_page_permission('search','查詢');
			$page_data['page_content']['Item']=$this->libraries_model->_get_product($product_sn);
			//var_dump($page_data['page_content']['Item']);exit;
			if(!$page_data['page_content']['Item']){
				$this->session->set_userdata(array('Mes'=>'很抱歉！查無該商品資料，或該商品不屬於您! '.@$Mes));
				redirect(base_url('/admin/product/productTable/'));
			}
			if($page_data['page_content']['Item']['default_channel_name']!=$type){
				redirect(base_url('/admin/product/productItem/'.$page_data['page_content']['Item']['default_channel_name'].'/'.$product_sn));
			}
		}else{
			//新增預設
			$this->Check_page_permission('add','新增');
			$product_sn=$this->libraries_model->_add_product($type); //先給編號（因為很多表需要）
			$page_data['page_content']['Item']['product_sn']=$product_sn;
			$page_data['page_content']['Item']['product_status']='0'; //預設不發布
			$page_data['page_content']['Item']['unispec_qty_in_stock']='0'; //庫存預設0
			$page_data['page_content']['Item']['product_type']='1'; //預設實體
			$page_data['page_content']['Item']['target_buyer']='一般使用者'; //預設實體
			$page_data['page_content']['Item']['unispec_flag']='1'; //預設單一規格
			$page_data['page_content']['Item']['pricing_method']='1'; //預設單一售價
			//付款方式新增抓供應商設定
		  $associated_payment_method = $this->libraries_model->_select('ij_supplier','supplier_sn',$this->session->userdata['member_data']['associated_supplier_sn'],0,0,'supplier_sn',0,'row','associated_payment_method')['associated_payment_method'];

			if($associated_payment_method){
				$associated_payment_array=explode(':',$associated_payment_method);
			}else{
				$associated_payment_array=array();
			}
			//var_dump($associated_payment_array);
		  foreach($payments as $key=>$payment){
				if($associated_payment_array){
	  				if(in_array($payment['payment_method'],$associated_payment_array)){
						$payments[$key]['checked']='1';
					}else{
						$payments[$key]['checked']='0';
						unset($payments[$key]);
					}
				}else{
					if($payment['default_flag']=='1'){
						$payments[$key]['checked']='1';
					}else{
						$payments[$key]['checked']='0';
					}
				}
		  }

		  //$page_data['page_content']['category_attributes'] = $this->libraries_model->category_attributes();
		  //$page_data['page_content']['category_attributes'] = $this->libraries_model->_select('ij_attribute_config','status','1',0,0,0,0,'result_array');
		}
		  $page_data['page_content']['categorys']=form_dropdown('Item[default_root_category_sn]',array('0'=>'請先選擇館別'),'','id="Categorys"  class="form-control" size="10"');
		  $page_data['page_content']['ext_categorys']=form_dropdown('categorys[]',array('0'=>'請先選擇館別'),'','id="Ext_Categorys" multiple class="form-control" size="10"');
			if(@$page_data['page_content']['Item']['associated_payment_method']){
				$associated_payment_array=explode(':',$page_data['page_content']['Item']['associated_payment_method']);
			}else{
				$associated_payment_array=array();
			}
		  foreach($payments as $key=>$payment){
		  	if($associated_payment_array){
		  		foreach($associated_payment_array as $payment2){
		  			if($payment2==$payment['payment_method'] || @$payments[$key]['checked']=='1'){
		  			//var_dump($payment2);
		  			//var_dump($payment['payment_method']);
						$payments[$key]['checked']='1';
					}else{
						$payments[$key]['checked']='0';
					}
				}
			}else{
				//$payments[$key]['checked']='0';
			}
		  }
		//var_dump($payments);
		$page_data['page_content']['payments'] = $payments;
        if($this->session->userdata('member_role_system_rules')[0]['role_name']=='供應商管理員'){
		  $_where=array('display_flag'=>'1','supplier_sn'=>$this->session->userdata['member_data']['associated_supplier_sn']);
        }else{
		  $_where=array('display_flag'=>'1','supplier_sn'=>@$page_data['page_content']['Item']['supplier_sn']);
        }
		$deliverys = $this->libraries_model->_select('ij_delivery_method_ct',$_where,0,0,0,0,0,'result_array');
        if($this->session->userdata('member_role_system_rules')[0]['role_name']=='供應商管理員'){
		  foreach($deliverys as $key=>$delivery){
			$deliverys[$key]['checked']='1';
		  }
        }

		//echo $this->db->last_query();
		if(@$page_data['page_content']['Item']['associated_delivery_method']){
		  foreach($deliverys as $key=>$delivery){
				if(strrpos(@$page_data['page_content']['Item']['associated_delivery_method'],$delivery['delivery_method']) !== false){
					$deliverys[$key]['checked']='1';
				}else{
					$deliverys[$key]['checked']='0';
				}
		  }
		}
		  //var_dump($payments);
		  $page_data['page_content']['deliverys'] = $deliverys;
		  $page_data['page_content']['gallerys'] = $this->libraries_model->_select('ij_product_gallery','product_sn',$product_sn,0,0,0,0,'result_array');
		  $page_data['page_content']['addons'] = $this->libraries_model->_get_addon_list($product_sn);
		  //echo $product_sn;
		  $page_data['page_content']['labels'] = $this->libraries_model->_get_promotion_label_by_psn($product_sn);
		  $page_data['page_content']['price_tier'] = $this->libraries_model->_select('ij_product_price_tier_log','product_sn',$product_sn,0,0,0,0,'result_array');
		  $page_data['page_content']['mutispecs'] = $this->libraries_model->_select('ij_mutispec_stock_log','product_sn',$product_sn,0,0,0,0,'result_array');
		  $page_data['page_content']['templates'] = $this->libraries_model->_select('ij_product_template_config','status','1',0,0,0,0,'result_array');
		  $page_data['page_content']['suppliers'] = $this->libraries_model->_select('ij_supplier','supplier_status','1',0,0,'supplier_name',0,'list','supplier_sn,supplier_name');
        if($this->session->userdata('member_role_system_rules')[0]['role_name']=='供應商管理員'){
        	$_where=array('display_flag'=>'1','sort_order<='=>'2');
        }else if($this->session->userdata('member_role_system_rules')[0]['role_name']=='經銷商管理員'){
        	$_where=array('display_flag'=>'1','sort_order>='=>'3');
        }else{
        	$_where=array('display_flag'=>'1');
        }
    	  $page_data['page_content']['product_status'] = $this->libraries_model->_select('ij_product_status_ct',$_where,0,0,0,'sort_order',0,'list','product_status,product_status_name');
    	//var_dump($page_data['page_content']['product_status']);
    	$page_data['page_content']['all_product_status'] = $this->libraries_model->_select('ij_product_status_ct',0,0,0,0,'sort_order',0,'list','product_status,product_status_name');
    	//var_dump($page_data['page_content']['all_product_status']);

		  //var_dump($page_data['page_content']['suppliers']);
		if($type=="婚禮網站"){
			//列出版型與顏色
		  //$page_data['page_content']['templates_colors'] = $this->libraries_model->_get_templates_colors($product_sn);
			$page_data['page_content']['channel_name']='婚禮網站';
			$page_data['page_content']['view_path'] = 'admin/page_product/list/ewedding_item'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'admin/page_product/list/ewedding_item_js'; // 頁面主內容的 JS view 位置, 必要
		}elseif($type=="婚宴管理"){
			$page_data['page_content']['channel_name']='婚宴管理';
			$page_data['page_content']['view_path'] = 'admin/page_product/list/checkIn_item'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'admin/page_product/list/checkIn_item_js'; // 頁面主內容的 JS view 位置, 必要
		}else{
			$page_data['page_content']['channel_name']='囍市集';
			$page_data['page_content']['view_path'] = 'admin/page_product/list/store_item'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'admin/page_product/list/store_item_js'; // 頁面主內容的 JS view 位置, 必要
		}
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 商品分類屬性列表 Template
	 *
	 */
	public function productAttribute()
	{
		$page_data = array();

		// 內容設定
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$Items=$this->libraries_model->_select('ij_attribute_config','status !=','2',0,0,0,0,'result_array',0,0,0,0);
		$Items=$this->libraries_model->_AddAmount($Items,'ij_attribute_config'); //加入計算數量
		//var_dump($Items);
		$page_data['page_content']['Items'] = $Items;
		$page_data['page_content']['view_path'] = 'admin/page_product/attribute/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_product/attribute/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 商品分類屬性編輯 Template
	 *
	 */
	public function productAttributeItem($attribute_sn=0)
	{
		$page_data = array();

		//寫入資料庫
		if($data_array=$this->input->post('attribute')){
			if(!$attribute_sn=$this->input->post('attribute_sn')){ //判斷新增或修改
				$data_array['sort_order'] = $this->ijw->nf_to_wf($data_array['sort_order']); //轉半形
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				$attribute_sn=$this->libraries_model->_insert('ij_attribute_config',$data_array);
				$this->session->set_userdata(array('Mes'=>'分類屬性新增成功!'));
			}else{
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				$data_array['sort_order'] = $this->ijw->nf_to_wf($data_array['sort_order']); //轉半形
				$this->libraries_model->_update('ij_attribute_config',$data_array,'attribute_sn',$attribute_sn);
				$this->session->set_userdata(array('Mes'=>'分類屬性修改成功!'));
			}
			redirect(base_url('admin/product/productAttribute'));
		}


		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		if($attribute_sn){
			$page_data['page_content']['Item'] = $this->libraries_model->_select('ij_attribute_config','attribute_sn',$attribute_sn,0,0,0,0,'row');
		}
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content']['view_path'] = 'admin/page_product/attribute/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_product/attribute/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 商品分類屬性值列表 Template
	 *
	 */
	public function productAttributeValue($attribute_sn=0)
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		if($attribute_sn){
			$page_data['page_content']['current']=$this->libraries_model->_select('ij_attribute_config','attribute_sn',$attribute_sn,0,0,0,0,'row');
		}
		$where = array('attribute_sn' => $attribute_sn);
		$Items=$this->libraries_model->_select('ij_attribute_value_config',$where,0,0,0,0,0,'result_array',0,0,0,0);
		$Items=$this->libraries_model->_AddAmount($Items,'ij_attribute_value_config'); //加入計算數量
		//var_dump($CategoryItems);
		$page_data['page_content']['Items'] = $Items;

		$page_data['page_content']['view_path'] = 'admin/page_product/attribute/value'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_product/attribute/value_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 商品分類屬性值編輯 Template
	 *
	 */
	public function productAttributeValueItem($attribute_value_sn=0,$attribute_sn=0)
	{
		$page_data = array();

		//寫入資料庫
		if($data_array=$this->input->post('attribute_value')){
			//$attribute_sn=$this->input->post('attribute_sn');
			if(!$attribute_value_sn=$this->input->post('attribute_value_sn')){ //判斷新增或修改
				$data_array['sort_order'] = $this->ijw->nf_to_wf($data_array['sort_order']); //轉半形
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				$attribute_value_sn=$this->libraries_model->_insert('ij_attribute_value_config',$data_array);
				$this->session->set_userdata(array('Mes'=>'分類屬性值新增成功!'));
			}else{
				$data_array['sort_order'] = $this->ijw->nf_to_wf($data_array['sort_order']); //轉半形
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				$this->libraries_model->_update('ij_attribute_value_config',$data_array,'attribute_value_sn',$attribute_value_sn);
				$this->session->set_userdata(array('Mes'=>'分類屬性值修改成功!'));
			}
			redirect(base_url('admin/product/productAttributeValue/'.$data_array['attribute_sn']));
		}


		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		if($attribute_value_sn){
			$page_data['page_content']['Item'] = $this->libraries_model->_select('ij_attribute_value_config','attribute_value_sn',$attribute_value_sn,0,0,0,0,'row');
			$attribute_sn = $page_data['page_content']['Item']['attribute_sn'];
		}
		$page_data['page_content']['Attribute'] = $this->libraries_model->_select('ij_attribute_config','attribute_sn',$attribute_sn,0,0,0,0,'row');
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content']['view_path'] = 'admin/page_product/attribute/value_item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_product/attribute/value_item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 庫存管理 Template
	 *
	 */
	public function inventory($type='table',$purchase_sales_stock_sn=0)
	{
		// 內容設定 ij_purchase_sales_stock_log
		//寫入資料庫
		if($data_array=$this->input->post('Item')){
			if(!$purchase_sales_stock_sn=$this->input->post('purchase_sales_stock_sn')){ //新增
				$this->Check_page_permission('add','新增');
				if($data_array['purchase_sales_stock_type']=='2'){ //報廢加上-
					$data_array['actua_spec_option_amount']= -$data_array['actua_spec_option_amount'];
					$kind='報廢';
				}else{
					$kind='入庫';
				}
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);//方便秀最後更新
				$purchase_sales_stock_sn=$this->libraries_model->_insert('ij_purchase_sales_stock_log',$data_array);
				if($data_array['actua_spec_option_amount']!=$this->input->post('old_amount')){ //是否有異動數量
					$chg_amount = intval($data_array['actua_spec_option_amount']);
					if($chg_amount > 0 ) $chg_amount='+'.$chg_amount; //增加 +號
					if($this->input->post('unispec_flag')){ //單一規格
						if(!$this->input->post('unispec_qty_in_stock')){ //原本是null 先變0
							$product= array('unispec_qty_in_stock'=>0);
						}else{
							$product= array();
						}
				    $product = $this->libraries_model->CheckUpdate($product,0);
						$this->libraries_model->_update('ij_product',$product,'product_sn',$data_array['product_sn']);
						$this->libraries_model->_update_field_amount('ij_product','product_sn',$data_array['product_sn'],'unispec_qty_in_stock',$chg_amount);
						//echo $this->db->last_query();
						//exit();
					}else{
						$product= array();
				    	$product = $this->libraries_model->CheckUpdate($product,0);
						$this->libraries_model->_update('ij_mutispec_stock_log',$product,'mutispec_stock_sn',$data_array['associated_mutispec_stock_sn']);
						$this->libraries_model->_update_field_amount('ij_mutispec_stock_log','mutispec_stock_sn',$data_array['associated_mutispec_stock_sn'],'qty_in_stock',$chg_amount);
					}
				}
				$this->session->set_userdata(array('Mes'=>$kind.'新增成功!'));
			}else{
				$this->Check_page_permission('edit','修改');
				if($data_array['purchase_sales_stock_type']=='2'){ //報廢加上-
					$data_array['actua_spec_option_amount']= -$data_array['actua_spec_option_amount'];
					$kind='報廢';
				}else{
					$kind='入庫';
				}
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				$this->libraries_model->_update('ij_purchase_sales_stock_log',$data_array,'purchase_sales_stock_sn',$purchase_sales_stock_sn);
				if($data_array['actua_spec_option_amount']!=$this->input->post('old_amount')){ //是否有異動數量
					if($this->input->post('purchase_sales_stock_type')==$data_array['purchase_sales_stock_type']){ //判斷類型無異動才做修改數量異動
						$chg_amount = intval($data_array['actua_spec_option_amount'])-intval($this->input->post('old_amount'));
					}else{
						$chg_amount = intval($data_array['actua_spec_option_amount']);
					}
					if($chg_amount > 0 ) $chg_amount='+'.$chg_amount; //增加 +號
					if($this->input->post('unispec_flag')){ //單一規格
						if(!$this->input->post('unispec_qty_in_stock')){ //原本是null 先變0
							$product= array('unispec_qty_in_stock'=>0);
						}else{
							$product= array();
						}
				    $product = $this->libraries_model->CheckUpdate($product,0);
						$this->libraries_model->_update('ij_product',$product,'product_sn',$data_array['product_sn']);
						$this->libraries_model->_update_field_amount('ij_product','product_sn',$data_array['product_sn'],'unispec_qty_in_stock',$chg_amount);
					}else{
						if($this->input->post('associated_mutispec_stock_sn')==$data_array['associated_mutispec_stock_sn']){ //判斷多重規格是否有異動
							$product= array();
					    $product = $this->libraries_model->CheckUpdate($product,0);
							$this->libraries_model->_update('ij_mutispec_stock_log',$product,'mutispec_stock_sn',$data_array['associated_mutispec_stock_sn']);
							$this->libraries_model->_update_field_amount('ij_mutispec_stock_log','mutispec_stock_sn',$data_array['associated_mutispec_stock_sn'],'qty_in_stock',$chg_amount);
			//echo $this->db->last_query();
			//exit();
						}else{
							//原規格-異動庫存
							$product= array();
					    $product = $this->libraries_model->CheckUpdate($product,0);
							$this->libraries_model->_update('ij_mutispec_stock_log',$product,'mutispec_stock_sn',$this->input->post('associated_mutispec_stock_sn'));
							if(-$chg_amount > 0 ) $chg_amount='+'.-$chg_amount; //增加 +號
							$this->libraries_model->_update_field_amount('ij_mutispec_stock_log','mutispec_stock_sn',$this->input->post('associated_mutispec_stock_sn'),'qty_in_stock',-$chg_amount);
							//新規格-異動庫存
							$product= array();
					    $product = $this->libraries_model->CheckUpdate($product,0);
							$this->libraries_model->_update('ij_mutispec_stock_log',$product,'mutispec_stock_sn',$data_array['associated_mutispec_stock_sn']);
					    if($chg_amount > 0 ) $chg_amount='+'.$chg_amount; //增加 +號
							$this->libraries_model->_update_field_amount('ij_mutispec_stock_log','mutispec_stock_sn',$data_array['associated_mutispec_stock_sn'],'qty_in_stock',$chg_amount);
						}
					}
				}else{
					if($this->input->post('unispec_flag')=='0'){ //多重規格
						if($this->input->post('associated_mutispec_stock_sn')!=$data_array['associated_mutispec_stock_sn']){ //判斷多重規格有異動才異動庫存
							$chg_amount = intval($data_array['actua_spec_option_amount']);
					    if($chg_amount > 0 ) $chg_amount='+'.$chg_amount; //增加 +號
							//原規格-異動庫存
							$product= array();
					    $product = $this->libraries_model->CheckUpdate($product,0);
							$this->libraries_model->_update('ij_mutispec_stock_log',$product,'mutispec_stock_sn',$this->input->post('associated_mutispec_stock_sn'));
							$chg_amount1=-$chg_amount;
					    if($chg_amount1 > 0 ) $chg_amount1='+'.$chg_amount1; //增加 +號
							$this->libraries_model->_update_field_amount('ij_mutispec_stock_log','mutispec_stock_sn',$this->input->post('associated_mutispec_stock_sn'),'qty_in_stock',$chg_amount1);
							//新規格-異動庫存
							$product= array();
					    $product = $this->libraries_model->CheckUpdate($product,0);
							$this->libraries_model->_update('ij_mutispec_stock_log',$product,'mutispec_stock_sn',$data_array['associated_mutispec_stock_sn']);
							$this->libraries_model->_update_field_amount('ij_mutispec_stock_log','mutispec_stock_sn',$data_array['associated_mutispec_stock_sn'],'qty_in_stock',$chg_amount);
						}
					}
				}
				$this->session->set_userdata(array('Mes'=>$kind.'修改成功!'));
			}
			redirect(base_url('admin/product/inventory/'));
		}

		$flg=$this->input->get('flg',true);
		if($flg=='reset'){
			redirect(base_url('admin/product/inventory'));
			exit();
		}else{
			//if(!$product_sn){
			$product_sn=$this->input->get('product_sn',true);
			//}
			$purchase_sales_stock_type=$this->input->get('stock_type',true);
			$page=$this->input->get('page',true);
		}
		$page_data = array();
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['product_sn'] = $product_sn;
		$page_data['page_content']['stock_type'] = $purchase_sales_stock_type;
    	$this->load->library('pagination');
		$page_data['page_content']['perpage'] = 10;
		$page_data['page_content']['total'] = count($this->libraries_model->_get_Inventory_list(@$product_sn,@$purchase_sales_stock_type));
	    $config = $this->ijw->admin_pagination('admin/product/inventory', $page_data['page_content']['total'],$page_data['page_content']['perpage']);
	    $this->pagination->initialize($config);

		$Items=$this->libraries_model->_get_Inventory_list(@$product_sn,@$purchase_sales_stock_type,$page,$page_data['page_content']['perpage']);
		$page_data['page_content']['Items'] = $Items;
    	$page_data['page_content']['page_links'] = $this->pagination->create_links();

		if($type=="table"){
			$page_data['page_content']['view_path'] = 'admin/page_product/inventory/table'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'admin/page_product/inventory/table_js'; // 頁面主內容的 JS view 位置, 必要
		}elseif($type=="log"){
			$page_data['page_content']['view_path'] = 'admin/page_product/inventory/log'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'admin/page_product/inventory/log_js'; // 頁面主內容的 JS view 位置, 必要
		}else{
			if(@$product_sn){
				if(!$Item=$this->libraries_model->_get_Inventory_item_by_product_sn(@$product_sn)){
					$this->session->set_userdata(array('Mes'=>'很抱歉！該編號查無商品，或該商品並非屬於您供應，敬請重新確認是否正確。'));
					redirect(base_url('admin/product/inventory/'));
				}
			}elseif(@$purchase_sales_stock_sn){
				$Item=$this->libraries_model->_get_Inventory_item(@$purchase_sales_stock_sn);
			}else{
				$this->session->set_userdata(array('Mes'=>'查無資料!'));
				redirect(base_url('admin/product/inventory/'));
			}
			if(!$purchase_sales_stock_sn){
				$this->Check_page_permission('add','新增');
			}else{
				$this->Check_page_permission('search','查詢');
			}
			if($Item['unispec_flag']=='0'){ //多重規格
				$page_data['page_content']['mutispec']=$this->libraries_model->_select('ij_mutispec_stock_log','product_sn',$Item['product_sn'],0,0,0,0,'result_array','mutispec_stock_sn,color_name');
			}
			//var_dump($page_data['page_content']['mutispec']);
			$page_data['page_content']['Item'] = $Item;
			$page_data['page_content']['view_path'] = 'admin/page_product/inventory/item'; // 頁面主內容 view 位置, 必要
			$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
			$page_data['page_level_js']['view_path'] = 'admin/page_product/inventory/item_js'; // 頁面主內容的 JS view 位置, 必要
		}
		$this->load->view('admin/general/main', $page_data);
	}
	/**
	 * 商品追蹤清單 Template
	 *
	 */
	public function wishList()
	{
		$flg=$this->input->get('flg',true);
		if($flg=='reset'){
			redirect(base_url('admin/product/wishList'));
			exit();
			/*$from='';
			$to='';
			$user_name='';
			$product_sn='';
			$product_name='';*/
		}else{
			$from=$this->input->get('from',true);
			$to=$this->input->get('to',true);
			$user_name=$this->input->get('user_name',true);
			$product_sn=$this->input->get('product_sn',true);
			$product_name=$this->input->get('product_name',true);
			$page=$this->input->get('page',true);
		}
		$page_data = array();
		// 內容設定 ij_track_list_log
				// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要 $start_date,$end_date
		//var_dump($CategoryItems);
		$page_data['page_content']['from'] = $from;
		$page_data['page_content']['to'] = $to;
		$page_data['page_content']['user_name'] = $user_name;
		$page_data['page_content']['product_sn'] = $product_sn;
		$page_data['page_content']['product_name'] = $product_name;
    $this->load->library('pagination');
		$page_data['page_content']['perpage'] = 10;
		$page_data['page_content']['total'] = count($this->libraries_model->_get_Track_list(@$from,@$to,@$user_name,@$product_sn,@$product_name));
    $config = $this->ijw->admin_pagination('admin/product/wishList', $page_data['page_content']['total'],$page_data['page_content']['perpage']);
    $this->pagination->initialize($config);

		$Items=$this->libraries_model->_get_Track_list(@$from,@$to,@$user_name,@$product_sn,@$product_name,$page,$page_data['page_content']['perpage']);
		$page_data['page_content']['Items'] = $Items;
    $page_data['page_content']['page_links'] = $this->pagination->create_links();
		$page_data['page_content']['view_path'] = 'admin/page_product/wish_list/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_product/wish_list/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}
	/**
	 * 商品顏色 Template
	 *
	 */
	public function productTemplate()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$Items=$this->libraries_model->_select('ij_product_template_config',0,0,0,0,0,0,'result_array');
		$page_data['page_content']['Items'] = $Items;
		$page_data['page_content']['view_path'] = 'admin/page_product/template/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_product/template/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 商品顏色 Template
	 *
	 */
	public function productTemplateItem($product_template_sn=0)
	{
		//寫入資料庫
		if($data_array=$this->input->post('Item')){
			if(!$product_template_sn=$this->input->post('product_template_sn')){ //不會有新增
				$data_array['sort_order'] = $this->ijw->nf_to_wf($data_array['sort_order']); //轉半形
				if($data_array['original_template_flag']){
					$data_array['upper_product_template_sn']='0'; //母版=0
				}
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				$attribute_sn=$this->libraries_model->_insert('ij_product_template_config',$data_array);
				$this->session->set_userdata(array('Mes'=>'版型設定新增成功!'));
			}else{
				if($data_array['original_template_flag']){
					$data_array['upper_product_template_sn']='0'; //母版=0
				}
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				$data_array['sort_order'] = $this->ijw->nf_to_wf($data_array['sort_order']); //轉半形
				$this->libraries_model->_update('ij_product_template_config',$data_array,'product_template_sn',$product_template_sn);
				$this->session->set_userdata(array('Mes'=>'版型設定修改成功!'));
			}
			redirect(base_url('admin/product/productTemplate'));
		}
		if($data_array=$this->input->post('picture')){
			//var_dump($data_array);
			$data_array['status']='1';
			$data_array = $this->libraries_model->CheckUpdate($data_array,1);
			$template_picture_sn=$this->libraries_model->_insert('ij_template_picture',$data_array);
      if (!empty($_FILES['photo']['name'])) {
					//$this->load->library('upload');
          $config['upload_path']  = ADMINUPLOADPATH.'template/' ;
          $config['allowed_types']= 'gif|jpg|png|jpeg';
          $config['max_size']     = '4196';
          $config['max_width']    = '1920';
          $config['max_height']   = '1080';
          $config['encrypt_name']   = true;

          $this->load->library('upload', $config);

					//$this->upload->initialize($config);
          if (!$this->upload->do_upload('photo')) {
							//$this->session->set_userdata(array('Mes'=>$this->session->userdata('Mes').' '.strip_tags($this->upload->display_errors())));
          		//var_dump($error);
          		$error['error'] = strip_tags($this->upload->display_errors());
							echo json_encode($error);
							exit();
					}else{
              $img	 = $this->upload->data();
							$this->libraries_model->_update('ij_template_picture',array('image_path'=>'template/'.$img['file_name']),'template_picture_sn',$template_picture_sn);
							$picture=$this->libraries_model->_Get_Template_Color($data_array['product_template_sn'],$data_array['associated_template_color_relation_sn']);
							echo json_encode($picture);
							exit();
							//var_dump($config['upload_path']);
              //if(is_file(ADMINUPLOADPATH.$banner_save_dir)) unlink(ADMINUPLOADPATH.$banner_save_dir);
          }
          //exit();
      }
		}

		if($data_array=$this->input->post('divider')){
			//var_dump($data_array);
			$data_array['status']='1';
			$data_array = $this->libraries_model->CheckUpdate($data_array,1);
			$template_divider_sn=$this->libraries_model->_insert('ij_template_divider_config',$data_array);
      if (!empty($_FILES['dividerphoto']['name'])) {
					//$this->load->library('upload');
          $config['upload_path']  = ADMINUPLOADPATH.'template/' ;
          $config['allowed_types']= 'gif|jpg|png|jpeg';
          $config['max_size']     = '4196';
          $config['max_width']    = '1920';
          $config['max_height']   = '1080';
          $config['encrypt_name']   = true;

          $this->load->library('upload', $config);

					//$this->upload->initialize($config);
          if (!$this->upload->do_upload('dividerphoto')) {
							//$this->session->set_userdata(array('Mes'=>$this->session->userdata('Mes').' '.strip_tags($this->upload->display_errors())));
          		//var_dump($error);
          		$error['error'] = strip_tags($this->upload->display_errors());
							echo json_encode($error);
							exit();
					}else{
              $img	 = $this->upload->data();
							$this->libraries_model->_update('ij_template_divider_config',array('default_divider_save_dir'=>'template/'.$img['file_name']),'template_divider_sn',$template_divider_sn);
							$divider=$this->libraries_model->_Get_Template_divider($data_array['product_template_sn']);
							echo json_encode($divider);
							exit();
							//var_dump($config['upload_path']);
              //if(is_file(ADMINUPLOADPATH.$banner_save_dir)) unlink(ADMINUPLOADPATH.$banner_save_dir);
          }
          //exit();
      }
		}
		$this->libraries_model->_Get_Template_divider($product_template_sn);
		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		if($product_template_sn){
			$page_data['page_content']['Item'] = $this->libraries_model->_select('ij_product_template_config','product_template_sn',$product_template_sn,0,0,0,0,'row');
		}else{
			//新增預設
			$product_template_sn=$this->libraries_model->_add_product_template(); //先給編號（因為顏色與圖片需要）
			$page_data['page_content']['Item'] = $this->libraries_model->_select('ij_product_template_config','product_template_sn',$product_template_sn,0,0,0,0,'row');
			//$page_data['page_content']['Item']['status']='1';
		}
		$page_data['page_content']['view_path'] = 'admin/page_product/template/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_product/template/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}
	/**
	 * 版型輪播圖 Template
	 *
	 */
	public function GetColor(){
		if($product_template_sn=$this->input->post('product_template_sn',true)){
			$picture=$this->libraries_model->_Get_Template_Color($product_template_sn);
			//var_dump($picture);
			if($product_sn=$this->input->post('product_sn',true)){
				$where = array('product_sn' => $product_sn,'status'=>'1');
				$website_color=$this->libraries_model->_select('ij_product_color_relation',$where,'',0,0,0,0,'result_array','website_color_sn');
				//找出已選
				foreach($picture as $key1=>$pi){
					foreach($website_color as $key2=>$wc){
						if($pi['website_color_sn']==$wc['website_color_sn']){
							$picture[$key1]['status']='1';
						}
					}
				}
			}
			//exit();
			echo json_encode($picture);
		}else{
			echo '';
		}
	}
	/**
	 * 版型分頁圖
	 *
	 */
	public function GetDivider(){
		if($product_template_sn=$this->input->post('product_template_sn',true)){
			$picture=$this->libraries_model->_Get_Template_divider($product_template_sn);
			echo json_encode($picture);
		}else{
			echo '';
		}
	}
	public function get_current_amount(){
		echo ($qty_in_stock=$this->libraries_model->_select('ij_mutispec_stock_log','mutispec_stock_sn',$this->input->post('mutispec_stock_sn',true),0,0,'qty_in_stock',0,'row','qty_in_stock')['qty_in_stock'])? $qty_in_stock:'0';
	}
	/**
	 * ajax check if value exist
	 *
	 */
	public function chk_if_exist(){
		$table=base64_decode($this->input->post('dtable'));
		$field=base64_decode($this->input->post('dfield'));
		$value=$this->input->post('dvalue');
		if($table && $field && $value){
		//var_dump($this->input->post());
			if($row=$this->libraries_model->_select($table,$field,$value,0,0,$field,0,'row',$field)){ //檢查是否有值
				echo $row[$field]; //有值
			}else{
				echo '0';
			}
			//echo $this->db->last_query();
		}else{
				echo '';
		}
	}
	public function getP_by_cat(){   //AJAX 根據根據分類抓對應商品
		$categorys = $this->input->post('categorys');
		$channel = $this->input->post('channel');
		if($categorys){
			//echo "<option value='0'>第一層</option>";
			//$this->db->start_cache();//快取10.14
			$this->load->model('Shop_model');
			$products=$this->Shop_model->_get_products($channel,0,$categorys,0,0,'name_ASC');
			//$where = array('channel_name'=>$channel_name,'upper_category_sn' => 0,'category_status' => 1);
			echo '<option value="">請選擇</option>';
			foreach($products as $crow){
				/*if($product_package_config_sn && $this->libraries_model->_select('ij_category_product_package_relation',array('product_package_config_sn' => $product_package_config_sn,'category_sn' => $crow['category_sn']),0,0,0,'category_sn',0,'row','category_sn')){
					$ifselect=' selected';
				}else{
					$ifselect='';
				}*/
				$ifselect='';
				echo "<option ".$ifselect." value='".$crow['product_sn']."'>".$crow['product_name']."</option>";
			}
			//$this->db->stop_cache();
		}
	}
	public function dealerCategory()
	{
		$page_data = array();
		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$this->libraries_model->get_dealer_categorys($this->session->userdata['member_data']['associated_dealer_sn']);
		$CategoryItems=$this->libraries_model->dealer_categoryParentChildTree($this->session->userdata['member_data']['associated_dealer_sn']);
		//var_dump(count($CategoryItems));

		$Store=$this->libraries_model->get_channels();
		$page_data['page_content']['Store']=form_dropdown('module[channel_name]',$Store,@$Item['channel_name'],'id="StoreName" class="form-control" ');

		$page_data['page_content']['CategoryItems'] = $CategoryItems;
		$page_data['page_content']['view_path'] = 'admin/page_product/category/dealercategory'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_product/category/dealercategory_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}
	/**
	 * 經銷商-分類編輯
	 *
	 */
	public function dealerCategoryItem($product_relation_sn=0)
	{
		if(!$product_relation_sn){
			$this->Check_page_permission('add','新增');
		}else{
			$this->Check_page_permission('search','查詢');
		}
		$page_data = array();
		//寫入資料庫
		if($data_array=$this->input->post('category')){
			if(!$product_relation_sn=$this->input->post('product_relation_sn')){ //判斷新增或修改
				//只會有修改
			}else{
				$this->load->library("ijw");
				$data_array['sort_order'] = $this->ijw->nf_to_wf($data_array['sort_order']); //轉半形
				//exit();
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				$this->libraries_model->_update('ij_category_relation',$data_array,'product_relation_sn',$product_relation_sn);
				$this->session->set_userdata(array('Mes'=>'分類上架修改成功!'));
			}
			redirect(base_url('admin/product/dealerCategory'));
		}


		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$channel_name='囍市集';
		if($product_relation_sn){
			$CategoryItem=$this->libraries_model->get_dealer_categorys($this->session->userdata['member_data']['associated_dealer_sn'],$product_relation_sn)[0];
			$page_data['page_content']['CategoryItem'] = $CategoryItem;
			$channel_name=$CategoryItem['channel_name'];
			$upper_category_sn=$CategoryItem['upper_category_sn'];
		    //$page_data['page_content']['category_attributes'] = $this->libraries_model->category_attributes($category_sn);
		}else{
			$this->session->set_userdata(array('Mes'=>'查無此分類編號!'));
			redirect(base_url('admin/product/dealerCategory'));
		}
//var_dump($page_data['page_content']['category_attributes']);
		$page_data['page_content']['categorys']=form_dropdown('category[upper_category_sn]',array('0'=>'請先選擇館別'),'','id="Categorys" class="form-control" size="5"');

		$Store=$this->libraries_model->get_channels();
		$page_data['page_content']['Store']=form_dropdown('category[channel_name]',$Store,@$channel_name,'id="StoreName" class="form-control" required onChange="ChgStoreName(this.value);"');
		$page_data['page_content']['channel_name']=@$channel_name;
		//$page_data['page_content']['upper_category_sn']=@$upper_category_sn;
		$page_data['page_content']['view_path'] = 'admin/page_product/category/dealer_item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_product/category/dealer_item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}
	public function goods(){
		//$goods=$this->libraries_model->_select('ntssi_goods',0,0,0,0,'gid',0,'result_array');
        $this->db->select('ntssi_goods.*,ntssi_bclass.catname');
        $this->db->from('ntssi_goods');
        $this->db->join('ntssi_bclass','ntssi_bclass.bid=ntssi_goods.bid');
        $this->db->limit(300);
        $this->db->order_by('gid','desc');
        $goods=$this->db->get()->result_array();
		//echo $this->db->last_query();
        //echo '<pre>' . var_export($goods, true) . '</pre>';exit;
		foreach($goods as $good){
	        $data=array();
			$data['default_channel_name']='囍市集';
			$data['product_status']='0'; //預設發布
			$data['unispec_qty_in_stock']='0'; //庫存預設0
			$data['product_type']='1'; //預設實體
			//$data['target_buyer']='一般使用者'; //預設實體
			$data['unispec_flag']='1'; //預設單一規格
			$data['pricing_method']='1'; //預設單一售價
			$data['supplier_sn']='1';
			$data['currency_code']='1';
			$data['free_delivery']='1';
			$data['open_preorder_flag']='1';
			$data['sort_order']='9999';
			$data['product_sn']=$good['gid'];
			$data['product_name']=$good['goodsname'];
			$data['special_item']=$good['intro'];
			$data['product_info']=$good['body'];
			$data['cost']=$good['cost'];
			$data['weight']=$good['weight'];
			$data['min_order_qt']=$good['buyunit'];
			$data['unit_name']=$good['unit'];
			$data['unispec_warehouse_num ']=$good['bn'];
			$data['meta_keywords']=$good['keywords'];
			$data['meta_title']=$good['goodsname'];
			$data['meta_content']=$good['intro'];
			$data['min_price']=$good['pricedesc'];
			$data['default_root_category_sn']=@$this->libraries_model->_select('ij_category','category_name',$good['catname'],0,0,'category_sn',0,'row','category_sn')['category_sn'];
		    $data = $this->libraries_model->CheckUpdate($data,0);
		    $data = $this->libraries_model->CheckUpdate($data,1);
			$this->libraries_model->_insert('ij_product',$data);
			$fix_price=array(
				'product_sn'=>$data['product_sn'],
				'fixed_price'=>$good['price'],
				'price'=>$good['pricedesc'],
				'status'=>'1',
			);
		    $fix_price = $this->libraries_model->CheckUpdate($fix_price,0);
		    $fix_price = $this->libraries_model->CheckUpdate($fix_price,1);
			$this->libraries_model->_insert('ij_product_fix_price_log',$fix_price);
			//if(!is_dir(ADMINUPLOADPATH.'product/'.$product_sn.'/')) mkdir(ADMINUPLOADPATH.'product/'.$product_sn,0777);
            $data=array(
            	'product_sn'  =>  $data['product_sn'],
           	 	'image_path'  =>  'product/'.$good['gimg'],
            	'image_alt'   =>  $good['goodsname'],
            	'status'      =>  '1',
            	'sort_order'  =>  '1'
            );
            //var_dump($data);
		  	$data = $this->libraries_model->CheckUpdate($data,1);
		  	$data = $this->libraries_model->CheckUpdate($data,0);
			$this->libraries_model->_insert('ij_product_gallery',$data);
			$pics=$this->libraries_model->_select('ntssi_good_pic','good_id',$data['product_sn'],0,0,'goodpic_id',0,'result_array');
	        //echo '<pre>' . var_export($goods[0], true) . '</pre>';
			$i=100;
			foreach($pics as $pic){
	            $data=array(
	            	'product_sn'  =>  $data['product_sn'],
	           	 	'image_path'  =>  'product/'.$pic['goodpic_name'],
	            	'image_alt'   =>  $pic['goodpic_content'],
	            	'status'      =>  '1',
	            	'sort_order'  =>  $i++
	            );
	            //var_dump($data);
			  	$data = $this->libraries_model->CheckUpdate($data,1);
			  	$data = $this->libraries_model->CheckUpdate($data,0);
				$this->libraries_model->_insert('ij_product_gallery',$data);
			}
			//exit;
		}

	}
}
