<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script src="public/metronic/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<script type="text/javascript" src="public/metronic/global/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script src="public/metronic/admin/pages/scripts/components-form-tools.js"></script>
<script src="public/metronic/admin/pages/scripts/portfolio.js"></script>

<script>
jQuery(document).ready(function() {
   	Metronic.init(); // init metronic core components
	  Layout.init(); // init current layout
    //ComponentsFormTools.init();
    $('.fancybox').fancybox();
    Portfolio.init();
		$(".deleteTemplateDivider").click(function(){
        var button = $(this);
        	var r = confirm('是否刪除此張代表圖檔?');
            if(r)
            {
						    $.post('/admin/admin/ajaxdel/', {dtable: '<?php echo base64_encode('ij_blog_article');?>',dfield:'<?php echo base64_encode('blog_article_sn');?>',dfilename:button.attr('blog_article_image_save_dir'),did:button.attr('ItemId')}, function(data){
                	if (data){
                		button.parent().remove();
                		//$('#TemplateItem'+button.attr('ItemId')).html('');
                	}else{
                		alert('刪除失敗');
                	}
						    });
            }
    });
<?php if(@$Item['post_category_sn_set']){?>
	ChgStoreName('<?php echo @$Item['post_channel_name_set'];?>','<?php echo @$Item['post_category_sn_set'];?>');
<?php }else{?>
	ChgStoreName('結婚商店','');
	//$('#StoreName').attr('disabled','disabled');
<?php }?>
		$("#Categorys").click(function(){
				//console.log($("#Categorys2 option[value='"+$(this).val()+"']").length);
				click_value=$(this).val();
				var exists = false; 
				$('#Categorys2 option').each(function(){
				//console.log(this.value);
				//console.log($(this).val());
				  if (this.value == click_value) {
				    exists = true;
				    this.remove();
				  }
				});
				//console.log(exists);
			if(exists){
				$("#Categorys2 option[value='"+click_value+"']").remove();
			}else{
				$('#Categorys2').append('<option selected>'+click_value+'</option>');
			}
		});
		CKEDITOR.replace( 'editor1', {
			height: 500
		} );
		<?if(@$Item['associated_dealer_sn'] && $this->session->userdata('member_role_system_rules')[0]['role_name']=='經銷商管理員' && @$Item['associated_dealer_sn']!=$this->session->userdata['member_data']['associated_dealer_sn']){?>
			$('input').attr('disabled',true);
			$('select').attr('disabled',true);
			$('textarea').attr('disabled',true);
			$('input[name="Item[post_flag]"]').attr('disabled',false);
			$('input[type="hidden"]').attr('disabled',false);
			$('.submit').attr('disabled',false);
		<?}?>
});
		function ChgStoreName(C,D){ //變動館別、帶入值
			if(!D) D='';
			$('#Categorys option').remove();$('#Categorys').append('<option>Loading...</option>');
			$.get('/admin/admin/ChgStoreName/'+C+'/'+D,function(data){$('#Categorys option').remove();$('#Categorys').append(data);});;
		}

</script>

