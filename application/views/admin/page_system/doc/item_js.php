<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script src="public/metronic/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<script type="text/javascript" src="public/metronic/global/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script src="public/metronic/admin/pages/scripts/components-form-tools.js"></script>
<script src="public/metronic/admin/pages/scripts/portfolio.js"></script>
<script>
jQuery(document).ready(function() {       
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
    //ComponentsFormTools.init();
		CKEDITOR.replace( 'editor1', {
			height: 800
		});
});
</script>

