<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link href="public/metronic/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/pages/css/inbox.css" rel="stylesheet" type="text/css"/>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>全站通知</h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="preview">總覽</a><i class="fa fa-circle"></i>
      </li>
      <li class="active">
         全站通知
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-bell-o font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">全站通知</span>
            </div>
            <div class="actions btn-set">
            </div>
          </div>
          <div class="portlet-body">
            <div class="row inbox">
              <div class="inbox-content">
                <table class="table table-striped table-advance table-hover">
                  <thead>
                    <tr>
                      <th colspan="3">
                        <input type="checkbox" class="mail-checkbox mail-group-checkbox">
                        <div class="btn-group">
                          <a class="btn btn-sm blue dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="false"> More <i class="fa fa-angle-down"></i></a>
                          <ul class="dropdown-menu">
                            <li>
                              <a href="javascript:;">
                              <i class="fa fa-pencil"></i> 標示為已讀 </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                              <a href="javascript:;">
                              <i class="fa fa-trash-o"></i> 刪除 </a>
                            </li>
                          </ul>
                        </div>
                      </th>
                      <th class="pagination-control" colspan="3" style="text-align:right;">
                        <span class="pagination-info"> 1-30 of 789 </span>
                        <a class="btn btn-sm blue">
                          <i class="fa fa-angle-left"></i>
                        </a>
                        <a class="btn btn-sm blue">
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="unread" data-messageid="1">
                      <td class="inbox-small-cells">
                        <input type="checkbox" class="mail-checkbox">
                      </td>
                      <td class="view-message hidden-xs">
                         客服人員
                      </td>
                      <td class="view-message ">
                        <a href="http://dev.ijwedding.com/preview_jerry/memberOrderList" target="_blank">
                          您的訂單已成立&nbsp;
                          <i class="fa fa-external-link"></i>
                        </a>
                      </td>
                      <td class="view-message text-right">
                         16:30 PM
                      </td>
                    </tr>
                    <tr class="unread" data-messageid="2">
                      <td class="inbox-small-cells">
                        <input type="checkbox" class="mail-checkbox">
                      </td>
                      <td class="view-message hidden-xs">
                         系統訊息
                      </td>
                      <td class="view-message">
                         <a href="preview/notification/item">您的密碼已修改</a>
                      </td>
                      <td class="view-message text-right">
                         March 15
                      </td>
                    </tr>
                    <?php for($i=0;$i<28;$i++):?>
                    <tr data-messageid="<?php echo $i+3;?>">
                      <td class="inbox-small-cells">
                        <input type="checkbox" class="mail-checkbox">
                      </td>
                      <td class="view-message hidden-xs">
                        系統訊息
                      </td>
                      <td class="view-message">
                        <a href="preview/notification/item">您的密碼已修改</a>
                      </td>
                      <td class="view-message text-right">
                         March 15
                      </td>
                    </tr>
                    <?php endfor;?>
                  </tbody>
                </table>
              </div>
              <!-- /.inbox-content -->
            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->