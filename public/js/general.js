//Check IE11
function IEVersion() {
	if (!!navigator.userAgent.match(/Trident\/7\./)) {
	 	return 11;
	}
}
// Social Share
//==================================================================================
var ij_share = {};
ij_share.setting = {};
ij_share.shareTarget = {
    "facebook" : "http://www.facebook.com/share.php?u={ADDRESS}",
    "twitter" : "http://twitter.com/home?status={TITLE}{ADDRESS}",
    "google" : "https://plusone.google.com/_/+1/confirm?hl=en&url={ADDRESS}",
    "weibo" : "http://service.weibo.com/share/share.php?url={ADDRESS}&appkey=&title={TITLE}, {SDESCRIPTION}",
    "email" : "mailto:?body={ADDRESS}&subject={TITLE}"
};
ij_share.getShareUrl = function(media) {
    var link = ij_share.shareTarget[media].replace('{ADDRESS}', encodeURIComponent(document.location.href))
                                .replace('{TITLE}', encodeURIComponent(document.title))
                                .replace('{SDESCRIPTION}', encodeURIComponent('歡迎一起分享我們的喜悅'));
    return link;
};
ij_share.init = function(setting){
    ij_share.setting = setting;
    if(ij_share.setting!==null)
    {
        for(var i=0; i<ij_share.setting.length; i++)
        {
            var shareUrl = ij_share.getShareUrl(ij_share.setting[i]);
            jQuery('.btn_group_social_share a.'+ij_share.setting[i]).attr('href', shareUrl);
            jQuery('.btn_group_social_share a.'+ij_share.setting[i]).slideDown();
        }
    }
}

jQuery(document).ready(function($) {
    $("img").unveil();

    jQuery('form[id|="form-rsvp"]').submit(function(){
        var form_rsvp_id = jQuery(this).attr('id');
        var complete = false;
        var input_text_set = jQuery('#'+form_rsvp_id+' input[type="text"]');
        for(var i=0; i<input_text_set.length; i++)
        {
            if(jQuery(input_text_set[i]).val().length==0 && !complete)
            {
                if(!confirm('表單尚未全部填寫完畢，是否送出？'))
                {
                    return false;
                }
                else
                {
                    complete = true;
                }
            }
        }

        jQuery.ajax({
            method: "POST",
            url: ij_base_url + "website/rsvp",
            dataType : "json",
            data: jQuery('#'+form_rsvp_id).serialize()
        }).done(function( feedback ) {
          if(feedback.success == 'Y'){
              alert('已通知新人，感謝您');
              location.reload(true);
          }else{
              alert(feedback.msg);
          }
        });
        return false;
    });


    // Gmap
    //==================================================================================
    /*
    jQuery("#location_map").gMap({
         maptype: google.maps.MapTypeId.ROADMAP, 
         zoom: 14, 
         markers: 
		 	[
		 		{
             		address: '台北市中山區植福路8號', 
             		html: "<strong>典華旗艦</strong>", 
            		popup: true,               
				}
            ], 
         panControl: true, 
         zoomControl: true, 
         mapTypeControl: true, 
         scaleControl: true, 
         streetViewControl: true, 
         scrollwheel: false, 
         styles: [ { "stylers": [ { "hue": "#00c0b6" }, { "gamma": 1 }, { "saturation": -50 } ] } ], 
         onComplete: function() {
             // Resize and re-center the map on window resize event
             var gmap = $("#location_map").data('gmap').gmap;
             window.onresize = function(){
                 google.maps.event.trigger(gmap, 'resize');
                 $("#location_map").gMap('fixAfterResize');
             };
        }
    });
    */
});