<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<div class="container">
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <li><a href="javascript:">聯盟會員</a></li>
    <li class="active">帳務管理</li>
</ul>
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar col-md-2 col-sm-3">
    <div class="desktop_view_sidebar">
		<ul class="list-group margin-bottom-25 sidebar-menu">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
	<div class="mobile_view_sidebar">
		<div class="side-nav-head border_title_bottom">
			<button class="fa fa-bars collapsed" data-toggle="collapse" data-target="#mob_sidebar_drop_down"></button>
			<h4 class="bfont">會員中心<!--會員中心--></h4>
		</div>
		<ul class="list-group margin-bottom-25 sidebar-menu collapse" id="mob_sidebar_drop_down">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="col-md-10 col-sm-9">
    <div class="product-page">
      <h2 style="height: 50px;">
        <i class="fa fa-dollar"></i> 帳務管理
        <span class="help-block" style="font-size: 14px;display: inline;float: right;">分潤獎金成立條件為『訂單完成或部份退貨』、『已付款』之訂單，四捨五入。</span>&nbsp;&nbsp;<br>
      </h2>
      <!--div class="alert alert-warning">
         當月的數目仍為<strong>預估值</strong>，正確數量將在下個月的15日結算，匯款完成系統也會透過 Email 通知您!
      </div-->
      <form method="post">
          <div class="note note-warning">
            <div class="input-group input-large date-picker input-daterange  pull-left"  data-date-format="yyyy-mm-dd">
              <span class="input-group-addon"> 期間 </span>
              <input type="text" class="form-control" name="from" value="<?=$data_array['from']?>">
              <span class="input-group-addon"> 到 </span>
              <input type="text" class="form-control" name="to" value="<?=$data_array['to']?>">
            </div>
            <div class="table-actions-wrapper pull-right">
              <span>
              </span>
              <select name="sub_order_status" class="table-group-action-input form-control input-inline input-sm" style="font-size:11px;">
                <option value="">訂單狀態</option>
              <?php foreach($sub_order_status as $_Item){ ?>
                <option <?php echo (@$data_array['sub_order_status']==$_Item['sub_order_status'])?'selected':'';?> value="<?php echo $_Item['sub_order_status']?>"><?php echo $_Item['sub_order_status_name']?></option>
              <?php }?>
              </select>
              <select name="payment_status" class="table-group-action-input form-control input-inline input-sm" style="font-size:11px;">
                <option value="">付款狀態</option>
              <?php foreach($payment_status as $_Item){?>
                <option <?php echo (@$data_array['payment_status']==$_Item['payment_status'])?'selected':'';?> value="<?php echo $_Item['payment_status']?>"><?php echo $_Item['payment_status_name']?></option>
              <?php }?>
              </select>
              <select name="check_status" class="table-group-action-input form-control input-inline input-sm" style="font-size:11px;">
                <option value="">出帳狀態</option>
              <?php foreach($check_status as $_Item){?>
                <option <?php echo (@$data_array['check_status']==$_Item['dealer_order_status'])?'selected':'';?> value="<?php echo $_Item['dealer_order_status']?>"><?php echo $_Item['dealer_order_status_name']?></option>
              <?php }?>
              </select>
              <button type="submit" class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button>
            </div>
            <div style="clear:both;"></div>
          </div>
        </form>
      <table class="table table-striped table-bordered table-hover" id="table_member">
        <thead>
          <tr>
            <th>訂單編號</th>
            <th>訂單日期</th>
            <th>訂單狀態</th>
            <th>付款狀態</th>
            <th>訂單金額</th>
            <th>分潤％數</th>
            <th>分潤獎金</th>
            <th>出帳狀態</th>
          </tr>
        </thead>
        <tbody>
      <?php if(is_array(@$orders)){
        $total=0;
        foreach($orders as $key => $value){
          if($value['original_order_sn']=='1' || $value['original_order_sn']=='2'){
            $share_total=round($value['sub_sum']*$value['share_percent']*$bonus);
          }else{
            $share_total=0;
          }
          $total+=$share_total;
        ?>
            <tr class="odd gradeX">
              <td><?=$value['sub_order_num']?></td>
              <td><?=substr($value['order_create_date'],0,10)?></td>
              <td><?=$value['sub_order_status_name']?></td>
              <td><?=$value['payment_status_name']?></td>
              <td style="text-align: right;"><?=$value['sub_sum']?></td>
              <td style="text-align: right;"><?=($value['share_percent']*$bonus)*100?>％</td>
              <td style="text-align: right;"><?=$share_total?></td>
              <td class="center"><span class="label label-<?=($value['original_order_sn']==0)?'success':'warning'?>"><?=$value['dealer_order_status_name']?></span></td>
            </tr>
<?php }}?>
        </tbody>
        <tfooter>
          <tr>
            <td style="text-align: right;" colspan="6">合計（不分頁數）：</td>
            <td style="text-align: right;"><?=number_format($total)?></td>
            <td></td>
          </tr>
        </tfooter>
      </table>
    </div>
  </div>
  <!-- END CONTENT -->
</div>
</div>
<!-- END SIDEBAR & CONTENT -->