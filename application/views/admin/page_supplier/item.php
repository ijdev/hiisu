<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/

if(isset($_result01) && is_array($_result01))
{
 	foreach($_result01 as $key => $value)
  {

  	$_result_item=$value;

  }
}
if(isset($_bank_info) && is_array($_bank_info))
{
 	foreach($_bank_info as $key => $value)
  {

  	$_bank_info_item=$value;

  }
}
if(isset($_contact_info) && is_array($_contact_info))
{
	$_contact_info_item=array();
	$_ik=1;
 	foreach($_contact_info as $key => $value)
  {
  	$_contact_info_item[$_ik]=$value;
  	$_ik++;
  }
}else{
	$_contact_info_item=array();
}

if(isset($_content_info) && is_array($_content_info))
{
	$_ik=1;
 	foreach($_content_info as $key => $value)
  {

  	$_content_info_item[$_ik]=$value;
  	$_ik++;

  }
}
$sameas_supplier_name='checked';
$sameasadd='checked';
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<style>
label.error{color:red !important;}
</style>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>供應商 <small>供應商設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        會員管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="<?php echo $_father_link;?>">供應商</a>
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         供應商設定
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">供應商設定</span>
            </div>
            <div class="actions btn-set">
              <a href="<?php echo $_father_link;?>" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
        <div class="col-md-3 col-sm-3">
          <?php $this->load->view('www/general/flash_error');?>
        </div>
          <div class="portlet-body form">
            <div class="tabbable">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_general" data-toggle="tab"> 一般設定 </a>
                </li>
                <?php
                if(isset($_result_item->supplier_sn) && $_result_item->supplier_sn > 0 )
                {
                	$_supplier_sn_now=$_result_item->supplier_sn;?>
                <li>
                  <a href="#tab_money" data-toggle="tab"> 帳務資訊 </a>
                </li>
                <li>
                  <a href="#tab_member" data-toggle="tab"> 聯絡窗口 </a>
                </li>
                <li>
                  <a href="#tab_manager" data-toggle="tab"> 管理人員 </a>
                </li>
                <li>
                  <a href="#tab_certificate" data-toggle="tab"> 電子商務設定 </a>
                </li>
                <!--li>
                  <a href="#tab_ship" data-toggle="tab"> 運費設定 </a>
                </li-->
                <?php
              		}
              	?>
              </ul>
              <div class="tab-content no-space">
                <!-- Tab 資料設定 -->
                <div class="tab-pane active" id="tab_general">
                  <form method="post" name="register-form" id="register-form" <?php if(isset($_action_link))echo " action=\"".$_action_link."\" ";?> class="form-horizontal error" novalidate="">
                    <div class="form-body">
                      <h4>帳號設定</h4>
                      <div class="form-group">
                        <label for="active" class="col-lg-3 control-label">啟用狀態 <span class="require">*</span></label>
                        <div class="col-lg-8">
                          <select require name="supplier_status" id="supplier_status" class="form-control">
                            <option value=1 <?php if(isset($_result_item->supplier_status) && $_result_item->supplier_status=='1') echo ' selected="selected"';?>>啟用</option>
                            <option value=2 <?php if(isset($_result_item->supplier_status) && $_result_item->supplier_status=='2') echo ' selected="selected"';?>>停用</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="firstname" class="col-lg-3 control-label">公司中文名稱 <span class="require">*</span></label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="supplier_name" id="supplier_name" value="<?php if(isset($_result_item->supplier_name)) echo $_result_item->supplier_name;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="lastname" class="col-lg-3 control-label">公司英文名稱</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="supplier_eng_name" id="supplier_eng_name" value="<?php if(isset($_result_item->supplier_eng_name)) echo $_result_item->supplier_eng_name;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="firstname" class="col-lg-3 control-label">品牌名稱</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="brand" id="brand" value="<?php if(isset($_result_item->brand)) echo $_result_item->brand;?>">
                        </div>
                      </div>
                     <div class="form-group">
                        <label class="col-lg-3 control-label">產業類別 </label>
                        <div class="col-lg-8">
                          <div class="radio-list">
                             <?php
							       					//var_dump($_member_domain_relation);
							                foreach($_biz_domain_list as $keys=>$values)
							                {
							    							$_checked="";
							    							//if(isset($_member_domain_relation)) //改單選
							    							//{
								    							//foreach($_member_domain_relation as $ckey => $cvalue)
								    							//{
								    									if(@$_result_item->main_domain_sn==$values->domain_sn)
								    									$_checked=" checked";
								    							//}
								    						//}
							    							echo  '  <label class="radio-inline"><input type="radio" name="main_domain_sn" value="'.$values->domain_sn.'" '.$_checked.'>'.$values->domain_name.'</label>';
															}
                						?>
                          </div>
                        </div>
                      </div>
                     <div class="form-group">
                        <label class="col-lg-3 control-label">合作模式 </label>
                        <div class="col-lg-8">
                          <div class="radio-list">
                             <?php
							                foreach($_cooperate_mode_list as $keys=>$values)
							                {
							    							$_checked="";
								    									if(@$_result_item->cooperate_mode_type==$values->cooperate_mode_type)
								    									$_checked=" checked";
							    							echo  '  <label class="radio-inline"><input type="radio" name="cooperate_mode_type" value="'.$values->cooperate_mode_type.'" '.$_checked.'>'.$values->cooperate_mode_type_name.'</label>';
															}
                						?>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">合作起訖</label>
                        <div class="col-lg-9">
                          <div class="input-group input-large date-picker input-daterange  pull-left" data-date="" data-date-format="yyyy-mm-dd">
                            <input type="text" class="form-control input-sm" name="partnership_start_date" value="<?php if(isset($_result_item->partnership_start_date)) echo $_result_item->partnership_start_date;?>">
                            <span class="input-group-addon"> 到 </span>
                            <input type="text" class="form-control input-sm" name="partnership_end_date" value="<?php if(isset($_result_item->partnership_end_date)) echo $_result_item->partnership_end_date;?>">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">分潤％數 <span class="require">*</span></label>
                        <div class="col-lg-2">
                          <input type="number" min="1" style="width: 100px;display: inline;" class="form-control" required name="share_percent" id="share_percent" value="<?php if(isset($_result_item->share_percent)) echo $_result_item->share_percent*100;?>"> ％
                        </div>
                      </div>
                      <!--hr>
                      <h4>密碼設定</h4> bridina 說不需要
                      <div class="form-group">
                        <label for="passwords" class="col-lg-3 control-label">密碼 <span class="require">*</span></label>
                        <div class="col-lg-8">
                          <input type="password" class="form-control" name="password" id="password" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="confirmpassword" class="col-lg-3 control-label">重複輸入密碼 <span class="require">*</span></label>
                        <div class="col-lg-8">
                          <input type="password" class="form-control" name="confirm_password" id="confirm_password" value="">
                        </div>
                      </div-->
                      <hr>
                      <h4>基本資料</h4>
                      <!--div class="form-group">
                        <label for="name" class="col-lg-3 control-label">官方網站</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name='website_url' id='website_url'  value="<?php if(isset($_result_item->website_url)) echo $_result_item->website_url;?>">
                        </div>
                      </div-->
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">公司負責人 <span class="require">*</span></label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" required name="supplier_chairman" id="supplier_chairman" value="<?php if(isset($_result_item->supplier_chairman)) echo $_result_item->supplier_chairman;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">統一編號</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="supplier_tax_id" id="supplier_tax_id" value="<?php if(isset($_result_item->supplier_tax_id)) echo $_result_item->supplier_tax_id;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">公司抬頭</label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="supplier_company_title" id="supplier_company_title" value="<?php if(isset($_result_item->supplier_company_title)) echo $_result_item->supplier_company_title;?>">
                          <input type="checkbox" class="form-control" name="sameas_supplier_name" id="sameas_supplier_name" <?php echo $sameas_supplier_name;?>>同公司中文名稱
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">公司電話 <span class="require">*</span></label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" required name="office_tel" id="office_tel" value="<?php if(isset($_result_item->office_tel)) echo $_result_item->office_tel;?>" >
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">公司傳真 <span class="require">*</span></label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" required name="office_fax" id="office_fax" value="<?php if(isset($_result_item->office_fax)) echo $_result_item->office_fax;?>" >
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">公司地址 <span class="require">*</span></label>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <div class="col-md-4">
						              <select id="addr_city_code" name="addr_city_code" required class="table-group-action-input form-control " >
						                  <option value="">縣市</option>
						                  <?php
						       					//$_city_list=$this->config->item("city_ct");
						                 while (list($keys, $values) = each ($_city_ct)) {
						    							echo  ' <option value="'.$keys.'">'.$values.'</option>';
														}?>
						                </select>
						                </div>
						                <div class="col-md-4">
						                  <select  id="addr_town_code" name="addr_town_code" required class="table-group-action-input form-control  " >
						                  <option value="">鄉鎮市區</option>
						                </select>
                            </div>
						                <div class="col-md-4">
                         			<input type="text" id="zipcode" required name="zipcode" class="pagination-panel-input form-control" size="12" style="margin: 0 5px;" placeholder="郵遞區號" value="<?php if(isset($_result_item->zipcode)) echo $_result_item->zipcode;?>">
                            </div>
                          </div>
                         <input type="text" id="addr1" required name="addr1"  class="pagination-panel-input form-control " size="12" style="margin: 0 5px;" placeholder="住址" value="<?php if(isset($_result_item->addr1)) echo $_result_item->addr1;?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">發票寄送地址</label>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <div class="col-md-4">
						              <select id="invoice_addr_city_code" name="invoice_addr_city_code"  class="table-group-action-input form-control " >
						                  <option value="">縣市</option>
						                  <?php
						                   $_city_ct2=$_city_ct;
						                 while (list($keys, $values) = each ($_city_ct2)) {
						    							echo  ' <option value="'.$keys.'">'.$values.'</option>';
														}?>
						                </select>
						                            </div>
						                <div class="col-md-4">
						                  <select  id="invoice_addr_town_code"  name="invoice_addr_town_code" class="table-group-action-input form-control  " >
						                  <option value="">鄉鎮市區</option>
						                </select>
                            </div>
						                <div class="col-md-4">
                         			<input type="text" id="invoice_send_zipcode"  name="invoice_send_zipcode" class="pagination-panel-input form-control" size="12" style="margin: 0 5px;" placeholder="郵遞區號" value="<?php if(isset($_result_item->invoice_send_zipcode)) echo $_result_item->invoice_send_zipcode;?>">
                            </div>
                          </div>
                         <input type="text" id="invoice_send_addr1"  name="invoice_send_addr1"  class="pagination-panel-input form-control " size="12" style="margin: 0 5px;" placeholder="" value="<?php if(isset($_result_item->invoice_send_addr1)) echo $_result_item->invoice_send_addr1;?>">
                          <input type="checkbox" class="form-control" name="sameasadd" id="sameasadd" <?php echo $sameasadd;?>>同公司地址
                        </div>
                      </div>

                      <!--div class="form-group">
                        <label for="name" class="col-lg-3 control-label">客服代表</label>
                        <div class="col-lg-8">
                          <select name="associated_sales_member_sn"  id="associated_sales_member_sn" class="form-control">
                            <?php
						                 while (list($keys, $values) = each ($_associated_sales_membe_list)) {
						    							echo  ' <option value="'.$values->member_sn.'">'.$values->last_name.$values->first_name.'</option>';
														}
						                ?>
                          </select>
                        </div>
                      </div-->
                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                          <button type="submit" class="btn green">儲存</button> &nbsp;
                          <a href="<?php echo $_father_link;?>"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                      </div>
                    </div>
			                <div class="form-group">
			                  <label for="updater" class="col-md-3 control-label">更新者</label>
			                  <div class="col-md-8">
			                    <input type="text" class="form-control" placeholder="<?php echo (@$_result_item->update_last_name)?@$_result_item->update_last_name:''; ?>" Disabled>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="time-update" class="col-md-3 control-label">更新時間</label>
			                  <div class="col-md-8">
			                    <input type="text" class="form-control" placeholder="<?php echo (@$_result_item->last_time_update!='0000-00-00 00:00:00')?@$_result_item->last_time_update:@$_result_item->create_date;?>" Disabled>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="time-update" class="col-md-3 control-label">建立時間</label>
			                  <div class="col-md-8">
			                    <input type="text" class="form-control" placeholder="<?php echo (@$_result_item->create_date!='0000-00-00 00:00:00')?@$_result_item->create_date:'';?>" Disabled>
			                  </div>
			                </div>
                    </div>
                    <input type="hidden" name="tab_id" value="tab_general">
                  </form>
                </div>
                <!-- End of Tab 一般資料設定 -->
                <!-- Tab 帳務資訊-->
                <div class="tab-pane" id="tab_money">
                  <form method="post" name="register-form" id="register-form3" action="admin/supplier/supplierTable/bank_update/<?php echo @$_result_item->supplier_sn;?>" class="form-horizontal">
                    <div class="form-body">
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">銀行名稱 <span class="require">*</span></label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" required name="bank_name" id="bank_name" value="<?php if(isset($_bank_info_item->bank_name)) echo $_bank_info_item->bank_name;?>" <?php if(isset($_bank_info_item->bank_name)) echo 'readonly';?>>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">銀行代號 <span class="require">*</span></label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" required name="bank_code" id="bank_code" value="<?php if(isset($_bank_info_item->bank_code)) echo $_bank_info_item->bank_code;?>" <?php if(isset($_bank_info_item->bank_code)) echo 'readonly';?>>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">分行名稱 <span class="require">*</span></label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" required name="branch_name" id="branch_name" value="<?php if(isset($_bank_info_item->branch_name)) echo $_bank_info_item->branch_name;?>" <?php if(isset($_bank_info_item->branch_name)) echo 'readonly';?>>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">分行代號 <span class="require">*</span></label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" required name="branch_code" id="branch_code" value="<?php if(isset($_bank_info_item->branch_code)) echo $_bank_info_item->branch_code;?>" <?php if(isset($_bank_info_item->branch_code)) echo 'readonly';?>>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">銀行帳號 <span class="require">*</span></label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" required name="bank_account" id="bank_account" value="<?php if(isset($_bank_info_item->bank_account)) echo $_bank_info_item->bank_account;?>" <?php if(isset($_bank_info_item->bank_account)) echo 'readonly';?>>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">帳號名稱 <span class="require">*</span></label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" required name="account_name" id="account_name" value="<?php if(isset($_bank_info_item->account_name)) echo $_bank_info_item->account_name;?>" <?php if(isset($_bank_info_item->account_name)) echo 'readonly';?>>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="active" class="col-lg-3 control-label">帳號狀態 <span class="require">*</span> </label>
                        <div class="col-lg-8">
                          <select name="bank_status" class="form-control">
                            <option value=1 <?php if(isset($_bank_info_item->status) && $_bank_info_item->status==1) echo ' selected="selected"';?>>有效</option>
                            <option value=0 <?php if(isset($_bank_info_item->status) && $_bank_info_item->status==0) echo ' selected="selected"';?>>無效</option>
                          </select>
                        </div>
                      </div>
                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                          <button type="submit" class="btn green">儲存</button> &nbsp;
                          <a href="<?php echo $_father_link;?>"><button type="button" class="btn default">Cancel</button></a>
                	        <span style="display:inline;margin-left:20px;" class="help-block">（有值無法異動，如須新增請先將狀態改為無效並儲存）</span>
                        </div>
                      </div>
                    </div>
			                <div class="form-group">
			                  <label for="updater" class="col-md-3 control-label">更新者</label>
			                  <div class="col-md-8">
			                    <input type="text" class="form-control" placeholder="<?php echo (@$_bank_info_item->update_last_name)?@$_bank_info_item->update_last_name:''; ?>" Disabled>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="time-update" class="col-md-3 control-label">更新時間</label>
			                  <div class="col-md-8">
			                    <input type="text" class="form-control" placeholder="<?php echo (@$_bank_info_item->last_time_update!='0000-00-00 00:00:00')?@$_bank_info_item->last_time_update:@$_result_item->create_date;?>" Disabled>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="time-update" class="col-md-3 control-label">建立時間</label>
			                  <div class="col-md-8">
			                    <input type="text" class="form-control" placeholder="<?php echo (@$_bank_info_item->create_date!='0000-00-00 00:00:00')?@$_bank_info_item->create_date:'';?>" Disabled>
			                  </div>
			                </div>
                    </div>
                    <input type="hidden" name="tab_id" value="tab_money">
                  </form>
                </div>
                <!-- End of Tab 帳務資訊 -->
                <!-- Tab 聯絡窗口 -->
                <div class="tab-pane" id="tab_member">
                  <div class="note note-warning" id="search_filter">
                    <div class="table-actions-wrapper pull-right">
                      <button id="Add_addon" class="btn btn-sm yellow table-group-action-submit" data-toggle="modal" href="#contact_form"><i class="fa fa-check"></i> 新增聯絡窗口</button>
                    </div>
                    <div style="clear:both;"></div>
                  </div>
                  <!-- End of search_filter -->
                  <table class="table table-striped table-bordered table-hover" id="table_contact">
                    <thead>
                      <tr>
                        <th>姓名</th>
                        <th>類別</th>
                        <th>電話</th>
                        <th>手機</th>
                        <th>Email</th>
                        <th>狀態</th>
                        <th>功能</th>
                      </tr>
                    </thead>
                    <tbody>
                <?php if(is_array($_contact_info_item)) //var_dump(count($_contact_info_item));
                	{
                 			foreach($_contact_info_item as $key => $value)
                    		{
                  ?>
                        <tr class="odd gradeX">
                          <td><?php echo $value->contact_name;?></td>
                          <td><?php echo @$value->contact_type_name;?></td>
                          <td><?php echo $value->office_tel.$value->office_tel_ext;?></td>
                          <td><?php echo $value->cell;?></td>
                          <td><?php echo $value->email;?></td>
                          <td>
                            <?php if($value->status==1):?>
                              <span class="label label-sm label-success"> 有效 </span>
                            <?php else:?>
                              <span class="label label-sm label-warning"> 無效 </span>
                            <?php endif;?>
                          </td>
                          <td class="center">
		                        <a href="admin/supplier/supplierTable/edit_contact/<?php echo $_supplier_sn_now;?>/<?php echo $value->contact_sn;?>" data-remote="false" data-target="#contact_edit" data-toggle="modal"><i class="fa fa-edit"></i>編輯</a> &nbsp;
                            <a class="deleteContact" ItemId="<?php echo $value->contact_sn;?>" supplier_sn="<?php echo $_supplier_sn_now;?>" href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                          </td>
                        </tr>
                      <?php }}?>
                    </tbody>
                  </table>
                </div>
                <!-- End of Tab 聯絡窗口 -->

                <!-- Tab 管理人員 -->
                <div class="tab-pane" id="tab_manager">
                  <div class="note note-warning" id="search_filter">
<?php	if($this->session->userdata['member_data']['member_level_type']=='9'){?>
                    <div class="table-actions-wrapper pull-right">
                      <!--span>新增管理員</span>
                      <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="36" placeholder="請輸入 Email"-->
                      <button id="Add_addon" class="btn btn-sm yellow table-group-action-submit" data-toggle="modal" href="#invite_form"><i class="fa fa-check"></i> 新增管理員</button>
                    </div>
                    <div style="clear:both;"></div>
<?php }?>
                  </div>
                  <!-- End of search_filter -->
                  <table class="table table-striped table-bordered table-hover" id="table_member_admin">
                    <thead>
                      <tr>
                        <th>姓名</th>
                        <th>帳號</th>
                        <th>Email</th>
                        <th>功能</th>
                      </tr>
                    </thead>
                    <tbody>
                <?php if(is_array(@$_result03))
                	{
                 			foreach($_result03 as $key => $value)
                    		{
                  ?>
                        <tr class="odd gradeX">
                          <td><a target="_blank" href="/admin/Member/memberTable_item/edit/<?php echo $value->member_sn;?>"><?php echo $value->last_name.$value->first_name;?><i class="fa fa-external-link"></i></a></td>
                          <td><?php echo $value->user_name;?></td>
                          <td><?php echo $value->email;?></td>
                          <td class="center">
                            <a class="deleteAddon" ItemId="<?php echo $value->member_sn;?>" supplier_sn="<?php echo $_supplier_sn_now;?>" href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                          </td>
                        </tr>
                      <?php }}?>
                    </tbody>
                  </table>
                </div>
                <!-- End of Tab 管理人員 -->

                <!-- Tab 電子商務設定 -->
                <div class="tab-pane" id="tab_certificate">
                  <form method="post" name="register-form" id="register-form2" <?php if(isset($_action_link))echo " action=\"".$_action_link."\" ";?> class="form-horizontal" >
                    <div class="form-body">
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">滿額免運金額</label>
                        <div class="col-lg-2">
                          <input type="number" class="form-control" required name="min_amount_for_noshipping" id="min_amount_for_noshipping" value="<?php if(isset($_result_item->min_amount_for_noshipping)) echo $_result_item->min_amount_for_noshipping;?>">
                        </div>
                      </div>
                       <!--div class="form-group">
                        <label for="name" class="col-lg-3 control-label">運費</label>
                        <div class="col-lg-8">
                          <input type="number" class="form-control" required name="shipping_fee" id="shipping_fee" value="<?php if(isset($_result_item->shipping_fee)) echo $_result_item->shipping_fee;?>">
                        </div>
                      </div>
                       <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">宅配說明</label>
                        <div class="col-lg-8">
                          <textarea class="form-control" name="shipping_description" rows="3"><?php echo @$_result_item->shipping_description;?></textarea>
                        </div>
                      </div-->
                      <div class="form-group">
                        <label for="note" class="col-md-2 control-label">付款方式</label>
                        <div class="col-md-10">
                          <div class="checkbox-list">
                          	<?php foreach($payments as $payment){?>
                            <label class="checkbox-inline">
                              <input class="checkbox-list" required type="checkbox" class="form-control" name="payment[]" value="<?php echo $payment['payment_method'];?>" <?php if(@$payment['checked']) echo 'checked';?> >&nbsp;<?php echo $payment['payment_method_name'];?>
                            </label>
                          <?php }?>
                          </div>
                        </div>
                      </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">退換貨須知</label>
                  <div class="col-md-10">
                    <textarea name="precautions" class="form-control maxlength-handler" rows="5"><?php echo $_result_item->precautions; ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">囍市集退換貨須知</label>
                  <div class="col-md-10">
                    <?=$system_words?>
                  </div>
                </div>
                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                          <button type="submit" class="btn green">儲存</button> &nbsp;
                          <a href="<?php echo $_father_link;?>"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                      </div>
                    </div>
			                <div class="form-group">
			                  <label for="updater" class="col-md-3 control-label">更新者</label>
			                  <div class="col-md-8">
			                    <input type="text" class="form-control" placeholder="<?php echo (@$_result_item->update_last_name)?@$_result_item->update_last_name:''; ?>" Disabled>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="time-update" class="col-md-3 control-label">更新時間</label>
			                  <div class="col-md-8">
			                    <input type="text" class="form-control" placeholder="<?php echo (@$_result_item->last_time_update!='0000-00-00 00:00:00')?@$_result_item->last_time_update:@$_result_item->create_date;?>" Disabled>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="time-update" class="col-md-3 control-label">建立時間</label>
			                  <div class="col-md-8">
			                    <input type="text" class="form-control" placeholder="<?php echo (@$_result_item->create_date!='0000-00-00 00:00:00')?@$_result_item->create_date:'';?>" Disabled>
			                  </div>
			                </div>
                		</div>
                    <input type="hidden" name="tab_id" value="tab_certificate">
                	</form>
                </div>
                <!-- End of Tab 電子商務設定 -->
                <!-- Tab 合約 -->
                <div class="tab-pane" id="tab_ship">
                  <table class="table table-striped table-bordered table-hover" id="table_member_order">
                    <thead>
                      <tr>
                        <th>年度</th>
                        <th>合約編號</th>
                        <th>名稱</th>
                        <th>有效日期</th>
                        <th>結束日期</th>
                        <th>簽訂日期</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php for($i=1;$i<=3;$i++):?>
                        <tr class="odd gradeX">
                          <td>2016</td>
                          <td>1</td>
                          <td class="center">test</td>
                          <td class="center">2016-01-01</td>
                          <td class="center">2016-12-31</td>
                          <td class="center">2016-01-01</td>
                        </tr>
                      <?php endfor;?>
                    </tbody>
                  </table>
                </div>
                <!-- End of Tab 合約 -->
              </div>
            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
<!-- Modal for 設定管理員 -->
<div class="modal fade" id="invite_form" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">新增管理員</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form" method="post">
          <div class="form-body">
            <div class="form-group">
              <label for="name" class="col-lg-3 control-label">供應商管理員</label>
              <div class="col-lg-8">
                <select name="associated_member_sn" required id="associated_member_sn" class="form-control">
                	<option value="">請選擇</option>
                  <?php
                  if(@$_result04){
	                 while (list($keys, $values) = each ($_result04)) {
	    							echo  ' <option value="'.$values->member_sn.'">'.$values->last_name.$values->first_name.' &nbsp; '.$values->email.'</option>';
									}}
	                ?>
                </select>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn blue">確認加入管理員</button>
      </div>
          <input type="hidden" name="tab_id" value="tab_manager">
        </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- Modal for 設定管理員 -->
<!-- Modal for 聯絡窗口 -->
<div class="modal fade" id="contact_form" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">新增聯絡窗口</h4>
      </div>
      <div class="modal-body">
                  <form action="admin/supplier/supplierTable/contact_update/<?php echo @$_result_item->supplier_sn;?>"  class="form-horizontal" >
                    <div class="form-body">
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">姓名 </label>
                        <div class="col-lg-8">
                          <input type="text" name="contact_name" class="form-control" required value="">
                        </div>
                      </div>
                     <div class="form-group">
                        <label class="col-lg-3 control-label">類別 </label>
                        <div class="col-lg-8">
                          <div class="radio-list">
                             <?php
							                foreach($_contact_type_list as $keys=>$values)
							                {
							    							$_checked="";
								    									if($_checked=="" && $values->default_flag=='1')
								    									$_checked=" checked";
							    							echo  '  <label class="radio-inline"><input type="radio" required name="contact_type" value="'.$values->contact_type.'" '.$_checked.'>'.$values->contact_type_name.'</label>';
															}
                						?>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">公司電話 </label>
                        <div class="col-lg-8">
                            <div class="col-lg-6" style="padding-left: 0px;">
                              <input type="text" class="form-control" name="office_tel" value="">
                            </div>
                            <div class="col-lg-6">
                              <label for="name" class="col-lg-5 control-label">分機</label>
                              <div class="col-lg-7">
                                <input type="text" class="form-control" name="office_tel_ext"value="">
                              </div>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">手機 </label>
                        <div class="col-lg-8">
                          <input type="text" class="form-control" name="cell" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-lg-3 control-label">電子郵件 </label>
                        <div class="col-lg-8">
                          <input type="email" class="form-control" name="email" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="active" class="col-lg-3 control-label">狀態 </label>
                        <div class="col-lg-8">
                          <select name="status" class="form-control">
                            <option value="1" selected="selected">是</option>
                            <option value="0">否</option>
                          </select>
                        </div>
                      </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn blue">確認加入</button>
      </div>
                    </div>
                    <input type="hidden" name="tab_id" value="tab_member">
        </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
</div>
<!-- Modal for 聯絡窗口 -->
<div class="modal fade" id="contact_edit" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">訊息記錄</h4>
      </div>
      <div class="modal-body">
        <img src="public/metronic/global/img/loading-spinner-grey.gif" alt="" class="loading">
        <span>
        &nbsp;&nbsp;內容載入中... </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn blue">Save changes</button>
      </div>
    </div>
  </div>
</div>