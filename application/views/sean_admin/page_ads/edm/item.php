<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/

foreach($_result01 as $key => $value)
	{
		$_data_info=$value;
	}		

?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/typeahead/typeahead.css">
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/pages/css/portfolio.css" rel="stylesheet" type="text/css"/>


<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>行銷管理 <small>電子報管理</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin-ijwedding/Ads">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        系統設定
        <i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin-ijwedding/Ads/edm">電子報管理</a>
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         電子報編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">電子報編輯</span>
            </div>
            <div class="tools">
              <a href="admin-ijwedding/Ads/edm"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" method="post" role="form" action="admin-ijwedding/Ads/edmItem/update/<?php echo $_data_info->edm_sn;?>">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">電子報標題 <span class="require">*</span></label>
                  <div class="col-lg-10">
                    <input type="text" name="edm_titile" class="form-control" value="<?php print $_data_info->edm_titile; ?>"  placeholder="電子報標題">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">最後修改日期 <span class="require">*</span></label>
                  <div class="col-lg-10">
                    <p class="form-control-static"><?php print $_data_info->last_time_update; ?></p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">最後發送日期 <span class="require">*</span></label>
                  <div class="col-lg-10">
                    <p class="form-control-static"><?php print $_data_info->last_time_send; ?></p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-2 control-label">內容</label>
                  <div class="col-lg-10">
                    <textarea name="edm_content" class="ckeditor form-control" rows="6"><?php print $_data_info->edm_content; ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-lg-2 control-label">建立者</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control"  placeholder="<?php echo $this->session->userdata['member_data']['user_name'];?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-2 control-label">建立時間</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control"  placeholder="<?php echo date("Y-m-d H:i:s");?>" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-2 col-md-10">
                    <button type="submit" class="btn green">儲存</button>
                    <a href="admin-ijwedding/Ads/edm"><button type="button" class="btn default">Cancel</button></a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->