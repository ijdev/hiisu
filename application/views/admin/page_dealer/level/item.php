<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>經銷商分級 <small>分級設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        會員管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin/member/dealerTable">經銷商</a>
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         <a href="admin/member/dealerLevel">經銷商分級管理</a>
         <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         經銷商分級編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">經銷商分級設定</span>
            </div>
            <div class="tools">
              <a href="admin/member/dealerLevel"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
          	<?php $this->load->view('sean_admin/general/flash_error');?>
            <form class="form-horizontal" role="form" action="<?php echo $_action_link;?>">
              <div class="form-body">
                <?php
								$_f_f=array("專頁大 Banner","登入頁背景圖","客製化子域名","廣告隱藏");
								$_b_f=array("成交訂單","廣告管理","會員列表","作品集編輯");
									$_form_one= new sean_form_general();

									    $em_columns = array(
										  $_table_field["名稱"]=>array('header'=>"名稱",'type'=>'textbox', 'required'=>'required', 'required_message'=>"",'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'maxlength'=>'16', 'default'=>'', 'source'=>'', 'rows'=>'15', 'cols'=>'50'),
										  //$_table_field["排序"]=>array('header'=>"排序",'type'=>'textbox', 'required'=>'required', 'required_message'=>"",'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'maxlength'=>'3', 'default'=>'0', 'source'=>'', 'rows'=>'15', 'cols'=>'50'),
										  $_table_field["說明"]=>array('header'=>"說明",'type'=>'textarea', 'required'=>'', 'required_message'=>"",'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'maxlength'=>'', 'default'=>'', 'source'=>'', 'rows'=>'15', 'cols'=>'50'),
										  //$_table_field["經銷商專頁功能"]=>array('header'=>"經銷商專頁功能",'type'=>'checkbox', 'required'=>'', 'required_message'=>"",'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'maxlength'=>'', 'default'=>'', 'source'=>$_f_f, 'rows'=>'15', 'cols'=>'50'),
										  //$_table_field["經銷商後台功能"]=>array('header'=>"經銷商後台功能",'type'=>'checkbox', 'required'=>'', 'required_message'=>"",'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'maxlength'=>'', 'default'=>'', 'source'=>$_b_f, 'rows'=>'15', 'cols'=>'50'),
										  $_table_field["是否啟用"]=>array('header'=>"是否啟用",'type'=>'status', 'required'=>'required', 'required_message'=>"",'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'maxlength'=>'', 'default'=>'', 'source'=>'', 'rows'=>'15', 'cols'=>'50'),
									);
								 		  if(isset($_result01) && is_array($_result01))
										 	{
										 		$_form_one->add_item($em_columns,$_result01);

											}else{
												$_form_one->add_item($em_columns);
											}
									//var_dump($_result01[0]->dealer_page_profile_config);
							  ?>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">成交訂單分潤比例(%)  <span class="require">*</span></label>
                  <div class="col-lg-8">
                        <div class="input-group spinner col-lg-2">
                          <input type="text" class="form-control form-inline" required name="dealer_bonus" id="dealer_bonus"  value="<?php if(isset($_result01[0]->dealer_bonus)) echo $_result01[0]->dealer_bonus;else echo "0";?>">
                          <div class="input-group-btn-vertical">
                            <button class="btn btn-default" type="button"><i class="fa fa-caret-up"></i></button>
                            <button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i></button>
                          </div>
                        </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="seq" class="col-lg-3 control-label">招募新會員獎金 <span class="require">*</span></label>
                  <div class="col-lg-1">
                    <input type="number" class="form-control" required name="newmember" id="newmember"  value="<?php if(isset($_result01[0]->newmember)) echo $_result01[0]->newmember;?>">
                  </div>
                </div>
          <div class="form-group">
            <label for="note" class="col-lg-3 control-label">經銷商專頁功能</label>
            <div class="col-lg-8 controlsean">
              <div class="checkbox-list">
								<?php foreach($_f_f as $Item){?>
                <label class="checkbox-inline">
                  <input class="checkbox-list" type="checkbox" class="form-control" name="dealer_page_profile_config[]" value="<?php echo $Item;?>" <?php if(strpos(@$_result01[0]->dealer_page_profile_config,$Item)!==false) echo 'checked';?> >&nbsp;<?php echo $Item;?>
                </label>
              	<?php }?>
								</div>
              </div>
            </div>
          <div class="form-group">
            <label for="note" class="col-lg-3 control-label">經銷商後台功能</label>
            <div class="col-lg-8 controlsean">
              <div class="checkbox-list">
								<?php foreach($_b_f as $Item){?>
                <label class="checkbox-inline">
                  <input class="checkbox-list" type="checkbox" class="form-control" name="dealer_backend_config[]" value="<?php echo $Item;?>" <?php if(strpos(@$_result01[0]->dealer_backend_config,$Item)!==false) echo 'checked';?> >&nbsp;<?php echo $Item;?>
                </label>
              	<?php }?>
								</div>
              </div>
            </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                    <a href="<?php echo $_father_link;?>"><button type="button" class="btn default">Cancel</button></a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->