<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script src="public/metronic/admin/pages/scripts/components-pickers.js"></script>
<script>
jQuery(document).ready(function() {
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
    ComponentsPickers.init();
    $(".click-to-select").dblclick(function(){
        var item = $(this);
        var item_id = $(this).attr("item_id");
        var mode = $(this).attr("mode");
        var category_id = $(this).attr("category_id");
        var product_status = $(this).attr("product_status");
        var supplier_sn = $(this).attr("supplier_sn");
        var old_text = $(this).text();
        //console.log($('#product_status').html());
        if(mode=='default_root_category_sn'){
            var input = $("<select id='cate_select' class='cate_select droplist-input'></select>");
            $(item).html(input);
            //$($('#category').html()).appendTo(".cate_select");
            $.get('/admin/product/ChgStoreName/囍市集/'+category_id,function(r){
                $(r).appendTo(".cate_select");
            });
        }else if(mode=='product_status'){
            var input = $("<select class='product_status droplist-input'></select>");
            $(item).html(input);
            $($('#product_status').html()).appendTo(".product_status");
            $(input).val(product_status);
        }else if(mode=='supplier_sn'){
            var input = $("<select class='supplier_sn droplist-input'></select>");
            $(item).html(input);
            $($('#supplier').html()).appendTo(".supplier_sn");
            $(input).val(product_status);
        }

            $(input).focus();
            $(input).blur(function(){
                if(mode=='default_root_category_sn' && category_id == $(input).val()){
                    $(item).html(old_text);
                }else if(mode=='product_status' && product_status == $(input).val()){
                    $(item).html(old_text);
                }else if(mode=='supplier_sn' && supplier_sn == $(input).val()){
                    $(item).html(old_text);
                }else if(!$(input).val()){
                    $(item).html(old_text);
                }else{
                    if($(input).val()){
                        $.post("<?php echo base_url('admin/product/productTable')?>",{item_id:item_id,mode:mode,new_value:$(input).val()});
                        //location.reload();
                        //var text=$(input).find("option:selected").attr('pure_category_name');
                        //$(item).text(text.replace(/│/g,'').replace('├─','')).replace(/&nbsp;/g,'');
                        if(mode=='default_root_category_sn'){
                            $(item).text($(input).find("option:selected").attr('pure_category_name'));
                            $(item).attr("category_id",$(input).val());
                        }else if(mode=='product_status'){
                            $(item).text($(input).find("option:selected").text());
                            $(item).attr("product_status",$(input).val());
                        }else if(mode=='supplier_sn'){
                            $(item).text($(input).find("option:selected").text());
                            $(item).attr("supplier_sn",$(input).val());
                        }
                    }
                }
            });
    });
	var initTableMember = function () {
        var table = $('#table_member');
        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": 依此欄位做順向排序",
                    "sortDescending": ": 依此欄位做逆向排序"
                },
                "emptyTable": "沒有資料可以顯示",
                "info": "顯示 _TOTAL_ 中的第 _START_ 到 _END_ 筆",
                "infoEmpty": "查無結果",
                "infoFiltered": "(從總筆數 _MAX_ 筆過濾出)",
                "lengthMenu": "顯示 _MENU_ 項目",
                "search": "搜尋:",
                "zeroRecords": "查無結果",
                "paginate": {
                    "previous":"上一頁",
                    "next": "下一頁",
                    "last": "最後",
                    "first": "最前"
                }
            },

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "columns": [{
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
<?=($this->session->userdata('member_role_system_rules')[0]['role_name']!='供應商管理員')?' "orderable": true
            }, {':'';?>
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": false //圖
            },{
                "orderable": true
            },{
                "orderable": true
            },{
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": false
            }],
            "lengthMenu": [
                [20, 50, 100,200, -1],
                [20, 50, 100,200, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [{  // set default column settings
                'orderable': true,
                'targets': [1]
            }, {
                "searchable": true,
                "targets": [1]
            }],
            "order": [] // set first column as a default sort by asc
        });

        var tableWrapper = jQuery('#table_member_wrapper');

        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('tr').addClass("active");
                } else {
                    $(this).attr("checked", false);
                    $(this).parents('tr').removeClass("active");
                }
            });
            jQuery.uniform.update(set);
        });

        table.on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });

        tableWrapper.find('.dataTables_length select').addClass("form-control input-xsmall input-inline"); // modify table per page dropdown
    }
    $(document).on('click', '.deleteItem', function(event) {
        var button = $(this);
        	var r = confirm('是否刪除此商品?');
            if(r)
            {
				$.post('/admin/admin/ajaxdel/', {dtable: '<?php echo base64_encode('ij_product');?>',dfield:'<?php echo base64_encode('product_sn');?>',did:button.attr('ItemId')}, function(data){
                	if (data){
                		button.parents().parents().filter('tr').remove();
                	}else{
                		alert('刪除失敗');
                	}
				});
            }
    });
    initTableMember();
});
</script>

