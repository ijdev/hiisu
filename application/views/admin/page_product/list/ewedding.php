<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品列表 <small>婚禮網站</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/admin">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品列表
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
        婚禮網站
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">商品列表 - 婚禮網站</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/product/productItem/婚禮網站" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>
          <!--div class="table-actions-wrapper pull-right">
            <span>
            </span>
            <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="10" maxlenght="10" style="margin: 0 5px;" placeholder="關鍵字">
            <select class="table-group-action-input form-control input-inline input-small input-sm">
              <option value="">是否發佈</option>
              <option value="publish">發佈</option>
              <option value="unpublished">未發佈</option>
              <option value="delete">已刪除</option>
            </select>
            <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button>
          </div-->
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>主分類</th>
                  <th>商品編號</th>
                  <th>名稱</th>
                  <th>定價</th>
                  <th>售價</th>
                  <th>定價/規格</th>
                  <th width="60">代表圖</th>
                  <th>推薦</th>
                  <th>特價</th>
                  <th>熱賣</th>
                  <th>獨家</th>
                  <th>發佈</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
<?php	if(@$Items){foreach($Items as $Item){?>
                  <tr class="odd gradeX">
                    <td><?php echo @$Item['category_name'];?></td>
                    <td><?php echo $Item['product_sn'];?></td>
                    <td><?php echo $Item['product_name'];?><a href="home/eweddingItem/<?php echo $Item['product_sn'];?>" target="_blank">&nbsp;<i class="fa fa-external-link"></i></a></td>
                    <td><?php echo @$Item['price1'];?></td>
                    <td><?php echo @$Item['price2'];?></td>
                    <td class="center">
                      <?php if($Item['pricing_method']=='1'):?>
                        單一
                      <?php elseif($Item['pricing_method']=='2'):?>
                        區間
                      <?php endif;?>／
                      <?php if($Item['unispec_flag']=='1'):?>
                        單一
                      <?php elseif($Item['unispec_flag']=='0'):?>
                        多重
                      <?php endif;?>
                    </td>
                    <td><?php if($Item['image_path']):?><img src="/public/uploads/<?php echo @$Item['image_path'];?>" width="60" ><?php else:?>無<?php endif;?></td>
									<?php if(@$Item['promotion']){ //var_dump($Item['promotion']).'<br>';
									foreach(@$Item['promotion'] as $pro){?>
                    <td class="center">
                      <?php if(@$pro['status']=='1'):?>
                        <i class="fa fa-check"></i>
                      <?php else:?>
                        <i class="fa fa-times"></i>
                      <?php endif;?>
                    </td>
                  <?php }}else{ ?>
                   <td class="center">
                        <i class="fa fa-times"></i>
                    </td>
                   <td class="center">
                        <i class="fa fa-times"></i>
                    </td>
                   <td class="center">
                        <i class="fa fa-times"></i>
                    </td>
                   <td class="center">
                        <i class="fa fa-times"></i>
                    </td>
                  <?php }?>
                    <td class="center">
                       <?php if($Item['product_status']):?>
                        <i class="fa fa-check"></i>
                      <?php else:?>
                        <i class="fa fa-times"></i>
                      <?php endif;?>
                    </td>
                    <td class="center">
                      <a href="admin/product/productItem/婚禮網站/<?php echo $Item['product_sn'];?>"><i class="fa fa-edit"></i>編輯</a> &nbsp;&nbsp;
                      <a href="javascript:void(0);" class="deleteItem" ItemId="<?php echo $Item['product_sn'];?>"><i class="fa fa-trash-o"></i>刪除</a>
                    </td>
                  </tr>
<?php }}?>
              </tbody>
            </table>

          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->