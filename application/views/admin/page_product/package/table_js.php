<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/table-managed.js"></script>
<script>
jQuery(document).ready(function() {       
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	var initTableMember = function () {
        var table = $('#table_member');
        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": 依此欄位做順向排序",
                    "sortDescending": ": 依此欄位做逆向排序"
                },
                "emptyTable": "沒有資料可以顯示",
                "info": "總共 _TOTAL_ 筆中的第 _START_ 到 _END_ 筆",
                "infoEmpty": "查無結果",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "顯示 _MENU_ 項目",
                "search": "快速搜尋:",
                "zeroRecords": "查無結果",
                "paginate": {
                    "previous":"上一頁",
                    "next": "下一頁",
                    "last": "最後",
                    "first": "最前"
                }
            },

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "columns": [{
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": true
            }, {
                "orderable": false
            }],
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,            
            "pagingType": "bootstrap_full_number",
            "columnDefs": [{  // set default column settings
                'orderable': false,
                'targets': [0]
            }, {
                "searchable": false,
                "targets": [0]
            }],
            "order": [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = jQuery('#table_member_wrapper');

        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('tr').addClass("active");
                } else {
                    $(this).attr("checked", false);
                    $(this).parents('tr').removeClass("active");
                }
            });
            jQuery.uniform.update(set);
        });

        table.on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });

        tableWrapper.find('.dataTables_length select').addClass("form-control input-xsmall input-inline"); // modify table per page dropdown
    }
    $(document).on('click', '.deleteItem', function(event) {
        var button = $(this);
        	var r = confirm('是否刪除此規格組合?');
            if(r)
            {
        				//console.log(button.attr('ItemId'));
						    $.post('/admin/admin/ajaxdel/', {dtable: '<?php echo base64_encode('ij_product_package_config');?>',dfield:'<?php echo base64_encode('product_package_config_sn');?>',did:button.attr('ItemId')}, function(data){
                	if (data){
                		button.parents().parents().filter('tr').remove();
                	}else{
                		alert('刪除失敗');
                	}
						    });
            }
    });
    initTableMember();
});
</script>

