<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<style>
.pi-img-wrapper img{border:1px solid #EFEFEF;}
.sale-product{background-color: #EFEFEF;padding: 12px;}
#carousel_ul_logo{margin-left: -25px;}
#carousel_ul_logo li{
  margin-left: 8px;
  margin-right: 8px;
}
.owl-probox #probox1,.owl-probox #probox1{margin-left: -25px;}
.owl-probox #probox1 ul li,.owl-probox #probox1 ul li{
  margin-left: 8px;
  margin-right: 8px;
}
.collapsed .glyphicon-chevron-down:before{
  content: "\f107";
  font-family: FontAwesome;
  font-size: 40px;
  color: #999;
}
.glyphicon-chevron-down:before{
  content: "\f106";
  font-family: FontAwesome;
  font-size: 40px;
  color: #999;
}
.down144{position:relative;}
.col-lg-12.col-md-12.pull-left.tab_morebtn.margin-bottom-10.drop_down_btn.down144down {
    position: absolute;
    bottom: 0;
    width: 100%;
}
#carousel_inner2>ul {
    margin: 0 -15px;
}
.top_image-box-item:first-child{padding-left: 15px;}
.top_image-box-item:last-child{padding-right: 15px;}
.row-s-15 {
    margin-right: -15px;
    margin-left: -15px;
}
.blog-post-item img {
    width: 100%;
}
#carousel_ul_logo li img {
    width: 100%;
}
@media (max-width: 768px){
.main {
    padding-top: 20px !important;
}
.sm-row {
    /*margin: 0 -15px;*/
    overflow: hidden; height:auto !important;
}
.sm-row img {
    max-height: none !important; width:100%;
}
#carousel_inner2>ul {
    margin: 0 auto;
}
.row-s-15 {
    margin:0 auto;}
}
}
</style>
<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<!--<link href="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">-->
<link href="public/metronic/global/plugins/carousel-owl-carousel/OwlCarousel2/assets/owl.carousel.css" rel="stylesheet">
<link href="public/metronic/global/plugins/slider-layer-slider/css/layerslider.css" rel="stylesheet">
<!-- Page level plugin styles END -->
<div>

<!-- BEGIN SIDEBAR & CONTENT -->
      <div class="top_image-box_heading margin-bottom-20">
        <div class="top_image-inner-box">
<?php if(@$slogan['banners']){ foreach($slogan['banners'] as $key=>$_Item){?>
          <a href="home/ad_link/<?php echo $_Item['banner_schedule_sn'];?>" title="<?php echo $_Item['banner_content_name'];?>" target="_blank">
          <img style="margin: 0 auto;width:<?php echo $slogan['width'];?>px;" src="public/uploads/<?php echo $_Item['banner_content_save_dir'];?>" class="dropimg img-responsive" alt="<?php echo $_Item['banner_content_name'];?>"></a>
<?php }}else{ if(@$slogan){
  if($slogan['default_banner_link']){?>
              <a href="<?=$slogan['default_banner_link'];?>" target="_blank" title="<?php echo @$slogan['banner_location_name'];?>" target="_blank">
  <?php }?>
                <img style="margin: 0 auto;width:<?php echo $slogan['width'];?>px;" src="public/uploads/<?php echo @$slogan['default_banner_content_save_dir'];?>" class="dropimg img-responsive" alt="<?php echo @$slogan['banner_location_name'];?>"></a>
<?php }}?>
        </div>
      </div>
      <div class="top_image-box clearfix owl-probox4" style="position: relative;">
        <div id='carousel_container'>
          <div id='left_scrolls'><i class="fa fa-angle-left" aria-hidden="true"></i></div>
          <div id='right_scrolls'><i class="fa fa-angle-right" aria-hidden="true"></i></div>
        </div>
        <div class="row-s-15 owl-carousel owl-theme" id="probox4">
<?php if(@$promo_category){ foreach($promo_category as $Item){?>
          <div class="top_image-box-item col-md-3 col-sm-6 col-xs-6 margin-bottom-10" style="border:0;">
            <a class="ftr_gallery-img" href='shop/shopCatalog/<?php echo $Item['category_sn'];?>'><img class="product-main-image" onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['main_picture_save_dir'];?>" alt="<?php echo $Item['main_picture_desc'];?>"></a>
            <div class="ftr_gallery-txt-box">
              <h4><a href="shop/shopCatalog/<?php echo $Item['category_sn'];?>"><?php echo $Item['category_name'];?></a></h4>
            <p class="text-left" style="height:60px; overflow:hidden; text-align: left !important;"><?php echo $this->ijw->truncate(strip_tags($Item['category_sub_name']),60);?></p>

  <?php /*
            <ul class="blog-tags">
<?php if(@$Item['category_attributes']){ foreach($Item['category_attributes'] as $Item2){?>
              <li><a href="/shop/shopCatalog/<?php echo $Item['category_sn'];?>?attribute_value_sn[]=<?=$Item2['attribute_value_name']?>"><img src="public/img/icon_tag.png"><?=$Item2['attribute_value_name']?></a></li>
<?}}?>
            </ul>
            <span class="ftr_img_more_btn" style="height:40px;">
              <a class="news_title_btn more_btn" style="color: #fff;" href="shop/shopCatalog/<?php echo $Item['category_sn'];?>">More<img src="public/img/icon_more.png"></a>
            </span>
            */?>
            </div>
          </div>
  <?php }}?>
        </div>
      </div>
      <div class="go_center clearfix">
        <h4 class="owl-featured newarv_head"> Recommendations 市集推薦 </h4>
      </div>
          <div id='carousel_inner2' class="clearfix down144 owl-probox1">
            <div id='carousel_container'>
              <div id='left_scrolls'><i class="fa fa-angle-left" aria-hidden="true"></i></div>
              <div id='right_scrolls'><i class="fa fa-angle-right" aria-hidden="true"></i></div>
            </div>
            <div id="carousel_inner" style="position: relative;"><!-- class="owl-probox"-->
              <ul class="clearfix list-unstyled owl-carousel owl-theme" id="probox1">
                <!--col-lg-12 col-lg-12 col-md-12 -->
  <?php if(@$promo_product1){ foreach($promo_product1 as $Item){?>
                <li class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item" style="width:226px;"><!---->
                  <div class="note note-success note-bordered heart<?php echo $Item['product_sn'];?>" style="display:none;position: relative;z-index: 999"></div>
                  <div class="thumb1st">
                    <a href="shop/shopItem/<?php echo $Item['product_sn'];?>"><img onerror="this.src='public/uploads/notyet.gif'" class="1stliimg img-responsive" src="public/uploads/<?php echo $Item['image_path'];?>"/></a>
                    <a class="gid113 btn btn-default add-wishlist href-reset" href="javascript:void(0);"><i product_sn="<?php echo $Item['product_sn'];?>" class="fa fa-heart nopadding"></i></a>
                    </div>
                  <div class="item-summary"><!-- col-lg-12 col-md-12 col-sm-12-->
                    <div class="summ1"><?=$Item['product_name']?></div>
                    <div class="d2sum">
                      <h2><a href="shop/shopItem/<?php echo $Item['product_sn'];?>"><?=$this->ijw->truncate($Item['special_item'],42)?></a></h2>
                    </div>
                    <!-- price -->
                    <div class="item-price text-left" >
                      $ <span class="pricetxt"><?php echo number_format($Item['price1']);?>&nbsp;<?php echo $Item['price2'];?></span>
                      </div>
                    <!-- /price -->
                    </div>
                  <p class="h_item2">
                    <?php if((($Item['unispec_flag'] && $Item['unispec_qty_in_stock'])||($Item['unispec_flag']=='0' && count($Item['mutispec'])>=1)) || $Item['open_preorder_flag']){?>
                    推薦
                    <?php }else{?>
                    補貨中
                    <?php }?>
                  </p>
                </li>
  <?php }}?>
              </ul>
            </div>
      </div>

      <!--<div class="pge_row_brdr"></div> -->
        <div class="pge_row_brdr"></div>
        <div class="clearfix">
          <h4 class="owl-featured  newarv_head">New Arrivals 新品上市</h4>
        </div>
        <div id='carousel_inner2' class="clearfix down144 owl-probox2">
          <div id='carousel_container'>
            <div id='left_scrolls'><i class="fa fa-angle-left" aria-hidden="true"></i></div>
            <div id='right_scrolls'><i class="fa fa-angle-right" aria-hidden="true"></i></div>
          </div>
          <div id="carousel_inner" style="position: relative;">
            <ul class="new_arril clearfix list-unstyled owl-carousel owl-theme" id="probox2">
              <!--col-lg-12 col-lg-12 col-md-12 -->
  <?php if(@$new_product){ foreach($new_product as $Item){?>
              <li class="col-lg-3 col-md-3 col-sm-3 col-xs-6" style="min-height: 390px;">
                <div class="note note-success note-bordered heart<?php echo $Item['product_sn'];?>" style="display:none;position: relative;z-index: 999"></div>
                <div class="thumb1st"><!-- col-lg-12 col-md-12 col-sm-12-->
                  <a href="shop/shopItem/<?php echo $Item['product_sn'];?>"><img onerror="this.src='public/uploads/notyet.gif'" class="1stliimg img-responsive" src="public/uploads/<?php echo $Item['image_path'];?>"/></a>
                  <a class="gid113 btn btn-default add-wishlist href-reset" href="javascript:void(0);" "><i product_sn="<?php echo $Item['product_sn'];?>" class="fa fa-heart nopadding"></i></a>
                  </div>
                <div class="item-summary"><!-- col-lg-12 col-md-12 col-sm-12-->
                  <div class="summ1"><?=$Item['product_name']?></div>
                  <div class="d2sum">
                    <h2><a href="shop/shopItem/<?php echo $Item['product_sn'];?>"><?=$this->ijw->truncate($Item['special_item'],42)?></a></h2>
                  </div>
                  <!-- price -->
                  <div class="item-price text-left" >
                    $ <span class="pricetxt"><?php echo number_format($Item['price1']);?>&nbsp;<?php echo $Item['price2'];?></span>
                    </div>
                  <!-- /price -->
                  </div>
  <?php if($Item['promotion_label_name']=='推薦'){?>
                <p class="h_item2">
                  <?php if((($Item['unispec_flag'] && $Item['unispec_qty_in_stock'])||($Item['unispec_flag']=='0' && count($Item['mutispec'])>=1)) || $Item['open_preorder_flag']){?>
                  推薦
                  <?php }else{?>
                  補貨中
                  <?php }?>
                </p>
  <?php }?>
              </li>
  <?php }}?>
              </ul>
          </div>
              <div class="tab_morebtn margin-bottom-10 drop_down_btn collapsed down144down" data-toggle="collapse" data-target="#drop_down144"  style="display:none;"><!--col-lg-12 col-md-12 pull-left --><a class="col-md-12 text-center"><span class="col-lg-12 col-md-12 pull-left glyphicon glyphicon-chevron-down text-center"></span></a></div>
              <div class="more_product_d_down collapse" id="drop_down144">
                  <div id='carousel_inner2'>
                    <ul class="col-lg-12 col-lg-12 col-md-12">
<?php if(@$new_product2){ foreach($new_product2 as $Item){?>
              <li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                  <div class="note note-success note-bordered heart<?php echo $Item['product_sn'];?>" style="display:none;position: relative;z-index: 999"></div>
                  <div class="thumb1st col-lg-12 col-md-12 col-sm-12">
                    <a href="shop/shopItem/<?php echo $Item['product_sn'];?>"><img onerror="this.src='public/uploads/notyet.gif'" class="1stliimg img-responsive" src="public/uploads/<?php echo $Item['image_path'];?>"/></a>
                    <a class="gid113 btn btn-default add-wishlist href-reset" href="javascript:void(0);" "><i product_sn="<?php echo $Item['product_sn'];?>" class="fa fa-heart nopadding"></i></a>
                  </div>
                  <div class="item-summary col-lg-12 col-md-12 col-sm-12">
                    <div class="summ1"><?=$Item['product_name']?></div>
                    <div class="d2sum">
                      <h2><a href="shop/shopItem/<?php echo $Item['product_sn'];?>"><?=$this->ijw->truncate($Item['special_item'],42)?></a></h2>
                    </div>
                      <!-- price -->
                      <div class="item-price text-left" >
                        $ <span class="pricetxt"><?php echo number_format($Item['price1']);?>&nbsp;<?php echo $Item['price2'];?></span>
                      </div>
                      <!-- /price -->
                  </div>
                <p class="h_item2">
                <?php if((($Item['unispec_flag'] && $Item['unispec_qty_in_stock'])||($Item['unispec_flag']=='0' && count($Item['mutispec'])>=1)) || $Item['open_preorder_flag']){?>
                  SALE
                <?php }else{?>
                  補貨中
                <?php }?>
                </p>
              </li>
<?php }}?>
                    </ul>
                  </div>
              </div>
  </div>
        <div class="prod_bottom_img margin-bottom-10">
<?php if(@$wide_banner['banners']){ foreach($wide_banner['banners'] as $key=>$_Item){?>
              <a href="home/ad_link/<?php echo $_Item['banner_schedule_sn'];?>" title="<?php echo $_Item['banner_content_name'];?>" target="_blank"><img  src="public/uploads/<?php echo $_Item['banner_content_save_dir'];?>" class="dropimg" alt="<?php echo $_Item['banner_content_name'];?>"></a>
<?php }}else{ if(@$wide_banner){?>
              <a href="<?php echo @$wide_banner['default_banner_link'];?>" title="<?php echo @$wide_banner['banner_location_name'];?>" target="_blank"><img src="public/uploads/<?php echo @$wide_banner['default_banner_content_save_dir'];?>" class="dropimg" alt="<?php echo @$wide_banner['banner_location_name'];?>"></a>
<?php }}?>
        </div>
      <!--<div class="pge_row_brdr"></div> -->
      <!--div class="product_scnd_slider">
        <div class="container mob_padding">
        <h2 class="owl-featured noborder important-txt news_c" style="font-family:'Times New Roman', serif; font-weight:normal;color: #3e4d5c !important;"></h2>
        <ul class="nav nav-tabs nav-bottom-border margin-bottom-20" style="display:block;  margin-right: auto !important;">
          <li class="nav-tabs_index active"><a href="#home0" data-toggle="tab" style="border-radius:0 !important" aria-expanded="true">最受歡迎商品</a></li>
          <li class="nav-tabs_index"><a href="#home1" data-toggle="tab" style="border-radius:0 !important" aria-expanded="false">市集獨家商品</a></li>
          <li class="nav-tabs_index"><a href="#home2" data-toggle="tab" style="border-radius:0 !important" aria-expanded="false">促銷高CP值特價商品</a></li>
        </ul>
<div  class="tab-content padding-0 mob_padding" id="tabcon" style="overflow: hidden;">
  <div class="tab-pane fade active in" id="home0">
     <ul class="sdul col-lg-12 col-md-12">
<?php if(@$promo_product2){ foreach($promo_product2 as $Item){?>
              <li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                  <div class="note note-success note-bordered heart<?php echo $Item['product_sn'];?>" style="display:none;position: relative;z-index: 999"></div>
                  <div class="thumb1st col-lg-12 col-md-12 col-sm-12">
                    <a href="shop/shopItem/<?php echo $Item['product_sn'];?>"><img onerror="this.src='public/uploads/notyet.gif'" class="1stliimg img-responsive" src="public/uploads/<?php echo $Item['image_path'];?>"/></a>
                    <a class="gid113 btn btn-default add-wishlist href-reset" href="javascript:void(0);" "><i product_sn="<?php echo $Item['product_sn'];?>" class="fa fa-heart nopadding"></i></a>
                  </div>
                  <div class="item-summary col-lg-12 col-md-12 col-sm-12">
                    <div class="summ1"><?=$Item['product_name']?></div>
                    <div class="d2sum">
                      <h2><a href="shop/shopItem/<?php echo $Item['product_sn'];?>"><?=$this->ijw->truncate($Item['special_item'],42)?></a></h2>
                    </div>
                      <div class="item-price text-left" >
                        $ <span class="pricetxt"><?php echo number_format($Item['price1']);?>&nbsp;<?php echo $Item['price2'];?></span>
                      </div>
                  </div>
                <p class="h_item2">
                <?php if((($Item['unispec_flag'] && $Item['unispec_qty_in_stock'])||($Item['unispec_flag']=='0' && count($Item['mutispec'])>=1)) || $Item['open_preorder_flag']){?>
                  SALE
                <?php }else{?>
                  補貨中
                <?php }?>
                </p>
              </li>
<?php }}?>
  </ul>
  <div class="col-lg-12 col-md-12 pull-left tab_morebtn margin-bottom-10" ><a class="col-md-12 text-center">MORE</a><span class="col-lg-12 col-md-12 pull-left glyphicon glyphicon-chevron-down text-center"></span></div>
  </div>

      <div class="tab-pane fade" id="home1">
       <ul class="sdul col-lg-12 col-md-12">
<?php if(@$promo_product4){ foreach($promo_product4 as $Item){?>
              <li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                  <div class="note note-success note-bordered heart<?php echo $Item['product_sn'];?>" style="display:none;position: relative;z-index: 999"></div>
                  <div class="thumb1st col-lg-12 col-md-12 col-sm-12">
                    <a href="shop/shopItem/<?php echo $Item['product_sn'];?>"><img onerror="this.src='public/uploads/notyet.gif'" class="1stliimg img-responsive" src="public/uploads/<?php echo $Item['image_path'];?>"/></a>
                    <a class="gid113 btn btn-default add-wishlist href-reset" href="javascript:void(0);" "><i product_sn="<?php echo $Item['product_sn'];?>" class="fa fa-heart nopadding"></i></a>
                  </div>
                  <div class="item-summary col-lg-12 col-md-12 col-sm-12">
                    <div class="summ1"><?=$Item['product_name']?></div>
                    <div class="d2sum">
                      <h2><a href="shop/shopItem/<?php echo $Item['product_sn'];?>"><?=$this->ijw->truncate($Item['special_item'],42)?></a></h2>
                    </div>
                      <div class="item-price text-left" >
                        $ <span class="pricetxt"><?php echo number_format($Item['price1']);?>&nbsp;<?php echo $Item['price2'];?></span>
                      </div>
                  </div>
                <p class="h_item2">
                <?php if((($Item['unispec_flag'] && $Item['unispec_qty_in_stock'])||($Item['unispec_flag']=='0' && count($Item['mutispec'])>=1)) || $Item['open_preorder_flag']){?>
                  SALE
                <?php }else{?>
                  補貨中
                <?php }?>
                </p>
              </li>
<?php }}?>
  </ul>
  <div class="col-lg-12 col-md-12 pull-left tab_morebtn"><a href="#" class="col-md-12 text-center">MORE</a><span class="col-lg-12 col-md-12 pull-left glyphicon glyphicon-chevron-down text-center"></span></div>
  </div>

      <div class="tab-pane fade" id="home2">
       <ul class="sdul col-lg-12 col-md-12">
<?php if(@$promo_product3){ foreach($promo_product3 as $Item){?>
              <li class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                  <div class="note note-success note-bordered heart<?php echo $Item['product_sn'];?>" style="display:none;position: relative;z-index: 999"></div>
                  <div class="thumb1st col-lg-12 col-md-12 col-sm-12">
                    <a href="shop/shopItem/<?php echo $Item['product_sn'];?>"><img onerror="this.src='public/uploads/notyet.gif'" class="1stliimg img-responsive" src="public/uploads/<?php echo $Item['image_path'];?>"/></a>
                    <a class="gid113 btn btn-default add-wishlist href-reset" href="javascript:void(0);" "><i product_sn="<?php echo $Item['product_sn'];?>" class="fa fa-heart nopadding"></i></a>
                  </div>
                  <div class="item-summary col-lg-12 col-md-12 col-sm-12">
                    <div class="summ1"><?=$Item['product_name']?></div>
                    <div class="d2sum">
                      <h2><a href="shop/shopItem/<?php echo $Item['product_sn'];?>"><?=$this->ijw->truncate($Item['special_item'],42)?></a></h2>
                    </div>
                      <div class="item-price text-left" >
                        $ <span class="pricetxt"><?php echo number_format($Item['price1']);?>&nbsp;<?php echo $Item['price2'];?></span>
                      </div>
                  </div>
                <p class="h_item2">
                <?php if((($Item['unispec_flag'] && $Item['unispec_qty_in_stock'])||($Item['unispec_flag']=='0' && count($Item['mutispec'])>=1)) || $Item['open_preorder_flag']){?>
                  SALE
                <?php }else{?>
                  補貨中
                <?php }?>
                </p>
              </li>
<?php }}?>
    </ul>
    <div class="col-lg-12 col-md-12 pull-left tab_morebtn"><a href="#" class="col-md-12 text-center">MORE</a><span class="col-lg-12 col-md-12 pull-left glyphicon glyphicon-chevron-down text-center"></span></div>
  </div>

    </div>
</div>
      </div-->
      <div class="clearfix">
        <div class="pge_row_brdr"></div>
      </div>
      <div class="clearfix">
        <div class="news_items">
          <div class="row" style="border:rgba(170, 170, 170, 0.5) 1px solid !important; padding:30px">
            <div class="col-lg-3 col-md-3 col-sm-12 padding-20 margin-bottom-0" style="padding-left: 0 !important;">
              <div class="news_title_box">
                <h2 class="owl-featured noborder txt news_c" style="color: #666;, font-family:'Times New Roman', serif; font-size:26px;font-weight:normal;">News&amp;Events</h2>
                <span style="height:40px;"><a class="btn-primary news_title_btn news_btn" style="color: #fff;" href="/home/blogList/網站最新消息">更多消息</a></span>
              </div>
            </div>
            <div class="news_link_box col-lg-9 col-md-9 col-sm-12">
  <?php if($news){ foreach($news as $_Item){?>
              <div class="news_block" style="padding-top:3px; border-bottom: 1px solid rgba(170, 170, 170, 0.4);padding-bottom:10px;">
                <?php echo substr($_Item['last_time_update'],0,10);?> &nbsp;<a href="home/blogItem/<?php echo $_Item['blog_article_sn'];?>" title="<?php echo $_Item['blog_article_titile'];?>" class="job2"><?php echo $_Item['blog_article_titile'];?></a>
              </div>
  <?php }}else{
  echo '<div class="news_block" style="padding-top:3px; border-bottom: 1px solid rgba(170, 170, 170, 0.4);padding-bottom:10px;">目前尚無最新消息</div>';
}?>
            </div>
          </div>
          </div>
      </div>
      <div class="news_post_items clearfix">
        <div style="width: 100%;padding:0;" class="container"><!--float: left;-->
          <h4 class="owl-featured newarv_head">BLOG 最新文章</h4>
        </div>
        <div class="owl-probox3" style="position:relative;">
          <div class="clearfix" id="carousel_container">
            <div id='left_scrolls'><i class="fa fa-angle-left" aria-hidden="true"></i></div>
            <div id='right_scrolls'><i class="fa fa-angle-right" aria-hidden="true"></i></div>
          </div>
          <div>
            <div class="clearfix row owl-carousel owl-theme" id="probox3">
              <?php if($new_blogs){ foreach($new_blogs as $_Item){?>
              <div class="blog-post-item col-md-3 col-sm-6 col-xs-6 margin-bottom-10" style="border:0;">
                <div style="height:192px;" class="sm-row">
                  <a class="img-hover" href="home/blogItem/<?php echo $_Item['blog_article_sn'];?>">
                  <?php if($_Item['blog_article_image_save_dir']):?><img style="max-height:192px;" class="img-responsive" alt="<?php echo $_Item['blog_article_titile'];?>-圖檔" src="/public/uploads/<?php echo @$_Item['blog_article_image_save_dir'];?>" ><?php else:?><img alt="無圖檔或尚未上傳" src="public/uploads/notyet.gif"><?php endif;?></a>
                </div>
                <h4 class="bfont text-left margin-top-20 size-16"><a href="home/blogItem/<?php echo $_Item['blog_article_sn'];?>">
                <?php echo $_Item['blog_article_titile'];?></a></h4>
                <p class="text-left" style="height:60px; overflow:hidden; text-align: left !important;"><?php echo $this->ijw->truncate(strip_tags($_Item['blog_article_content']),58);?></p>
                <?php /*<ul class="text-left size-12 list-inline list-separator margin-bottom-10">
              <li>
                <i class="fa fa-calendar"></i>
                <?php echo substr($_Item['last_time_update'],0,10);?>
              </li>
              </ul>*/?>
              </div>
              <?php }}else{
  echo '尚無文章';
}?>
            </div>
          </div>
        </div>
        <div class="news_post_items_btn">
          <span style="height:40px;">
            <a class="btn-primary news_title_btn news_btn" style="color: #fff;" href="/home/blogList">更多文章</a>
          </span>
        </div>
      </div>

      <?php /*<div class="pge_row_brdr" style="display:block;">&nbsp;</div>*/?>

      <div class="logo_slider" style="position:relative;">
        <div class="clearfix">
          <h4 class="owl-featured  newarv_head"> PARTNER 合作夥伴</h4>
        </div>
        <div class="owl-probox5">
          <div id='carousel_container'>
            <div id='left_scrolls'><i class="fa fa-angle-left" aria-hidden="true"></i></div>
            <div id='right_scrolls'><i class="fa fa-angle-right" aria-hidden="true"></i></div>
          </div>
          <div id='carousel_inner' style="position: relative;">
            <ul class="owl-carousel owl-theme" id="probox5"><!--id='carousel_ul_logo'-->
  <?php if(@$partner['banners']){ foreach($partner['banners'] as $key=>$_Item){?>
              <li class="item"><a href="home/ad_link/<?php echo $_Item['banner_schedule_sn'];?>" title="<?php echo $_Item['banner_content_name'];?>" target="_blank"><img src="public/uploads/<?php echo $_Item['banner_content_save_dir'];?>" alt="<?php echo $_Item['banner_content_name'];?>"></a></li>
  <?php }}?>
            </ul>
          </div>
        </div>
  </div>

      <div class="pge_row_brdr"></div>
      <div class="ftr_logo_box">
        <div style="width: 100%;float: left;" class="container">
          <h2 class="owl-featured noborder" style="float:left;"></h2>
        </div>
        <div class="ftr_logo_box-inner">
          <div class="row">
            <?php if(@$bottoms['banners']){ foreach($bottoms['banners'] as $key=>$_Item){?>
            <div style="/*padding-left:0px;*/" class="blog-post-item col-md-3 col-sm-6 col-xs-6 margin-bottom-10"><a href="home/ad_link/<?php echo $_Item['banner_schedule_sn'];?>" title="<?php echo $_Item['banner_content_name'];?>" target="_blank"><img style="max-width: 100%;" src="public/uploads/<?php echo $_Item['banner_content_save_dir'];?>" alt="<?php echo $_Item['banner_content_name'];?>"></a></div>
  <?php }}?>
          </div>
        </div>
      </div>
  <!-- END SALE PRODUCT & NEW ARRIVALS -->
  <!-- END CONTENT -->
</div>
</div>
<!-- END SIDEBAR & CONTENT -->