<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Home 主內容
*/
?>

<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<link href="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="public/metronic/global/plugins/slider-layer-slider/css/layerslider.css" rel="stylesheet">
<!-- Page level plugin styles END -->
<style>
	.feature-title{
		margin-top: 60px;
    	font-size: 32px;
    	color:#cebea2;
	}
	.feature-desc{
		line-height: 2;
		font-size: 18px;
		padding-top: 6px;
	}
	.feature-box-02{
		height:520px;background-image: url(public/img/ewedding-feature-02.png);background-size: cover;
	}
	.feature-bg-02{
		width:36%;margin:0% 8% 0% 56%;background: rgba(255, 255, 255, .8);position: absolute;top:0;left:0;height:180px;
	}
	.feature-title-02{
    	font-size: 32px;color:#664E3D;width:36%;margin:0% 8% 0% 56%;position: absolute;height:180px;padding:24px;
	}
	.feature-desc-02,.feature-desc-04,.feature-desc-06,.feature-desc-08{
		line-height: 1.5;font-size: 14px;padding-top: 6px;color:#000;
	}

	.feature-box-04{
		height:520px;background-image: url(public/img/ewedding-feature-04.png);background-size: cover;
	}
	.feature-bg-04{
		width:36%;margin:0% 56% 0% 8%;background: rgba(255, 255, 255, .8);position: absolute;top:0;left:0;height:240px;
	}
	.feature-title-04{
    	font-size: 32px;color:#664E3D;width:36%;margin:0% 56% 0% 8%;position: absolute;height:240px;padding:24px;
	}

	.feature-box-06{
		height:520px;background-image: url(public/img/ewedding-feature-06.png);background-size: cover;
	}
	.feature-bg-06{
		width:36%;margin:0% 56% 0% 8%;background: rgba(255, 255, 255, .8);position: absolute;top:0;left:0;height:240px;
	}
	.feature-title-06{
    	font-size: 32px;color:#664E3D;width:36%;margin:0% 56% 0% 8%;position: absolute;height:240px;padding:24px;
	}

	.feature-box-08{
		height:520px;background-image: url(public/img/ewedding-feature-08.png);background-size: cover;
	}
	.feature-bg-08{
		width:36%;margin:0% 56% 0% 8%;background: rgba(255, 255, 255, .8);position: absolute;top:0;left:0;height:240px;
	}
	.feature-title-08{
    	font-size: 32px;color:#664E3D;width:36%;margin:0% 56% 0% 8%;position: absolute;height:240px;padding:24px;
	}
	.feature-list-title, .ecommerce h2{
		text-align:center;
		font-size: 36px;
		border-top:1px solid #cebea2;
		border-bottom:1px solid #cebea2;
		padding: 12px 0 12px 0;
		width:96%;
		margin: 0 auto 60px auto;
	}
	.feature-icon-list div{
		text-align: center;
		margin-bottom: 30px;
	}
	.feature-icon-list img.img-responsive{
		display: initial;
		padding-bottom: 8px;
	}
	.btn-freetry{
		border-radius: 8px !important;background-color: #F6B93A;color:#FFF;
	}

	@media screen and (max-width: 990px) {
		.feature-box-02{
			height:400px;
		}
		.feature-bg-02{
    		width:92%;margin:0% 4% 0% 4%;
		}
	    .feature-title-02 {
    		width:92%;margin:0% 4% 0% 4%;font-size: 24px;
	    }

		.feature-box-04{
			height:400px;
		}
		.feature-bg-04{
    		width:92%;margin:0% 4% 0% 4%;
		}
	    .feature-title-04 {
    		width:92%;margin:0% 4% 0% 4%;font-size: 24px;
	    }

		.feature-box-06{
			height:400px;
		}
		.feature-bg-06{
    		width:92%;margin:0% 4% 0% 4%;
		}
	    .feature-title-06 {
    		width:92%;margin:0% 4% 0% 4%;font-size: 24px;
	    }

		.feature-box-08{
			height:360px;
		}
		.feature-bg-08{
    		width:92%;margin:0% 4% 0% 4%;
		}
	    .feature-title-08 {
    		width:92%;margin:0% 4% 0% 4%;font-size: 24px;
	    }
	}
</style>
<div class="container">
  <div class="row service-box padding-top-40 margin-bottom-40" id="section-service">
  	<div class="col-md-6 col-lg-6">
  		<img src="public/img/ewedding-feature-01.png" class="img-responsive">
  	</div>
  	<div class="col-md-6 col-lg-6">
  		<div class="feature-title">
  			手機及電腦瀏覽一次搞定<br>
  			Mobile Friendly
  		</div>
  		<div class="feature-desc">
  			一次擁有手機、平板、電腦三種版本的婚禮網站．即使使用手機瀏覽網站也無障礙．隨時隨地和親朋好友分享最新結婚資訊．
  		</div>
  	</div>
  </div>
</div>
<div class="feature-box-02 margin-bottom-40">
	<div style="height:120px;"></div>
	<div style="position: relative;">
		<div class="feature-bg-02"></div>
		<div class="feature-title-02">
			自訂專屬的獨立網址<br>
			Custom Domain Name
	  		<div class="feature-desc-02">
	  			一次擁有手機、平板、電腦三種版本的婚禮網站．即使使用手機瀏覽網站也無障礙．隨時隨地和親朋好友分享最新結婚資訊．
	  		</div>
		</div>
	</div>
</div>
<div class="container">
  <div class="row service-box padding-top-40">
  	<div class="col-md-6 col-lg-6">
  		<div class="feature-title">
  			電子婚訊提早發佈婚訊<br>
  			Save the Date
  		</div>
  		<div class="feature-desc">
  			透過網路第一時間分享結婚的訊息，讓親友們能在3個月前或更早安排行程，並將時間保留下來．貼心的倒數日期讓親友與你們一起期待婚禮的到來．
  		</div>
  	</div>
  	<div class="col-md-6 col-lg-6">
  		<img src="public/img/ewedding-feature-03.png" class="img-responsive">
  	</div>
  </div>
</div>
<div class="feature-box-04 margin-bottom-40">
	<div style="height:120px;"></div>
	<div style="position: relative;">
		<div class="feature-bg-04"></div>
		<div class="feature-title-04">
			多場活動回函線上完成<br>
			Multiple Online RSVP
	  		<div class="feature-desc-04">
	  			可以一次建立多個活動回函，不管是訂婚、結婚、補請、教堂儀式或婚禮派對，賓客都可透過手機畫面輕鬆回覆出席資訊．統計出席人數及用餐需求更快速精準，酒席桌數不再浪費．
	  		</div>
		</div>
	</div>
</div>
<div class="container">
  <div class="row service-box padding-top-40 margin-bottom-40" id="section-service">
  	<div class="col-md-6 col-lg-6">
  		<img src="public/img/ewedding-feature-05.png" class="img-responsive">
  	</div>
  	<div class="col-md-6 col-lg-6">
  		<div class="feature-title">
  			搭配同系列喜帖設計更完美<br>
  			Matching Invitation Cards
  		</div>
  		<div class="feature-desc">
  			多款專業設計的網站版型適合各種婚禮主題，可搭配同系列喜帖、回函卡、謝卡，更可印上 QR Code ，一碼進入婚禮網站，讓你們的婚禮更吸睛．
  		</div>
  	</div>
  </div>
</div>
<div class="feature-box-06 margin-bottom-40">
	<div style="height:120px;"></div>
	<div style="position: relative;">
		<div class="feature-bg-06"></div>
		<div class="feature-title-06">
			盡情分享照片影音紀錄<br>
			Share Photos & Videos
	  		<div class="feature-desc-06">
	  			支援相簿及影音播放功能．可輕鬆分享最美的婚紗相片，也可收藏最精彩的婚禮影音紀錄，讓親朋好友們透過剪影一起參與你們幸福的結婚過程．
	  		</div>
		</div>
	</div>
</div>
<div class="container">
  <div class="row service-box padding-top-40 margin-bottom-40" id="section-service">
  	<div class="col-md-6 col-lg-6">
  		<img src="public/img/ewedding-feature-07.png" class="img-responsive">
  	</div>
  	<div class="col-md-6 col-lg-6">
  		<div class="feature-title">
  			傳遞愛的故事與感謝<br>
  			Tell Your Story and Thanks
  		</div>
  		<div class="feature-desc">
  			還記得兩人的相遇嗎？交往過程的重要回憶都可以記錄下來．婚禮的籌備過程有一路相伴的好友，也有辛苦的廠商工作人員，都可以在「感謝有你」的單元中傳遞你們的愛與感謝．
  		</div>
  	</div>
  </div>
</div>
<div class="feature-box-08 margin-bottom-60">
	<div style="height:120px;"></div>
	<div style="position: relative;">
		<div class="feature-bg-08"></div>
		<div class="feature-title-08">
			線上也能包禮金<br>
			Gift Registry
	  		<div class="feature-desc-08">
	  			貼心的線上禮金服務，讓無法前來的親朋好友也能獻上一份心意．
	  		</div>
		</div>
	</div>
</div>
<div class="container">
  <div class="service-box padding-top-40 margin-bottom-40" id="section-service">
  	<div class="row">
	  	<div class="col-md-12">
	  		<h2 class="feature-list-title">貼心的功能設計，讓喜訊分享更便利</h2>
	  	</div>
	</div>
	<div class="row">
	  	<div class="col-md-12">
		  	<div class="row feature-icon-list">
	  			<div class="col-sm-1"></div>
	  			<div class="col-sm-2">
	  				<img src="public/img/ewedding-feature-icon-01.png" class="img-responsive"><br>
	  				婚紗相簿先放閃
	  			</div>
	  			<div class="col-sm-2">
	  				<img src="public/img/ewedding-feature-icon-02.png" class="img-responsive"><br>
	  				愛的影音說故事
	  			</div>
	  			<div class="col-sm-2">
	  				<img src="public/img/ewedding-feature-icon-03.png" class="img-responsive"><br>
	  				留言互動更熱烈
	  			</div>
	  			<div class="col-sm-2">
	  				<img src="public/img/ewedding-feature-icon-04.png" class="img-responsive"><br>
	  				地圖導航不迷路
	  			</div>
	  			<div class="col-sm-2">
	  				<img src="public/img/ewedding-feature-icon-05.png" class="img-responsive"><br>
	  				婚禮倒數更期待
	  			</div>
	  			<div class="col-sm-1"></div>
	  		</div>
		  	<div class="row feature-icon-list">
	  			<div class="col-sm-1"></div>
	  			<div class="col-sm-2">
	  				<img src="public/img/ewedding-feature-icon-06.png" class="img-responsive"><br>
	  				電子喜帖更便利
	  			</div>
	  			<div class="col-sm-2">
	  				<img src="public/img/ewedding-feature-icon-07.png" class="img-responsive"><br>
	  				回函管理更輕鬆
	  			</div>
	  			<div class="col-sm-2">
	  				<img src="public/img/ewedding-feature-icon-08.png" class="img-responsive"><br>
	  				賓客統計不煩惱
	  			</div>
	  			<div class="col-sm-2">
	  				<img src="public/img/ewedding-feature-icon-09.png" class="img-responsive"><br>
	  				一鍵分享更便利
	  			</div>
	  			<div class="col-sm-2">
	  				<img src="public/img/ewedding-feature-icon-10.png" class="img-responsive"><br>
	  				DVD 封存留回憶
	  			</div>
	  			<div class="col-sm-1"></div>
	  		</div>
		  	<div class="row feature-icon-list">
	  			<div class="col-sm-1"></div>
	  			<div class="col-sm-2">
	  				<img src="public/img/ewedding-feature-icon-11.png" class="img-responsive"><br>
	  				完成網站五分鐘
	  			</div>
	  			<div class="col-sm-2">
	  				<img src="public/img/ewedding-feature-icon-12.png" class="img-responsive"><br>
	  				密碼設定護隱私
	  			</div>
	  			<div class="col-sm-2">
	  				<img src="public/img/ewedding-feature-icon-13.png" class="img-responsive"><br>
	  				自訂網址更好記
	  			</div>
	  			<div class="col-sm-2">
	  				<img src="public/img/ewedding-feature-icon-14.png" class="img-responsive"><br>
	  				簡訊提醒不忘記
	  			</div>
	  			<div class="col-sm-2">
	  				<img src="public/img/ewedding-feature-icon-15.png" class="img-responsive"><br>
	  				線上禮金無國界
	  			</div>
	  			<div class="col-sm-1"></div>
		  	</div>
	  	</div>
  	</div>
	<div class="row margin-bottom-60" style="text-align:center;">
		<a class="btn btn-lg btn-freetry" href="javascript:;">立即免費體驗</a>
  	</div>
</div>






