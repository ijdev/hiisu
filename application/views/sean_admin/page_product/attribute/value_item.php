<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品分類 <small>分類設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="<?php echo $init_control;?>">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        商品分類
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="<?php echo $init_control;?>product/productAttribute">分類屬性</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="<?php echo $init_control;?>product/productAttributeValue">分類屬性-<?php echo $_father_name["attribute_name"];?></a>
        <i class="fa fa-circle"></i>
      <li class="active">
         分類屬性值設定
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">分類屬性值設定</span>
            </div>
            <div class="actions btn-set">
              <a href="<?php echo $_father_link;?>" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <?php $this->load->view('admin-ijwedding/general/flash_error');?>
            <form class="form-horizontal" role="form" action="<?php echo $_action_link;?>">
              
              <?php

									$_form_one= new sean_form_general();
									
											
									$em_columns = array(										
										  $_table_field["分類屬性值"]=>array('header'=>"分類屬性值",'type'=>'textbox', 'required'=>'required', 'required_message'=>"",'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'maxlength'=>'', 'default'=>'', 'source'=>'', 'rows'=>'15', 'cols'=>'50'),  	
										   $_table_field["排序"]=>array('header'=>"排序",'type'=>'textbox', 'required'=>'required', 'required_message'=>"",'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'maxlength'=>'', 'default'=>'0', 'source'=>'', 'rows'=>'15', 'cols'=>'50'),  	
										  $_table_field["是否啟用"]=>array('header'=>"是否啟用",'type'=>'status', 'required'=>'required', 'required_message'=>"",'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'maxlength'=>'', 'default'=>'', 'source'=>'', 'rows'=>'15', 'cols'=>'50'),  	
									);
								 		  if(isset($_result01) && is_array($_result01))
										 	{
										 		$_form_one->add_item($em_columns,$_result01);
												
											}else{
												$_form_one->add_item($em_columns);
											}
									
							?>
               
               <!--
              
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label"> <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label for="seq" class="col-lg-3 control-label">排序</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" value="0">
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">是否啟用 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <select class="form-control">
                      <option>是</option>
                      <option>否</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-lg-3 control-label">更新者</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="administrator" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-3 control-label">更新時間</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="2015-05-25 10:00" Disabled>
                  </div>
                </div>
              </div>
              -->
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                    <a href="<?php echo $_father_link;?>"><button type="button" class="btn default">Cancel</button></a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->