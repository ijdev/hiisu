<?php
set_include_path(APPPATH . 'third_party/' . PATH_SEPARATOR .     get_include_path());
require APPPATH .'third_party/newwebpay/newconf.php';
require APPPATH .'third_party/newwebpay/mwt-newebpay_sdk.php';
class Newwebpay {
	protected $CI;
	public function __construct(){
		$this->CI =& get_instance();
	}

public function ccard_pay($order_num,$payment_method,$total,$email=''){
	global $merchantID,$hashKey,$hashIV,$url,$ver,$ReturnURL,$NotifyURL,$ClientBackURL,$ATM_ExpireDate,$Order_Title;
	if($order_num && $total){
		/* 送給藍新資料 預設刷卡付清*/
		$trade_info_arr = array(
			'MerchantID' => $merchantID,
			'RespondType' => 'JSON',
			'TimeStamp' => time(),
			'Version' => $ver,
			'MerchantOrderNo' => $order_num,
			'Amt' => $total,
			'ItemDesc' => $Order_Title,
			'CREDIT' => 1,
			'VACC' => 0,//ATM
			'ReturnURL' => $ReturnURL, //支付完成 返回商店網址
			'NotifyURL' => $NotifyURL, //支付通知網址
			'CustomerURL' =>'', //商店取號網址
			'Email' =>$email, //付款人電子信箱
			'ClientBackURL' => $ClientBackURL, //支付取消 返回商店網址
			'OrderComment' =>'', //商店備註
		);
		if($payment_method==2){ //刷卡分三期
			$trade_info_arr['CREDIT']=0;
			$trade_info_arr['InstFlag']=3;
		}elseif($payment_method==9){ //WEBATM
			$trade_info_arr['CREDIT']=0;
			$trade_info_arr['VACC']=1;
			$trade_info_arr['WEBATM']=1;
			$trade_info_arr['ExpireDate']=date("Y-m-d" , mktime(0,0,0,date("m"),date("d")+$ATM_ExpireDate,date("Y")));
		}elseif($payment_method==8){ //實體ATM
			$trade_info_arr['CREDIT']=0;
			$trade_info_arr['VACC']=1;
			$trade_info_arr['WEBATM']=0;
			$trade_info_arr['NotifyURL'].='/2';
			$trade_info_arr['ExpireDate']=date("Y-m-d" , mktime(0,0,0,date("m"),date("d")+$ATM_ExpireDate,date("Y")));
		}elseif($payment_method==11){
			$trade_info_arr['BARCODE']=1;
			$trade_info_arr['CREDIT']=0;
			$trade_info_arr['VACC']=0;
		}elseif($payment_method==12){
			$trade_info_arr['CVS']=1;
			$trade_info_arr['CREDIT']=0;
			$trade_info_arr['VACC']=0;
		}
        //echo '<pre>' . var_export($trade_info_arr,true) . '</pre>';exit;

		if ($total>0){
			$TradeInfo = create_mpg_aes_encrypt($trade_info_arr, $hashKey, $hashIV);
			$SHA256 = strtoupper(hash("sha256", SHA256($hashKey,$TradeInfo,$hashIV)));
			echo CheckOut($url,$merchantID,$TradeInfo,$SHA256,$ver);
		}else{
			return false;
		}
	}else{
		return false;
	}
}
public function ccard_return($p=3){
	global $hashKey,$hashIV;
	$TradeInfo = file_get_contents("php://input");
	if(!$TradeInfo) $TradeInfo=$this->CI->input->post('TradeInfo');
	//$TradeInfo="&&&TradeInfo=c3a01da6ccb192f5e334e2160e03213dea5a229c41eed1a22eac6de523d251327c8ef8eac9dbb20adda5139d023802808fb688deb1a81e79a7e01b337dc725b1fcde9f979bcd88d1a292bbf6aa97331d299485266f0a7ae7477212712133a4c9f664fef0e5774734f3997ba922f9069b35751dbb1ac3909582ffe99e5a5883f81f667f718846073639e7268624e8cea4656b62a6d027b56e6e73bd31cf2dcd93597f2fb44bd1918ec09a4291c652042a5c9d82551382293e62f7c07f81c7ad2a8156cfac2ce4b82b57e8d8051d9451eb4ad3d3854af991a5f392776d743a6f2213a7591cc0270bf63b604f04105325433bd13eb56dc3cdbed110fd4c6161827624d080da1e1847a4f7da1c91ea468523cf5dd831db9e228f9e07e84556d603e8620bcbe6755cc0e491473b64e712033acd67f750e848e18acf071fd76501d2d4d61119fed158b607a53abda784ee9b7c33427b08bccdce0871764244059d52e42fb4cf5fc0806e24e67b7b8ff3275ea7ffca0a6b1673ca7f16e0e7e37cdb4005d780daa33e3746964f89e79996035e7deb8c8ad662c706d34281ccf857d34012fc5ce8899b771a1d311fbb065a0a5adf62cc18ed94316c7b4d2b5222ee2420a4";
	if($TradeInfo){
		$arr = mb_split("&",$TradeInfo);
		$get_aes = str_replace("TradeInfo=","",$arr[$p]);
		$data = create_aes_decrypt($get_aes,$hashKey,$hashIV);
		return json_decode($data,true);
	}else{
		show_error('查無交易資料');
	}
}
}