<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>系統管理員 <small>資料編輯</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">會員資料編輯</span>
            </div>
            <div class="actions btn-set">
              <a href="<?php echo $init_control;?><?php echo $_father_link;?>" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <div class="tabbable">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_general" data-toggle="tab"> 一般設定 </a>
                </li>
                <?php
                if($_action_now!="add" and $_action_now!="adding"){
	                echo '<li><a href="#tab_qa" data-toggle="tab"> 客服紀錄 </a></li>';
	                if(!$ifeditself){
	                	echo '<li><a href="#tab_admin" data-toggle="tab"> 權限角色設定 </a></li>';
	                }
              	}
                ?>
              </ul>
              <div class="tab-content no-space">
                <!-- Tab 資料設定 -->
                <div class="tab-pane active" id="tab_general">
                 <?php $this->load->view('sean_admin/general/flash_error');?>
                 <form id="register-form" <?php
                 	if(isset($_action_link))
                 	echo " action=\"".$_action_link."\" ";
                 	?> class="form-horizontal error" novalidate="">
                  <div class="form-group">
                    <label for="name" class="col-lg-3 control-label">會員身分<span class="require">*</span></label>
                    <div class="col-lg-3">
                      <select name="member_level_type" required id="member_level_type" class="form-control">
                        <option value="">請選擇</option>
                         <?php
                           foreach($_member_level_types as $values){
                              echo  '<option value="'.$values->member_level_type.'"';
                              if(@$_result01[0]->member_level_type==$values->member_level_type)
                              echo ' selected="selected"';
                              echo '>'.$values->member_level_type_name.'</option>';
                           }
                        ?>
                      </select>
                    </div>
                  </div>
                 	<?php
										if(isset($em_columns) && is_array($em_columns))
										{
										 	$_form_one= new sean_form_general();

										 	if(isset($_result01) && is_array($_result01))
										 	{
										 		$_form_one->add_item($em_columns,$_result01);

											}else{
												$_form_one->add_item($em_columns);
											}
										}

									?>

                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                          <button type="submit" class="btn green">儲存</button>
                          <a href="<?php echo $init_control;?><?php echo $_father_link;?>" class="btn green"><i class="fa fa-list"></i> <?php echo $this->lang->line("cencel_list");?></a>

                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- End of Tab 資料設定 -->
                <!-- Tab 權限角色設定 -->
                <div class="tab-pane" id="tab_admin">
                 <?php $this->load->view('sean_admin/general/flash_error');?>
                 <form <?php
                 	if(isset($_action_link))
                 	echo " action=\"".$_action_link."\" ";
                 	?> class="form-horizontal error" novalidate="">

                    <div class="form-group">
                      <label for="note" class="col-md-2 control-label">角色設定</label>
                      <div class="col-md-10">
                        <div class="checkbox-list">
                        	<?php foreach($member_roles as $mr){?>
                          <label class="checkbox-inline">
                            <input class="checkbox-list" type="checkbox" class="form-control" name="member_roles[<?php echo @$mr['member_role_relation_sn'];?>]" value="1" <?php if(@$mr['status']=='1') echo 'checked';?> >&nbsp;<?php echo $mr['role_name'];?>
                          </label>
                        <?php }?>
                        </div>
                      </div>
                    </div>

                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                          <button type="submit" class="btn green">儲存</button>
                          <a href="<?php echo $init_control;?><?php echo $_father_link;?>" class="btn green"><i class="fa fa-list"></i> <?php echo $this->lang->line("cencel_list");?></a>

                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- End of Tab 權限角色設定 -->
                <!-- Tab 訂單紀錄 -->
                <div class="tab-pane" id="tab_order">
                  <table class="table table-striped table-bordered table-hover" id="table_order">
                    <thead>
                      <tr>
                        <th>訂單編號</th>
                        <th>收件人姓名</th>
                        <th>付款方式</th>
                        <th>運費</th>
                        <th>應收總價</th>
                        <th>訂單日期</th>
                        <th>訂單狀態</th>
                        <th>付款狀態</th>
                        <th>配送狀態</th>
                        <th>詳細資訊</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php for($i=0;$i<8;$i++):?>
                        <tr class="odd gradeX">
                          <td>2345682</td>
                          <td>Dxsc</td>
                          <td>Credit Card</td>
                          <td>70</td>
                          <td>870</td>
                          <td><small>2015-05-20 12:00:00</small></td>
                          <td class="text-center">
                            <?php if($i%3==0):?>
                              <span class="label label-default">已結束</span>
                            <?php elseif($i%3==1):?>
                              <span class="label label-info">處理中</span>
                            <?php else:?>
                              <span class="label label-danger">未處理</span>
                            <?php endif;?>
                          </td>
                          <td class="text-center">
                            <?php if($i%2==0):?>
                              <span class="label label-warning">未付款</span>
                            <?php else:?>
                              <span class="label label-success">已付款</span>
                            <?php endif;?>
                          </td>
                          <td>未發貨</td>
                          <td>
                            <a href="preview/orderItem"><i class="fa fa-edit"></i>訂單內容</a>
                          </td>
                        </tr>
                      <?php endfor;?>
                    </tbody>
                  </table>
                  <!-- page nav -->
                  <div class="row">
                    <div class="col-md-5 col-sm-12">
                      <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
                    </div>
                    <div class="col-md-7 col-sm-12">
                      <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                        <ul class="pagination" style="visibility: visible;">
                          <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                          <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                          <li><a href="#">3</a></li>
                          <li><a href="#">4</a></li>
                          <li class="active"><a href="#">5</a></li>
                          <li><a href="#">6</a></li>
                          <li><a href="#">7</a></li>
                          <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                          <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!-- end of page nav -->
                </div>
                <!-- End of Tab 訂單紀錄 -->
                <!-- Tab Q&A -->
                <div class="tab-pane" id="tab_qa">
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">問題列表</span>
            </div>
            <!--div class="tools">
              <a href="javascript:;" class="collapse">
              </a>
              <a href="javascript:;" class="reload">
              </a>
            </div-->
          </div>
<form id="form1" class="form-horizontal" role="form" method="post" action="admin/main/sys_member_item/edit/<?php echo $_id?>#tab_qa">
          <div class="portlet-body">
            <div class="note note-warning">
              <div class="input-group input-large date-picker input-daterange" data-date="<?php echo $from;?>" data-date-format="yyyy-mm-dd">
                <span class="input-group-addon"> 日期 </span>
                <input type="text" class="form-control daterange" name="from" value="<?php echo $from;?>">
                <span class="input-group-addon"> 到 </span>
                <input type="text" class="form-control daterange" name="to" value="<?php echo $to;?>">
              </div>
            </div>
</form>
                  <div class="table-scrollable">
                    <!-- ticket 列表 -->
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>表單時間</th>
                  <th>編號</th>
                  <th>Email</th>
                  <th>電話</th>
                  <th>問題類型</th>
                  <th>問題主題</th>
                  <th>訂單編號</th>
                  <th>狀態</th>
                  <th>更新時間</th>
                </tr>
              </thead>
              <tbody>
              <?php if($messages){ foreach($messages as $key=>$_Item){?>
                  <tr>
                    <td><?php echo $_Item['issue_date']?></td>
                    <td><?php echo $_Item['member_question_sn']?></td>
                    <td><?php echo $_Item['email']?></td>
                    <td><?php echo $_Item['cell']?></td>
                    <td><?php echo $_Item['ccapply_detail_code_name']?></td>
                    <td><?php echo $_Item['question_subject']?></td>
                    <td><?php echo $_Item['associated_sub_order_sn']?><a href="admin/admin/orderItem/<?php echo $_Item['associated_sub_order_sn'];?>" target="_blank">&nbsp;<i class="fa fa-external-link"></i></a></td>
                    <td>
                      <a href="admin/admin/callcenterMsg/<?php echo $_Item['member_question_sn']?>" data-remote="false" data-target="#ajax_msg" data-toggle="modal" class="label label-sm label-<?php echo $_Item['description']?>"><?php echo $_Item['member_question_status_name']?></a>
                      <!-- label-success(綠), info(藍), danger(紅), warning(黃), table : ij_callcenter_status -->
                    </td>
                    <td><?php echo $_Item['last_time_update1']?></td>
                  </tr>
                <?php }}?>
              </tbody>
            </table>
                    <!-- /ticket 列表 -->
                  </div>
					</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
<!-- Modal 顯示與客服的訊息列表 -->
<div class="modal fade" id="ajax_msg" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">訊息記錄</h4>
      </div>
      <div class="modal-body">
        <img src="public/metronic/global/img/loading-spinner-grey.gif" alt="" class="loading">
        <span>
        &nbsp;&nbsp;內容載入中... </span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn blue">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- /Modal 顯示與客服的訊息列表 -->
<!-- Modal 顯示與客服的訊息列表 -->
<div class="modal fade" id="ajax_msg_done" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">訊息記錄</h4>
      </div>
      <div class="modal-body">
        <img src="public/metronic/global/img/loading-spinner-grey.gif" alt="" class="loading">
        <span>
        &nbsp;&nbsp;內容載入中... </span>
      </div>
    </div>
  </div>
</div>
<!-- /Modal 顯示與客服的訊息列表 -->