<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Home 主內容
*/
?>
<style>

.ftbar select{
    width: 20%;
    float: right;
}
.ftbar label {
    width: 78%;
    text-align: right;
}
  .note-title{
    color:#cebea2;
  }
  .fontred{
	  color:red;
  }
  .btngr{
	  float:left;
	  width:49%;
	  background-color:#666666;
	  color:white;
	  padding:7px;
	  margin-right:2%;
	  text-align:center;
  }
  .btn-freetry {
    background-color: white;
	float:left;
    color: #000;
    border: 1px solid #000;
    width: 49%;
}
  .btn-buy{
    background-color: #F6B93A;
    color: #FFF;
    border: 0px solid #000;
  }
  .themeReviewBtn .mix-details{
    font-size: 20px;
    color: #FFF;
  }
  .themeReviewBtn .mix-details > i{
    font-size: 48px;
  }
  .themeFunc .colorBox{
    width: 28px;
    height: 28px;
    display: inline-block;
    vertical-align: middle;
    border:1px solid #666;
  }
    .themeFunc .colorBox span{
      width: 24px;
      height: 24px;
      display: inline-block;
      margin:1px;
    }
	
	.ui-slider {
    position: relative;
    text-align: left;
}
	.ui-slider-horizontal .ui-slider-range {
    top: 0;
    height: 0.5%;
}
	.ui-slider-horizontal .ui-slider-range {
    top: 0;
    
}
.ui-slider .ui-slider-range {
    position: absolute;
    z-index: 1;
    font-size: .7em;
    display: block;
    border: 0;
    background-position: 0 0;
}
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
    border: 1px solid #d3d3d3;
    background: #e6e6e6 url(images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x;
    font-weight: normal;
    color: #555555;
}
.ui-slider-horizontal .ui-slider-handle {
    top: -.3em;
    margin-left: -.6em;
}
.ui-slider .ui-slider-handle {
    position: absolute;
    z-index: 2;
    width: 1.2em;
    height: 1.2em;
    cursor: default;
}
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
    border: 1px solid #d3d3d3;
    background: #e6e6e6 url(images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x;
    font-weight: normal;
    color: #555555;
}
.ui-slider-horizontal .ui-slider-handle {
    top: -.3em;
    margin-left: -.6em;
}
.ui-slider .ui-slider-handle {
    position: absolute;
    z-index: 2;
    width: 1.2em;
    height: 1.2em;
    cursor: default;
}
	div#slider-range {
    background-size: 16px 16px;
    background-image: -webkit-linear-gradient(top left, transparent, transparent 25%, rgba(255, 255, 255, 0.3) 25%, rgba(255, 255, 255, 0.3) 50%, transparent 50%, transparent 75%, rgba(255, 255, 255, 0.3) 75%, rgba(255, 255, 255, 0.3));
    background-image: -moz-linear-gradient(top left, transparent, transparent 25%, rgba(255, 255, 255, 0.3) 25%, rgba(255, 255, 255, 0.3) 50%, transparent 50%, transparent 75%, rgba(255, 255, 255, 0.3) 75%, rgba(255, 255, 255, 0.3));
    background-image: -o-linear-gradient(top left, transparent, transparent 25%, rgba(255, 255, 255, 0.3) 25%, rgba(255, 255, 255, 0.3) 50%, transparent 50%, transparent 75%, rgba(255, 255, 255, 0.3) 75%, rgba(255, 255, 255, 0.3));
    background-image: linear-gradient(to bottom right, transparent, transparent 25%, rgba(255, 255, 255, 0.3) 25%, rgba(255, 255, 255, 0.3) 50%, transparent 50%, transparent 75%, rgba(255, 255, 255, 0.3) 75%, rgba(255, 255, 255, 0.3));
    background-color: #1ABC9C !important;
	
    left: 0%;
    width: 100%;

	
}
</style>
<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<div class="container">
<!--  <h3><span>版型設計</span></h3>-->
  <div class="row margin-bottom-40">
    <!-- SIDEBAR START -->
	<div id="shopcetlog" class="padding0 sidebar col-md-3 col-sm-4">
		<div class="  sidebar-filter categories-menu">
		<h3>商品分類</h3>
		<ul class="padding_left0 categories_list">
			<li class="categories_list_sub_li" data-toggle="collapse" data-target="#side_menu1">商品分類 1
				<ul id="side_menu1" class="collapse"> 
					<li><a href="/shop/shopCatalog/41">商品分類 1.1</a></li>
					<li><a href="/shop/shopCatalog/42">商品分類 1.2</a></li>
					<li><a href="/shop/shopCatalog/43">商品分類 1.3</a></li>
					<li><a href="/shop/shopCatalog/47">商品分類 1.4</a></li>
				</ul>
			</li>
			<li class="categories_list_sub_li" data-toggle="collapse" data-target="#side_menu2">商品分類 2
				<ul id="side_menu2" class="collapse"> 
					<li><a href="/shop/shopCatalog/42">商品分類 2.1</a></li>
					<li><a href="/shop/shopCatalog/47">商品分類 2.2</a></li>
					<li><a href="/shop/shopCatalog/41">商品分類 2.3</a></li>
					<li><a href="/shop/shopCatalog/43">商品分類 2.4</a></li>
				</ul>
			</li>
			<li class="categories_list_sub_lastli"><a href="/shop/shopCatalog/42">商品分類 3</a></li>
		</ul>
	 </div>
	 
	 <div class="sidebar-filter filter2">
      	
				
        
		

		
		<h3 class="filter-txt ">商品篩選
		<i id="icon-filter" data-toggle="collapse" data-target="#side_menu3" onclick="myFunction3()" class="fa fa-minus-square"></i>
		
		<i id="icon-filter3" data-toggle="collapse" data-target="#side_menu3" onclick="myFunction4()" class="fa fa-plus-square"></i>
		</h3>		
				<script>
		
					function myFunction3() {
						document.getElementById("icon-filter3").style.display = "block";
						document.getElementById("icon-filter").style.display = "none";
						}
					function myFunction4() {
    
						document.getElementById("icon-filter3").style.display = "none";
						document.getElementById("icon-filter").style.display = "block";
						}
				</script>

		<div class="inner_filter2 collapse in" id="side_menu3">
		 <script>
  $(document).ready(function(){
    $('.fdcheck').on('change', function(){
      var category_list = [];
      $('#filterschk :input:checked').each(function(){
        var category = $(this).val();
        category_list.push(category);
      });

      if(category_list.length == 0)
        $('.filterpro').fadeIn();
      else {
        $('.filterpro').each(function(){
          var item = $(this).attr('data-tag');
          if(jQuery.inArray(item,category_list) > -1)
            $(this).fadeIn('slow');
          else
            $(this).hide();
        });
      }   
    });
  }); 
  </script>
			<div id="filterschk" class="menu2_chk_box">
				
				<h4>分類(預設1)</h4>
				
					<div class="padding_left0 chk_box_lbl">
						<input type="checkbox" id="tab1_check" class="fdcheck" value="基礎保養" name="cate1">
						<label class="sphov" for="tab1_check"><span>基礎保養</span></label>
					</div>
					<div class="padding_left0 chk_box_lbl">
						<input type="checkbox" id="tab2_check" class="fdcheck" value="加強護理" name="cate2">
						<label class="sphov" for="tab2_check"><span>加強護理</span></label>
					</div>
					<div class="padding_left0 chk_box_lbl">
						<input type="checkbox" id="tab3_check" class="fdcheck" value="臉部其他" name="cate3">
						<label class="sphov" for="tab3_check"><span>臉部其他</span></label>
					</div>
			</div>
			
			
	<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 padding0 box1_sidecat">
				<div class="margin-bottom-10 size-15 fontwt400">尺寸</div>
    	            
         	<a class="tag" name="value3" href="javascript:getProduct('3','XS');">
        	<span class="txt XS" id="value22">XS</span>
			</a>
    	    <a class="tag" name="value3" href="javascript:getProduct('3','S');">
				<span class="txt L" id="value21">S</span>
			</a>
             <a class="tag" name="value3" href="javascript:getProduct('3','M');">
				<span class="txt M" id="value5">M</span>
			 </a>
              <a class="tag" name="value3" href="javascript:getProduct('3','L');">
				<span class="txt S" id="value1">L</span>
			   </a>
				<div style="clear:both"></div>	
		</div>
		  
		
		<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 padding0 box2_sidecat">
			<div class="margin-bottom-10 size-15 fontwt400">顏色</div>
    	         <a name="value4" href="javascript:getProduct('4','33b2c3');">
						<div id="value19" class="value 33b2c3 circle" style="float:left;width:30px;height:30px;margin: 0 5px 5px 5px;border-radius: 100%!important;background-color:#33b2c3;text-align: center;font-size: 18px;line-height: 30px; color:#fff;">
						</div>
					</a>
       
                  <a name="value4" href="javascript:getProduct('4','272f54');">
                	<div id="value18" class="value 272f54 circle" style="float:left;width:30px;height:30px;margin: 0 5px 5px 5px;border-radius: 100%;background-color:#272f54;text-align: center;font-size: 18px;line-height: 30px; color:#fff;">
						</div>
				 </a>
      
               	<a name="value4" href="javascript:getProduct('4','e2e9c7');">
                	<div id="value17" class="value e2e9c7 circle" style="float:left;width:30px;height:30px;margin: 0 5px 5px 5px;border-radius: 100%;background-color:#e2e9c7;text-align: center;font-size: 18px;line-height: 30px; color:#fff;">
					</div>
              	</a>
       
                 <a name="value4" href="javascript:getProduct('4','eedfb9');">
                	<div id="value11" class="value eedfb9 circle" style="float:left;width:30px;height:30px;margin: 0 5px 5px 5px;border-radius: 100%;background-color:#eedfb9;text-align: center;font-size: 18px;line-height: 30px; color:#fff;">
					</div>
              	</a>
        
               	<a name="value4" href="javascript:getProduct('4','881936');">
                	<div id="value10" class="value 881936 circle" style="float:left;width:30px;height:30px;margin: 0 5px 5px 5px;border-radius: 100%;background-color:#881936;text-align: center;font-size: 18px;line-height: 30px; color:#fff;">
					</div>
              	</a>
       
                <a name="value4" href="javascript:getProduct('4','000000');">
                	<div id="value7" class="value 000000 circle" style="float:left;width:30px;height:30px;margin: 0 5px 5px 5px;border-radius: 100%;background-color:#000000;text-align: center;font-size: 18px;line-height: 30px; color:#fff;">
					</div>
              	</a>
       
               	<a name="value4" href="javascript:getProduct('4','ffffff');">
        				<div id="value6" class="value ffffff circle" style="float:left;width:30px;height:30px;margin: 0 5px 5px 5px;border-radius: 100%;background-color:#ffffff;text-align: center;font-size: 18px;line-height: 30px; color:#eeeeee;border: 2px solid #eeeeee;">
							</div>
              	</a>
        
					<div style="clear:both"></div>	
		</div>
		
				
		<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 padding0 box3_sidecat">
			<div class="margin-bottom-10 size-15 fontwt400">容量 </div>
    	  <a class="tag" name="value5" href="javascript:getProduct('5','15ml');">
        	<span class="txt 200ml" id="value20">
					15ml
        	</span>
          </a>
          <a class="tag" name="value5" href="javascript:getProduct('5','30ml');">
        	<span class="txt 100ml" id="value16">
					30ml
        	</span>
          </a>
          <a class="tag" name="value5" href="javascript:getProduct('5','40ml');">
        	<span class="txt 30ml" id="value15">
					40ml
        	</span>
          </a>
    	   <a class="tag" name="value5" href="javascript:getProduct('5','150ml');">
        	<span class="txt 15ml" id="value14">
				150ml
        	</span>
          </a>
		<a class="tag" name="value5" href="javascript:getProduct('5','200ml');">
        	<span class="txt 250ml" id="value9">
				200ml
        	</span>
          </a>
      <a class="tag" name="value5" href="javascript:getProduct('5','250ml');">
        	<span class="txt 40ml" id="value8">
				250ml
        	</span>
          </a>
		<div style="clear:both"></div>	
	</div>
	
			<p>
				<label class="ranglabal" for="amount">範圍</label><br>
			</p>
				<div id="slider-range" class="margin-bottom-10 ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" style="    max-width: 200px;"><div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 100%;"></span></div>
			<p>
			<input type="text" id="amount" name="pricerange" style="border:0;">
			</p>
			<!--<p align="center" style="padding-top: 32px;">
						<button class="btn btn-default">進階搜尋</button>
			</p>-->
		</div>
      </div>

      <!--<div class="sidebar-products margin-bottom-40 clearfix">
        <h2>優惠商品</h2>
      </div>

      <div class="sidebar-products margin-bottom-40 clearfix">
        <h2>推薦商品</h2>
      </div>-->

      <div class="padding_left0 sidebar-products clearfix">
        <h2 class="pull-left">熱賣商品</h2>
        <div class="item">
          <a class="sidepro" href="shop/shopItem/13/42"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/product/13/1a079d367b0c77763dc9a7383d8d9d1b.jpg" alt="中式雷雕喜帖 - KC017V 紫梅囍鵲"></a>
          <h3><a href="shop/shopItem/13/42">中式雷雕喜帖 - KC017V 紫梅囍鵲</a></h3>
          <div class="price">$55&nbsp;<font style="font-size:12px;color:#333333;">起</font></div>
        </div>
        <div class="item">
          <a class="sidepro" href="shop/shopItem/10/42"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/product/10/d3b7137ad68e4e50e290425fcc20042c.jpg" alt="客製喜帖_1"></a>
          <h3><a href="shop/shopItem/10/42">客製喜帖_1</a></h3>
          <div class="price">$1&nbsp;<font style="font-size:12px;color:#333333;"></font></div>
        </div>
        <div class="item">
          <a class="sidepro" href="shop/shopItem/16/42"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/product/16/604ad9e1c0c1c946e3e8451d25f17173.jpg" alt="【信封燙金版費】喜帖信封燙金"></a>
          <h3><a href="shop/shopItem/16/42">【信封燙金版費】喜帖信封燙金</a></h3>
          <div class="price">$300&nbsp;<font style="font-size:12px;color:#333333;"></font></div>
        </div>
        <div class="item">
          <a class="sidepro" href="shop/shopItem/9/42"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/product/9/74882f1d6e211c93c5c28d6c15ccff19.jpg" alt="請您來喝囍酒"></a>
          <h3><a href="shop/shopItem/9/42">請您來喝囍酒</a></h3>
          <div class="price">$1&nbsp;<font style="font-size:12px;color:#333333;">起</font></div>
        </div>
        <div class="item">
          <a class="sidepro" href="shop/shopItem/11/42"><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/product/11/1bbc7514ad231312ff78557865b97d02.jpg" alt="喜帖_3"></a>
          <h3><a href="shop/shopItem/11/42">喜帖_3</a></h3>
          <div class="price">$69&nbsp;<font style="font-size:12px;color:#333333;">起</font></div>
        </div>
      </div>
		<!---------------------------blog content--------------------------------->
		<div class=" padding_left0 sidebar-products clearfix">
			<h2>本館文章</h2>
			<div class="blog_box">
				<div class="blog_img">
					<img src="/public/uploads/blog/c4153a3a5af73c78a3e884be91a67c7e.jpg">
				</div>
				<div class="blog_txt">
					<div class="blog-cnt-hrd">
						<h5>Heading</h5>
					</div>
					<p>Blog Dummy Text Blog Dummy Text Blog Dummy Text Blog Dummy Text</p>
				</div>
			</div>
			<div class="blog_box">
				<div class="blog_img">
					<img src="/public/uploads/blog/c4153a3a5af73c78a3e884be91a67c7e.jpg">
				</div>
				<div class="blog_txt">
					<div class="blog-cnt-hrd">
						<h5>Heading</h5>
					</div>
					<p>Blog Dummy Text Blog Dummy Text Blog Dummy Text Blog Dummy Text</p>
				</div>
			</div>
			<div class="blog_box">
				<div class="blog_img">
					<img src="/public/uploads/blog/c4153a3a5af73c78a3e884be91a67c7e.jpg">
				</div>
				<div class="blog_txt">
					<div class="blog-cnt-hrd">
						<h5>Heading</h5>
					</div>
					<p>Blog Dummy Text Blog Dummy Text Blog Dummy Text Blog Dummy Text</p>
				</div>
			</div>
			<div class="blog_box">
				<div class="blog_img">
					<img src="/public/uploads/blog/c4153a3a5af73c78a3e884be91a67c7e.jpg">
				</div>
				<div class="blog_txt">
					<div class="blog-cnt-hrd">
						<h5>Heading</h5>
					</div>
					<p>Blog Dummy Text Blog Dummy Text Blog Dummy Text Blog Dummy Text</p>
				</div>
			</div>
			<div class="blog_box">
				<div class="blog_img">
					<img src="/public/uploads/blog/c4153a3a5af73c78a3e884be91a67c7e.jpg">
				</div>
				<div class="blog_txt">
					<div class="blog-cnt-hrd">
						<h5>Heading</h5>
					</div>
					<p>Blog Dummy Text Blog Dummy Text Blog Dummy Text Blog Dummy Text</p>
				</div>
			</div>
			
		</div>
		<!------------------------------blog end------------------------------------------>
    </div>
	<!-- SIDEBAR END -->
	<!-- BEGIN CONTENT -->
    <div class="col-md-9 col-sm-8 W80 padding0">
						<div class="ad_product_image padding0">
									<img src="public/img/ad_product.jpg">
						</div>
						
						
						
						 <!-- <div class="theme-page">
							<div class="note row">
							  <div class="col-md-10 col-sm-8">
								<h4 class="block note-title">挑選適合您的版型</h4>
								<p>
								  訂製專屬你們的線上婚禮喜訊，輕鬆分享給親朋好友。免費體驗，喜歡再購買。
								</p>
							  </div>
							  <div class="col-md-2 col-sm-4">
								<div style="padding-top:16px;">
								  <a class="btn blue btn-lg" href="javascript:;">開始免費試用</a>
								</div>
							  </div>
							</div></div>-->
							<div class="col-md-12 col-sm-12 filter-bar">
			  <!---div class="pull-right">
				<label class="control-label">顯示數量:</label>
				<select class="form-control input-sm ajax_product" name="limit">
				  <option value="#?limit=24" selected="selected">24</option>
				  <option value="#?limit=25">25</option>
				  <option value="#?limit=50">50</option>
				  <option value="#?limit=75">75</option>
				  <option value="#?limit=100">100</option>
				</select>
			  </div-->
			  <div class="pull-right col-md-12 ftbar padding0">
				<label class="control-label ">排序依據:</label>
				<select class="form-control input-sm " name="sort" onchange="$('#form1').submit();">
				  <option value="sort_order">最新商品</option>
				  <option value="name_ASC">名稱 (A - Z)</option>
				  <option value="name_DESC">名稱 (Z - A)</option>
				  <option value="price_ASC">價格 (Low &gt; High)</option>
				  <option value="price_DESC">價格 (High &gt; Low)</option>
				  <option value="rating_DESC">訂購數量 (Highest)</option>
				  <option value="rating_ASC">訂購數量 (Lowest)</option>
				</select>
			  </div>
			</div>
							<div class="row padding0">
					<?php if($templates){ foreach($templates as $Item){
						?>
								<div class="col-md-6 col-sm-6 template-items">
								  <div class="row">
									<div class="themeBox">
									  <h2 class="fontred">明星最愛 - 包偉銘結婚版型</h2>
									   <div class="themeBox-demo pull-right">
										<a href="<?php echo $Item['template_demo_website'];?>" target="_blank"><i class="fa fa-play-circle-o"></i> DEMO</a>
									  </div>
									  <div style="clear:both;"></div>
									  <a href="/home/eweddingItem/19" class="themeReviewBtn" rel="<?php echo $Item['product_sn'];?>">
										<img src="public/uploads/<?php echo $Item['image_path'];?>">
										<div class="mix-details">
										  <div style="height:36%"></div>
										  <i class="icon-magnifier-add font-white-sharp "></i><br>
										  看更多細節
										</div>
									  </a>
									  <div class="themeImg-shadow"></div>
									 <a href="/home/eweddingItem/19"><h2><?php echo $Item['product_name'];?> 說明介紹說明介紹說明介紹紹說明介紹說明介紹紹說明介紹</h2></a>
									 <h2 class="fontred col-md-12 padding0 pull-left">
											 <small class="fontred">$</small> 
											 <strong class="fontred">2980</strong>
											 <small class="fontred">起</small>
									 </h2> 
									  
									 <div class="tmplistbtns col-md-12 padding0 pull-left">
											<a class="col-md-6 btngr padding0 pull-left"> 立即結帳</a>
											<a class="lwhitebtn btn btn-freetry btn-sm fancybox detailspopup add2cart" product_package_config_sn="<?php echo $Item['product_package_config_sn'];?>" product_sn="<?php echo $Item['product_sn'];?>" href="javascript:;">加入購物車</a>
									 </div>
									 
									  <!--<div class="themeFunc">
										<a class="btn blue btn-sm fancybox detailspopup" href="member/my/ewedding/<?php echo urlencode(base64_encode('add_free_site'));?>/<?php echo $Item['product_sn'];?>/">免費試用</a>
										<a class="btn btn-freetry btn-sm fancybox detailspopup add2cart" product_package_config_sn="<?php echo $Item['product_package_config_sn'];?>" product_sn="<?php echo $Item['product_sn'];?>" href="javascript:;">加入購物車</a>
										<!--a class="btn btn-buy btn-sm fancybox detailspopup shopaddcart" product_sn="<?php echo $Item['product_sn'];?>" href="javascript:;">直接購買</a-->
									   <!-- <a class="btn btn-buy btn-sm fancybox detailspopup" href="home/eweddingItem/<?php echo $Item['product_sn'];?>">詳細資料</a>
									  </div>
									  -->
									</div>
								  </div>
								</div>
					<?php }}?>
							</div>
							
							
							
							
							<div class="row padding0">
					<?php if($templates){ foreach($templates as $Item){
						?>
								<div class="col-md-6 col-sm-6 template-items">
								  <div class="row">
									<div class="themeBox">
									  <h2 class="fontred">明星最愛 - 包偉銘結婚版型</h2>
									   <div class="themeBox-demo pull-right">
										<a href="<?php echo $Item['template_demo_website'];?>" target="_blank"><i class="fa fa-play-circle-o"></i> DEMO</a>
									  </div>
									  <div style="clear:both;"></div>
									  <a href="/home/eweddingItem/19" class="themeReviewBtn" rel="<?php echo $Item['product_sn'];?>">
										<img src="public/uploads/<?php echo $Item['image_path'];?>">
										<div class="mix-details">
										  <div style="height:36%"></div>
										  <i class="icon-magnifier-add font-white-sharp "></i><br>
										  看更多細節
										</div>
									  </a>
									  <div class="themeImg-shadow"></div>
									 <a href="/home/eweddingItem/19"><h2><?php echo $Item['product_name'];?> 說明介紹說明介紹說明介紹紹說明介紹說明介紹紹說明介紹</h2></a>
									 <h2 class="fontred col-md-12 padding0 pull-left">
											 <small class="fontred">$</small> 
											 <strong class="fontred">2980</strong>
											 <small class="fontred">起</small>
									 </h2> 
									  
									 <div class="tmplistbtns col-md-12 padding0 pull-left">
											<a class="col-md-6 btngr padding0 pull-left"> 立即結帳</a>
											<a class="lwhitebtn btn btn-freetry btn-sm fancybox detailspopup add2cart" product_package_config_sn="<?php echo $Item['product_package_config_sn'];?>" product_sn="<?php echo $Item['product_sn'];?>" href="javascript:;">加入購物車</a>
									 </div>
									 
									  <!--<div class="themeFunc">
										<a class="btn blue btn-sm fancybox detailspopup" href="member/my/ewedding/<?php echo urlencode(base64_encode('add_free_site'));?>/<?php echo $Item['product_sn'];?>/">免費試用</a>
										<a class="btn btn-freetry btn-sm fancybox detailspopup add2cart" product_package_config_sn="<?php echo $Item['product_package_config_sn'];?>" product_sn="<?php echo $Item['product_sn'];?>" href="javascript:;">加入購物車</a>
										<!--a class="btn btn-buy btn-sm fancybox detailspopup shopaddcart" product_sn="<?php echo $Item['product_sn'];?>" href="javascript:;">直接購買</a-->
									   <!-- <a class="btn btn-buy btn-sm fancybox detailspopup" href="home/eweddingItem/<?php echo $Item['product_sn'];?>">詳細資料</a>
									  </div>
									  -->
									</div>
								  </div>
								</div>
					<?php }}?>
							</div>
							
							
							
							
							
							<div class="row  padding0">
					<?php if($templates){ foreach($templates as $Item){
						?>
								<div class="col-md-6 col-sm-6 template-items">
								  <div class="row">
									<div class="themeBox">
									  <h2 class="fontred">明星最愛 - 包偉銘結婚版型</h2>
									   <div class="themeBox-demo pull-right">
										<a href="<?php echo $Item['template_demo_website'];?>" target="_blank"><i class="fa fa-play-circle-o"></i> DEMO</a>
									  </div>
									  <div style="clear:both;"></div>
									  <a href="/home/eweddingItem/19" class="themeReviewBtn" rel="<?php echo $Item['product_sn'];?>">
										<img src="public/uploads/<?php echo $Item['image_path'];?>">
										<div class="mix-details">
										  <div style="height:36%"></div>
										  <i class="icon-magnifier-add font-white-sharp "></i><br>
										  看更多細節
										</div>
									  </a>
									  <div class="themeImg-shadow"></div>
									 <a href="/home/eweddingItem/19"><h2><?php echo $Item['product_name'];?> 說明介紹說明介紹說明介紹紹說明介紹說明介紹紹說明介紹</h2></a>
									 <h2 class="fontred col-md-12 padding0 pull-left">
											 <small class="fontred">$</small> 
											 <strong class="fontred">2980</strong>
											 <small class="fontred">起</small>
									 </h2> 
									  
									 <div class="tmplistbtns col-md-12 padding0 pull-left">
											<a class="col-md-6 btngr padding0 pull-left"> 立即結帳</a>
											<a class="lwhitebtn btn btn-freetry btn-sm fancybox detailspopup add2cart" product_package_config_sn="<?php echo $Item['product_package_config_sn'];?>" product_sn="<?php echo $Item['product_sn'];?>" href="javascript:;">加入購物車</a>
									 </div>
									 
									  <!--<div class="themeFunc">
										<a class="btn blue btn-sm fancybox detailspopup" href="member/my/ewedding/<?php echo urlencode(base64_encode('add_free_site'));?>/<?php echo $Item['product_sn'];?>/">免費試用</a>
										<a class="btn btn-freetry btn-sm fancybox detailspopup add2cart" product_package_config_sn="<?php echo $Item['product_package_config_sn'];?>" product_sn="<?php echo $Item['product_sn'];?>" href="javascript:;">加入購物車</a>
										<!--a class="btn btn-buy btn-sm fancybox detailspopup shopaddcart" product_sn="<?php echo $Item['product_sn'];?>" href="javascript:;">直接購買</a-->
									   <!-- <a class="btn btn-buy btn-sm fancybox detailspopup" href="home/eweddingItem/<?php echo $Item['product_sn'];?>">詳細資料</a>
									  </div>
									  -->
									</div>
								  </div>
								</div>
					<?php }}?>
							</div>
							
							
							
							<div class="row padding0">
					<?php if($templates){ foreach($templates as $Item){
						?>
								<div class="col-md-6 col-sm-6 template-items">
								  <div class="row">
									<div class="themeBox">
									  <h2 class="fontred">明星最愛 - 包偉銘結婚版型</h2>
									   <div class="themeBox-demo pull-right">
										<a href="<?php echo $Item['template_demo_website'];?>" target="_blank"><i class="fa fa-play-circle-o"></i> DEMO</a>
									  </div>
									  <div style="clear:both;"></div>
									  <a href="/home/eweddingItem/19" class="themeReviewBtn" rel="<?php echo $Item['product_sn'];?>">
										<img src="public/uploads/<?php echo $Item['image_path'];?>">
										<div class="mix-details">
										  <div style="height:36%"></div>
										  <i class="icon-magnifier-add font-white-sharp "></i><br>
										  看更多細節
										</div>
									  </a>
									  <div class="themeImg-shadow"></div>
									 <a href="/home/eweddingItem/19"><h2><?php echo $Item['product_name'];?> 說明介紹說明介紹說明介紹紹說明介紹說明介紹紹說明介紹</h2></a>
									 <h2 class="fontred col-md-12 padding0 pull-left">
											 <small class="fontred">$</small> 
											 <strong class="fontred">2980</strong>
											 <small class="fontred">起</small>
									 </h2> 
									  
									 <div class="tmplistbtns col-md-12 padding0 pull-left">
											<a class="col-md-6 btngr padding0 pull-left"> 立即結帳</a>
											<a class="lwhitebtn btn btn-freetry btn-sm fancybox detailspopup add2cart" product_package_config_sn="<?php echo $Item['product_package_config_sn'];?>" product_sn="<?php echo $Item['product_sn'];?>" href="javascript:;">加入購物車</a>
									 </div>
									 
									  <!--<div class="themeFunc">
										<a class="btn blue btn-sm fancybox detailspopup" href="member/my/ewedding/<?php echo urlencode(base64_encode('add_free_site'));?>/<?php echo $Item['product_sn'];?>/">免費試用</a>
										<a class="btn btn-freetry btn-sm fancybox detailspopup add2cart" product_package_config_sn="<?php echo $Item['product_package_config_sn'];?>" product_sn="<?php echo $Item['product_sn'];?>" href="javascript:;">加入購物車</a>
										<!--a class="btn btn-buy btn-sm fancybox detailspopup shopaddcart" product_sn="<?php echo $Item['product_sn'];?>" href="javascript:;">直接購買</a-->
									   <!-- <a class="btn btn-buy btn-sm fancybox detailspopup" href="home/eweddingItem/<?php echo $Item['product_sn'];?>">詳細資料</a>
									  </div>
									  -->
									</div>
								  </div>
								</div>
					<?php }}?>
							</div>
							
							
							
							
							<div class="row padding0">
					<?php if($templates){ foreach($templates as $Item){
						?>
								<div class="col-md-6 col-sm-6 template-items">
								  <div class="row">
									<div class="themeBox">
									  <h2 class="fontred">明星最愛 - 包偉銘結婚版型</h2>
									   <div class="themeBox-demo pull-right">
										<a href="<?php echo $Item['template_demo_website'];?>" target="_blank"><i class="fa fa-play-circle-o"></i> DEMO</a>
									  </div>
									  <div style="clear:both;"></div>
									  <a href="/home/eweddingItem/19" class="themeReviewBtn" rel="<?php echo $Item['product_sn'];?>">
										<img src="public/uploads/<?php echo $Item['image_path'];?>">
										<div class="mix-details">
										  <div style="height:36%"></div>
										  <i class="icon-magnifier-add font-white-sharp "></i><br>
										  看更多細節
										</div>
									  </a>
									  <div class="themeImg-shadow"></div>
									 <a href="/home/eweddingItem/19"><h2><?php echo $Item['product_name'];?> 說明介紹說明介紹說明介紹紹說明介紹說明介紹紹說明介紹</h2></a>
									 <h2 class="fontred col-md-12 padding0 pull-left">
											 <small class="fontred">$</small> 
											 <strong class="fontred">2980</strong>
											 <small class="fontred">起</small>
									 </h2> 
									  
									 <div class="tmplistbtns col-md-12 padding0 pull-left">
											<a class="col-md-6 btngr padding0 pull-left"> 立即結帳</a>
											<a class="lwhitebtn btn btn-freetry btn-sm fancybox detailspopup add2cart" product_package_config_sn="<?php echo $Item['product_package_config_sn'];?>" product_sn="<?php echo $Item['product_sn'];?>" href="javascript:;">加入購物車</a>
									 </div>
									 
									  <!--<div class="themeFunc">
										<a class="btn blue btn-sm fancybox detailspopup" href="member/my/ewedding/<?php echo urlencode(base64_encode('add_free_site'));?>/<?php echo $Item['product_sn'];?>/">免費試用</a>
										<a class="btn btn-freetry btn-sm fancybox detailspopup add2cart" product_package_config_sn="<?php echo $Item['product_package_config_sn'];?>" product_sn="<?php echo $Item['product_sn'];?>" href="javascript:;">加入購物車</a>
										<!--a class="btn btn-buy btn-sm fancybox detailspopup shopaddcart" product_sn="<?php echo $Item['product_sn'];?>" href="javascript:;">直接購買</a-->
									   <!-- <a class="btn btn-buy btn-sm fancybox detailspopup" href="home/eweddingItem/<?php echo $Item['product_sn'];?>">詳細資料</a>
									  </div>
									  -->
									</div>
								  </div>
								</div>
					<?php }}?>
							</div>
							
							
							
							
							<div class="row padding0">
					<?php if($templates){ foreach($templates as $Item){
						?>
								<div class="col-md-6 col-sm-6 template-items">
								  <div class="row">
									<div class="themeBox">
									  <h2 class="fontred">明星最愛 - 包偉銘結婚版型</h2>
									   <div class="themeBox-demo pull-right">
										<a href="<?php echo $Item['template_demo_website'];?>" target="_blank"><i class="fa fa-play-circle-o"></i> DEMO</a>
									  </div>
									  <div style="clear:both;"></div>
									  <a href="/home/eweddingItem/19" class="themeReviewBtn" rel="<?php echo $Item['product_sn'];?>">
										<img src="public/uploads/<?php echo $Item['image_path'];?>">
										<div class="mix-details">
										  <div style="height:36%"></div>
										  <i class="icon-magnifier-add font-white-sharp "></i><br>
										  看更多細節
										</div>
									  </a>
									  <div class="themeImg-shadow"></div>
									 <a href="/home/eweddingItem/19"><h2><?php echo $Item['product_name'];?> 說明介紹說明介紹說明介紹紹說明介紹說明介紹紹說明介紹</h2></a>
									 <h2 class="fontred col-md-12 padding0 pull-left">
											 <small class="fontred">$</small> 
											 <strong class="fontred">2980</strong>
											 <small class="fontred">起</small>
									 </h2> 
									  
									 <div class="tmplistbtns col-md-12 padding0 pull-left">
											<a class="col-md-6 btngr padding0 pull-left"> 立即結帳</a>
											<a class="lwhitebtn btn btn-freetry btn-sm fancybox detailspopup add2cart" product_package_config_sn="<?php echo $Item['product_package_config_sn'];?>" product_sn="<?php echo $Item['product_sn'];?>" href="javascript:;">加入購物車</a>
									 </div>
									 
									  <!--<div class="themeFunc">
										<a class="btn blue btn-sm fancybox detailspopup" href="member/my/ewedding/<?php echo urlencode(base64_encode('add_free_site'));?>/<?php echo $Item['product_sn'];?>/">免費試用</a>
										<a class="btn btn-freetry btn-sm fancybox detailspopup add2cart" product_package_config_sn="<?php echo $Item['product_package_config_sn'];?>" product_sn="<?php echo $Item['product_sn'];?>" href="javascript:;">加入購物車</a>
										<!--a class="btn btn-buy btn-sm fancybox detailspopup shopaddcart" product_sn="<?php echo $Item['product_sn'];?>" href="javascript:;">直接購買</a-->
									   <!-- <a class="btn btn-buy btn-sm fancybox detailspopup" href="home/eweddingItem/<?php echo $Item['product_sn'];?>">詳細資料</a>
									  </div>
									  -->
									</div>
								  </div>
								</div>
					<?php }}?>
							</div>
							<div class="row padding0">
							  <ul class="pagination text-center col-md-12">
														<?php echo $page_links;?>
							  </ul>   
							</div>
							
							
							
							
							
							
							
							
							
							
		</div>
	  
    </div>
	
    <!-- END CONTENT -->
  </div>
  <!-- END SIDEBAR & CONTENT -->
</div>