<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('libraries_model');
		$this->page_data['unreadmessage']= $this->libraries_model->get_unread_message_count();
		$this->load->library("ijw");
		$this->LoginCheck();
	}

	public function LoginCheck()
	{
		if($this->session->userdata('member_login')== true)
		return true;
		else
		redirect(base_url("/admin/login"));
		exit();
	}


	/**
	 *  客服中心回應內容
	 *	直接顯示在 model 中
	 *
	 */
	public function callcenterMsg($member_question_sn = false)
	{
			$this->LoginCheck();
			$page_data = array();

			$_question_status = $this->libraries_model->_select('ij_member_question_status_ct','display_flag','1',0,0,0,0,'result_array');
			$_question_close_status = $this->libraries_model->_select('ij_cc_question_close_ct','display_flag','1',0,0,'cc_question_close_code',0,'result_array');
			if($member_question_sn){
				$_result = $this->libraries_model->_get_question_qna_log($member_question_sn);
				$page_data['_result'] = $_result;
			}
			//var_dump($_result);
			// 內容設定
			if(@$_result[0]['question_status']=='5') //已結案
			{
				// 若為已解決， modal footer 不顯示
				$page_data['done'] = true;
			}
			else
			{
				$page_data['done'] = false;
			}
			$page_data['_question_status'] = $_question_status;
			$page_data['_question_close_status'] = $_question_close_status;
			$this->load->view('admin/page_callcenter/modal_ticket_msg', $page_data);

	}

  public function addCallcenter()
	{
		$this->LoginCheck();
		if($this->input->post()){
			if($this->input->post('reply',true)){
		    $data = array(
		        'member_question_sn' => $this->input->post('member_question_sn',true),
		        'qna_content' => $this->input->post('reply',true),
		        'qna_date'    => date("Y-m-d H:i:s",time()),
		        'create_date'    => date("Y-m-d H:i:s",time()),
		        'process_ccagent_member_sn'    => $this->session->userdata['member_data']['member_sn'],
		        'create_member_sn'    => $this->session->userdata['member_data']['member_sn'],
		        'cc_reply_flag'        => '1'
		    );
				$this->libraries_model->_insert('ij_member_question_qna_log',$data);
			}
	    $data2 = array(
	        'question_status' => $this->input->post('question_status',true),
	        'cc_faq_close_code' => $this->input->post('cc_faq_close_code',true)
	    );
			$data2 = $this->libraries_model->CheckUpdate($data2,0);
			$this->libraries_model->_update('ij_member_question',$data2,'member_question_sn',$this->input->post('member_question_sn',true));
			$this->session->set_userdata(array('Mes'=>'回覆或更改狀態成功！'));
			if($this->input->post('member_sn',true)){
				$question_description = @$this->libraries_model->_get_callcenter_list(array('member_question_sn'=>$this->input->post('member_question_sn',true)))[0]['question_description'];
				//var_dump($question_description);exit;
		    	$this->ijw->send_mail('廠商留言回覆','',$this->input->post('member_sn',true),nl2br($data['qna_content']),'',$this->input->post('supplier_name',true),nl2br($question_description));
		    }
		}

			redirect($_SERVER['HTTP_REFERER']);
			//redirect(base_url('admin/admin/ticketTable'));
	}

  public function addNewCallcenter()
	{
		$this->LoginCheck();
		if($this->input->post('reply',true)){
	    $data = array(
	        'member_question_sn' => $this->input->post('member_question_sn',true),
	        'qna_content' => $this->input->post('reply',true),
	        'qna_date'    => date("Y-m-d H:i:s",time()),
	        'create_date'    => date("Y-m-d H:i:s",time()),
	        'create_member_sn'    => $this->session->userdata['member_data']['member_sn'],
	        'cc_reply_flag'        => '0'
	    );
			$data = $this->libraries_model->CheckUpdate($data,1);
			$this->libraries_model->_insert('ij_member_question_qna_log',$data);
		}else{
			$ExpectManDay=$this->libraries_model->_select('ij_ccapply_detail_code_ct',array('ccapply_detail_code'=>$this->input->post('ccapply_detail_code',true)),0,0,0,'ExpectManDay','asc','row','ExpectManDay')['ExpectManDay'];
			$this->load->model('membership/Callcenter');
		    $data = array(
		        'member_sn' => $this->input->post('member_sn',true),
		        'ccapply_detail_code' => $this->input->post('ccapply_detail_code',true),
		        'issue_date'          => date("Y-m-d H:i:s",time()),
		        'issue_expected_close_date'          => date("Y-m-d H:i:s",time()+86400*$ExpectManDay),
		        'question_subject' =>  $this->input->post('question_subject',true),
		        'associated_sub_order_sn' =>  $this->input->post('associated_sub_order_sn',true),
		        'question_description' =>  $this->input->post('question_description',true),
		        'question_status' => "2"
		    );
			//var_dump($data);exit;
			$data = $this->libraries_model->CheckUpdate($data,1);
		    if($nonce = $this->Callcenter->create_callcenter($data))
			{
				$this->session->set_userdata(array('Mes'=>'訊息送出成功！'));
		    }else{
				$this->session->set_userdata(array('Mes'=>'訊息送出失敗！!'));
		    }
		}
			redirect(base_url('admin/order/orderTable'));
	}



	/**
	 * AJAX 根據館別-分類編號秀已選
	 *
	 */
	public function ChgStoreName(){
		$channel_name = urldecode($this->uri->segment(4));
		$channel_name = '囍市集'; //先固定
		$post_category_array = explode('、',urldecode($this->uri->segment(5)));
		//var_dump($channel_name);
		if($channel_name){
			$ij_categorys=$this->libraries_model->categoryParentChildTree($channel_name);
				foreach($ij_categorys as $crow){
					foreach($post_category_array as $crow2){
						//$w1=preg_replace('/\s(?=)/│/├─/', '', trim($crow2));
						//$w2=str_replace('&nbsp;','',trim($crow['category_name']));
						//$w2=preg_replace('/\s(?=)/│/├─/', '', $w2);
						if($crow2==$crow['pure_category_name']){
							//echo $w1;
							//echo $w2;
							$ifselect=' selected';
							break;
						}else{
							$ifselect='';
						}
					}
					if(strrpos($crow['category_name'], "│")===false){
						$style="style='height:16px;'".$ifselect;
					}else{
						$style="style='height:16px;margin-left: 1px;'".$ifselect;
					}
					echo "<option ".$style." value='".$crow['pure_category_name']."'>".$crow['category_name']."</option>";
				}
			//}
			//$this->db->stop_cache();
		}
	}
	public function ChgChannelName(){
		$channel_name = urldecode($this->uri->segment(4));
		$blog_faq_flag = ($this->uri->segment(5))?$this->uri->segment(5):'1';
		if($channel_name){
			$where=array('status'=>'1','post_channel_name_set'=>$channel_name,'blog_faq_flag'=>$blog_faq_flag);
			$Items=$this->libraries_model->_select('ij_content_type_config',$where,0,0,0,0,0,'result_array');
			//var_dump($Items);
			//echo (strrpos(@$Item['associated_content_type_sn_set'], $_Item['content_title'])!==false)?'checked':'';
			foreach($Items as $_item){
				/*echo '<div class="radio-inline">';
                echo '<label class="radio"><span><input type="radio" name="Item[associated_content_type_sn_set]"  value="'.$_item['content_title'].'"></span> '.$_item['content_title'].'</label>';
				echo '</div>';*/
				echo "<option value='".$_item['content_type_sn']."'>".$_item['content_title']."</option>";

			}
		}
	}
	/**
	 * 廣告檔期列表 Template
	 * 沒用
	 */
	public function adsSchedule()
	{
		$flg=$this->input->get('flg',true);
		if($flg=='reset'){
			redirect(base_url('admin/admin/adsMaterial'));
			exit();
		}else{
			$from=($this->input->get('from',true))? $this->input->get('from',true):date('Y-m-d', strtotime(date('Y-m-d'). ' - 7 days'));
			$to=($this->input->get('to',true))? $this->input->get('to',true):date('Y-m-d');
			$published_locaiotn_type=$this->input->get('published_locaiotn_type',true);
			$banner_location_sn=$this->input->get('banner_location_sn',true);
		}
		$where=array();
		if($from) $where['banner_eff_start_date >=']=$from.' 00:00:00';
		if($to) $where['banner_eff_end_date <=']=$to.' 23:59:59';
		if($published_locaiotn_type) $where['ij_banner_content.published_locaiotn_type']=$published_locaiotn_type;
		if($banner_location_sn) $where['ij_banner_content.banner_location_sn']=$banner_location_sn;


		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/schedule/table'; // 頁面主內容 view 位置, 必要

		$page_data['page_content']['Items']=$this->libraries_model->_get_adsMaterials(0,$where);
		$where1=array('display_flag'=>'1');
		$page_data['page_content']['published_locaiotn_types']=$this->libraries_model->_select('ij_published_locaiotn_type_ct',$where1,0,0,0,0,0,'result_array'); //頻道
		if($published_locaiotn_type){
			$where2['published_locaiotn_type']=$published_locaiotn_type;
			$page_data['page_content']['banner_locations']=$this->libraries_model->_select('ij_banner_location',$where2,0,0,0,'banner_location_name',0,'result_array'); //版位
		}else{
			$page_data['page_content']['banner_locations']=array();
		}
		$page_data['page_content']['from'] = $from;
		$page_data['page_content']['to'] = $to;
		$page_data['page_content']['published_locaiotn_type'] = $published_locaiotn_type;
		$page_data['page_content']['banner_location_sn'] = $banner_location_sn;

		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/schedule/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * AJAX 根據館別-分類編號秀已選
	 *
	 */
	public function Chg_locaiotn_type($published_locaiotn_type=0){
		if($published_locaiotn_type){
			$where1['published_locaiotn_type']=(int)$published_locaiotn_type;
			$banner_locations=$this->libraries_model->_get_adsSlots(0,$published_locaiotn_type); //版位
			//var_dump($ij_categorys);
			echo '<option value="">請選擇</option>';
				foreach($banner_locations as $crow){
					//foreach($post_category_array as $crow2){
						//$w1=preg_replace('/\s(?=)/│/├─/', '', trim($crow2));
						//$w2=str_replace('&nbsp;','',trim($crow['category_name']));
						//$w2=preg_replace('/\s(?=)/│/├─/', '', $w2);
						//if($crow2==$crow['pure_category_name']){
							//echo $w1;
							//echo $w2;
							//$ifselect=' selected';
							//break;
						//}else{
							$ifselect='banner_type_name="'.$crow['banner_type_name'].'"';
							$ifselect.=' banner_type="'.$crow['banner_type'].'"';
						//}
					//}
					echo "<option class='banner_location_sn' ".$ifselect." value='".$crow['banner_location_sn']."'>".$crow['banner_location_name']."</option>";
				}
			//}
			//$this->db->stop_cache();
		}
	}

	/**
	 * 兌換券列表 Template
	 *
	 */
	public function certificate()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/certificate/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/certificate/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 兌換券編輯 Template
	 *
	 */
	public function certificateItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/certificate/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/certificate/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 兌換券統計 Template
	 *
	 */
	public function certificateReport()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/certificate/report'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['status'] = array(
														array('label' => '未發送', 'num' => 200),
														array('label' => '已發送', 'num' => 100),
														array('label' => '已審核', 'num' => 300),
														array('label' => '已使用', 'num' => 300),
														array('label' => '失效', 'num' => 100)
														);
		$page_data['page_level_js']['enable'] = array(
														array('label' => '尚未使用', 'num' => 30000),
														array('label' => '已使用', 'num' => 70000)
														);
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/certificate/report_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 兌換券申請列表 Template
	 *
	 */
	public function certificateApply()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/certificate/table_apply'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/certificate/table_apply_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 兌換券申請編輯 Template
	 *
	 */
	public function certificateApplyItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/certificate/item_apply'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/certificate/item_apply_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 兌換券登錄 Template
	 *
	 */
	public function certificateActive()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/certificate/item_active'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/certificate/item_active_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}


	/**
	 * 虛擬折扣卷發送 Template
	 *
	 */
	public function ecouponSend()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/ecoupon/send'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/ecoupon/send_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 虛擬折扣卷發送名單 Template
	 *
	 */
	public function ecouponSendList()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/ecoupon/send_list'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/ecoupon/send_list_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 實體折扣卷編輯 Template
	 *
	 */
	public function couponItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/coupon/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/coupon/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 實體折扣卷統計 Template
	 *
	 */
	public function couponReport()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/coupon/report'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['status'] = array(
														array('label' => '未發送', 'num' => 200),
														array('label' => '已發送', 'num' => 100),
														array('label' => '已審核', 'num' => 300),
														array('label' => '已使用', 'num' => 300),
														array('label' => '失效', 'num' => 100)
														);
		$page_data['page_level_js']['enable'] = array(
														array('label' => '尚未使用', 'num' => 30000),
														array('label' => '已使用', 'num' => 70000)
														);
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/coupon/report_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}
	/**
	 * 實體折扣卷申請列表 Template
	 *
	 */
	public function couponApply()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/coupon/table_apply'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/coupon/table_apply_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 實體折扣卷申請編輯 Template
	 *
	 */
	public function couponApplyItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/coupon/item_apply'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/coupon/item_apply_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 實體折扣卷登錄 Template
	 *
	 */
	public function couponActive()
	{
		$page_data = array();
		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/coupon/item_active'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/coupon/item_active_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}


	/**
	 * 系統一般設定 Template
	 *
	 */
	public function basic()
	{
		//寫入資料庫
		if($data_array=$this->input->post('basic')){
			if(!$system_param_config_sn=$this->input->post('system_param_config_sn')){ //判斷新增或修改
				$data_array = $this->libraries_model->CheckUpdate($data_array,1);
				$system_param_config_sn=$this->libraries_model->_insert('ij_system_param_config',$data_array);
				$this->session->set_userdata(array('Mes'=>'一般參數新增成功!'));
			}else{
				$data_array = $this->libraries_model->CheckUpdate($data_array,0);
				$this->libraries_model->_update('ij_system_param_config',$data_array,'system_param_config_sn',$system_param_config_sn);
				$this->session->set_userdata(array('Mes'=>'一般參數修改成功!'));
			}
			redirect(base_url('admin/admin/basic'));
		}
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要

		$page_data['page_content']['Item']=$this->libraries_model->_select('ij_system_param_config','system_param_name','滿額免運門檻',0,0,0,0,'row');

		$page_data['page_content']['view_path'] = 'admin/page_system/basic/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_system/basic/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}


	/**
	 * 產業屬性列表 Template
	 *
	 */
	public function domain()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_system/domain/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_system/domain/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 產業屬性編輯 Template
	 *
	 */
	public function domainItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_system/domain/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_system/domain/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}


	/**
	 * 付款方式列表 Template
	 *
	 */
	public function payment()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_system/payment/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_system/payment/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 付款方式編輯 Template
	 *
	 */
	public function paymentItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_system/payment/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_system/payment/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}


	/**
	 * 運費列表 Template
	 *
	 */
	public function shipping()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_system/shipping/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_system/shipping/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 運費編輯 Template
	 *
	 */
	public function shippingItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_system/shipping/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_system/shipping/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}


	/**
	 * mail內容設定列表 Template
	 *
	 */
	public function mail()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_system/mail/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_system/mail/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * mail內容設定編輯 Template
	 *
	 */
	public function mailItem()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_system/mail/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_system/mail/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}


	/**
	 * doc內容設定列表 Template
	 *
	 */
	public function doc()
	{
		$page_data = array();

		// 內容設定

		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_system/doc/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_content']['Items']=$this->libraries_model->_select('ij_system_file_config',0,0,0,0,0,0,'result_array');

		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_system/doc/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * doc內容設定編輯 Template
	 *
	 */
	public function docItem($system_file_sn=0)
	{
		if($this->input->post()){
			if($this->input->post('system_file_sn')){ //判斷新增或修改
					$data_array['system_file_ontent']=$this->input->post('system_file_ontent');
					$data_array = $this->libraries_model->CheckUpdate($data_array,0);
					$this->libraries_model->_update('ij_system_file_config',$data_array,'system_file_sn',$this->input->post('system_file_sn'));
					$this->session->set_userdata(array('Mes'=>'系統文件更新成功！'));
					redirect(base_url('/admin/admin/doc'));
			}else{
					$data_array['system_file_ontent']=$this->input->post('system_file_ontent');
					$data_array['system_file_name']=$this->input->post('system_file_name');
					$data_array['status']='1';
					$data_array['sort_order']='9999';
					$data_array = $this->libraries_model->CheckUpdate($data_array,0);
					$data_array = $this->libraries_model->CheckUpdate($data_array,1);
					$this->libraries_model->_insert('ij_system_file_config',$data_array);
					$this->session->set_userdata(array('Mes'=>'系統文件新增成功！'));
					redirect(base_url('/admin/admin/doc'));
			}
		}

		$page_data = array();
		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_system/doc/item'; // 頁面主內容 view 位置, 必要
		$page_data['page_content']['Item']=$this->libraries_model->_select('ij_system_file_config','system_file_sn',$system_file_sn,0,0,0,0,'row');
		/*if(!$page_data['page_content']['Item']){
				$this->session->set_userdata(array('Mes'=>'查無此系統文件，請重新確認！'));
				redirect(base_url('/admin/admin/doc'));
		}*/
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_system/doc/item_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 分潤結帳 Template
	 *
	 */
	public function accounting()
	{
		$page_data = array();

		// 內容設定


		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_order/list/accounting'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_order/list/accounting_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}

	/**
	 * 婚宴地點管理 Template
	 *
	 */
	public function location($type="")
	{
		$page_data = array();

		// 內容設定

		switch($type){
			case "item":
				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'admin/page_system/location/item'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'admin/page_system/location/item_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('admin/general/main', $page_data);

			break;
			default:
				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'admin/page_system/location/table'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'admin/page_system/location/table_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('admin/general/main', $page_data);

			break;
		}
	}

	/**
	 * 活動查詢 Template
	 *
	 */
	public function event()
	{
		$page_data = array();

		// 內容設定
				// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_ads/event/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_ads/event/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}


	/**
	 * 電子禮金管理 Template
	 *
	 */
	public function credit($type="")
	{
		$page_data = array();

		// 內容設定

		switch($type){
			case "report":
				// general view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'admin/page_order/credit/report'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['enable'] = array(
																array('label' => '尚未使用', 'num' => 30000),
																array('label' => '已使用', 'num' => 70000)
																);
				$page_data['page_level_js']['view_path'] = 'admin/page_order/credit/report_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('admin/general/main', $page_data);

			break;
			default:
				// productCategoryItem view setting
				$page_data['general_header'] = array(); // header 可以透過這個參數帶入
				$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$page_data['page_content']['view_path'] = 'admin/page_order/credit/table'; // 頁面主內容 view 位置, 必要
				$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$page_data['page_level_js']['view_path'] = 'admin/page_order/credit/table_js'; // 頁面主內容的 JS view 位置, 必要
				$this->load->view('admin/general/main', $page_data);

			break;
		}
	}
	/**
	 * 婚禮網站管理
	 *
	 */
	public function weddingWebsite()
	{
		if($data_array=$this->input->get('orderTable',true)){
			//var_dump($data_array);
		}

		$page_data = array();
		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_wedding/table'; // 頁面主內容 view 位置, 必要
		$page_data['page_content']['ccapply'] = $this->libraries_model->_select('ij_ccapply_code_ct',array('display_flag'=>'1'),0,0,0,'ccapply_code','asc','result_array');
		$page_data['page_content']['orders'] = $this->libraries_model->_get_wedding_websites();
		$Store=$this->libraries_model->get_channels();
		$page_data['page_content']['Store']=form_dropdown('orderTable[channel_name]',$Store,@$data_array['channel_name'],'id="channel_name" class="table-group-action-input form-control input-inline input-sm" style="font-size:11px;"');
		$page_data['page_content']['sub_order_status']=$this->libraries_model->_select('ij_sub_order_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array','sub_order_status,sub_order_status_name');
		$page_data['page_content']['payment_status']=$this->libraries_model->_select('ij_payment_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array','payment_status,payment_status_name');
		$page_data['page_content']['admin_users']=$this->libraries_model->_get_admin_users();
		$page_data['page_content']['data_array']=$data_array;
		$this->load->model('Shop_model');
		$page_data['page_content']['templates'] =$this->Shop_model->Get_Product_Template();

		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_wedding/table_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}
	/**
	 * 婚禮網站設定
	 *
	 */
	public function weddingWebsite_item($wedding_website_sn = false)
	{
		$this->LoginCheck();
		if($this->input->post('wedding_website',true) && $this->input->post('wedding_website_sn',true)){
				$data_array=$this->input->post('wedding_website',true);
				//var_dump($this->input->post('wedding_website_sn',true));
				$this->libraries_model->_update('ij_wedding_website',$data_array,'wedding_website_sn',$this->input->post('wedding_website_sn',true));
				//echo $this->db->last_query();
				//exit();

				//關聯廠商
				$_where=" where member_sn='".$this->input->post('member_sn',true)."'";
				$_model_name="Member_table";
		    $this->load->model('member/'.$_model_name);
				$this->$_model_name->action_update($_where,"member_sn",$this->input->post('member_sn',true));

				$this->session->set_userdata(array('Mes'=>'更新成功!'));
				redirect(base_url('admin/admin/weddingWebsite'));
				exit();
		}
			$page_data = array();

			$_question_status = $this->libraries_model->_select('ij_member_question_status_ct','display_flag','1',0,0,0,0,'result_array');
			$_question_close_status = $this->libraries_model->_select('ij_cc_question_close_ct','display_flag','1',0,0,'cc_question_close_code',0,'result_array');
			if($wedding_website_sn){
				$_result = $this->libraries_model->_get_wedding_websites($wedding_website_sn);
				$page_data['_result'] = $_result;
			}
			//var_dump($_result[0]['question_status']);
			// 內容設定
			if(@$_result[0]['question_status']=='5') //已結案
			{
				// 若為已解決， modal footer 不顯示
				$page_data['done'] = true;
			}
			else
			{
				$page_data['done'] = false;
			}
			//關聯經銷商短名改由 ij_serial_card 關聯
			/*$_where=" left join ij_dealer on ij_dealer.dealer_sn=ij_member_dealer_relation.dealer_sn where member_sn='".$_result[0]['member_sn']."' ";
			$this->load->model('sean_models/Sean_db_tools');
			$_member_dealers=$this->Sean_db_tools->db_get_max_record("dealer_short_name","ij_member_dealer_relation",$_where);*/
			$_where=" left join ij_dealer on ij_dealer.dealer_sn=ij_serial_card.stock_out_dealer_sn where used_member_sn='".$_result[0]['member_sn']."' ";
			$this->load->model('sean_models/Sean_db_tools');
			$_member_dealers=$this->Sean_db_tools->db_get_max_record("distinct dealer_short_name","ij_serial_card",$_where);

			$_member_dealer='';
			foreach($_member_dealers as $Item){
				$_member_dealer.=$Item->dealer_short_name.',';
			}
			$page_data['_member_dealer']=substr($_member_dealer, 0, -1);

			//全部有效經銷商短名
			$_where=" where dealer_status='1' ";
			$_dealers=$this->Sean_db_tools->db_get_max_record("dealer_short_name","ij_dealer",$_where);
			$_dealer_list='';
			foreach($_dealers as $Item){
				$_dealer_list.='"'.$Item->dealer_short_name.'",';
			}
			$page_data['_dealer_list']=substr($_dealer_list, 0, -1);
			$page_data['_question_status'] = $_question_status;
			$page_data['_question_close_status'] = $_question_close_status;
			$this->load->view('admin/page_wedding/modal_wedding_item', $page_data);

	}
	/**
	 * 婚禮網站設定
	 *
	 */
	public function weddingWebsite_addnew()
	{
		$this->LoginCheck();
		if($this->input->post('wedding_website',true)){
				$data_array=$this->input->post('wedding_website',true);
				$promo_price=$this->input->post('promo_price',true);
        $order = array(
            'orginal_total_order_amount'  => $promo_price,
            'total_order_amount'  => $promo_price,
            'member_sn'  => $data_array['member_sn']
        );
				$order_item=$this->input->post('order_item',true);  //product_name、product_package_name
				$order_item['product_sn']=$data_array['associated_product_sn'];
				$order_item['mutispec_stock_sn']=$data_array['product_package_config_sn'];
				$order_item['buy_amount']=1;
				$order_item['sales_price']=$promo_price;
				$order_item['actual_sales_amount']=$promo_price;

				$order_sn=$this->libraries_model->_save_order($order,$order_item);
				if(!$order_sn){
					$this->session->set_userdata(array('Mes'=>'新增失敗，錯誤訊息：訂單新增失敗'));
					redirect(base_url('admin/admin/weddingWebsite'));
					exit();
				}
				$data_array['associated_order_sn']=$order_sn;
				//var_dump($data_array);
				//exit();
				$url=API_INVSITE.'api/create_wedding_website';

				$this->load->library("ijw");
				$response=$this->ijw->get_array_from_curl($url,$data_array);
				if($response['success']){
					$this->session->set_userdata(array('Mes'=>'新增成功!'));
				}else{
					$this->session->set_userdata(array('Mes'=>'新增失敗，錯誤訊息：'.$response['error']));
				}
				//var_dump($response);
				//$this->libraries_model->_update('ij_wedding_website',$data_array,'wedding_website_sn',$this->input->post('wedding_website_sn',true));
				//echo $this->db->last_query();


				redirect(base_url('admin/admin/weddingWebsite'));
				exit();
		}
	}
	/**
	 * 婚禮網站序號管理
	 *
	 */
	public function serialNumber()
	{
		//if($data_array=$this->input->get('orderTable',true)){
			//var_dump($data_array);
		//}

		$page_data = array();
		// 內容設定
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_wedding/serial'; // 頁面主內容 view 位置, 必要
		$page_data['page_content']['ccapply'] = $this->libraries_model->_select('ij_ccapply_code_ct',array('display_flag'=>'1'),0,0,0,'ccapply_code','asc','result_array');
		$page_data['page_content']['serials'] = $this->libraries_model->_get_wedding_serials();
		$Store=$this->libraries_model->get_channels();
		$page_data['page_content']['Store']=form_dropdown('orderTable[channel_name]',$Store,@$data_array['channel_name'],'id="channel_name" class="table-group-action-input form-control input-inline input-sm" style="font-size:11px;"');
		$page_data['page_content']['sub_order_status']=$this->libraries_model->_select('ij_sub_order_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array','sub_order_status,sub_order_status_name');
		$page_data['page_content']['payment_status']=$this->libraries_model->_select('ij_payment_status_ct',array('display_flag'=>'1'),0,0,0,0,0,'result_array','payment_status,payment_status_name');
		//$page_data['page_content']['admin_users']=$this->libraries_model->_get_admin_users();
		//$page_data['page_content']['data_array']=$data_array;
		$this->load->model('Shop_model');
		$page_data['page_content']['templates'] =$this->Shop_model->Get_Product_Template();

		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_wedding/serial_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}
	/**
	 * 婚禮網站序號設定
	 *
	 */
	public function weddingWebsite_serial_item($serial_card_sn = false)
	{
		$this->LoginCheck();
		if($this->input->post('wedding_website',true) && $this->input->post('serial_card_sn',true)){
				$data_array=$this->input->post('wedding_website',true);
				if($data_array['stock_out_date']=='0000-00-00 00:00:00'){
					$data_array['stock_out_date']=date('Y-m-d H:i:s');
				}
				if($data_array['serial_card_status']=='1'){
					$data_array['serial_card_status']='2';
				}
			  $data_array = $this->libraries_model->CheckUpdate($data_array,0); //update

				//var_dump($this->input->post('serial_card_sn',true));
				$this->libraries_model->_update('ij_serial_card',$data_array,'serial_card_sn',$this->input->post('serial_card_sn',true));
				//echo $this->db->last_query();
				//exit();


				$this->session->set_userdata(array('Mes'=>'設定成功!'));
				redirect(base_url('admin/admin/serialNumber'));
				exit();
		}
			$page_data = array();

			//$_question_status = $this->libraries_model->_select('ij_member_question_status_ct','display_flag','1',0,0,0,0,'result_array');
			//$_question_close_status = $this->libraries_model->_select('ij_cc_question_close_ct','display_flag','1',0,0,'cc_question_close_code',0,'result_array');
			$this->load->model('Shop_model');
			if($serial_card_sn){
				$_result = $this->libraries_model->_get_wedding_serials($serial_card_sn);
				$page_data['_result'] = $_result;
				//$product=$this->libraries_model->_select('ij_product','product_sn',$_result[0]['associated_product_sn'],0,0,'product_sn',0,'row','default_channel_name,default_root_category_sn');
				$product=$this->Shop_model->_get_product($_result[0]['associated_product_sn']);
			}

			//var_dump($_result[0]['question_status']);
			// 內容設定
			if(@$_result[0]['question_status']=='5') //已結案
			{
				// 若為已解決， modal footer 不顯示
				$page_data['done'] = true;
			}
			else
			{
				$page_data['done'] = false;
			}
			//$page_data['templates'] =$this->Shop_model->Get_Product_Template();
			$page_data['templates'] =	$this->Shop_model->_get_products(@$product['default_channel_name'],0,@$product['default_root_category_sn'],0,0,'name_ASC');

			$Store=$this->libraries_model->get_channels();
			$page_data['Store']=form_dropdown('module[channel_name]',$Store,@$product['default_channel_name'],'id="StoreName" class="form-control" onChange="ChgChannelName(this.value);"');

			//$ij_categorys=$this->libraries_model->categoryParentChildTree(@$product['default_channel_name']);
			//if(!$ij_categorys){
				$ij_categorys=array('0'=>'請先選擇館別');
			//}
			//$page_data['packages'] = $this->Shop_model->_get_package_price_by_psn($data);

		  $page_data['categorys']=form_dropdown('category_id',$ij_categorys,'','id="category_id" class="form-control" size="5"');

			//經銷商短名
			$page_data['_member_dealer']=$this->libraries_model->_select('ij_dealer','dealer_status','1',0,0,'dealer_sn',0,'result_array','dealer_sn,dealer_short_name');

			//$page_data['_question_status'] = $_question_status;
			$page_data['default_root_category_sn'] = @$product['default_root_category_sn'];
			$page_data['default_channel_name'] = @$product['default_channel_name'];
			$page_data['product'] = $product;
			$this->load->view('admin/page_wedding/modal_wedding_serial_item', $page_data);

	}
	/**
	 * ajax刪除
	 *
	 */
	public function ajaxdel(){
		if (!$this->input->is_ajax_request()) {
   			exit('No direct script access allowed');
		}
		$HTTP_REFERER_array=explode('/',str_replace(base_url(),'',$_SERVER["HTTP_REFERER"]));
		$target_class=$HTTP_REFERER_array[1]; //controller
		$target_method=$HTTP_REFERER_array[2]; //method
		//echo $_SERVER['HTTP_REFERER'];
		//var_dump(stripos($this->ijw->check_permission(1,$target_method,$target_class),'del'));
		//exit;
		if(stripos($this->ijw->check_permission(1,$target_method,$target_class),'del')===false){
		    //var_dump($this->ijw->check_permission(1,$target_method,$target_class));
			echo false;
			exit;
		}
		$table=base64_decode($this->input->post('dtable',true));
		$field=base64_decode($this->input->post('dfield',true));
		$id=$this->input->post('did',true);
		$dkind=$this->input->post('dkind',true);
		$dfilename=$this->input->post('dfilename',true);
		if($table && $field && $id){
			if($row=$this->libraries_model->_select($table,$field,$id,0,0,$field,0,'row',0,0,0,0)){ //檢查是否有值
				//var_dump($row);
			switch ($table) {
				case 'ij_edm_send_config':
					if($row['send_time']=='0000-00-00 00:00:00'){ //尚未寄送才可以刪除
		    			$this->libraries_model->_delete('ij_edm_send_list_log','edm_send_id',$id);
		    			echo $this->libraries_model->_delete($table,$field,$id);
	    				//$upt_data=array('status'=>'2');//改變狀態->2 刪除
	    			}else{
						echo false;
						exit();
	    			}
	        break;
					case 'ij_serial_card':
						if($row['serial_card_status']=='2'){ //狀態=2 出庫才能停用
	    				$upt_data=array('serial_card_status'=>'4');//改變狀態->4 停用
	    			}else{
								//var_dump($row);
								echo false;
								exit();
	    			}
	        break;
					case 'ij_category':
					if($this->libraries_model->_select('ij_product_category_relation',$field,$id,0,0,$field,0,'num_rows')==0 && $this->libraries_model->_select('ij_category','upper_category_sn',$id,0,0,'upper_category_sn',0,'num_rows')==0){ //是否已有參照
						//var_dump($row['banner_save_dir']);exit;
		        		if(is_file(ADMINUPLOADPATH.$row['banner_save_dir'])) unlink(ADMINUPLOADPATH.$row['banner_save_dir']);
		        		if(is_file(ADMINUPLOADPATH.$row['main_picture_save_dir'])) unlink(ADMINUPLOADPATH.$row['main_picture_save_dir']);
		        		if(is_file(ADMINUPLOADPATH.$row['main_banner'])) unlink(ADMINUPLOADPATH.$row['main_banner']);
						$this->libraries_model->_delete('ij_product_category_relation',$field,$id); //一併刪除關聯表
						$this->libraries_model->_delete('ij_category_attribute_log',$field,$id); //一併刪除關聯表
						$this->libraries_model->_delete('ij_category_product_package_relation',$field,$id); //一併刪除關聯表
						$this->libraries_model->_delete('ij_category_relation',$field,$id); //一併刪除關聯表
						$this->libraries_model->_delete('ij_category_spec_option_relation',$field,$id); //一併刪除關聯表
	    				echo $this->libraries_model->_delete($table,$field,$id);
	    			}else{
						echo false;
	    			}

	        break;
					case 'ij_purchase_sales_stock_log':
						if($row['purchase_sales_stock_status']=='2'){ //狀態=2
		    				echo $this->libraries_model->_delete($table,$field,$id);
	    			}else{
	    				$upt_data=array('purchase_sales_stock_status'=>'2');//改變狀態->2
	    			}
	        break;
					case 'ij_member_question_status_ct':
						if($row['display_flag']=='2'){ //狀態=2
		    				echo $this->libraries_model->_delete($table,$field,$id);
	    			}else{
	    				$upt_data=array('display_flag'=>'2');//改變狀態->2
	    			}
	        break;
					case 'ij_member_message_center_log':
						if($row['message_status']=='2'){ //狀態=2
		    				echo $this->libraries_model->_delete($table,$field,$id);
	    			}else{
	    				$upt_data=array('message_status'=>'2');//改變狀態->2
	    			}
	        break;
	        case 'ij_attribute_config':
					//if($row['status']=='2'){ //狀態=2 直接刪除
						if($this->libraries_model->_select('ij_attribute_value_config',$field,$id,0,0,$field,0,'num_rows')==0){ //是否已有資料
	    					echo $this->libraries_model->_delete('ij_attribute_value_config',$field,$id);
	    					echo $this->libraries_model->_delete($table,$field,$id);
	    				}else{
							echo false;
						}
					//}else{
	    				$upt_data=array('status'=>'2');//改變狀態->2
					//}
	        break;
	        case 'ij_attribute_value_config':
						if($row['status']=='2'){ //狀態=2 直接刪除
							if($this->libraries_model->_select('ij_product_attribute_log',$field,$id,0,0,$field,0,'num_rows')==0){ //是否已有參照
	    					echo $this->libraries_model->_delete($table,$field,$id);
	    				}else{
								echo false;
							}
						}else{
	    				$upt_data=array('status'=>'2');//改變狀態->2
						}
	        break;
	        case 'ij_spec_option_config':
						if($row['status']=='2'){ //狀態=2 直接刪除
							$this->libraries_model->_delete('ij_category_spec_option_relation',$field,$id); //一併刪除關聯表
	    				echo $this->libraries_model->_delete($table,$field,$id);
						}else{
	    				$upt_data=array('status'=>'2');//改變狀態->2
						}
	        break;
	        case 'ij_product_package_config':
						if($row['status']=='2'){ //狀態=2 直接刪除
							$this->libraries_model->_delete('ij_product_package_spec_option_relation',$field,$id); //一併刪除關聯表
	    				echo $this->libraries_model->_delete($table,$field,$id);
						}else{
	    				$upt_data=array('status'=>'2');//改變狀態->2
						}
	        break;
	        case 'ij_member':
						if($row['member_status']=='3'){ //狀態=3 直接刪除
							$this->libraries_model->_delete('ij_partner','member_sn',$id); //一併刪除關聯表
							$this->libraries_model->_delete('ij_bank_info','associated_member_sn',$id); //一併刪除關聯表
	    					$this->libraries_model->_delete($table,$field,$id);
						}else{
	    					$upt_data=array('member_status'=>'3');//改變狀態->2
						}
	        break;
	        case 'ij_product_gallery':
						if($row['status']=='2'){ //狀態=2 直接刪除
	        		$image_path = $this->db->where($field,$id)->get($table)->row()->image_path;
		          if(is_file(ADMINUPLOADPATH.$image_path)) unlink(ADMINUPLOADPATH.$image_path);
	    				echo $this->libraries_model->_delete($table,$field,$id);
						}else{
	    				$upt_data=array('status'=>'2');//改變狀態->2
						}
	        break;
	        case 'ij_template_picture':
						if($row['status']=='2'){ //狀態=2 直接刪除
	        		$image_path = $this->db->where($field,$id)->get($table)->row()->image_path;
		          if(is_file(ADMINUPLOADPATH.$image_path)) unlink(ADMINUPLOADPATH.$image_path);
	    				echo $this->libraries_model->_delete($table,$field,$id);
						}else{
	    				$upt_data=array('status'=>'2');//改變狀態->2
						}
	        break;
	        case 'ij_template_divider_config':
						if($row['status']=='2'){ //狀態=2 直接刪除
	        		$image_path = $this->db->where($field,$id)->get($table)->row()->default_divider_save_dir;
		          if(is_file(ADMINUPLOADPATH.$image_path)) unlink(ADMINUPLOADPATH.$image_path);
	    				echo $this->libraries_model->_delete($table,$field,$id);
						}else{
	    				$upt_data=array('status'=>'2');//改變狀態->2
						}
	        break;
	        case 'ij_blog_article':
					if($dfilename){ //刪除圖檔
			        	if(is_file(ADMINUPLOADPATH.$dfilename)) unlink(ADMINUPLOADPATH.$dfilename);
			            //echo str_replace($dfilename.':','',$row[$dkind]);
			            //exit();
		    			$upt_data=array('blog_article_image_save_dir'=>'');//拿掉刪除的圖檔檔名
		    		}else{
						$this->libraries_model->_delete('ij_blog_relation',$field,$id); //一併刪除關聯表
		        		//$image_path = $this->db->where($field,$id)->get($table)->row()->blog_article_image_save_dir;
						$this->libraries_model->_delete('ij_blog_type_relation',$field,$id); //一併刪除關聯表
			            if(is_file(ADMINUPLOADPATH.$row['blog_article_image_save_dir'])) unlink(ADMINUPLOADPATH.$row['blog_article_image_save_dir']);
		    				$this->libraries_model->_delete($table,$field,$id);
	    			}
	        break;
	        case 'ij_faq':
	        	$this->libraries_model->_delete($table,$field,$id);
	        break;
	        case 'ij_content_type_config':
		        if(!$this->db->like('post_channel_name_set',$row['content_title'])->get('ij_faq')->num_rows() && !$this->db->like('associated_content_type_sn',$row['content_title'])->get('ij_blog_article')->num_rows()){ //這些有值不允許刪除
		        	$this->libraries_model->_delete($table,$field,$id);
		        }else{
		        	echo false;
		        	exit;
		        }
	        break;
	        case 'ij_banner_location':
				if($row['status']=='2'){ //狀態=2 直接刪除
	        		$image_path = $this->db->where($field,$id)->get($table)->row()->default_banner_content_save_dir;
		            if(is_file(ADMINUPLOADPATH.$image_path)) unlink(ADMINUPLOADPATH.$image_path);
	    			echo $this->libraries_model->_delete($table,$field,$id);
				}else{
					if($dfilename){ //刪除圖檔
			        	if(is_file(ADMINUPLOADPATH.$dfilename)) unlink(ADMINUPLOADPATH.$dfilename);
			          //echo str_replace($dfilename.':','',$row[$dkind]);
			          //exit();
		    			$upt_data=array('default_banner_content_save_dir'=>'');//拿掉刪除的圖檔檔名
		    		}else{
	    				$upt_data=array('status'=>'2');//改變狀態->2
	    			}
				}
	        break;
	        case 'ij_banner_content':
					if($dfilename){ //刪除圖檔
			            if(is_file(ADMINUPLOADPATH.$dfilename)) unlink(ADMINUPLOADPATH.$dfilename);
			            //echo str_replace($dfilename.':','',$row[$dkind]);
			            //exit();
		    			$upt_data=array('banner_content_save_dir'=>'');//拿掉刪除的圖檔檔名
		    		}else{
		        		$image_path = $row['banner_content_save_dir'];
			            if(is_file(ADMINUPLOADPATH.$image_path)) unlink(ADMINUPLOADPATH.$image_path);
		    			$this->libraries_model->_delete('ij_banner_schedule_log',$field,$id);
						$this->session->set_userdata('general_header','');
		    			$this->libraries_model->_delete($table,$field,$id);
		    		}
	        break;
	        case 'ij_dealer':
	        	if($dkind && $dfilename){ //刪除經銷商圖檔
			          if(is_file(ADMINUPLOADPATH.$dfilename)) unlink(ADMINUPLOADPATH.$dfilename);
			          //echo str_replace($dfilename.':','',$row[$dkind]);
			          //exit();
		    				$upt_data=array($dkind=>str_replace($dfilename.':','',$row[$dkind]));//拿掉刪除的圖檔檔名
	        	}else{
							if($row['status']=='2'){ //狀態=2 直接刪除
		        		$image_path = $this->db->where($field,$id)->get($table)->row()->image_path;
			          if(is_file(ADMINUPLOADPATH.$image_path)) unlink(ADMINUPLOADPATH.$image_path);
		    				echo $this->libraries_model->_delete($table,$field,$id);
							}else{
		    				$upt_data=array('status'=>'2');//改變狀態->2
							}
						}
	        break;
	        case 'ij_product':
				if($row['product_status']=='9'){ //狀態=9 直接刪除
		        	//if(!$this->db->where($field,$id)->get('ij_dealer_order_iteim')->num_rows() && !$this->db->where($field,$id)->get('ij_order_item')->num_rows() && !$this->db->where($field,$id)->get('ij_purchase_sales_stock_log')->num_rows() && !$this->db->where($field,$id)->get('ij_track_list_log')->num_rows()){ //這些有值不允許刪除
		        		$images = $this->db->where($field,$id)->get('ij_product_gallery')->result_array(); //刪除圖檔
		        		foreach($images as $row){
				          if(is_file(ADMINUPLOADPATH.$row['image_path'])) unlink(ADMINUPLOADPATH.$row['image_path']);
				        }
				        //一併刪除關聯表
				        $this->libraries_model->_delete('ij_product_promotion_label_relation',$field,$id);
				        $this->libraries_model->_delete('ij_product_attribute_log',$field,$id);
				        $this->libraries_model->_delete('ij_product_category_relation',$field,$id);
				        $this->libraries_model->_delete('ij_product_fix_price_log',$field,$id);
				        $this->libraries_model->_delete('ij_product_gallery',$field,$id);
				        $this->libraries_model->_delete('ij_product_price_tier_log',$field,$id);
				        $this->libraries_model->_delete('ij_temp_order_item',$field,$id);
				        $this->libraries_model->_delete('ij_dealer_order_iteim',$field,$id);
				        $this->libraries_model->_delete('ij_order_item',$field,$id);
				        $this->libraries_model->_delete('ij_purchase_sales_stock_log',$field,$id);
				        $this->libraries_model->_delete('ij_track_list_log',$field,$id);
						if($_result=$this->libraries_model->_select('ij_addon_config','apply_product_sn',$id,0,0,'apply_product_sn',0,'result',0,0,0,0)){
							foreach($_result as $_item){
				        		$this->libraries_model->_delete('ij_addon_log','addon_config_sn',$_item['addon_config_sn']);
							}
						}
				        $this->libraries_model->_delete('ij_addon_config','apply_product_sn',$id);
				        $this->libraries_model->_delete('ij_addon_log','associated_product_sn',$id);
		    			echo $this->libraries_model->_delete($table,$field,$id);
		    			//}else{
		    				//var_dump($row);
						//	echo false;
		    			//}
				}else{
					$upt_data=array('product_status'=>'9');//改變狀態->2
				}
	        break;
	        case 'ij_coupon_code_config':
						if($row['status']=='2'){ //狀態=2 直接刪除
		        	if(!$this->db->where($field,$id)->where('coupon_code_status','2')->get('ij_coupon_code')->num_rows()){ //有值不允許刪除
				        //一併刪除代碼資料
				        $this->libraries_model->_delete('ij_coupon_code',$field,$id);
		    				echo $this->libraries_model->_delete($table,$field,$id);
		    			}else{
								echo false;
		    			}
						}else{
	    				$upt_data=array('status'=>'2'); //改變狀態->2
	    				$this->libraries_model->_update('ij_coupon_code',array('coupon_code_status'=>'4'),$field,$id);  //改變狀態->4 刪除
						}
	        break;
	        case 'ij_sub_order':
				//if($row['status']=='2'){ //狀態=2 直接刪除
				    $this->libraries_model->_delete('ij_order_item',$field,$id);
				    $this->libraries_model->_delete('ij_gift_cash','associated_order_sn',$row['order_sn']);
		    		echo $this->libraries_model->_delete($table,$field,$id);
				    $this->libraries_model->_delete('ij_order','order_sn',$row['order_sn']);
				//}else{
    			//	$upt_data=array('status'=>'2'); //改變狀態->2
    			//	$this->libraries_model->_update('ij_coupon_code',array('coupon_code_status'=>'4'),$field,$id);  //改變狀態->4 刪除
				//}
	        break;
			default:
				if($row['status']=='2'){ //狀態=2 直接刪除
					echo $this->libraries_model->_delete($table,$field,$id);
				}else{
					$upt_data=array('status'=>'2');//改變狀態->2
				}
				//var_dump($row);exit;
	        break;
	      }

	        //紀錄log
					$_trans_log["_table"]=$table;
					$_trans_log["_before"]=json_encode($row);
					$_trans_log["_after"]=json_encode(@$upt_data);
					$_trans_log["_type"]="2";
					$_trans_log["_key_id"]=$id;
					$this->libraries_model->trans_log($_trans_log);
					//var_dump($this->libraries_model->_update($table,$upt_data,$field,$id));
					//echo $this->db->last_query();
				echo (@$upt_data)? $this->libraries_model->_update($table,$upt_data,$field,$id):'true'; //改變狀態2=刪除
			}else{
				echo false;
			}
		}else{
				echo false;
		}
	}
	public function get_unread_message_count(){
		//echo $this->libraries_model->get_unread_message_count();
		$message_subject=strip_tags($this->libraries_model->_select('ij_member_message_center_log',array('default_display_time <='=>date('Y-m-d H:i:s'),'message_status'=>'1','ij_member_message_center_log.member_sn'=>@$this->session->userdata['member_data']['member_sn'],'read_flag'=>null),0,0,0,'default_display_time','desc','row','message_subject')['message_subject']);
		$result=array($this->libraries_model->get_unread_message_count(),$message_subject);
		echo json_encode($result);
	}
}
