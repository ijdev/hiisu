<?php defined('BASEPATH') OR exit('No direct script access allowed');
//var_dump($page_content);
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8">
<title>HiiSU|囍市集 -系統管理</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta content="" name="author">
<base href="<?php echo $this->config->item('base_url'); ?>" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="public/metronic/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="public/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="public/metronic/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="public/metronic/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="public/metronic/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css">
<link href="public/metronic/global/plugins/morris/morris.css" rel="stylesheet" type="text/css">
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="public/metronic/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
<link href="public/metronic/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css">
<link href="public/metronic/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="public/metronic/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="public/metronic/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="public/metronic/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="icon" type="image/png" href="public/img/favicon.png" />
<style>
body{
	font-family: '微軟正黑體';
}
h1, h2, h3, h4, h5, h6{
	font-family: '微軟正黑體';
}
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
<!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  -->
<body>
<!-- BEGIN HEADER -->
<?php switch($this->session->userdata('member_role_system_rules')[0]['role_name']){
	case '經銷商管理員':
		$_header='header_dealer';
	break;
	case '供應商管理員':
		$_header='header_supplier';
	break;
	case 9:
	default:
		$_header='header';
	break;
}?>
<?php $this->load->view(($this->uri->segment(1)? 'admin/':'').'general/'.$_header, isset($general_header)? $general_header:null);?>
<!-- Header END -->


<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
  <?php $this->load->view($page_content['view_path'], $page_content);?>
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
  <div class="container">
     2018-<?=date('Y')?> &copy; HiiSU. All Rights Reserved.
  </div>
</div>
<div class="scroll-to-top">
  <i class="icon-arrow-up"></i>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="public/metronic/global/plugins/respond.min.js"></script>
<script src="public/metronic/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="public/metronic/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="public/metronic/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<?php $this->load->view($page_level_js['view_path'], $page_level_js);?>
<script>
jQuery(document).ready(function() {
    $.get("admin/admin/get_unread_message_count", function(data, status){
        //console.log("Data: " + data + "\nStatus: " + status);
    });
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>