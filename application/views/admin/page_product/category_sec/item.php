<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品次分類 <small>次分類設定</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">分類設定</span>
            </div>
            <div class="tools">
              <a href="preview/productCategory"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">名稱 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-3 control-label">母分類 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <select class="form-control">
                      <option>分類一</option>
                      <option>分類二</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-3 control-label">分類屬性</label>
                  <div class="col-lg-8">
                    <div class="checkbox-list">
                      <label class="checkbox-inline"><input type="checkbox" name="product[attr][]" value="1">分類屬性一</label>
                      <label class="checkbox-inline"><input type="checkbox" name="product[attr][]" value="1">分類屬性二</label>
                      <label class="checkbox-inline"><input type="checkbox" name="product[attr][]" value="1">分類屬性三</label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="seq" class="col-lg-3 control-label">經銷折扣</label>
                  <div class="col-lg-8">
                    <div class="form-group">
                      <div class="col-lg-1">
                        <input type="text" class="form-control form-inline" value="5">
                      </div>
                      <div class="col-lg-1">
                        <span class="form-control-static form-inline">%</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-3 control-label">說明</label>
                  <div class="col-lg-8">
                    <textarea class="form-control" rows="3"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="seq" class="col-lg-3 control-label">排序 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" value="0">
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-3 control-label">是否啟用 <span class="require">*</span></label>
                  <div class="col-lg-8">
                    <select class="form-control">
                      <option>是</option>
                      <option>否</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-lg-3 control-label">更新者</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="administrator" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-3 control-label">更新時間</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="2015-05-25 10:00" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="button" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->