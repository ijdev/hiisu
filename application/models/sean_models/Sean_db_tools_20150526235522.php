<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
     *
     * Database_tools_model: contains generic functions, used in several controllers
     *
     */
 
class Sean_db_tools extends CI_Model 
{

    public function __construct() {
        parent::__construct();
      /* load 資料庫 */
      $this->load->database();
    }

    
		public function db_get_one_record($_field,$_table,$_where)
		{	
				
				$_one="";
				$action_one="SELECT $_field from $_table $_where limit 1";
				$query = $this->db->query($action_one);

				if ($query->num_fields() > 0)
				{
				   $_one = $query->row($_field);
				} 
				$query->free_result();
				return $_one;
		} 
		
		public function db_get_max_record($_field,$_table,$_where)
		{	
				
				$_one="";
				$action_one="SELECT $_field from $_table $_where";
				
				$query = $this->db->query($action_one);

				if ($query->num_fields() > 0)
				{
				   	$_one=$query->result();
				} 
				$query->free_result();
				return $_one;
		} 
		
		public function db_get_max_num($_field,$_table,$_where)
		{	
				
				$_max=0;
				$action_one="SELECT $_field from $_table $_where";
				
				$query = $this->db->query($action_one);
				$_max=$query->num_rows();
				$query->free_result();
				return $_max;
		} 
		
		
	
	
}

/* End of file database_tools_model.php */
/* Location: ./application/models/database_tools_model.php */
