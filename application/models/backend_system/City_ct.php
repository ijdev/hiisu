<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class City_ct extends My_general_ct {

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('sean_models/Sean_db_tools');
		
		
		/* Begin 修改  */
		$this->_ct_name="城市代碼表";// 功能名稱
		$this->_url="Sysconfig/City_ct/";//列表功能url
		$this->_url_item="Sysconfig/City_ct_item/";//處理動作 url
		$this->_table="ij_city_ct";//table name
		$this->_id="city_code";//table key 
		$this->_table_field=array(
					 "city_code" => "城市代號",
					 "associated_country_code" => "所屬國家",
					 "associated_state_code" => "所屬省(州)",
					 "city_name" => "城市中文名稱",
					 "city_eng_name" => "城市英文名稱",					 
					 "eff_start_date" => "生效起始日",
					 "eff_end_date" => "生效終止日",
					 "sort_order" => "排序順次",
					 "default_flag" => "是否為預設值",
					 "display_flag" => "是否顯示",
				);
				
		/* End 修改  */		

    }
    
    //取得列表內容
    public function table_value_list( $_where,$_id = 0) 
	{
		$_table_key=$this->_id;    
	    $page_data['_body_result']=array();
		//判斷是否有帶入特殊查詢
		if(strlen($_where)==0)
		{
			$_where="  order by sort_order asc";
			 
		}
		// 取得列表 	 	 	 	 	 	
		$_result01=$this->Sean_db_tools->db_get_max_record("*",$this->_table,$_where);
		
		/* 列表標頭名稱 */
		$page_data['_table_thead_name']=$this->_table_field;
		
		
		/* 類表內容 */
		if(is_array($_result01))
		{                                  
                 		$_i=0;
                 		foreach($_result01 as $key => $value)
                    	{                    			                    			                    			
			                    		
			                    		if($value->default_flag == 1)
			                    		{
			                    					$_default_flag = '<i class="fa fa-check"></i>';
			                    		}else{
			                    					$_default_flag = '<i class="fa fa-times"></i>';
			                    		}
										if($value->display_flag == 1)
			                    		{
			                    					$_display_flag = '<i class="fa fa-check"></i>';
			                    		}else{
			                    					$_display_flag = '<i class="fa fa-times"></i>';
			                    		}
			                    				
			                    		$_body_result='';
						/* Begin 修改  */
			                    		
							$page_data['_body_result'][$_i][0]=array("_value"=>$value->city_code,"_class"=>"");
			              $page_data['_body_result'][$_i][1]=array("_value"=>'台灣',"_class"=>"");											
			              $page_data['_body_result'][$_i][2]=array("_value"=>'台灣',"_class"=>"");											
			              $page_data['_body_result'][$_i][3]=array("_value"=>$value->city_name,"_class"=>"");
										$page_data['_body_result'][$_i][4]=array("_value"=>$value->city_eng_name,"_class"=>"");																											
										$page_data['_body_result'][$_i][5]=array("_value"=>$value->eff_start_date,"_class"=>"");											
										$page_data['_body_result'][$_i][6]=array("_value"=>$value->eff_end_date,"_class"=>"");											
										$page_data['_body_result'][$_i][7]=array("_value"=>$value->sort_order,"_class"=>"");
										$page_data['_body_result'][$_i][8]=array("_value"=>$_default_flag,"_class"=>"");	
									  $page_data['_body_result'][$_i][9]=array("_value"=>$_display_flag,"_class"=>"");
			                    		
						/* End 修改  */
										
										
										$page_data['_body_result'][$_i][10]=array("_value"=>'
			                    		 		<a href="'.$this->init_control.$this->_url_item.'edit/'.$value->$_table_key.'"><i class="fa fa-edit"></i>編輯</a>&nbsp;&nbsp;',"_class"=>"");
                				$_i++;
								
						
                			}
                	
    				}
			return $page_data;
			
    }

	
}
