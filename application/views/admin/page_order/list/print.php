<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- BEGIN PAGE CONTENT -->
<div style="width:1200px !important;">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-xs-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="icon-basket font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">
              訂單 #<?php echo $order[0]['sub_order_num']?> </span>
              <span class="caption-helper"><?php echo $order[0]['order_create_date']?></span>
            </div>
          </div>

                <div class="row">
                  <div class="col-xs-6 col-sm-6">
                    <div class="portlet blue-hoki box">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs"></i>訂購人資料
                        </div>
                      </div>
                      <div class="portlet-body">
                        <div class="row static-info">
                          <div class="col-xs-2 name">
                             姓名:
                          </div>
                          <div class="col-xs-10 value">
                             <a href="/admin/Member/memberTable_item/edit/<?php echo $order[0]['member_sn']?>" target="_blank"><?php echo $order[0]['buyer_first_name']?><?php echo $order[0]['buyer_last_name']?>&nbsp;<i class="fa fa-external-link"></i></a>
                          </div>
                        </div>
                        <div class="row static-info">
                          <div class="col-xs-2 name">
                             Email:
                          </div>
                          <div class="col-xs-10 value">
                             <?php echo $order[0]['buyer_email']?>
                          </div>
                        </div>
                        <div class="row static-info">
                          <div class="col-xs-2 name">
                             住址 :
                          </div>
                          <div class="col-xs-10 value">
                             <?php echo $order[0]['buyer_addr_city']?><?php echo $order[0]['buyer_addr_town']?> <?php echo $order[0]['buyer_zipcode']?> <?php echo $order[0]['buyer_addr1']?>
                          </div>
                        </div>
                        <div class="row static-info">
                          <div class="col-xs-2 name">
                             市話:
                          </div>
                          <div class="col-xs-10 value">
                             <?php echo $order[0]['buyer_tel_area_code']?>-<?php echo $order[0]['buyer_tel']?>
                          </div>
                        </div>
                        <div class="row static-info">
                          <div class="col-xs-2 name">
                             手機:
                          </div>
                          <div class="col-xs-10 value">
                              <?php echo $order[0]['buyer_cell']?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6">
                    <div class="portlet green-meadow box">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs"></i>收件人資料
                        </div>

                      </div>
                      <div class="portlet-body">
                        <div class="row static-info">
                          <div class="col-xs-2 name">姓名:</div>
                          <div class="col-xs-10 value"><?php echo $order[0]['receiver_last_name']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-xs-2 name">住址:</div>
                          <div class="col-xs-10 value"><?php echo $order[0]['receiver_addr_city']?><?php echo $order[0]['receiver_addr_town']?> <?php echo $order[0]['receiver_zipcode']?> <?php echo $order[0]['receiver_addr1']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-xs-2 name">市話:</div>
                          <div class="col-xs-10 value"><?php echo $order[0]['receiver_tel_area_code']?>-<?php echo $order[0]['receiver_tel']?><?php echo (@$order[0]["receiver_tel_ext"])?' #'.$order[0]["receiver_tel_ext"]:'';?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-xs-2 name">手機:</div>
                          <div class="col-xs-10 value"><?php echo $order[0]['receiver_cell']?></div>
                        </div>
                  			<div class="row static-info">
                          <div class="col-xs-2 name">&nbsp;</div>
                          <div class="col-xs-10 value">&nbsp;</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- portlet 購物清單 - 實體商品 -->
                <div class="row">
                  <div class="col-xs-12 col-sm-12">
                    <div class="portlet red-sunglo box">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs"></i>訂單相關資料
                        </div>

                      </div>
                <div class="portlet-body">
	                <div class="row">
                  	<div class="col-xs-6 col-sm-12">
                        <div class="row static-info">
                          <div class="col-xs-4 name">店家:</div>
                          <div class="col-xs-8 value"><?php echo $order[0]['supplier_name']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-xs-4 name">訂單編號/交易序號:</div>
                          <div class="col-xs-8 value"><?php echo $order[0]['sub_order_num']?> / <?php echo $order[0]['order_num']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-xs-4 name">付款方式:</div>
                          <div class="col-xs-8 value"><?php echo $order[0]['payment_method_name']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-xs-4 name">付款狀態:</div>
                          <div class="col-xs-8 value"><span class="label label-success"><?php echo $order[0]['payment_status_name']?></span></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-xs-4 name">配送方式/總重量:</div>
                          <div class="col-xs-8 value"><?php echo $order[0]['delivery_method_name']?> / <?php echo ($order[0]['total_weight']!='0.0000')?round($order[0]['total_weight'],1).'kg':'0'?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-xs-4 name">配送狀態:</div>
                          <div class="col-xs-8 value"><span class="label label-warning"><?php echo $order[0]['delivery_status_name']?></span></div>
                        </div>
	            			</div>
                  	<div class="col-xs-6 col-sm-12">
                        <div class="row static-info">
                          <div class="col-xs-4 name">訂單日期:</div>
                          <div class="col-xs-8 value"><?php echo $order[0]['order_create_date']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-xs-4 name">訂單狀態:</div>
                          <div class="col-xs-8 value"><span class="label label-info"><?php echo $order[0]['sub_order_status_name']?></span></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-xs-4 name">發票格式:</div>
                          <div class="col-xs-8 value"><?php echo ($order[0]['company_tax_id'] && $order[0]['invoice_title'])?'三':'二'?>聯</div>
                        </div>
                        <div class="row static-info">
                          <div class="col-xs-4 name">發票抬頭:</div>
                          <div class="col-xs-8 value"><?php echo $order[0]['invoice_title']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-xs-4 name">統一編號:</div>
                          <div class="col-xs-8 value"><?php echo $order[0]['company_tax_id']?></div>
                        </div>
                        <div class="row static-info">
                          <div class="col-xs-4 name">訂單客服:</div>
                          <div class="col-xs-8 value"><?php echo $order[0]['last_name']?></div>
                        </div>
	            			</div>
		                  <div class="col-xs-12 col-sm-12">
		                        <div class="row static-info">
		                          <div class="col-xs-2 name">備註:</div>
		                          <div class="col-xs-10 value"><?php echo $order[0]['memo']?></div>
		                        </div>
		                  </div>
	            		</div>
            		</div>
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs"></i>訂購商品明細 &nbsp; ( 共 <?php echo $order[0]['sub_count']?> 項 )
                        </div>
                        <div class="actions">
                          <!--
                          <a href="javascript:;" class="btn btn-default btn-sm" data-toggle="modal" data-target="#pproductModal">
                            <i class="fa fa-plus"></i> 新增
                          </a>
                          -->
                        </div>
                      </div>
                      <div class="portlet-body">
                        <div class="table-responsive">
                          <table class="table table-hover table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>商品編號</th>
                                <th>商品名稱</th>
                                <th>庫號</th>
                                <th>規格</th>
                                <th>數量</th>
                                <th>售價</th>
                                <th>加購</th>
                                <th>折扣</th>
                                <?php echo ($order[0]['order_item'][0]['upgrade_discount_amount'])?'<th>升級折價</th>':'';?>
                                <th>囍市錢包</th>
<?php if($this->session->userdata['member_data']['member_level_type']=='9'){?>
                                <th>分潤趴數/金額</th>
<?php }?>
                                <th>小計</th>
                                <!--th>功能</th-->
                              </tr>
                            </thead>
                            <tbody>
                              <?php if($order[0]['order_item']){ foreach($order[0]['order_item'] as $order_item){?>
                                <tr>
                                  <td><a title="商品後台頁面" href="/admin/product/productItem/<?php echo $order[0]['channel_name']?>/<?php echo $order_item['product_sn'];?>" target="_blank"><?php echo $order_item['product_sn'];?></a></td>
                                  <td><a title="商品前台頁面" href="/shop/shopItem/<?php echo $order_item['product_sn'];?>" target="_blank"><?php echo $order_item['product_name'];?></a></td>
                                  <td><?php echo $order_item['unispec_warehouse_num'];?></td>
                                  <td><?php echo $order_item['product_package_name'];?><?php echo ($order_item['upgrade_discount_amount'])? '(升級)':'';?></td>
                                  <td align="center"><?php echo $order_item['buy_amount'];?></td>
                                  <td align="right">$<?php echo $order_item['sales_price'];?> &nbsp; </td>
                                  <td><?php echo $order_item['addon_flag']==1? '<i class="fa fa-check-square-o"></i>':'';?></td>
                                  <td align="right"><?php echo $order_item['coupon_code_cdiscount_amount'];?> &nbsp; </td>
                                <?php echo ($order_item['upgrade_discount_amount'])?'<td align="right">'.$order_item['upgrade_discount_amount'].'</td>':'';?>
                                  <td align="right"><?php echo $order_item['gift_cash_discount_amount'];?> &nbsp; </td>
<?php if($this->session->userdata['member_data']['member_level_type']=='9'){?>
                                  <td align="center"><?php echo $order_item['sharing_percentage']*100;?>% / <?php echo $order_item['sharing_amount'];?></td>
<?php }?>
                                  <td align="right"><?php echo $order_item['actual_sales_amount'];?> &nbsp; </td>
                                  <!--td>
                                    <a href="javascript:;" data-toggle="modal" data-target="#pproductModal">
                                      <i class="fa fa-pencil-square"></i>編輯
                                    </a>&nbsp;&nbsp;
                                    <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>刪除</a>
                                  </td-->
                                </tr>
                              <?php @$cdiscount_amount+=$order_item['coupon_code_cdiscount_amount'];@$gdiscount_amount+=$order_item['gift_cash_discount_amount'];@$actual_sales_amount+=$order_item['actual_sales_amount'];@$upgrade_discount_amount+=$order_item['upgrade_discount_amount'];}}?>
                              <tr>
                                <td colspan="5" align="right">折價券折抵</td>
                                <td colspan="2" align="right"><?php echo $cdiscount_amount;?> &nbsp; </td>
                                <td colspan="2" align="right">總金額</td>
                                <td colspan="2" align="right"><?php echo $actual_sales_amount+$upgrade_discount_amount;?> &nbsp; </td>
                              </tr>
                              <tr>
                                <td colspan="5" align="right">囍市錢包折抵</td>
                                <td colspan="2" align="right"><?php echo $gdiscount_amount;?> &nbsp; </td>
                                <td colspan="2" align="right">折扣</td>
                                <td colspan="2" align="right"><?php echo $upgrade_discount_amount;?> &nbsp; </td>
                              </tr>
                              <tr>
                                <td colspan="9" align="right">運費</td>
                                <td colspan="2" align="right"><?php echo $order[0]['shipping_charge_amount'];?> &nbsp; </td>
                              </tr>
                              <tr>
                                <td colspan="9" align="right">訂單總金額</td>
                                <td colspan="2" align="right"><?php echo $order[0]['shipping_charge_amount']+$actual_sales_amount;?> &nbsp; </td>
                              </tr>
                              <tr>
                                <td colspan="9" align="right">實付總額</td>
                                <td colspan="2" align="right"><?php echo $order[0]['shipping_charge_amount']+$actual_sales_amount;?> &nbsp; </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <!-- End of portlet-body -->
                    </div>
                    <!-- End of portlet -->
                  </div>
                </div>
