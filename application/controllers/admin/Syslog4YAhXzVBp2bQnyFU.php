<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* for admin check system log file at CI system
*/
class Syslog4YAhXzVBp2bQnyFU extends MY_Controller {
 	public function __construct()
	{
		 parent::__construct();
		 //
	}
	public function  index()
	{
		$page_data = array();
		if(!$log_date=$this->input->get('date'))
		{
			$log_date = date('Y-m-d');
		}
		$page_data['log_date']=$log_date;

		$log_file = APPPATH.'logs/log-'.$log_date.'.php';
		if(file_exists($log_file) && $log = file_get_contents($log_file))
		{
			$page_data['log_content'] = $log;
		}
		else
		{
			$page_data['log_content'] = '';
		}

		$this->load->view('admin/page_system/ci/log.php', $page_data);
	}
}