<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>


<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script src="public/metronic/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/pages/scripts/components-pickers.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script>
jQuery(document).ready(function() {       
	// initiate layout and plugins
	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	ComponentsPickers.init();
	
	var chart_status = AmCharts.makeChart( "chart_status", {
        "type": "pie",
        "theme": "light",
		"legend": {
			"markerType": "circle",
			"position": "bottom",
			"autoMargins": false
		},
        "dataProvider": <?php echo json_encode($status);?>,
        "valueField": "num",
        "titleField": "label",
        "outlineAlpha": 0.4,
        "depth3D": 15,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "angle": 30,
        "export": {
        "enabled": true,
        "libs": {
        "path": "http://www.amcharts.com/lib/3/plugins/export/libs/"
        }
        }
    });
	
	var chart_enable = AmCharts.makeChart( "chart_enable", {
        "type": "pie",
        "theme": "light",
		"legend": {
			"markerType": "circle",
			"position": "bottom",
			"autoMargins": false
		},
        "dataProvider": <?php echo json_encode($enable);?>,
        "valueField": "num",
        "titleField": "label",
        "outlineAlpha": 0.4,
        "depth3D": 15,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "angle": 30,
        "export": {
        "enabled": true,
        "libs": {
        "path": "http://www.amcharts.com/lib/3/plugins/export/libs/"
        }
        }
    });
});
</script>

