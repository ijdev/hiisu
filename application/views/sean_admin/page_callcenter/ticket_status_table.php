<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>客服管理 <small>狀態管理</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">狀態列表</span>
            </div>
            <div class="tools">
              <a href="javascript:;" class="collapse">
              </a>
              <a href="javascript:;" class="reload">
              </a>
            </div>
          </div>
          <div class="portlet-body">
            <div class="table-toolbar">
              <div class="row">
                <div class="col-md-6">
                  <div class="btn-group">
                    <button id="sample_editable_1_new" class="btn green" data-toggle="modal" data-target="#memberModal">
                      新增 &nbsp;<i class="fa fa-plus"></i>
                    </button>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="btn-group pull-right">
                  </div>
                </div>
              </div>
            </div>
            <table class="table table-striped table-bordered table-hover" id="table_status">
              <thead>
                <tr>
                  <th>狀態</th>
                  <th>編輯</th>
                  <th>刪除</th>
                </tr>
              </thead>
              <tbody>
                  <tr class="odd gradeX">
                    <td>
                      <span class="label label-sm label-warning">未處理</span>
                    </td>
                    <td class="center">
                      <a href="javascript:void(0);" data-toggle="modal" data-target="#memberModal"><i class="fa fa-edit"></i></a>
                    </td>
                    <td class="center">
                      <a href="javascript:void(0);" class="delete"><i class="fa fa-trash-o"></i></a>
                    </td>
                  </tr>
                  <tr class="odd gradeX">
                    <td>
                      <span class="label label-sm label-info">處理中</span>
                    </td>
                    <td class="center">
                      <a href="javascript:void(0);" data-toggle="modal" data-target="#memberModal"><i class="fa fa-edit"></i></a>
                    </td>
                    <td class="center">
                      <a href="javascript:void(0);" class="delete"><i class="fa fa-trash-o"></i></a>
                    </td>
                  </tr>
                  <tr class="odd gradeX">
                    <td>
                      <span class="label label-sm label-success">已處理</span>
                    </td>
                    <td class="center">
                      <a href="javascript:void(0);" data-toggle="modal" data-target="#memberModal"><i class="fa fa-edit"></i></a>
                    </td>
                    <td class="center">
                      <a href="javascript:void(0);" class="delete"><i class="fa fa-trash-o"></i></a>
                    </td>
                  </tr>
              </tbody>
            </table>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
<!-- Modal -->
<div class="modal fade" id="memberModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">編輯狀態名稱</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-without-legend" role="form">
          <fieldset>
            <div class="form-group">
              <label for="lastname" class="col-lg-4 control-label">分類標題</label>
              <div class="col-lg-8">
                <input type="text" class="form-control" id="category_name">
              </div>
            </div>
            <div class="form-group">
              <label for="lastname" class="col-lg-4 control-label">分類顏色</label>
              <div class="col-lg-8">
                <div class="radio-list">
                  <label class="radio-inline">
                    <input type="radio" name="status" value="warning"><span class="label label-sm label-warning">&nbsp;&nbsp;</span>
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="status" value="info"><span class="label label-sm label-info">&nbsp;&nbsp;</span>
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="status" value="success"><span class="label label-sm label-success">&nbsp;&nbsp;</span>
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="status" value="danger"><span class="label label-sm label-danger">&nbsp;&nbsp;</span>
                  </label>
                </div>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">儲存</button>
      </div>
    </div>
  </div>
</div>
