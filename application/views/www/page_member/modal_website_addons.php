<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 婚禮網站加購
*/
?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
  <h4 class="modal-title">婚禮網站加購</h4>
  <h5 style="text-align: center;" class="modal-title">直接選擇欲加購數量即可</h4>
</div>
<form role="form" name="reply_form" id="shop" method="post" action="shop/shopView">
<div class="modal-body">
    <div class="row">
		  <div class="form-group">
     		<div class="col-lg-12 col-md-12 col-sm-12">
<?php if(@$addons=$product['addons']){ foreach($addons as $key=>$Item){
	if($Item['product_name']!='獨立網址' || (!$wedding_website[0]['customize_website_url_flag'] || !$wedding_website[0]['customize_website_url'])){?>
              <div class="row">
                <div class="col-xs-2" style="text-align:right;">
                  <input class="addon" addon_log_sn="<?php echo $Item['addon_log_sn'];?>" id="addon<?php echo $Item['addon_log_sn'];?>" type="checkbox" name="addon[<?php echo $key;?>][addon_log_sn]" value=<?php echo $Item['addon_log_sn'];?>>
                </div>
                <div class="col-xs-5">
                  <!--span class="btn btn-warning btn-xs">加購</span-->
                  <?php echo $Item['product_name'];?>
                </div>
                <div class="col-xs-2" style="text-align:right;">
									NT$<?php echo number_format($Item['addon_price']);?>
                </div>
                <div class="col-xs-3">
                  <select id="addon_amount<?php echo $Item['addon_log_sn'];?>" addon_log_sn="<?php echo $Item['addon_log_sn'];?>" addon_limitation="<?php echo $Item['addon_limitation'];?>" unispec_qty_in_stock="<?php echo $Item['unispec_qty_in_stock'];?>" name="addon[<?php echo $key;?>][addon_amount]" class="addon_amount">
                      <option value="">0</option>
                  </select>
                </div>
              </div>
<input type="hidden" name="addon[<?php echo $key;?>][associated_product_sn]" value="<?php echo $Item['associated_product_sn']; ?>">
<input type="hidden" name="addon[<?php echo $key;?>][addon_price]" value="<?php echo $Item['addon_price']; ?>">
<input type="hidden" name="addon[<?php echo $key;?>][product_name]" value="<?php echo $Item['product_name']; ?>">
<input type="hidden" name="addon[<?php echo $key;?>][addon_limitation]" value="<?php echo $Item['addon_limitation']; ?>">
<?php }}}?>
    </div>
    </div>
    </div>
</div>

  <div class="modal-footer">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6">
        <button data-dismiss="modal" style="margin-top: 20px;margin-right: 100px;" type="button" id="cancelbutton" class="btn yellow">取消</button> &nbsp; 
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6">
        <button style="margin-top: 20px;margin-right: 100px;" type="submit" id="shopaddcart" class="btn blue">確認加購</button>
        <input type="hidden" name="wedding_website_sn" value="<?php echo $wedding_website_sn?>">
        <input type="hidden" name="original_order_sn" value="<?php echo $wedding_website[0]['associated_order_sn']?>">
        <input type="hidden" name="product_sn" value="<?php echo $wedding_website[0]['associated_product_sn']?>">
        <input type="hidden" name="qty" value="1">
      </div>
    </div>
  </div>
</form>
<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script>
jQuery(document).ready(function() {       
   	//Metronic.init(); // init metronic core components
	  //Layout.init(); // init current layout
    //ComponentsPickers.init();
		    		$('.addon_amount').each(function(){
							var addon_limitation = parseInt($(this).attr('addon_limitation')); //購買上限
							var unispec_qty_in_stock = parseInt($(this).attr('unispec_qty_in_stock')); //庫存
							if (unispec_qty_in_stock < addon_limitation) addon_limitation=unispec_qty_in_stock;
							var html='';
					//console.log(addon_limitation);
					//console.log(qty);
		    			for (var x = 0; x <= addon_limitation; x++) {
								html+='<option value="'+x+'">'+x+'</option>';
		    			}
							$(this)
							    .find('option')
							    .remove()
							    .end()
							    .append(html)
							;
						});		
				$('.addon_amount').click(function(event) {
					if($(this).val()>0){
						$('#addon'+$(this).attr('addon_log_sn')).prop('checked', true);
					}
				});							
				$('#shopaddcart').click(function(e) {
					var ifaddon=false;
	    		$('.addon').each(function(){
						//var addon_limitation = $(this).attr('addon_limitation'));
						if($(this).prop('checked')){
							//console.log($('#addon_amount'+$(this).attr('addon_log_sn')).val());
							if($('#addon_amount'+$(this).attr('addon_log_sn')).val()==0){
								ifaddon=true;
							}
						}
					});
					if(ifaddon){
								alert('請選擇欲購買加購之數量');
								return false;
					}
					
					 var url = "shop/add"; // the script where you handle the form input.
					    $.ajax({
					           type: "POST",
					           url: url,
										 dataType:'json',
					           data: $("#shop").serialize(), // serializes the form's elements.
					           success: function(data)
					           {
					           	if (data.error){
					           		alert(data.error);
					           	}else{
					           		$('.top-cart-info-count').text(data.total_items+' 件');
					           		$('.top-cart-info-value').text('$ '+data.total);
					           		//console.log(data.total);
					           		location.href='shop/shopView';
					           	}
					           }
					         });
					    e.preventDefault();
				});	
});
</script>