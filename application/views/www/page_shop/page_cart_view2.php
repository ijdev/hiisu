<?php defined('BASEPATH') OR exit('No direct script access allowed');
$_web_member_data=$this->session->userdata('web_member_data');
//var_dump($_web_member_data);
?>
<style>
  .pi-img-wrapper img{border:1px solid #EFEFEF;}
  .sale-product{background-color: #EFEFEF;padding: 12px;}
  .goods-page-image img{
    -webkit-box-shadow: 0 1px 8px rgba(0, 0, 0, 0.5);
    box-shadow: 0 1px 8px rgba(0, 0, 0, 0.5);
  }
  .del-goods-col{text-align: right;line-height: 2;}
  .del-goods{}
  .box_gift_cash{float: right; font-size: 18px; padding-right:24px;}
  .fa-exclamation-circle{font-size: 18px;background-color: #FFF;color: #FF0000;}
  .fa-usd-box{border-radius: 50%;border:2px solid #dfba49;display: inline-block;border-radius: 50% !important;    width: 18px;height: 18px;text-align: center; font-size: 12px;}
  .fa-usd-box .text-warning{font-size: 10px;}

  .shopping-total-price{border-top: 1px solid #cebea2 !important;font-weight: bold !important;padding-top: 24px !important;}
  .shopping-total{ width: 92%;}

  .table-shipping{
      border:0px; width:96% !important;margin:0 auto;
  }
  .table-shipping td{border:0px;padding: 6px 0px 6px 0;}
.shopping_steps {
    width: 695px;
    text-align: center;
    font-size: 1.2em;
    color: #333;
    height: 52px;
    margin: 20px auto;
}
.shopping_steps .step_over {
    width: 150px;
    line-height: 12px;
    height: 50px;
    float: left;
    position: relative;
    font-weight: 700;
    margin-right: 5px;
    margin-left: 5px;
}
.shopping_steps .step_arrow {
    width: 15px;
    float: left;
}
.shopping_steps .step_normal {
    width: 150px;
    line-height: 12px;
    height: 50px;
    float: left;
    position: relative;

    color: #ccc;
    margin-right: 5px;
    margin-left: 5px;
}
.shopping_steps .step_number {
    background-color: #DB4A4A;
    color: #fff;
    position: absolute;
    top: 25px;
    left: 68px;
    font-size: 15px;
    line-height: 20px;
    height: 20px;
    width: 20px;
    border-radius: 10px !important;
}
.shopping_steps .step_normal_number {
    background-color: #ccc;
    color: #fff;
    position: absolute;
    top: 25px;
    left: 65px;
    font-size: 15px;
    line-height: 20px;
    height: 20px;
    width: 20px;
    border-radius: 10px !important;
}
i.icon-caret-right:before {
    content: "\f0da";
    font-family: fontAwesome;
    font-style: normal;
    font-size: 18px;
}
.margin-bottom-25.display_none {
    display: none;
}
.clear_shopcart {
    display: table;
    line-height: 25px;
    width: 100%;
    margin: auto;
    background-color: rgba(210,210,210,0.2);
    margin-bottom: 15px;
    border: 1px solid rgba(210, 210, 210, 0.2);
    text-align: right;
}
.clear_shopcart a {
    display: block;
    margin: 5px;
    float: right;
    cursor: pointer;
    color: #8F8F8F;
    line-height: 25px;
    height: 25px;
    padding: 5px;
    padding-top: 0px;
    padding-bottom: 0px;
}
.clear_shopcart i {
    margin-right: 5px;
    font-size: 14px;
    color: #333333;
}
i.icon-trashhs:before {
    content: "\f014";
    font-family: fontAwesome;
    font-style: normal;
}
.trans_name {
    width: 100%;
    margin: auto;
    text-align: left;
    font-size: 16px;
    color: #333333;
    margin-bottom: 10px;
}



.border_left {
    border-left: 1px solid rgba(100,100,100,0.1) !important;
}
table.responsive {
    width: 100%;
    margin: 0;
    padding: 0;
    border-collapse: collapse;
    border-spacing: 0;
}
table {
    background-color: transparent;
}
table.responsive tr {
    border: 1px solid #ddd;
}
table.responsive th {
    background-color: #F3F3F3;
}
table.responsive th {
    text-transform: uppercase;
    font-size: 14px;
    letter-spacing: 1px;
}
li{
  list-style:none;
}
table.responsive th, table.responsive td {
    padding: 5px;
    text-align: center;
}
.shopping_present {
    padding: 10px;
    border: 1px solid rgba(100,100,100,0.1);
}
#cart_add_items .pics {
    width: 70px;
}
td.padding-top-20 {
    padding-bottom: 20px;
}
.shop-item-price.height-50 {
    color: #de4a50;
    font-size: 18px;
}
.cart_botton {
    border-radius: 3px !important;
  color:#66667c;
}
.cart_botton_r {
    border-radius: 3px !important;
  color:#fff;
}
.cart_botton:hover {
    border-radius: 3px !important;
  color:#000;
}
.cart_botton_r:hover {
  color:#fff;
}
.tab_lable label.radio {
    display: inline-block;
    margin: 2px 0px 7px 0px;
    font-size: 15px;
    color: #404040;
    cursor: pointer;
    font-weight: 600;
  float: left;
}

td.shopping_border.shp-rdo_box {
    padding: 0px 0px 0px 0px;
  background-color:#fff;
}
td.shopping_border.shp-rdo_box .tab-content {
    background: #fff;
  border-bottom: 1px solid #ddd;
}
.nav-pills li a label.radio {

      float: none;
}
.invoice_option {
    padding: 10px;
}

.radio{
  color:#999;
  font-size:15px;
  position:relative;
}
.radio span {
    cursor: pointer;
    position: relative;
    padding-left: 28px;
}

.radio span:after{
  content:'';
  width:19px;
  height:19px;
  border: 2px solid rgba(0,0,0,0.3);
  position:absolute;
  left:0;
  top:1px;
  border-radius:100%;
  -ms-border-radius:100%;
  -moz-border-radius:100%;
  -webkit-border-radius:100%;
  box-sizing:border-box;
  -ms-box-sizing:border-box;
  -moz-box-sizing:border-box;
  -webkit-box-sizing:border-box;
}
.radio span:hover:after{

border: 2px solid #333333;
}

.radio input[type="radio"]{
   cursor: pointer;
  position:absolute;
  width:100%;
  height:100%;
  z-index: 1;
  opacity: 0;
  filter: alpha(opacity=0);
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)"
}
.radio input[type="radio"]:checked + span{
  color:#000;

}
.radio input[type="radio"]:checked + span:after{
    width:19px;
  height:19px;
  border: 2px solid #000!important;
}
.radio input[type="radio"]:checked + span:before{
    content:'';
   border:2px solid #000!important;
  width:5px;
  height:5px;
  position:absolute;
  background:#000!important;
  left:7px;
  top:8px;
  border-radius:100%;
  -ms-border-radius:100%;
  -moz-border-radius:100%;
  -webkit-border-radius:100%;
}
.padding0{
  padding:0px;
}
.padding_left0{
  padding-left:0px;
}
.padding_right0{
  padding-right:0px;
}

.W100{
  float:left;
  width:100%;
  display:block;
}
.inpthr{

    margin: auto;
    width: 44%;
    display: block;
    padding: 7px;
    text-align: center;
    border: 1px solid #ddd;

}
.shopping_steps .step_over {

    color: #ccc;
}
.blkfont {
   font-weight: 700;
    color: #000!important;
}
label.sphov {
    cursor: pointer;
}
.rdbtnicls{
  padding-left: 21%;
  color:rgb(60, 118, 61);
  display: none;

}
.fbtnsgrn:hover span:after, #tboneprtsec:hover span:after, #tboneprt:hover span:after {
   border: 2px solid #333333;
}
</style>
<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<link href="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="public/metronic/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css"><!-- for slider-range -->
<link href="public/metronic/global/plugins/rateit/src/rateit.css" rel="stylesheet" type="text/css">
<!-- Page level plugin styles END -->
			<ul class="breadcrumb">
				<li><a href="/">首頁</a></li>
				<li><a href="shop">線上商店</a></li>
				<li class="active">購物車</li>
			</ul>
	<!-- shopping steps -->
			<div class="shopping_steps" style="width:521px;">
				 <div class="step_over"><div class="step_normal_number">1</div>商品及配送</div>
				 <div class="step_arrow"><i class="icon-caret-right"></i></div>
				 <!--div class="step_normal"><div class="step_normal_number">2</div>折扣運費</div>
				 <div class="step_arrow"><i class="icon-caret-right"></i></div-->
				 <div class="step_normal blkfont"><div class="step_number ">2</div>填寫資料</div>
				 <div class="step_arrow"><i class="icon-caret-right"></i></div>
				 <div class="step_normal"><div class="step_normal_number">3</div>訂單完成</div>
			</div>
			<!--<div class="clear_shopcart"><a onclick="location.href='cart.php?Action=clear';"><i class="icon-trashhs"></i>清空購物車</a></div>-->
	<!-- shopping steps -->
	<!-- BEGIN SIDEBAR & CONTENT -->
<div id="showshopping">
       <!--div class="trans_name mob_trans">購買商品清單</div-->
       <table class="table-hover responsive mob_tab">
			<thead>
				 <tr>
					<th class="span_hidden">序號
					</th><th>圖片
					</th><th>商品名稱
					</th><th>賣家
          			</th><th>規格
					</th><th>價格
					</th><th>數量
					</th><th>優惠折扣
					</th><th>小計</th>
					</tr>
			</thead>
			<tbody class="mob_tbody">
						<?php
							$count=1;
						if($cart){foreach($cart as $Item)
						{?>
							<tr class="sm-flex notrash">
							  <td data-label="序號" class="span_hidden mob_td1">
							  <?php echo $count;?>
							  </td>
							  <td class="mob_td2" data-label="圖片"><a href="/shop/shopItem/<?php echo $Item['product']['product_sn'];?>">
								<?php if(!$Item['addon_log_sn']){?><img onerror="this.src='public/uploads/notyet.gif'" src="public/uploads/<?php echo $Item['image_path'];?>" alt="<?php echo $Item['image_alt'];?>" width="70" >
								<?php }else{?>
										<span>加購</span>
									<?php }?>
							  </a>
							  </td>
							  <td class="mob_td3" data-label="商品名稱" class="receivePay_text"><a href="/shop/shopItem/<?php echo $Item['product']['product_sn'];?>"><?php echo $Item['name'];?></a></td>
							  <td class="mob_td4_1 receivePay_text" data-label="賣家"><?php echo $Item['product']['brand'];?></td>
                <td class="mob_td4_1" data-label="規格"><span><?php echo @$Item['product_package_name'];?> <?php echo @$Item['mutispec_color_name'];?></span></td>
								<td class="mob_td4_1" data-label="價格"><span>$<?php echo $Item['price'];?></span></td>
								<td class="mob_td5_1" data-label="數量">
									<span>x<?php echo $Item['qty'];?></span>
								</td>
								<td class="mob_td5_1" data-label="優惠折扣"><span>$<?php echo $Item['coupon_money'];?></span></td>
								<td class="mob_td5_1" data-label="小計"><span>$<?php echo $Item['subtotal'];?></span></td>
								</tr>
								<?php $supplier_name=$Item['supplier_name'];$count++; }

								}else{
								echo '<ul>購物車是空的喔，快去採購吧^^</ul>';}?>

          </tbody>
		</table>
	<table width="100%" class="tab_lable">
          <tbody>
				<!--<tr>
					<td height="34" align="right"><div class="shop-item-price height-50">商品總金額：609元</div></td>
				</tr>-->
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td id="showtrans">
                    <table align="right" class="nomargin">
                          <tbody><tr>
                            <td align="center">&nbsp;</td>
                            <td align="left">&nbsp;</td>
                            <td colspan="2" align="right" style="padding-right:2px;">商品總金額：</td>
                            <td align="right">$<?php
								if(@$this->cart->total()){
									echo number_format($this->cart->total());
								}else{
									echo "0";
								}
							?> 元</td>
                          </tr>
                          <!--tr>
                            <td align="center">&nbsp;</td>
                            <td align="left">&nbsp;</td>
                            <td colspan="2" align="right" style="padding-right:2px;">折扣後金額：</td>
                            <td align="right">$<?php
								if(@$this->cart->total()){
									echo number_format($this->cart->total());
								}else{
									echo "0";
								}
							?>元 </td>
                          </tr-->
                          <tr>
                            <td align="center">&nbsp;</td>
                            <td align="left">&nbsp;</td>
                            <td colspan="2" align="right" style="padding-right:2px;">運費合計：</td>
                            <td align="right">$<?php echo $down_cart['shipping_charge_amount'];?> 元</td>
                          </tr>
                          <!--tr>
                            <td align="center">&nbsp;</td>
                            <td align="left">&nbsp;</td>
                            <td colspan="2" align="right" style="padding-right:2px;">配送方式：</td>
                            <td align="right"><?php echo @$delivery_method_name;?> </td>
                          </tr-->
                          <tr>
                            <td align="center">&nbsp;</td>
                            <td align="left">&nbsp;</td>
                            <td colspan="2" align="right" class="shop-item-price height-50">消費總金額：</td>
                            <td align="right" class="shop-item-price height-50">$
                            <?php echo number_format($down_cart['total_order_amount']);?> 元 </td>
                          </tr>
                     </tbody></table>
                    </td>
				</tr>
<form name="shoppingform" id="shoppingform" action="shop/gotoPay" method="POST" class="bv-form">
				<tr>
					<td>
							<table width="100%" height="30" border="0" cellpadding="3" cellspacing="0" style="border: 0px solid #cccccc;">
								<tbody>
								<tr>
								<td height="35" valign="bottom"><!--付款方式-->
									<div class="trans_name" style="width:auto;">付款方式</div>
								</td>
							</tr>
							<tr>
						<td class="padding-10 shopping_border">
			     <?php if($payments){ foreach($payments as $payment){?>
								<div class="col-md-12 col-sm-12 margin-top-10 clearfix">
									<div class="col-md-2 col-sm-12 padding-0 clearfix">
										<label class="radio inline"><input type="radio" required id="payment_method_<?php echo $payment['payment_method'];?>" required class="form-control" name="order[payment_method]" value="<?php echo $payment['payment_method'];?>" <?php if(@$payment['checked']) echo 'checked';?>> <span> <?php echo $payment['payment_method_name'];?></span> </label>
									</div>
									<div class="col-md-10 col-sm-12 padding-0">
										<?php echo $payment['description'];?>
									</div>
								</div>
			     <?php }}?>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td valign="bottom"><div class="trans_name" style="width:auto;">發票格式</div></td>
							</tr>
							<tr>
								<td class="shopping_border shp-rdo_box"><!--發票-->
									<div class="col-md-12 col-sm-12">
										<div class="form-group has-feedback">
											<ul class="nav-pills">
												<li class="active" style="margin-left: -16px;">
											<a id="tboneprt" >
													<label class="radio inline">
											  <input name="ifinvoice" required id="ifinvoice0" value="0" onclick="$('#ifinvoice_f1').hide();" data-bv-field="ifinvoice" type="radio" style="" value="0"  />
											  <span class="greenon"> 二聯式發票 </span>
											   </label>
											   </a>
												</li>
												<li>
											<a id="tboneprtsec" >
													<label class="radio inline">
											  <input name="ifinvoice" required id="ifinvoice1" value="1" onclick="$('#ifinvoice_f1').show();" data-bv-field="ifinvoice" type="radio"  style=""  />
											  <span class="greenon"> 三聯式統一發票 </span>
										   </label>
												<i class="form-control-feedback" data-bv-icon-for="ifinvoice" style="display: none;"></i></a>
												</li>
												<!--<i id="rightsh" class="form-control-feedback glyphicon glyphicon-ok" data-bv-icon-for="ifinvoice" style="display:none;color: #3c763d; "></i>-->
											</ul>
										<small class="help-block" data-bv-validator="notEmpty" data-bv-for="ifinvoice" data-bv-result="NOT_VALIDATED" style="display: none;">請選擇發票</small></div>
										<!--div>貼心提醒：配合無紙化政策，紙本發票僅提供有統編之公司索取。發票將在成功收貨後14個工作日內寄出。</div-->
									</div>
									<div class="tab-content">
										<div id="ifinvoice_f0" style="display:none;" >
										</div>
										<div id="ifinvoice_f1" style="display:none;">
											<div id="invoiceshow1"  class="col-md-12 col-sm-12 margin-top-10 invoice_option">
												<div class="form-group has-feedback">
													<label>* 統一編號</label>
                          <input type="text" class="form-control" id="company_tax_id" name="order[company_tax_id]" value="<?php echo @$order["company_tax_id"]?>" maxlength="8" placeholder="統一編號">
												</div>
												<div class="form-group has-feedback">
													<label>* 發票抬頭</label>
                          <input type="text" class="form-control" id="invoice_title" name="order[invoice_title]" value="<?php echo @$order["invoice_title"]?>" placeholder="發票抬頭">
												</div>
											</div>
											<input class="inpthr" type="input" value="提醒：若後續辦理退貨，須填寫銷貨折讓單並蓋公司大小章或統一發票章" name="invoice_num" id="invoice_num" class="form-control" />
										</div>
									</div>

								</td>
							</tr>
								<tr><td>&nbsp;</td></tr>
							<tr>
								<td><div class="trans_name" style="width:auto;">訂購人資訊</div>
								<!--<input type="button" name="button3" id="button3" value="修改"  onclick="showWin('url','shopping_ajax_userinfo.php?act=modi','',550,250);">--></td>
							</tr>
							<tr>
								<td height="35" valign="bottom" id="showuserinfo"><input name="act" value="update" type="hidden">
<div class="shopping_border">
		<div class="col-xs-12 col-sm-6 padding0">
		<div class="col-xs-12 col-sm-12" style="margin:15px 0 0">
			<div class="form-group has-feedback">
                <input type="checkbox" class="fdcheck" id="tab2_check" name="update_member" value="yes">
				<label class="sphov" for="tab2_check"><span>結帳成功後同步更新您的會員資料(重登後生效)</span></label>
				<label>* 電子信箱</label>
				<input type="text" required id="email" name="order[buyer_email]" class="form-control" value="<?php echo (@$order["buyer_email"])? $order["buyer_email"]:$_web_member_data["email"];?>">
			</div>
		</div>
		<div class="col-xs-12 col-sm-12">
			<div class="form-group has-feedback">
				<label>* 訂購人姓名</label>
		        	<input type="text" required id="last_name" name="order[buyer_last_name]" class="form-control" value="<?php echo (@$order["buyer_last_name"])? $order["buyer_last_name"]:$_web_member_data["last_name"];?>" >
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 margin-0">
			<div class="form-group has-feedback">
				<label>* 訂購人地址</label><br>
				<div class="padding_left0 col-xs-12 col-sm-12 col-md-4 padding-0 margin-bottom-10">
					<select id="buyer_addr_state" name="order[buyer_addr_state]" class="form-control select" style=" float:left" data-bv-field="county2"><option value="">請選擇</option>
		              <option value="台灣本島" <?=(@$_web_member_data['addr_state']=='台灣本島' || @$order['buyer_addr_state']=='台灣本島')?'selected':'';?>>台灣本島</option>
		              <option value="外島地區" <?=(@$_web_member_data['addr_state']=='外島地區' || @$order['buyer_addr_state']=='外島地區')?'selected':'';?>>外島地區</option>
					</select>
				</div>
				<div class=" col-xs-12 col-sm-12 col-md-4 padding-0 margin-bottom-10">
					<select id="addr_city_code" name="order[buyer_addr_city]" class="form-control select" data-bv-field="province2">
                      <option value="" disable> 縣市 </option>
						<?php while (list($keys, $values) = each ($city_ct)) {
							echo  ' <option value="'.$keys.'">'.$values.'</option>';
						}?>
					</select>
				</div>
				<div class="padding_right0 col-xs-12 col-sm-12 col-md-4 padding-0 margin-bottom-10">
					<select id="addr_town_code" name="order[buyer_addr_town]" class="form-control select">
						<option value="">請先選擇縣市</option>
					</select>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-3">
			<div class="form-group margin-bottom-10">
                <input type="text" required id="zipcode" name="order[buyer_zipcode]" class="form-control" size="8" maxlength="5" placeholder="郵遞區號" value="<?php echo (@$order["buyer_zipcode"])? $order["buyer_zipcode"]:$_web_member_data["zipcode"];?>"> 
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9">
			<div class="form-group has-feedback">
        <input type="text" required id="addr" name="order[buyer_addr1]" class="form-control" placeholder="地址" value="<?php echo (@$order["buyer_addr1"])? $order["buyer_addr1"]:$_web_member_data["addr1"];?>">
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 padding-0">
				<div class="col-xs-12 col-sm-12" style=" margin:15px 0 0">
			<div class="form-group has-feedback">
				<label>* 聯絡電話</label>
                    <input type="text" id="tel" name="order[buyer_tel]" class="form-control" size="16" maxlength="10" placeholder="請輸入市話號碼" value="<?php echo (@$order["buyer_tel"])? $order["buyer_tel"]:$_web_member_data["tel"];?>">
			</div>
		</div>
		<div class="col-xs-12 col-sm-12">
			<div class="form-group has-feedback">
				<label>* 聯絡手機</label>
                <input type="text" required id="cell" name="order[buyer_cell]" class="form-control" value="<?php echo (@$order["buyer_cell"])? $order["buyer_cell"]:$_web_member_data["cell"];?>">
			<small class="help-block" data-bv-validator="notEmpty" data-bv-for="other_tel" data-bv-result="NOT_VALIDATED" style="display: none;">請輸入手機，不可空白</small></div>
		</div>
			</div>
	<div style="clear:both"></div>
</div>

</td>
							</tr>
							<tr><td>&nbsp;</td></tr>
							<tr>
								<td><div class="trans_name" style="width:auto;">收貨人資訊</div>
								<div class="check_same">
								<input id="checkbox_data_sync_1" type="checkbox" class="fdcheck">
					<label class="sphov" for="checkbox_data_sync_1"><span>資料同訂購人請打勾</span></label>
								<!--a href="javascript:void(0);" onclick="showpage(&quot;shopping_ajax_receiver.php&quot;,&quot;&quot;,&quot;showreceiver&quot;);"><i class="icon-edit"></i> 從收件人記錄本填入</a--> </div>
								</td>
							</tr>
							<tr>
								<td id="showreceiver"></td>
							</tr>
							<tr>
								<td id="showshoppingreceiver"><div class="shopping_border">
	<div class="col-xs-12 col-sm-6 padding-0">
		<!--div class="col-xs-12 col-sm-12" style="margin:15px 0 0">
			<div class="form-group has-feedback">
				<label>* 電子信箱</label>
				<input name="order[receiver_email]" id="receiver_email" type="email" value="" size="32" class=" form-control">
			</div>
		</div-->
		<div class="col-xs-12 col-sm-12" style="margin:15px 0 0">
			<div class="form-group has-feedback">
				<label>* 收貨人姓名</label>
                <input type="text" required id="name1" name="order[receiver_last_name]" class="form-control" value="<?php echo (@$order["receiver_last_name"])? $order["receiver_last_name"]:@$receiver_data["last_name"];?>" placeholder="*請填寫您的有效證件上的真實姓名，以免延誤您按時收到貨物">
			</div>

		</div>

		<div class="col-xs-12 col-sm-12">
			<div class="form-group has-feedback">
				<label>* 聯絡電話</label>
				<input type="text" id="tel1" class="form-control" name="order[receiver_tel]" size="16" maxlength="10" placeholder="請輸入市話號碼" value="<?php echo (@$order["receiver_tel"])? $order["receiver_tel"]:@$receiver_data["tel"];?>">
			</div>
		</div>

		<div class="col-xs-12 col-sm-12">
			<div class="form-group has-feedback">
				<label>* 聯絡手機</label>
                <input type="text" required id="cell1" name="order[receiver_cell]" class="form-control" value="<?php echo (@$order["receiver_cell"])? $order["receiver_cell"]:@$receiver_data["cell"];?>">
			</div>
		</div>
				<div class="col-xs-12 col-sm-12">
			<div class="form-group">
				<label class="W100">* 收貨人地址</label>
				<div class="padding_left0 col-xs-12 col-sm-12 col-md-4 padding-0 margin-bottom-10">
					<select id="receiver_addr_state" name="order[receiver_addr_state]" class="form-control select" style=" float:left">
						<option value="">請選擇</option>
		                <option value="台灣本島" <?=(@$receiver_addr_state=='台灣本島' || @$order['receiver_addr_state']=='台灣本島')?'selected':'';?>>台灣本島</option>
		                <option value="外島地區" <?=(@$receiver_addr_state=='外島地區' || @$order['receiver_addr_state']=='外島地區')?'selected':'';?>>外島地區</option>
					</select>
				</div>
				<div class=" col-xs-12 col-sm-12 col-md-4 padding-0 margin-bottom-10">
					<select id="addr_city_code1" name="order[receiver_addr_city]" class="form-control select">
                      <option value="" disable> 縣市 </option>
						<?php while (list($keys, $values) = each ($receiver_addr_citys)) {
							echo  ' <option value="'.$keys.'">'.$values.'</option>';
						}?>
					</select>
				</div>
				<div class="padding_right0 col-xs-12 col-sm-12 col-md-4 padding-0 margin-bottom-10">
					<select id="addr_town_code1" name="order[receiver_addr_town]" class="form-control select">
						<option value="">請先選擇縣市</option>
					</select>
				</div>
			</div>

		</div>

		<div class="col-xs-12 col-sm-12 col-md-3">
			<div class="form-group margin-bottom-10">
                <input type="text" required id="zipcode1" name="order[receiver_zipcode]" class="form-control" size="8" maxlength="5" placeholder="郵遞區號" value="<?php echo (@$order["receiver_zipcode"])? $order["receiver_zipcode"]:@$receiver_data["zipcode"];?>">
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-9">
			<div class="form-group has-feedback">
                <input type="text" required id="addr1" name="order[receiver_addr1]" class="form-control" value="<?php echo (@$order["receiver_addr1"])? $order["receiver_addr1"]:@$receiver_data["addr1"];?>">
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 margin-bottom-10">
		*因收貨地區與運費計算相關，如需修改請至上一步修改正確配送地區，謝謝。</div>

	</div>

	<div class="col-xs-12 col-sm-6 padding-0">
		<!--div class="col-xs-12 col-sm-12" style="margin:15px 0 0">
			<div class="form-group">
				<label>配送日期</label>
				<input type="text" class="form-control hasDatepicker" id="datepicker" name="datepicker" readonly="">
			</div>
		</div>
		<div class="col-xs-12 col-sm-12" style="margin:15px 0 0">
			<div class="form-group has-feedback">
				<label class="radio W100">* 宅配時間</label>
					<label class="radio inline fbtnsgrn">
					  <input value="1" name="HomeTimeType" id="HomeTimeType" type="radio" style="" value="0" data-bv-field="HomeTimeType"/>
					  <span class="greenon4"> 不指定時間 </span>
					</label>
					<label class="radio inline fbtnsgrn"><input type="radio" value="5" name="HomeTimeType" id="HomeTimeType" data-bv-field="HomeTimeType" /><span class="greenon4">早上09：00～12：00</span></label>
					<label class="radio inline fbtnsgrn"><input type="radio" value="6" name="HomeTimeType" id="HomeTimeType" data-bv-field="HomeTimeType" /><span class="greenon4">下午12：00~17：00</span></label>
					<label class="radio inline fbtnsgrn"><input type="radio" value="7" name="HomeTimeType" id="HomeTimeType" data-bv-field="HomeTimeType"><span class="greenon4">晚上17：00~20：00</span></label>
			</div>
		</div-->
		<div class="col-xs-12 col-sm-12" style="margin:15px 0 0">
			<div class="form-group">
				<label>訂單備註</label>
				<textarea name="sub_order[memo]" id="receiver_memo" rows="6" cols="80" class="form-control"><?php echo @$order["memo"]?></textarea>
			</div>
		</div>

		<!--div class="col-xs-12 col-sm-12">
			<input type="checkbox" id="ifsendreceiver" class="fdcheck" value="1" name="ifsendreceiver">
			<label class="sphov" for="ifsendreceiver"><span>訂單副本寄給收貨人</span></label>
		</div-->

	</div>

	<div style="clear:both"></div>

</div>

</td>
							</tr>

							</tbody></table>
						</td>
					</tr>
				<tr>
					<td height="24" align="right" valign="middle">&nbsp;</td>
				</tr>
				<tr>
					<td height="23" class="padding-top-20" align="center">
						<input type="button" name="button3" id="button3" value="上一步" onclick="location.href='/shop/shopView';" class="btn size-15 cart_botton"> &nbsp;
						<input type="submit" name="button" id="submit1" value="送出訂單並付款" class="btn size-15 btn-primary cart_botton_r">
					</td>
				</tr>
</form>
		</tbody>
	</table>
 </div>
<!-- copy data -->




<!-- END SIDEBAR & CONTENT -->
<div id="gift_cash_exclamation" style="display:none;">
  <div class="alert alert-success">
    <ul>
      <li>每次消費可百分百(100%)，1點折抵新台幣1元，全額折抵訂購金額．</li>
      <li>系統依購物金的使用期限，自動從最接近過期的點數開始折抵．</li>
      <li>您可前往「會員中心」查看您持有的購物金餘額，使用期限與紀錄．</li>
    </ul>
  </div>
</div>
<div id="coupon_input" style="display:none;">
  <div style="width:75%">
    <div style="height:32px;"></div>
    <strong>請輸入代碼（折扣券、活動代碼皆適用）</strong>
    <form class="form-horizontal" role="form" id="coupon_form">
      <div class="form-body">
        <div class="form-group">
          <div class="col-md-12">
            <div class="input-group">
<div style="display:-webkit-box;text-align:center;">
	              <input required type="text" id="couponcode" style="width:420px;" class="form-control" placeholder="請輸入活動代碼並按確定">
              <span class="input-group-btn">
              <button class="btn blue couponcode" type="submit">確定</button>
              </span>
</div>
<div class="note note-success note-bordered" id="note_couponcode" style="text-align:center;width:auto;display:none;"></div>
            </div>
            <span class="help-block">
              請注意英文字母大小寫視為不同，單次結帳恕無法重複折扣．
            </span>
          </div>
        </div>
      </div>
	<input type="hidden" id="rowid">
    </form>
  </div>
</div>

