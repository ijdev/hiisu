<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sys_member extends My_general_ct {
    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('sean_models/Sean_db_tools');
		/* Begin 修改  */
		$this->_title_name="系統管理員";// 功能名稱
		$this->_url="sys_member/";//列表功能url
		$this->_url_item="sys_member/sys_member_item/";//處理動作 url
		$this->_table="ij_member";//table name
		$this->_table_key="member_code";//table key
		$this->_table_field=array(
					 "user_name" => "帳號",
					 "email" => "email",
					 "last_name" => "姓",
					 "first_name" => "名",
					 "tel" => "市話",
					 "cell" => "手機",
					 "wedding_date" => "結婚日期",
					 "birthday" => "生日",
					 "addr1" => "住址",
					 "memo" => "備註",
					 "password" => "密碼",
					 "member_status" => "帳號狀態",
					 "member_level_type" => "會員等級",
					 "system_user_flag" => "是否為總控人員",
				);

		/* End 修改  */

    }

    //取得列表內容
    public function table_value_list( $_where,$_id = 0)
	{
		$_table_key=$this->_id;
	    $page_data['_body_result']=array();
		//判斷是否有帶入特殊查詢
		if(strlen($_where)==0)
		{
			$_where="  order by apply_date asc";

		}
		// 取得列表
		$_v=" ".$this->_table.".*, concat(first_name,'',last_name) as ijtmp_name ";
		$_result01=$this->Sean_db_tools->db_get_max_record($_v,$this->_table,$_where);

		/* 列表標頭名稱 */
		$page_data['_table_thead_name']=array("姓名","Email","縣市","鄉鎮市區","住址","電話","結婚日期","關聯廠商","帳號狀態","功能");
		$page_data['_table_thead_field']=array("ijtmp_name","email","addr_state","addr_city","addr1","tel","wedding_date","member_level_type","member_status");


		/* 類表內容 */
		if(is_array($_result01))
		{
                 		$_i=0;
                 		foreach($_result01 as $key => $value)
                    	{


			                    $_body_result='';
						/* Begin 修改  */
			                    $i_key=0;
								foreach(	$page_data['_table_thead_field'] as $key => $_value)
								{
									$page_data['_body_result'][$_i][$i_key]=array("_value"=>$value->$_value,"_class"=>"");
			                    	$i_key++;
								}


						/* End 修改  */
									$page_data['_body_result'][$_i][$i_key]=array("_value"=>'
			                    		 		<a href="'.$this->init_control.$this->_url_item.'edit/'.$value->$_table_key.'"><i class="fa fa-edit"></i>編輯</a>&nbsp;&nbsp;
						                      <a href="'.$this->init_control.$this->_url_item.'delete/'.$value->$_table_key.'"><i class="fa fa-circle-o-notch"></i>重發驗證信</a>&nbsp;&nbsp;
											   <a href="'.$this->init_control.$this->_url_item.'delete/'.$value->$_table_key.'"><i class="fa fa-lightbulb-o"></i>忘記密碼&nbsp;&nbsp;
						                  	',"_class"=>'class="center"');
                				$_i++;


                			}

    				}
			return $page_data;

    }


}