<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>分潤結帳</h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">分潤結帳</span>
            </div>
            <div class="actions btn-set">
            </div>
          </div>
          <div class="note note-warning">
            <div class="input-group input-large date-picker input-daterange  pull-left" data-date="2015-05" data-date-format="yyyy-mm">
              <span class="input-group-addon" onclick="alert('Start Search');"> 期間 </span>
              <input type="text" class="form-control input-sm" name="from" value="2015-05">
              <span class="input-group-addon"> 到 </span>
              <input type="text" class="form-control input-sm" name="to" value="2015-06">
            </div>
            <div class="table-actions-wrapper pull-right">
              <span>
              </span>
              <input type="text" class="pagination-panel-input form-control input-inline input-md" size="12" style="margin: 0 5px; font-size:10px;" placeholder="聯盟會員名稱">
              <input type="text" class="pagination-panel-input form-control input-inline input-md" size="12" style="margin: 0 5px; font-size:10px;" placeholder="經銷商名稱">
              <select class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;">
                <option value="">出帳狀態</option>
                <option value="">未出帳</option>
                <option value="">已出帳</option>
              </select>
              <select class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size:11px;">
                <option value="">客服人員</option>
                <option value="">Administrator</option>
                <option value="">Lucile</option>
                <option value="">Jenny</option>
              </select>
              <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button>
              <button class="btn btn-sm green table-group-action-submit"><i class="fa fa-download"></i> 下載</button>
            </div>
            <div style="clear:both;"></div>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>結算年月</th>
                  <th>聯盟會員名稱</th>
                  <th>經銷商名稱</th>
                  <th>訂單數量</th>
                  <th>分潤金額</th>
                  <th>出帳狀態</th>
                  <th>出帳日期</th>
                  <th>變更時間</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=0;$i<8;$i++):?>
                  <tr class="odd gradeX">
                    <td>201506</td>
                    <td>DSDS</td>
                    <td>典華(與聯盟會員名稱不並存)</td>
                    <td class="text-center">
                      20
                    </td>
                    <td>870</td>
                    <td class="text-center">
                      <?php if($i%2==0):?>
                        <span class="label label-warning">未出帳</span>
                      <?php else:?>
                        <span class="label label-default">已出帳</span>
                      <?php endif;?>
                    </td>
                    <td><small>2015-05-20</small></td>
                    <td><small>2015-05-20 12:00:00</small></td>
                    <td class="text-center">
                      <a href="#form_edit" data-toggle="modal" ><i class="fa fa-edit"></i>編輯</a>
                    </td>
                  </tr>
                <?php endfor;?>
              </tbody>
            </table>
            <!-- page nav -->
            <div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                  <ul class="pagination" style="visibility: visible;">
                    <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                    <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li class="active"><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                    <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- end of page nav -->
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
<!-- Modal for 表單填寫 -->
<div class="modal fade" id="form_edit" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">分潤結帳設定</h4>
      </div>
      <div class="modal-body">
         <div class="form">
          <form class="form-horizontal form-without-legend" role="form">
            <div class="form-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-5 control-label">結算年月</label>
                    <div class="col-md-7">
                      <span class="form-control-static">201506</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-5 control-label">聯盟會員</label>
                    <div class="col-md-7">
                      <span class="form-control-static"></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-5 control-label">經銷商</label>
                    <div class="col-md-7">
                      <span class="form-control-static">典華</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-5 control-label">訂單數量</label>
                    <div class="col-md-7">
                      <span class="form-control-static">20</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-5 control-label">分潤金額</label>
                    <div class="col-md-7">
                      <span class="form-control-static">2000</span>
                    </div>
                  </div>
                </div>
              </div>
              <hr>
              <div class="form-group">
                <label class="col-md-3 control-label">出帳狀態 <span class="require">*</span></label>
                <div class="col-md-9">
                  <select class="form-control">
                    <option>未出帳</option>
                    <option>已出帳</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">出帳日期 <span class="require">*</span></label>
                <div class="col-md-9">
                  <input type="text" class="form-control form-control-inline input-small date-picker" value="2015-05-08">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">實際出帳金額 <span class="require">*</span></label>
                <div class="col-md-9">
                  <input type="text" class="form-control form-control-inline input-small" value="">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">備註</label>
                <div class="col-md-9">
                  <textarea class="form-control" rows="3"></textarea>
                </div>
              </div>
            </div>
            <div style="clear:both;"></div>
          </form>
         </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">關閉</button>
        <button type="button" class="btn blue">更新</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- Modal for 表單填寫 -->