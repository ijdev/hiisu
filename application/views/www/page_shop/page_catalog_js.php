<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 線上商店分類 page level js
*/
?>


<script src="//code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script><!-- for slider-range -->
<script src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
<script src="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->
<script src='public/metronic/global/plugins/zoom/jquery.zoom.min.js' type="text/javascript"></script><!-- product zoom -->
<script src="public/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script><!-- Quantity -->


<!-- BEGIN LayerSlider -->
<script src="public/metronic/global/plugins/slider-layer-slider/js/greensock.js" type="text/javascript"></script><!-- External libraries: GreenSock -->
<script src="public/metronic/global/plugins/slider-layer-slider/js/layerslider.transitions.js" type="text/javascript"></script><!-- LayerSlider script files -->
<script src="public/metronic/global/plugins/slider-layer-slider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script><!-- LayerSlider script files -->
<script src="public/metronic/frontend/pages/scripts/layerslider-init.js" type="text/javascript"></script>
<!-- END LayerSlider -->

<!-- BEGIN LayerSlider -->
<!-- END LayerSlider -->

<script src="public/metronic/frontend/layout/scripts/layout.js" type="text/javascript"></script>
<script type="text/javascript">
	var category_sn='<?php echo $_key_id;?>';
    jQuery(document).ready(function() {
		$('.fdcheck').on('change', function(){
		  var category_list = [];
		  $('#filterschk :input:checked').each(function(){
			var category = $(this).val();
			//console.log(category);
			category_list.push(category);
		  });

		  if(category_list.length == 0)
			$('.filterpro').fadeIn();
		  else {
			$('.filterpro').each(function(){
			  	var item = $(this).attr('data-tag');
			  	var item_array = item.split(" ");
			  	//console.log(jQuery.inArray(item,category_list));
		  		$if_attr=false;
			  		//console.log(category_list);
			  	item_array.forEach(function(entry) {
    				//console.log(jQuery.inArray(entry,category_list));
				  	if(jQuery.inArray(entry,category_list) > -1){
				  		$if_attr=true;
					}
				});
			  	if($if_attr){
					$(this).fadeIn('slow');
			  	}else{
					$(this).hide();
			  	//console.log($(this));
				}
			});
		  }
		});
	    Layout.init();
        Layout.initOWL();
        LayersliderInit.initLayerSlider();
        //Layout.initImageZoom();
        Layout.initTouchspin();
        Layout.initFixHeaderWithPreHeader();
        Layout.initNavScrolling();
        Layout.initSliderRange();

    $( "#slider-range" ).slider({
        range: true,
        min: 0,
        max: 500,
        values: [ <?php echo $min_price;?>, <?php echo $max_price;?> ],
        slide: function( event, ui ) {
            $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
        },
        change: function(event, ui) {
            // when the user change the slider
        },
        stop: function(event, ui) {
	    	//console.log(ui.values[0]);
	    	//console.log(ui.values[1]);
				$('.filterpro').each(function(){
				  	var money = $(this).attr('money');
				  	//var money = $(this).attr('data-tag');
				  	//console.log(money);
			  		$if_attr=false;
				  	//console.log(category_list);
				  	if(money>=ui.values[0] && money<=ui.values[1]){
				  		//$if_attr=true;
						$(this).fadeIn('slow');
				  	}else{
						$(this).hide();
				  	//console.log($(this));
					}
				});
            // when the user stopped changing the slider
            //$.POST("to.php",{first_value:ui.values[0], second_value:ui.values[1]},function(data){},'json');
        }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
                        " - $" + $( "#slider-range" ).slider( "values", 1 ) );
				$('.shopadd').click(function(e) {
					//alert();
					var product_sn=$(this).attr('product_sn');
					if(product_sn){
					 var url = "shop/add";
					    $.ajax({
					           type: "POST",
					           url: url,
								dataType:'json',
					           data: { product_sn:product_sn,category_sn:category_sn,qty:'1' },
					           success: function(data)
					           {
					           	if (data.error){
					           		alert(data.error);
					           	}else{
					           		$('.top-cart-info-count').text(data.total_items);
					           		$('.top-cart-info-value').text('$ '+data.total);
					           		//更新購物車popup
					           		$('#top-cart-content').load('shop/top_cart');
					           		//console.log(data.total);
					           		location.href='/shop/shopView';
					           	}
					           }
					         });
					  }
					    e.preventDefault();
				});
				$('.addcart').click(function(e) {
					var product_sn=$(this).attr('product_sn');
					var product_spec=$(this).attr('product_spec');
					if(product_sn){
					 var url = "shop/add";
					    $.ajax({
					           type: "POST",
					           url: url,
							   dataType:'json',
					           data: { product_sn:product_sn,category_sn:category_sn,qty:'1',product_spec:product_spec },
					           success: function(data)
					           {
					           	if (data.error){
					           		alert(data.error);
					           	}else{
					           		$('.top-cart-info-count').text(data.total_items);
					           		$('.top-cart-info-value').text('$ '+data.total);
					           		//更新購物車popup
					           		$('.top-cart-content').load('shop/top_cart');
					           	}
					           }
					         });
					  }
					    e.preventDefault();
					});
			$('li.dropdown-link').on('click', function() {
			    var $el = $(this);
			    if ($el.hasClass('dropdown-link')) {
					           		//console.log('xx');
			        var $a = $el.children('a.dropdown-link');
			        if ($a.length && $a.attr('href')) {
					           		//console.log('aa');
			            location.href = $a.attr('href');
			        }
			    }
			});
				$('.fa-heart').click(function(e) {
					var product_sn=$(this).attr('product_sn');
					if(product_sn){
 						$.get('member/AddwishList/'+product_sn+'/1',function(data){
	                $('.heart'+product_sn).fadeIn("slow").html(data);
	                if(data!='<a href="member/memberSignIn">請先點我登入會員</a>'){
										setTimeout(function(){$('.heart'+product_sn).hide();$('.heart'+product_sn).find("#text").text("");}, 10000);
									}
					          //console.log(product_sn);
					          //console.log($('.heart'+product_sn));
 						 });
					}
				});
    });
$(document).ready(function(e) {
var $ofweidht=innerWidth;   //取得螢幕寬度	
if($ofweidht<=768){     //設定判斷條件
$('#shopcetlog #icon-filter').addClass('collapsed');
$('#goods-s,#side_menu3x,#side_menu_blog').removeClass('in');   
}
});
$(window).resize(function(e) {
var $ofweidht=innerWidth;   //取得螢幕寬度	
if($ofweidht<=768){     //設定判斷條件
$('#shopcetlog #icon-filter').addClass('collapsed');
$('#goods-s,#side_menu3x,#side_menu_blog').removeClass('in');
} 
if($ofweidht>=769){     //設定判斷條件
$('#shopcetlog #icon-filter').removeClass('collapsed');
$('#goods-s,#side_menu3x,#side_menu_blog').addClass('in');
}    
});

</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->