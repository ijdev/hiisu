<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>會員管理 <small>囍市錢包管理</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/admin">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        會員管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
         <a href="admin/member/gift_cash">囍市錢包管理</a>
         <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         囍市錢包管理編輯
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">囍市錢包管理編輯</span>
            </div>
            <div class="actions btn-set">
              <a href="admin/member/gift_cash" class="btn green"><i class="fa fa-list"></i> 回列表</a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post">
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">會員編號</label>
                <div class="col-md-10">
                  <p class="form-control-static"><?php echo $Item['associated_member_sn'];?></p>
                </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">會員名稱</label>
                <div class="col-md-10">
                  <p class="form-control-static"><?php echo $Item['partner_name'];?></p>
                </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">類別</label>
                <div class="col-md-10">
                <?if (array_key_exists(@$Item['cash_type'], $edit_cash_type)){?>
                  <select name="Item[cash_type]" required class="table-group-action-input form-control input-inline input-sm">
                    <option value="">類別</option>
                     <?php foreach($cash_types as $_key=>$_value){
                          echo '<option value="'.$_key.'"';
                          if(@$Item['cash_type']==$_key)
                          echo ' selected="selected"';
                          echo '>'.$_value.'</option>';
                       }?>
                  </select>
                <?}else{
                  echo '<p class="form-control-static">'.$cash_types[@$Item['cash_type']].'</p>';?>
                    <input type="hidden" name="Item[cash_type]" value="<?php echo @$Item['cash_type']; ?>">
                <?}?>
                </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">當前累積可用囍市錢包</label>
                <div class="col-md-10">
                  <p class="form-control-static number-redb" id="current_amount"><?php echo number_format($cash_money,2);?></p>
                </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-md-2 control-label">金額*</label>
                <div class="col-md-2">
                  <input type="number" step="1" class="form-control" name="Item[cash_amount]" value="<?php echo @$Item['cash_amount'];?>">
                </div>
                  <label class="col-lg-3 help-block" style="display:inline;">&nbsp;發放＋，扣除－</label>
              </div>
              <div class="form-group">
                <label for="password" class="col-md-2 control-label">內容備註</label>
                <div class="col-md-10">
                  <textarea class="form-control" rows="3" name="Item[memo]"><?php echo @$Item['memo'];?></textarea>
                </div>
              </div>
                <div class="form-group">
                  <label for="updater" class="col-md-2 control-label">更新者</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['update_member_name'])?@$Item['update_member_name']:''; ?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-md-2 control-label">更新時間</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control" placeholder="<?php echo (@$Item['last_time_update']!='0000-00-00 00:00:00')?@$Item['last_time_update']:@$Item['create_date']; ?>" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <input type="hidden" name="gift_cash_sn" value="<?php echo @$Item['gift_cash_sn']; ?>">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="reset" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->