<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 訂單交易完成 (成功 / 失敗)
*/
?>
<div class="container">

<div id="css">
<style type="text/css">
  .order-table{
    border: 0px;
    width: 100%;
    border-spacing: 2px;
  }
    .order-table tr{
    }
      .order-table tr th{
        border-bottom: 2px solid #EFEFEF;
        padding:9px 0 10px 0;
        font-weight: bold;
      }
      .order-table tr td{
        border-bottom: 1px solid #EFEFEF;
        padding:9px 0 10px 0;
      }
</style>
<?php if($order['sub_order']){ 
	foreach($order['sub_order'] as $key=> $Item){
		if($Item['delivery_status']=='8'){
		}
	}
}
?>
</div>
<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<!-- Page level plugin styles END -->

<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="shop">線上商店</a></li>
    <li class="active">付款完成</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-25">
  <!-- BEGIN CONTENT -->
  <div class="col-md-12 col-sm-12">
    <div class="product-page" style="display:<?php echo ($order['payment_status']=='2')?'block':'none';?>">
      <!-- 交易成功 -->
      <div class="row">
        <div class="col-md-3 col-sm-3">
          <img src="public/img/tick_green.png" class="img-responsive">
        </div>
        <div class="col-md-9 col-sm-9">
          <h2>付款成功</h2>
          <p>
            感謝您購買愛結婚商品,您已完成此筆交易的訂購程序<br>
            <?php if($order['sub_order'][0]['delivery_status']=='8'){
          	 echo '您訂購的虛擬商品已經成功啟用，如有任何相關問題歡迎聯繫客服哦。';
          	}else{
          	 echo '我們將儘快為您處理訂單並安排出貨';
          	}?>
          </p>
          <div class="text-left padding-top-20">
            <button class="btn btn-primary" onclick="location.href='member/memberOrderList';" type="button">查訂單</button> &nbsp; 
           <?php if($order['sub_order'][0]['product_type']=='2'){ //虛擬商品 ?>
          	 <button class="btn btn-primary" onclick="location.href='member/my/ewedding';" type="button">婚禮網站</button> &nbsp; 
           <?php }?>
            <button class="btn btn-primary" onclick="location.href='shop';" type="button">繼續購物</button> &nbsp; 
             <br><br>
          </div>
        </div>
      </div>
      <!-- /交易成功 -->
    <br><br><br>
    </div>
    <div class="product-page" style="display:<?php echo ($order['payment_status']=='1')?'block':'none';?>">
      <!-- 交易成功 -->
      <div class="row">
        <div class="col-md-3 col-sm-3">
          <img src="public/img/close_red.png" class="img-responsive">
        </div>
        <div class="col-md-9 col-sm-9">
          <h2>付款尚未完成</h2>
          <p>
            請完成付款，我們才會為您處理訂單並安排出貨喔
          </p>
          <div class="text-left padding-top-20">
            <button class="btn btn-primary" onclick="" type="button">前往付款</button> &nbsp; 
            <button class="btn btn-primary" onclick="location.href='shop';" type="button">繼續購物</button> &nbsp; 
             <br><br>
          </div>
        </div>
      </div>
      <!-- /交易成功 -->
    <br><br><br>
    </div>
    <div class="product-page padding-top-50">
      <h1>
        <i class="fa fa-list-alt"></i> 訂單明細
        <!--div class="actions pull-right">
          <a href="preview/memberOrderList" class="btn btn-sm">
            <i class="fa fa-list-alt"></i> 回訂單列表
          </a>
        </div-->
      </h1>
      <div class="alert alert-success">
        <div class="row">
          <div class="col-md-5 col-sm-5">
            <strong>訂單日期 : </strong><?php echo $order['order_create_date'];?>
          </div>
          <div class="col-md-5 col-sm-5">
           <strong>交易編號 : </strong>#<?php echo $order['order_sn'];?>
          </div>
          <div class="col-md-2 col-sm-2" style="text-align:right;"> 
            <button type="button" class="print btn btn-default btn-sm">列印</button>
          </div>
        </div>
      </div>
      <div class="row" id="order-detail">
        <div class="col-md-12 col-sm-12">
          <!-- 付款狀態 -->
          <div class="portlet box blue-hoki">
            <div class="portlet-title">
              <div class="caption">
                <i class="fa fa-search"></i>付款狀態
              </div>
            </div>
            <div class="portlet-body">
              <table class="order-table">
                <tr>
                  <td class="col-md-3">
                    <strong>付款方式</strong>
                  </td>
                  <td class="col-md-9">
                    <?php echo $order['payment_method_name'];?>
                  </td>
                </tr>
                <tr>
                  <td class="col-md-6">
                    <strong>付款狀態</strong>
                  </td>
                  <td class="col-md-6">
                    <?php if($order['payment_done_date']=='2' && $order['payment_done_date']!='0000-00-00 00:00:00'){
                    	 echo '已付款'.' ('.$order['payment_done_date'].')';
                    	}else{
                    	 echo $order['payment_status_name'];
                    	}?>
                  </td>
                </tr>
                <tr>
                  <td class="col-md-3">
                    <strong>應付金額</strong>
                  </td>
                  <td class="col-md-9">
                    <?php echo number_format($order['total_order_amount']);?>
                  </td>
                </tr>                
              </table>
            </div>
          </div>
          <!-- 付款狀態 -->

<?php if($order['sub_order']){ foreach($order['sub_order'] as $key=> $Item){ 
		if($Item['channel_name']=='婚禮網站'){?>
                <div class="row">
                  <div class="col-md-12 col-sm-12">
                    <div class="portlet red-sunglo box">
                      <div class="portlet-title">
                        <div class="caption">
                          <i class="fa fa-cogs"></i>購物明細 - <?php echo $Item['channel_name']?> &nbsp; 訂單編號： <?php echo $Item['sub_order_sn']?>
                        </div>
                        <div class="actions">
                          <!--
                          <a href="javascript:;" class="btn btn-default btn-sm" data-toggle="modal" data-target="#pproductModal">
                            <i class="fa fa-plus"></i> 新增 
                          </a>
                          -->
                        </div>
                      </div>
                      <div class="portlet-body">
                        <div class="table-responsive">
                          <table class="table table-hover table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>商品名稱</th>
                                <th>規格</th>
                                <th>單價</th>
                                <th>數量</th>
                                <th>加購</th>
                                <th>優惠</th>
                                <th>電子禮金</th>
                                <th>小計</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php if($Item['order_item']){ foreach($Item['order_item'] as $order_item){?>
                                <tr>
							                    <td><a href="/shop/shopItem/<?php echo $order_item['product_sn'];?>" target="_blank"><?php echo $order_item['product_name'];?></a></td>
							                    <td><?php echo $order_item['product_package_name'];?><?php echo ($order_item['upgrade_discount_amount'])? '(升級)':'';?></td>
							                    <td>$<?php echo $order_item['sales_price'];?></td>
							                    <td><?php echo $order_item['buy_amount'];?></td>
							                    <td><?php echo $order_item['addon_flag']==1? '<i class="fa fa-check-square-o"></i>':'';?></td>
							                    <td><?php echo $order_item['coupon_code_cdiscount_amount'];?></td>
							                    <td><?php echo $order_item['gift_cash_discount_amount'];?></td>
							                    <td><?php echo number_format($order_item['actual_sales_amount']);?></td>
                                </tr>
                              <?php }}?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <!-- End of portlet-body -->
                    </div>
                    <!-- End of portlet -->
                  </div>
                </div>
<?php }}}?>
          <!-- 購物明細 -->
<?php if($order['sub_order']){ foreach($order['sub_order'] as $key=> $Item){ 
		if($Item['channel_name']=='結婚商店'){?>
          <div class="portlet box blue-hoki">
            <div class="portlet-title">
              <div class="caption">
                <i class="fa fa-search"></i>購物明細 - <?php echo $Item['channel_name']?> &nbsp; 訂單編號： <?php echo $Item['sub_order_sn']?>
              </div>
            </div>
            <div class="portlet-body">
              <table class="order-table">
                <tr>
                  <th>商品名稱</th>
                  <th>規格</th>
                  <th>單價</th>
                  <th>數量</th>
                  <th>優惠</th>
                  <th>電子禮金</th>
                  <th>小計</th>
                </tr>
                <?php if($Item['order_item']){ 
                			foreach($Item['order_item'] as $key2=> $order_item){?>
                  <tr>
                    <td><?php echo $order_item['product_name'];?></td>
                    <td><?php echo $order_item['product_package_name'];?></td>
                    <td>$<?php echo $order_item['sales_price'];?></td>
                    <td><?php echo $order_item['buy_amount'];?></td>
                    <td><?php echo $order_item['coupon_code_cdiscount_amount'];?></td>
                    <td><?php echo $order_item['gift_cash_discount_amount'];?></td>
							      <td><?php echo number_format($order_item['actual_sales_amount']);?></td>
                  </tr>
                <?php }}?>
              </table>
            </div>
          </div>
<?php }}}?>
          <!-- 購物明細 -->
          <!-- 配送 -->
          <div class="portlet box blue-hoki">
            <div class="portlet-title">
              <div class="caption">
                <i class="fa fa-search"></i>配送
              </div>
            </div>
            <div class="portlet-body">
              <table class="order-table">
                <tr>
                  <td>
                    <strong>配送方式</strong>
                  </td>
                  <td><?php echo $order['sub_order'][0]['delivery_method_name'];?></td>
                </tr>
                <tr>
                  <td>
                    <strong>出貨狀態</strong>
                  </td>
                  <td><?php echo $order['sub_order'][0]['delivery_status_name'];?></td>
                </tr>
              </table>
            </div>
          </div>
          <!-- 配送 -->
          <div class="portlet box blue-hoki">
            <div class="portlet-title">
              <div class="caption">
                <i class="fa fa-search"></i>收件人資料
              </div>
            </div>
            <div class="portlet-body">
              <table class="order-table">
                <tr>
                  <td  class="col-md-6"><strong>姓名</strong></td>
                  <td  class="col-md-6"><?php echo $order['receiver_last_name'];?></td>
                </tr>
                <tr>
                  <td><strong>聯絡電話</strong></td>
                  <td><?php echo $order['receiver_cell'];?> &nbsp; <?php echo $order['receiver_tel_area_code'];?>-<?php echo $order['receiver_tel'];?><?php echo ($order['receiver_tel_ext'])? '#'.$order['receiver_tel_ext']:'';?></td>
                </tr>
                <tr>
                  <td><strong>地址</strong></td>
                  <td><?php echo $order['receiver_zipcode'];?> <?php echo $order['receiver_addr_city'];?><?php echo $order['receiver_addr_town'];?><?php echo $order['receiver_addr1'];?></td>
                </tr>
              </table>
            </div>
          </div>
          <!-- 配送 -->
        </div>
      </div>
      <!-- /#order-detail -->
    </div>
  </div>
  <!-- END CONTENT -->
</div>