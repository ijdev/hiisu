<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard js
*/
?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="public/metronic/global/plugins/flot/jquery.flot.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/flot/jquery.flot.categories.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="public/metronic/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script>
        var data1 = <?php echo $chart_array1 ?>;
        var data2 = <?php echo $chart_array2 ?>;
</script>
<script src="public/metronic/admin/pages/scripts/ecommerce-index.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {    
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Demo.init(); // init demo features
        EcommerceIndex.init();
		$('.reload').click(function(e) {
			$('#overview_area').load("admin/main #overview_area");
    	});
    });
</script>
<!-- END JAVASCRIPTS -->
