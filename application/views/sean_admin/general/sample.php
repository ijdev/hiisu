<?php


//control sample

/**
	 * 婚宴地點管理 Template
	 *
	 */
	public function location($type="",$_id="")
	{
		/* Begin 修改 */
		//載入 model   
		$_model_name="ij_biz_domain";
		$_ct_name="婚宴地點管理";// 功能名稱
		$_ct_name_item="婚宴地點管理";// 功能子名稱
		$_url="domain/";//列表功能url
		$_url_item="domain/item/";//處理動作 url
		//table list where 
		$_table_where=" ";
		$_table="ij_wedding_location";//table name
		$_table_key="wedding_location_sn";//table key 
		$_page_content="page_system/location/table";
		$_page_level_js="page_system/location/table_js";
		$_page_content_item="page_system/location/item";
		$_page_level_js_item="page_system/location/item_js";
		/* END 修改 */

	

		switch($type){
			case "add" :
			case "item" :
			case "edit" :
			
				// 頁面主內容 view 位置, 必要
				$this->page_data['page_content']['view_path'] = $this->page_data['init_control'].$_page_content_item; 
				// 頁面主內容的 JS view 位置, 必要
				$this->page_data['page_level_js']['view_path'] = $this->page_data['init_control'].$_page_level_js_item; 
				$this->load->view('admin-ijwedding/general/main', $this->page_data);

			break;
			
			default:
				// 返回連結
		    $this->page_data['_father_link']=$_url;
		    $this->page_data['_action_link']=$this->page_data['init_control'].$_url."add";
				$_result01=$this->Sean_db_tools->db_get_max_record("*",$_table,$_table_where);
				$this->page_data['_result01']=$_result01;
			
				// 頁面主內容 view 位置, 必要
				$this->page_data['page_content']['view_path'] = $this->page_data['init_control'].$_page_content; 
				// 頁面主內容的 JS view 位置, 必要
				$this->page_data['page_level_js']['view_path'] = $this->page_data['init_control'].$_page_level_js; 
				$this->load->view('admin-ijwedding/general/main', $this->page_data);
			break;
		}
	}







?>


...............................................................................................................

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- BEGIN FOOTER -->
<?php $this->load->view($init_control.'general/_item_header.php');?>
<!-- END FOOTER -->      	
      	
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-map-marker font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase"><?php echo $_ct_name_item;?></span>
            </div>
            <div class="tools">
              <a href="admin-ijwedding/location"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
          	
          	
          	
            <form class="form-horizontal" role="form" action="<?php echo $_action_link;?>">
              <div class="form-body">
                
                
                <?php

									$_form_one= new sean_form_general();
									$em_columns = array(										
										  'wedding_location_name'=>array('header'=>"地點名稱",'type'=>'textbox', 'required'=>'required', 'required_message'=>"",'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'maxlength'=>'', 'default'=>'', 'source'=>'', 'rows'=>'15', 'cols'=>'50'),  	
										  'addr1'=>array('header'=>"住址",'type'=>'address', 'required'=>'required', 'required_message'=>"",'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'maxlength'=>'', 'default'=>'', 'source'=>'', 'rows'=>'15', 'cols'=>'50'),  	
										  'status'=>array('header'=>"是否啟用",'type'=>'status', 'required'=>'required', 'required_message'=>"",'form_control'=>'', 'placeholder'=> '' , 'post_addition'=>'','pre_addition'=>'', 'maxlength'=>'', 'default'=>'', 'source'=>'', 'rows'=>'15', 'cols'=>'50'),  	
									);
								 	$_form_one->add_item($em_columns);
									
							?>
               
               
                
                <div class="form-group">
                  <label for="updater" class="col-lg-3 control-label">更新者</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="<?php echo $_member_data["first_name"].$_member_data["last_name"];?>" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-lg-3 control-label">更新時間</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control"  placeholder="<?php echo date("Y-m-d H:i:s");?>" Disabled>
                  </div>
                </div>
              </div>
              
              
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="button" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
            
            
            
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
        
<!-- BEGIN FOOTER -->
<?php $this->load->view($init_control.'general/_item_footer.php');?>
<!-- END FOOTER -->