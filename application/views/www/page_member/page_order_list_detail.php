<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容
*/
//var_dump($order);
?>

<div class="container mob_padding">

<!-- Page level plugin styles START -->
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<style type="text/css">
  .select2_dealer{
    padding: 0px;
  }
#table_member th,td
{
    text-align:center;
    vertical-align:middle;
}
</style>
<div id="css" type="text/css">
<style type="text/css">
  .order-table{
    width: 100%;
    border-spacing: 2px;
  }
    .order-table tr{
    }
      .order-table tr th{
        border-bottom: 2px solid #EFEFEF;
        padding:9px 0 10px 0;
        font-weight: bold;
      }
      .order-table tr td{
        border-bottom: 1px solid #EFEFEF;
        padding:9px 0 10px 0;
      }
    .ldetailtop ul li {
    	margin-right: 7%;
    }
    .pink{
    	background: deeppink;
    }
</style>
</div>
<!-- Page level plugin styles END -->
<div class="container mob_padding">
<ul class="breadcrumb">
    <li><a href="/">首頁</a></li>
    <li><a href="/member">會員中心</a></li>
    <li class="active">訂單查詢</li>
</ul>

<!-- BEGIN SIDEBAR & CONTENT -->
<div class="row margin-bottom-40">
  <!-- BEGIN SIDEBAR -->
  <div class="sidebar col-md-2 col-sm-3">
    <div class="desktop_view_sidebar">
		<ul class="list-group margin-bottom-25 sidebar-menu">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
	<div class="mobile_view_sidebar">
		<div class="side-nav-head border_title_bottom">
			<button class="fa fa-bars collapsed" data-toggle="collapse" data-target="#mob_sidebar_drop_down"></button>
			<h4 class="bfont">會員中心<!--會員中心--></h4>
		</div>
		<ul class="list-group margin-bottom-25 sidebar-menu collapse" id="mob_sidebar_drop_down">
		  <?php $this->load->view(($this->uri->segment(1)? 'www/':'').'page_member/sidebar');?>
		</ul>
	</div>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->


  <div id="order-detail" class="col-md-10 col-sm-10 margin-bottom-30 product_right border_left minheight">
                              <!--訂單列表開始 -->
			<h3 class="col-md-12 pull-left padding0">訂單明細</h3>
						<div class="col-md-12 pull-left padding0 ldetailtop">
						<ul class="col-md-12 pull-left padding0 ">
						<li>交易編號：<?php echo $order['order_num'];?></li>
						<li>訂單編號：<?php echo $order['sub_order'][0]['sub_order_num'];?></li>
						<li>訂單日期：<?php echo $order['order_create_date'];?></li>
						<?/*<li class="pull-right"> <input type="button" class="pgord_topbtn print" value="列印"/></li>*/?>
						<li class="pull-right" style="cursor: pointer;" onclick="history.go(-1);">&nbsp;&nbsp;<i class="fa fa-list"></i></li>
						</ul>
						</div>

<div id="onetabcont" class="orderlistdetail">
				<div class="col-md-12 padding0">
				 <table class="col-md-12 padding0 ldtabbox1 order-table">
					<thead>
						   <tr>
						         <th>商品編號</th>
						         <th>商品名稱</th>
						         <th>賣家</th>
						         <th>規格 </th>
						  		 <th>單價 </th>
								 <th>數量</th>
						         <th>小計</th>
						  </tr>
					 </thead>
					 <tbody>
				<?php $actual_sales_amount=0;$shipping_charge_amount=0;$return_amount=0;
					if($order['sub_order']){ foreach($order['sub_order'] as $key=> $Item2){ if($Item2['sub_order_sn']==$sub_order_sn){
              			$shipping_charge_amount+=$Item2['shipping_charge_amount'];
						if($Item2['order_item']){ foreach($Item2['order_item'] as $_key2=>$Item){ ?>
					 <tr>
						 <td class="mob_title2" data-title="商品編號"><a href="/shop/shopItem/<?php echo $Item['product_sn'];?>" target="_blank"><?php echo $Item['product_sn'];?></a></td>
						 <td class="mob_title2" data-title="商品名稱"><a href="/shop/shopItem/<?php echo $Item['product_sn'];?>" target="_blank"><?php echo $Item['product_name'];?></a>
						<?php echo $Item['addon_flag']==1? '<label class="label label-xs pink">加</label>':'';?>
						<?=($Item['order_cancel_sn'])?'<label class="label label-xs label-info">退</label>':''?>
						 </td>
						 <td class="mob_title2" data-title="賣家"><?php echo (@$Item2['brand'])?$Item2['brand']:'';?></td>
						 <td class="mob_title2" data-title="規格"><?php echo ($Item['mutispec_color_name'])?$Item['mutispec_color_name']:'單一規格';?></td>
						 <td class="mob_title2" data-title="單價"><?php echo number_format($Item['sales_price']);?></td>
						 <td class="mob_title2" data-title="數量"><?php echo $Item['buy_amount'];?></td>
						 <td class="mob_title2" data-title="小計"><?php echo number_format($Item['actual_sales_amount']);?></td>
					 </tr>
<?php $actual_sales_amount+=$Item['actual_sales_amount'];
	if(@$Item['return_amount']>0){
		$return_amount+=(int)$Item['return_amount'];
		//var_dump($return_amount);
	}
 }}}}
								}?>
					 </tbody>
				 </table>

				</div>

				<div class="col-md-12 pull-left padding0">
				<p></p>

			<div class="col-md-12 padding0 onetabback">
			  <ul>
				<li>
				<span class="pg_listhd">訂單總計</span></li>
				<li><p class="pgordlist">商品總金額 </p>
				<p class="pgordlist"><?php echo number_format($actual_sales_amount);?> 元 </p>
				</li>
				<li><p class="pgordlist">配送費用 </p>
				<p class="pgordlist"><?php echo number_format($shipping_charge_amount);?> 元</p>
				</li>
				<!--li><p class="pgordlist">使用紅利點數/折價券折扣金額</p>
				<p class="pgordlist">-0元  </p>
				</li-->
				<li><p class="pgordlist">消費總金額</p>
				<p class="pgordlist"><?php echo number_format($shipping_charge_amount+$actual_sales_amount);?> 元  </p>
				</li>
<?php if(($order['sub_order'][0]['sub_order_status']=='2' || $order['sub_order'][0]['sub_order_status']=='3' || $order['sub_order'][0]['sub_order_status']=='8') && $return_amount>0){?>
				<li><p class="pgordlist">退貨總金額</p>
				<p class="pgordlist">
					<?php if($order['sub_order'][0]['delivery_status']=='8'){
						echo number_format($shipping_charge_amount+$return_amount);
					}else{
						echo number_format($return_amount);
					}
					?>
				 元  </p>
				</li>
<?php }?>
			  </ul>
			</div>

			<div class="col-md-12 padding0 onetabback">
			  <ul>
				<li>
				<span class="pg_listhd">訂單資訊</span></li>
				<li><p class="pgordlist">訂單狀態 </p>
				<p class="pgordlist"><?php echo $order['sub_order'][0]['sub_order_status_name'];?>  </p>
				</li>
				<li><p class="pgordlist">付款方式 </p>
				<p class="pgordlist"><?php echo $order['payment_method_name'];?></p>
				</li>
				<li><p class="pgordlist">付款狀態</p>
				<p class="pgordlist">
					<?php if($order['payment_done_date']=='2' && $order['payment_done_date']!='0000-00-00 00:00:00'){
                	 echo '已付款'.' ('.$order['payment_done_date'].')';
                	}else{
                	 echo $order['sub_order'][0]['payment_status_name'];
                	}?> </p>
				</li>
				<li><p class="pgordlist">配送方式 </p>
				<p class="pgordlist"><?php echo $order['sub_order'][0]['delivery_method_name'];?>  </p>
				</li>
				<li><p class="pgordlist">配送狀態 </p>
				<p class="pgordlist"><?php echo $order['sub_order'][0]['delivery_status_name'];?>  </p>
				</li>

			  </ul>
			</div>


			<!--div class="col-md-12 padding0 onetabback">
			  <ul>
				<li>
				<span class="pg_listhd">折扣資訊</span></li>

				<li><p class="pgordlist">使用折價券 </p>
				<p class="pgordlist">100</p>
				</li>
				<li><p class="pgordlist">紅利折抵點數點</p>
				<p class="pgordlist">20點</p>
				</li>
				<li><p class="pgordlist">紅利兌換點數點</p>
				<p class="pgordlist">10點  </p>
				</li>
			  </ul>
			</div-->


			<div class="col-md-12 padding0 onetabback">
			  <ul>
				<li>
				<span class="pg_listhd">發票資訊</span></li>

				<li><p class="pgordlist">發票格式</p>
				<p class="pgordlist"><?php echo ($order['company_tax_id'] && $order['invoice_title'])?'三':'二'?>聯式</p>
				</li>
				<li><p class="pgordlist">發票地址</p>
				<p class="pgordlist">
			  <?php if($order['invoice_receiver_zipcode']) echo $order['invoice_receiver_zipcode']; else echo $order['buyer_zipcode'];?>
              <?php if($order['invoice_receiver_addr_city']) echo $order['invoice_receiver_addr_city']; else echo $order['buyer_addr_city'];?>
              <?php if($order['invoice_receiver_addr_town']) echo $order['invoice_receiver_addr_town']; else echo $order['buyer_addr_town']?>
              <?php if($order['invoice_receiver_addr1']) echo $order['invoice_receiver_addr1']; else echo $order['buyer_addr1'];?>
				</p>
				</li>
				<li><p class="pgordlist">發票號碼</p>
				<p class="pgordlist"><?php echo ($order['invoice_num'])?$order['invoice_num']:'尚未開立'?> </p>
				</li>
<?php if($order['invoice_num']){?>
				<li><p class="pgordlist">開立日期</p>
				<p class="pgordlist"><?php echo substr($order['invoice_time'],0,10)?> </p>
				</li>
<?php }?>
<?php if($order['invoice_title'] && $order['company_tax_id']){?>
				<li><p class="pgordlist">發票抬頭</p>
				<p class="pgordlist"><?php echo $order['invoice_title']?></p>
				</li>
				<li><p class="pgordlist">統一編號</p>
				<p class="pgordlist"><?php echo $order['company_tax_id']?> </p>
				</li>
<?php }?>
			  </ul>
			</div>


				<div class="col-md-12 padding0 onetabback">
			  <ul>
				<li>
				<span class="pg_listhd">收件人資料</span></li>

				<li><p class="pgordlist">姓名</p>
				<p class="pgordlist"><?php echo $this->ijw->mask_words($order['receiver_last_name']);?></p>
				</li>
				<li><p class="pgordlist">聯絡電話</p>
				<p class="pgordlist"><?php echo $order['receiver_cell'];?> &nbsp; <?php echo $order['receiver_tel'];?></p>
				</li>
				<li><p class="pgordlist">收貨地址</p>
				<p class="pgordlist">
				<?php echo $order['receiver_zipcode'];?> <?php echo $order['receiver_addr_city'];?><?php echo $order['receiver_addr_town'];?>
				<?php echo $order['receiver_addr1'];?>
				</p>
				</li>
				<!--li><p class="pgordlist">配送日期</p>
				<p class="pgordlist">2016/11/24 </p>
				</li>
				<li><p class="pgordlist">宅配時間</p>
				<p class="pgordlist">早上09:00~12:00 </p>
				</li-->
				<li><p class="pgordlist">訂單備註</p>
				<p class="pgordlist">
<?php if(@$order['sub_order'][0]['memo']){?>
<pre><?php echo @$order['sub_order'][0]['memo']?></pre>
<?}?>
				</p>
				</li>
			  </ul>
			</div>



 </div>




							<!-- /訂單列表結束 -->

							</div>
  <!-- copy data -->
  <!-- END CONTENT -->
</div>
<!-- END SIDEBAR & CONTENT -->

</div> <!-- container -->
</div>
