<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<style>
  .select2_dealer,.select3_dealer{height:28px;}
</style>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>系統管理員 <small>列表與搜尋</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container" style="width: 1200px;">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">系統管理員</span>
            </div>
            <div class="actions btn-set">
              <a href="<?php echo $_action_link;?>" class="btn green"><i class="fa fa-plus"></i> 新增</a>
            </div>
          </div>

            <!-- End of search_filter -->
            <table class="table table-striped table-bordered table-hover" name="table_member" id="table_member">
              <thead>
                <tr>
                  <th>姓名</th>
                  <th>帳號</th>
                  <th>Email</th>
                  <th>最後登入時間</th>
                  <th>登入次數</th>
                  <th>管理員分類</th>
                  <th>帳號狀態</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
               <?php
                $_system_status_ct=$this->config->item("system_status_ct");
								$_member_level_type_ct=$this->config->item("member_level_type_ct");
								//$_system_status_ct= array(
								//"1" => "未啟用",
								//"2" => "已啟用",
								//);
                if(is_array($_result01))
                	{
                 			foreach($_result01 as $key => $value)
                    		{
                  ?>
                  <tr class="odd gradeX">
                    <td><?php echo $value->last_name;?></td>
                    <td><?php echo $value->user_name;?></td>
                    <td><?php echo $value->email;?></td>
                    <td class="center"><?php echo $value->last_login_time;?></td>
                    <td class="center"><?php echo $value->login_count;?></td>
                    <td class="center"><?php echo $value->roles;?></td>
                    <td><?php echo $_system_status_ct[$value->member_status];?> </td>
                    <td class="center">
                      <a href="<?php echo $init_control;?>sys_member_item/edit/<?php echo $value->member_sn;?>"><i class="fa fa-edit"></i>編輯</a>&nbsp;&nbsp;

                    </td>
                  </tr>
                <?php } } ?>

              </tbody>
            </table>

          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->