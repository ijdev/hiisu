<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1><?php echo $_father_name;?> <small><?php echo $_ct_name_item;?></small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="<?php echo $_home_url;?>">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        <?php echo $_father_name;?>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="<?php echo $_father_link;?>"><?php echo $_ct_name;?></a>
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
         <?php echo $_ct_name_item;?>
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    
    
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">