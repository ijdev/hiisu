<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends MY_Controller {
	 	public function __construct()
		{
			 parent::__construct();

				$this->load->library('session');
				$this->load->helper('form');
				$this->load->helper('url');
				$this->load->library('form_validation');
				$this->load->library('sean_lib/Sean_form_general');
				$this->load->model('general/My_general_ct');
				$this->load->config('ij_config');
				$this->load->config('ij_sys_config');
				$this->load->model('sean_models/Sean_general');
				$this->load->model('sean_models/Sean_db_tools');
				$this->load->model('libraries_model');
				$this->load->helper('cookie');

				$this->page_data = array();
				$this->page_data['unreadmessage']= $this->libraries_model->get_unread_message_count();
				$this->page_data['init_control']="admin/main/";
				$this->page_data['_home_url']="";
				$this->init_control=$this->page_data['init_control'];
				$this->page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
				$this->page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
				$this->page_data['page_content']['view_path'] = 'sean_admin/page_home/page_content'; // 頁面主內容 view 位置, 必要
				$this->page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
				$this->page_data['page_level_js']['view_path'] = 'sean_admin/page_home/page_content_js'; // 頁面主內容的 JS view 位置, 必要
				$this->lang->load('general', 'zh-TW');
				$this->page_data["_per_page"]=2;
		}


	public function index()
	{
		$page_data = array();

		// 內容設定

	    $this->LoginCheck();
		// general view setting
		$page_data['general_header'] = array(); // header 可以透過這個參數帶入, 必要
		$page_data['page_content'] = array(); // 頁面主內容由這個參數帶入, 必要
		$page_data['page_content']['view_path'] = 'admin/page_home/page_content'; // 頁面主內容 view 位置, 必要
		$page_data['page_content']['total_amount']=$this->libraries_model->get_order_statics('sum(total_order_amount) as t')['t'];
		$page_data['page_content']['total_orders']=$this->libraries_model->get_order_statics('count(*) as t')['t'];
		$page_data['page_content']['average_amount']=($page_data['page_content']['total_orders']>0)?$page_data['page_content']['total_amount']/$page_data['page_content']['total_orders']:'0';
		$_where="date_format(order_create_date, '%Y%m') = date_format(curdate() , '%Y%m')"; //本月
		$page_data['page_content']['current_month_total_amount']=$this->libraries_model->get_order_statics('sum(total_order_amount) as t',$_where)['t'];;
		$page_data['page_content']['current_month_total_orders']=$this->libraries_model->get_order_statics('count(*) as t',$_where)['t'];
		$page_data['page_content']['current_month_average_amount']=($page_data['page_content']['current_month_total_orders']>0)?$page_data['page_content']['current_month_total_amount']/$page_data['page_content']['current_month_total_orders']:'0';

		$page_data['page_content']['hot_sales']=$this->libraries_model->get_order_item_statics($_where);
		$page_data['page_content']['click_sales']=$this->libraries_model->get_order_item_statics($_where,'click_number desc');

		$page_data['page_content']['sub_orders']=$this->libraries_model->get_orders($_where);
		$page_data['page_content']['members']=$this->libraries_model->get_order_members($_where);
	    $chart_array1=$this->libraries_model->get_order_statics_period('count(*)');
		$page_data['page_content']['chart_array1']=json_encode($chart_array1,JSON_HEX_AMP);
	    $chart_array2=$this->libraries_model->get_order_statics_period();
		$page_data['page_content']['chart_array2']=json_encode($chart_array2,JSON_HEX_AMP);

		$page_data['page_level_js'] = array(); // 頁面主內容的 JS 由這個參數帶入, 必要
		$page_data['page_level_js']['view_path'] = 'admin/page_home/page_content_js'; // 頁面主內容的 JS view 位置, 必要
		$this->load->view('admin/general/main', $page_data);
	}
}