<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mail extends My_general_ct {

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('sean_models/Sean_db_tools');


		/* Begin 修改  */
		$this->_ct_name="MAIL類型";// 功能名稱
		$this->_url="Sysconfig/Mail/";//列表功能url
		$this->_url_item="Sysconfig/Mail_item/";//處理動作 url
		$this->_table="ij_system_email";//table name
		$this->_id="system_email_sn";//table key
		$this->_table_field=array(
					 "system_email_sn" => "MAIL類型代碼",
					 "system_email_type" => "Mail類型",
					 "system_email_titile" => "Mail標題",
					 "system_email_content" => "內容",
					 "memo" => "備註",
					 "status" => "是否啟用",
					 "create_date" => "建立時間",
					 "create_member_sn" => "建立者",
					 "update_member_sn" => "更新者",
					 "last_time_update" => "更新時間",
					 "last_time_send" => "最後發送日期",
				);

				$this->_table_field2=array(
					 "system_email_type" => "Mail類型",
					 "system_email_titile" => "Mail標題",
					 "status" => "是否啟用",
					 "last_send_time" => "最後發送日期",
				);

		/* End 修改  */

    }

    //取得列表內容
    public function table_value_list( $_where,$_id = 0)
	{
		$_table_key=$this->_id;
	    $page_data['_body_result']=array();
		//判斷是否有帶入特殊查詢
		if(strlen($_where)==0)
		{
			$_where="  order by system_email_type asc";

		}
		// 取得列表
		$_result01=$this->Sean_db_tools->db_get_max_record("*",$this->_table,$_where);

		/* 列表標頭名稱 */
		$page_data['_table_thead_name']=$this->_table_field2;


		/* 類表內容 */
		if(is_array($_result01))
		{
                 		$_i=0;
                 		foreach($_result01 as $key => $value)
                    	{

			                    		if($value->status == 1)
			                    		{
			                    					$_default_status = '<i class="fa fa-check"></i>';
			                    		}else{
			                    					$_default_status = '<i class="fa fa-times"></i>';
			                    		}


			                    		$_body_result='';
						/* Begin 修改  */

	                  //$page_data['_body_result'][$_i][0]=array("_value"=>$value->mail_template_sn,"_class"=>"");
			              $page_data['_body_result'][$_i][1]=array("_value"=>$value->system_email_type,"_class"=>"");
										$page_data['_body_result'][$_i][2]=array("_value"=>$value->system_email_titile,"_class"=>"");
										//$page_data['_body_result'][$_i][3]=array("_value"=>$value->system_email_content,"_class"=>"");
										//$page_data['_body_result'][$_i][4]=array("_value"=>$value->sort_order,"_class"=>"");
										$page_data['_body_result'][$_i][5]=array("_value"=>$_default_status,"_class"=>"");
									 // $page_data['_body_result'][$_i][6]=array("_value"=>$value->create_date,"_class"=>"");
									  //$page_data['_body_result'][$_i][7]=array("_value"=>$value->create_member_sn,"_class"=>"");
									  //$page_data['_body_result'][$_i][8]=array("_value"=>$value->update_member_sn,"_class"=>"");
									  //$page_data['_body_result'][$_i][9]=array("_value"=>$value->last_time_update,"_class"=>"");
									  $page_data['_body_result'][$_i][10]=array("_value"=>$value->last_time_send,"_class"=>"");

						/* End 修改  */


										$page_data['_body_result'][$_i][11]=array("_value"=>'
			                    		 		<a href="'.$this->init_control.$this->_url_item.'edit/'.$value->$_table_key.'"><i class="fa fa-edit"></i>編輯</a>&nbsp;&nbsp;
						                      <a href="'.$this->init_control.$this->_url_item.'delete/'.$value->$_table_key.'" onclick="return deletechecked();"><i class="fa fa-trash-o"></i>刪除</a>&nbsp;&nbsp;
						                  	',"_class"=>'class="center"');
                				$_i++;


                			}

    				}
			return $page_data;

    }



}
