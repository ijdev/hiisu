<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容的 page level js
*/
?>

<script type="text/javascript" src="public/metronic/global/plugins/select2/select2.min.js"></script>
<script src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
<script src="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->
<script src="public/metronic/global/plugins/zoom/jquery.zoom.min.js" type="text/javascript"></script><!-- product zoom -->
<script src="public/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script><!-- Quantity -->
<script type="text/javascript" src="public/metronic/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script src="public/metronic/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/metronic/global/scripts/metronic.js" type="text/javascript"></script>
<script src="public/metronic/frontend/layout/scripts/layout.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init();
        Layout.initFixHeaderWithPreHeader();
        Layout.initNavScrolling();
        Layout.initUniform();
				/*$(".select2_dealer").select2({
				    tags: ["典華1", "典華2", "典華3", "典華4", "典華5"]
				});*/
	    });
</script>

<script>

  // When the browser is ready...
  $(function() {

    // Setup form validation on the #register-form element
    $("#register-form").validate({

        // Specify the validation rules
        rules: {
            old_pwd: {
                required: true,
                minlength: 6
            },
            password: {
                required: true,
                minlength: 6
            },
             confirm_password: {
                required: true,
                equalTo: "#password",
                minlength: 6
            },
        },

        // Specify the validation error messages
        messages: {
            old_pwd: {
                required: "請輸入原密碼",
                minlength: "密碼最小長度為6"
            },
            password: {
				required: "請輸入密碼",
                minlength: "密碼最小長度為6"
            },
            confirm_password: {
				required: "請重複輸入密碼",
                equalTo:"確認密碼與新密碼不符",
                minlength: "密碼最小長度為6"
            }
        },

        submitHandler: function(form) {
            form.submit();
        }


    });

  });

  </script>
<!-- END PAGE LEVEL JAVASCRIPTS -->