<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link href="public/metronic/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css"/>
<link href="public/metronic/admin/pages/css/inbox.css" rel="stylesheet" type="text/css"/>

<!-- BEGIN PAGE HEAD -->
<form method="post" id="form1">
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>全站通知</h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/main">總覽</a><i class="fa fa-circle"></i>
      </li>
      <li class="active">
         全站通知
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-bell-o font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">全站通知</span>
            </div>
            <div class="actions btn-set">
            </div>
          </div>
          <div class="portlet-body">
            <div class="row inbox">
              <div class="inbox-content">
                <table class="table table-striped table-advance table-hover">
                  <thead>
                    <tr>
                      <th colspan="3">
                        <input type="checkbox" id="select_all" class="mail-checkbox mail-group-checkbox">
                        <div class="btn-group">
                          <a class="btn btn-sm blue dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="false"> More <i class="fa fa-angle-down"></i></a>
                          <ul class="dropdown-menu">
                            <li>
                        			<a href="javascript:;" onclick="$('#flg').val('read');$('#form1').submit();">
                              <i class="fa fa-pencil"></i> 標示為已讀 </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                        			<a href="javascript:;" onclick="$('#flg').val('del');$('#form1').submit();">
                              <i class="fa fa-trash-o"></i> 刪除 </a>
                            </li>
                          </ul>
                        </div>
                      </th>
                      <th class="pagination-control" colspan="3" style="text-align:right;">
                	<?php echo $page_links;?>
                      </th>
                    </tr>
                  </thead>
			            <tbody>
              		<?php if($messages){ foreach($messages as $key=>$_Item){?>
			              <tr class="<?php echo (!$_Item['read_flag'])? 'un':'';?>read" data-messageid="<?php echo $key+1;?>">
			                <td class="inbox-small-cells">
			                  <input type="checkbox" class="mail-checkbox" name="member_message_center_sn[]" value="<?php echo $_Item['member_message_center_sn'];?>">
			                </td>
			                <td class="view-message hidden-xs">
			                   <?php switch($_Item['message_code']){
			                   		case 1:
			                   			echo '客服:'.$_Item['update_member_sn'];
			                   			break;
			                   		default:
			                   			echo '系統訊息';
			                   		}?>
			                </td>
			                <td class="view-message" >
			                	<?php if($_Item['message_link_url']){?>
				                  <!--<a href="<?php echo $_Item['message_code'];?>" target="_blank"-->
			                  		<a target="_blank" href="admin/CRM/notification/item/<?php echo $_Item['member_message_center_sn'];?>">
				                    <?php echo $_Item['message_content'];?>&nbsp;
				                    <i class="fa fa-external-link"></i>
				                  </a>
				                  <?php }else{?>
			                  		<a href="admin/CRM/notification/item/<?php echo $_Item['member_message_center_sn'];?>"><?php echo $_Item['message_subject'];?></a>
				                  <?php }?>
			                </td>
			                <td style="width:220px;" class="view-message text-right">
			                   <?php echo $_Item['transmit_date'];?>
			                </td>
			              </tr>
			              <?php }}?>
			            </tbody>
			            <input type='hidden' name='flg' id='flg' value=''>
                </table>
              </div>
              <!-- /.inbox-content -->
            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
			          </form>
