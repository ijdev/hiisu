<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Page 主架構
*/
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <meta charset="utf-8">
  <title>IJWedding</title>

  <base href="<?php echo $this->config->item('base_url'); ?>preview/www/dealer/" />
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <meta content="Meta description" name="description">
  <meta content="Meta keywords" name="keywords">
  <meta content="keenthemes" name="author">

  <meta property="og:site_name" content="-CUSTOMER VALUE-">
  <meta property="og:title" content="-CUSTOMER VALUE-">
  <meta property="og:description" content="-CUSTOMER VALUE-">
  <meta property="og:type" content="website">
  <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
  <meta property="og:url" content="-CUSTOMER VALUE-">

  <link rel="shortcut icon" href="favicon.ico">

  <!-- Fonts START -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
  <!-- Fonts END -->

  <!-- Global styles START -->          
  <link href="public/metronic/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="public/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
  <link href="public/metronic/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Global styles END --> 

  <!-- Theme styles START -->
  <link href="public/metronic/global/css/components.css" rel="stylesheet">
  <link href="public/metronic/frontend/layout/css/style.css" rel="stylesheet">  
  <link href="public/metronic/frontend/pages/css/style-shop.css" rel="stylesheet" type="text/css">
  <link href="public/metronic/frontend/layout/css/style-responsive.css" rel="stylesheet">
  <link href="public/metronic/frontend/pages/css/style-layer-slider.css" rel="stylesheet">
  <link href="public/metronic/frontend/layout/css/themes/red.css" rel="stylesheet" id="style-color">
  <link href="public/metronic/frontend/layout/css/custom.css" rel="stylesheet">
  <!-- Theme styles END -->


  <link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
  <link href="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
  <link href="public/metronic/global/plugins/slider-layer-slider/css/layerslider.css" rel="stylesheet">

  <style>
    .logo-dealer{
      width:92px;
      border-radius: 50%;
      webkit-border-radius: 100% !important;
      -moz-border-radius: 100% !important;
      border-radius: 100% !important;
      -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .3);
      position: absolute;
      bottom: -20px;
    }
    .ecommerce .header-navigation{
      padding-left:100px;
    }
    .ecommerce .header-navigation ul li a{
      color:#cebea2;
    }
    .ecommerce .header-navigation ul li a:hover{
      color:rgb(218, 178, 107);
    }
    .pre-footer{
      background-color: #cebea2;
      color: #FFF;
    }
    a{
      color:#cebea2 !important;
    }
    a:hover{
      color:rgb(218, 178, 107) !important;
    }
    .album-box{
      margin: 16px;
      -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .3);

    }
    .album-cover img{
      background-color: #FFF;
      border:3px solid #FFF;
      margin-top:3px;
    }
  </style>
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="ecommerce">
<!-- BEGIN HEADER -->
  <div class="header">
    <div class="container" style="position:relative;">
      <a class="site-logo" href="preview/ewedding">
        <img src="public/img/sample_dealer.jpg" class="logo-dealer">
      </a>
      <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

      <div class="top-cart-block">
        <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="16" style="margin: 0 5px;" placeholder="搜尋結婚商品">&nbsp;
        <a><i class="fa fa-search search-btn" style="font-size:20px;"></i></a>
      </div>
      <!-- BEGIN NAVIGATION -->
      <div class="header-navigation">
        <ul>
          <li><a href="preview/ewedding" data-anchor="section-service">結婚網站</a></li>
          <li><a href="preview/checkIn" data-anchor="section-price">現場報到系統</a></li>
        </ul>
      </div>
      <!-- END NAVIGATION -->
    </div>
    <!-- Header END -->
  </div>

    <div class="main">
      <div class="container">
        <div class="row margin-top-20" style="background-color:#FFF; padding-top:10px; padding-bottom:10px;">
          <!-- BEGIN TWO PRODUCTS -->
          <div class="col-md-5">
            <h2>LinLi Wedding 林莉婚紗</h2>
            <p>LinLi Wedding自1987年成立至今，創辦人林莉老師是專業造型師出身，帶領資深團隊多年來服務超過四萬對新人，是許多藝人及名流選擇婚紗的首選。<br>
台北市中山北路二段155號<br>
電話：886.2.2531.5565 </p>
          </div>
          <!-- END TWO PRODUCTS -->
          <!-- BEGIN PROMO -->
          <div class="col-md-7 shop-index-carousel">
            <div class="content-slider">
              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                </ol>
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="public/img/sample-cover.jpg" class="img-responsive" alt="Berry Lace Dress">
                  </div>
                  <div class="item">
                    <img src="public/img/sample-cover2.jpg" class="img-responsive" alt="Berry Lace Dress">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- END PROMO -->
        </div>
        <!-- /介紹 -->

        <div class="row margin-top-20" style="background-color:#FFF; padding-top:10px; padding-bottom:10px;">
          <div class="col-md-12 col-sm-12">
            <h2 class="margin-top-20 margin-bottom-20"> 作品集 </h2>
            <div class="row margin-bottom-20">
              <?php for($i=0;$i<5;$i++):?>
                <div class="col-md-4 col-sm-4">
                  <div class="album-box">
                    <div class="row">
                      <div class="col-md-12 col-sm-12 album-cover"><img src="public/img/album-sample-01.jpg" class="img-responsive"></div>
                    </div>
                    <div class="row">
                      <div class="col-md-4 col-sm-4 album-cover"><img src="public/img/album-sample-02.jpg" class="img-responsive"></div>
                      <div class="col-md-4 col-sm-4 album-cover"><img src="public/img/album-sample-03.jpg" class="img-responsive"></div>
                      <div class="col-md-4 col-sm-4 album-cover"><img src="public/img/album-sample-04.jpg" class="img-responsive"></div>
                    </div>
                  </div>
                </div>
              <?php endfor;?>
            </div>
          </div>
        </div>
        <!-- /.作品集 -->

        <div class="row margin-top-20 margin-bottom-40" style="background-color:#FFF; padding-top:10px; padding-bottom:10px;">
          <div class="col-md-12 col-sm-12">
            <h2 class="margin-top-20 margin-bottom-20"> 粉絲團訊息 </h2>
            <?php for($i=0;$i<6;$i++):?>
              <div class="row margin-bottom-20">
                <div class="col-md-6 col-sm-6">
                  <div class="row" style="border-bottom:1px dotted #efefef;">
                    <div class="col-md-3 col-sm-6">
                      <a href="https://www.facebook.com/168527351961/photos/a.222091756961.146831.168527351961/10152972697181962/?type=1&theater" target="_blank"><img src="public/img/message-sample.jpg" class="img-responsive" alt="Berry Lace Dress"></a>
                    </div>
                    <div class="col-md-9 col-sm-6" style="border-right:1px dotted #efefef;">
                      【#LinLitalk】單身已婚戀愛中的妳，七夕情人節快樂💋<br>
                        #LinLi #訂製禮服 #LinLi新人每年都可以預約回來免費拍攝結婚週年照 #婚紗 #婚紗照 #LinLiwedding ＃婚禮<br>
                        線上看更多我們的作品<br>
                        ▶http://www.linli.com.tw/photo/trend.html<br>
                        線上預約試穿或參觀禮服是免費的喔<br>
                        ▶http://www.linliwedding.com/form.html
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <div class="row" style="border-bottom:1px dotted #efefef;">
                    <div class="col-md-3 col-sm-6">
                      <a href="https://www.facebook.com/168527351961/photos/a.222091756961.146831.168527351961/10152972697181962/?type=1&theater" target="_blank"><img src="public/img/message-sample.jpg" class="img-responsive" alt="Berry Lace Dress"></a>
                    </div>
                    <div class="col-md-9 col-sm-6">
                      【#LinLitalk】單身已婚戀愛中的妳，七夕情人節快樂💋<br>
                        #LinLi #訂製禮服 #LinLi新人每年都可以預約回來免費拍攝結婚週年照 #婚紗 #婚紗照 #LinLiwedding ＃婚禮<br>
                        線上看更多我們的作品<br>
                        ▶http://www.linli.com.tw/photo/trend.html<br>
                        線上預約試穿或參觀禮服是免費的喔<br>
                        ▶http://www.linliwedding.com/form.html
                    </div>
                  </div>
                </div>
              </div>
            <?php endfor;?>
          </div>
        </div>

      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
    <div class="pre-footer">
      <div class="container">
        <div class="row">
          <!-- BEGIN BOTTOM ABOUT BLOCK -->
          <div class="col-md-4 col-sm-6 pre-footer-col">
            <h2>關於</h2>
            <p>婚禮的專家，創造您幸福回憶的小幫手．</p>
        </div>
      </div>
    </div>
    <!-- END PRE-FOOTER -->

    <!-- BEGIN FOOTER -->
    <div class="footer">
      <div class="container">
        <div class="row">
          <!-- BEGIN COPYRIGHT -->
          <div class="col-md-6 col-sm-6 padding-top-10">
            2015 © IJWedding. ALL Rights Reserved. <a href="#">Privacy Policy</a> | <a href="#">Terms of Service</a>
          </div>
          <!-- END COPYRIGHT -->
          <!-- BEGIN PAYMENTS -->
          <div class="col-md-6 col-sm-6">
            <ul class="social-footer list-unstyled list-inline pull-right">
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-skype"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube"></i></a></li>
            </ul>  
          </div>
          <!-- END PAYMENTS -->
        </div>
      </div>
    </div>
    <!-- END FOOTER -->

    <!-- Load javascripts at bottom, this will reduce page load time -->
    <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
    <!--[if lt IE 9]>
    <script src="public/metronic/global/plugins/respond.min.js"></script>
    <![endif]--> 
    <script src="public/metronic/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="public/metronic/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="public/metronic/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>      
    <script src="public/metronic/frontend/layout/scripts/back-to-top.js" type="text/javascript"></script>
    <script src="public/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->

    <script src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
    <script src="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->
    <script src="public/metronic/global/plugins/zoom/jquery.zoom.min.js" type="text/javascript"></script><!-- product zoom -->
    <script src="public/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script><!-- Quantity -->

    <!-- BEGIN LayerSlider -->
    <script src="public/metronic/global/plugins/slider-layer-slider/js/greensock.js" type="text/javascript"></script><!-- External libraries: GreenSock -->
    <script src="public/metronic/global/plugins/slider-layer-slider/js/layerslider.transitions.js" type="text/javascript"></script><!-- LayerSlider script files -->
    <script src="public/metronic/global/plugins/slider-layer-slider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script><!-- LayerSlider script files -->
    <script src="public/metronic/frontend/pages/scripts/layerslider-init.js" type="text/javascript"></script>
    <!-- END LayerSlider -->

    <script src="public/metronic/frontend/layout/scripts/layout.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            Layout.init();
            Layout.initFixHeaderWithPreHeader();
            Layout.initNavScrolling();
        });
    </script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->

</body>
<!-- END BODY -->
</html>