<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 註冊 主內容的 page level js
*/
?>

<script src="public/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
<script src="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->
<script src="public/metronic/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script><!-- Quantity -->
<script src="public/metronic/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="public/metronic/global/plugins/rateit/src/jquery.rateit.js" type="text/javascript"></script>
<!-- BEGIN LayerSlider -->
<script src="public/metronic/global/plugins/slider-layer-slider/js/greensock.js" type="text/javascript"></script><!-- External libraries: GreenSock -->
<script src="public/metronic/global/plugins/slider-layer-slider/js/layerslider.transitions.js" type="text/javascript"></script><!-- LayerSlider script files -->
<script src="public/metronic/global/plugins/slider-layer-slider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script><!-- LayerSlider script files -->
<script src="public/metronic/frontend/pages/scripts/layerslider-init.js" type="text/javascript"></script>
<!-- END LayerSlider -->

<script src="public/js/easyzoom.js"></script>

<script src="public/metronic/frontend/layout/scripts/layout.js" type="text/javascript"></script>
<script type="text/javascript">
	var min_order_qt=<?php echo $product['min_order_qt'];?>;
	var incremental=<?php echo $product['incremental'];?>;
    jQuery(document).ready(function() {
        Layout.init();
        Layout.initOWL();
        LayersliderInit.initLayerSlider();
        //Layout.initImageZoom();
        Layout.initTouchspin();
        Layout.initFixHeaderWithPreHeader();
        Layout.initNavScrolling();
        //Layout.initUniform();
$('.note-success').delay(3000).fadeOut('slow');
	$('.contact').click(function(event) {
		$('html,body').animate({scrollTop:$('.product-page-content').offset().top}, 1000);
		//$('#QA').focus();
		//$('#qa_tab_title').focus();
		$('#qa_tab').click();
	});

	$('#product_spec').change(function(event) {
		var qty_in_stock = $('option:selected', this).attr('qty_in_stock');
		var qty = parseInt($('#qty').val());
		//console.log(qty_in_stock);
		var html='<option value="0">請選擇數量</option>';
	for (var x = min_order_qt; x <= qty_in_stock; x+=incremental) {
		ifselect=(x==qty)?'selected':'';
			html+='<option value="'+x+'" '+ifselect+'>'+x+'</option>';
	}
		//console.log(html);
		$('#qty')
		    .find('option')
		    .remove()
		    .end()
		    .append(html)
		;
	});
	$('#qty').change(function(event) {
		//console.log($('#qty').val());
		if($('#qty').val() > 0){
			$('.addon_amount').each(function(){
				if($(this).attr('addon_limitation')=='0'){
					var addon_limitation = parseInt($('#qty').val()); //購買上限
					var unispec_qty_in_stock = addon_limitation;
					start=addon_limitation;
				}else{
					var addon_limitation = parseInt($(this).attr('addon_limitation')); //購買上限
					var unispec_qty_in_stock = parseInt($(this).attr('unispec_qty_in_stock')); //庫存
					start=1;
				}
				if (unispec_qty_in_stock < addon_limitation) addon_limitation=unispec_qty_in_stock;
				var html='';
			//console.log(addon_limitation);
			//console.log(qty);
				for (var x = start; x <= addon_limitation; x++) {
						html+='<option value="'+x+'">'+x+'</option>';
				}
				$(this)
				    .find('option')
				    .remove()
				    .end()
				    .append(html)
				;
			});
		}
	});
	$('.addon_amount').click(function(event) {
		if($('#qty').val()==0 || !$('#qty').val()){
			alert('請先選擇主商品購買數量');
		}else{
			$('#addon'+$(this).attr('addon_log_sn')).prop('checked', true);
		}
	});
	$('#shopadd').click(function(e) {
		if($('#product_spec').val()==0 || !$('#product_spec').val()){
			alert('請選擇規格');
			return false;
		}
		if($('#qty').val()==0 || !$('#qty').val()){
			alert('請選擇購買數量');
			return false;
		}
		var ifaddon=false;
	$('.addon').each(function(){
			//var addon_limitation = $(this).attr('addon_limitation'));
				//console.log($(this).attr('addon_log_sn'));
			if($(this).prop('checked')){
				//console.log($(this).attr('addon_log_sn'));
				//console.log($('#addon_amount'+$(this).attr('addon_log_sn')).val());
				if($('#addon_amount'+$(this).attr('addon_log_sn')).val()==0){
					ifaddon=true;
				}
			}
		});
		if(ifaddon){
					alert('請選擇欲購買加購的商品數量');
					return false;
		}

		 var url = "shop/add"; // the script where you handle the form input.
		    $.ajax({
		           type: "POST",
		           url: url,
					dataType:'json',
		           data: $("#shop").serialize(), // serializes the form's elements.
		           success: function(data)
		           {
		           	if (data.error){
		           		alert(data.error);
		           	}else{
		           		$('.top-cart-info-count').text(data.total_items+' 件');
		           		$('.top-cart-info-value').text('$ '+data.total);
		           		//更新購物車popup
		           		//$('.top-cart-content').load('shop/top_cart');
		           		location.href='shop/shopView';
		           	}
		           }
		         });
		    e.preventDefault();
	});
	$('.addwish').click(function(e) {
		var product_sn=$(this).attr('product_sn');
		if(product_sn){
				$.get('member/AddwishList/'+product_sn+'/1',function(data){
        $('.heart'+product_sn).fadeIn("slow").html(data);
        if(data!='<a href="member/memberSignIn">請先點我登入會員</a>'){
							setTimeout(function(){$('.heart'+product_sn).hide();$('.heart'+product_sn).find("#text").text("");}, 10000);
						}
      //$('.heart'+product_sn).fadeOut("slow");
		          //console.log(product_sn);
		          //console.log($('.heart'+product_sn));
				 });
		}
	});
	$('#shopaddcart').click(function(e) {
		if($('#product_spec').val()==0 || !$('#product_spec').val()){
			alert('請選擇規格');
			return false;
		}
		if($('#qty').val()==0 || !$('#qty').val()){
			alert('請選擇購買數量');
			return false;
		}
		var ifaddon=false;
	$('.addon').each(function(){
			//var addon_limitation = $(this).attr('addon_limitation'));
			if($(this).prop('checked')){
				//console.log($('#addon_amount'+$(this).attr('addon_log_sn')).val());
				if($('#addon_amount'+$(this).attr('addon_log_sn')).val()==0){
					ifaddon=true;
				}
			}
		});
		if(ifaddon){
					alert('請選擇欲購買加購的商品數量');
					return false;
		}

		 var url = "shop/add"; // the script where you handle the form input.
		    $.ajax({
		           type: "POST",
		           url: url,
					dataType:'json',
		           data: $("#shop").serialize(), // serializes the form's elements.
		           success: function(data)
		           {
		           	if (data.error){
		           		alert(data.error);
		           	}else{
		           		$('.top-cart-info-count').text(data.total_items+' 件');
		           		$('.top-cart-info-value').text('$ '+data.total);
		           		$('.top-cart-content').load('shop/top_cart');
		           		//console.log(data.total);
		           	}
		           }
		         });
		    e.preventDefault();
	});
<?php if($product['unispec_flag']=='1'){?>
	        $('#qty').trigger('change');
<?php }?>
    });
  var $easyzoom = $('.easyzoom').easyZoom();

  // Setup thumbnails example
  var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');

  $('.thumbnails').on('click', 'a', function(e) {
   var $this = $(this);

   e.preventDefault();

   // Use EasyZoom's `swap` method
   api1.swap($this.data('standard'), $this.attr('href'));
  });

  // Setup toggles example
  var api2 = $easyzoom.filter('.easyzoom--with-toggle').data('easyZoom');

  $('.toggle').on('click', function() {
   var $this = $(this);

   if ($this.data("active") === true) {
    $this.text("Switch on").data("active", false);
    api2.teardown();
   } else {
    $this.text("Switch off").data("active", true);
    api2._init();
   }
  });
function addtofacebook() {u=location.href;t=document.title;window.open("https://www.facebook.com/sharer.php?u="+encodeURIComponent(u)+"&t="+encodeURIComponent(t),"","fullscreen=no,scrollbars=no,resizable=no");}
function addtoline() {u=location.href;t=document.title;window.open("<?=$line_share?>","","fullscreen=yes,scrollbars=yes,resizable=yes");}

	$('.addcart').click(function(e) {
		var product_sn=$(this).attr('product_sn');
		var product_spec=$(this).attr('product_spec');
		if(product_sn){
		 var url = "shop/add";
		    $.ajax({
		           type: "POST",
		           url: url,
					dataType:'json',
		           data: { product_sn:product_sn,qty:'1',product_spec:product_spec },
		           success: function(data)
		           {
		           	if (data.error){
		           		alert(data.error);
		           	}else{
		           		$('.top-cart-info-count').text(data.total_items);
		           		$('.top-cart-info-value').text('$ '+data.total);
		           		//更新購物車popup
		           		$('.top-cart-content').load('shop/top_cart');
		           		//console.log(data.total);
		           	}
		           }
		         });
		  }
		    e.preventDefault();
	});
	$('.shopadd').click(function(e) {
		var product_sn=$(this).attr('product_sn');
		var product_spec=$(this).attr('product_spec');
		if(product_sn){
		 var url = "shop/add";
		    $.ajax({
		           type: "POST",
		           url: url,
					dataType:'json',
		           data: { product_sn:product_sn,qty:'1',product_spec:product_spec },
		           success: function(data)
		           {
		           	if (data.error){
		           		alert(data.error);
		           	}else{
		           		//$('.top-cart-info-count').text(data.total_items+' 件');
		           		//$('.top-cart-info-value').text('$ '+data.total);
		           		//更新購物車popup
		           		//$('.top-cart-content').load('shop/top_cart');
					    location.href='/shop/shopView';
		           		//console.log(data.total);
		           	}
		           }
		         });
		  }
		    e.preventDefault();
	});
	$('.fa-heart').click(function(e) {
		var product_sn=$(this).attr('product_sn');
		if(product_sn){
				$.get('member/AddwishList/'+product_sn+'/1',function(data){
        $('.heart'+product_sn).fadeIn("slow").html(data);
        if(data!='<a href="member/memberSignIn">請先點我登入會員</a>'){
							setTimeout(function(){$('.heart'+product_sn).hide();$('.heart'+product_sn).find("#text").text("");}, 10000);
						}
		          //console.log(product_sn);
		          //console.log($('.heart'+product_sn));
				 });
		}
	});
	checkoutupdate = function(ifcheckout,rowid){
	};

</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-54f5843b32e9e31d" async></script>