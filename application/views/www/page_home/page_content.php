<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Home 主內容
*/
?>

<!-- Page level plugin styles START -->
<link href="public/metronic/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<link href="public/metronic/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="public/metronic/global/plugins/slider-layer-slider/css/layerslider.css" rel="stylesheet">
<!-- Page level plugin styles END -->

<!-- BEGIN SERVICE BOX -->   
<div id="section-service-box">
  <div class="container">
    <div class="row service-box padding-top-35" id="section-service">
      <div class="col-md-4 col-sm-4">
        <div class="service-box-heading">
          <img src="public/img/icon-website.png" alt="婚禮網站">
          <span>婚禮網站 <span class="alias hidden-md">Website</span></span>
        </div>
        <p>訂製專屬你們的精美婚禮網站，線上分享喜訊，驚艷親朋好友．</p>
      </div>
      <div class="col-md-4 col-sm-4">
        <div class="service-box-heading">
          <img src="public/img/icon-app.png" alt="現場報到系統">
          <span>婚宴管理 <span class="alias hidden-md">APP</span></span>
        </div>
        <p>簽到、禮金流程電子化，讓婚宴現場不再手忙腳亂</p>
      </div>
      <div class="col-md-4 col-sm-4">
        <div class="service-box-heading">
          <img src="public/img/icon-shop.png" alt="結婚商店">
          <span>結婚商店 <span class="alias hidden-md">Shop</span></span>
        </div>
        <p>簡潔到奢華，各種風格婚宴周邊小物任你挑選。</p>
      </div>
    </div>
    <!-- END SERVICE BOX --> 
  </div>
</div>
<div style="background-color:#FFF;">
  <div class="container">

    <!-- BEGIN BLOCKQUOTE BLOCK -->   
    <div class="row padding-top-40 margin-bottom-30 feature-box-01">
      <div class="col-md-6" style="padding:30px 0px">
        <img src="public/img/feature-website-title.png" alt="結婚網站" class="img-responsive">
        <p>
          各式精美的網站版型供你選擇，響應不同螢幕大小，操作簡易，讓你動動滑鼠，就能架設專屬自己的婚禮網站．<br>
          把喜訊的傳遞以及回函管理濃縮在婚禮網站，結合社群輕鬆傳遞你的幸福氛圍．
        </p>
        <div style="text-align:center;padding-top: 24px;">
          <a href="javascript:;" class="btn btn-warning" style="background-color:#cebea2;margin:0px auto;font-size:22px;padding-left:48px;padding-right:48px;">開始製作</a>
        </div>
      </div>
      <div class="col-md-6">
        <img src="public/img/feature-website.png" alt="結婚網站" class="img-responsive">
      </div>
    </div>
    <!-- END BLOCKQUOTE BLOCK -->
  </div>
</div>
<div style="background-image:url(public/img/devider-home-app.png);background-size:cover;height:520px;">
  <div class="container">
    <div class="row padding-top-60 margin-bottom-30 feature-box-02">
      <div class="col-md-6 col-md-offset-6" style="">
        <img src="public/img/feature-app-title" alt="婚宴管理" class="img-responsive">
        <p>
          透過創新的科技管理婚宴現場．APP系統替你掌握現場賓客報導狀況及禮金收入．婚禮當天，新郎新娘只需要擔心自己美不美，不需要擔心遺漏了誰．
          <div style="text-align:center;padding-top: 48px;">
            <a href="javascript:;" class="btn btn-warning" style="background-color:#cebea2;margin:0px auto;font-size:22px;padding-left:48px;padding-right:48px;">開始體驗</a>
          </div>
        </p>
      </div>
    </div>
  </div>
</div>
<div style="background-color:#FFF;">
  <div class="container">

    <!-- BEGIN BLOCKQUOTE BLOCK -->   
    <div class="row padding-top-60 margin-bottom-30 feature-box-03">
      <div class="col-md-3">
        <img src="public/img/feature-shop-title.png" alt="結婚商店" class="img-responsive">
      </div>
      <div class="col-md-6">
        <p>
          各式婚禮小物、邀請卡及婚禮包裝材料，風格從簡約時尚道精緻奢華任你挑選，專業的包裝設計、創意的商品巧思，幫你把婚禮包裝得更完美．
        </p>
      </div>
      <div class="col-md-3">
        <div style="text-align:center;padding-top: 10px;">
          <a href="javascript:;" class="btn btn-warning" style="background-color:#cebea2;margin:0px auto;font-size:22px;padding-left:48px;padding-right:48px;">開始選購</a>
        </div>
      </div>
    </div>
    <div class="row margin-bottom-60 feature-box-03">
      <div class="col-md-4">
        <div class="product-item">
          <div class="pi-img-wrapper">
            <img src="http://www.kute.com.tw/UploadFile/GoodPic/142770355013875_middle.jpg" class="img-responsive">
          </div>
          <h3>創意喜帖</h3>
        </div>
      </div>
      <div class="col-md-4">
        <div class="product-item">
          <div class="pi-img-wrapper">
            <img src="http://www.kute.com.tw/UploadFile/GoodPic/142769743610634_middle.jpg" class="img-responsive">
          </div>
          <h3>個性化婚紗謝卡</h3>
        </div>
      </div>
      <div class="col-md-4">
        <div class="product-item">
          <div class="pi-img-wrapper">
            <img src="http://www.kute.com.tw/UploadFile/GoodPic/14124193333806_middle.jpg" class="img-responsive">
          </div>
          <h3>結婚證書夾</h3>
        </div>
      </div>
    </div>
    <!-- END BLOCKQUOTE BLOCK -->
  </div>
</div>
<!--
<div style="background-color:#FFF;">
  <div class="container">
    <div class="row padding-top-60 our-clients" id="section-parters">
      <h3>合作夥伴 Partners & Distributors</h3>
    </div>
  </div>
</div>
<div style="">
  <div class="container">
    <div class="row" id="section-partners-box">
      <div class="client-item">
        <a href="kute.com.tw" target="_blank">
          <img src="public/img/home-partner-1.png" class="img-responsive" alt="">
        </a>
      </div>
      <div class="client-item">
        <a href="https://www.ilikeus.com.tw/" target="_blank">
          <img src="public/img/home-partner-2.png" class="img-responsive" alt="">
        </a>
      </div>
    </div>
  </div>
</div>
-->