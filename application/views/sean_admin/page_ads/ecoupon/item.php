<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<!-- END PAGE LEVEL STYLES -->



<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>行銷管理 <small>虛擬折扣卷活動編輯</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin-ijwedding/Ads">首頁</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        優惠活動
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin-ijwedding/Ads/ecoupon">虛擬折扣卷</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        設定
      </li>
    </ul>
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN SETTING TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">虛擬折扣卷活動設定</span>
            </div>
            <div class="tools">
              <a href="admin-ijwedding/Ads/ecoupon"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
          </div>
          <div class="portlet-body form">
            <form class="form-horizontal" role="form">
              <div class="form-body">
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">活動名稱</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" value="活動名稱">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">最低消費金額</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="0">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">活動起始日期</label>
                  <div class="col-lg-4">
                    <div class="input-group date form_datetime">
                      <input type="text" class="form-control form-control-inline input-medium date-picker" value="2015-05-08">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">活動結束日期</label>
                  <div class="col-lg-4">
                    <div class="input-group date form_datetime">
                      <input type="text" class="form-control form-control-inline input-medium date-picker" value="2015-05-08">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-2 control-label">折扣價格</label>
                  <div class="col-lg-10">
                    <div class="radio-list row">
                      <div class="col-md-3">
                        <label class="radio-inline" style="padding-left:0px;">
                          <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                           金額抵用 (ex.90)
                        </label>
                      </div>
                      <div class="col-md-8">
                        <input type="text" size="3" value="0" class="form-control form-control-inline input-xsmall input-sm">
                      </div>
                    </div>
                    <div class="radio-list row">
                      <div class="col-md-3">
                        <label class="radio-inline" style="padding-left:0px;">
                          <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                          百分比抵用 (ex.0.9)
                        </label>
                      </div>
                      <div class="col-md-8">
                        <input type="text" size="3" value="0" class="form-control form-control-inline input-xsmall input-sm">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="active" class="col-lg-2 control-label">適用分類</label>
                  <div class="col-lg-10">
                    <input type="hidden" class="form-control select2_category" value="分類1, 分類2">
                    <!-- select option 在 item_js 中 -->
                  </div>
                </div>
                <div class="form-group">
                  <label for="note" class="col-lg-2 control-label">適用產品</label>
                  <div class="col-lg-10">
                    <textarea class="form-control" rows="3"></textarea>
                    <span class="help-block">一行一個產品編號</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-lg-2 control-label">數量</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control input-medium" value="100">
                  </div>
                </div>
                <div class="form-group">
                  <label for="updater" class="col-md-2 control-label">更新者</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control"  placeholder="administrator" Disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="time-update" class="col-md-2 control-label">更新時間</label>
                  <div class="col-md-10">
                    <input type="text" class="form-control"  placeholder="2015-05-25 10:00" Disabled>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">儲存</button>
                    <button type="button" class="btn default">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- END SETTING TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
