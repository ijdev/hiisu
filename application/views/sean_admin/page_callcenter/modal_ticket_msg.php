<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 客服中心往返的 msg 列表
*/
?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
  <h4 class="modal-title">訊息記錄</h4>
</div>
<div class="modal-body">
  <div class="portlet box">
    <div class="portlet-body" id="chats">
      <ul class="chats">
        <li class="in">
          <img class="avatar" alt="" src="public/img/icon-supportfemale-48.png">
          <div class="message">
            <span class="arrow">
            </span>
            <a href="javascript:;" class="name">
            客服人員 </a>
            <span class="datetime">
            at May 04 2015 20:09 </span>
            <span class="body">
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
          </div>
        </li>
        <li class="out">
          <img class="avatar" alt="" src="public/img/icon-diamond-48.png">
          <div class="message">
            <span class="arrow">
            </span>
            <a href="javascript:;" class="name"> Luciel Lo </a>
            <span class="datetime">
            at May 04 2015 20:00 </span>
            <span class="body">
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
          </div>
        </li>
        <li class="in">
          <img class="avatar" alt="" src="public/img/icon-supportfemale-48.png">
          <div class="message">
            <span class="arrow">
            </span>
            <a href="javascript:;" class="name">
            客服人員 </a>
            <span class="datetime">
            at May 04 2015 19:50 </span>
            <span class="body">
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
          </div>
        </li>
        <li class="out">
          <img class="avatar" alt="" src="public/img/icon-diamond-48.png">
          <div class="message">
            <span class="arrow">
            </span>
            <a href="javascript:;" class="name"> Luciel Lo </a>
            <span class="datetime">
            at May 04 2015 19:30 </span>
            <span class="body">
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<?php if(!isset($done) || $done=== false):?>
  <!-- 若 ticket 狀態尚未完成則顯示 -->
  <div class="modal-footer">
    <div class="row">
      <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9">
        <textarea class="form-control" rows="2"></textarea>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3">
        <button type="button" class="btn blue">送出</button>
      </div>
    </div>
  </div>
  <!-- /若 ticket 狀態尚未完成則顯示 -->
<?php endif;?>