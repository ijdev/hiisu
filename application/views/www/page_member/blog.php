<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Blog Page
*/
?>

	<div class="outer-blog">
		<div class="col-sm-12 blog_page-top-bar">
			<p>Home > Blog </p>
		</div>
		<div class="col-sm-3 blog-left">
			<div class="col-sm-12 blog-search">
				<form>
					<div>
						<span><input type="text" value="" name="s" id="s" placeholder="Start Searching...." />
						<input type="submit" id="searchsubmit" value="Search"/></span> 
					</div>
				</form>
			</div>
			<div class="col-sm-12 blogside-bar">
				<h5>文章分類</h5>
				<ul>
					<li>全版文章</li>
					<li>最新消息</li>
					<li>問⯅答</li>
					<li>常見問題</li>
					<li>活動新訊</li>
					<li>特別企劃</li>
				</ul>
			</div>
			<div class="col-sm-12 blogside-bar2">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#home">不能忽視的事實是</a></li>
						<li><a data-toggle="tab" href="#menu1">古老的人類的文明</a></li>
					</ul>

					<div class="tab-content">
						<div id="home" class="tab-pane fade in active">
						  <div class="col-sm-5 sidebar_img-box">
						  
						  </div>
						  <div class="col-sm-6 sidebar_cnt">
						  
						  </div>
						</div>
						
						<div id="menu1" class="tab-pane fade">
						  <ul>
							<li>1.1</li>
							<li>1.2</li>
							<li>1.3</li>
							<li>1.4</li>
						  </ul>
						  						
						</div>
					</div>
				
			</div>
		</div>
		<div class="col-sm-9 blog-right">
			<div class="col-sm-12 blog-content">
				<div class="blog-cnt-hrd">
					<h2>話題:GOLDWELL歌薇DS光感60秒髮膜FG市調大隊評鑑報告</h2>
					<p>2016-09-18 / 105 Views</p>
				</div>
				
				<p>開頭ඛ破題，對於本次髮膜產品整體不算滿意，簡單說沒有購買意願，對細部原因有⯆趣了解的人請再往ୗ看。</p>
				
				<p>本人超長髮(過腰)，髮質細軟又經過多次ᰁ燙，算是中度受損髮(洗完髮完全不護就是會毛會瘋狂打結)。一開始試用好幾次都是在全部頭髮ୖ，只覺得跟普通潤髮乳效果差不多，吹完頭髮還會覺得頭髮微卡，並沒有ᰂ順感。擔心是自己錯覺，因此後面2次做了特別的實驗。</p>
				
				<p>實驗PART 1</p>
				
				<p>本產品vs.完全不潤/護髮</p>
				
				<p>圖左是使用本次的市調髮膜，圖右是完全沒用任何潤/護髮，只有深層清潔洗髮。</p>
				<div class="col-sm-5 blog-img-box">
					<img class="img-responsive" src="public/img/147419064115181.jpg" alt="話題:GOLDWELL歌薇DS光感60秒髮膜FG市調大隊評鑑報告">
				</div>
			</div>
			<div class="col-sm-12 blog-content-bottom-bar">
			
			</div>
		</div>
	</div>
	

<style>
.outer-blog {
    float: left;
    width: 100%;
    padding: 0px 0px;
    border: 1px solid #000;
    margin: 10px 0px;
}
.col-sm-12.blog_page-top-bar {
    border-bottom: 1px solid #000;
}
.col-sm-12.blog_page-top-bar p {
    margin: 0px;
    padding: 6px 0px;
}
.col-sm-12.blog-search {
    border: 1px solid #000;
    margin: 10px 0px;
    padding: 5px;
}

.col-sm-12.blog-search input#s {
    padding: 8px 5px;
    font-size: 16px;
    float: left;
    width: 77%;
}
.col-sm-12.blog-search input#searchsubmit {
    padding: 8px 0px;
    float: left;
    width: auto;
    font-size: 16px;
}
.col-sm-12.blogside-bar {
    border: 1px solid #000;
    padding: 5px;
}
.col-sm-12.blogside-bar h5 {
    border-bottom: 1px solid #000;
    margin: 0px;
    padding: 10px 0px;
    font-size: 18px;
    font-weight: 600;
}
.col-sm-12.blogside-bar ul {
    margin: 0px;
    list-style: none;
    line-height: 30px;
    padding: 5px;
}
.col-sm-12.blogside-bar2 {
    border: 1px solid #000;
    padding: 5px;
    margin: 10px 0px;
}
.col-sm-9.blog-right {
    margin: 10px 0px;
}
.col-sm-12.blog-content {
    border: 1px solid #000;
    padding: 0px;
}
.blog-cnt-hrd {
    float: left;
    width: 100%;
    border-bottom: 1px solid #ddd;
    margin-bottom: 10px;
}
.col-sm-12.blog-content h2 {
    margin: 2px 0px 10px;
    padding: 5px 2px;
}
.col-sm-12.blog-content p {
    padding: 5px 2px;
    margin-top: 20px;
}
.col-sm-12.blogside-bar2 ul li {
    width: 50%;
}
.col-sm-12.blogside-bar2 a {
    padding: 3px !important;
    font-size: 14px;
}
.col-sm-5.blog-img-box {
    margin: 20px 0px;
    padding: 10px 0px;
}
</style>  