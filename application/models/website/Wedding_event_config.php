<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wedding_event_config extends CI_Model {

    public $wedding_website_sn;
    public $limit = 10; // query limit setting
    public $until = 0; // last query sn

    public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
    }
    /*
    * 婚宴資訊列表
    * 全數拉出管理
    * 限制的部分 toto
    */
    public function get_result( $wedding_website_sn = 0 )
    {
        $this->db->select('*');
        $this->db->from('ij_wedding_event_config');
        $this->db->where('wedding_website_sn', $wedding_website_sn);
        $this->db->where('status', 1);
        $this->db->order_by('wedding_event_start_date', 'asc');
        $this->db->order_by('wedding_event_sn', 'desc');
        $query = $this->db->get();
        return $result = $query->result_array();
    }
}