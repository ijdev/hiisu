<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
$this->CI =& get_instance();
$this->CI->load->model('libraries_model');
$member_level_type_ct=$this->CI->libraries_model->_select('ij_member_level_type_ct','display_flag',1,0,0,'member_level_type',0,'list','member_level_type,member_level_type_name');

$config  = array(
					 "member_data" => array(
					 "member_sn" => "會員代號",
					 "user_name" => "會員帳號",
					 "password" => "會員密碼",
					 "first_name" => "姓名",
					 "last_name" => "姓名",
					 "openid_name" => "openID",
					 "openid_nickname" => "暱稱",// F or M
					 "birthday" => "生日",//2014-11-19 	年－月－日
					 "wedding_date" => "結婚日期",//2014-11-19 	年－月－日
					 "tel_area_code" => "區碼",
					 "tel" => "電話",
					 "tel_ext" => "分機",
					 "fax" => "傳真",
					 "cell" => "行動電話",
					 "email" => "email",
					 "gender" => "性別",
					 "zipcode" => "郵遞區號",
					 "addr_state" => "本島/外島",
					 "addr_city_code" => "縣市",
					 "addr_town_code" => "鄉鎮",
					 "addr1" => "地址",
					 "registration_date" => "註冊日期",
					 "member_status" => "狀態",
					 "system_user_flag" => "是否為管理者",
					 "member_level_type" => "等級",
					 "dealer_user_type" => "dealer分級",
					 "associated_supplier_sn" => "供應商",
					 "associated_dealer_sn" => "經銷商",
					 "associated_partner_sn" => "聯盟會員",
					 "accept_edm_flag" => "是否願意收到電子報"
				 ),

			"member_log" => array(
					"iid" => "iid",
					"type" => "type",//1 login true , 2 login false , 3 logout
					"browser" => "browser",
					"b_version" => "version",
					"mobile" => "mobile",
					"robot" => "robot",
					"platform" => "platform",
					"referrer" => "referrer",
					"agent_string" => "agent_string",
					"accept_lang" => "accept_lang",
					"accept_chartset" => "accept_chartset",
					"account" => "account",
					"mid" => "mid",
					"login_time" => "login_time",
					"connect_ip" => "connect_ip",
				),
			"member_level_type_ct"=>$member_level_type_ct,
);
