<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品管理 <small>上下架管理</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="preview">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
         <a href="preview/inventory">上下架管理</a>
         <i class="fa fa-circle"></i>
      </li>
      <li>
        商品名稱 XXX
         <i class="fa fa-circle"></i>
      </li>
      <li class="active">
        上下架管理 Log
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">商品名稱 XXX - 上下架管理 Log</span>
            </div>
            <div class="actions btn-set">
            </div>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>主分類</th>
                  <th>商品編號</th>
                  <th>名稱</th>
                  <th>成本價</th>
                  <th>動作</th>
                  <th>數量</th>
                  <th>描述</th>
                  <th>異動時間</th>
                  <th>異動人員</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=0;$i<8;$i++):?>
                  <tr class="odd gradeX">
                    <td>線上喜帖</td>
                    <td>d234234</td>
                    <td>產品<?php echo $i;?><a href="http://dev.ijwedding.com/preview_jerry/shopItem" target="_blank">&nbsp;<i class="fa fa-external-link"></i></a></td>
                    <td>500</td>
                    <td class="center">
                      <?php if($i%2==0):?>
                        <span class="label label-success">入庫</span>
                      <?php else:?>
                        <span class="label label-danger">報廢</span>
                      <?php endif;?>
                    </td>
                    <td>100</td>
                    <td>描述描述描述描述描述描述</td>
                    <td><small>2015-05-20 12:00:00</small></td>
                    <td class="center">Administrator</td>
                  </tr>
                <?php endfor;?>
              </tbody>
            </table>

            <div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                  <ul class="pagination" style="visibility: visible;">
                    <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                    <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li class="active"><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                    <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->