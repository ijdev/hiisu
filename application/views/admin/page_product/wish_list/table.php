<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>商品管理 <small>追蹤清單</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin/admin">Home</a><i class="fa fa-circle"></i>
      </li>
      <li>
        商品管理
        <i class="fa fa-circle"></i>
      </li>
      <li class="active">
        追蹤清單
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
<form id="form1" class="form-horizontal" role="form" method="get">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">商品追蹤清單</span>
            </div>
            <div class="actions btn-set">
            </div>
          </div>
          <div class="note note-warning">
            <div class="input-group input-large date-picker input-daterange pull-left" data-date="2015-05" data-date-format="yyyy-mm">
              <span class="input-group-addon"> 期間 </span>
              <input type="text" class="form-control input-sm" name="from" value="<?php echo @$from;?>">
              <span class="input-group-addon"> 到 </span>
              <input type="text" class="form-control input-sm" name="to" value="<?php echo @$to;?>">
            </div>
            <div class="table-actions-wrapper pull-left">
              <span>
              </span>
              <input type="text" name="user_name" value="<?php echo @$user_name;?>" class="pagination-panel-input form-control input-inline input-sm" size="8" style="margin: 0 5px;" placeholder="會員姓名">
              <input type="text" name="product_sn" value="<?php echo @$product_sn;?>" class="pagination-panel-input form-control input-inline input-sm" size="8" style="margin: 0 5px;" placeholder="商品編號">
              <input type="text" name="product_name" value="<?php echo @$product_name;?>" class="pagination-panel-input form-control input-inline input-sm" size="8" style="margin: 0 5px;" placeholder="商品名稱">
              <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋</button> &nbsp; 
              <!--button onclick="location.href='admin/product/wishListx';" class="btn btn-sm purple"><i class="fa"></i> 清除</button-->
              <button id="reset_form" onclick="this.form.flg.value='reset';" class="btn btn-sm purple"><i class="fa"></i> 清除</button>
              <input type="hidden" name="flg" value="">
            </div>
            <div style="clear:both;"></div>
          </div>
          <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>加入時間</th>
                  <th>商品編號</th>
                  <th>商品名稱</th>
                  <th>會員姓名</th>
                </tr>
              </thead>
              <tbody>
<?php	if(@$Items){foreach(@$Items as $Item){?>
                  <tr class="odd gradeX">
                    <td><?php echo $Item['create_date'];?></td>
                    <td><a><?php echo $Item['product_sn'];?></a></td>
                    <td><?php echo $Item['product_name'];?></td>
                    <td><a><?php echo $Item['user_name'];?></a></td>
                  </tr>
<?php }}?>
              </tbody>
            </table>

            <!-- page nav -->
            <div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">筆數：<?php echo $total;?> </div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
									<?php echo $page_links;?>
                </div>
              </div>
            </div>
            <!-- end of page nav -->
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </form>
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->