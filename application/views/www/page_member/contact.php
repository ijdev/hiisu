<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Contact us
*/
?>


<div class="row">

				<!-- 說明 -->

				<div class="col-xs-10 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-1 col-xs-offset-1">

                    <h2><span class="bfont size-25 weight-300">聯絡我們</span></h2>

					<p>
	為了提供您最快速的服務，請您先瀏覽相關說明。若仍然無法回答您的問題請留下您的問題，我們將竭誠為您服務。</p>


				</div>

				<!-- /說明 -->

				<!-- FORM -->

				<div class="col-xs-10 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-1 col-xs-offset-1 margin-bottom-50">

					<form name="form1" id="regform" method="post" action="https://sm5r.ddcs.com.tw/help/contact.php" class="form-horizontal bv-form" role="form" enctype="multipart/form-data" novalidate="novalidate"><button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                        <input type="hidden" name="action" value="insert">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group has-feedback">
									<label>* 聯&nbsp;絡&nbsp;人</label>
									<input type="name" name="companyname" id="companyname" class="form-control radius-0" data-bv-field="companyname"><i class="form-control-feedback" data-bv-icon-for="companyname" style="display: none;"></i>
								<small class="help-block" data-bv-validator="stringLength" data-bv-for="companyname" data-bv-result="NOT_VALIDATED" style="display: none;">請輸入真實姓名，不可空白</small><small class="help-block" data-bv-validator="notEmpty" data-bv-for="companyname" data-bv-result="NOT_VALIDATED" style="display: none;">請輸入真實姓名，不可空白</small></div>
							</div>
							<div class="col-xs-12 col-sm-12">
								<div class="form-group has-feedback">
									<label>* 電子信箱</label>
									<input name="email" class="form-control radius-0" type="email" id="email" data-bv-field="email"><i class="form-control-feedback" data-bv-icon-for="email" style="display: none;"></i>
								<small class="help-block" data-bv-validator="notEmpty" data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">請輸入Email，不可空白</small><small class="help-block" data-bv-validator="emailAddress" data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">Email無效</small></div>
							</div>
							<div class="col-xs-12 col-sm-12">
								<div class="form-group has-feedback">
									<label>* 電話號碼</label>
									<input name="tel_two" type="phone" id="tel_two" class="form-control radius-0" placeholder="填入市話或手機號碼" data-bv-field="tel_two"><i class="form-control-feedback" data-bv-icon-for="tel_two" style="display: none;"></i>
								<small class="help-block" data-bv-validator="notEmpty" data-bv-for="tel_two" data-bv-result="NOT_VALIDATED" style="display: none;">請輸入電話，不可空白</small></div>
							</div>
							<div class="col-xs-12 col-sm-12">
								<div class="form-group has-feedback">
									<label>* 聯絡事項</label>
									<select class="form-control select radius-0" name="type" data-bv-field="type">
										<option value="">請選擇</option>
																					<option value="商品問題">商品問題</option>
																					<option value="購買問題">購買問題</option>
																					<option value="訂單問題">訂單問題</option>
																			</select><i class="form-control-feedback" data-bv-icon-for="type" style="display: none;"></i>
								<small class="help-block" data-bv-validator="notEmpty" data-bv-for="type" data-bv-result="NOT_VALIDATED" style="display: none;">請選擇聯絡事項</small></div>
							</div>
							<div class="col-xs-12 col-sm-12">
								<div class="form-group has-feedback">
									<label>* 聯絡內容</label>
									<textarea maxlength="10000" class="form-control radius-0" name="content" cols="40" rows="5" id="content" data-bv-field="content"></textarea><i class="form-control-feedback" data-bv-icon-for="content" style="display: none;"></i>
								<small class="help-block" data-bv-validator="stringLength" data-bv-for="content" data-bv-result="NOT_VALIDATED" style="display: none;">請填寫聯絡內容，不可少於4個字</small><small class="help-block" data-bv-validator="notEmpty" data-bv-for="content" data-bv-result="NOT_VALIDATED" style="display: none;">請填寫聯絡內容</small></div>
							</div>
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>附件</label>
									<input type="file" name="file">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12">
								<div class="form-group has-feedback">
									<label>*驗證碼<!--驗證碼--></label>
									<input type="text" name="inputcode" id="inputcode" class="form-control radius-0" data-bv-field="inputcode"><i class="form-control-feedback" data-bv-icon-for="inputcode" style="display: none;"></i>
								<small class="help-block" data-bv-validator="stringLength" data-bv-for="inputcode" data-bv-result="NOT_VALIDATED" style="display: none;">請輸入5個字數</small><small class="help-block" data-bv-validator="notEmpty" data-bv-for="inputcode" data-bv-result="NOT_VALIDATED" style="display: none;">請輸入驗證碼，不可空白</small></div>
							</div>
							<div class="col-xs-6 col-sm-6 col-xs-6 col-sm-6 nopadding margin-bottom-20">
								<img id="responsive" class="img-responsive" src="https://sm5r.ddcs.com.tw/Resources/securimage/securimage_show.php?sid=1477484554" style="max-width:130px">
							</div>
							<div class="col-xs-12 col-sm-6">
								<a href="javascript:void(0);" onclick="document.getElementById('responsive').src = document.getElementById('responsive').src+'?';"><i class="fa fa-refresh size-20"></i></a>
							</div>
							<div class="col-xs-12 col-sm-12 nopadding">
								<button type="submit" name="Submit2" class="col-xs-12 col-sm-4 btn btn-black"> 確定送出</button>
							</div>

					</form>
				</div>

				<!-- /FORM -->
			</div>