<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 總控 dashboard
*/
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="public/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->



<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>行銷管理 <small>虛擬折扣券統計</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
    <ul class="page-breadcrumb breadcrumb">
      <li>
        <a href="admin-ijwedding/Ads">首頁</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        行銷管理
        <i class="fa fa-circle"></i>
      </li>
      <li>
        優惠活動
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="admin-ijwedding/Ads/ecoupon">虛擬折扣券</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        統計
      </li>
    </ul>
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-6">
        <!-- BEGIN ITEM TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">活動名稱 > 狀態統計</span>
            </div>
            <div class="actions btn-set">
            </div>
          </div>
          <div class="portlet-body">
            <div id="chart_status" class="chart" style="height: 300px;"></div>
          </div>
        </div>
        <!-- END SETTING TABLE PORTLET-->
      </div>
      <div class="col-md-6">
        <!-- BEGIN ITEM TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">活動名稱 > 啟用比例</span>
            </div>
            <div class="actions btn-set">
            </div>
          </div>
          <div class="portlet-body">
            <div id="chart_enable" class="chart" style="height: 300px;"></div>
          </div>
        </div>
        <!-- END SETTING TABLE PORTLET-->
      </div>
    </div>

    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN ITEM TABLE PORTLET-->
        <div class="portlet light">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cogs font-green-sharp"></i>
              <span class="caption-subject font-green-sharp bold uppercase">活動名稱 > 序號管理</span>
            </div>
          </div>
          <div class="portlet-body form">
            <div class="note note-warning">
              <div class="table-actions-wrapper pull-right">
                <select class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size: 11px;">
                  <option>是否發送</option>
                  <option>已發送</option>
                  <option>未發送</option>
                </select>
                <select class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size: 11px;">
                  <option>是否使用</option>
                  <option>已使用</option>
                  <option>未使用</option>
                </select>
                <select class="table-group-action-input form-control input-inline input-xsmall input-sm" style="font-size: 11px;">
                  <option>是否失效</option>
                  <option>已失效</option>
                  <option>未失效</option>
                </select>
                <input type="text" class="pagination-panel-input form-control input-inline input-sm" size="30" style="margin: 0 5px;" placeholder="使用 barcode scanner 掃 code 搜尋">
                <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> 搜尋虛擬折扣券</button>
              </div>
              <div style="clear:both;"></div>
            </div>
            <table class="table table-striped table-bordered table-hover" id="table_member">
              <thead>
                <tr>
                  <th>編號</th>
                  <th>是否發送</th>
                  <th>是否使用</th>
                  <th>是否失效</th>
                  <th>功能</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=0;$i<20;$i++):?>
                  <tr class="odd gradeX">
                    <td>
                      xewfrhowhfoih
                    </td>
                    <td class="center">
                      <i class="fa fa-check"></i>
                    </td>
                    <td class="center">
                      <?php if($i%2==0):?>
                        <i class="fa fa-check"></i>
                      <?php else:?>
                        <i class="fa fa-times"></i>
                      <?php endif;?>
                    </td>
                    <td class="center">
                      <i class="fa fa-times"></i>
                    </td>
                    <td class="center">
                      <a href="javascript:void(0);"><i class="fa fa-trash-o"></i>棄用</a>
                    </td>
                  </tr>
                <?php endfor;?>
              </tbody>
            </table>
            <div class="row">
              <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="table_member_info" role="status" aria-live="polite">Showing 21 to 25 of 32 entries</div>
              </div>
              <div class="col-md-7 col-sm-12">
                <div class="dataTables_paginate paging_bootstrap_full_number" id="table_member_paginate">
                  <ul class="pagination pull-right" style="visibility: visible;">
                    <li class="prev"><a href="#" title="最前"><i class="fa fa-angle-double-left"></i></a></li>
                    <li class="prev"><a href="#" title="上一頁"><i class="fa fa-angle-left"></i></a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li class="active"><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li class="next"><a href="#" title="下一頁"><i class="fa fa-angle-right"></i></a></li>
                    <li class="next"><a href="#" title="最後"><i class="fa fa-angle-double-right"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END SETTING TABLE PORTLET-->
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->
