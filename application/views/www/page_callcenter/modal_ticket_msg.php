<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* 客服中心往返的 msg 列表
*/
?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
  <h4 class="modal-title">問答記錄 &nbsp; 主題：<?php echo $_result['member_question'][0]['question_subject']?></h4>
</div>
<div class="modal-body">
  <div class="portlet box">
    <div class="portlet-body" id="chats">
      <ul class="chats">問題說明：<?php echo $_result['member_question'][0]['question_description']?>
<?php	if(@$_result['qna_log']){foreach(@$_result['qna_log'] as $Item){?>
        <li class="<?php echo ($Item['cc_reply_flag'])?'in':'out';?>">
          <img class="avatar" alt="" src="public/img/<?php echo ($Item['cc_reply_flag'])?'icon-supportfemale-48':'icon-diamond-48';?>.png">
          <div class="message">
            <span class="arrow">
            </span>
            <a href="javascript:;" class="name">
            <?php echo $Item['process_ccagent_member_name'];?> </a>
            <span class="datetime">
            at <?php echo $Item['qna_date'];?> </span>
            <span class="body">
            	<?php echo $Item['qna_content'];?>
            </span>
          </div>
        </li>
<?php }}?>
        <!--i class="out">
          <img class="avatar" alt="" src="public/img/icon-diamond-48.png">
          <div class="message">
            <span class="arrow">
            </span>
            <a href="javascript:;" class="name"> Luciel Lo </a>
            <span class="datetime">
            at May 04 2015 20:00 </span>
            <span class="body">
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
          </div>
        </li>
        <li class="in">
          <img class="avatar" alt="" src="public/img/icon-supportfemale-48.png">
          <div class="message">
            <span class="arrow">
            </span>
            <a href="javascript:;" class="name">
            客服人員 </a>
            <span class="datetime">
            at May 04 2015 19:50 </span>
            <span class="body">
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
          </div>
        </li>
        <li class="out">
          <img class="avatar" alt="" src="public/img/icon-diamond-48.png">
          <div class="message">
            <span class="arrow">
            </span>
            <a href="javascript:;" class="name"> Luciel Lo </a>
            <span class="datetime">
            at May 04 2015 19:30 </span>
            <span class="body">
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
          </div>
        </li-->
      </ul>
    </div>
  </div>
</div>
<?php if(!isset($done) || $done=== false):?>
  <!-- 若 ticket 狀態尚未完成則顯示 -->
<form role="form" name="reply_form" method="post" action="member/addCallcenter">
  <div class="modal-footer">
    <div class="row">
      <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9">
        <textarea required name="reply" id="reply" class="form-control" rows="2"></textarea>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3">
        <button type="submit" id="replybuttonx" class="btn blue">送出</button>
        <input type="hidden" name="member_question_sn" value="<?php echo @$_result['member_question'][0]['member_question_sn']; ?>">
      </div>
    </div>
  </div>
</form>
  <!-- /若 ticket 狀態尚未完成則顯示 -->
<?php endif;?>