<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event_type_ct extends CI_Model {

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('sean_models/Sean_db_tools');


		/* Begin 修改  */
		$this->_ct_name="活動類別代碼表";// 功能名稱
		$this->_url="Sysconfig/Event_type_ct/";//列表功能url
		$this->_url_item="Sysconfig/Event_type_ct_item/";//處理動作 url
		$this->_table="ij_event_type_ct";//table name
		$this->_id="event_type";//table key
		$this->_table_field=array(
					 "event_type" => "動動類別代碼",
					 "event_type_name" => "活動類別名稱",
					 "description" => "描述",
					 "sort_order" => "排序順次",
					 "default_flag" => "是否為預設值",
					 "display_flag" => "是否顯示",
				);

		/* End 修改  */

    }

    //取得列表內容
    public function table_value_list( $_where,$_id = 0)
	{
		$_table_key=$this->_id;
	    $page_data['_body_result']=array();
		//判斷是否有帶入特殊查詢
		if(strlen($_where)==0)
		{
			$_where="  order by sort_order asc";

		}
		// 取得列表
		$_result01=$this->Sean_db_tools->db_get_max_record("*",$this->_table,$_where);

		/* 列表標頭名稱 */
		$page_data['_table_thead_name']=$this->_table_field;


		/* 類表內容 */
		if(is_array($_result01))
		{
                 		$_i=0;
                 		foreach($_result01 as $key => $value)
                    	{

			                    		if($value->default_flag == 1)
			                    		{
			                    					$_default_flag = '<i class="fa fa-check"></i>';
			                    		}else{
			                    					$_default_flag = '<i class="fa fa-times"></i>';
			                    		}
										if($value->display_flag == 1)
			                    		{
			                    					$_display_flag = '<i class="fa fa-check"></i>';
			                    		}else{
			                    					$_display_flag = '<i class="fa fa-times"></i>';
			                    		}

			                    		$_body_result='';
						/* Begin 修改  */

										$page_data['_body_result'][$_i][0]=array("_value"=>$value->event_type,"_class"=>"");
			              $page_data['_body_result'][$_i][1]=array("_value"=>$value->event_type_name,"_class"=>"");
										$page_data['_body_result'][$_i][2]=array("_value"=>$value->description,"_class"=>"");
										$page_data['_body_result'][$_i][3]=array("_value"=>$value->sort_order,"_class"=>"");
										$page_data['_body_result'][$_i][4]=array("_value"=>$_default_flag,"_class"=>"");
									  $page_data['_body_result'][$_i][5]=array("_value"=>$_display_flag,"_class"=>"");

						/* End 修改  */


										$page_data['_body_result'][$_i][6]=array("_value"=>'
			                    		 		<a href="'.$this->init_control.$this->_url_item.'edit/'.$value->$_table_key.'"><i class="fa fa-edit"></i>編輯</a>&nbsp;&nbsp;
						                      <a href="'.$this->init_control.$this->_url_item.'delete/'.$value->$_table_key.'" onclick="return deletechecked();"><i class="fa fa-trash-o"></i>刪除</a>&nbsp;&nbsp;
						                  	',"_class"=>'class="center"');
                				$_i++;


                			}

    				}
			return $page_data;

    }

	public function action_edit( $_where = "") {

		$_result="";

		$_result=$this->Sean_db_tools->db_get_max_record("*",$this->_table,$_where);

		return $_result;

	}

	public function action_insert() {

			$_result="";

			foreach($this->_table_field as $key => $value)
			{
					if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
					$_data[$key]=$this->input->get_post($key);
			}



		$_result=$this->Sean_db_tools->db_insert_record($_data,$this->_table);

		return $_result;

	}

	public function action_update($_where) {

			$_result="";

			foreach($this->_table_field as $key => $value)
			{
					if($this->input->get_post($key) || strlen($this->input->get_post($key)) > 0)
					$_data[$key]=$this->input->get_post($key);

			}



		$_result=$this->Sean_db_tools->db_update_record($_data,$this->_table,$_where);

		return $_result;

	}

	public function action_delete( $_where) {

		$_result="";

		$_result=$this->Sean_db_tools->db_delete_record($this->_table,$_where);

		return $_result;

	}

	//取得新排序的值
	public function get_next_sort($_id=0 ) {

		$_result="0";

		$_where=" where ".$this->_id." > 0 order by sort_order desc ";
		$_result=$this->Sean_db_tools->db_get_one_record("sort_order",$this->_table,$_where);

		if(is_array($_result["sort_order"]) )
		$_result=1;
		else
		$_result=$_result["sort_order"]+1;

		return $_result;

	}

	//取得定義參數值
	public function get_define_value($_id)
	{
		if(isset($this->$_id))
		return $this->$_id;
		else
		return "";
	}

}
